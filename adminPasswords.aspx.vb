'JA2010051301 - ENTERPRISE ADMIN SECTION PASSWORD DECRYPTION - Start
Imports System.Configuration.ConfigurationManager
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT.GlobalsClass
Imports ExpandIT
Imports ExpandIT.ExpandITLib

Partial Class adminPasswords
    Inherits ExpandIT.Page

    Const AllowedLogins As String = "mfrabottaCSR|mfrabotta|mfrabottab2b"

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        eis.PageAccessRedirect("HomePage")
        If Not (CBoolEx(globals.User("B2B")) Or CBoolEx(globals.User("B2C"))) Then ShowLoginPage()
        AccessToAdminSection(globals.User("UserLogin"))

        If Page.IsPostBack Then
            Page_Load_PostBack(sender, e)
        End If
    End Sub

    Public Sub AccessToAdminSection(ByVal login As String)
        Dim redirectpage As String
        Dim thePath As String

        If Not isLoginAllowed(login) Then
            If HttpContext.Current.Request.ServerVariables("QUERY_STRING") <> "" Then
                thePath = HttpContext.Current.Request.ServerVariables("SCRIPT_NAME") & "?" & HttpContext.Current.Request.ServerVariables("QUERY_STRING")
            Else
                thePath = HttpContext.Current.Request.ServerVariables("SCRIPT_NAME")
            End If

            redirectpage = VRoot & "/" & AppSettings("URL_NOACCESS") & "?AccessClass=AdminSection&returl=" & HttpContext.Current.Server.UrlEncode(CStrEx(thePath))
            HttpContext.Current.Response.Redirect(redirectpage)
        End If
    End Sub

    Public Function isLoginAllowed(ByVal login As String)
        Dim validLogins As String()
        Dim cont As Integer

        validLogins = AllowedLogins.Split("|")

        If validLogins.Length > 0 Then
            For cont = 0 To validLogins.Length - 1 Step 1
                If login = validLogins(cont) Then
                    Return True
                End If
            Next
        End If
        Return False
    End Function

    Protected Sub Page_Load_PostBack(ByVal sender As Object, ByVal e As System.EventArgs)

        If (Me.txtPasswordInput.Text = "") Then
            Me.txtPasswordOutput.Style.Add("color", "Red")
            Me.txtPasswordOutput.Text = "Please enter a User Login"
        Else
            Dim SQL As String = "SELECT UserPassword FROM UserTable WHERE UserLogin =" & SafeString(Me.txtPasswordInput.Text)
            Dim userDict As String = getSingleValueDB(SQL)
            If userDict <> "" Then
                'DeCrypt
                Me.txtPasswordOutput.Style.Add("color", "Green")
                Me.txtPasswordOutput.Text = Server.HtmlEncode(DecryptXOrWay(userDict))
            Else
                Me.txtPasswordOutput.Style.Add("color", "Red")
                Me.txtPasswordOutput.Text = "Invalid User Login"
            End If
        End If
    End Sub

    Protected Function DecryptXOrWay(ByVal s As Object) As String
        Dim r As String = ""
        Dim i, ch As Integer
        For i = 1 To (Len(s)) Step +2
            ch = CInt("&H" & Mid(s, i, 2))
            ch = ch Xor 111
            r = r & Chr(ch)
        Next
        Return r
    End Function

    Protected Sub DeCryptAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles DeCryptAll.Click
        Dim SQL As String
        SQL = "ALTER TABLE [UserTable] ADD [DeCryptPassword] [nvarchar](50) NULL"
        excecuteNonQueryDb(SQL)

        SQL = "SELECT UserGuid, UserPassword FROM UserTable"
        Dim usersGuids As ExpDictionary = SQL2Dicts(SQL)

        For Each Item As ExpDictionary In usersGuids.Values
            SQL = "UPDATE UserTable SET DeCryptPassword=" & SafeString(Server.HtmlEncode(DecryptXOrWay(Item("UserPassword")))) & " WHERE UserGuid=" & SafeString(Item("UserGuid"))
            excecuteNonQueryDb(SQL)
        Next
        Me.MsgDeCryptAll.Style.Add("color", "Green")
        Me.MsgDeCryptAll.Text = "Passwords DeCrypted successfully"
        Me.MsgRemoveAll.Text = ""
    End Sub

    Protected Sub RemoveAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles RemoveAll.Click
        Dim SQL As String
        SQL = "ALTER TABLE [UserTable] DROP COLUMN [DeCryptPassword]"
        excecuteNonQueryDb(SQL)
        Me.MsgRemoveAll.Style.Add("color", "Green")
        Me.MsgRemoveAll.Text = "Passwords Removed successfully"
        Me.MsgDeCryptAll.Text = ""
    End Sub
End Class
'JA2010051301 - ENTERPRISE ADMIN SECTION PASSWORD DECRYPTION - End
