<%@ Import Namespace="System.Configuration.ConfigurationManager" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="user_lost_password.aspx.vb"
    Inherits="user_lost_password" CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master"
    RuntimeMasterPageFile="ThreeColumn.master" CrumbName="<%$ Resources: Language, LABEL_LOST_PASSWORD_SEND_HEADING %>" %>

<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<%@ Register Src="~/controls/Message.ascx" TagName="Message" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <div class="UserLostPasswordPage">
        <uc1:message id="Message1" runat="server" />
        <uc1:pageheader id="PageHeader1" runat="server" text="<%$ Resources: Language, LABEL_LOST_PASSWORD_SEND_HEADING %>"
            enabletheming="true" />
        <asp:Panel ID="PanelBack" runat="server">
            <input class="AddButton" type="button" onclick="goBack(); return true;" value="<% = Resources.Language.LABEL_BACK %>" />

            <script type="text/javaScript">
			function goBack()
			{ window.history.back(); }
            </script>

        </asp:Panel>
    </div>
</asp:Content>
