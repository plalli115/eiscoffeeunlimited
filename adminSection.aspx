<%--AM2010050502 - ENTERPRISE ADMIN SECTION - Start--%>
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="adminSection.aspx.vb" Inherits="adminSection"
    CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master"
    RuntimeMasterPageFile="ThreeColumn.master" CrumbName="ADMIN SECTION" %>

<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <div class="AccountPage">
        <uc1:PageHeader ID="PageHeader1" runat="server" Text="ADMIN SECTION"
            EnableTheming="true" />
        <ul>
            <li>
                <asp:HyperLink ID="hplAdminSQL" runat="server" NavigateUrl="adminSQL.aspx" Text="Run a SQL Query"></asp:HyperLink>
            </li>
            <li>
                <asp:HyperLink ID="hplAdminPasswordDecryption" runat="server" NavigateUrl="adminPasswords.aspx" Text="DeCrypt Passwords"></asp:HyperLink>
            </li>
        </ul>
    </div>
</asp:Content>
<%--AM2010050502 - ENTERPRISE ADMIN SECTION - End--%>
