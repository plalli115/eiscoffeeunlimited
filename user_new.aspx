<%@ Page Language="VB" AutoEventWireup="false" CodeFile="user_new.aspx.vb" Inherits="user_new"
    CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master"
    RuntimeMasterPageFile="ThreeColumn.master" CrumbName="<%$Resources: Language, LABEL_MENU_ACCOUNT %>" %>

<%@ Register TagPrefix="expandit" TagName="usernew" Src="~/controls/user/Controls_ExpandITUser.ascx" %>
<%@ Reference Control="~/controls/user/Controls_ExpandITUser.ascx" %>
<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <div class="UserNewPage">        
        <asp:PlaceHolder ID="PlaceHolder1" runat="server" />
    </div>
</asp:Content>
