<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%--AM2010102901 - USER NO INFO AFTER SAVE - Start--%>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%--AM2010102901 - USER NO INFO AFTER SAVE - End--%>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="user.aspx.vb" Inherits="user"
    ValidateRequest="false" CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master"
    RuntimeMasterPageFile="ThreeColumn.master" %>
<%--AM2010102901 - USER NO INFO AFTER SAVE - Start--%>
<%@ Register Src="~/controls/Message.ascx" TagName="Message" TagPrefix="uc1" %>
<%--AM2010102901 - USER NO INFO AFTER SAVE - End--%>
<%@ Register TagName="ucB2B" TagPrefix="exp" Src="~/controls/user/Controls_ExpandITB2BUser.ascx" %>
<%@ Register TagName="ucB2C" TagPrefix="exp" Src="~/controls/user/Controls_ExpandITUser.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <div class="UserPage">    
<%--AM2010102901 - USER NO INFO AFTER SAVE - Start--%>    
        <%If CStrEx(Request("UserUpdated") <> "1") Then%>
<%--AM2010102901 - USER NO INFO AFTER SAVE - End--%>
          
            <exp:ucB2C runat="server" ID="ucB2C" />
 
<%--AM2010102901 - USER NO INFO AFTER SAVE - Start--%>
        <%Else %>
            <uc1:Message ID="Message1" runat="server" />
        <%End If%>
<%--AM2010102901 - USER NO INFO AFTER SAVE - End--%>
    </div>
</asp:Content>
