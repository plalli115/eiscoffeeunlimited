﻿'AM2010100101 - ADDRESS VALIDATION - Start
Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.GlobalsClass
Imports ExpandIT.ExpandITLib
Imports System.Collections.Generic
Imports System.Data
Imports System.Net
Imports System.Math
Imports Microsoft.VisualBasic
Imports ExpandIT.Debug


Namespace ExpandIT

    Public Class AddressValidator
        Inherits RequestResponse
        Private responseData As New ExpDictionary
        Private sqlString As String = String.Empty
        Private errorMessage As String = ""
        
        Public WriteOnly Property SqlStatement() As String
            Set(ByVal value As String)
                sqlString = value
            End Set
        End Property

        Protected Overrides Function validate() As Object

            Dim FedexError = ""
            Dim xmlDoc

            Try
            xmlDoc = CreateObject("Microsoft.XMLDOM")
            xmlDoc.async = False
            Dim res As String = Response()
            xmlDoc.loadXML(res)
            xmlDoc.loadXML(xmlDoc.getElementsByTagName("v2:ProposedAddressDetails").item(0).xml)
            FedexError = xmlDoc.getElementsByTagName("v2:Score").Item(0).text

            If (FedexError = 0) Then
                Return False
            Else
                Return True
            End If
            Catch ex As Exception
                Return False
            End Try

        End Function



        ''' <summary>
        ''' Get error description if not successful 
        ''' </summary>
        ''' <returns>Error description string</returns>
        ''' <remarks></remarks>
        Function GetError()
            Dim xmlDoc
            Dim Description As String = ""

            Description = ""

            Try
            xmlDoc = CreateObject("Microsoft.XMLDOM")
            xmlDoc.async = False
            xmlDoc.loadXML(Response())
            xmlDoc.loadXML(xmlDoc.getElementsByTagName("v2:ProposedAddressDetails").Item(0).xml)
            Description = xmlDoc.getElementsByTagName("v2:Changes").Item(0).text

            If Description = "APARTMENT_NUMBER_NOT_FOUND" Then
                Description = "Apartment number not found for this address."
            ElseIf Description = "APARTMENT_NUMBER_REQUIRED" Then
                Description = "Address requires apartment number."
            ElseIf Description = "NORMALIZED" Then
                Description = "Address normalized - abbreviations applied."
            ElseIf Description = "REMOVED_DATA" Then
                Description = "Dropped data.."
            ElseIf Description = "BOX_NUMBER_REQUIRED" Then
                Description = "Address requires box number."
            ElseIf Description = "NO_CHANGES" Then
                Description = "Match - no changes applied to input address."
            ElseIf Description = "MODIFIED_TO_ACHIEVE_MATCH" Then
                Description = "Address modified to achieve match."
            ElseIf Description = "STREET_RANGE_MATCH" Then
                Description = "Match to street range."
            ElseIf Description = "BOX_NUMBER_MATCH" Then
                Description = "Match to box number."
            ElseIf Description = "RR_OR_HC_MATCH" Then
                Description = "Match to Rural Route (RR) / Highway Contract (HC) address."
            ElseIf Description = "CITY_MATCH" Then
                Description = "Match to city (non-US, non-Canada)."
            ElseIf Description = "POSTAL_CODE_MATCH" Then
                Description = "Match to postal code only (non-street)."
            ElseIf Description = "RR_OR_HC_BOX_NUMBER_NEEDED" Then
                Description = "Need box number for Rural Route / Highway Contract (HC) match."
            ElseIf Description = "FORMATTED_FOR_COUNTRY" Then
                Description = "Formatting performed for country (non-US, non-Canada)."
            ElseIf Description = "APO_OR_FPO_MATCH" Then
                Description = "Match to military address (e.g. APO/FPO)."
            ElseIf Description = "GENERAL_DELIVERY_MATCH" Then
                Description = "Match to general delivery."
            ElseIf Description = "FIELD_TRUNCATED" Then
                Description = "Address exceeded 35 character plug-in limit."
            ElseIf Description = "UNABLE_TO_APPEND_NON_ADDRESS_DATA" Then
                Description = "Unable to append non-address; data 35 character limit imposed."
            ElseIf Description = "INSUFFICIENT_DATA" Then
                Description = "Insufficient data for address verification."
            ElseIf Description = "HOUSE_OR_BOX_NUMBER_NOT_FOUND" Then
                Description = "Address (house or box number) not found."
            ElseIf Description = "POSTAL_CODE_NOT_FOUND" Then
                Description = "Postal code not found."
            ElseIf Description = "INVALID_COUNTRY" Then
                Description = "Invalid country."
            ElseIf Description = "SERVICE_UNAVAILABLE_FOR_ADDRESS" Then
                Description = "Service unavailable for request address."
            End If
            Catch ex As Exception
                Description = Resources.Language.MESSAGE_ERROR_VALIDATING_ADDRESS
            End Try



            Return Description

        End Function


        ''' <summary>
        ''' Get Response
        ''' </summary>
        ''' <returns>Response</returns>
        ''' <remarks></remarks>
        Function GetResponse()
            Dim xmlDoc, FedexError, ErrorMsg
            Dim addressR1, cityR, stateR
            Dim zipR As String
            Dim responseData As New ExpDictionary

            xmlDoc = CreateObject("Microsoft.XMLDOM")
            xmlDoc.async = False
            xmlDoc.loadXML(Response())

            xmlDoc.loadXML(xmlDoc.getElementsByTagName("v2:ProposedAddressDetails").Item(0).xml)
            FedexError = xmlDoc.getElementsByTagName("v2:Score").Item(0).text

            If FedexError = 0 Then
                ErrorMsg = GetError()
                responseData("ErrorResponse") = ErrorMsg
            Else
            xmlDoc.loadXML(xmlDoc.getElementsByTagName("v2:Address").Item(0).xml)
            addressR1 = xmlDoc.getElementsByTagName("v2:StreetLines").Item(0).text
            cityR = xmlDoc.getElementsByTagName("v2:City").Item(0).text
            stateR = xmlDoc.getElementsByTagName("v2:StateOrProvinceCode").Item(0).text
            zipR = xmlDoc.getElementsByTagName("v2:PostalCode").Item(0).text
            Dim zipR2 As String() = zipR.Split("-")
            zipR = zipR2(0)

            responseData("Address") = addressR1
            responseData("City") = cityR
            responseData("State") = stateR
            responseData("Zip") = zipR

            ErrorMsg = GetError()
                responseData("MsgResponse") = ErrorMsg
            End If

            'End If

            Return responseData

        End Function

        Public Function AddressValidation(ByVal company As String, ByVal address1 As String, ByVal address2 As String, ByVal city As String, ByVal state As String, ByVal zipCode As String, ByVal Country As String, ByVal ResidentialAddress As Boolean)
            Dim request
            Dim n1, n2, OrderDate
            n1 = DatePart("m", Date.Today.AddDays(1))
            n2 = DatePart("d", Date.Today.AddDays(1))
            OrderDate = DatePart("yyyy", Date.Today.AddDays(1)) & "-" & n1.ToString.PadLeft(2).Replace(" ", "0") & "-" & n2.ToString.PadLeft(2).Replace(" ", "0") & "T"
            OrderDate = DateTime.Now.ToString("o")

            Dim responseData As New ExpDictionary



            request = "<AddressValidationRequest xmlns=""http://fedex.com/ws/addressvalidation/v2"">" & _
                "<WebAuthenticationDetail>" & _
                "<UserCredential>" & _
                "<Key>" & AppSettings("FEDEX_USER_KEY_ADDRESS_VALIDATION") & "</Key>" & _
                "<Password>" & AppSettings("FEDEX_USER_PASSWORD_ADDRESS_VALIDATION") & "</Password>" & _
                "</UserCredential>" & _
                "</WebAuthenticationDetail>"

            request &= "<ClientDetail>" & _
                "<AccountNumber>" & AppSettings("FEDEX_ACCOUNT_NUMBER_ADDRESS_VALIDATION") & "</AccountNumber>" & _
                "<MeterNumber>" & AppSettings("FEDEX_METER_NUMBER_ADDRESS_VALIDATION") & "</MeterNumber>" & _
                "<Localization>" & _
                "<LanguageCode>" & AppSettings("LANGUAGE_DEFAULT") & "</LanguageCode>" & _
                "<LocaleCode>" & Country & "</LocaleCode>" & _
                "</Localization>" & _
                "</ClientDetail>"

            request &= "<TransactionDetail><CustomerTransactionId>AddressValidationFedex</CustomerTransactionId></TransactionDetail>"
            request &= "<Version><ServiceId>aval</ServiceId><Major>2</Major><Intermediate>0</Intermediate><Minor>0</Minor></Version>"
            request &= "<RequestTimestamp>" & OrderDate & "</RequestTimestamp>"

            request &= "<Options>" & _
            "<VerifyAddresses>true</VerifyAddresses>" & _
            "<CheckResidentialStatus>true</CheckResidentialStatus>" & _
            "<MaximumNumberOfMatches>5</MaximumNumberOfMatches>" & _
            "<StreetAccuracy>TIGHT</StreetAccuracy>" & _
            "<DirectionalAccuracy>TIGHT</DirectionalAccuracy>" & _
            "<CompanyNameAccuracy>TIGHT</CompanyNameAccuracy>" & _
            "<ConvertToUpperCase>true</ConvertToUpperCase>" & _
           "</Options>"

            request &= "<AddressesToValidate><Address>"


            If address2 = "" Then
                request &= "<StreetLines>" & address1 & "</StreetLines>"
            Else
                request &= "<StreetLines>" & address1 & " " & address2 & "</StreetLines>"
            End If

            If city = "" Then
            Else
                request &= "<City>" & city & "</City>"
            End If

            If state = "" Then
            Else
                request &= "<StateOrProvinceCode>" & state & "</StateOrProvinceCode>"
            End If
            If zipCode = "" Then
            Else
                request &= "<PostalCode>" & zipCode & "</PostalCode>"
            End If

            request &= "<CountryCode>" & Country & "</CountryCode>"

            request &= "</Address></AddressesToValidate></AddressValidationRequest>"

            'request = "<AddressValidationRequest xmlns=""http://fedex.com/ws/addressvalidation/v2""><WebAuthenticationDetail><UserCredential><Key>k4aNIClRwQZNKtQM</Key><Password>vNO7vAFiUF3gBgtpj3lwAeCWd</Password></UserCredential></WebAuthenticationDetail><ClientDetail><AccountNumber>484380864</AccountNumber><MeterNumber>101171728</MeterNumber><Localization><LanguageCode>en</LanguageCode><LocaleCode>US</LocaleCode></Localization></ClientDetail><TransactionDetail><CustomerTransactionId>AddressValidationFedex</CustomerTransactionId><Localization><LanguageCode>en</LanguageCode><LocaleCode>US</LocaleCode></Localization></TransactionDetail><Version><ServiceId>aval</ServiceId><Major>2</Major><Intermediate>0</Intermediate><Minor>0</Minor></Version><RequestTimestamp>2009-10-15T09:30:47-05:00</RequestTimestamp><Options><VerifyAddresses>true</VerifyAddresses><CheckResidentialStatus>true</CheckResidentialStatus><MaximumNumberOfMatches>5</MaximumNumberOfMatches><StreetAccuracy>TIGHT</StreetAccuracy><DirectionalAccuracy>TIGHT</DirectionalAccuracy><CompanyNameAccuracy>TIGHT</CompanyNameAccuracy><ConvertToUpperCase>true</ConvertToUpperCase></Options><AddressesToValidate><Address><StreetLines>2601 wells ave ste 121</StreetLines><City>fern park</City><StateOrProvinceCode>FL</StateOrProvinceCode><PostalCode>32730</PostalCode><CountryCode>US</CountryCode><Residential>1</Residential></Address></AddressesToValidate></AddressValidationRequest>"

            If Send(request, AppSettings("FEDEX_URL_ADDRESS_VALIDATION")) Then
                responseData = GetResponse()
                'Check if there wasn't any error and if the address proposed is the same as the given one
                If CStrEx(responseData("MsgResponse")) <> "" Then
                    'There was a message, not an error, check the address
                    If CStrEx(responseData("Address")).ToUpper = Trim(CStrEx(address1) & " " & CStrEx(address2)).ToUpper Then
                        If CStrEx(responseData("City")).ToUpper = CStrEx(city).ToUpper Then
                            If CStrEx(responseData("State")).ToUpper = CStrEx(state).ToUpper Then
                                If CStrEx(responseData("Zip")).ToUpper = CStrEx(zipCode).ToUpper Then
                                    responseData.Remove("MsgResponse")
                                    responseData.Remove("Address")
                                    responseData.Remove("City")
                                    responseData.Remove("State")
                                    responseData.Remove("Zip")
                                    responseData("AddressVerified") = "SUCCESS"
                                End If
                            End If
                        End If
                    End If
                End If
            Else
                errorMessage = GetError()
                responseData("ErrorResponse") = errorMessage
                'responseData = GetResponse()
            End If

            Return responseData

        End Function

    End Class
End Namespace
'AM2010100101 - ADDRESS VALIDATION - End