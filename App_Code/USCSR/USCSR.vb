'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Collections.Generic
Imports System.Data
Imports System.Net
Imports ExpandIT.GlobalsClass
Imports ExpandIT31
Imports ExpandIT31.ExpanditFramework.BLLBase

Namespace ExpandIT

    Public Class USCSR

        Private globals As GlobalsClass
        '*****shipping*****-start
        Private shipping As New liveShipping()
        '*****shipping*****-end
        'AM2011032201 - RECURRING ORDERS STAND ALONE - START
        Private recurringObj As USRecurringOrders
        'AM2011032201 - RECURRING ORDERS STAND ALONE - END

        Public Sub New(ByVal _globals As GlobalsClass)
            globals = _globals
            'AM2011032201 - RECURRING ORDERS STAND ALONE - START
            recurringObj = New USRecurringOrders(globals)
            'AM2011032201 - RECURRING ORDERS STAND ALONE - END
        End Sub

        Public Sub setCSR2Session()
            HttpContext.Current.Session("SalesPersonGuid") = HttpContext.Current.Request.Cookies("store").Item("CSRGuid")
        End Sub

        Public Sub setCSR2Cookie()
            HttpContext.Current.Response.Cookies("store")("CSRGuid") = HttpContext.Current.Session("SalesPersonGuid")
        End Sub

        Public Sub setCSROnLogin()
            HttpContext.Current.Session("SalesPersonGuid") = CStrEx(globals.User("SalesPersonGuid"))
            setCSR2Cookie()
        End Sub

        Public Sub AssignSalesPersonGuid(ByRef dict As ExpDictionary)
            Dim SalesPersonGuid As String

            SalesPersonGuid = getSalesPersonGuid()
            If SalesPersonGuid <> "" Then
                dict("SalesPersonGuid") = SalesPersonGuid
            End If
        End Sub

        Public Sub calculateShipping(ByVal orderdict As ExpDictionary)
            If CStrEx(orderdict("DocumentType")) <> "Return Order" Then
                '*****shipping*****-start 
                If Not CBoolEx(AppSettings("EXPANDIT_US_USE_SHIPPING")) Then
                    If AppSettings("SHIPPING_HANDLING_ENABLED") Then
                        globals.eis.CalcShippingHandling(orderdict, globals.Context)
                    End If
                Else
                    If CStrEx(orderdict("ShippingHandlingProviderGuid")) <> "" And AppSettings("SHIPPING_HANDLING_ENABLED") And Not orderdict("ShippingHandlingProviderGuid") Is Nothing And Not orderdict("ServiceCode") Is Nothing And Not orderdict("ShipToZipCode") Is Nothing And Not orderdict("ShipToZipCode") Is DBNull.Value And Not orderdict("ShippingHandlingProviderGuid") Is DBNull.Value Then
                        globals.shippingError = shipping.shippingProviderSelection(orderdict("ShippingHandlingProviderGuid"), orderdict("ServiceCode"), orderdict, globals.Context, orderdict("ShipToZipCode"), globals)
                        shipping.ErrorsHandler(globals)

                    End If
                End If
                '*****shipping*****-end
            End If
            globals.eis.SaveOrderDictionary(orderdict)
        End Sub

        Public Function LoadHoldOrder(ByVal HeaderGuid As String) As ExpDictionary
            Dim dtOrder As DataTable
            Dim Order As ExpDictionary
            Dim exchrate As Decimal
            Dim sql As String
            'Delete the active cart for this Sales Person Code
            sql = "SELECT HeaderGuid FROM CartHeader WHERE HeaderGuid <>" & SafeString(HeaderGuid) & " AND SalesPersonGuid=" & SafeString(getSalesPersonGuid()) & " AND MultiCartStatus='ACTIVE'"
            Dim retval = getSingleValueDB(sql)
            If Not retval Is Nothing Then
                sql = "DELETE FROM CartHeader WHERE HeaderGuid <>" & SafeString(HeaderGuid) & " AND SalesPersonGuid=" & SafeString(getSalesPersonGuid()) & " AND MultiCartStatus='ACTIVE'"
                excecuteNonQueryDb(sql)
                sql = "DELETE FROM CartLine WHERE HeaderGuid=" & SafeString(retval.ToString)
            End If

            sql = ""
            dtOrder = SQL2DataTable("SELECT * FROM CartHeader WHERE HeaderGuid=" & SafeString(HeaderGuid))
            If dtOrder.Rows.Count > 0 Then
                Order = SetDictionary(dtOrder.Rows(0))
                exchrate = CDblEx(Order("CurrencyExchangeRate"))
                If CDblEx(exchrate) = 0 Then exchrate = 1
                sql = "SELECT * FROM CartLine WHERE CartLine.HeaderGuid=" & SafeString(HeaderGuid)
                Order("Lines") = SQL2Dicts(sql, "LineGuid", -1)

            Else
                Order = New ExpDictionary()
                Order("HeaderGuid") = HeaderGuid
                Order("CustomerReference") = "?"
            End If
            LoadHoldOrder = Order
        End Function

        Public Sub setAdjustmentInfo(ByVal dict As Object)
            If CBoolEx(AppSettings("EXPANDIT_US_USE_ADJUSTMENT")) AndAlso getSalesPersonGuid() <> "" Then
                If Not dict("AdjustmentAmount") Is Nothing Then globals.OrderDict("AdjustmentAmount") = CStrEx(dict("AdjustmentAmount"))
                If Not dict("AdjustmentNote") Is Nothing Then globals.OrderDict("AdjustmentNote") = CStrEx(dict("AdjustmentNote"))
            End If
        End Sub

        Public Sub requestAdjustmentInfo(ByVal completeOrder As Boolean, ByRef orderDict As ExpDictionary)
            If getSalesPersonGuid() <> "" Then
                If CBoolEx(AppSettings("EXPANDIT_US_USE_ADJUSTMENT")) Then
                    orderDict("AdjustmentAmount") = HttpContext.Current.Request("AdjustmentAmount")
                    orderDict("AdjustmentNote") = HttpContext.Current.Request("AdjustmentNote")
                    If completeOrder Then
                        'No adjustments should be made without a note
                        If orderDict("AdjustmentAmount") <> 0 And CStrEx(orderDict("AdjustmentNote")) = "" Then
                            globals.messages.Errors.Add(Resources.Language.LABEL_ADJUSTMENT_WITHOUT_NOTE)
                        End If
                    End If
                End If
            End If
        End Sub

        Public Sub clearAdjustmentInfo(ByRef orderDict As ExpDictionary)
            If getSalesPersonGuid() <> "" Then
                If CBoolEx(AppSettings("EXPANDIT_US_USE_ADJUSTMENT")) Then
                    orderDict("AdjustmentAmount") = DBNull.Value
                    orderDict("AdjustmentNote") = DBNull.Value
                End If
            End If
        End Sub

        Public Sub add2ReqDictAdjustment(ByRef req As Dictionary(Of String, ArrayList))
            If CBoolEx(AppSettings("EXPANDIT_US_USE_ADJUSTMENT")) AndAlso getSalesPersonGuid() <> "" Then
                If Not req.ContainsKey("AdjustmentAmount") Then req.Add("AdjustmentAmount", New ArrayList())
                If Not req.ContainsKey("AdjustmentNote") Then req.Add("AdjustmentNote", New ArrayList())
            End If
        End Sub

        Public Sub compareAndUpdateAdjustmentInfo(ByRef OrderObject As ExpDictionary, ByRef bUpdateHeader As Boolean, ByVal req As Dictionary(Of String, ArrayList))
            If CBoolEx(AppSettings("EXPANDIT_US_USE_ADJUSTMENT")) AndAlso getSalesPersonGuid() <> "" Then
                If req("AdjustmentAmount").Count = 1 Then
                    ' Compare old value by db value, these should be the same otherwise the header has changed.
                    If CDblEx(OrderObject("AdjustmentAmount")) = CDblEx(HttpContext.Current.Request("AdjustmentAmount_prev")) Then
                        ' Has the value been updated?
                        If CStrEx(HttpContext.Current.Request("AdjustmentAmount_prev")) <> CStrEx(HttpContext.Current.Request("AdjustmentAmount")) Then
                            OrderObject("AdjustmentAmount") = CStrEx(HttpContext.Current.Request("AdjustmentAmount"))
                            bUpdateHeader = True
                        End If
                    End If
                End If
                If req("AdjustmentNote").Count = 1 Then
                    ' Compare old value by db value, these should be the same otherwise the header has changed.
                    If CStrEx(OrderObject("AdjustmentNote")) = CStrEx(HttpContext.Current.Request("AdjustmentNote_prev")) Then
                        ' Has the value been updated?
                        If CStrEx(HttpContext.Current.Request("AdjustmentNote_prev")) <> CStrEx(HttpContext.Current.Request("AdjustmentNote")) Then
                            OrderObject("AdjustmentNote") = CStrEx(HttpContext.Current.Request("AdjustmentNote"))
                            bUpdateHeader = True
                        End If
                    End If
                End If
            End If
        End Sub

        Public Sub applyAdjustment(ByRef orderDict As ExpDictionary)
            'Calculate the Total if there is an adjustment
            'There is no need to check if the user is a CSR, because only CSRs are able to add an adjustment value on the cart
            If CBoolEx(AppSettings("EXPANDIT_US_USE_ADJUSTMENT")) Then
                orderDict("Total") += CDblEx(orderDict("AdjustmentAmount"))
                orderDict("TotalInclTax") += CDblEx(orderDict("AdjustmentAmount"))
            End If
        End Sub

        Public Sub requestHoldOrderDate(ByRef orderDict As ExpDictionary)
            If getSalesPersonGuid() <> "" AndAlso CBoolEx(AppSettings("EXPANDIT_US_USE_HOLD_ORDER_DATE")) Then
                If CStrEx(HttpContext.Current.Request("HoldOrderDate")) <> "" Then
                    orderDict("HoldOrderDate") = HttpContext.Current.Request("HoldOrderDate")
                End If
            End If
        End Sub

        Public Sub clearHoldOrderDate(ByRef orderDict As ExpDictionary)
            If getSalesPersonGuid() <> "" AndAlso CBoolEx(AppSettings("EXPANDIT_US_USE_HOLD_ORDER_DATE")) Then
                orderDict("HoldOrderDate") = DBNull.Value
            End If
        End Sub

        Public Function getOrderStatusFromHeader(ByVal HeaderGuid As String)

            Dim sql As String
            Dim sql2 As String


            sql = "SELECT COUNT(*) FROM CANCELLEDORDERS WHERE CancelOrder=1 AND HEADERGUIDORIGINAL=" & SafeString(HeaderGuid)

            If CIntEx(getSingleValueDB(sql)) > 0 Then
                Return Resources.Language.LABEL_CSR_STATUS_CANCELLED
            End If

            sql = "SELECT COUNT(*) FROM SHOPSALESHEADER WHERE DOCUMENTTYPE='Return Order' AND OrderReference IN (SELECT CUSTOMERREFERENCE FROM SHOPSALESHEADER WHERE HEADERGUID=" & SafeString(HeaderGuid) & ")"

            If CIntEx(getSingleValueDB(sql)) > 0 Then
                Return Resources.Language.LABEL_CSR_STATUS_RETURNED
            End If


            sql = "SELECT COUNT(*) FROM SHOPSALESHEADER WHERE DOCUMENTTYPE='Return Order' AND OrderReference IS NULL AND HEADERGUID=" & SafeString(HeaderGuid)

            If CIntEx(getSingleValueDB(sql)) > 0 Then
                Return Resources.Language.LABEL_CSR_STATUS_RETURNED
            End If

            sql = "SELECT SUM(Quantity) FROM SalesShipmentLine WHERE DocumentGuid=(SELECT TOP 1 DocumentGuid " & _
                    "FROM [SalesShipmentHeader] SS INNER JOIN ShopSalesHeader SSH ON " & _
                    "SS.CustomerReference=SSH.CustomerReference WHERE SSH.HeaderGuid =" & SafeString(HeaderGuid) & _
                    " ORDER BY SS.HeaderDate) AND Quantity>0 AND Type=2 "

            sql2 = "SELECT SUM(Quantity) FROM ShopSalesLine WHERE HeaderGuid=" & SafeString(HeaderGuid)

            If CIntEx(getSingleValueDB(sql)) > 0 Then
                If CIntEx(getSingleValueDB(sql)) < CIntEx(getSingleValueDB(sql2)) Then
                    Return Resources.Language.LABEL_CSR_STATUS_PARTIALLY_SHIPPED
                Else
                    Return Resources.Language.LABEL_CSR_STATUS_SHIPPED
                End If
            End If

            sql = "SELECT PaymentType FROM ShopSalesHeader WHERE HeaderGuid=" & SafeString(HeaderGuid)
            If CStrEx(getSingleValueDB(sql)) = "PAPERCHECK" Then
                sql2 = "SELECT CustomerReference FROM ShopSalesHeader WHERE HeaderGuid=" & SafeString(HeaderGuid)
                sql = "SELECT * FROM PaperChecks WHERE OrderNumber=" & SafeString(getSingleValueDB(sql2))
                Dim checks As ExpDictionary = SQL2Dicts(sql)
                Dim count As Double = 0
                If Not checks Is Nothing AndAlso Not checks Is DBNull.Value AndAlso checks.Count > 0 Then
                    For Each item As ExpDictionary In checks.Values
                        count += CDblEx(item("PaperCheckTotals"))
                    Next
                End If
                sql = "SELECT PaperCheckTotals FROM ShopSalesHeader WHERE HeaderGuid=" & SafeString(HeaderGuid)
                sql2 = "SELECT TotalInclTax FROM ShopSalesHeader WHERE HeaderGuid=" & SafeString(HeaderGuid)
                If CDblEx(getSingleValueDB(sql)) + count < CDblEx(getSingleValueDB(sql2)) Then
                    Return Resources.Language.LABEL_CSR_STATUS_UNDERPAID
                End If
            End If

            Return Resources.Language.LABEL_CSR_STATUS_ORDERED

        End Function

        Public Function getSalesPersonGuid() As String
            Try
                If Not CBoolEx(globals.User("Anonymous")) Then
                    If CStrEx(HttpContext.Current.Request.Cookies("store").Item("CSRGuid")) <> "" Then
                        setCSR2Session()
                        Return HttpContext.Current.Session("SalesPersonGuid")
                    Else
                        Return ""
                    End If
                Else
                    HttpContext.Current.Session("SalesPersonGuid") = ""
                    Return ""
                End If
            Catch ex As Exception
                Return ""
            End Try
        End Function

        Function getTrackingNo(ByVal HeaderGuid As String)
            Dim sql As String = "SELECT SS.HeaderDate, SS.PackageTrackingNo " & _
                    "FROM [SalesShipmentHeader] SS INNER JOIN ShopSalesHeader SSH ON " & _
                    "SS.CustomerReference=SSH.CustomerReference WHERE SSH.HeaderGuid =" & SafeString(HeaderGuid) & _
                    " ORDER BY SS.HeaderDate"
            Dim tracking As ExpDictionary = SQL2Dicts(sql)
            Return tracking
        End Function

        Public Function historyLookupWarningNoRefRedirect(ByVal IsCSR As String, ByVal errorStr As String) As String
            If IsCSR = "1" And getSalesPersonGuid() <> "" Then
                Return "history_lookup.aspx?CSR=1&errstr=" & errorStr
            Else
                Return "history_lookup.aspx?errstr=" & errorStr
            End If
        End Function

        Public Function historyLookupDetailRedirect(ByVal IsCSR As String, ByVal headerGuidStr As String) As String
            If IsCSR = "1" And getSalesPersonGuid() <> "" Then
                Return "history_detail.aspx?CSR=1&HeaderGuid=" & headerGuidStr
            Else
                Return "history_detail.aspx?HeaderGuid=" & headerGuidStr
            End If
        End Function

        Public Function historyLookupWarningRefNotFoundRedirect(ByVal IsCSR As String) As String
            If IsCSR = "1" And getSalesPersonGuid() <> "" Then
                Return "history_lookup.aspx?CSR=1&" & globals.messages.QueryString()
            Else
                Return "history_lookup.aspx?" & globals.messages.QueryString()
            End If
        End Function

        Public Function historyLookupHistoryRedirect(ByVal IsCSR As String) As String
            If IsCSR = "1" And getSalesPersonGuid() <> "" Then
                Return "history.aspx?CSR=1"
            Else
                Return "history.aspx"
            End If
        End Function

        Public Sub historyLookupTransfer()
            If CStrEx(HttpContext.Current.Request("CSR")) = "1" And getSalesPersonGuid() <> "" Then
                HttpContext.Current.Server.Transfer(VRoot & "/_history_lookup.aspx?CSR=1", False)
            Else
                HttpContext.Current.Server.Transfer(VRoot & "/_history_lookup.aspx", False)
            End If
        End Sub

        Public Sub deleteCSRActiveOrders()
            Dim sql As String
            ' Delete any active orders from this CSR 
            sql = "DELETE FROM CartLine WHERE HeaderGuid IN (SELECT HeaderGuid FROM CartHeader WHERE MultiCartStatus='ACTIVE' AND (SalesPersonGuid=" & SafeString(globals.User("SalesPersonGuid")) & " OR UserGuid=" & SafeString(globals.User("UserGuid")) & "))"
            excecuteNonQueryDb(sql)
            sql = "DELETE FROM CartHeader WHERE MultiCartStatus='ACTIVE' AND (SalesPersonGuid=" & SafeString(globals.User("SalesPersonGuid")) & " OR UserGuid=" & SafeString(globals.User("UserGuid")) & ")"
            excecuteNonQueryDb(sql)
        End Sub

        Public Sub clearCSRCookieAndSession()
            HttpContext.Current.Session("SalesPersonGuid") = ""
            HttpContext.Current.Response.Cookies("CSRGuid").Value = ""
        End Sub

        Public Function csrTopMenu() As MenuItem
            If getSalesPersonGuid() <> "" And globals.eis.CheckPageAccess("HomePage") Then
                Dim itm As MenuItem
                itm = New MenuItem
                itm.Id = "CSR"
                itm.URL = VRoot & "/CSR.aspx"
                itm.Caption = Resources.Language.LABEL_MENU_CSR
                Return itm
            End If
            Return Nothing
        End Function

        Public Function csrBottomMenu() As MenuItemBottom
            If getSalesPersonGuid() <> "" And globals.eis.CheckPageAccess("HomePage") Then
                Dim itm As MenuItemBottom
                itm = New MenuItemBottom
                itm.Id = "CSR"
                itm.URL = VRoot & "/CSR.aspx"
                itm.Caption = Resources.Language.LABEL_MENU_CSR
                Return itm
            End If
            Return Nothing
        End Function

        Public Sub buildDict2UpdateSQLs(ByVal colNameDict As ExpDictionary, ByVal item As KeyValuePair(Of Object, Object), ByRef sb As StringBuilder, ByVal tableName As String, ByVal cond As String)
            If item.Key <> "SalesPersonGuid" Then
                If colNameDict.Exists(item.Key) Then
                    sb.Append(" UPDATE " & tableName & " SET ")
                    sb.Append(item.Key)
                    sb.Append(" = ")
                    sb.Append(SafeString(item.Value))
                    sb.Append(cond)
                    sb.AppendLine(";")
                End If
            End If
        End Sub

        Public Function userStatus() As String
            If getSalesPersonGuid() <> "" Then
                Return "CSR"
            Else
                Return "*determine*"
            End If
        End Function

        Public Function PossibleShippingProvidersSQLString(ByVal isAppendConstraint As Boolean, ByVal userStatusPrm As String) As String
            Dim sql As New StringBuilder()
            Dim userType As String = ""

            userType = userStatus()
            If userType = "*determine*" Then
                userType = userStatusPrm
            End If

            ' Find out if there is data in the PaymentType_ShippingHandlingProvider table
            ' If there is no data in the PaymentType_ShippingHandlingProvider table this means 
            ' that no relations are defined on valid combinations of payment types and shipping handling providers
            Dim o As Object = getSingleValueDB("SELECT TOP 1 ShippingProviderGuid FROM PaymentType_ShippingHandlingProvider")

            If o IsNot Nothing Then
                ' We have a relationship between payment types and shipping handling providers
                If userType <> "CSR" Then
                    sql.Append("SELECT ShippingHandlingProviderGuid, ProviderName FROM ShippingHandlingProvider " & _
                                "WHERE ShippingHandlingProviderGuid IN(SELECT DISTINCT ShippingProviderGuid FROM PaymentType_ShippingHandlingProvider " & _
                                    "WHERE PaymentType IN(SELECT PaymentType FROM PaymentType WHERE UserType = '" & userType & "') AND ShippingProviderGuid NOT LIKE 'CSR%') ")
                    If isAppendConstraint Then
                        sql.Append(" AND ShippingHandlingProviderGuid NOT IN ('NONE') ORDER BY SortIndex")
                    End If
                Else
                    sql.Append("SELECT ShippingHandlingProviderGuid, ProviderName FROM ShippingHandlingProvider " & _
                                "WHERE ShippingHandlingProviderGuid IN(SELECT DISTINCT ShippingProviderGuid FROM PaymentType_ShippingHandlingProvider " & _
                                "WHERE PaymentType IN(SELECT PaymentType FROM PaymentType WHERE UserType = '" & userType & "')) ")
                    If isAppendConstraint Then
                        sql.Append(" AND ShippingHandlingProviderGuid NOT IN ('NONE') ORDER BY SortIndex")
                    End If
                End If
            Else
                ' We don't have any relationships between payment types and shipping handling providers,
                ' so we just want to get all of the providers in the ShippingHandlingProvider table
                sql.Append("SELECT ShippingHandlingProviderGuid, ProviderName FROM ShippingHandlingProvider ")
                If userType <> "CSR" Then
                    sql.Append(" WHERE ShippingHandlingProviderGuid NOT LIKE 'CSR%'")
                End If
            End If

            Return sql.ToString()
        End Function

        Public Function PossiblePaymentTypesSQLString(ByVal userStatusPrm As String, ByVal CustomerShippingProvider As String, Optional ByVal isAppendConstraint As Boolean = False) As String
            Dim userType As String = ""

            userType = userStatus()
            If userType = "*determine*" Then
                userType = userStatusPrm
            End If

            Dim sql As New StringBuilder()

            ' Find out if there is data in the PaymentType_ShippingHandlingProvider table
            ' If there is no data in the PaymentType_ShippingHandlingProvider table this means 
            ' that no relations are defined on valid combinations of payment types and shipping handling providers
            Dim o As Object = getSingleValueDB("SELECT TOP 1 ShippingProviderGuid FROM PaymentType_ShippingHandlingProvider")
            If o IsNot Nothing Then
                ' We have a relationship between payment types and shipping handling providers
                If IsSQL2005 Then
                    ' Use the newer SQL 2005 syntax
                    sql.Append("SELECT PaymentType FROM PaymentType WHERE UserType = '" & userType & "'")
                    sql.Append(" INTERSECT SELECT PaymentType FROM PaymentType_ShippingHandlingProvider WHERE ")
                    If isAppendConstraint Then
                        sql.Append(" ShippingProviderGuid NOT IN ('NONE')")
                    Else
                        sql.Append(" ShippingProviderGuid = " & SafeString(CustomerShippingProvider))
                    End If
                Else
                    ' Use the older SQL 2000 syntax
                    sql.Append("SELECT PaymentType FROM PaymentType p WHERE UserType = '" & userType & "'")
                    sql.Append(" AND EXISTS")
                    sql.Append(" (SELECT PaymentType FROM PaymentType_ShippingHandlingProvider ps WHERE ")
                    If isAppendConstraint Then
                        sql.Append(" ShippingProviderGuid NOT IN ('NONE')")
                    Else
                        sql.Append(" ShippingProviderGuid = " & SafeString(CustomerShippingProvider))
                    End If
                    sql.Append(" AND p.PaymentType = ps.PaymentType )")
                End If
            Else
                ' We don't have any relationships between payment types and shipping handling providers,
                ' so we just want to get all of the payment types in the PaymentType table
                sql.Append("SELECT PaymentType FROM PaymentType WHERE UserType = '" & userType & "'")
            End If

            Return sql.ToString()

        End Function

        Public Function findCartUpdateMethod()
            If CStrEx(HttpContext.Current.Request("Save")) <> "" Then
                Return "save"
            Else
                Return ""
            End If
        End Function

        Public Function checkCSRAllowed2Order(ByVal type As String) As Boolean
            If Not CBoolEx(AppSettings("CSR_ALLOWED_TO_ORDER")) Then
                'Check if the order is for a CSR user
                Dim orderForCSR As Boolean = False
                orderForCSR = getSingleValueDB("SELECT COUNT(*) FROM UserTableB2B WHERE UserGuid=" & SafeString(globals.OrderDict("UserGuid")) & " AND UserType=4") > 0
                If orderForCSR Then
                    If type = "order" Then
                        globals.messages.Errors.Add(Resources.Language.LABEL_CSR_NOT_ALLOWED_TO_ORDER)
                    ElseIf type = "park" Then
                        globals.messages.Errors.Add(Resources.Language.LABEL_PARK_SALES_PERSON)
                    End If
                    Return False
                End If
            End If
            Return True
        End Function

        Public Sub saveOrder(ByRef myUrl As String, ByVal updateMethod As String, ByVal bLinesHaveBeenRemoved As Boolean, ByVal thePage As Page, ByRef cartPanel As Panel, ByVal infoString As String, ByRef ph As PlaceHolder)
            If updateMethod = "save" Then
                'JA2010092201 - RECURRING ORDERS - START
                If CBoolEx(globals.OrderDict("RecurringOrder")) Then
                    recurringObj.applyRecurring(globals.OrderDict)
                End If
                'JA2010092201 - RECURRING ORDERS - END
                If Not checkCSRAllowed2Order("park") Then
                    myUrl = "cart.aspx?GroupGuid=" & globals.GroupGuid
                    HttpContext.Current.Response.Redirect(MakeParam(myUrl, globals.messages.QueryString, infoString))
                    'Return False
                Else
                    globals.OrderDict("MultiCartStatus") = "HOLD"
                    globals.OrderDict("CustomerReference") = globals.eis.GenCustRef()
                    globals.eis.SaveOrderDictionary(globals.OrderDict)
                    Dim MessageSaved As Control = ph.Controls(0).FindControl("MessageSaved")
                    MessageSaved.Visible = True
                    cartPanel.Visible = False
                    'Return True
                End If
            Else
                If bLinesHaveBeenRemoved > 0 Then
                    myUrl = HttpContext.Current.Request("RetURL") & "?LinesHaveBeenRemoved=" & bLinesHaveBeenRemoved
                Else
                    myUrl = HttpContext.Current.Request("RetURL")
                End If
                'Return False
                HttpContext.Current.Response.Redirect(MakeParam(myUrl, globals.messages.QueryString, infoString))
            End If
        End Sub

        ' Helper function: Adds the error string to the url.
        Private Function MakeParam(ByVal url As String, ByVal errorString As String, ByVal infoString As String) As String
            Dim retv As String

            retv = url
            If errorString <> "" Then
                retv = retv & IIf(InStr(1, retv, "?") = 0, "?", "&") & errorString
            End If
            If infoString <> "" Then
                retv = retv & IIf(InStr(1, retv, "?") = 0, "?", "&") & infoString
            End If

            Return retv
        End Function

        Public Sub outOfBasketSetShippingValue(ByRef orderObject As ExpDictionary)
            If CStrEx(OrderObject("DocumentType")) <> "Return Order" Then
                '*****shipping*****-start
                OrderObject("ShippingAmount") = 0
                '*****shipping*****-end
            End If
        End Sub

        Public Sub parkAllowed(ByVal ph As PlaceHolder)
            If getSalesPersonGuid() <> "" And Not globals.IsCartEmpty Then
                controlVisible(ph, "parkBtn", True)
            Else
                controlVisible(ph, "parkBtn", False)
            End If
        End Sub

        Public Sub cartLoadHoldOrder(ByRef cartRender As Object)
            If CStrEx(HttpContext.Current.Request("HeaderGuidHold")) <> "" Then
                globals.OrderDict = LoadHoldOrder(HttpContext.Current.Request("HeaderGuidHold"))
                globals.OrderDict("MultiCartStatus") = "ACTIVE"
                globals.OrderDict("IsCalculated") = True
                cartRender.IsActiveOrder = True
                globals.eis.SaveOrderDictionary(globals.OrderDict)
            End If
        End Sub

        Public Sub cartLoadReturnOrder(ByRef cartRender As Object, ByRef purchase As Button, ByVal phPark As PlaceHolder, ByVal phReturn As PlaceHolder)
            If CStrEx(globals.OrderDict("DocumentType")) = "Return Order" Then
                controlVisible(phPark, "parkBtn", False)
                controlVisible(phReturn, "Returned", True)
                cartRender.isReturn = True
                purchase.Visible = False
            End If
        End Sub

        Public Sub noReturnRecalculate(ByRef OrderDict As ExpDictionary)
            If CStrEx(globals.OrderDict("DocumentType")) <> "Return Order" Then
                OrderDict("IsCalculated") = 0
                OrderDict("ReCalculated") = 1
            End If
        End Sub

        Public Sub notRefundShipping(ByVal ph As PlaceHolder)
            If CBoolEx(globals.OrderDict("NotRefundShipping")) = True Then
                'refundShipping.Checked = True
                cbxChecked(ph, "RefundShipping", True)
            Else
                'refundShipping.Checked = False
                cbxChecked(ph, "RefundShipping", False)
            End If
        End Sub

        Public Sub completeReturn(ByRef CartPaymentAddress As Object, ByRef CartShippingAddress As Object)
            Dim line As ExpDictionary

            For Each line In globals.OrderDict("Lines").Values
                Dim reason As String = HttpContext.Current.Request(line("ProductGuid"))
                line("ReturnReason") = reason
                line("DocumentType") = "Return Order"
            Next
            globals.OrderDict("HeaderComment") = CStrEx(HttpContext.Current.Request("HeaderComment"))
            globals.OrderDict("CustomerPONumber") = CStrEx(HttpContext.Current.Request("CustomerPONumber"))
            globals.eis.SaveOrderDictionary(globals.OrderDict)

            Dim HeaderGuid As String = ""
            Dim aKey As KeyValuePair(Of Object, Object)

            ' Prepare the new id for the ShopSalesHeader
            If String.IsNullOrEmpty(ExpandITLib.DBNull2Nothing(globals.OrderDict("HeaderGuid"))) Then
                Try
                    HeaderGuid = GenGuid()
                Catch ex As Exception

                End Try
            Else
                HeaderGuid = globals.OrderDict("HeaderGuid")
            End If

            ' Copy cart lines to order lines
            ' Set new id's for the order to be saved.
            For Each aKey In globals.OrderDict("Lines")
                globals.OrderDict("Lines")(aKey.Key)("HeaderGuid") = HeaderGuid
                globals.OrderDict("Lines")(aKey.Key)("LineGuid") = GenGuid()
            Next

            ' Store the order lines in the ShopSalesLine table.
            Dicts2Table(globals.OrderDict("Lines"), "ShopSalesLine", "")

            ' Field maps/updates
            globals.OrderDict("HeaderGuid") = HeaderGuid
            globals.OrderDict("globals.UserGuid") = HttpContext.Current.Session("UserGuid")


            ' Set ShopID (for telmi)
            globals.OrderDict("WebShopID") = globals.shopID

            globals.eis.SaveOrderDictionary(globals.OrderDict)

            CartPaymentAddress = globals.User
            CartShippingAddress = globals.User

            globals.OrderDict = globals.eis.LoadOrderDictionary(globals.User)

            globals.OrderDict("HeaderDate") = Now
            ' Store order header data in the ShopSalesHeader table.
            If Dict2Table(globals.OrderDict, "ShopSalesHeader", "") Then 'check save success             
                ' Empty the shopping cart.
                ' Delete cart lines
                excecuteNonQueryDb("DELETE FROM CartLine WHERE HeaderGuid=" & SafeString(HeaderGuid))

                ' Delete cart
                excecuteNonQueryDb("DELETE FROM CartHeader WHERE HeaderGuid=" & SafeString(HeaderGuid))

                ' Update the cartcounters. Call with Empty aregument to clear all counters.
                globals.eis.UpdateMiniCartInfo(Nothing)

            End If
            HttpContext.Current.Response.Redirect(VRoot & "/confirmed.aspx?HeaderGuid=" & HttpContext.Current.Server.UrlEncode(HeaderGuid) & "&return=return")
        End Sub

        Public Sub refundShippingCheckedChanged(ByRef refundShipping As CheckBox)
            If refundShipping.Checked Then
                globals.OrderDict("NotRefundShipping") = True
                globals.OrderDict("Total") = CDblEx(globals.OrderDict("Total")) - CDblEx(globals.OrderDict("ShippingAmount")) - CDblEx(globals.OrderDict("HandlingAmount"))
                If CDblEx(globals.OrderDict("ShippingAmountInclTax")) = 0 Then
                    globals.OrderDict("ShippingAmountInclTax") = CDblEx(globals.OrderDict("ShippingAmount"))
                End If
                If CDblEx(globals.OrderDict("HandlingAmountInclTax")) = 0 Then
                    globals.OrderDict("HandlingAmountInclTax") = CDblEx(globals.OrderDict("HandlingAmount"))
                End If
                globals.OrderDict("TotalInclTax") = CDblEx(globals.OrderDict("TotalInclTax")) - CDblEx(globals.OrderDict("ShippingAmountInclTax")) - CDblEx(globals.OrderDict("HandlingAmountInclTax"))
                globals.OrderDict("ShippingAmount") = 0
                globals.OrderDict("ShippingAmountInclTax") = 0
                globals.OrderDict("HandlingAmount") = 0
                globals.OrderDict("HandlingAmountInclTax") = 0
                globals.OrderDict("ShippingHandlingProviderGuid") = "NONE"
                globals.OrderDict("ServiceCode") = "NONE"
                globals.eis.SaveOrderDictionary(globals.OrderDict)
                refundShipping.Enabled = False
            End If
        End Sub

        Function getLinkToHistoryDetail(ByVal order As ExpDictionary)
            Dim str As String = ""

            Dim sql As String = "SELECT HeaderGuid FROM ShopSalesHeader WHERE CustomerReference IN (SELECT ModifiedRecurrency FROM ShopSalesHeader WHERE (CustomerReference = " & SafeString(CStrEx(Order("CustomerReference"))) & "))"
            Dim headerguid As String = CStrEx(getSingleValueDB(sql))
            If headerguid <> "" Then
                str = "<a href=""" & VRoot & "/history_detail.aspx?HeaderGuid=" & headerguid & """>" & CStrEx(order("CustomerReference")) & "</a>"
            Else
                str = "<a href=""" & VRoot & "/history_detail.aspx?HeaderGuid=" & order("HeaderGuid") & """>" & CStrEx(order("CustomerReference")) & "</a>"
            End If

            Return str

        End Function

        Function getLinkToHistoryDetailReturnOrders(ByVal order As ExpDictionary)
            Dim str As String = ""

            Dim sql As String = "SELECT HeaderGuid FROM ShopSalesHeader WHERE CustomerReference IN (SELECT OrderReference FROM ShopSalesHeader WHERE (HeaderGuid = " & SafeString(Order("HeaderGuid")) & " ))"
            Dim headerguid As String = CStrEx(getSingleValueDB(sql))
            If headerguid <> "" Then
                sql = "SELECT OrderReference FROM ShopSalesHeader WHERE (HeaderGuid = " & SafeString(Order("HeaderGuid")) & " )"
                Dim custref As String = CStrEx(getSingleValueDB(sql))
                str = "<a href=""" & VRoot & "/history_detail.aspx?HeaderGuid=" & headerguid & "&CustRef=" & custref & """>" & CStrEx(order("CustomerReference")) & "</a>"
            Else
                str = "<a href=""" & VRoot & "/history_detail.aspx?HeaderGuid=" & order("HeaderGuid") & """>" & CStrEx(order("CustomerReference")) & "</a>"
            End If

            Return str

        End Function

        Public Sub loginAuthenticated(ByRef lbLoginStatus As LinkButton)
            If getSalesPersonGuid() <> "" Then
                Dim salesPersonName As String = CStrEx(getSingleValueDB("SELECT ContactName FROM UserTableB2B WHERE SalesPersonGuid=" & SafeString(getSalesPersonGuid())))
                lbLoginStatus.Text = Replace(Resources.Language.LABEL_LOGGED_IN_AS_NAME, "%1", salesPersonName)
            End If
        End Sub

        Public Sub userOnDataObjectCreating(ByRef bllObject As ExpanditFramework.BLLBase.UserBase)
            If getSalesPersonGuid() <> "" And CStr(HttpContext.Current.Session("NewUserGuid")) <> "" Then
                bllObject = ExpanditFactory.CreateUser(CStr(HttpContext.Current.Session("NewUserGuid"))) 'Retreive the User object
            Else
                bllObject = ExpanditFactory.CreateUser(CStr(HttpContext.Current.Session("UserGuid"))) 'Retreive the User object
            End If
        End Sub

        Public Sub userOnItemUpdated(ByVal useConfirmedRegistration As Boolean, ByVal newValue As UserBase.UserStates)
            If getSalesPersonGuid() <> "" And CStrEx(HttpContext.Current.Session("NewUserGuid")) <> "" Then
                If globals.OrderDict Is Nothing Then
                    globals.OrderDict = globals.eis.LoadOrderDictionary(globals.User)
                End If
                globals.eis.LoadUser(CStrEx(HttpContext.Current.Session("NewUserGuid")))
                HttpContext.Current.Session("UserGuid") = CStrEx(HttpContext.Current.Session("NewUserGuid"))
                HttpContext.Current.Session("NewUserGuid") = ""
                HttpContext.Current.Response.Cookies("store")("UserGuid") = CStrEx(HttpContext.Current.Session("UserGuid"))
                setCSR2Cookie()
                globals.eis.SetCookieExpireDate()
                If Not globals.OrderDict Is Nothing Then
                    globals.OrderDict("SalesPersonGuid") = getSalesPersonGuid()
                    globals.OrderDict("UserGuid") = CStrEx(HttpContext.Current.Session("UserGuid"))
                    globals.eis.SaveOrderDictionary(globals.OrderDict)
                End If
                HttpContext.Current.Items.Add("messages", globals.messages.ContextItems)
                HttpContext.Current.Server.Transfer(VRoot & "/user.aspx?UserUpdated=1")
            Else
                If useConfirmedRegistration AndAlso (newValue = UserBase.UserStates.New) Then
                    ResetAutoLogin()
                    HttpContext.Current.Response.Redirect(VRoot & "/user-confirm_reg.aspx")
                Else
                    If HttpContext.Current.Request.QueryString("bouncecheckout") = "true" Then
                        'Transfer to the url contained in the query string
                        HttpContext.Current.Server.Transfer(HttpContext.Current.Request.QueryString("bouncereturl"))
                    Else
                        'A little trick to reset the formview after a successful update/insert 
                        'The PageMessages are added to the Context.Items so they can be shown after redirection
                        HttpContext.Current.Items.Add("messages", globals.messages.ContextItems)
                        'AM2010102901 - USER NO INFO AFTER SAVE - Start
                        'Server.Transfer(Request.UrlReferrer.AbsolutePath & "?UserUpdated=1")
                        HttpContext.Current.Server.Transfer(VRoot & "/user.aspx?UserUpdated=1")
                        'AM2010102901 - USER NO INFO AFTER SAVE - End
                    End If
                End If
            End If
        End Sub

        Private Sub ResetAutoLogin()
            ' Delete users cookie
            HttpContext.Current.Session("UserGuid") = ""
            HttpContext.Current.Response.Cookies("store")("UserGuid") = ""
            setCSR2Cookie()
            HttpContext.Current.Response.Cookies("store").Path = HttpContext.Current.Server.UrlPathEncode(VRoot) & "/"
            globals.eis.UpdateMiniCartInfo(Nothing)
        End Sub

        Public Function isReturn(ByVal dict As ExpDictionary)
            Return CStrEx(dict("DocumentType")) = "Return Order"
        End Function

        Public Sub buslogicSumInclTax(ByRef AmountSumInclTax As Decimal, ByVal orderLine As ExpDictionary)
            If isReturn(orderLine) Then
                AmountSumInclTax += CDblEx(orderLine("SubTotalInclTax"))
            End If
        End Sub

        Public Sub buslogicSetTotals(ByRef orderobject As ExpDictionary, ByVal amountSumInclTax As Decimal, ByVal amountSum As Decimal)
            orderobject("SubTotalInclTax") = amountSumInclTax
            orderobject("TotalInclTax") = amountSumInclTax
            orderobject("TaxAmount") = amountSumInclTax - amountSum
        End Sub

        Public Sub buslogicSetOrderLineTotals(ByRef orderline As ExpDictionary)
            If isReturn(orderline) Then
                orderline("SubTotalInclTax") = orderline("Quantity") * orderline("ListPriceInclTax")
                orderline("TotalInclTax") = orderline("SubTotalInclTax")
            End If
        End Sub

        Public Sub addControl2PlaceHolder(ByRef thePage As Page, ByRef ph As PlaceHolder, ByVal controlURL As String, Optional ByVal properties As ExpDictionary = Nothing, Optional ByVal clearControls As Boolean = True)
            Dim myControl As Control = thePage.LoadControl(controlURL)
            myControl.ID = ph.ID & "Ctrl1"
            If properties IsNot Nothing Then
                For Each propertyDict As ExpDictionary In properties.Values
                    globals.eis.SetControlProperties(myControl, propertyDict("propName"), propertyDict("propValue"))
                Next
            End If
            If clearControls Then
                ph.Controls.Clear()
            End If
            ph.Controls.Add(myControl)
        End Sub

        Public Sub controlVisible(ByVal ph As PlaceHolder, ByVal controlName As String, ByVal visible As Boolean)
            Dim _control As Control
            Try
                _control = ph.Controls(0).FindControl(controlName)
                _control.Visible = visible
            Catch ex As Exception

            End Try
        End Sub

        Public Sub cbxChecked(ByVal ph As PlaceHolder, ByVal checkBoxName As String, ByVal checked As Boolean)
            Dim _cbx As CheckBox
            Try
                _cbx = ph.Controls(0).FindControl(checkBoxName)
                _cbx.Checked = checked
            Catch ex As Exception

            End Try
        End Sub

        Public Sub cartLoadPlaceHolders(ByRef thePage As Page, ByRef phCSRCartRefundShipping As PlaceHolder, ByRef phCSRCartPark As PlaceHolder, ByRef phCSRCartParkSaved As PlaceHolder, ByRef phCSRCartReturn As PlaceHolder, ByVal CartPaymentAddress As ExpDictionary, ByVal CartShippingAddress As ExpDictionary)
            addControl2PlaceHolder(thePage, phCSRCartRefundShipping, VRoot & "/controls/USCSR/CSRCartRefundShipping.ascx")
            addControl2PlaceHolder(thePage, phCSRCartPark, VRoot & "/controls/USCSR/CSRCartPark.ascx")
            addControl2PlaceHolder(thePage, phCSRCartParkSaved, VRoot & "/controls/USCSR/CSRCartParkSaved.ascx")
            Dim CSRCartReturnPropsDict As New ExpDictionary
            Dim propertyDict As New ExpDictionary
            propertyDict("propName") = "CartPaymentAddress"
            propertyDict("propValue") = CartPaymentAddress
            CSRCartReturnPropsDict.Add(1, propertyDict)
            propertyDict = New ExpDictionary
            propertyDict("propName") = "CartShippingAddress"
            propertyDict("propValue") = CartShippingAddress
            CSRCartReturnPropsDict.Add(2, propertyDict)
            addControl2PlaceHolder(thePage, phCSRCartReturn, VRoot & "/controls/USCSR/CSRCartReturn.ascx", CSRCartReturnPropsDict)
        End Sub

        Public Sub confirmedLoadPlaceHolder(ByRef thePage As Page, ByRef phCSRConfirmedOrderCompleted As PlaceHolder, ByVal orderDict As ExpDictionary, ByVal bMailSend As Boolean)
            Dim CSRConfirmedOrderCompletedPropsDict As New ExpDictionary
            Dim propertyDict As New ExpDictionary
            propertyDict("propName") = "Order"
            propertyDict("propValue") = orderDict
            CSRConfirmedOrderCompletedPropsDict.Add(1, propertyDict)
            propertyDict = New ExpDictionary
            propertyDict("propName") = "bMailSend"
            propertyDict("propValue") = bMailSend
            CSRConfirmedOrderCompletedPropsDict.Add(2, propertyDict)
            addControl2PlaceHolder(thePage, phCSRConfirmedOrderCompleted, VRoot & "/controls/USCSR/CSRConfirmedOrderCompleted.ascx", CSRConfirmedOrderCompletedPropsDict)
        End Sub


    End Class

End Namespace
'AM2011031801 - ENTERPRISE CSR STAND ALONE - End

