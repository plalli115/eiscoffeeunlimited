﻿Imports Microsoft.VisualBasic
Imports System.IO

Namespace ExpandIT

    ''' <summary>
    ''' This class is responsible for reading files within the Internet Shop application folder
    ''' </summary>
    ''' <remarks></remarks>
    Public Class FileReader

        Public Shared Function readFile(ByVal path As String) As String
            Dim retStr As String = Nothing
            Try

                Using sr As New StreamReader(File.OpenRead(HttpContext.Current.Server.MapPath(path)))
                    retStr = sr.ReadToEnd
                End Using

            Catch ex As Exception
                Return Nothing
            End Try
            Return retStr
        End Function

        ''' <summary>
        ''' Reads a file and returns a string in the requested encoding
        ''' </summary>
        ''' <param name="path">Relative path to the file</param>
        ''' <param name="reqEncoding">The requested encoding of the returned string</param>
        ''' <returns>A string representation of a file in the defined encoding</returns>
        ''' <remarks></remarks>
        Public Shared Function readFile(ByVal path As String, ByVal reqEncoding As Encoding) As String

            Dim b() As Byte = Nothing
            Dim enc As Encoding = GetFileEncoding(HttpContext.Current.Server.MapPath(path))

            Try

                Using fs As New FileStream(HttpContext.Current.Server.MapPath(path), FileMode.Open, FileAccess.Read)

                    b = New Byte(fs.Length) {}
                    Dim bytesToRead As Integer = b.Length - 1
                    Dim readFrom As Integer = 0
                    Dim bytesRead As Integer = 0

                    While bytesToRead > 0
                        bytesRead = fs.Read(b, readFrom, bytesToRead)
                        bytesToRead = bytesToRead - bytesRead
                        readFrom = readFrom + bytesRead
                    End While

                End Using

                b = Encoding.Convert(enc, reqEncoding, b)

                Return reqEncoding.GetString(b)

            Catch ex As Exception
                Return Nothing
            End Try

        End Function

        ''' <summary>
        ''' Returns the Encoding of a text file.
        ''' </summary>
        ''' <param name="FileName"></param>
        ''' <returns></returns>
        ''' <remarks>Return Encoding.Default if no Unicode BOM (byte order mark) is found.</remarks>
        Public Shared Function GetFileEncoding(ByVal FileName As String) As Encoding

            Dim Result As Encoding = Nothing
            Dim FI As FileInfo = New FileInfo(FileName)
            Dim FS As FileStream = Nothing

            Try
                FS = FI.OpenRead()
                Dim UnicodeEncodings As Encoding() = {Encoding.BigEndianUnicode, Encoding.Unicode, Encoding.UTF8}
                For i As Integer = 0 To Result Is Nothing And UnicodeEncodings.Length - 1
                    FS.Position = 0
                    Dim Preamble As Byte() = UnicodeEncodings(i).GetPreamble()
                    Dim PreamblesAreEqual As Boolean = True
                    For j As Integer = 0 To PreamblesAreEqual And Preamble.Length - 1
                        PreamblesAreEqual = Preamble(j) = FS.ReadByte()
                    Next
                    If PreamblesAreEqual Then
                        Result = UnicodeEncodings(i)
                    End If
                Next
            Catch Ex As System.IO.IOException

            Finally
                If FS IsNot Nothing Then
                    FS.Close()
                End If
                If Result Is Nothing Then
                    Result = Encoding.Default
                End If
            End Try

            Return Result

        End Function

    End Class

End Namespace
