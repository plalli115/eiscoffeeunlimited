'AM2010050401 - PICTURES CROPPER - Start
Imports System.Data.OleDb
Imports Radactive.WebControls.ILoad
Imports Radactive.WebControls.ILoad.Configuration
Imports ExpandIT.ExpandITLib

Public Class CustomStorageManager
    Implements Radactive.WebControls.ILoad.Core.ICustomStorageManager
    '5
    ' Get the folder path for an i-load file
    Public Shared Function GetFolderPath(ByVal configurationInternalCode As String, ByVal resizeInternalCode As String, ByVal relative As Boolean) As String
        ' Get the folder path for a certain configurationInternalCode
        Dim folderPath As String = ""
        Dim SQL As String = ""
        Dim pictureType As String = ""

        Select Case resizeInternalCode
            Case "__Selected"
                pictureType = "DestinationPath"
            Case "__Source"
                pictureType = "ProcessedPath"
            Case Else
                pictureType = "ProcessingPath"
        End Select

        SQL = "SELECT " & pictureType & " FROM ImageProcessor WHERE PictureTypeGuid='" & configurationInternalCode & _
                "' AND Crop=1"

        folderPath = CStrEx(getSingleValueDB(SQL))

        Return folderPath
    End Function

    Public Shared Function GetFolderPath(ByVal configurationInternalCode As String, ByVal resizeInternalCode As String) As String
        Return GetFolderPath(configurationInternalCode, resizeInternalCode, False)
    End Function

    ' Get the folder path for the xml index file
    Public Shared Function GetIndexFolderPath(ByVal configurationInternalCode As String, ByVal relative As Boolean) As String
        Return GetFolderPath(configurationInternalCode, "__Index", relative)
    End Function

    Public Shared Function GetIndexFolderPath(ByVal configurationInternalCode As String) As String
        Return GetIndexFolderPath(configurationInternalCode, False)
    End Function
    '4
    ' Get the file path for an i-load file
    Public Shared Function GetFilePath(ByVal configurationInternalCode As String, ByVal imageId As String, ByVal resizeInternalCode As String, ByVal fileExtension As String, ByVal relative As Boolean) As String
        ' Get the file name
        Dim fileName As String = System.IO.Path.GetFileNameWithoutExtension(imageId) + "." + fileExtension

        ' Get the folder path for a certain configurationInternalCode / resizeInternalCode
        Dim folderPath As String = GetFolderPath(configurationInternalCode, resizeInternalCode, relative)

        If (relative) Then
            Return folderPath + "/" + fileName
        Else
            Return System.IO.Path.Combine(folderPath, fileName)
        End If
    End Function

    Public Shared Function GetFilePath(ByVal configurationInternalCode As String, ByVal imageId As String, ByVal resizeInternalCode As String, ByVal fileExtension As String) As String
        Return GetFilePath(configurationInternalCode, imageId, resizeInternalCode, fileExtension, False)
    End Function
    '3
    ' Get the file path for the xml index file
    Public Shared Function GetIndexFilePath(ByVal configurationInternalCode As String, ByVal imageId As String, ByVal relative As Boolean) As String
        Return GetFilePath(configurationInternalCode, imageId, "__Index", "xml", relative)
    End Function
    '2
    Public Shared Function GetIndexFilePath(ByVal configurationInternalCode As String, ByVal imageId As String) As String
        Return GetIndexFilePath(configurationInternalCode, imageId, False)
    End Function

    '1
    ' Get a new image id without file name collisions
    Public Shared Function GetNewImageId(ByVal configurationInternalCode As String, ByVal fileName As String) As String
        ' Get the image id
        Dim imageId As String = System.IO.Path.GetFileNameWithoutExtension(fileName)

        ' Get the file path
        Dim filePath As String = GetIndexFilePath(configurationInternalCode, imageId)

        If (Not System.IO.File.Exists(filePath)) Then
            Return imageId
        End If

        Dim i As Integer = 2
        While (True)
            Dim imageId2 As String = imageId + "_" + i.ToString()

            filePath = GetIndexFilePath(configurationInternalCode, imageId2)

            If (Not System.IO.File.Exists(filePath)) Then
                Return imageId2
            End If

            i = i + 1
        End While
        Return Nothing
    End Function

#Region "ICustomStorageManager Members"

    Public Function IndexXmlExists(ByVal configurationInternalCode As String, ByVal imageId As String, ByVal customStorageManagerExtraData As Object) As Boolean Implements Core.ICustomStorageManager.IndexXmlExists
        Dim filePath As String = GetIndexFilePath(configurationInternalCode, imageId)
        Return System.IO.File.Exists(filePath)
    End Function

    Public Function ImageExists(ByVal configurationInternalCode As String, ByVal imageId As String, ByVal resizeInternalCode As String, ByVal fileExtension As String, ByVal customStorageManagerExtraData As Object) As Boolean Implements Core.ICustomStorageManager.ImageExists
        Dim filePath As String = GetFilePath(configurationInternalCode, imageId, resizeInternalCode, fileExtension)
        Return System.IO.File.Exists(filePath)
    End Function

    Public Function LoadIndexXml(ByVal configurationInternalCode As String, ByVal imageId As String, ByVal customStorageManagerExtraData As Object) As String Implements Core.ICustomStorageManager.LoadIndexXml
        Dim filePath As String = GetIndexFilePath(configurationInternalCode, imageId)
        If (System.IO.File.Exists(filePath)) Then
            Return System.IO.File.ReadAllText(filePath, System.Text.Encoding.UTF8)
        Else
            Throw New System.IO.IOException("Unable to find file '" + filePath + "'.")
        End If
    End Function

    Public Function LoadImage(ByVal configurationInternalCode As String, ByVal imageId As String, ByVal resizeInternalCode As String, ByVal fileExtension As String, ByVal customStorageManagerExtraData As Object) As Byte() Implements Core.ICustomStorageManager.LoadImage
        Dim filePath As String = GetFilePath(configurationInternalCode, imageId, resizeInternalCode, fileExtension)
        If (System.IO.File.Exists(filePath)) Then
            Return System.IO.File.ReadAllBytes(filePath)
        Else
            Throw New System.IO.IOException("Unable to find file '" + filePath + "'.")
        End If
    End Function

    Public Sub SaveIndexXml(ByVal configurationInternalCode As String, ByVal imageId As String, ByVal indexXml As String, ByVal customStorageManagerExtraData As Object) Implements Core.ICustomStorageManager.SaveIndexXml
        Dim filePath As String = GetIndexFilePath(configurationInternalCode, imageId)
        If (System.IO.File.Exists(filePath)) Then
            System.IO.File.Delete(filePath)
        End If
        System.IO.File.WriteAllText(filePath, indexXml, System.Text.Encoding.UTF8)
    End Sub

    Public Sub SaveImage(ByVal configurationInternalCode As String, ByVal imageId As String, ByVal resizeInternalCode As String, ByVal fileExtension As String, ByVal image() As Byte, ByVal imageSize As System.Drawing.Size, ByVal customStorageManagerExtraData As Object) Implements Core.ICustomStorageManager.SaveImage
        Dim filePath As String = GetFilePath(configurationInternalCode, imageId, resizeInternalCode, fileExtension)
        If (System.IO.File.Exists(filePath)) Then
            System.IO.File.Delete(filePath)
        End If
        System.IO.File.WriteAllBytes(filePath, image)
    End Sub

    Public Sub DeleteIndexXml(ByVal configurationInternalCode As String, ByVal imageId As String, ByVal customStorageManagerExtraData As Object) Implements Core.ICustomStorageManager.DeleteIndexXml
        Dim filePath As String = GetIndexFilePath(configurationInternalCode, imageId)
        If (System.IO.File.Exists(filePath)) Then
            System.IO.File.Delete(filePath)
        End If
    End Sub

    Public Sub DeleteImage(ByVal configurationInternalCode As String, ByVal imageId As String, ByVal resizeInternalCode As String, ByVal fileExtension As String, ByVal customStorageManagerExtraData As Object) Implements Core.ICustomStorageManager.DeleteImage
        Dim filePath As String = GetFilePath(configurationInternalCode, imageId, resizeInternalCode, fileExtension)
        If (System.IO.File.Exists(filePath)) Then
            System.IO.File.Delete(filePath)
        End If
    End Sub

#End Region

End Class
'AM2010050401 - PICTURES CROPPER - End
