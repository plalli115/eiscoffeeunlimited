Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Collections.Generic
Imports System.Data
Imports System.Net
Imports ExpandIT.GlobalsClass

Namespace ExpandIT

    Public Class USKitting

        Dim pageobj As ExpandIT.Page = CType(System.Web.HttpContext.Current.Handler, ExpandIT.Page)

        Public Sub IntLoadKittingInfo(ByVal products As ExpDictionary, ByVal general As Object)
            Dim i As Integer
            Dim sql, from, where, productguid, KitBOMNo As String
            Dim item As Object
            Dim kittinginfo As New ExpDictionary
            Dim properties As ExpDictionary
            Dim langguid As String = CStrEx(general("LanguageGuid"))
            Dim deflangguid As String = general("DefaultLanguageGuid")
            Dim extlangguid As String = general("ExternalLanguageGuid")
            Dim prdArray() As String
            Dim productInKit As String
            Dim line As ExpDictionary
            Dim criteria As String

            If CBoolEx(AppSettings("USE_PRODUCTVARIANTTRANSLATION")) Then
                criteria = "VariantCode IS NULL OR VariantCode=''"
            Else
                criteria = "1=1"
            End If
            ' Make a product list
            For Each item In products.Values
                productguid = CStrEx(item("ProductGuid"))
                If productguid <> "" Then
                    KitBOMNo = CStrEx(item("KitBOMNo"))
                    If KitBOMNo <> "" Then
                        kittinginfo = New ExpDictionary
                        from = " FROM ProductionBOMLine PBL LEFT JOIN ProductTable PD ON PBL.ProductGuid=PD.ProductGuid "
                        where = " WHERE PBL.ProductionBOMNo=" & SafeString(KitBOMNo)
                        If CBoolEx(AppSettings("USE_PRODUCTTRANSLATION")) Then
                            sql = "SELECT PBL.LineGuid,PBL.ProductGuid,PBL.UOM,PBL.Quantity,PBL.VariantDescription," & _
                                "PBL.UOMDescription, PT.ProductName AS [ProductNameTranslation],PD.ProductName "
                            sql &= from & " LEFT JOIN (SELECT * FROM ProductTranslation WHERE (" & criteria & ") " & _
                                "AND " & HttpContext.Current.Application("EXTERNAL_LANGUAGEGUID_SQL") & "=" & _
                                SafeString(extlangguid) & ") AS PT ON PBL.ProductGuid=PT.ProductGuid " & where
                        Else
                            sql = "SELECT LineGuid,ProductGuid,UOM,Quantity,VariantDescription,UOMDescription," & _
                                "PD.ProductName "
                            sql &= from & where
                        End If

                        kittinginfo = SQL2Dicts(sql, "LineGuid")
                        'Load Properties needed
                        prdArray = New String() {}
                        i = 0
                        For Each line In kittinginfo.Values
                            productInKit = CStrEx(line("ProductGuid"))
                            If CBoolEx(AppSettings("USE_PRODUCTTRANSLATION")) Then
                                If CStrEx(line("ProductNameTranslation")) <> "" Then line("ProductName") = line("ProductNameTranslation")
                                line.Remove("ProductNameTranslation")
                            End If
                            If productInKit <> "" Then
                                ReDim Preserve prdArray(i)
                                prdArray(i) = productInKit
                                i = i + 1
                            End If
                        Next
                        properties = New ExpDictionary
                        properties = EISClass.IntLoadProperties("PRD", prdArray, New String() {"PICTURE2"}, langguid, deflangguid)
                        For Each line In kittinginfo.Values
                            If properties.Exists(line("ProductGuid")) Then CopyDictValues(line, properties(line("ProductGuid")))
                        Next
                        item("KittingInfo") = kittinginfo
                    End If
                End If
            Next

        End Sub

    End Class

End Namespace
