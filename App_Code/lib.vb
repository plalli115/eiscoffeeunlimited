﻿Imports System.Configuration.ConfigurationManager
Imports Microsoft.VisualBasic
Imports System.IO
Imports ExpandIT.GlobalsClass
Imports ExpandIT.B2BBaseClass
Imports ExpandIT.BuslogicBaseClass
Imports ExpandIT.DatabasePathClass
Imports ExpandIT.CurrencyBaseClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT.Debug
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Generic
Imports System.Net.Mail.SmtpClient
Imports System.IO.Compression
Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Xml
Imports ExpandIT.SectionSettingsManager
Imports ExpandIT31.ExpanditFramework.Util
Imports ExpandIT31.ExpanditFramework.Infrastructure
Imports System.Net.Mail
Imports ExpandIT31.ExpanditFramework.BLLBase
Imports ExpandIT
Imports ExpandIT.EISClass
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Drawing.Imaging


Namespace ExpandIT

    Public Class EISClass

        Private globals As GlobalsClass
        'Changed here to make use of both currency classes
        Private currency As CurrencyBaseClass
        Private cart As CartClass
        Private buslogic As BuslogicBaseClass
        Private b2b As B2BBaseClass



        '*****promotions*****-start
        Private promotions As promotions
        '*****promotions*****-end
        '*****CUSTOMER SPECIFIC CATALOGS*****-START
        Private obj As csc
        '*****CUSTOMER SPECIFIC CATALOGS*****-END
        '*****shipping*****-start
        Private shipping As New liveShipping()
        '*****shipping*****-end
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
        Private csr As USCSR
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - End

        Public Sub New()

        End Sub

        Public Sub New(ByVal _globals As GlobalsClass)
            globals = _globals
            currency = globals.currency
            cart = globals.cart
            buslogic = globals.buslogic
            b2b = globals.b2b
            '*****CUSTOMER SPECIFIC CATALOGS*****-START
            obj = New csc(globals)
            '*****CUSTOMER SPECIFIC CATALOGS*****-END
            '*****promotions*****-start
            promotions = New promotions(_globals)
            '*****promotions*****-end
            'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
            csr = New USCSR(_globals)
            'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
        End Sub

        Public Shared Function HiLite(ByVal s As String, ByVal searchstring As String, ByVal bIsHTML As Boolean) As String
            Dim objRE As Regex
            Dim objMatch As Match
            Dim objMatches As MatchCollection
            Dim i As Integer
            Dim retv As String
            Dim ss As String
            Dim firstpart, lastpart, donepart, matchpart As String
            Dim lastindex As Integer
            Dim starthilite As String
            Dim endhilite As String

            starthilite = "<span class=""SearchHiLite"">"
            endhilite = "</span>"
            retv = IIf(bIsHTML, HttpContext.Current.Server.HtmlDecode(s), s)

            ' Remove double wildcards
            While InStr(1, searchstring, "**")
                searchstring = Replace(searchstring, "**", "*")
            End While

            ' Transform search string to pattern
            If Len(searchstring) > 0 Then
                ss = searchstring
                ss = Replace(ss, "\", "\\")
                ss = Replace(ss, ".", "\.")
                ss = Replace(ss, "?", ".")
                ss = Replace(ss, "*", "(.*)")
                ss = Replace(ss, "[", "(\[)")
                objRE = New Regex(ss, RegexOptions.IgnoreCase)
                If retv <> "" Then
                    lastindex = Len(retv) + 1
                    objMatches = objRE.Matches(retv)
                    If objMatches.Count > 0 Then
                        For i = objMatches.Count To 1 Step -1
                            objMatch = objMatches.Item(i - 1)
                            firstpart = Left(retv, objMatch.Index)
                            matchpart = Mid(retv, objMatch.Index + 1, objMatch.Length)
                            lastpart = Mid(retv, objMatch.Index + objMatch.Length + 1, lastindex - (objMatch.Index + objMatch.Length + 1))
                            donepart = Mid(retv, lastindex)
                            If Not bIsHTML Then
                                lastpart = HTMLEncode(lastpart)
                                matchpart = HTMLEncode(matchpart)
                                If i = 1 Then firstpart = HTMLEncode(firstpart)
                            End If

                            retv = firstpart & starthilite & matchpart & endhilite & lastpart & donepart
                            lastindex = objMatch.Index + 1
                        Next
                    Else
                        retv = IIf(bIsHTML, retv, HTMLEncode(retv))
                    End If
                End If

            End If

            Return retv
        End Function

#Region "lib_datamanip"

        ''' <summary>
        ''' Encrypts a string.
        ''' </summary>
        ''' <param name="UnencryptedText">This parameter specifies the string that you want to encrypt.</param>
        ''' <returns>An encrypted string</returns>
        ''' <remarks>
        ''' The encryption is actually a scrambling of the string. Scrambling makes a password unreadable. 
        ''' Encryption can use either XOr or MD5.
        ''' </remarks>
        Public Function Encrypt(ByVal UnencryptedText As String) As String
            Dim retv As String
            If UCase(AppSettings("ENCRYPTION_TYPE")) = "MD5" Then
                retv = ExpandIT.Crypto.coreMD5(MD5_SECRET & UnencryptedText)
            Else
                retv = EncryptXOrWay(UnencryptedText)
            End If
            Return retv
        End Function

        ''' <summary>
        ''' Loads a collection of cookies into a dictionary. Returns Nothing if there is no value in the cookie.
        ''' </summary>
        ''' <param name="CookieName">Name of the Cookie to load values from. Ex. Store, User, MiniCart</param>
        ''' <returns>Returns Nothing if no values was found.</returns>
        ''' <remarks></remarks>
        Public Shared Function Cookie2Dict(ByVal CookieName As Object) As ExpDictionary
            Dim retval As ExpDictionary
            Dim key As Object

            If HttpContext.Current.Request.Cookies(CookieName).Count > 0 Then
                retval = New ExpDictionary()
                For Each key In HttpContext.Current.Request.Cookies(CookieName)
                    retval.Add(key, HttpContext.Current.Request.Cookies(CookieName)(key))
                Next
                Return retval
            End If
            Return Nothing
        End Function

        ''' <summary>
        ''' This function will load the minicart cookie from the machine.
        ''' </summary>
        ''' <param name="CookieName">Name of the Cookie to load values from. Ex. Store, User, MiniCart</param>
        ''' <returns>Returns Nothing if no values was found.</returns>
        ''' <remarks></remarks>
        Public Shared Function GetMiniCartFromCookie(ByVal CookieName As Object) As ExpDictionary
            Dim retval As ExpDictionary = Nothing

            If Not HttpContext.Current.Request.Cookies(CookieName) Is Nothing Then

                If Not HttpContext.Current.Request.Cookies(CookieName) Is Nothing Then
                    retval = New ExpDictionary()
                    If HttpContext.Current.Request.Cookies(CookieName)("MiniCartVersionNo") = "EIS30" Then
                        If HttpContext.Current.Request.Cookies(CookieName)("MarshallMiniCartInfo") <> "" Then
                            retval = UnmarshallDictionary(HttpContext.Current.Request.Cookies(CookieName)("MarshallMiniCartInfo"))
                        Else
                            retval = Nothing
                        End If
                    Else
                        retval = Nothing
                    End If
                End If
            End If
            Return retval
        End Function

#End Region

#Region "lib_user"

        ''' <summary>
        ''' Returns the current userstatus
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function determineUserStatus() As String
            ' Check User Status
            Dim result As String = ""
            If Not CBoolEx(globals.User("Anonymous")) Then
                If CBoolEx(globals.User("B2B")) Then
                    result = "B2B"
                Else
                    result = "B2C"
                End If
            Else
                result = "Anonymous"
            End If
            Return result
        End Function

        ''' <summary>
        ''' Updates the users selected language in the cookie and optionally the database.
        ''' </summary>
        ''' <param name="LanguageGuid">The language to set.</param>
        ''' <param name="UpdateDatabase">A boolean value specifying if the users record in the database should be updated.</param>
        ''' <remarks>
        ''' The language should be stored in the database if you want to remember it the next time a user is logging in.
        ''' Dependencies: SetCookieExpireDate
        ''' See Also: SetUserID        
        ''' </remarks>
        Public Sub SetUserLanguage(ByVal LanguageGuid As String, ByVal UpdateDatabase As Boolean)
            Dim sql As String

            ' Initialize the cookie
            HttpContext.Current.Response.Cookies("user")("LanguageGuid") = LanguageGuid
            HttpContext.Current.Response.Cookies("user").Path = HttpContext.Current.Server.UrlPathEncode(VRoot) & "/"

            If UpdateDatabase Then
                ' Write the selection to the database if a user record exist
                sql = "UPDATE UserTable SET LanguageGuid=" & SafeString(LanguageGuid) & " WHERE UserGuid=" & SafeString(HttpContext.Current.Session("UserGuid"))
                excecuteNonQueryDb(sql)
            End If
        End Sub

        ''' <summary>
        ''' Makes sure that the current user has a userguid set in the cookie.
        ''' </summary>
        ''' <remarks>
        ''' This function initializes the users language and userguid.
        ''' Dependencies: SetCookieExpireDate    
        ''' See Also: SetUserLanguage            
        ''' </remarks>
        Public Sub SetUserID()
            Dim i As Integer
            Dim sBrowserCheck, bRunCookieTest, aBrowsers, aRobots As Object

            ' Set User id
            If Not HttpContext.Current.Request.Cookies("store") Is Nothing Then
                HttpContext.Current.Session("UserGuid") = HttpContext.Current.Request.Cookies("store").Item("UserGuid")
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
                If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
                    csr.setCSR2Session()
                End If
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
            Else
                HttpContext.Current.Session("UserGuid") = ""
            End If

            If HttpContext.Current.Session("LanguageGuid") Is Nothing And Not HttpContext.Current.Request.Cookies("user") Is Nothing Then HttpContext.Current.Session("LanguageGuid") = HttpContext.Current.Request.Cookies("user").Item("LanguageGuid")
            If HttpContext.Current.Session("LanguageGuid") Is Nothing Then HttpContext.Current.Session("LanguageGuid") = AppSettings("LANGUAGE_DEFAULT")
            If AppSettings("USE_NTLM") Then
                ' Use NT security

                ' Check that user is authenticated
                If HttpContext.Current.Request.ServerVariables("AUTH_TYPE") = "" Then
                    HttpContext.Current.Response.Redirect(AppSettings("URL_NOACCESS"))
                End If

                ' Check that the server security model matches
                If HttpContext.Current.Request.ServerVariables("AUTH_TYPE") <> "NTLM" And HttpContext.Current.Request.ServerVariables("AUTH_TYPE") <> "Negotiate" Then
                    globals.User = New ExpDictionary
                    globals.User("LanguageGuid") = AppSettings("LANGUAGE_DEFAULT")
                    HttpContext.Current.Response.Write(Resources.Language.MESSAGE_SITE_IS_USING_NTLM_SECURITY & " [" & _
                        HttpContext.Current.Request.ServerVariables("AUTH_TYPE") & "]")
                    HttpContext.Current.Response.End()
                End If
            End If

            If HttpContext.Current.Session("UserGuid") = "" Then
                ' Generate new User id
                HttpContext.Current.Session("UserGuid") = GenGuid()

                ' Initialize the cookie
                HttpContext.Current.Response.Cookies("store")("UserGuid") = HttpContext.Current.Session("UserGuid")
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
                If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
                    csr.setCSR2Cookie()
                End If
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
                SetCookieExpireDate()
            End If

            '
            ' Cookie test of the website.
            ' This check will decide if cookies can be written on the clients computer. The test consist of both
            ' a session cookies and a persistent cookie. If the session cookie fails then the shop is blocked
            ' for the user. If persistent cookie fails then no Expire date is written for the cookie.
            '

            Dim hascookie As Boolean = False
            If Not HttpContext.Current.Request.Cookies("ExpTest") Is Nothing Then
                hascookie = HttpContext.Current.Request.Cookies("ExpTest").Item("ExpTestCookie") <> ""
            End If

            If Not hascookie Then
                '
                ' Most browsers have Mozilla as first part of HTTP_USER_AGENT. Therefore we only run the cookietest for those.
                ' Other "real" browsers include "opera" or "microsoft internet explore" in USER_AGENT
                ' Web spiders / robots will not run the cookietest.
                '
                sBrowserCheck = HttpContext.Current.Request.ServerVariables("HTTP_USER_AGENT")
                aBrowsers = New String() {"Mozilla", "Microsoft Internet Explorer", "Opera", "WebTV", "JDK", "HotJava"}
                aRobots = New String() {"Slurp", "Googlebot", "Ask Jeeves", "Ask+Jeeves", "AvantGo", "grub-client", "MSIECrawler", "ZyBorg"}
                bRunCookieTest = False

                For i = 0 To UBound(aBrowsers) - 1
                    If InStr(1, sBrowserCheck, aBrowsers(i), 1) = 1 Then
                        bRunCookieTest = True
                        Exit For
                    End If
                Next

                ' Some robots are identified by Mozilla - These should not run the cookie test.
                If aBrowsers(i) = "Mozilla" Then
                    For i = 0 To UBound(aRobots) - 1
                        If InStr(1, sBrowserCheck, aRobots(i), 1) > 0 Then
                            bRunCookieTest = False
                            Exit For
                        End If
                    Next
                End If

                If HttpContext.Current.Request.Headers("Sec-Fetch-Mode") = "cors" Then
                    bRunCookieTest = False
                End If

                If bRunCookieTest Then
                    HttpContext.Current.Response.Cookies("ExpTest")("ExpTestCookie") = "ThisCookieIsForTesting"
                    HttpContext.Current.Response.Cookies("ExpTest").Path = HttpContext.Current.Server.UrlPathEncode(VRoot) & "/"
                    HttpContext.Current.Response.Cookies("ExpTestPer")("ExpTestCookiePer") = "ThisCookieIsForTestingPersistent"
                    HttpContext.Current.Response.Cookies("ExpTestPer").Expires = Now.Add(TimeSpan.FromDays(1))
                    HttpContext.Current.Response.Cookies("ExpTestPer").Path = HttpContext.Current.Server.UrlPathEncode(VRoot) & "/"
                    HttpContext.Current.Response.Redirect(VRoot & "/include/cookietest.aspx?returl=" & HttpContext.Current.Server.UrlEncode(CStrEx(HttpContext.Current.Request.ServerVariables("SCRIPT_NAME"))) & "&ExpQueryString=" & HttpContext.Current.Server.UrlEncode(CStrEx(HttpContext.Current.Request.QueryString())))
                End If
            End If

            ' Set initial user information
            globals.User = New ExpDictionary
            globals.User("UserGuid") = HttpContext.Current.Session("UserGuid")
            globals.User("LanguageGuid") = HttpContext.Current.Session("LanguageGuid").ToString
            globals.User("B2B") = False
            globals.User("B2C") = False
            globals.User("Anonymous") = True
        End Sub

        ''' <summary>
        ''' This function loads and sets the User dictionary. The function takes care of all logic
        ''' involved in logging in to the shop. This is including copying of
        ''' data between the CustomerTable, UserTableB2B and UserTable.
        ''' </summary>
        ''' <param name="UserGuid">The user guid of the user.</param>
        ''' <returns>The User dictionary</returns>
        ''' <remarks>
        ''' After a call to LoadUser the global variable User is set. The function also returns the User dictionary.
        ''' Use User("Anonymous"), User("B2B") and User("B2C") to test for the user type.
        ''' If you must load more than one user then you should use the CopyDictValues function. 
        ''' See Also: CopyDictValues.
        '''</remarks>
        Public Function LoadUser(ByVal UserGuid As String) As ExpDictionary
            Dim UserExists As Boolean
            Dim B2BUserExists As Boolean
            Dim CustomerExists As Boolean
            Dim dictUser As ExpDictionary = Nothing
            Dim dictB2BUser As ExpDictionary = Nothing
            Dim dtCustomer As DataTable
            Dim dictCustomer As ExpDictionary = Nothing
            Dim sql, sqlUser As String
            Dim dtUser As DataTable = Nothing

            ' Used to cache the user dictionary. All pages doesn't have to care about load times
            ' as the user last loaded will be returned. If it is required to load two different users
            ' Then one should set the bUserLoaded variable to False.

            If Not globals.User Is Nothing Then
                If globals.User("UserGuid") <> UserGuid Then globals.bUserLoaded = False
            Else
                globals.bUserLoaded = False
            End If

            If globals.bUserLoaded Then
                LoadUser = globals.User
            Else
                ' Check for user in the standard user table
                If Not AppSettings("USE_NTLM") Then
                    sqlUser = "SELECT * FROM UserTable WHERE UserGuid=" & SafeString(UserGuid)
                Else
                    sqlUser = "SELECT * FROM UserTable WHERE LOWER(UserLogin)=" & SafeString(LCase(HttpContext.Current.Request.ServerVariables("AUTH_USER")))
                End If

                dtUser = SQL2DataTable(sqlUser)

                If dtUser.Rows.Count > 0 Then
                    dictUser = SetDictionary(dtUser.Rows(0))
                    UserExists = True
                Else
                    UserExists = False
                End If

                ' Check for user in the B2B user table
                B2BUserExists = False
                If B2BEnabled Then
                    If Not AppSettings("USE_NTLM") Then
                        sql = "SELECT * FROM UserTableB2B WHERE EnableLogin<>0 AND UserGuid=" & SafeString(UserGuid)
                    Else
                        sql = "SELECT * FROM UserTableB2B WHERE EnableLogin<>0 AND LOWER(UserLogin)=" & SafeString(LCase(HttpContext.Current.Request.ServerVariables("AUTH_USER")))
                    End If

                    Dim dtB2BUser As DataTable = SQL2DataTable(sql)
                    If dtB2BUser.Rows.Count > 0 Then
                        dictB2BUser = SetDictionary(dtB2BUser.Rows(0))
                        B2BUserExists = True
                    End If
                End If

                ' Look for customer information in backend customer table
                CustomerExists = False
                If B2BUserExists Then
                    sql = "SELECT TOP 1 * FROM CustomerTable WHERE EnableLogin<>0 AND ltrim(rtrim(CustomerGuid))=" & SafeString(Trim(dictB2BUser("CustomerGuid")))
                    dtCustomer = SQL2DataTable(sql)
                    CustomerExists = dtCustomer.Rows.Count > 0
                    If CustomerExists Then
                        dictCustomer = SetDictionary(dtCustomer.Rows(0))
                        b2b.DecodeAddress(dictCustomer)
                    End If
                    B2BUserExists = CustomerExists
                End If

                ' Make sure the user is updated if information has changed
                If Not B2BUserExists Then
                    If Not UserExists Then

                        ' *********************
                        ' Setting User as Anonymous
                        ' *********************
                        UserExists = False
                        dictUser = New ExpDictionary
                        dictUser("UserGuid") = UserGuid
                        dictUser("SecondaryCurrencyGuid") = DBNull.Value
                        dictUser("Anonymous") = True
                        dictUser("B2C") = False
                        dictUser("B2B") = False
                        Try
                            dictUser("CurrencyGuid") = HttpContext.Current.Request.Cookies("user").Item("CurrencyGuid")
                        Catch ex As Exception

                        End Try
                    Else

                        ' ***************
                        ' Setting User as B2C
                        ' ***************
                        dictUser("Anonymous") = False
                        dictUser("B2C") = True
                        dictUser("B2B") = False

                        ' If we have a user with IsB2B<>0 and no corresponding B2B user
                        ' then the B2B user has been deleted or disabled.
                        ' We must make the current user anonymous be resetting the
                        ' cookie and redirecting back to this page.
                        If CStrEx(dictUser("IsB2B")) = "1" Then
                            HttpContext.Current.Response.Cookies("store")("UserGuid") = ""
                            'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
                            If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
                                csr.setCSR2Cookie()
                            End If
                            'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
                            HttpContext.Current.Response.Cookies("store").Path = HttpContext.Current.Server.UrlPathEncode(VRoot) & "/"
                            HttpContext.Current.Response.Redirect(VRoot & "/")
                        End If
                    End If
                Else
                    ' B2B entry exists
                    If Not UserExists Then

                        ' *************************
                        ' CREATING RECORD IN USERTALBE
                        ' *************************
                        UserExists = False

                        ' The UserGuid can exist if we are using NTLM
                        ' If this is the case it must be deleted
                        If AppSettings("USE_NTLM") Then
                            sql = "DELETE FROM UserTable WHERE UserGuid=" & SafeString(dictB2BUser("UserGuid"))
                            excecuteNonQueryDb(sql)
                        End If

                        ' Make empty userDict
                        dictUser = New ExpDictionary

                        ' Copy all values from the customer table to the new user
                        If CustomerExists Then
                            CopyDictValues(dictUser, dictCustomer)
                        End If

                        ' Copy all values from the B2B user to the new user
                        CopyDictValues(dictUser, dictB2BUser)

                        ' Make sure the B2B customer is activated if the confirmation method is used
                        dictUser("Status") = UserBase.UserStates.Confirmed

                        ' Insert the new user in the database
                        If Not dictUser.Exists("UserCreated") Then
                            dictUser.Add("UserCreated", DateTime.Now)
                        End If
                        SetDictUpdateEx(dictUser, "UserTable")

                    Else

                        ' ***************************
                        ' CHECKING FOR UPDATE REASON
                        ' ***************************
                        Dim UpdateUserTable As Boolean
                        Dim UserPasswordVersion, UserB2BPasswordVersion As String
                        Dim UserPassword, UserB2BPassword As String

                        ' Checking for changes
                        Dim dictResult As ExpDictionary

                        ' Storing Password information
                        '#080709. Changed to handle DBNull values 
                        UserPasswordVersion = DBNull2Nothing(dictUser("PasswordVersion"))
                        UserPassword = DBNull2Nothing(dictUser("UserPassword"))
                        UserB2BPasswordVersion = DBNull2Nothing(dictB2BUser("PasswordVersion"))
                        UserB2BPassword = DBNull2Nothing(dictB2BUser("UserPassword"))

                        dictResult = New ExpDictionary
                        CopyDictValues(dictResult, dictCustomer)
                        CopyDictValues(dictResult, dictB2BUser)

                        ' Check if an update of the user is needed
                        UpdateUserTable = Not CompareDict(dictUser, dictResult, New String() {"UserPassword", "PasswordVersion"})
                        If Not UpdateUserTable Then
                            ' Update anyway if the password version has changed
                            UpdateUserTable = (CStrEx(UserPasswordVersion) <> CStrEx(UserB2BPasswordVersion)) Or (CStrEx(dictUser("IsB2B")) <> "1")
                        End If
                        ' ********************
                        ' UPDATING USER TABLE
                        ' ********************
                        UserExists = True

                        ' Updating UserTable From CustomerTable
                        If UpdateUserTable Then
                            ' Updating UserTable from CustomerTable
                            If CustomerExists Then
                                If dictCustomer("CurrencyGuid") <> HttpContext.Current.Session("UserCurrencyGuid") Then
                                    dictCustomer("CurrencyGuid") = HttpContext.Current.Session("UserCurrencyGuid")
                                End If
                                CopyDictValues(dictUser, dictCustomer)
                            End If

                            ' Updating UserTable From UserTableB2B
                            CopyDictValues(dictUser, dictB2BUser)

                            If Not AppSettings("USE_NTLM") Then
                                ' Assigning Password information
                                If UserPasswordVersion <> UserB2BPasswordVersion Then
                                    dictUser("PasswordVersion") = UserB2BPasswordVersion
                                    dictUser("UserPassword") = UserB2BPassword
                                Else
                                    dictUser("PasswordVersion") = UserPasswordVersion
                                    dictUser("UserPassword") = UserPassword
                                End If
                            Else
                                ' Set UserGuid in cookie
                                UserGuid = CStrEx(dictB2BUser("UserGuid"))
                                HttpContext.Current.Response.Cookies("store")("UserGuid") = UserGuid
                                'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
                                If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
                                    csr.setCSR2Cookie()
                                End If
                                'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
                                SetCookieExpireDate()
                            End If
                            dictUser("IsB2B") = 1
                            dictUser("UserModified") = Date.Now
                            SetDictUpdateEx(dictUser, "UserTable", sqlUser)
                        End If
                    End If

                    dictUser("Anonymous") = False
                    dictUser("B2C") = False
                    dictUser("B2B") = True
                End If

                'AM2010071901 - ENTERPRISE CSC - Start
                If CBoolEx(AppSettings("EXPANDIT_US_USE_ENTERPRISE_CSC")) Then
                    Dim enterpriseCSCObj As New enterpriseCSC(globals)
                    Dim catalogNo As String = ""
                    catalogNo = enterpriseCSCObj.getCustomerCatalog(CStrEx(dictUser("CustomerGuid")))
                    If CStrEx(dictUser("CatalogNo")) <> catalogNo Then
                        dictUser("CatalogNo") = catalogNo
                        If Not dictUser("Anonymous") Then
                            SetDictUpdateEx(dictUser, "UserTable", sqlUser)
                        End If
                    End If
                Else
                    dictUser("CatalogNo") = CStrEx(getCSC())
                End If
                'AM2010071901 - ENTERPRISE CSC - End
                'WLB.01/09/2015 - COU-WEB-L01 - Begin
                '--------------------------------------------------------------
                ' Replicate the above logic, replacing the catalog number from
                ' the Customer Specific Catalog with the catalog number 
                ' fetched by customer group.
                '--------------------------------------------------------------
                If CBoolEx(AppSettings("EXPANDIT_US_USE_ENTERPRISE_CSC_GROUPS")) Then
                    Dim enterpriseCSCGroupObj As New enterpriseCSCGroup(globals)
                    Dim groupCatalogNo As String = ""
                    groupCatalogNo = enterpriseCSCGroupObj.getCustomerCatalog(CStrEx(dictUser("InetCustomerGroupGuid")))
                    If CStrEx(dictUser("CatalogNo")) <> groupCatalogNo Then
                        dictUser("CatalogNo") = groupCatalogNo
                        If Not dictUser("Anonymous") Then
                            SetDictUpdateEx(dictUser, "UserTable", sqlUser)
                        End If
                    End If
                Else
                    dictUser("CatalogNo") = CStrEx(getCSC())
                End If
                'WLB.01/09/2015 - COU-WEB-L01 - End
                globals.User = dictUser

                ' Ensure that essential values are set
                If NaV(globals.User("CountryGuid")) Then globals.User("CountryGuid") = AppSettings("DEFAULT_COUNTRYGUID")
                ' Set an initial currency if none is present
                If NaV(globals.User("CurrencyGuid")) Then globals.User("CurrencyGuid") = AppSettings("MULTICURRENCY_SITE_CURRENCY")
                ' #<<CHANGED Hotfix 2>> Apply logic to ensure the correct currency is set.    
                ExpandIT.CurrencyControlLogicFactory.CreateInstance(globals.User)

                If NaV(globals.User("LanguageGuid")) Then globals.User("LanguageGuid") = HttpContext.Current.Session("LanguageGuid").ToString
                If globals.User("LanguageGuid") = "" Then globals.User("LanguageGuid") = AppSettings("LANGUAGE_DEFAULT")
                'If HttpContext.Current.Session("ExternalLanguageGuid") Is Nothing Then HttpContext.Current.Session("ExternalLanguageGuid") = GetExternalFromIsoLanguageGuid(globals.User("LanguageGuid"))
                HttpContext.Current.Session("ExternalLanguageGuid") = GetExternalFromIsoLanguageGuid(globals.User("LanguageGuid"))

                ' Patch email address from Navision 3.5x
                globals.User("EmailAddress") = Replace(CStrEx(globals.User("EmailAddress")), "*", "@")

                HttpContext.Current.Session("LanguageGuid") = globals.User("LanguageGuid")
                HttpContext.Current.Response.Cookies("user")("LanguageGuid") = HttpContext.Current.Session("LanguageGuid").ToString
                HttpContext.Current.Response.Cookies("user").Path = HttpContext.Current.Server.UrlPathEncode(VRoot) & "/"
                If CBoolEx(globals.User("Anonymous")) Then
                    HttpContext.Current.Response.Cookies("user").Item("CurrencyGuid") = globals.User("CurrencyGuid")
                End If
                LoadUser = globals.User
                globals.bUserLoaded = True
            End If
        End Function

        ''' <summary>
        ''' Loads a dictionary with values from UserTable
        ''' </summary>
        ''' <param name="UserGuid">The user guid of the user.</param>
        ''' <returns>A dictionary with UserInformation or an empty dictionary if no user were found</returns>
        ''' <remarks></remarks>
        Public Function GetUserDict(ByVal UserGuid As String) As ExpDictionary
            Dim retv As ExpDictionary = Sql2Dictionary("SELECT * FROM UserTable WHERE UserGuid=" & SafeString(UserGuid))
            If retv Is Nothing Then retv = New ExpDictionary
            Return retv
        End Function

        ''' <summary>
        ''' This function loads information needed by CalcOrder in the customer specific pricing module.
        ''' </summary>
        ''' <param name="User">The user for which the context should be loaded.</param>
        ''' <returns>Dictionary containing the context for the user.</returns>
        ''' <remarks></remarks>
        Public Function LoadContext(ByVal User As ExpDictionary) As ExpDictionary
            Dim retval As New ExpDictionary
            retval("Errors") = New ExpDictionary
            retval("Warnings") = New ExpDictionary
            retval("UserGuid") = User("UserGuid")
            retval("CurrencyGuid") = User("CurrencyGuid")
            retval("DefaultCurrency") = AppSettings("MULTICURRENCY_SITE_CURRENCY")
            retval("CustomerGuid") = User("CustomerGuid")
            If CStrEx(retval("CustomerGuid")) = "" Then retval("CustomerGuid") = AppSettings("ANONYMOUS_CUSTOMERGUID")
            LoadContext = retval
        End Function

        ''' <summary>
        ''' This function set the expire date for the "store" cookie according to the COOKIE_LIFETIME setting.
        ''' </summary>
        ''' <remarks>This function should always be called after setting value in the "store" cookie.</remarks>
        Public Sub SetCookieExpireDate()
            SetCookieExpireDateEx("store")
        End Sub

        ''' <summary>
        ''' This function is written to allow for several cookies to make use of the SetCookieExpireDate function.
        ''' </summary>
        ''' <param name="CookieString">The root of the cookie.</param>
        ''' <returns>A Date Object</returns>
        ''' <remarks>This function should always be called after setting value in the "store" cookie.</remarks>
        Public Function SetCookieExpireDateEx(ByVal CookieString As Object) As Date
            Dim retv, maxdate, mindate As Date
            Dim lifetime As Decimal

            lifetime = CDblEx(AppSettings("COOKIE_LIFETIME"))
            If lifetime <> 0 Then
                maxdate = CDate("2038-01-01")
                mindate = CDate("1980-01-01")
                retv = Now.Add(System.TimeSpan.FromDays(lifetime))
                If retv > maxdate Then
                    retv = maxdate
                ElseIf retv < mindate Then
                    retv = mindate
                End If
                If HttpContext.Current.Request.Cookies("store")("OnlyPersistentCookies") <> "NotAllowed" Then
                    HttpContext.Current.Response.Cookies(CookieString).Expires = retv
                End If
            Else
                retv = Nothing
            End If
            HttpContext.Current.Response.Cookies(CookieString).path = HttpContext.Current.Server.UrlPathEncode(VRoot) & "/"
            SetCookieExpireDateEx = retv
        End Function

        ''' <summary>
        ''' Send a new pasword to a user who has forgotten his/her password.
        ''' </summary>
        ''' <param name="EmailAddress">The email address of the user who requested a new login/password</param>
        ''' <returns>Boolean</returns>
        ''' <remarks>Dependencies: Encrypt, RandomString, Dict2Table, SQL2Dicts, ValidateEmail, SendMail</remarks>
        Public Function ForgottenPassword(ByVal EmailAddress As String) As Boolean
            Dim sql, sqlB2B, strNewPwd As String
            Dim dictUsers As ExpDictionary
            '#Changed 080829: Removed arrKey, changed the other paramaters to ExpDictionary
            Dim dictUser, dictB2BUsers, dictB2BUser, dictUserBackup As ExpDictionary
            MailUtils.ConfigMailer()
            ForgottenPassword = True
            ' Validate the Email address against a regular expression
            If Not ValidateEmail(EmailAddress) Then
                globals.messages.Errors.Add(Resources.Language.MESSAGE_EMAIL_ADDRESS_NOT_VALID)
                Return False
            End If
            ' Load the user
            sql = "SELECT * FROM UserTable WHERE EmailAddress=" & SafeString(EmailAddress) & " OR EmailAddress=" & SafeString(Replace(EmailAddress, "@", "*"))
            dictUsers = SQL2Dicts(sql, "", -1)
            ' If user was not found then have a look for a B2B user who has not logged in before.
            If dictUsers.Count = 0 And B2BEnabled Then
                sqlB2B = "SELECT * FROM UserTableB2B WHERE EmailAddress=" & SafeString(EmailAddress) & " OR EmailAddress=" & SafeString(Replace(EmailAddress, "@", "*"))
                dictB2BUsers = SQL2Dicts(sqlB2B, "", -1)
                ' A B2B User exist with this login.
                ' Allow only one B2B account with
                If dictB2BUsers.Count > 0 Then
                    ' grab the first instance     
                    '#changed 080829 replaced the arrKey line
                    dictB2BUser = ExpandITLib.GetFirst(dictB2BUsers).Value
                    ' Login the user and proceed as normal.
                    ' Note: LoadUser overwrites the global User dictionary.
                    dictUserBackup = New ExpDictionary()
                    CopyDictValues(dictUserBackup, globals.User)
                    LoadUser(dictB2BUser("UserGuid"))
                    ' If the user has been created as B2B User then proceed sending the password.
                    If CBoolEx(globals.User("B2B")) Then
                        ' Get the User record once more since the login should now have been created.
                        dictUsers = SQL2Dicts(sql, "", -1)
                    End If
                    ' Set the User dictionary back.
                    globals.User = dictUserBackup
                End If
                If dictB2BUsers.Count > 1 Then
                    globals.messages.Messages.Add(Resources.Language.MESSAGE_LOST_PASSWORD_MORE_ACCOUNT_USING_EMAIL)
                End If
            End If
            ' User not found, skip. (After looking for B2C and B2B User)
            If dictUsers.Count = 0 Then
                globals.messages.Errors.Add(Resources.Language.MESSAGE_LOST_PASSWORD_USER_DOESNT_EXIST)
                ForgottenPassword = False
                Exit Function
            End If
            If dictUsers.Count > 1 Then
                globals.messages.Messages.Add(Resources.Language.MESSAGE_LOST_PASSWORD_MORE_ACCOUNT_USING_EMAIL)
            End If

            ' Get first user element            
            dictUser = GetFirst(dictUsers).Value

            ' Generate a new password
            strNewPwd = RandomString(AppSettings("SEND_FORGOTTEN_PASSWORD_LEN"))

            ' Update user information
            dictUser("UserPassword") = Encrypt(strNewPwd)
            dictUser("UserModified") = Now
            Dict2Table(dictUser, "UserTable", "UserGuid = '" & dictUser("UserGuid") & "'")

            ' Prepare the body of the mail
            Dim strBody As String = prepareHtmlMail(Encoding.UTF8, dictUser)
            If CBoolEx(AppSettings("USE_EMBEDDED_CSS_MAIL_METHOD")) Then
                Dim replacements As Dictionary(Of String, String) = New Dictionary(Of String, String)
                replacements.Add("%PASSWORD%", strNewPwd)
                replacements.Add("%SHOP_URL%", (HttpContext.Current.Request.ServerVariables("SERVER_NAME") & VRoot & "/user.aspx"))
                strBody = MailUtils.MakeReplacements(strBody, replacements)
                Dim recipient As New Dictionary(Of Object, Object)
                recipient.Add(EmailAddress, "")
                Dim mail As MailMessage = Mailer.CreateMailMessage(CStrEx(AppSettings("MAIL_EMAIL_SENDAS")), _
                    recipient, Resources.Language.LABEL_LOST_PASSWORD_SUBJECT, True, Encoding.UTF8)
                MailUtils.EmbedImages(strBody, String.Format("~/{0}/", AppSettings("LOST_PWD_MAIL_FOLDER")), mail)
                ' Send the mail
                Return Mailer.SendEmail(AppSettings("MAIL_REMOTE_SERVER"), AppSettings("MAIL_REMOTE_SERVER_PORT"), mail, globals.messages)
            Else
                strBody = Replace(strBody, "%PASSWORD%", strNewPwd)
                strBody = Replace(strBody, "%SHOP_URL%", (HttpContext.Current.Request.ServerVariables("SERVER_NAME") & VRoot & "/user.aspx"))

                ' Send the mail
                Return Mailer.SendEmail(AppSettings("MAIL_REMOTE_SERVER"), AppSettings("MAIL_REMOTE_SERVER_PORT"), CStrEx(AppSettings("MAIL_EMAIL_SENDAS")), EmailAddress, _
                    "", Resources.Language.LABEL_LOST_PASSWORD_SUBJECT, strBody, CStrEx(AppSettings("MAIL_FORMAT")) = "0", Encoding.UTF8, globals.messages)
            End If

        End Function



        Protected Function prepareHtmlMail(ByVal reqEncoding As Encoding, ByVal userDict As ExpDictionary) As String
            Dim retStr As String = Nothing
            Dim standardLanguageISO As String = "en"
            Dim folder As String = AppSettings("LOST_PWD_MAIL_FOLDER")
            Dim cssFileFolder As String = AppSettings("CSS_MAIL_FILE_FOLDER")
            Dim cssfile As String = AppSettings("CSS_MAIL_FILE")
            Dim languageISO As String = CStrEx(HttpContext.Current.Session("languageGuid")).ToLower()

            Dim filename As String
            Dim standardFilename As String
            Dim mailPath As String
            Dim standardMailPath As String
            Dim cssPath As String

            filename = AppSettings("LOST_PWD_MAIL_NAME_MAIN_PART") & languageISO & "." & AppSettings("LOST_PWD_MAIL_SUFFIX")
            standardFilename = AppSettings("LOST_PWD_MAIL_NAME_MAIN_PART") & standardLanguageISO & "." & AppSettings("LOST_PWD_MAIL_SUFFIX")

            mailPath = "~/" & folder & "/" & filename
            standardMailPath = "~/" & folder & "/" & standardFilename
            cssPath = "~/" & cssFileFolder & "/" & cssfile

            Dim templFilePath As String = Nothing

            If ExpandIT31.ExpanditFramework.Util.FileReader.fileHasContent(mailPath) Then
                templFilePath = mailPath
            ElseIf ExpandIT31.ExpanditFramework.Util.FileReader.fileHasContent(standardMailPath) Then
                templFilePath = standardMailPath
            End If

            If templFilePath IsNot Nothing Then
                Dim replaceDict As ExpDictionary = replaceParametersWithUserDictValues("MAIL_LOST_PWD_REPLACE_PARAMS", userDict)
                If CBoolEx(AppSettings("USE_EMBEDDED_CSS_MAIL_METHOD")) Then 'May not work on medium trust
                    retStr = MailUtils.EmbedCss(templFilePath, MailUtils.getInlineStyleSheetFromFile(cssPath, reqEncoding))
                    retStr = replaceHtmlFileContent(retStr, replaceDict)
                Else 'Works on medium trust
                    Dim str As String = FileReader.readFile(templFilePath, reqEncoding)
                    retStr = MailUtils.getInlineStyleSheetFromFile(cssPath, reqEncoding) & replaceHtmlFileContent(str, replaceDict)
                End If
            End If

            Return retStr

        End Function

        ''' <summary>
        ''' This function determines if a user (UserGuid) can use a particular login or if its taken by another user.
        ''' </summary>
        ''' <param name="UserGuid">The guid of the user, which you are trying to test a new login on.</param>
        ''' <param name="UserLogin">The login which needs to be tested. To be sure it does not already exist.</param>
        ''' <returns>Boolean</returns>
        ''' <remarks></remarks>
        Public Function IsLoginTaken(ByVal UserGuid As String, ByVal UserLogin As String) As Boolean
            Dim sql As String
            Dim retv As Boolean

            ' Check UserTable first
            sql = "SELECT UserLogin FROM UserTable WHERE UserGuid<>" & SafeString(UserGuid) & " AND LOWER(UserLogin)=" & SafeString(LCase(UserLogin))
            retv = HasRecords(sql)

            If Not retv Then
                If B2BEnabled Then
                    ' Check UserTableB2B if it was not found in the UserTable
                    sql = "SELECT UserLogin FROM UserTableB2B WHERE UserGuid<>" & SafeString(UserGuid) & " AND LOWER(UserLogin)=" & SafeString(LCase(UserLogin))
                    retv = HasRecords(sql)
                Else
                    retv = False
                End If
            End If
            Return retv
        End Function

        Public Sub updateUserCurrency(ByVal currencyGuid As String)
            globals.User = LoadUser(HttpContext.Current.Session("UserGuid"))
            globals.User("CurrencyGuid") = currencyGuid
            If Not CBoolEx(globals.User("Anonymous")) Then
                Dict2Table(globals.User, "UserTable", "UserGuid = " & SafeString(globals.User("UserGuid")))
            Else
                HttpContext.Current.Response.Cookies("user").Item("CurrencyGuid") = currencyGuid
            End If
            HttpContext.Current.Session("UserCurrencyGuid") = currencyGuid
            SetRecalcUserCart(HttpContext.Current.Session("UserGuid"))
            CalculateOrder()
        End Sub

        ''' <summary>
        ''' ShowLoginLink
        ''' </summary>
        ''' <returns>Boolean</returns>
        ''' <remarks></remarks>
        Public Function ShowLoginLink() As Boolean
            Dim retv As Boolean = False
            Dim b2cAccess, b2bAccess As Boolean

            If Not AppSettings("USE_NTLM") And globals.User("Anonymous") Then
                AdvCheckPageAccess("HomePage", b2cAccess, b2bAccess)
                retv = b2cAccess Or b2bAccess
            End If

            Return retv
        End Function
#End Region
#Region "lib_md5"
        Private Const MD5_SECRET As String = "EHMTVCISTRTTHPOOHAIS"
#End Region
#Region "lib_accesscontrol"

        ''' <summary>
        ''' This function will check if the current user has access to the specified access class.
        ''' </summary>
        ''' <param name="AccessClass">The name of the access class.</param>
        ''' <returns>The function returns true if the current user has access.</returns>
        ''' <remarks>Depends on AdvCheckPageAccess and PageAccessRedirect</remarks>
        Public Function CheckPageAccess(ByVal AccessClass As String) As Boolean
            Dim b2cAccess, b2bAccess As Boolean
            Return AdvCheckPageAccess(AccessClass, b2cAccess, b2bAccess)
        End Function

        ''' <summary>
        ''' Get the user access table contents
        ''' </summary>
        ''' <returns>A dictionary containing the contents of the UserAccessTable</returns>
        ''' <remarks></remarks>
        Public Shared Function GetUserAccessArray() As ExpDictionary
            Dim retv As ExpDictionary
            Dim cachekey As String = "UserAccessArray"

            retv = CacheGet(cachekey)
            If retv Is Nothing Then
                ' Initialize the array
                retv = SQL2Dicts("SELECT * FROM UserAccessTable", "AccessClass")

                ' Set the cache
                CacheSet(cachekey, retv, "UserAccessTable")
            End If

            Return retv
        End Function

        ''' <summary>
        ''' This function will check if the current user has access to the specified access class.
        ''' It also determines if access would require B2B or B2C privileges.        
        ''' </summary>
        ''' <param name="AccessClass">The name of the access class.</param>
        ''' <param name="b2cAccess">Will be set to true if the specified AccessClass is accessible for B2C users.</param>
        ''' <param name="b2bAccess">Will be set to true if the specified AccessClass is accessible for B2B users.</param>
        ''' <returns>The function returns true if the current user has access.</returns>
        ''' <remarks>This function is a helper function for the the CheckPageAccess function.</remarks>
        Public Function AdvCheckPageAccess(ByVal AccessClass As String, ByRef b2cAccess As Boolean, ByRef b2bAccess As Boolean) As Boolean
            Dim retv As Boolean = False
            Dim useraccess As ExpDictionary
            Dim AnonymousAccess As Boolean

            b2bAccess = False
            b2cAccess = False

            If AccessClass <> "" Then
                useraccess = GetUserAccessArray()
                If useraccess.ContainsKey(AccessClass) Then
                    b2bAccess = useraccess(AccessClass)("B2B") = "1"
                    b2cAccess = useraccess(AccessClass)("B2C") = "1"
                    AnonymousAccess = useraccess(AccessClass)("Anonymous") = "1"
                    If ((globals.User("B2B") And b2bAccess) Or _
                       (globals.User("B2C") And b2cAccess) Or _
                       (globals.User("Anonymous") And AnonymousAccess)) _
                    Then retv = True
                End If
            Else
                ' If no access class is specified
                retv = True
                b2bAccess = True
                b2cAccess = True
            End If

            Return retv

        End Function

        ''' <summary>
        ''' Redirects the user to another page if access is denied.
        ''' </summary>
        ''' <param name="AccessClass">The name of the access class to check.</param>
        ''' <remarks>The user will be redirected to the login page if the current user can gain access to the requested page by logging in.</remarks>
        Public Sub PageAccessRedirect(ByVal AccessClass As String)
            Dim redirectpage As String
            Dim b2cAccess, b2bAccess As Boolean
            Dim thePath As String

            If Not AdvCheckPageAccess(AccessClass, b2cAccess, b2bAccess) Then
                If HttpContext.Current.Request.ServerVariables("QUERY_STRING") <> "" Then
                    thePath = HttpContext.Current.Request.ServerVariables("SCRIPT_NAME") & "?" & HttpContext.Current.Request.ServerVariables("QUERY_STRING")
                Else
                    thePath = HttpContext.Current.Request.ServerVariables("SCRIPT_NAME")
                End If

                If globals.User("Anonymous") And (b2cAccess Or b2bAccess) Then
                    redirectpage = VRoot & "/user_login.aspx?returl=" & HttpContext.Current.Server.UrlEncode(CStrEx(thePath))
                Else
                    redirectpage = VRoot & "/" & AppSettings("URL_NOACCESS") & "?AccessClass=" & HttpContext.Current.Server.UrlEncode(AccessClass) & "&returl=" & HttpContext.Current.Server.UrlEncode(CStrEx(thePath))
                End If
                HttpContext.Current.Response.Redirect(redirectpage)
            End If
        End Sub

        ''' <summary>
        ''' Redirects the user to the login page.
        ''' </summary>
        ''' <remarks>The returl will be set so that the user will return to the current page after a successfull login.</remarks>
        Public Shared Sub ShowLoginPage()
            Dim redirectpage As String
            Dim thePath As Object

            If HttpContext.Current.Request.ServerVariables("QUERY_STRING") <> "" Then
                thePath = HttpContext.Current.Request.ServerVariables("SCRIPT_NAME") & "?" & HttpContext.Current.Request.ServerVariables("QUERY_STRING")
            Else
                thePath = HttpContext.Current.Request.ServerVariables("SCRIPT_NAME")
            End If
            redirectpage = VRoot & "/user_login.aspx?returl=" & HttpContext.Current.Server.UrlEncode(CStrEx(thePath))
            HttpContext.Current.Response.Redirect(redirectpage)
        End Sub

        ''' <summary>
        ''' Redirects the user to a given page.
        ''' </summary>
        ''' <param name="aUrl">URL of the page</param>
        ''' <remarks>The returl will be set so that the user can return to the current page after some processing.</remarks>
        Public Shared Sub ShowPage(ByVal aUrl As Object)
            Dim thePath As String
            Dim redirectpage As Object
            If HttpContext.Current.Request.ServerVariables("QUERY_STRING") <> "" Then
                thePath = HttpContext.Current.Request.ServerVariables("SCRIPT_NAME") & "?" & HttpContext.Current.Request.ServerVariables("QUERY_STRING")
            Else
                thePath = HttpContext.Current.Request.ServerVariables("SCRIPT_NAME")
            End If
            redirectpage = VRoot & "/" & aUrl & "?returl=" & HttpContext.Current.Server.UrlEncode(CStrEx(thePath))
            HttpContext.Current.Response.Redirect(redirectpage)
        End Sub

#End Region
#Region "lib_caching"

        ''' <summary>
        ''' Insert an aggregated dependency into cache
        ''' </summary>
        ''' <param name="key">The key that will identify the object in cache</param>
        ''' <param name="v">The object to cache</param>
        ''' <param name="tableNames">A string array with the tablenames that the object depends on</param>
        ''' <remarks>
        ''' Adds an object to cache with dependencies on multiple tables. 
        ''' A change in one of the dependent tables will invalidate and empty the cache.
        ''' </remarks>
        Public Shared Sub CacheSetAggregated(ByVal key As String, ByVal v As Object, ByVal tableNames As String())
            'JA2010022301 - SqlDependencyCaching - Start
            CacheManager.CacheSetAggregated(key, v, tableNames)
            'JA2010022301 - SqlDependencyCaching - End

            'If tableNames IsNot Nothing Then
            '    If Not IsLocalMode Or CBoolEx(AppSettings("LOCALMODE_USE_CACHE")) Then
            '        Dim aggDep As New AggregateCacheDependency()
            '        Dim dbName As String = CacheManager.DataBaseName
            '        For i As Integer = 0 To tableNames.Length - 1
            '            aggDep.Add(New SqlCacheDependency(dbName, tableNames(i)))
            '        Next
            '        Try
            '            CacheManager.InsertIntoCache(key, v, aggDep)
            '        Catch ex As Exception
            '            FileLogger.log(ex.Message)
            '        End Try
            '    End If
            'End If
        End Sub

        ''' <summary>
        ''' Insert an object with single dependency into cache.
        ''' </summary>
        ''' <param name="key">The key that will identify the object in cache</param>
        ''' <param name="v">The object to cache</param>
        ''' <param name="tableName">String containing the tablename that the object depends on</param>
        ''' <remarks>
        ''' Adds an object to cache with a dependency on a single table.
        ''' A change in the dependent table will invalidate and empty the cache.        
        ''' </remarks>
        Public Shared Sub CacheSet(ByVal key As String, ByVal v As Object, ByVal tableName As String)
            If Not IsLocalMode Or CBoolEx(AppSettings("LOCALMODE_USE_CACHE")) Then
                Try
                    CacheManager.InsertIntoCache(key, tableName, v)
                Catch ex As Exception
                    FileLogger.log(ex.Message)
                End Try
            End If
        End Sub

        ''' <summary>
        ''' Get an object from the cache
        ''' </summary>
        ''' <param name="key">String that identifies the object in cache</param>
        ''' <returns>The cached object</returns>
        ''' <remarks></remarks>
        Public Shared Function CacheGet(ByVal key As String) As Object
            Return HttpContext.Current.Cache.Get(key)
        End Function

        ''' <summary>
        ''' Marshalls a scripting dictionary to a string.
        ''' </summary>
        ''' <param name="d">The dictionary to be mashalled</param>
        ''' <returns></returns>
        ''' <remarks>A very usefull function for marshalling the dictionary object.</remarks>
        Public Shared Function MarshallDictionary(ByVal d As Object) As Object
            Try
                Dim memStream As MemoryStream = New MemoryStream()
                ExpXMLSerializer.Serialize(memStream, d)
                Dim b As Byte() = memStream.ToArray()
                Dim bc As Byte() = Compress(b)
                Dim str As String = System.Convert.ToBase64String(bc)
                Dim str2 As String = System.Convert.ToString(b)
                Return str
            Catch ex As Exception

            End Try
            Return String.Empty
        End Function

        ''' <summary>
        ''' Compresses data
        ''' </summary>
        ''' <param name="data">Byte() with to compress</param>
        ''' <returns>Byte() with compressed data</returns>
        ''' <remarks></remarks>
        Public Shared Function Compress(ByVal data As Byte()) As Byte()
            Dim ms As MemoryStream = New MemoryStream()
            Dim Stream As IO.Compression.GZipStream = New IO.Compression.GZipStream(ms, IO.Compression.CompressionMode.Compress)
            Stream.Write(data, 0, data.Length)
            Stream.Close()
            Return ms.ToArray()
        End Function

        ''' <summary>
        ''' Decompresses data
        ''' </summary>
        ''' <param name="data">Byte() with data to decompress</param>
        ''' <returns>Byte() with decompressed data</returns>
        ''' <remarks></remarks>
        Public Shared Function Decompress(ByVal data As Byte()) As Byte()
            Dim ms As MemoryStream = New MemoryStream()
            ms.Write(data, 0, data.Length)
            ms.Position = 0
            Dim stream As IO.Compression.GZipStream = New IO.Compression.GZipStream(ms, IO.Compression.CompressionMode.Decompress)
            Dim temp As MemoryStream = New MemoryStream()
            Dim buffer As Byte() = New Byte(1024) {}
            While (True)
                Dim read As Integer = stream.Read(buffer, 0, buffer.Length)
                If (read <= 0) Then
                    Exit While
                Else
                    temp.Write(buffer, 0, buffer.Length)
                End If
            End While
            stream.Close()
            Return temp.ToArray()
        End Function

        ''' <summary>
        ''' Unmarshalls a scripting dictionary from a string.
        ''' </summary>
        ''' <param name="s">The string to be unmashalled</param>
        ''' <returns>Dictionary</returns>
        ''' <remarks>A very usefull function for unmarshalling the dictionary object.</remarks>
        Public Shared Function UnmarshallDictionary(ByVal s As Object) As Object
            Dim retval As New ExpDictionary()
            Try
                Dim dc As Byte() = Decompress(System.Convert.FromBase64String(s))
                Dim mdcStream As MemoryStream = New MemoryStream(dc)
                mdcStream.Position = 0
                ExpXMLSerializer.Deserialize(mdcStream, retval)
            Catch ex As Exception

            End Try
            Return retval
        End Function

#End Region
#Region "lib_catalog"

        ''' <summary>
        ''' Creates a link to a product page based on a productguid
        ''' </summary>
        ''' <param name="productguid">Reference to the product.</param>
        ''' <param name="groupguid">Optional group reference.</param>
        ''' <returns></returns>
        ''' <remarks>If you pass a group reference the specified group is selected in the tree view of the catalog. If 0 (zero) is specified as groupguid then the link.aspx page will select the first group where the product is present. Use this function to reference products instead of creating absolute liks. This will improve the felxibility of the design.</remarks>
        Public Shared Function ProductLink(ByVal productguid As Object, ByVal groupguid As Object) As String
            ' Calls into C# code
            Return ExpandIT31.ExpanditFramework.Util.AppUtil.ProductLink(productguid, groupguid)
        End Function

        ''' <summary>
        ''' Creates a link to a group
        ''' </summary>
        ''' <param name="groupguid">A group reference.</param>
        ''' <returns></returns>
        ''' <remarks>Use this function to reference groups instead of creating absolute liks. This will improve
        ''' the felxibility of the design.
        ''' </remarks>
        Public Shared Function GroupLink(ByVal groupguid As Object) As String
            ' Calls into C# code
            Return ExpandIT31.ExpanditFramework.Util.AppUtil.GroupLink(groupguid)
        End Function

        ''' <summary>
        ''' Creates a link to a picture.
        ''' </summary>
        ''' <param name="filename">A relative pathname for an image file in the shop.</param>
        ''' <returns></returns>
        ''' <remarks>
        ''' Use this function to reference groups instead of creating absolute liks. This will improve
        ''' the felxibility of the design.
        ''' </remarks>
        Public Shared Function PictureLink(ByVal filename As String) As String
            PictureLink = HttpContext.Current.Server.UrlPathEncode(VRoot & "/" & filename)
        End Function

        ''' <summary>
        ''' Load the catalog groups at the root level. These root level groups are interpeted as catalogs.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks>This function returns a dictionary containing information on the root level catalog groups.</remarks>
        Public Function CatLoadCatalogs() As Object
            Dim sql As String
            Dim groups, info As Object

            ' Load the top level group guids
            'JA2010030901 - PERFORMANCE -Start
            'ExpandIT US - USE CSC - Start
            If CBoolEx(AppSettings("EXPANDIT_US_USE_CSC")) Then
                sql = obj.groupsWithProductsSQL("'0'")
            Else
                'AM2010071901 - ENTERPRISE CSC - Start
                sql = "SELECT GroupGuid FROM GroupTable WHERE ParentGuid=0 AND CatalogNo=" & SafeString(globals.User("CatalogNo"))
                'AM2010071901 - ENTERPRISE CSC - End
            End If
            'ExpandIT US - USE CSC - End
            'JA2010030901 - PERFORMANCE -End
            groups = Sql2Dictionaries(sql, New String() {"GroupGuid"})

            ' Load information about groups
            info = CatGroups2Info(groups)
            CatLoadGroups(info)

            CatLoadCatalogs = groups
        End Function

        ''' <summary>
        ''' Create a default info structure based on an array or a dictionary of groups.
        ''' </summary>
        ''' <param name="groups">An array or dictionary of groups.</param>
        ''' <returns></returns>
        ''' <remarks>
        ''' If you pass an array to this function each entry in the array must be a group guid.
        ''' If the groups parameter is a dictionary each entry must be a dictionary containing 
        ''' at least one element named GroupGuid.
        ''' </remarks>
        Public Function CatGroups2Info(ByVal groups As Object) As Object
            Dim info As ExpDictionary
            Dim GroupGuid As String
            Dim grouparray() As String
            Dim Group As ExpDictionary
            Dim groupinfo As ExpDictionary

            If Right(TypeName(groups), 2) = "()" Then
                grouparray = groups
                groups = New ExpDictionary
                For Each GroupGuid In grouparray
                    Group = New ExpDictionary
                    Group.Add("GroupGuid", Integer.Parse(GroupGuid))
                    groups.Add(GroupGuid.ToString, Group)
                Next
            End If

            groupinfo = New ExpDictionary()
            groupinfo.Add("Groups", groups)
            groupinfo.Add("PropertyArray", Nothing)

            info = CatDefaultInfo()
            info.Add("GroupInfo", groupinfo)

            CatGroups2Info = info
        End Function

        ''' <summary>
        ''' Create a default info structure based on an array or a dictionary of products.
        ''' </summary>
        ''' <param name="products">An array or dictionary of products.</param>
        ''' <returns></returns>
        ''' <remarks>
        ''' If you pass an array to this function each entry in the array must be a product guid.
        ''' If the products parameter is a dictionary each entry must be a dictionary containing 
        ''' at least one element named ProductGuid.
        ''' </remarks>
        Public Function CatProducts2Info(ByVal products As Object) As ExpDictionary
            Dim info As ExpDictionary
            Dim productguid As String
            Dim productarray() As String
            Dim product, productinfo As ExpDictionary

            If Right(TypeName(products), 2) = "()" Then
                productarray = products
                products = New ExpDictionary
                For Each productguid In productarray
                    product = New ExpDictionary
                    product.Add("ProductGuid", productguid)
                    products.Add(productguid, product)
                Next
            End If

            productinfo = New ExpDictionary
            productinfo.Add("Products", products)
            productinfo.Add("ProductFieldArray", DBNull.Value)
            productinfo.Add("PropertyArray", DBNull.Value)

            info = CatDefaultInfo()
            info.Add("ProductInfo", productinfo)

            Return info
        End Function

        ''' <summary>
        ''' Load information about a single product.
        ''' </summary>
        ''' <param name="productguid">Reference to a product.</param>
        ''' <param name="bLoadVariants">Boolean specifying if variant information should be loaded.</param>
        ''' <param name="bLoadRelated">Boolean specifying if related products should be loaded.</param>
        ''' <param name="bCustomerSpecificPrice">Boolean specifying if customer specific prices should be calculated.</param>
        ''' <returns></returns>
        ''' <remarks>
        ''' This function is normally used on product pages to load information about a single product.
        ''' It is an encapsulation of the more complex functions that load entire groups of products.
        ''' </remarks>
        Public Function CatDefaultLoadProduct(ByVal productguid As String, ByVal bLoadVariants As Boolean, ByVal bLoadRelated As Boolean, ByVal bCustomerSpecificPrice As Boolean) As Object
            Dim products As ExpDictionary
            Dim cachekey As String

            cachekey = "Product-" & productguid & "-" & HttpContext.Current.Session("LanguageGuid").ToString & "-" & IIf(bLoadVariants, 1, 0) & IIf(bLoadRelated, 1, 0)
            products = CatDefaultLoadProducts(New String() {productguid}, bLoadVariants, bLoadRelated, bCustomerSpecificPrice, Nothing, Nothing, cachekey)
            CatDefaultLoadProduct = products(productguid)
        End Function

        ''' <summary>
        ''' Load information about a range of products.
        ''' </summary>
        ''' <param name="products">An array or dictionary of products.</param>
        ''' <param name="bLoadVariants">Boolean specifying if variant information should be loaded.</param>
        ''' <param name="bLoadRelated">Boolean specifying if related products should be loaded.</param>
        ''' <param name="bCustomerSpecificPrice">Boolean specifying if customer specific prices should be calculated.</param>
        ''' <param name="ProductFieldArray">An array of fields from the ProductTable to load.</param>
        ''' <param name="proparray">An array of properties to load.</param>
        ''' <param name="cachekey">A cache key that should be used to cache the result.</param>
        ''' <returns></returns>
        ''' <remarks>
        ''' If ProductFieldArray is null all fields are loaded from the ProductTable. The proparray can also be set to null,
        ''' which will cause all properties to be loaded. Specifying null for ProductFieldArray or proparray will lower the 
        ''' performance compared to passing a specific set of product fields and property names.
        ''' The result is only cached if a cachekey is specified.
        ''' </remarks>
        Public Function CatDefaultLoadProducts(ByVal products As Object, ByVal bLoadVariants As Boolean, ByVal bLoadRelated As Boolean, ByVal bCustomerSpecificPrice As Boolean, ByVal ProductFieldArray() As String, ByVal proparray() As String, ByVal cachekey As String) As Object
            Dim info As ExpDictionary
            Dim productinfo As ExpDictionary
            Dim product As ExpDictionary
            Dim productarray As Object
            Dim productguid As String
            Dim relatedinfo As ExpDictionary

            info = CatDefaultInfo()
            info("General")("CacheKey") = cachekey

            If Right(TypeName(products), 2) = "()" Then
                productarray = products
                products = New ExpDictionary()
                For Each productguid In productarray
                    product = New ExpDictionary()
                    product.Add("ProductGuid", productguid)
                    products.Add(productguid, product)
                Next
            End If

            productinfo = New ExpDictionary
            productinfo("Products") = products
            productinfo("ProductFieldArray") = ProductFieldArray
            productinfo("PropertyArray") = proparray
            productinfo("LoadAllVariants") = bLoadVariants
            productinfo("CustomerSpecificPrice") = bCustomerSpecificPrice
            productinfo("ConvertCurrency") = True
            info("ProductInfo") = productinfo

            If bLoadRelated Then
                relatedinfo = New ExpDictionary
                relatedinfo("ProductFieldArray") = ProductFieldArray
                relatedinfo("PropertyArray") = proparray
                relatedinfo("LoadAllVariants") = bLoadVariants
                relatedinfo("CustomerSpecificPrice") = bCustomerSpecificPrice
                relatedinfo("ConvertCurrency") = True
                productinfo("RelatedInfo") = relatedinfo
            End If

            CatLoadProducts(info)
            products = info("ProductInfo")("Products")
            Return products
        End Function

        ''' <summary>
        ''' Creates the general part of an info dictionary. The values are based on the current User context.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function CatDefaultGeneral() As ExpDictionary
            Dim retv As New ExpDictionary()
            retv("LanguageGuid") = HttpContext.Current.Session("LanguageGuid").ToString
            retv("DefaultLanguageGuid") = AppSettings("LANGUAGE_DEFAULT")
            retv("CalcPrices") = True

            retv("DefaultCurrency") = AppSettings("MULTICURRENCY_SITE_CURRENCY")
            If HttpContext.Current.Session("UserCurrencyGuid").ToString <> "" Then
                retv("CurrencyArray") = New Object() {HttpContext.Current.Session("UserCurrencyGuid").ToString, HttpContext.Current.Session("UserSecondaryCurrencyGuid").ToString}
            Else
                retv("CurrencyArray") = New Object() {HttpContext.Current.Session("UserCurrencyGuid").ToString}
            End If
            CatDefaultGeneral = retv
        End Function

        ''' <summary>
        ''' Creates a default info dictionary. The values are based on the current User context.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function CatDefaultInfo() As ExpDictionary
            Dim retv As New ExpDictionary()
            retv.Add("General", CatDefaultGeneral())
            Return retv
        End Function

        ''' <summary>
        ''' Loads group information based on the content in the info dictionary.
        ''' </summary>
        ''' <param name="info">Info dictionary</param>
        ''' <remarks>Info dictionaries are described in the developers guide.</remarks>
        Public Sub CatLoadGroups(ByRef info As ExpDictionary)
            Dim cachekey As String
            Dim cachedict As ExpDictionary = Nothing
            Dim general As ExpDictionary
            ' Check cache
            general = info("General")
            cachekey = CStrEx(general("CacheKey"))
            If cachekey <> "" Then cachedict = CacheGet(cachekey) Else cachedict = Nothing
            If Not cachedict Is Nothing Then
                ' Use cache
                info = cachedict

                ' Use general part from parameter
                info("General") = general
            Else
                ' Load info                
                IntLoadGroups(info)

                ' Set cache
                If cachekey <> "" Then
                    Dim tableNames As String() = Split(AppSettings("GroupProductRelatedTablesForCaching"), "|")
                    CacheSetAggregated(cachekey, info, tableNames)
                End If
            End If

            ' Calculate prices and convert currencies
            If general("CalcPrices") Then CatCalcPrices(info)
        End Sub

        ''' <summary>
        ''' Loads product information based on the content in the info dictionary.
        ''' </summary>
        ''' <param name="info">Info dictionary</param>
        ''' <remarks>Info dictionaries are described in the developers guide.</remarks>
        Public Sub CatLoadProducts(ByRef info As ExpDictionary)
            Dim cachekey As String
            Dim cachedict, general As Object
            ' Check cache
            general = info("General")
            cachekey = CStrEx(general("CacheKey"))
            If cachekey <> "" Then cachedict = CacheGet(cachekey) Else cachedict = Nothing
            If Not cachedict Is Nothing Then
                ' Use cache
                info = cachedict

                ' Use general part from parameter
                info("General") = general
            Else
                ' Load info
                IntLoadProductsInfo(info)

                ' Set cache
                If cachekey <> "" Then
                    Dim tableNames As String() = Split(AppSettings("GroupProductRelatedTablesForCaching"), "|")
                    CacheSetAggregated(cachekey, info, tableNames)
                End If
            End If
            ' Calculate prices and convert currencies
            If general("CalcPrices") Then CatCalcPrices(info)
        End Sub

        ' ************************************************************************
        '
        ' INTERNAL FUNCTIONS
        '
        ' ************************************************************************

        Private Sub IntLoadProductsInfo(ByRef info As ExpDictionary)
            Dim productinfo, relatedinfo, general As Object
            Dim products As ExpDictionary = Nothing
            Dim product As ExpDictionary = Nothing
            Dim bLoadAllVariants As Boolean
            Dim bConvertCurrency, bCustomerSpecificPrice As Boolean
            Dim prdfldarray() As String = {}
            Dim prdproparray() As String = {}
            Dim langguid As String
            Dim extlangguid As String
            Dim productguid As String
            Dim deflangguid As String

            'JA2010030901 - PERFORMANCE -Start
            Dim intStartNumber As Integer
            Dim intEndNumber As Integer
            Dim intIndex As Integer
            Dim dictTmpProducts As ExpDictionary = Nothing
            'JA2010030901 - PERFORMANCE -End

            relatedinfo = Nothing
            productinfo = Nothing

            ' Read parameters
            general = info("General")
            langguid = CStrEx(general("LanguageGuid"))
            extlangguid = general("ExternalLanguageGuid")
            deflangguid = general("DefaultLanguageGuid")
            If extlangguid Is Nothing Then
                extlangguid = GetExternalFromIsoLanguageGuid(langguid)
            End If

            If info.Exists("ProductInfo") Then

                productinfo = info("ProductInfo")

                'JA2010030901 - PERFORMANCE -Start
                If (productinfo("Products").Count <= 0) Then Exit Sub
                'JA2010030901 - PERFORMANCE -End

                'JA2010030901 - PERFORMANCE -Start
                If general.Exists("Performance") Then
                    dictTmpProducts = productinfo("Products")
                    products = New ExpDictionary()
                    intStartNumber = (CIntEx(general("Performance")("TotalPerPage")) * (CIntEx(general("Performance")("PageNumber")))) + 1
                    intEndNumber = CIntEx(general("Performance")("TotalPerPage")) * (CIntEx(general("Performance")("PageNumber")) + 1)
                    If (dictTmpProducts.Count < intEndNumber) Or (CStrEx(general("Performance")("TotalPerPage")).ToUpper = "ALL") Then intEndNumber = dictTmpProducts.Count
                    Dim lstProductKeys As New List(Of Object)(dictTmpProducts.Keys())
                    For intIndex = intStartNumber To intEndNumber Step 1
                        products(CStrEx(lstProductKeys.Item(intIndex - 1))) = dictTmpProducts(CStrEx(lstProductKeys.Item(intIndex - 1)))
                    Next
                Else
                    products = productinfo("Products")
                End If
                'JA2010030901 - PERFORMANCE -End
                prdfldarray = productinfo("ProductFieldArray")
                prdproparray = productinfo("PropertyArray")
                bLoadAllVariants = CBoolEx(productinfo("LoadAllVariants"))
                bConvertCurrency = CBoolEx(productinfo("ConvertCurrency"))
                bCustomerSpecificPrice = CBoolEx(productinfo("CustomerSpecificPrice"))
            End If

            ' Validate parameters
            If langguid = "" Then HttpContext.Current.Response.Write("Invalid LanguageGuid<br />")
            If extlangguid = "" Then HttpContext.Current.Response.Write("Invalid ExternalLangaugeGuid<br />")

            ' Load Products
            IntLoadProducts(products, prdfldarray, prdproparray, langguid, extlangguid, deflangguid)

            ' Mark products with pricing and currency information
            If bConvertCurrency Or bCustomerSpecificPrice Then
                For Each product In products.Values
                    productguid = CStrEx(product("ProductGuid"))
                    If productguid <> "" Then
                        If bConvertCurrency Then
                            product("_cc") = True
                        End If
                        If bCustomerSpecificPrice Then
                            product("_csp") = True
                        End If
                    End If
                Next
            End If

            ' Load Variants
            If bLoadAllVariants Then
                IntLoadVariants(products, langguid, extlangguid)
            End If

            ' Load Related Products
            If productinfo.Exists("RelatedInfo") Then
                relatedinfo = productinfo("RelatedInfo")
                IntLoadRelated(products, general, relatedinfo)
            End If

            'AM2010061801 - KITTING - START
            'Load Kitting Info
            If AppSettings("EXPANDIT_US_USE_KITTING") Then
                Dim kittingObj As New USKitting()

                kittingObj.IntLoadKittingInfo(products, general)
            End If
            'AM2010061801 - KITTING - END

            'JA2010030901 - PERFORMANCE -Start
            If general.Exists("Performance") Then
                dictTmpProducts = productinfo("Products")
                Dim lstProductKeys As New List(Of Object)(dictTmpProducts.Keys)
                For intIndex = intStartNumber To intEndNumber Step 1
                    productinfo("Products")(lstProductKeys.Item(intIndex - 1)) = products(lstProductKeys.Item(intIndex - 1))
                Next
            End If
            'JA2010030901 - PERFORMANCE -End

        End Sub


        '*****CUSTOMER SPECIFIC CATALOGS***** - START
        Public Sub IntLoadProductsInfoCSC(ByRef info As ExpDictionary)
            IntLoadProductsInfo(info)
        End Sub
        '*****CUSTOMER SPECIFIC CATALOGS***** - END

        Private Sub CatCalcPrices(ByVal info As ExpDictionary)
            Dim currencyproducts As ExpDictionary
            Dim priceproducts As ExpDictionary
            Dim general, defaultcurrency, currencyarray As Object
            Dim i As Integer
            Dim k As String

            general = info("General")

            ' Get products for which we want to calculate a price or convert currency
            currencyproducts = New ExpDictionary
            priceproducts = New ExpDictionary
            IntLocateDictionaries(info, New Object() {"_cc", "_csp"}, New Object() {currencyproducts, priceproducts}, False)

            ' Remove invalid products
            For Each k In priceproducts.ClonedKeyArray
                If priceproducts(k).ContainsKey("_Invalid") Then
                    priceproducts.Remove(k)
                End If
            Next

            ' Get customer specific prices
            If priceproducts.Count > 0 Then
                GetCustomerPricesEx(priceproducts)
            End If

            ' Convert currencies
            defaultcurrency = general("DefaultCurrency")
            If general.Exists("CurrencyArray") Then currencyarray = general("CurrencyArray") Else currencyarray = New Object() {}
            For i = 0 To UBound(currencyarray)
                ConvertAmounts(currencyproducts, New Object() {"ListPrice", "TaxAmount", "ListPriceInclTax", "LineTotal", "TotalInclTax", "LineDiscountAmount", "LineDiscountAmountInclTax"}, defaultcurrency, currencyarray(i))
            Next
        End Sub

        Private Sub IntLoadGroups(ByVal info As ExpDictionary)
            '*****CUSTOMER SPECIFIC CATALOGS***** - START
            If AppSettings("EXPANDIT_US_USE_CSC") Then
                obj.IntLoadGroupsCSC(info)
            Else
                '*****CUSTOMER SPECIFIC CATALOGS***** - END
                Dim groupinfo As ExpDictionary
                Dim subgroupinfo As ExpDictionary
                Dim proparray() As String = {}
                Dim grouparray() As String = {}
                Dim sql As String
                Dim subgroups As ExpDictionary = Nothing
                Dim grouplist As String
                Dim newinfo As ExpDictionary
                Dim Group As ExpDictionary
                Dim groups As ExpDictionary
                Dim subgroups2 As ExpDictionary
                Dim productinfo As ExpDictionary
                Dim products As ExpDictionary = Nothing
                Dim i As Integer
                Dim key As String
                Dim groupguid As String
                Dim langguid As String
                Dim defaultlangguid As String
                Dim general As ExpDictionary
                Dim item As ExpDictionary
                Dim properties As ExpDictionary
                Dim subgroup As ExpDictionary
                Dim products2 As ExpDictionary = Nothing
                Dim product As ExpDictionary = Nothing

                subgroupinfo = Nothing
                productinfo = Nothing
                groupinfo = Nothing

                ' Read parameters
                general = info("General")
                langguid = CStrEx(general("LanguageGuid"))
                defaultlangguid = CStrEx(general("DefaultLanguageGuid"))

                If info.Exists("GroupInfo") Then
                    groupinfo = info("GroupInfo")
                    groups = groupinfo("Groups")
                    proparray = groupinfo("PropertyArray")
                    If groupinfo.Exists("SubgroupInfo") Then
                        subgroupinfo = groupinfo("SubgroupInfo")
                    End If
                    If groupinfo.Exists("ProductInfo") Then
                        productinfo = groupinfo("ProductInfo")
                    End If
                Else
                    groups = New ExpDictionary
                End If

                ' Make a group list
                grouparray = New String() {}
                grouplist = ""
                i = 0
                For Each item In groups.Values
                    groupguid = CStrEx(item("GroupGuid"))
                    If groupguid <> "" Then
                        grouplist = grouplist & ", " & groupguid
                        ReDim Preserve grouparray(i)
                        grouparray(i) = groupguid
                        i = i + 1
                    End If
                Next
                grouplist = Mid(grouplist, 3)

                ' Exit function if no groups were specified
                If grouplist = "" Then
                    Exit Sub
                End If

                ' Load Properties
                properties = IntLoadProperties("GRP", grouparray, proparray, langguid, defaultlangguid)

                ' Load Subgroups
                If Not subgroupinfo Is Nothing Then
                    'AM2010071901 - ENTERPRISE CSC - Start
                    sql = "SELECT GroupGuid, ParentGuid FROM GroupTable WHERE ParentGuid IN (" & grouplist & ") AND CatalogNo=" & SafeString(globals.User("CatalogNo")) & " ORDER BY ParentGuid, SortIndex"
                    'sql = "SELECT GroupGuid, ParentGuid FROM GroupTable WHERE ParentGuid IN (" & grouplist & ")  ORDER BY ParentGuid, SortIndex"
                    'AM2010071901 - ENTERPRISE CSC - End

                    subgroups = Sql2Dictionaries2(sql, New String() {"ParentGuid", "GroupGuid"})

                    ' Create a new dictionary containing references to all subgroups
                    subgroups2 = New ExpDictionary()
                    For Each Group In subgroups.Values
                        For Each subgroup In Group.Values
                            subgroups2(subgroup("GroupGuid") & "," & subgroup("ParentGuid")) = subgroup
                        Next
                    Next

                    ' Make recursive call to load subgroups
                    subgroupinfo.Add("Groups", subgroups2)
                    newinfo = New ExpDictionary()
                    newinfo.Add("General", general)
                    newinfo.Add("GroupInfo", subgroupinfo)
                    IntLoadGroups(newinfo)
                End If

                ' Load Products
                If Not productinfo Is Nothing Then
                    ' Find the products for the specified groups
                    sql = "SELECT GroupProduct.GroupGuid, GroupProduct.ProductGuid FROM GroupProduct INNER JOIN ProductTable ON GroupProduct.ProductGuid=ProductTable.ProductGuid WHERE GroupProduct.GroupGuid IN (" & grouplist & ") ORDER BY GroupGuid, SortIndex"
                    products = Sql2Dictionaries2(sql, New String() {"GroupGuid", "ProductGuid"})

                    ' Create a new flat dictionary containing references to all products
                    products2 = New ExpDictionary()
                    For Each Group In products.Values
                        For Each product In Group.Values
                            products2(product("GroupGuid") & "," & product("ProductGuid")) = product
                        Next
                    Next

                    ' Make call to load products
                    productinfo.Add("Products", products2)
                    newinfo = New ExpDictionary()
                    newinfo.Add("General", general)
                    newinfo.Add("ProductInfo", productinfo)
                    IntLoadProductsInfo(newinfo)
                End If

                ' Assing info to groups
                For Each Group In groups.Values
                    groupguid = CStrEx(Group("GroupGuid"))
                    If groupguid <> "" Then
                        ' Set properties
                        CopyDictValues(Group, properties(groupguid))

                        ' Set subgroups
                        If Not subgroupinfo Is Nothing Then
                            key = SafeString(groupguid)
                            If subgroups.Exists(key) Then
                                Group("Subgroups") = subgroups(key)
                            End If
                        End If

                        ' Set products
                        If Not productinfo Is Nothing Then
                            key = SafeString(groupguid)
                            If products.Exists(key) Then
                                Group("Products") = products(key)
                            End If
                        End If
                    End If
                Next

                ' Remove the products from productinfo
                If Not productinfo Is Nothing Then
                    productinfo.Remove("Products")
                End If

            End If
        End Sub

        Private Shared Sub IntLocateDictionaries(ByVal haystack As ExpDictionary, ByVal needlearray() As Object, _
            ByVal resultarray() As Object, ByVal removemarks As Boolean)

            Dim i As Integer
            Dim item As Object

            For i = 0 To UBound(needlearray)
                If haystack.Exists(needlearray(i)) Then
                    resultarray(i)(CStr(resultarray(i).Count + 1)) = haystack
                    If removemarks Then haystack.Remove(needlearray(i))
                End If
            Next

            ' Scan for sub dictionaries
            For Each item In haystack.Values
                If TypeName(item) = "ExpDictionary" Then IntLocateDictionaries(item, needlearray, resultarray, removemarks)
            Next
        End Sub

        Private Sub IntLoadRelated(ByVal products As ExpDictionary, ByVal general As Object, ByVal relatedinfo As Object)
            Dim productarray, relationtype As Object
            Dim i As Integer
            Dim prdlist, sql, productguid As String
            Dim info As ExpDictionary
            Dim productrelations As ExpDictionary
            Dim productrelation As ExpDictionary
            Dim relatedproduct As ExpDictionary
            Dim relatedproducts, item, product As Object

            ' Make a product list
            prdlist = ""
            productarray = New Object() {}
            i = 0
            For Each item In products.Values
                productguid = CStrEx(item("ProductGuid"))
                If productguid <> "" Then
                    prdlist = prdlist & ", " & SafeString(productguid)
                    ReDim Preserve productarray(i)
                    productarray(i) = productguid
                    i = i + 1
                End If
            Next
            prdlist = Mid(prdlist, 3)
            If prdlist = "" Then Exit Sub

            sql = "SELECT ProductRelation.ProductGuid, ProductRelation.ProductRelationType, ProductRelation.RelatedToProductGuid, ProductRelation.SortIndex "
            'JA2010030901 - PERFORMANCE -Start
            sql &= "FROM ProductRelation INNER JOIN (SELECT DISTINCT GroupProduct.ProductGuid FROM GroupProduct "
            'ExpandIT US - USE CSC - Start
            If CBoolEx(AppSettings("EXPANDIT_US_USE_CSC")) Then
                If globals.Context Is Nothing Then
                    globals.Context = LoadContext(globals.User)
                End If
                sql &= " INNER JOIN (" & obj.getListAvailableItems(globals.User) & ") AS AvailableItems ON GroupProduct.ProductGuid=AvailableItems.ProductGuid "
            End If
            'AM2010071901 - ENTERPRISE CSC - Start
            sql &= "WHERE CatalogNo=" & SafeString(globals.User("CatalogNo")) & ") AS GP ON RelatedToProductGuid=GP.ProductGuid "
            'AM2010071901 - ENTERPRISE CSC - End
            'ExpandIT US - USE CSC - End
            'JA2010030901 - PERFORMANCE -End
            sql &= "WHERE ProductRelation.ProductGuid IN (" & prdlist & ") ORDER BY ProductRelation.SortIndex"
            productrelations = Sql2Dictionaries2(sql, New Object() {"ProductGuid", "ProductRelationType", "RelatedToProductGuid"})

            ' Make related product dictionary
            relatedproducts = New ExpDictionary
            For Each productrelation In productrelations.Values
                For Each relationtype In productrelation.Values
                    For Each relatedproduct In relationtype.Values
                        productguid = CStrEx(relatedproduct("RelatedToProductGuid"))
                        If productguid <> "" Then
                            product = New ExpDictionary()
                            product("ProductGuid") = productguid
                            relatedproducts(SafeString(productguid)) = product
                        End If
                        relatedproduct.Remove("SortIndex")
                    Next
                Next
            Next

            ' Call to load products
            relatedinfo("Products") = relatedproducts
            info = New ExpDictionary()
            info("Recursion") = True
            info("General") = general
            info("ProductInfo") = relatedinfo
            IntLoadProductsInfo(info)

            ' Set related product information on productrelations
            For Each productrelation In productrelations.Values
                For Each relationtype In productrelation.Values
                    For Each relatedproduct In relationtype.Values
                        productguid = CStrEx(relatedproduct("RelatedToProductGuid"))
                        relatedproduct.Remove("RelatedToProductGuid")
                        If productguid <> "" Then
                            CopyDictValues(relatedproduct, relatedproducts(SafeString(productguid)))
                        End If
                    Next
                Next
            Next

            ' Set related products on product dictionary
            For Each product In products.Values
                productguid = CStrEx(product("ProductGuid"))
                If productguid <> "" Then
                    If productrelations.Exists(SafeString(productguid)) Then
                        product("Related") = productrelations(SafeString(productguid))
                    End If
                End If
            Next
        End Sub

        Private Shared Sub IntLoadProducts(ByVal products As ExpDictionary, ByVal ProductFieldArray() As String, ByVal PropertyArray() As String, ByVal LanguageGuid As String, ByVal ExternalLanguageGuid As String, ByVal DefaultLanguageGuid As String)
            Dim sql As String
            Dim fieldlist As String
            Dim prdlist As String
            Dim prdvarlist As String
            Dim product As ExpDictionary
            Dim fieldvalues As ExpDictionary
            Dim properties As ExpDictionary = Nothing
            Dim sqldelim As String
            Dim i As Integer
            Dim item As ExpDictionary
            Dim variantcode As String
            Dim bLoadProductFields, bLoadProperties, bLoadTranslation As Boolean
            Dim key As String
            Dim variantvalues As ExpDictionary = Nothing
            Dim productarray() As String
            Dim productguid As String
            Dim variantvalue As ExpDictionary
            Dim criteria As String

            sqldelim = "\"
            bLoadTranslation = False

            ' Exit function no products were specified
            If products.Count = 0 Then
                Exit Sub
            End If

            ' Make a field list
            If ProductFieldArray Is Nothing Then
                fieldlist = "ProductTable.*"
                bLoadProductFields = True
                bLoadTranslation = CBoolEx(AppSettings("USE_PRODUCTTRANSLATION"))
            Else
                fieldlist = ""
                For i = 0 To UBound(ProductFieldArray)
                    If UCase(ProductFieldArray(i)) <> "PRODUCTGUID" Then
                        fieldlist = fieldlist & ", ProductTable.[" & ProductFieldArray(i) & "]"
                    End If

                    If UCase(ProductFieldArray(i)) = "PRODUCTNAME" Then
                        bLoadTranslation = CBoolEx(AppSettings("USE_PRODUCTTRANSLATION"))
                    End If
                Next
                bLoadProductFields = True 'fieldlist <> ""
                fieldlist = "ProductTable.ProductGuid" & fieldlist
            End If

            ' Make a product list
            prdlist = ""
            prdvarlist = ""
            productarray = New String() {}
            i = 0
            For Each item In products.Values
                productguid = CStrEx(item("ProductGuid"))
                If item.Exists("VariantCode") Then variantcode = CStrEx(item("VariantCode")) Else variantcode = ""
                If productguid <> "" Then
                    prdlist = prdlist & ", " & SafeString(productguid)
                    If variantcode <> "" Then prdvarlist = prdvarlist & ", " & SafeString(productguid & sqldelim & variantcode)
                    ReDim Preserve productarray(i)
                    productarray(i) = productguid
                    i = i + 1
                End If
            Next
            prdlist = Mid(prdlist, 3)
            prdvarlist = Mid(prdvarlist, 3)
            If prdlist = "" Then Exit Sub

            ' Load products
            If bLoadProductFields Then
                ' Get fields from the ProductTable and Translations
                If bLoadTranslation Then
                    If CBoolEx(AppSettings("USE_PRODUCTVARIANTTRANSLATION")) Then
                        criteria = "VariantCode IS NULL OR VariantCode=''"
                    Else
                        criteria = "1=1"
                    End If
                    sql = "SELECT " & fieldlist & ", PT.ProductName AS [ProductNameTranslation] FROM ProductTable LEFT JOIN" & _
                     " (SELECT * FROM ProductTranslation WHERE (" & criteria & ") AND " & HttpContext.Current.Application("EXTERNAL_LANGUAGEGUID_SQL") & "=" & SafeString(ExternalLanguageGuid) & ") AS PT" & _
                     " ON ProductTable.ProductGuid=PT.ProductGuid" & _
                     " WHERE ProductTable.ProductGuid IN (" & prdlist & ")"
                Else
                    sql = "SELECT " & fieldlist & ", ProductTable.ProductName AS [ProductNameTranslation] FROM ProductTable WHERE ProductGuid IN (" & prdlist & ")"
                End If
                fieldvalues = Sql2Dictionaries(sql, New Object() {"ProductGuid"})
                ' Set the product name translation where no translation was found
                If CBoolEx(AppSettings("USE_PRODUCTTRANSLATION")) Then
                    For Each item In fieldvalues.Values
                        If CStrEx(item("ProductNameTranslation")) <> "" Then item("ProductName") = item("ProductNameTranslation")
                        item.Remove("ProductNameTranslation")
                    Next
                End If
            Else
                fieldvalues = Nothing
            End If

            ' Load specific variants
            If prdvarlist <> "" Then
                'AM2010021901 - VARIANTS - Start
                If CBoolEx(AppSettings("EXPANDIT_US_USE_VARIANTS")) Then
                    'AM2010021901 - VARIANTS - End
                    If CBoolEx(AppSettings("USE_PRODUCTVARIANTTRANSLATION")) Then
                        sql = "SELECT ProductVariant.*, PT.ProductName AS [VariantNameTranslation] FROM ProductVariant LEFT JOIN" & _
                         " (SELECT * FROM ProductTranslation WHERE " & HttpContext.Current.Application("EXTERNAL_LANGUAGEGUID_SQL") & "=" & SafeString(ExternalLanguageGuid) & ") AS PT" & _
                         " ON ProductVariant.ProductGuid=PT.ProductGuid AND ProductVariant.VariantCode=PT.VariantCode" & _
                         " WHERE ProductVariant.ProductGuid+" & SafeString(sqldelim) & "+ProductVariant.VariantCode IN (" & prdvarlist & ")"
                    Else
                        sql = "SELECT ProductVariant.*, ProductVariant.VariantName AS [VariantNameTranslation] FROM ProductVariant" & _
                         " WHERE ProductVariant.ProductGuid+" & SafeString(sqldelim) & "+ProductVariant.VariantCode IN (" & prdvarlist & ")"
                    End If
                    variantvalues = Sql2Dictionaries(sql, New Object() {"ProductGuid", "VariantCode"})
                End If
            End If

            ' Load properties
            If PropertyArray Is Nothing Then
                bLoadProperties = True
            Else
                bLoadProperties = UBound(PropertyArray) >= 0
            End If
            If bLoadProperties Then
                properties = IntLoadProperties("PRD", productarray, PropertyArray, LanguageGuid, DefaultLanguageGuid)
            End If

            For Each product In products.Values
                productguid = product("ProductGuid")
                ' Set product info
                If fieldvalues.Exists(SafeString(productguid)) Then
                    CopyDictValues(product, fieldvalues(SafeString(productguid)))
                    'AM2010051801 - SPECIAL ITEMS - Start
                    product("IsSpecial") = IsSpecial(productguid)
                    'AM2010051801 - SPECIAL ITEMS - End
                    'AM2010051802 - WHAT's NEW - Start
                    product("IsNew") = IsNew(productguid)
                    'AM2010051801 - SPECIAL ITEMS - Start
                    product("LastName") = LastName(productguid)
                    'AM2010051801 - SPECIAL ITEMS - End
                    'AM2010051802 - WHAT's NEW - End
                    ' Set variant info
                    If Not variantvalues Is Nothing Then
                        If product.Exists("VariantCode") Then variantcode = CStrEx(product("VariantCode")) Else variantcode = ""
                        If variantcode <> "" Then
                            key = SafeString(productguid) & "," & SafeString(variantcode)
                            If variantvalues.Exists(key) Then
                                variantvalue = variantvalues(key)
                                product("VariantName") = IIf(CStrEx(variantvalue("VariantNameTranslation")) <> "", variantvalue("VariantNameTranslation"), variantvalue("VariantName"))
                            End If
                        End If
                    End If

                    ' Set properties
                    If bLoadProperties Then
                        If properties.Exists(productguid) Then CopyDictValues(product, properties(productguid))
                    End If
                Else
                    product("_Invalid") = True
                End If
            Next

        End Sub

        'AM2010051801 - SPECIAL ITEMS - Start
        Private Shared Function IsSpecial(ByVal productGuid As String)
            Dim sql As String = ""
            Dim specialsEnabled As Boolean = False
            Dim specialsCampaign As String = ""

            'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - Start
            Dim pageobj As ExpandIT.Page = CType(System.Web.HttpContext.Current.Handler, ExpandIT.Page)
            'specialsEnabled = CBoolEx(getSingleValueDB("SELECT SpecialsEnabled FROM EESetup"))
            Try
                specialsEnabled = CBoolEx(pageobj.eis.getEnterpriseConfigurationValue("SpecialsEnabled"))
            Catch ex As Exception
            End Try
            'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - End

            If specialsEnabled Then

                'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - Start
                'specialsCampaign = CStrEx(getSingleValueDB("SELECT SpecialsCampaign FROM EESetup"))
                Try
                    specialsCampaign = CStrEx(pageobj.eis.getEnterpriseConfigurationValue("SpecialsCampaign"))
                Catch ex As Exception
                End Try
                'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - End

                If AppSettings("REQUIRE_CATALOG_ENTRY") Then
                    sql = "SELECT A.ProductGuid FROM Attain_ProductPrice A INNER JOIN (SELECT DISTINCT " & _
                        "GroupProduct.ProductGuid FROM GroupProduct) AS GP ON A.ProductGuid=GP.ProductGuid "
                Else
                    sql = "SELECT ProductGuid FROM Attain_ProductPrice A "
                End If

                sql &= "WHERE CustomerRelType = 3 AND CustomerRelGuid = " & SafeString(specialsCampaign) & " AND " & _
                    "(StartingDate <= " & SafeDatetime(DateValue(Now)) & " OR StartingDate IS NULL) AND " & _
                    "(EndingDate >= " & SafeDatetime(DateValue(Now)) & " OR EndingDate IS NULL) AND "

                sql &= "(CurrencyGuid = " & SafeString(HttpContext.Current.Session("UserCurrencyGuid").ToString) & " OR "

                sql &= "CurrencyGuid IS NULL OR CurrencyGuid = '' ) AND A.ProductGuid=" & SafeString(productGuid)
                If CStrEx(getSingleValueDB(sql)) <> "" Then
                    Return True
                End If
            End If

            Return False

        End Function
        'AM2010051801 - SPECIAL ITEMS - End

        'AM2010051802 - WHAT's NEW - Start
        Private Shared Function IsNew(ByVal productGuid As String)
            Dim sql As String
            Dim useDate As Boolean = False
            Dim daysBack As Integer = 0

            If AppSettings("REQUIRE_CATALOG_ENTRY") Then
                sql = "SELECT ProductTable.ProductGuid FROM ProductTable INNER JOIN (SELECT DISTINCT " & _
                    "GroupProduct.ProductGuid FROM GroupProduct) AS GP ON ProductTable.ProductGuid=GP.ProductGuid"
            Else
                sql = "SELECT ProductGuid FROM ProductTable"
            End If

            'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - Start
            Dim pageobj As ExpandIT.Page = CType(System.Web.HttpContext.Current.Handler, ExpandIT.Page)
            'useDate = CBoolEx(getSingleValueDB("SELECT NewItemUseDate FROM EESetup"))
            Try
                useDate = CBoolEx(pageobj.eis.getEnterpriseConfigurationValue("NewItemUseDate"))
            Catch ex As Exception
            End Try
            'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - End

            If useDate Then
                'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - Start
                'daysBack = CIntEx(getSingleValueDB("SELECT NewItemDaysBack FROM EESetup"))
                Try
                    daysBack = CBoolEx(pageobj.eis.getEnterpriseConfigurationValue("NewItemDaysBack"))
                Catch ex As Exception
                End Try
                'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - End
                sql &= " WHERE DateAdded IS NOT NULL AND DATEDIFF(day,DateAdded,GETDATE()) <= " & daysBack
            Else
                sql = " WHERE NewItem='True'"
            End If
            sql &= " AND ProductTable.ProductGuid=" & SafeString(productGuid)
            If CStrEx(getSingleValueDB(sql)) <> "" Then
                Return True
            End If

            Return False
        End Function
        'AM2010051801 - SPECIAL ITEMS - Start
        Public Shared Function LastName(ByVal productGuid As String) As String
            Dim isNewText As String = ""
            Dim isSpecialText As String = ""
            Dim lastNameText As String = ""

            If IsNew(productGuid) Then
                isNewText = Resources.Language.LABEL_ISNEW
            End If
            If IsSpecial(productGuid) Then
                isSpecialText = Resources.Language.LABEL_ISSPECIAL
            End If

            lastNameText = isNewText & " " & isSpecialText

            Return lastNameText.Trim()
        End Function
        'AM2010051801 - SPECIAL ITEMS - End
        'AM2010051802 - WHAT's NEW - End

        Private Sub IntLoadVariants(ByVal products As ExpDictionary, ByVal LanguageGuid As Object, ByVal ExternalLanguageGuid As Object)
            Dim i As Integer
            Dim Sql As String
            Dim product, prdlist, productarray, item, variantlists, variantlist, productguid, variantdict As Object

            ' Exit function no products were specified
            If products.Count = 0 Then
                Exit Sub
            End If

            ' Make a product list
            prdlist = ""
            productarray = New Object() {}
            i = 0
            For Each item In products.Values
                productguid = CStrEx(item("ProductGuid"))
                If productguid <> "" Then
                    prdlist = prdlist & ", " & SafeString(productguid)
                    ReDim Preserve productarray(i)
                    productarray(i) = productguid
                    i = i + 1
                End If
            Next
            prdlist = Mid(prdlist, 3)
            If prdlist = "" Then Exit Sub

            ' Load variants
            'AM2010021901 - VARIANTS - Start
            If CBoolEx(AppSettings("EXPANDIT_US_USE_VARIANTS")) Then
                'AM2010021901 - VARIANTS - End
                If CBoolEx(AppSettings("USE_PRODUCTVARIANTTRANSLATION")) Then
                    Sql = "SELECT ProductVariant.*, PT.ProductName AS [VariantNameTranslation] FROM ProductVariant LEFT JOIN" & _
                     " (SELECT * FROM ProductTranslation WHERE " & HttpContext.Current.Application("EXTERNAL_LANGUAGEGUID_SQL") & "=" & SafeString(ExternalLanguageGuid) & ") AS PT" & _
                     " ON ProductVariant.ProductGuid=PT.ProductGuid AND ProductVariant.VariantCode=PT.VariantCode" & _
                     " WHERE ProductVariant.ProductGuid IN (" & prdlist & ") ORDER BY ProductVariant.ProductGuid, ProductVariant.VariantCode"
                Else
                    Sql = "SELECT ProductVariant.*, ProductVariant.VariantName AS [VariantNameTranslation] FROM ProductVariant" & _
                     " WHERE ProductVariant.ProductGuid IN (" & prdlist & ") ORDER BY ProductVariant.ProductGuid, ProductVariant.VariantCode"
                End If
                variantlists = Sql2Dictionaries2(Sql, New Object() {"ProductGuid", "VariantCode"})
                For Each variantlist In variantlists.Values
                    For Each variantdict In variantlist.Values
                        variantdict.Remove("ProductGuid")
                        If CStrEx(variantdict("VariantNameTranslation")) <> "" Then variantdict("VariantName") = CStrEx(variantdict("VariantNameTranslation"))
                        variantdict.Remove("VariantNameTranslation")
                    Next
                Next

                ' Set variantlists for products
                For Each product In products.Values
                    productguid = product("ProductGuid")
                    If variantlists.Exists(SafeString(productguid)) Then
                        product("Variants") = variantlists(SafeString(productguid))
                    End If
                Next
            End If
        End Sub

        Public Shared Function IntLoadProperties(ByVal OwnerTypeGuid As Object, ByVal OwnerRefGuids As Object, ByVal Props As Object, ByVal LanguageGuid As Object, ByVal DefaultLanguageGuid As Object) As ExpDictionary
            Dim sql As String
            Dim i As Integer
            Dim transdict As ExpDictionary
            Dim properties, key, d, a, shortkey As Object
            Dim item, refguid, lastrefguid, propdict As Object

            REM # 080708 Made changes to this function to increase SQL-query performance.
            'JA2010030901 - PERFORMANCE -Start
            If Not Props Is Nothing Then
                'JA2010030901 - PERFORMANCE -End
                'AM2010071901 - ENTERPRISE CSC - Start
                Dim pageobj As ExpandIT.Page = CType(System.Web.HttpContext.Current.Handler, ExpandIT.Page)
                sql = "SELECT PropOwnerRefGuid, PropGuid, PropLangGuid, PropTransText FROM PropVal INNER JOIN PropTrans" & _
                    " ON PropVal.PropValGuid=PropTrans.PropValGuid" & _
                        " WHERE PropVal.CatalogNo=" & SafeString(pageobj.globals.User("CatalogNo")) & " AND PropOwnerTypeGuid=" & SafeString(OwnerTypeGuid)
                'AM2010071901 - ENTERPRISE CSC - End
                'JA2010030901 - PERFORMANCE -Start
                'Only check language if multilanguage is enabled on the website
                If CBoolEx(AppSettings("MultiLanguageEnabled")) Then
                    sql &= " AND (PropLangGuid IS NULL" & _
                " OR PropLangGuid=" & SafeString(LanguageGuid) & _
                " OR PropLangGuid=" & SafeString(DefaultLanguageGuid) & _
                ")"
                End If
                'JA2010030901 - PERFORMANCE -End

                ' Select only the items specified in the ItemGuids parameter.
                ' If this is not specified all items are selected.
                If OwnerRefGuids IsNot Nothing Then
                    sql = sql & " AND PropOwnerRefGuid IN ("
                    For i = 0 To UBound(OwnerRefGuids)
                        If i > 0 Then sql = sql & ","
                        sql = sql & SafeString(OwnerRefGuids(i))
                    Next
                    sql = sql & ")"
                End If

                ' Select only the properties specified in the Props parameter.
                ' If this is not specified all properties are selected.
                If Not IsNull(Props) And Not Props Is Nothing Then
                    sql = sql & " AND PropGuid IN ("
                    For i = 0 To UBound(Props)
                        If i > 0 Then sql = sql & ","
                        sql = sql & SafeString(Props(i))
                    Next
                    sql = sql & ")"
                End If

                ' Order the result
                sql = sql & " ORDER BY PropOwnerRefGuid, PropGuid"

                'Force execution plan
                sql = sql & " OPTION(HASH JOIN)"

                If LanguageGuid = DefaultLanguageGuid Then
                    properties = Sql2Dictionaries(sql, New Object() {"PropOwnerRefGuid", "PropGuid"})
                Else
                    transdict = Sql2Dictionaries(sql, New Object() {"PropOwnerRefGuid", "PropGuid", "PropLangGuid"})

                    ' Pick the correct translations
                    properties = New ExpDictionary()
                    For Each key In transdict.Keys
                        d = transdict(key)
                        a = Split(key, ",")
                        a(UBound(a)) = ""
                        shortkey = Join(a, ",")
                        shortkey = Mid(shortkey, 1, Len(shortkey) - 1)
                        If properties(shortkey) Is Nothing Or CStrEx(d("PropLangGuid")) = LanguageGuid Then
                            properties(shortkey) = d
                        End If
                    Next
                End If
                'JA2010030901 - PERFORMANCE -Start
            Else
                properties = New ExpDictionary()
            End If
            'JA2010030901 - PERFORMANCE -End

            ' Collect the properties for each owner
            Dim retv As New ExpDictionary()
            lastrefguid = ""
            propdict = Nothing

            For Each item In properties.Values
                If Not item("PropTransText").Equals(DBNull.Value) Then
                    refguid = item("PropOwnerRefGuid")
                    If refguid <> lastrefguid Then
                        propdict = New ExpDictionary()
                        propdict.Add("PropOwnerRefGuid", refguid)
                        retv.Add(refguid, propdict)
                    End If
                    propdict(item("PropGuid")) = item("PropTransText")
                    lastrefguid = refguid
                End If
            Next

            IntLoadProperties = retv
        End Function
#End Region
#Region "lib_company"

        ''' <summary>
        ''' Returns company information as HTML
        ''' </summary>
        ''' <param name="CompanyGuid">Company Guid</param>
        ''' <returns>Returns HTML rendered version of the company.</returns>
        ''' <remarks></remarks>
        Public Function RenderCompanyInformation(ByVal CompanyGuid As String) As String
            Dim retv As String = ""
            Dim dictCompany As Object

            dictCompany = GetCompanyInformation(CompanyGuid)
            If dictCompany.Count > 0 Then
                retv = retv & dictCompany("CompanyName") & "<br />"
                retv = retv & dictCompany("Address1") & "<br />"
                If CStrEx(dictCompany("Address2")) <> "" Then retv = retv & dictCompany("Address2") & "<br />"
                retv = retv & dictCompany("ZipCode") & "&nbsp;" & dictCompany("CityName") & "<br />"
                retv = retv & dictCompany("CountryName") & "<br />"
                retv = retv & Resources.Language.LABEL_EMAIL & ": <a href=""mailto:" & dictCompany("EmailAddress") & """>" & dictCompany("EmailAddress") & "</a><br />"
                retv = retv & Resources.Language.LABEL_HOMEPAGE & ": <a href=""" & dictCompany("HomePage") & """>" & dictCompany("HomePage") & "</a><br />"
                retv = retv & Resources.Language.LABEL_PHONENO & ": " & dictCompany("PhoneNo") & "<br />"
                retv = retv & Resources.Language.LABEL_FAXNO & ": " & dictCompany("FaxNo") & "<br />"
                retv = retv & Resources.Language.LABEL_PUBLIC_ID & ": " & dictCompany("PublicId") & "<br />"
            End If
            RenderCompanyInformation = retv
        End Function

        ''' <summary>
        ''' Returns company information as Text
        ''' </summary>
        ''' <param name="CompanyGuid">Company Guid</param>
        ''' <returns>Returns Text rendered version of the company.</returns>
        ''' <remarks></remarks>
        Public Function RenderCompanyInformationText(ByVal CompanyGuid As String) As String
            Dim retv As String = ""
            Dim dictCompany As Object

            dictCompany = GetCompanyInformation(CompanyGuid)
            If dictCompany.Count > 0 Then
                retv = retv & dictCompany("CompanyName") & vbCrLf
                retv = retv & dictCompany("Address1") & vbCrLf
                If CStrEx(dictCompany("Address2")) <> "" Then retv = retv & dictCompany("Address2") & vbCrLf
                retv = retv & dictCompany("ZipCode") & " " & dictCompany("CityName") & vbCrLf
                retv = retv & dictCompany("CountryName") & vbCrLf
                retv = retv & Resources.Language.LABEL_EMAIL & ": " & dictCompany("EmailAddress") & vbCrLf
                retv = retv & Resources.Language.LABEL_HOMEPAGE & ": " & dictCompany("HomePage") & vbCrLf
                retv = retv & Resources.Language.LABEL_PHONENO & ": " & dictCompany("PhoneNo") & vbCrLf
                retv = retv & Resources.Language.LABEL_FAXNO & ": " & dictCompany("FaxNo") & vbCrLf
                retv = retv & Resources.Language.LABEL_PUBLIC_ID & ": " & dictCompany("PublicId") & vbCrLf
            End If
            RenderCompanyInformationText = retv
        End Function

        ''' <summary>
        ''' Returns company information as dictionary.
        ''' </summary>
        ''' <param name="CompanyGuid">Company Guid</param>
        ''' <returns></returns>
        ''' <remarks>Uses caching.</remarks>
        Public Function GetCompanyInformation(ByVal CompanyGuid As String) As ExpDictionary
            Dim sql As String
            Dim retv As ExpDictionary = Nothing

            ' Caching
            Dim cachekey As String = "COMPANYINFORMATION_" & CompanyGuid
            If CStrEx(CompanyGuid) = "1" Then
                retv = CacheGet(cachekey)
            End If

            If retv Is Nothing Then
                sql = "SELECT * FROM CompanyTable WHERE CompanyGuid = " & SafeString(CompanyGuid)
                retv = Sql2Dictionary(sql)
                CacheSet(cachekey, retv, "CompanyTable")
            End If

            GetCompanyInformation = retv
        End Function

#End Region
#Region "lib_country"

        ''' <summary>
        ''' This function gets two dictionaries with names and values for displaying a dropdown list of available
        ''' countries. The function is used on the user_new and user pages.
        ''' </summary>
        ''' <param name="dictCountryGuids">A dictionary to hold the country values.</param>
        ''' <param name="dictCountryNames">A dictionary to hold the country names.</param>
        ''' <remarks>Please see the user_new.vb and user.vb pages for an example of usage of this function.</remarks>
        Public Shared Sub GetCountries(ByRef dictCountryGuids As ExpDictionary, ByRef dictCountryNames As ExpDictionary)
            Dim dtCountryTable As DataTable

            If HttpContext.Current.Application("CountryNames") Is Nothing Then
                dtCountryTable = SQL2DataTable("SELECT CountryName, CountryGuid FROM CountryTable ORDER BY CountryName")
                dictCountryGuids = New ExpDictionary
                dictCountryNames = New ExpDictionary
                For Each row As DataRow In dtCountryTable.Rows
                    dictCountryGuids(CStrEx(row("CountryName"))) = row("CountryGuid")
                    dictCountryNames(CStrEx(row("CountryGuid"))) = row("CountryName")
                Next

                ' Cache the values
                HttpContext.Current.Application.Lock()
                HttpContext.Current.Application("CountryGuids") = dictCountryGuids
                HttpContext.Current.Application("CountryNames") = dictCountryNames
                HttpContext.Current.Application.UnLock()
            Else
                dictCountryGuids = HttpContext.Current.Application("CountryGuids")
                dictCountryNames = HttpContext.Current.Application("CountryNames")
            End If
        End Sub

        ''' <summary>
        ''' Get the name of a country by specifying the country code.
        ''' </summary>
        ''' <param name="CountryGuid">A code representing a country.</param>
        ''' <returns></returns>
        ''' <remarks>This function is the reverse of GetCountryGuid.</remarks>
        Public Shared Function GetCountryName(ByVal CountryGuid As String) As String
            Dim names As ExpDictionary = Nothing
            Dim guids As ExpDictionary = Nothing

            GetCountries(guids, names)
            Return CStrEx(names(CountryGuid))
        End Function

        ''' <summary>
        ''' Get the code of a country by specifying the country name.
        ''' </summary>
        ''' <param name="CountryName">The name of the country to lookup.</param>
        ''' <returns></returns>
        ''' <remarks>This function is the reverse of GetCountryName.</remarks>
        Public Shared Function GetCountryGuid(ByVal CountryName As String) As String
            Dim names As ExpDictionary = Nothing
            Dim guids As ExpDictionary = Nothing

            GetCountries(guids, names)
            Return CStrEx(guids(CountryName))
        End Function

        ''' <summary>
        ''' Loads all the availale countries in the shop.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks>Uses caching.</remarks>
        Public Function LoadCountries() As ExpDictionary
            Dim sql As String
            Dim retv As ExpDictionary = Nothing

            retv = CacheGet("SiteCountries")
            If retv Is Nothing Then
                sql = "SELECT CountryName, CountryGuid FROM CountryTable ORDER BY CountryName"
                retv = SQL2Dicts(sql, "CountryGuid", -1)
                CacheSet("SiteCountries", retv, "CountryTable")
            End If

            Return retv
        End Function
#End Region
#Region "lib_currency"

        ''' <summary>
        ''' This function returns true or false depending on if the user is using secondary currency or not.
        ''' </summary>
        ''' <param name="User">Dictionary containing the user information.</param>
        ''' <returns></returns>
        ''' <remarks>Returns a Boolean specifying if the user is setup for using secondary currency.</remarks>
        Public Function UseSecondaryCurrency(ByVal User As Object) As Boolean
            ' Only use secondary currency for the user if SecondaryCurrencyGuid <> "" and SecondaryCurrencyGuid <> CurrencyGuid and
            ' Multiple currency is enabled.
            If globals.bUseMultiCurrency And (HttpContext.Current.Session("UserSecondaryCurrencyGuid").ToString <> "") And (CStrEx(globals.User("CurrencyGuid")) <> HttpContext.Current.Session("UserSecondaryCurrencyGuid").ToString) Then
                Return True
            End If
        End Function

        Public Sub ConvertAmounts(ByVal dicts As ExpDictionary, ByVal keyarray As Object, _
                ByVal fromcurrency As Object, ByVal tocurrency As Object)

            Dim i As Integer
            Dim rate, subdict, key, lastcurrency, localcurrency As Object

            ' Valid source currency
            fromcurrency = CStrEx(fromcurrency)

            If fromcurrency = "" Then
                fromcurrency = AppSettings("MULTICURRENCY_SITE_CURRENCY")
            End If

            ' Validate target currency
            tocurrency = CStrEx(tocurrency)
            If tocurrency = "" Then tocurrency = fromcurrency

            If globals.MulticurrencyEnabled Then
                ' Get exchange rate
                If fromcurrency = tocurrency Then
                    rate = 1
                Else
                    rate = currency.GetExchangeFactor(fromcurrency, tocurrency)
                End If
            Else
                rate = 1
            End If

            lastcurrency = ""
            For Each subdict In dicts.Values
                ' Get currency for this particular product
                localcurrency = fromcurrency
                If subdict.Exists("CurrencyGuid") Then
                    localcurrency = CStrEx(subdict("CurrencyGuid"))
                End If

                If CStrEx(localcurrency) = "" Then
                    localcurrency = AppSettings("MULTICURRENCY_SITE_CURRENCY")
                End If

                If globals.MulticurrencyEnabled Then
                    ' Check if currency has changed
                    If localcurrency <> lastcurrency Then
                        ' Update exchange rate
                        If localcurrency = tocurrency Then
                            rate = 1
                        Else
                            rate = currency.GetExchangeFactor(localcurrency, tocurrency)
                        End If

                        ' Update last currency
                        lastcurrency = localcurrency
                    End If
                End If

                For i = 0 To UBound(keyarray)
                    key = keyarray(i)
                    If subdict.Exists(key) Then
                        subdict(key & "_" & tocurrency) = CDblEx(subdict(key)) * rate
                    End If
                Next
            Next
        End Sub
#End Region
#Region "lib_display"

        ''' <summary>
        ''' Changes line breaks in a formatted text to HTML.
        ''' </summary>
        ''' <param name="s">The text to format.</param>
        ''' <returns>A formatted text.</returns>
        ''' <remarks>Carriage returns are formatted as line breaks in HTLM. The formatted text is returned as string.</remarks>
        Public Shared Function FormatText(ByVal s As String) As String
            Dim retv As String

            If s = "" Or s Is Nothing Then
                retv = ""
            Else
                retv = s
                retv = retv.Replace(vbCrLf, Chr(10))
                retv = retv.Replace(Chr(10), "<br />" & vbCrLf)
            End If
            Return retv
        End Function

        ''' <summary>
        ''' This function initiates the StyleSheetRowCounter variable to 0. See the NextRowAB function
        ''' for a description of usage.
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub InitNextRowAB()
            globals.StyleSheetRowCounter = 0
        End Sub

        ''' <summary>
        ''' This function is used to control layout of a table with alternating row styles.
        ''' The function changes the StyleSheetRowCounter variable to 0 if 1 and 1 if 0. Use
        ''' this to switch between two alternating styles e.g. MyStyle1 and MyStyle2
        ''' </summary>
        ''' <remarks>Returns nothing. Changes the StyleSheetRowCounter variable.</remarks>
        Public Sub NextRowAB()
            globals.StyleSheetRowCounter = (globals.StyleSheetRowCounter + 1) Mod 2
        End Sub

        ''' <summary>
        ''' This function inserts a HTML 'img' element displaying the specified image file. The file name is relative to the
        ''' shop root directory.
        ''' </summary>
        ''' <param name="picture">The picture file path relative to the shop root directory.</param>
        ''' <param name="additionaltags">A string specifying additional tags for the IMG HTML element.</param>
        ''' <returns></returns>
        ''' <remarks>
        ''' Returns a string containing the HTML code for the image to be displayed on the page. <br />
        ''' This function works like the PictureTagImagesDir function except it expects the image file to be placed relative to the
        ''' root of the Shop directory.
        ''' For an example see the product.aspx template.
        ''' </remarks>
        Public Shared Function PictureTag(ByVal picture As Object, ByVal additionaltags As String) As String
            picture = CStrEx(picture)
            If Not InStr(additionaltags, "alt=") = 0 Then additionaltags &= " alt=""+"""
            If picture <> "" Then
                Return "<img src=""" & HttpContext.Current.Server.UrlPathEncode(VRoot & "/" & picture) & """ " & additionaltags & " />"
            End If
            Return ""
        End Function

        ''' <summary>
        ''' This function is part one of two functions, which generates the catalog tree on the website.
        ''' </summary>
        ''' <param name="groupguid">Current position in the catalog tree.</param>
        ''' <remarks></remarks>
        Public Sub InitCatalogStructure(ByVal groupguid As String)
            Dim catstructure As ExpDictionary
            Dim info As ExpDictionary
            Dim groupinfo As ExpDictionary
            Dim html, sql As String
            Dim defaultgroup As ExpDictionary
            Dim groups As ExpDictionary
            Dim html_cachekey As String
            Dim bct_cachekey As String
            Dim bct As BreadcrumbTrail

            ' Validate group guid
            groupguid = CLngEx(groupguid)

            ' Bail out if already set
            If globals.catalogTreeHTML IsNot Nothing And globals.breadcrumbTrail IsNot Nothing Then Return
            'AM2010071901 - ENTERPRISE CSC - Start
            'JA2010030901 - PERFORMANCE -Start
            html_cachekey = "TreeHTML-"
            If Not CBoolEx(AppSettings("SHOW_DYNAMIC_CATALOG")) Then
                html_cachekey &= groupguid & "-"
            End If
            html_cachekey &= HttpContext.Current.Session("LanguageGuid").ToString & "-" & globals.User("CatalogNo")
            'JA2010030901 - PERFORMANCE -End
            bct_cachekey = "TreeBCT-" & groupguid & "-" & HttpContext.Current.Session("LanguageGuid").ToString & "-" & globals.User("CatalogNo")
            'AM2010071901 - ENTERPRISE CSC - End
            '*****CUSTOMER SPECIFIC CATALOGS***** -START
            'ExpandIT US - USE CSC - Start
            If CBoolEx(AppSettings("EXPANDIT_US_USE_CSC")) Then
                html_cachekey &= "-" & CStrEx(globals.User("CustomerGuid"))
                bct_cachekey &= "-" & CStrEx(globals.User("CustomerGuid"))
            End If
            'ExpandIT US - USE CSC - End
            '*****CUSTOMER SPECIFIC CATALOGS***** -END
            'JA2010030901 - PERFORMANCE -Start
            bct = Nothing
            html = Nothing
            'JA2010030901 - PERFORMANCE -End
            html = CacheGet(html_cachekey)
            bct = CacheGet(bct_cachekey)
            If html Is Nothing Or bct Is Nothing Then
                'JA2010030901 - PERFORMANCE -Start
                If html Is Nothing Then
                    'JA2010030901 - PERFORMANCE -End
                    ' Load the default group if no group was specified
                    If groupguid = 0 Then
                        'ExpandIT US - USE CSC - Start
                        'AM2010071901 - ENTERPRISE CSC - Start
                        sql = "SELECT TOP 2 GroupGuid FROM GroupTable WHERE ParentGuid=0 AND CatalogNo=" & SafeString(globals.User("CatalogNo"))
                        'sql = "SELECT TOP 2 GroupGuid FROM GroupTable WHERE ParentGuid=0  ORDER BY SortIndex"
                        'AM2010071901 - ENTERPRISE CSC - End
                        'JA2010030901 - PERFORMANCE -Start
                        'If CBoolEx(AppSettings("EXPANDIT_US_USE_CSC")) Then
                        '    If CBoolEx(globals.User("B2B")) Then
                        '        Dim groupsList As String
                        '        groupsList = CStrEx(obj.groupsWithProducts(0))
                        '        If groupsList <> "" Then
                        '            sql &= " AND GroupGuid IN (" & groupsList & ")"
                        '        Else
                        '            sql &= " AND 1=2"
                        '        End If
                        '    End If
                        'End If
                        'JA2010030901 - PERFORMANCE -End
                        sql &= " ORDER BY SortIndex"
                        'ExpandIT US - USE CSC - End

                        'groups = Sql2Dictionaries(sql, "GroupGuid")

                        '' Only set a default group if there is only one top level
                        'If groups.Count = 1 Then
                        '    defaultgroup = GetFirst(groups).Value
                        '    groupguid = defaultgroup("GroupGuid")
                        'End If
                        'JA2010030901 - PERFORMANCE -End
                    End If

                    html = ""
                    'JA2010030901 - PERFORMANCE -Start
                End If
                'JA2010030901 - PERFORMANCE -End
                bct = New BreadcrumbTrail
                groupinfo = New ExpDictionary()
                groupinfo("PropertyArray") = New String() {"NAME"}
                info = CatDefaultInfo()
                info("General")("CalcPrices") = False
                info("GroupInfo") = groupinfo

                If AppSettings("SHOW_DYNAMIC_CATALOG") Then
                    catstructure = LoadCatalogStructureDynamic("0", 0, Nothing, info, html, True, bct)
                Else
                    catstructure = LoadCatalogStructure(groupguid, 0, Nothing, info, html, True, bct)
                End If

                Dim tableNames As String() = Split(AppSettings("GroupProductRelatedTablesForCaching"), "|")
                CacheSetAggregated(html_cachekey, html, tableNames)
                CacheSetAggregated(bct_cachekey, bct, tableNames)
            End If

            globals.catalogTreeHTML = html
            globals.breadcrumbTrail = bct.Clone

        End Sub

        Public Function LoadCatalogStructure(ByVal groupguid As String, ByVal level As Integer, ByVal subgroups As ExpDictionary, ByVal info As ExpDictionary, ByRef html As String, ByVal bFirstCall As Boolean, ByVal bct As BreadcrumbTrail) As ExpDictionary
            Dim sql As String
            Dim group As ExpDictionary = Nothing
            Dim groups As ExpDictionary
            Dim parentguid As String
            Dim catstructure As ExpDictionary
            Dim strhtml As String

            ' Load subgroup if we are at the first level


            'JA2010030901 - PERFORMANCE -Start
            ''AM2010071901 - ENTERPRISE CSC - Start
            'If bFirstCall Then
            '    sql = "SELECT GroupGuid, ParentGuid FROM GroupTable WHERE ParentGuid=" & groupguid & " AND CatalogNo=" & SafeString(globals.User("CatalogNo")) & " ORDER BY SortIndex"
            'Else
            '    ' Get groups in the same group as this group
            '    sql = "SELECT GroupGuid, ParentGuid FROM GroupTable WHERE ParentGuid IN (SELECT ParentGuid FROM GroupTable WHERE GroupGuid=" & groupguid & ") AND CatalogNo=" & SafeString(globals.User("CatalogNo")) & " ORDER BY SortIndex"
            'End If
            ''AM2010071901 - ENTERPRISE CSC - End
            '*****CUSTOMER SPECIFIC CATALOGS*****-START
            If CBoolEx(AppSettings("EXPANDIT_US_USE_CSC")) Then
                Dim parents As String = ""
                If bFirstCall Then
                    parents = SafeString(groupguid)
                Else
                    ' Get groups in the same group as this group
                    parents = "SELECT ParentGuid FROM GroupTable WHERE GroupGuid=" & groupguid & ")"
                End If
                sql = obj.groupsWithProductsSQL(parents)
            Else
                If bFirstCall Then
                    sql = "SELECT GroupGuid, ParentGuid FROM GroupTable WHERE ParentGuid=" & groupguid & " AND CatalogNo=" & SafeString(globals.User("CatalogNo")) & " ORDER BY SortIndex"
                Else
                    ' Get groups in the same group as this group
                    sql = "SELECT GroupGuid, ParentGuid FROM GroupTable WHERE ParentGuid IN (SELECT ParentGuid FROM GroupTable WHERE GroupGuid=" & groupguid & ") AND CatalogNo=" & SafeString(globals.User("CatalogNo")) & " ORDER BY SortIndex"
                End If
            End If
            '*****CUSTOMER SPECIFIC CATALOGS*****-END
            'JA2010030901 - PERFORMANCE -End

            groups = Sql2Dictionaries(sql, New Object() {"GroupGuid"})
            If groups.Count = 0 And Not bFirstCall Then
                LoadCatalogStructure = Nothing
                Exit Function
            End If

            ' Get the current group
            If Not bFirstCall Then
                group = groups(SafeString(groupguid))
                group("Selected") = True
                parentguid = group("ParentGuid")
            Else
                parentguid = groupguid
            End If

            ' Set the subgroups for the current group
            If Not subgroups Is Nothing Then
                group("Subgroups") = subgroups
            End If

            ' Load properties
            info("GroupInfo")("Groups") = groups
            CatLoadGroups(info)

            ' Render HTML
            ' Do not show the top level if there is only one catalog
            If parentguid <> 0 Or groups.Count > 1 Then
                strhtml = ""
                Dim nodetype As String
                Dim padding As Integer = 0
                Dim previousGroup As String = ""
                For Each group In groups.Values
                    strhtml = strhtml & "<table class=""boxText"" Style=""width:100%;"" border='0' cellspacing='0' cellpadding='0' >"
                    If group("ParentGuid") = "1" Then
                        strhtml = strhtml & "<tr><td colspan='2' align='right' valign='top'>" & PictureTag("App_Themes/Theme1/spacer.gif", "width='11' height='5' ") & "</td></tr>"
                    End If
                    Dim sLink : sLink = ""
                    Dim sParentLink
                    If group.Exists("Selected") Then
                        If group("ParentGuid") = 0 Then
                            sParentLink = VRoot & "/shop.aspx"
                        Else
                            sParentLink = GroupLink(group("ParentGuid"))
                        End If
                    Else
                        sParentLink = GroupLink(group("GroupGuid"))
                    End If
                    sLink = GroupLink(group("GroupGuid"))
                    'End If


                    padding = CIntEx(NumberOfParents(group("GroupGuid"))) * 14

                    strhtml = strhtml & "<TR><TD ALIGN='RIGHT' Style=""width:26px; height:10px; vertical-align:middle; padding-left:" & padding & "px;""><a  href=""" & sParentLink & """>"


                    If group.Exists("Selected") Then
                        If CStrEx(AppSettings("MasterPageFolder")).Contains("Enterprise") Then
                            strhtml = strhtml & PictureTag("App_Themes/EnterpriseBlue/bg_list2.gif", "border=0")
                        Else
                            strhtml = strhtml & PictureTag("App_Themes/EnterpriseBlue/bg_list.gif", "border=0")
                        End If
                        ' Add to breadcrumb trail
                        Dim bc As New Breadcrumb
                        bc.Caption = CStrEx(group("NAME"))
                        bc.URL = GroupLink(group("GroupGuid"))
                        bct.Insert(0, bc)
                    Else
                        If CStrEx(AppSettings("MasterPageFolder")).Contains("Enterprise") Then
                            strhtml = strhtml & PictureTag("App_Themes/EnterpriseBlue/bg_list2.gif", "border=0")
                        Else
                            strhtml = strhtml & PictureTag("App_Themes/EnterpriseBlue/bg_list.gif", "border=0")
                        End If
                    End If

                    strhtml = strhtml & "</a></TD>"
                    strhtml = strhtml & "<TD ALIGN='LEFT' STYLE=""padding-left:8px; vertical-align:top; height:10px;"" ><a class=""CatalogText"" href=""" & sLink & """>"


                    If CStrEx(group("NAME")) = "" Then
                        strhtml = strhtml & "<font color=red>[" & Resources.Language.MESSAGE_MISSING_PROPERTY & "]</font><br/>"
                    Else
                        If level = 0 And group.Exists("Selected") Then
                            strhtml = strhtml & "<B>"
                        End If
                        strhtml = strhtml & HTMLEncode(group("NAME")) & "<br/>"
                        If level = 0 And group.Exists("Selected") Then
                            strhtml = strhtml & "</B>"
                        End If
                    End If

                    strhtml = strhtml & "</a></TD></TR><tr style=""height:3px; background-color:#C7C7C7;""></tr>"


                    strhtml = strhtml & "</TABLE>"
                    If group.Exists("Selected") Then
                        'strhtml = strhtml & "<div style=""MARGIN-LEFT: 8px;"">" & html & "</div>"
                        strhtml = strhtml & html
                    End If
                    previousGroup = group("GroupGuid")
                Next
                html = strhtml

            End If

            REM -- Load the parent group
            If parentguid <> 0 Then
                catstructure = LoadCatalogStructure(parentguid, level + IIf(bFirstCall, 0, 1), groups, info, html, False, bct)
            Else
                catstructure = groups
            End If
            LoadCatalogStructure = catstructure
        End Function

        Public Function LoadCatalogStructureDynamic(ByVal groupguid As String, ByVal level As Integer, ByVal subgroups As ExpDictionary, ByVal info As ExpDictionary, ByRef html As String, ByVal bFirstCall As Boolean, ByVal bct As BreadcrumbTrail) As ExpDictionary
            Dim sql As String
            Dim group As ExpDictionary = Nothing
            Dim groups As ExpDictionary
            Dim parentguid As String
            Dim catstructure As ExpDictionary
            Dim strhtml As String
            'JA2010030901 - PERFORMANCE -Start
            Dim htmlLoaded As Boolean = False

            ''Load all the Catalog
            ''AM2010071901 - ENTERPRISE CSC - Start
            'sql = "SELECT GroupGuid, ParentGuid FROM GroupTable WHERE CatalogNo=" & SafeString(globals.User("CatalogNo")) & " ORDER BY SortIndex"
            ''sql = "SELECT GroupGuid, ParentGuid FROM GroupTable  ORDER BY SortIndex"
            ''AM2010071901 - ENTERPRISE CSC - End

            'groups = Sql2Dictionaries(sql, New Object() {"GroupGuid"})
            'If groups.Count = 0 And Not bFirstCall Then
            '    LoadCatalogStructureDinamic = Nothing
            '    Exit Function
            'End If
            If Not globals.eis.CheckPageAccess("Catalog") Then
                strhtml = ""
                strhtml = strhtml & "<div id=""menu""><ul id=""toplevel"">"
                strhtml = strhtml & "</ul></div>"
                html = strhtml
                groups = New ExpDictionary
                htmlLoaded = True
            Else
                Dim strDynamicCacheKey As String = "DynamicGroupReturn-" & SafeString(globals.User("CatalogNo"))
                '*****CUSTOMER SPECIFIC CATALOGS*****-START
                If CBoolEx(AppSettings("EXPANDIT_US_USE_CSC")) Then
                    strDynamicCacheKey &= "-" & CStrEx(globals.User("CustomerGuid"))
                End If
                '*****CUSTOMER SPECIFIC CATALOGS*****-END
                Dim onlyGroupIsParent As Boolean = True
                groups = CacheGet(strDynamicCacheKey)
                If groups Is Nothing Then
                    '*****CUSTOMER SPECIFIC CATALOGS*****-START
                    Dim strCSCAvailableItemsSQL As String
                    If CBoolEx(AppSettings("EXPANDIT_US_USE_CSC")) Then
                        strCSCAvailableItemsSQL = obj.getListAvailableItems(globals.User)
                    Else
                        strCSCAvailableItemsSQL = "SELECT ProductGuid FROM ProductTable"
                    End If
                    'AM2010071901 - ENTERPRISE CSC - Start
                    sql = "EXEC EISECSCCatalogWithProducts @strCatalog=" & SafeString(globals.User("CatalogNo")) & ", @strCSCAvailableItemsSQL=" & SafeString(strCSCAvailableItemsSQL)
                    'AM2010071901 - ENTERPRISE CSC - End
                    groups = Sql2Dictionaries(sql, New Object() {"GroupGuid"})
                    If groups.Count = 0 And Not bFirstCall Then
                        LoadCatalogStructureDynamic = Nothing
                        Exit Function
                    End If
                    'JA2010030901 - PERFORMANCE -Start
                    'Dim tableNames As String() = {"GroupTable", "GroupProduct", "ProductTable"}
                    'CacheSetAggregated(strDynamicCacheKey, groups, tableNames)
                    '*****CUSTOMER SPECIFIC CATALOGS*****-END
                    'End If
                    ' Get the current group
                    If Not bFirstCall Then
                        group = groups(SafeString(groupguid))
                        group("Selected") = True
                        'parentguid = group("ParentGuid")
                        'Else
                        parentguid = groupguid
                    End If

                    ' Set the subgroups for the current group
                    'If Not subgroups Is Nothing Then
                    '    group("Subgroups") = subgroups
                    'End If

                    ' Load properties
                    info("GroupInfo")("Groups") = groups
                    CatLoadGroups(info)
                    Dim tableNames As String() = {"GroupTable", "GroupProduct", "ProductTable"}
                    CacheSetAggregated(strDynamicCacheKey, groups, tableNames)
                End If

                If html = "" Then
                    'JA2010030901 - PERFORMANCE -End
                    ' Render HTML
                    ' Do not show the top level if there is only one catalog
                    If groups.Count = 1 Then
                        For Each groupDict As ExpDictionary In groups.Values
                            onlyGroupIsParent = CBoolEx(groupDict("IsAParent"))
                        Next
                    End If
                    If groups.Count > 1 OrElse Not onlyGroupIsParent Then
                        strhtml = ""
                        Dim padding As Integer = 0
                        Dim previousGroup As String = ""
                        Dim previousPadding As Integer = 0
                        strhtml = strhtml & "<div id=""menu""><ul id=""toplevel"">"
                        Dim strKeyArray As Object()
                        strKeyArray = groups.DictKeys()

                        For intIndex As Integer = 0 To UBound(strKeyArray)
                            Dim sLink : sLink = ""
                            Dim sParentLink
                            If groups(strKeyArray(intIndex)).Exists("Selected") Then
                                If group("ParentGuid") = 0 Then
                                    sParentLink = VRoot & "/shop.aspx"
                                Else
                                    sParentLink = GroupLink(groups(strKeyArray(intIndex))("ParentGuid"))
                                End If
                            Else
                                sParentLink = GroupLink(groups(strKeyArray(intIndex))("GroupGuid"))
                            End If
                            sLink = GroupLink(groups(strKeyArray(intIndex))("GroupGuid"))

                            padding = CIntEx(groups(strKeyArray(intIndex))("NumberOfParents")) * 14

                            If previousPadding > padding Then
                                While previousPadding <> 0
                                    If previousPadding - 14 >= padding Then
                                        strhtml = strhtml & "</ul></li>"
                                    End If
                                    previousPadding = previousPadding - 14
                                End While
                            End If

                            If CBoolEx(groups(strKeyArray(intIndex))("IsAParent")) Then
                                strhtml = strhtml & "<li>"
                                strhtml = strhtml & "<a class=""fly""  href=""" & sParentLink & """>"
                                If CStrEx(groups(strKeyArray(intIndex))("NAME")) = "" Then
                                    strhtml = strhtml & "<font color=red>[" & Resources.Language.MESSAGE_MISSING_PROPERTY & "]</font>"
                                Else
                                    strhtml = strhtml & HTMLEncode(groups(strKeyArray(intIndex))("NAME"))
                                End If
                                strhtml = strhtml & "</a><ul>"
                            Else
                                strhtml = strhtml & "<li>"
                                strhtml = strhtml & "<a  href=""" & sParentLink & """>"
                                If CStrEx(groups(strKeyArray(intIndex))("NAME")) = "" Then
                                    strhtml = strhtml & "<font color=red>[" & Resources.Language.MESSAGE_MISSING_PROPERTY & "]</font>"
                                Else
                                    strhtml = strhtml & HTMLEncode(groups(strKeyArray(intIndex))("NAME"))
                                End If
                                strhtml = strhtml & "</a></li>"
                            End If

                            If groups(strKeyArray(intIndex)).Exists("Selected") Then
                                'strhtml = strhtml & "<div style=""MARGIN-LEFT: 8px;"">" & html & "</div>"
                                strhtml = strhtml & html
                            End If
                            previousGroup = groups(strKeyArray(intIndex))("GroupGuid")
                            previousPadding = padding
                        Next
                        strhtml = strhtml & "</ul></div>"
                        html = strhtml

                    End If
                Else
                    htmlLoaded = True
                End If
            End If

            If bct.Count = 0 Then
                'Add breadCrumb for the expanded catalog
                If Not groups(SafeString(CStrEx(GetRequest("GroupGuid")))) Is Nothing Then
                    Dim bread As String = groups(SafeString(CStrEx(GetRequest("GroupGuid"))))("NAME") & "^" & CStrEx(GetRequest("GroupGuid"))
                    Dim groupGuids As String = CStrEx(GetRequest("GroupGuid"))
                    Dim mysql As String
                    While groupGuids <> "0"
                        'AM2010071901 - ENTERPRISE CSC - Start
                        mysql = "SELECT * FROM GroupTable WHERE GroupGuid=" & SafeString(groupGuids) & " AND CatalogNo=" & SafeString(globals.User("CatalogNo"))
                        'mysql = "SELECT * FROM GroupTable WHERE GroupGuid=" & SafeString(groupGuids)
                        'AM2010071901 - ENTERPRISE CSC - End
                        Dim retval As ExpDictionary = SQL2Dicts(mysql, "GroupGuid")
                        If Not retval Is Nothing And Not retval Is DBNull.Value Then
                            If retval.Count > 0 Then
                                If CStrEx(retval(groupGuids)("ParentGuid")) <> "0" Then
                                    bread = bread & "*" & groups(SafeString(CStrEx(retval(groupGuids)("ParentGuid"))))("NAME") & "^" & CStrEx(retval(groupGuids)("ParentGuid"))
                                End If
                            End If
                        End If
                        groupGuids = CStrEx(retval(groupGuids)("ParentGuid"))
                    End While
                    If bread <> "" Then
                        Dim breads() As String = bread.Split("*")
                        Dim i As Integer = 0
                        For i = 0 To breads.Length - 1
                            Dim twoValues() As String = breads(i).Split("^")
                            ' Add to breadcrumb trail
                            Dim bc As New Breadcrumb
                            bc.Caption = CStrEx(twoValues(0))
                            bc.URL = GroupLink(twoValues(1))
                            bct.Insert(0, bc)
                        Next
                    End If
                End If
            End If

            If Not htmlLoaded Then

                REM -- Load the parent group
                If parentguid <> 0 Then
                    catstructure = LoadCatalogStructure(parentguid, level + IIf(bFirstCall, 0, 1), groups, info, html, False, bct)
                Else
                    catstructure = groups
                End If
            End If
            'JA2010030901 - PERFORMANCE -End
            LoadCatalogStructureDynamic = catstructure
        End Function

        Public Function NumberOfParents(ByVal strGroupGuid As String) As Integer
            Dim sql As String
            Dim number As Integer = 0
            'JA2010030901 - PERFORMANCE -Start
            Dim objCacheValue As Object = Nothing
            Dim strCacheKey As String = "NumberOfParents-" & strGroupGuid & "-" & SafeString(globals.User("CatalogNo"))
            If strCacheKey <> "" Then objCacheValue = EISClass.CacheGet(strCacheKey)
            If Not objCacheValue Is Nothing Then
                Return CIntEx(objCacheValue)
            Else
                While True
                    'AM2010071901 - ENTERPRISE CSC - Start
                    sql = "SELECT ParentGuid FROM GroupTable WHERE GroupGuid=" & SafeString(strGroupGuid) & " AND CatalogNo=" & SafeString(globals.User("CatalogNo"))
                    'sql = "SELECT ParentGuid FROM GroupTable WHERE GroupGuid=" & SafeString(nGGuid)
                    'AM2010071901 - ENTERPRISE CSC - End
                    strGroupGuid = getSingleValueDB(sql)
                    If strGroupGuid <> 1 Then
                        If (strGroupGuid = 0) Then Exit While
                        number += 1
                    Else
                        Exit While
                    End If
                End While
                If strCacheKey <> "" Then
                    EISClass.CacheSet(strCacheKey, number, "GroupTable")
                End If
            End If
            'JA2010030901 - PERFORMANCE -End
            Return number

        End Function

        Public Function IsParent(ByVal nGGuid) As Boolean
            Dim result As Boolean = False
            Dim sql As String
            'JA2010030901 - PERFORMANCE -Start
            Dim cachekey As String
            Dim cacheString As String

            cachekey = "IsParent-" & CStrEx(globals.User("CatalogNo")) & "-" & nGGuid & "-" & HttpContext.Current.Session("LanguageGuid").ToString
            '*****CUSTOMER SPECIFIC CATALOGS***** - START
            If CBoolEx(AppSettings("EXPANDIT_US_USE_CSC")) Or CBoolEx(AppSettings("EXPANDIT_US_USE_INCLUDES_EXCLUDES")) Then
                cachekey &= "-" & CStrEx(globals.User("CustomerGuid"))
            End If
            '*****CUSTOMER SPECIFIC CATALOGS***** - END
            If cachekey <> "" Then cacheString = EISClass.CacheGet(cachekey) Else cacheString = Nothing
            If CStrEx(cacheString) <> "" Then
                If cacheString = "True" Then
                    result = True
                End If
            Else
                sql = "SELECT * FROM GroupTable WHERE ParentGuid=" & SafeString(nGGuid) & " AND CatalogNo=" & SafeString(globals.User("CatalogNo"))
                Dim retval As ExpDictionary = SQL2Dicts(sql)
                If Not retval Is Nothing AndAlso retval.Count > 0 Then
                    result = True
                End If
                'JA2010030901 - PERFORMANCE -Start
                ' Set cache
                If cachekey <> "" Then
                    Dim tableNames As String() = Split(AppSettings("GroupProductRelatedTablesForCaching"), "|")
                    EISClass.CacheSetAggregated(cachekey, result.ToString(), tableNames)
                End If
                'JA2010030901 - PERFORMANCE -End
            End If
            Return result
        End Function

        Public Shared Function SizeText(ByVal str As String, ByVal width As Decimal) As String
            Dim d() As Decimal
            Dim w As Decimal = 0
            Dim retv As String
            Dim i As Integer

            d = New Decimal() {0.002, 0.542, 0.542, 0.542, 0.542, 0.542, 0.542, 0.542, 0.542, _
                0.542, 0, 0.542, 0.542, 0, 0.542, 0.542, 0.542, 0.542, 0.542, 0.542, _
                0.542, 0.542, 0.542, 0.542, 0.542, 0.542, 0.542, 0.542, 0.542, 0.542, _
                0.542, 0.542, 0.167, 0.25, 0.292, 0.458, 0.375, 0.708, 0.229, 0.167, _
                0.292, 0.292, 0.375, 0.458, 0.208, 0.292, 0.208, 0.375, 0.375, 0.375, _
                0.375, 0.375, 0.375, 0.375, 0.375, 0.375, 0.375, 0.375, 0.208, 0.208, _
                0.458, 0.458, 0.458, 0.333, 0.542, 0.417, 0.417, 0.417, 0.417, 0.375, _
                0.375, 0.417, 0.458, 0.25, 0.292, 0.375, 0.333, 0.5, 0.417, 0.458, 0.375, _
                0.458, 0.375, 0.375, 0.333, 0.417, 0.417, 0.583, 0.417, 0.417, 0.375, _
                0.25, 0.375, 0.25, 0.417, 0.375, 0.375, 0.375, 0.375, 0.333, 0.375, _
                0.375, 0.208, 0.375, 0.375, 0.167, 0.208, 0.333, 0.167, 0.583, 0.375, _
                0.375, 0.375, 0.375, 0.25, 0.333, 0.25, 0.375, 0.375, 0.5, 0.375, 0.375, _
                0.333, 0.375, 0.333, 0.375, 0.458, 0.542, 0.417, 0.208, 0.167, 0.375, _
                0.333, 0.625, 0.417, 0.417, 0.375, 1, 0.375, 0.292, 0.625, 0.208, 0.375, _
                0.208, 0.208, 0.167, 0.167, 0.333, 0.333, 0.417, 0.375, 0.542, 0.375, 0.542, _
                0.333, 0.292, 0.583, 0.208, 0.333, 0.417, 0.167, 0.25, 0.375, 0.375, 0.375, _
                0.375, 0.333, 0.375, 0.375, 0.5, 0.333, 0.458, 0.458, 0.292, 0.5, 0.375, _
                0.292, 0.458, 0.333, 0.333, 0.375, 0.375, 0.375, 0.208, 0.375, 0.333, _
                0.333, 0.458, 0.625, 0.625, 0.625, 0.333, 0.417, 0.417, 0.417, 0.417, _
                0.417, 0.417, 0.583, 0.417, 0.375, 0.375, 0.375, 0.375, 0.25, 0.25, _
                0.25, 0.25, 0.417, 0.417, 0.458, 0.458, 0.458, 0.458, 0.458, 0.458, _
                0.458, 0.417, 0.417, 0.417, 0.417, 0.417, 0.375, 0.375, 0.375, 0.375, _
                0.375, 0.375, 0.375, 0.375, 0.583, 0.333, 0.375, 0.375, 0.375, 0.375, _
                0.167, 0.167, 0.167, 0.167, 0.375, 0.375, 0.375, 0.375, 0.375, 0.375, _
                0.375, 0.458, 0.375, 0.375, 0.375, 0.375, 0.375, 0.375, 0.375, 0.375}

            For i = 1 To Len(str)
                w = w + d(Asc(Mid(str, i, 1)))
                If w > width Then Exit For
            Next
            If i <= Len(str) Then retv = Mid(str, 1, i - 1) & "..." Else retv = str
            Return retv
        End Function

        ''' <summary>
        ''' Gets the text to display whether list prices shown are incl. tax, excl. tax or a mixture.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetPriceInformation() As String
            ' Including/Excluding Tax
            Select Case AppSettings("LISTPRICE_INCLUDE_TAX")
                Case "0"
                    'If AppSettings("") Then
                    'Return Resources.Language.LABEL_PRICES_ARE_EXLUDING_TAX
                    'Else
                    Return ""
                    'End If
                Case "1"
                    Return Resources.Language.LABEL_PRICES_ARE_INLUDING_TAX
                Case "2"
                    Return Resources.Language.LABEL_PRICES_ARE_MIXED_TAX
                Case Else
                    Throw New Exception("Invalid value of LISTPRICE_INCLUDE_TAX in function GetPriceInformation.")
            End Select
        End Function

#End Region
#Region "lib_form"
        ''' <summary>
        ''' Validates that the specified string is a valid email address.
        ''' </summary>
        ''' <param name="adr">Email address to validate.</param>
        ''' <returns></returns>
        ''' <remarks>The function will only check the syntax of the email address. It will not check that the address actually exists.
        ''' If the email address is valid the function returns true.</remarks>
        Public Shared Function ValidateEmail(ByVal adr As Object) As Boolean
            Dim emailAddress As String = CStrEx(adr)
            If emailAddress <> "" Then
                Return RegExpTest(emailAddress, "^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$")
            End If
            Return False
        End Function

        ''' <summary>
        ''' Collecting Requests (forms and querystrings (POST and GET)) into a dictionary
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks>BAS webclient function</remarks>
        Public Shared Function RequestGetDict() As ExpDictionary
            Dim retval As New ExpDictionary()
            Dim i As Integer

            For i = 0 To HttpContext.Current.Request.Form.Count - 1
                RequestAddElements(HttpContext.Current.Request.Form.GetKey(i), HttpContext.Current.Request.Form(i), retval)
            Next
            For i = 0 To HttpContext.Current.Request.QueryString.Count - 1
                RequestAddElements(HttpContext.Current.Request.QueryString.GetKey(i), HttpContext.Current.Request.QueryString(i), retval)
            Next
            Return retval
        End Function

        ''' <summary>
        ''' Helper function for getting the request elements
        ''' </summary>
        ''' <param name="aKey"></param>
        ''' <param name="aValue"></param>
        ''' <param name="dicRequest"></param>
        ''' <remarks>BAS webclient function. Internal function used in RequestGetDict</remarks>
        Public Shared Sub RequestAddElements(ByVal aKey As Object, ByVal aValue As Object, ByVal dicRequest As Object)
            Dim arrKey, lb, ub, i, dicTmp As Object
            arrKey = Split(aKey, "!")
            lb = LBound(arrKey)
            ub = UBound(arrKey)
            i = lb
            dicTmp = dicRequest
            For i = lb To ub
                If i < ub Then
                    If dicTmp(arrKey(i)) Is Nothing Then dicTmp(arrKey(i)) = New ExpDictionary()
                    dicTmp = dicTmp(arrKey(i))
                Else
                    dicTmp(arrKey(i)) = aValue
                End If
            Next
        End Sub

        ''' <summary>
        ''' Sets focus on an element on a page after the page has loaded
        ''' </summary>
        ''' <param name="anElementName">The id of the element that should have set focus.</param>
        ''' <remarks>
        ''' Overwrites the standard OnLoad event for the HTML page.
        ''' Uses Java script.
        ''' </remarks>
        Shared Sub SetFocusOnElement_OnLoad(ByVal anElementName As Object)
            HttpContext.Current.Response.Write( _
                "<" & "script type=""text/javascript"">" & vbCrLf & _
                "<!--" & vbCrLf & _
                "       var formField = document.getElementById('" & anElementName & "');" & vbCrLf & _
                "       if (formField)" & vbCrLf & _
                "       {" & vbCrLf & _
                "	        formField.focus();" & vbCrLf & _
                "       }" & vbCrLf & _
                "//-->" & vbCrLf & _
                "<" & "/script>")
        End Sub
#End Region
#Region "lib_init"
        ''' <summary>
        ''' Detects if the shop uses product translations.
        ''' </summary>
        ''' <remarks>Product translations are usually extracted from the backend system.</remarks>
        Private Sub InitProductTranslations()
            Dim sql As String
            Dim languageguidsql As String
            Dim dt As DataTable

            sql = "SELECT TOP 1 * FROM ProductTranslation WHERE NOT LanguageGuid IS NULL"
            dt = SQL2DataTable(sql)
            HttpContext.Current.Application.Lock()

            ' Check if the product translation table is empty
            If CBoolEx(AppSettings("USE_PRODUCTTRANSLATION")) Then
                Try
                    HttpContext.Current.Application("EXTERNAL_LANGUAGEGUID_IS_NUMERIC") = dt.Columns("LanguageGuid").DataType.ToString <> "System.String"
                Catch ex As Exception

                End Try

            End If

            ' Save information on how to select an external language
            If HttpContext.Current.Application("EXTERNAL_LANGUAGEGUID_IS_NUMERIC") Then
                languageguidsql = "CAST(LanguageGuid AS VarChar(38))"
            Else
                languageguidsql = "LanguageGuid"
            End If
            HttpContext.Current.Application("EXTERNAL_LANGUAGEGUID_SQL") = languageguidsql

            HttpContext.Current.Application.UnLock()

        End Sub

        Public Function HasField(ByVal tablename As String, ByVal fieldname As Object) As Boolean
            Dim sql As String = "SELECT * FROM [" & tablename & "]"
            Dim d As ExpDictionary = Sql2Dictionary(sql)
            Return d.Exists(fieldname)
        End Function

        Public Shared Function HasRecords(ByVal sql As String) As Boolean
            Dim dr As SqlDataReader
            Dim conn As SqlConnection
            Dim cmd As SqlCommand
            Dim retv As Boolean

            conn = GetDBConnection()
            cmd = New SqlCommand(sql, conn)
            dr = cmd.ExecuteReader()
            retv = dr.Read
            dr.Close()
            conn.Close()
            Return retv
        End Function

#End Region
#Region "lib_language"
        ' This region contains functions handling languages within the shop.        

        ''' <summary>
        ''' This function returns the value of a specified label.
        ''' </summary>
        ''' <param name="labelname">The name of the label.</param>
        ''' <returns>Returns a string with the value of the translated label.</returns>
        ''' <remarks>
        ''' Example: GetLabel("LABEL_MOST_POPULAR_ITEM_USER") 
        ''' returns "My most popular products" if the users language is English.
        ''' </remarks>
        Public Function GetLabel(ByVal labelname As String) As String
            Dim retv As String
            Try
                retv = HttpContext.GetGlobalResourceObject("Language", labelname)
            Catch ex As Exception
                retv = labelname
            End Try

            If Not String.IsNullOrEmpty(retv) Then
                Return retv
            Else
                Return labelname
            End If

        End Function

        ''' <summary>
        ''' Internal function used in LoadUser and ProductTranslation. This function translates from ISO language Codes to
        ''' external language codes used in the Back-end system. Information is taken from the LanguageIso639 table.
        ''' </summary>
        ''' <param name="LanguageGuid">The language code in ISO format.</param>
        ''' <returns></returns>
        ''' <remarks>Returns the translated language guid.
        ''' The reverse function is GetIsoLanguageGuidFromExternal
        ''' </remarks>
        Public Shared Function GetExternalFromIsoLanguageGuid(ByVal LanguageGuid As String) As String
            Dim dt As DataTable
            Dim sql As String
            Dim cachekey As String
            Dim retv As String

            cachekey = "ExtLang-" & LanguageGuid
            retv = CacheGet(cachekey)

            If String.IsNullOrEmpty(retv) Then
                sql = "SELECT * FROM LanguageTable WHERE LanguageGuid=" & SafeString(LanguageGuid)
                dt = SQL2DataTable(sql)
                If dt.Rows.Count > 0 Then
                    If CStrEx(dt.Rows(0)("ExternalLanguageGuid")) = "" Then
                        retv = LanguageGuid
                    Else
                        retv = CStrEx(dt.Rows(0)("ExternalLanguageGuid"))
                    End If
                End If
                CacheSet(cachekey, retv, "LanguageTable")
            End If

            Return retv
        End Function

        ''' <summary>
        ''' Loads all the availible languages in the shop.
        ''' </summary>
        ''' <returns>Dictionary with all languages</returns>
        ''' <remarks></remarks>
        Public Function LoadLanguages() As ExpDictionary
            ' Changed here to use native language name
            Dim sql As String = loadLanguagesSQLString()
            Dim dictRetval As ExpDictionary
            Dim elm As String

            dictRetval = CacheGet("SiteLanguages")
            If dictRetval Is Nothing Then
                dictRetval = SQL2Dicts(sql, "LanguageGuid", -1)
                For Each elm In dictRetval.Keys
                    If CStrEx(dictRetval(elm)("LanguageName")) = "" Then
                        dictRetval(elm)("LanguageName") = dictRetval(elm)("LanguageGuid")
                    End If
                Next
                CacheSet("SiteLanguages", dictRetval, "LanguageTable")
            End If

            Return dictRetval

        End Function

        ''' <summary>
        ''' Loads all the availible languages in the shop.
        ''' </summary>
        ''' <returns>DataTable with all languages</returns>
        ''' <remarks></remarks>
        Public Function LoadLanguages(ByVal load As Boolean) As DataTable

            Dim dt As DataTable

            dt = CacheGet("DataTableSiteLanguages")
            If dt Is Nothing Then
                dt = ExpandITLib.SQL2DataTable(loadLanguagesSQLString())
                CacheSet("DataTableSiteLanguages", dt, "LanguageTable")
            End If

            Return dt

        End Function

        ''' <summary>
        ''' Returns a SQL-Statement used to load all availible languages in the shop
        ''' </summary>
        ''' <returns>A string as SQL-Statement</returns>
        ''' <remarks></remarks>
        Function loadLanguagesSQLString() As String
            Dim sb As New StringBuilder()
            sb.AppendLine(" SELECT LanguageGuid, LanguageNameNative as LanguageName FROM LanguageTable WHERE LanguageEnabled <> 0 ")
            sb.AppendLine(" AND LanguageNameNative is not null ")
            sb.AppendLine("UNION ")
            sb.AppendLine("SELECT LanguageGuid, LanguageName FROM LanguageTable WHERE LanguageEnabled <> 0 ")
            sb.AppendLine("AND LanguageNameNative is null ")
            sb.AppendLine(" ORDER BY LanguageName ")
            Return sb.ToString()
        End Function

#End Region
#Region "lib_main"
        ''' <summary>
        ''' This function is called on every page load to ensure that the general shop settings has been initialized.
        ''' </summary>
        ''' <remarks>The function will only initialize the shop settings once in an applications lifetime.</remarks>
        Public Sub InitializeShop()
            If Not HttpContext.Current.Application("InitializedShop") Then InitShop()
        End Sub

        ''' <summary>
        ''' Initializes different parts of the shop.
        ''' </summary>
        ''' <remarks>The initialization shows in the Application object.</remarks>
        Public Sub InitShop()
            InitProductTranslations()
            HttpContext.Current.Application("InitializedShop") = True
        End Sub

        ''' <summary>
        ''' Returns the absolute HTTP path to the shop.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks>The HTTP path is returned in the form "http://www.mydomain.com/shop".</remarks>
        Public Shared Function ShopFullPath() As String
            ShopFullPath = ShopFullPathServerOnly() & VRoot
        End Function

        Public Shared Function ShopFullPathServerOnly() As String
            Dim Protocol, Port, ServerName As String
            Protocol = "http://"
            Port = HttpContext.Current.Request.ServerVariables("SERVER_PORT")
            If Port = "80" Then Port = "" Else Port = ":" & Port
            If HttpContext.Current.Request.IsSecureConnection Then
                If Port = ":443" Then Port = ""
                Protocol = "https://"
            End If
            ServerName = HttpContext.Current.Request.ServerVariables("HTTP_HOST")
            If InStr(1, ServerName, ":") > 0 Then
                ServerName = Mid(ServerName, 1, InStr(1, ServerName, ":") - 1)
            End If
            ShopFullPathServerOnly = Protocol & ServerName & Port
        End Function
#End Region
#Region "lib_order"
        ''' <summary>
        ''' Generates either a random string or a number, which serve as the customers reference to an order.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks>This string or number will have to be unique with regards to the value in the field 'CustomerReference'
        ''' in the tables CartHeader and ShopSalesHeader. If the function can not create this in 10 tries then
        ''' an exception is thrown.
        ''' </remarks>
        Public Function GenCustRef() As String
            Dim retv As String = ""
            Dim bGenCustRef As Boolean = False

            ' Check 10 times if the customer reference is ok.
            For i As Integer = 1 To 1000
                retv = RandomNumber(10)
                If Not HasRecords("SELECT TOP 1 * FROM CartHeader WHERE CustomerReference = " & SafeString(retv)) Then
                    If Not HasRecords("SELECT * FROM ShopSalesHeader WHERE CustomerReference = " & SafeString(retv)) Then
                        'AM2010091001 - ONLINE PAYMENTS - Start
                        'If CBoolEx(AppSettings("EXPANDIT_US_USE_EEPG_ONLINE_PAYMENTS")) Then
                        '    If Not HasRecords("SELECT * FROM EEPGOnlinePayments WHERE PaymentReference = " & SafeString(retv)) Then
                        '        bGenCustRef = True
                        '        Exit For
                        '    End If
                        'Else
                        '    bGenCustRef = True
                        '    Exit For
                        'End If

                        'JA2011030701 - PAYMENT TABLE - START
                        If Not HasRecords("SELECT * FROM PaymentTable WHERE PaymentReference = " & SafeString(retv)) Then
                            bGenCustRef = True
                            Exit For
                        End If
                        'JA2011030701 - PAYMENT TABLE - END

                        'AM2010091001 - ONLINE PAYMENTS - End


                    End If
                End If
            Next

            ' Return the value or the error message "CustRefErr"
            If bGenCustRef Then
                Return retv
            Else
                Throw New Exception("Unable to create a new customer reference. Please contact the site administrator.")
            End If
        End Function

        ''' <summary>
        ''' Internal function. This function returns the name of a new cart.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function GetNewMultiCartDescription() As String
            Return Resources.Language.LABEL_NEW_CART
        End Function

        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
        ''' <summary>
        ''' This function loads a specified order from the database. The function is used to load old orders
        ''' from the ShopSalesHeader and ShopSalesLine tables.
        ''' </summary>
        ''' <param name="HeaderGuid">The guid for the order.</param>
        ''' <returns></returns>
        ''' <remarks>Returns a dictionary containing the order header and line information.</remarks>
        Public Function LoadOrder(ByVal HeaderGuid As String, Optional ByVal DocumentType As Integer = 0) As ExpDictionary
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
            Dim dtOrder As DataTable
            Dim Order As ExpDictionary
            Dim exchrate As Decimal
            Dim sql As String
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
            Dim HeaderTable As String = "ShopSalesHeader"
            Dim LineTable As String = "ShopSalesLine"

            If DocumentType > 0 Then
                HeaderTable = CStrEx(getSingleValueDB("SELECT HeaderTable FROM CustomerLedgerDrillDownSetup WHERE DocumentType=" & DocumentType))
                LineTable = CStrEx(getSingleValueDB("SELECT LineTable FROM CustomerLedgerDrillDownSetup WHERE DocumentType=" & DocumentType))
            End If

            dtOrder = SQL2DataTable("SELECT * FROM " & HeaderTable & " WHERE HeaderGuid=" & SafeString(HeaderGuid))
            If dtOrder.Rows.Count > 0 Then
                Order = SetDictionary(dtOrder.Rows(0))
                exchrate = CDblEx(Order("CurrencyExchangeRate"))
                If CDblEx(exchrate) = 0 Then exchrate = 1
                sql = "SELECT * FROM " & LineTable & " WHERE " & LineTable & ".HeaderGuid=" & SafeString(HeaderGuid)
                Order("Lines") = SQL2Dicts(sql, "LineGuid", -1)
                If DocumentType > 0 Then
                    If Not Order("Lines") Is Nothing Then
                        For Each item As ExpDictionary In Order("Lines").values
                            'AM2010071901 - ENTERPRISE CSC - Start
                            '*****CUSTOMER SPECIFIC CATALOGS*****-START
                            If CBoolEx(AppSettings("EXPANDIT_US_USE_CSC")) Then
                                sql = "SELECT COUNT(*) FROM (" & obj.getListAvailableItems(globals.User) & ") WHERE ProductGuid=" & SafeString(item("ProductGuid"))
                            Else
                                '*****CUSTOMER SPECIFIC CATALOGS*****-END
                                sql = "SELECT COUNT(ProductGuid) FROM GroupProduct WHERE CatalogNo=" & SafeString(globals.User("CatalogNo")) & " AND ProductGuid=" & SafeString(item("ProductGuid"))
                            End If
                            If CIntEx(getSingleValueDB(sql)) > 0 Then
                                item("Available") = True
                            Else
                                item("Available") = False
                            End If
                            'AM2010071901 - ENTERPRISE CSC - End
                        Next
                    End If
                End If
                'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
            Else
                Order = New ExpDictionary()
                Order("HeaderGuid") = HeaderGuid
                Order("CustomerReference") = "?"
            End If
            LoadOrder = Order
        End Function

        'JA2010092201 - RECURRING ORDERS - START
        ''' <summary>
        ''' This function loads a specified order from the database. The function is used to load old orders
        ''' from the ShopSalesHeader and ShopSalesLine tables.
        ''' </summary>
        ''' <param name="HeaderGuid">The guid for the order.</param>
        ''' <returns></returns>
        ''' <remarks>Returns a dictionary containing the order header and line information.</remarks>
        Public Function LoadOrderModifyRecurrency(ByVal HeaderGuid As String) As ExpDictionary
            Dim dtOrder As DataTable
            Dim Order As ExpDictionary
            Dim exchrate As Decimal
            Dim sql As String
            Dim retval As String
            Dim RNumber As Integer
            Dim RPeriod As String
            Dim RQuantity As String
            Dim RFinalDate As String
            Dim sku, comment, VariantCode As String
            Dim quantity As Integer
            'AM0122201001 - UOM - Start
            Dim uom As String = ""
            'AM0122201001 - UOM - End
            'Delete the active cart for this Sales Person Code
            'sql = "SELECT HeaderGuid FROM CartHeader WHERE SalesPersonCode= " & SafeString(globals.User("SalesPersonCode")) & " AND MultiCartStatus='ACTIVE'"
            sql = "SELECT HeaderGuid FROM CartHeader WHERE UserGuid= " & SafeString(globals.User("UserGuid")) & " AND MultiCartStatus='ACTIVE'"
            retval = getSingleValueDB(sql)
            If Not retval Is Nothing Then
                sql = "DELETE FROM CartHeader WHERE HeaderGuid=" & SafeString(retval.ToString)
                excecuteNonQueryDb(sql)
                sql = "DELETE FROM CartLine WHERE HeaderGuid=" & SafeString(retval.ToString)
            End If

            globals.OrderDict = Me.LoadOrderDictionary(globals.User)

            'Load the order that needs to be modified
            dtOrder = SQL2DataTable("SELECT * FROM ShopSalesHeader WHERE HeaderGuid=" & SafeString(HeaderGuid))
            If dtOrder.Rows.Count > 0 Then
                Order = SetDictionary(dtOrder.Rows(0))
                exchrate = CDblEx(Order("CurrencyExchangeRate"))
                If CDblEx(exchrate) = 0 Then exchrate = 1
                sql = "SELECT * FROM ShopSalesLine WHERE ShopSalesLine.HeaderGuid=" & SafeString(HeaderGuid)
                Order("Lines") = SQL2Dicts(sql, "LineGuid", -1)

                ' Save comment and purchase order number
                If Not Order("HeaderComment") Is Nothing Then globals.OrderDict("HeaderComment") = CStrEx(Order("HeaderComment"))
                If Not Order("CustomerPONumber") Is Nothing Then globals.OrderDict("CustomerPONumber") = CStrEx(Order("CustomerPONumber"))

                If Not Order("YourName") Is Nothing Then globals.OrderDict("YourName") = CStrEx(Order("YourName"))
                'WLB 10/10/2012 - BEGIN - Add "Your E-mail" to Additional Information
                If Not Order("YourEmail") Is Nothing Then globals.OrderDict("YourEmail") = CStrEx(Order("YourEmail"))
                'WLB 10/10/2012 - END - Add "Your E-mail" to Additional Information

                'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
                If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
                    csr.setAdjustmentInfo(Order)
                End If
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
                'globals.OrderDict("SalesPersonCodeOriginal") = Order("SalesPersonCode")
                globals.OrderDict("ModifiedRecurrency") = Order("CustomerReference")
                globals.OrderDict("CustomerReferenceOriginal") = Order("CustomerReference")
                globals.OrderDict("RecurringOrder") = True
                globals.OrderDict("UserGuid") = Order("UserGuid")

                globals.OrderDict("ShippingHandlingProviderGuid") = CStrEx(Order("ShippingHandlingProviderGuid"))
                globals.OrderDict("ResidentialShipping") = CStrEx(Order("ResidentialShipping"))
                globals.OrderDict("SaturdayDelivery") = CStrEx(Order("SaturdayDelivery"))
                globals.OrderDict("ServiceCode") = CStrEx(Order("ServiceCode"))
                globals.OrderDict("Insurance") = CStrEx(Order("Insurance"))
                globals.OrderDict("InsuredValue") = CStrEx(Order("InsuredValue"))
                globals.OrderDict("ShipToZipCode") = CStrEx(Order("ShipToZipCode"))
                globals.OrderDict("IsCalculated") = False

                For Each line As ExpDictionary In Order("Lines").Values
                    RNumber = CIntEx(line("RNumber"))
                    RPeriod = CStrEx(line("RPeriod"))
                    RQuantity = CIntEx(line("RQuantity"))
                    RFinalDate = CStrEx(line("RFinalDate"))
                    sku = CStrEx(line("ProductGuid"))
                    quantity = CIntEx(line("Quantity"))
                    comment = CStrEx(line("LineComment"))
                    VariantCode = CStrEx(line("VariantCode"))
                    'AM0122201001 - UOM - Start
                    If AppSettings("EXPANDIT_US_USE_UOM") Then
                        uom = CStrEx(line("UOM"))
                    End If
                    'AM0122201001 - UOM - End
                    'ShippingHandling = CStrEx(Order("ShippingHandlingProviderGuid"))
                    AddItemToOrder2Recurring(globals.OrderDict, sku, quantity, comment, VariantCode, RNumber, RPeriod, RQuantity, RFinalDate, uom)
                Next
            End If

            SaveOrderDictionary(globals.OrderDict)

            Return globals.OrderDict

        End Function

        'JA2010092201 - RECURRING ORDERS - END

        ''' <summary>
        ''' This function loads the current cart dictionary for a specified user.
        ''' </summary>
        ''' <param name="UserDict">User dictionary for whom the cart dictionary is needed.</param>
        ''' <returns>Returns a dictionary containing the cart for the specified user.</returns>
        ''' <remarks></remarks>
        Public Function LoadOrderDictionary(ByRef UserDict As ExpDictionary) As ExpDictionary
            Dim retval As ExpDictionary
            Dim dtCart As DataTable
            Dim HeaderGuid As String
            Dim sql As String

            retval = New ExpDictionary()
            retval.Add("Lines", New ExpDictionary())
            ' Read the order header
            dtCart = SQL2DataTable("SELECT * FROM CartHeader WHERE (UserGuid=" & SafeString(UserDict("UserGuid")) & ") AND (MultiCartStatus='ACTIVE')")
            If dtCart.Rows.Count = 0 Then
                ' If no active cart exists Then create it.
                HeaderGuid = GetHeaderGuidEx(UserDict("UserGuid"), False)

                ' Generate empty cart dictionary
                retval("HeaderGuid") = HeaderGuid
                retval("CreatedDate") = Date.Now
                retval("ModifiedDate") = Date.Now
                retval("UserGuid") = UserDict("UserGuid")
                retval("MultiCartStatus") = "ACTIVE"
                retval("MultiCartDescription") = GetNewMultiCartDescription()
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
                If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
                    csr.AssignSalesPersonGuid(retval)
                End If
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
                globals.IsCartEmpty = True
                'WLB.10.12.2012.BEGIN - Make InternetB2B E-mail the default for "YourEmail"
                retval("YourEmail") = globals.User("EmailAddress")
                'WLB.10.12.2012.END
            Else
                retval = SetDictionary(dtCart.Rows(0))

                sql = "SELECT * FROM CartLine" & _
                      "  WHERE HeaderGuid=" & SafeString(retval("HeaderGuid")) & _
                      " ORDER BY LineNumber"
                retval("Lines") = SQL2Dicts(sql, "LineGuid", -1)

                globals.IsCartEmpty = (retval("Lines").Count = 0)
            End If
            LoadOrderDictionary = retval
        End Function

        ''' <summary>
        ''' This function saves an order dictionary to the database. 
        ''' Call this function after modifying the order dictionary. 
        ''' If the order exists it is updated.
        ''' </summary>
        ''' <param name="OrderDictionary">The order dictionary to save to the database.</param>
        ''' <remarks></remarks>
        Public Sub SaveOrderDictionary(ByVal OrderDictionary As ExpDictionary)
            Dim sql As String
            Dim dblines, dbline As ExpDictionary
            Dim line As ExpDictionary
            Dim fieldname As String
            Dim deletelist As String
            Dim lineguid As String
            Dim hasnewlines, hasupdatedlines As Boolean

            ' Only save the order if any relevant information is present.
            ' Else, delete the CartHeader and lines.
            '*****promotions*****-start
            'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
            If OrderDictionary("Lines").Count > 0 Or _
              CStrEx(OrderDictionary("HeaderComment")) <> "" Or _
              CStrEx(OrderDictionary("CustomerPONumber")) <> "" Or _
              CStrEx(OrderDictionary("YourName")) <> "" Or _
              CStrEx(OrderDictionary("YourEmail")) <> "" Or _
              CStrEx(OrderDictionary("AdjustmentAmount")) <> "" Or _
              CStrEx(OrderDictionary("AdjustmentNote")) <> "" Or _
              CStrEx(OrderDictionary("PromotionCode")) <> "" Or _
              CStrEx(OrderDictionary("ShippingHandlingProviderGuid")) <> "" Or _
              CStrEx(OrderDictionary("SalesPersonGuid")) <> "" _
            Then
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
                '*****promotions*****-end
                ' Update the existing cart header.
                ' If it does not exist, it will be inserted.
                SetDictUpdateEx(OrderDictionary, "CartHeader", "SELECT * FROM CartHeader WHERE HeaderGuid=" & SafeString(OrderDictionary("HeaderGuid")))


                ' Read existing cart lines
                sql = "SELECT * FROM CartLine WHERE HeaderGuid=" & SafeString(OrderDictionary("HeaderGuid"))
                dblines = Sql2Dictionaries(sql, New Object() {"LineGuid"})

                ' Flag all database lines for deletion
                For Each dbline In dblines.Values
                    dbline("_Delete") = True
                Next

                '- Find and apply changes to dblines
                hasnewlines = False
                hasupdatedlines = False
                For Each line In OrderDictionary("Lines").Values
                    lineguid = line("LineGuid")
                    If dblines.Exists(SafeString(lineguid)) Then
                        ' Find corresponding dbline
                        dbline = dblines(SafeString(lineguid))
                        dbline.Remove("_Delete")

                        ' Compare the values
                        For Each fieldname In dbline.Keys
                            ' Check if the same field is present in the order line
                            If line.Exists(fieldname) Then
                                If CStrEx(dbline(fieldname)) <> CStrEx(line(fieldname)) Then
                                    CopyDictValues(dbline, line)
                                    hasupdatedlines = True
                                    dbline("_Update") = True
                                    Exit For
                                End If
                            End If
                        Next
                    Else
                        ' Introduce New dbline
                        dbline = New ExpDictionary
                        CopyDictValues(dbline, line)
                        dbline("_New") = True
                        dblines.Add(SafeString(dbline("LineGuid")), dbline)
                        hasnewlines = True
                    End If
                Next

                ' Find lines to delete
                deletelist = ""
                For Each dbline In dblines.Values
                    If dbline.ContainsKey("_Delete") Then
                        deletelist = deletelist & "," & SafeString(dbline("LineGuid"))
                    End If
                Next
                deletelist = Mid(deletelist, 2)

                ' Insert New lines
                If hasnewlines Then
                    For Each dbline In dblines.Values
                        If dbline.Exists("_New") Then
                            SetDictUpdateEx(dbline, "CartLine")
                        End If
                    Next
                End If

                ' Change updated lines
                If hasupdatedlines Then
                    For Each dbline In dblines.Values
                        If dbline.ContainsKey("_Update") Then
                            sql = "SELECT * FROM CartLine WHERE LineGuid=" & SafeString(dbline("LineGuid"))
                            SetDictUpdateEx(dbline, "CartLine", sql)
                        End If
                    Next
                End If

                ' Delete unused lines
                If deletelist <> "" Then
                    sql = "DELETE FROM CartLine WHERE LineGuid IN (" & deletelist & ")"
                    excecuteNonQueryDb(sql)
                End If
            Else
                ' Delete the order, as no important information is on it.
                sql = "DELETE FROM CartLine WHERE HeaderGuid = " & SafeString(OrderDictionary("HeaderGuid"))
                excecuteNonQueryDb(sql)
                sql = "DELETE FROM CartHeader WHERE HeaderGuid = " & SafeString(OrderDictionary("HeaderGuid"))
                excecuteNonQueryDb(sql)
            End If
        End Sub

        ''' <summary>
        ''' This function add the specified product to the users cart. 
        ''' The function is used on the cart.aspx page, and should 
        ''' be used to add items to the users cart.
        ''' </summary>
        ''' <param name="OrderObject">The order object the product should be added to.</param>
        ''' <param name="ProductGuid">ProductGuid for the item.</param>
        ''' <param name="Quantity">A quantity of the product to add to the cart.</param>
        ''' <param name="Comment">If not empty, this comment is added to the cart line.</param>
        ''' <remarks></remarks>
        Public Sub AddItemToOrder(ByVal OrderObject As ExpDictionary, ByVal ProductGuid As String, ByVal Quantity As Decimal, ByVal Comment As String)
            AddItemToOrder2(OrderObject, ProductGuid, Quantity, Comment, "")
        End Sub

        ''' <summary>
        ''' This function add a specified product to the users cart. The function supports variants on the item.
        '''Variants are not supported in the implementation. The AddItemToOrder function should be used.
        ''' </summary>
        ''' <param name="OrderObject"></param>
        ''' <param name="ProductGuid"></param>
        ''' <param name="Quantity"></param>
        ''' <param name="Comment"></param>
        ''' <param name="VariantCode"></param>
        ''' <remarks>In stead of this function use AddItemToOrder.</remarks>
        Public Sub AddItemToOrder2(ByVal OrderObject As ExpDictionary, ByVal ProductGuid As String, ByVal Quantity As Decimal, ByVal Comment As String, ByVal VariantCode As String, Optional ByVal UOM As String = "") 'AM0122201001 - UOM - Start
            'AM0122201001 - UOM - End
            '*****CUSTOMER SPECIFIC CATALOGS*****-START
            If AppSettings("EXPANDIT_US_USE_CSC") Then
                obj.AddItemToOrder2CSC(OrderObject, ProductGuid, Quantity, Comment, VariantCode, UOM)
            Else
                '*****CUSTOMER SPECIFIC CATALOGS*****-END
                Dim dtProduct As DataTable
                Dim dtUom As DataTable
                Dim NewQuantity As Decimal
                Dim InCart As Boolean
                Dim OrderLine As ExpDictionary
                Dim NextLineNumber As Integer
                Dim sql As String
                Dim sqlUom As String

                If Quantity > 0 Then
                    If CBool(AppSettings("CART_ALLOW_DECIMAL")) Then
                        NewQuantity = CDblEx(SafeUFloat(CDblEx(Quantity)))
                    Else
                        NewQuantity = CLngEx(SafeULong(CDblEx(Quantity)))
                    End If

                    ' Check If the product is already in the cart - If compress
                    InCart = False
                    If CBool(AppSettings("COMPRESS_CART")) Then
                        For Each OrderLine In OrderObject("Lines").values
                            'AM0122201001 - UOM - Start
                            If Trim(OrderLine("ProductGuid")).ToUpper = Trim(ProductGuid).ToUpper And Trim(CStrEx(OrderLine("VariantCode"))) = Trim(CStrEx(VariantCode)) And Not InCart And Trim(CStrEx(OrderLine("UOM"))) = Trim(CStrEx(UOM)) Then
                                'AM0122201001 - UOM - End
                                OrderLine("Quantity") = NewQuantity + CDblEx(OrderLine("Quantity"))
                                OrderLine("IsCalculated") = False
                                OrderLine("VersionGuid") = GenGuid()

                                ' Only set the comment if there was not one before
                                If CStrEx(OrderLine("LineComment")) = "" Then OrderLine("LineComment") = Comment
                                InCart = True
                                Exit For
                            End If
                        Next
                    End If

                    ' If Not in the cart already Then add it! or not to Compress
                    If Not InCart Then
                        OrderLine = New ExpDictionary
                        NextLineNumber = GetMaxLineNumber(OrderObject("Lines")) + 10000
                        If AppSettings("REQUIRE_CATALOG_ENTRY") Then
                            'AM2010071901 - ENTERPRISE CSC - Start
                            'JA2010030901 - PERFORMANCE -Start
                            sql = "SELECT ProductTable.* FROM ProductTable INNER JOIN GroupProduct ON ProductTable.ProductGuid=GroupProduct.ProductGuid WHERE ProductTable.ProductGuid=" & SafeString(ProductGuid) & " AND CatalogNo=" & SafeString(globals.User("CatalogNo"))
                            'JA2010030901 - PERFORMANCE -End
                            'AM2010071901 - ENTERPRISE CSC - End
                        Else
                            sql = "SELECT * FROM ProductTable WHERE ProductGuid=" & SafeString(ProductGuid)
                        End If
                        dtProduct = SQL2DataTable(sql)
                        If dtProduct.Rows.Count > 0 Then
                            Dim row As DataRow = dtProduct.Rows(0)
                            OrderLine("LineGuid") = GenGuid()
                            OrderLine("HeaderGuid") = OrderObject("HeaderGuid")
                            OrderLine("VariantCode") = CStrEx(VariantCode)
                            OrderLine("ProductGuid") = row("ProductGuid")
                            OrderLine("ProductName") = row("ProductName")
                            OrderLine("ListPrice") = row("ListPrice")
                            OrderLine("LineNumber") = NextLineNumber
                            OrderLine("Quantity") = NewQuantity
                            OrderLine("LineComment") = Comment
                            OrderLine("ProductGroupCode") = row("ProductGroupCode")
                            'AM0122201001 - UOM - Start
                            If AppSettings("EXPANDIT_US_USE_UOM") Then
                                sqlUom = "SELECT * FROM ItemUnitOfMeasure WHERE ItemNo = " & SafeString(ProductGuid) & " AND [Code] = " & SafeString(UOM)
                                dtUom = SQL2DataTable(sqlUom)
                                If dtUom.Rows.Count = 0 Then
                                    globals.messages.Warnings.Add(Replace(Replace(Replace(Resources.Language.MESSAGE_PRODUCT_INVALID_UOM_REPLACED, "%1", UOM), "%2", ProductGuid), "%3", row("SalesUOM")))
                                    UOM = row("SalesUOM")
                                End If
                                OrderLine("UOM") = CStrEx(UOM)
                            End If
                            'AM0122201001 - UOM - End

                            'AM2010061801 - KITTING - START
                            If CBoolEx(AppSettings("EXPANDIT_US_USE_KITTING")) Then
                                OrderLine("KitBOMNo") = CStrEx(row("KitBOMNo"))
                            End If
                            'AM2010061801 - KITTING - END

                            '<!-- ' ExpandIT US - MPF - US Sales Tax - Start -->
                            ' 20080529 - ExpandIT US - MPF - US Sales Tax Modification
                            If (AppSettings("EXPANDIT_US_USE_US_SALES_TAX")) Then
                                OrderLine("TaxGroupCode") = row("TaxGroupCode")
                            End If
                            '<!-- ' ExpandIT US - MPF - US Sales Tax - Finish -->

                            '*****shipping*****-start
                            'AM2010042601 - ENTERPRISE SHIPPING - Start
                            If CBoolEx(AppSettings("EXPANDIT_US_USE_SHIPPING")) Or Not CBoolEx(AppSettings("EXPANDIT_US_USE_SHIPPING")) And CBoolEx(AppSettings("ENTERPRISE_SHIPPING_USE_WEIGHT")) Then
                                'AM2010042601 - ENTERPRISE SHIPPING - End
                                OrderLine("ShippingWeight") = row("ShippingWeight")
                            End If
                            '*****shipping*****-end
                            ' For performance reasons.
                            OrderLine("IsCalculated") = False
                            OrderLine("VersionGuid") = GenGuid()
                            If OrderObject("Lines") Is Nothing Then
                                OrderObject.Add("Lines", New ExpDictionary)
                            End If
                            OrderObject("Lines").Add(OrderLine("LineGuid"), OrderLine)
                        Else
                            globals.messages.Warnings.Add(Replace(Resources.Language.MESSAGE_PRODUCT_DOES_NOT_EXISTS, "%1", ProductGuid))
                        End If
                    End If
                End If
            End If

        End Sub

        'JA2010092201 - RECURRING ORDERS - START
        Public Sub AddItemToOrder2Recurring(ByVal OrderObject As ExpDictionary, ByVal ProductGuid As String, ByVal Quantity As Decimal, ByVal Comment As String, ByVal VariantCode As String, ByVal RNumber As Integer, ByVal RPeriod As String, ByVal RQuantity As String, ByVal RFinalDate As String, Optional ByVal UOM As String = "") 'AM0122201001 - UOM - Start

            'AM0122201001 - UOM - End
            '*****CUSTOMER SPECIFIC CATALOGS*****-START
            If AppSettings("EXPANDIT_US_USE_CSC") Then
                obj.AddItemToOrder2RecurringCSC(OrderObject, ProductGuid, Quantity, Comment, VariantCode, RNumber, RPeriod, RQuantity, RFinalDate, UOM)
            Else
                '*****CUSTOMER SPECIFIC CATALOGS*****-END
                Dim dtProduct As DataTable
                Dim NewQuantity As Decimal
                Dim InCart As Boolean
                Dim OrderLine As ExpDictionary
                Dim NextLineNumber As Integer
                Dim sql As String

                If Quantity > 0 Then
                    If CBool(AppSettings("CART_ALLOW_DECIMAL")) Then
                        NewQuantity = CDblEx(SafeUFloat(CDblEx(Quantity)))
                    Else
                        NewQuantity = CLngEx(SafeULong(CDblEx(Quantity)))
                    End If

                    ' Check If the product is already in the cart - If compress
                    InCart = False
                    If CBool(AppSettings("COMPRESS_CART")) Then
                        For Each OrderLine In OrderObject("Lines").values
                            'AM0122201001 - UOM - Start
                            If Trim(OrderLine("ProductGuid")) = Trim(ProductGuid) And Trim(CStrEx(OrderLine("VariantCode"))) = Trim(CStrEx(VariantCode)) And Not InCart And Trim(CStrEx(OrderLine("UOM"))) = Trim(CStrEx(UOM)) Then
                                'AM0122201001 - UOM - End
                                OrderLine("Quantity") = NewQuantity + CDblEx(OrderLine("Quantity"))
                                OrderLine("IsCalculated") = False
                                OrderLine("VersionGuid") = GenGuid()

                                ' Only set the comment if there was not one before
                                If CStrEx(OrderLine("LineComment")) = "" Then OrderLine("LineComment") = Comment
                                InCart = True
                                Exit For
                            End If
                        Next
                    End If

                    ' If Not in the cart already Then add it! or not to Compress
                    If Not InCart Then
                        OrderLine = New ExpDictionary
                        NextLineNumber = GetMaxLineNumber(OrderObject("Lines")) + 10000
                        If AppSettings("REQUIRE_CATALOG_ENTRY") Then
                            'AM2010071901 - ENTERPRISE CSC - Start
                            sql = "SELECT ProductTable.* FROM ProductTable, GroupProduct WHERE ProductTable.ProductGuid=GroupProduct.ProductGuid AND ProductTable.ProductGuid=" & SafeString(ProductGuid) & " AND CatalogNo=" & SafeString(globals.User("CatalogNo"))
                            'AM2010071901 - ENTERPRISE CSC - End
                        Else
                            sql = "SELECT * FROM ProductTable WHERE ProductGuid=" & SafeString(ProductGuid)
                        End If
                        dtProduct = SQL2DataTable(sql)
                        If dtProduct.Rows.Count > 0 Then
                            Dim row As DataRow = dtProduct.Rows(0)
                            OrderLine("LineGuid") = GenGuid()
                            OrderLine("HeaderGuid") = OrderObject("HeaderGuid")
                            OrderLine("VariantCode") = CStrEx(VariantCode)
                            OrderLine("ProductGuid") = row("ProductGuid")
                            OrderLine("ProductName") = row("ProductName")
                            OrderLine("ListPrice") = row("ListPrice")
                            OrderLine("LineNumber") = NextLineNumber
                            OrderLine("Quantity") = NewQuantity
                            OrderLine("LineComment") = Comment
                            OrderLine("RNumber") = RNumber
                            OrderLine("RPeriod") = RPeriod
                            OrderLine("RQuantity") = RQuantity
                            OrderLine("RFinalDate") = RFinalDate
                            'AM0122201001 - UOM - Start
                            If AppSettings("EXPANDIT_US_USE_UOM") Then
                                OrderLine("UOM") = CStrEx(UOM)
                            End If
                            'AM0122201001 - UOM - End

                            'AM2010061801 - KITTING - START
                            If CBoolEx(AppSettings("EXPANDIT_US_USE_KITTING")) Then
                                OrderLine("KitBOMNo") = CStrEx(row("KitBOMNo"))
                            End If
                            'AM2010061801 - KITTING - END

                            'OrderLine("MembershipItem") = row("MembershipItem")
                            'OrderLine("MembershipMonths") = row("MembershipMonths")


                            '<!-- ' ExpandIT US - MPF - US Sales Tax - Start -->
                            ' 20080529 - ExpandIT US - MPF - US Sales Tax Modification
                            If (AppSettings("EXPANDIT_US_USE_US_SALES_TAX")) Then
                                OrderLine("TaxGroupCode") = row("TaxGroupCode")
                            End If
                            '<!-- ' ExpandIT US - MPF - US Sales Tax - Finish -->

                            '*****shipping*****-start
                            'AM2010042601 - ENTERPRISE SHIPPING - Start
                            If CBoolEx(AppSettings("EXPANDIT_US_USE_SHIPPING")) Or Not CBoolEx(AppSettings("EXPANDIT_US_USE_SHIPPING")) And CBoolEx(AppSettings("ENTERPRISE_SHIPPING_USE_WEIGHT")) Then
                                'AM2010042601 - ENTERPRISE SHIPPING - End
                                OrderLine("ShippingWeight") = row("ShippingWeight")
                            End If
                            '*****shipping*****-end
                            ' For performance reasons.
                            OrderLine("IsCalculated") = False
                            OrderLine("VersionGuid") = GenGuid()
                            If OrderObject("Lines") Is Nothing Then
                                OrderObject.Add("Lines", New ExpDictionary)
                            End If
                            OrderObject("Lines").Add(OrderLine("LineGuid"), OrderLine)
                        Else
                            globals.messages.Warnings.Add(Replace(Resources.Language.MESSAGE_PRODUCT_DOES_NOT_EXISTS, "%1", ProductGuid))
                        End If
                    End If
                End If
            End If

        End Sub
        'JA2010092201 - RECURRING ORDERS - END

        Public Shared Function GetMaxLineNumber(ByVal lines As ExpDictionary) As Integer
            Dim retv As Integer = 0
            Dim line As ExpDictionary

            retv = 0
            For Each line In lines.Values
                If CLngEx(line("LineNumber")) > retv Then retv = CLngEx(line("LineNumber"))
            Next
            GetMaxLineNumber = retv
        End Function

        ''' <summary>
        ''' Internal function. The function gets the current cart header for the specified user.
        ''' If the cart record is not found and bCreate is true, a new cart is created in the database.
        ''' </summary>
        ''' <param name="UserGuid">The user guid.</param>
        ''' <param name="bCreate">Create new cart in database.</param>
        ''' <returns>Returns the header guid for the users cart header.</returns>
        ''' <remarks></remarks>
        Public Function GetHeaderGuidEx(ByVal UserGuid As String, ByVal bCreate As Boolean) As String
            Dim dtCart As DataTable
            Dim retv As String
            Dim sql As String

            dtCart = SQL2DataTable("SELECT * FROM CartHeader WHERE UserGuid=" & SafeString(UserGuid) & " AND MultiCartStatus='ACTIVE'")
            If dtCart.Rows.Count > 0 Then
                retv = CStrEx(dtCart.Rows(0)("HeaderGuid"))
            Else
                retv = GenGuid()
                If bCreate Then
                    sql = "INSERT INTO CartHeader " & _
                     "(HeaderGuid, CreatedDate, ModifiedDate, UserGuid, MultiCartStatus, MultiCartDescription) " & _
                     "VALUES " & _
                     "(" & SafeString(retv) & "," & SafeDatetime(Now) & "," & SafeDatetime(Now) & _
                     "," & SafeString(UserGuid) & "," & SafeString("ACTIVE") & _
                     "," & SafeString(GetNewMultiCartDescription) & ")"
                    excecuteNonQueryDb(sql)
                End If
            End If

            Return retv
        End Function

        ''' <summary>
        ''' This function is used to calculate customer specific prices for the current order.
        ''' If OrderDict dictionary is not loaded the current order for the current user is loaded.
        ''' The function uses the CalcOrder function found in Include/buslogic.aspx file. The buslogic.aspx
        ''' file implements back-end specific business logic.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks>
        ''' Returns the order dictionary with customer specific prices calculated for the order.
        ''' Loads the order if it is not already loaded.
        ''' Sets the OrderDict dictionary object.
        ''' </remarks>
        Public Function CalculateOrder() As ExpDictionary
            If Not IsDict(globals.OrderDict) Then globals.OrderDict = LoadOrderDictionary(globals.User)
            Return CalculateOrder(globals.OrderDict)
        End Function


        Public Function CalculateOrder(ByVal OrderDict As ExpDictionary) As ExpDictionary
            Dim errVar As Object
            Dim localErrorStr As String = ""
            If Not IsDict(globals.Context) Then globals.Context = LoadContext(globals.User)
            If OrderDict.Count > 0 Then
                buslogic.CalcOrder(OrderDict, globals.Context)
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
                If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
                    csr.calculateShipping(OrderDict)
                Else
                    '*****shipping*****-start 
                    If Not CBoolEx(AppSettings("EXPANDIT_US_USE_SHIPPING")) Then
                        If AppSettings("SHIPPING_HANDLING_ENABLED") Then
                            CalcShippingHandling(OrderDict, globals.Context)
                        End If
                    Else
                        If CStrEx(OrderDict("ShippingHandlingProviderGuid")) <> "" And AppSettings("SHIPPING_HANDLING_ENABLED") And Not OrderDict("ShippingHandlingProviderGuid") Is Nothing And Not OrderDict("ServiceCode") Is Nothing And Not OrderDict("ShipToZipCode") Is Nothing And Not OrderDict("ShipToZipCode") Is DBNull.Value And Not OrderDict("ShippingHandlingProviderGuid") Is DBNull.Value Then
                            globals.shippingError = shipping.shippingProviderSelection(OrderDict("ShippingHandlingProviderGuid"), OrderDict("ServiceCode"), OrderDict, globals.Context, OrderDict("ShipToZipCode"), globals)
                            shipping.ErrorsHandler(globals)

                        End If
                    End If
                    '*****shipping*****-end
                    SaveOrderDictionary(OrderDict)
                End If
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
            End If

            For Each errVar In globals.Context("Errors").Values
                localErrorStr = errVar
                globals.messages.Errors.Add(errVar)
            Next

            For Each errVar In globals.Context("Warnings").Values
                globals.messages.Warnings.Add(errVar)
            Next

            If localErrorStr <> "" Then
                globals.messages.Errors.Add(Resources.Language.ERROR_THERE_WERE_ERRORS_CALC_PRICES_FOR_PRODUCTS)
            End If

            globals.IsCartEmpty = (OrderDict("Lines").Count = 0)

            If UseSecondaryCurrency(globals.User) Then
                ' Convert fileds on header
                currency.ConvertCurrency(OrderDict, OrderDict("CurrencyGuid"), HttpContext.Current.Session("UserSecondaryCurrencyGuid").ToString)
                ' Convert fields on lines
                currency.ConvertCurrency(OrderDict("Lines"), OrderDict("CurrencyGuid"), HttpContext.Current.Session("UserSecondaryCurrencyGuid").ToString)
            End If

            Return OrderDict
        End Function

        ''' <summary>
        ''' This function calculates customer specific prices for a list of products. The function
        ''' Uses the implementation of customer specific pricing found in Include/buslogic.aspx.
        ''' The function is used on pages displaying customer specific price lists.
        ''' </summary>
        ''' <param name="dictProducts">A dictionary containing a list of productGuids.</param>
        ''' <remarks>Returns the dictionary containing the products including the customer specific prices.</remarks>
        Public Sub GetCustomerPricesEx(ByVal dictProducts As ExpDictionary)
            Dim tmpOrderDict As ExpDictionary
            Dim k As Object
            Dim tmpOrderLine As ExpDictionary
            Dim errVar As String

            If Not IsDict(globals.Context) Then globals.Context = LoadContext(globals.User)
            tmpOrderDict = New ExpDictionary()
            tmpOrderDict("Lines") = New ExpDictionary()

            For Each k In dictProducts.Keys
                tmpOrderLine = New ExpDictionary
                'AM2010051801 - SPECIAL ITEMS - Start
                tmpOrderLine("BaseUOM") = dictProducts(k)("BaseUOM")
                'AM2010051801 - SPECIAL ITEMS - End
                tmpOrderLine("ProductGuid") = dictProducts(k)("ProductGuid")
                tmpOrderLine("VariantCode") = dictProducts(k)("VariantCode")
                tmpOrderLine("ProductGroupCode") = dictProducts(k)("ProductGroupCode")
                tmpOrderLine("Quantity") = 1
                tmpOrderLine("LineGuid") = k
                tmpOrderDict("Lines").Add(k, tmpOrderLine)
            Next

            tmpOrderDict("CurrencyGuid") = globals.Context("CurrencyGuid")
            If CStrEx(tmpOrderDict("CurrencyGuid")) = "" Then tmpOrderDict("CurrencyGuid") = globals.Context("DefaultCurrency")
            ' Calculate the "fake" order
            buslogic.CalcOrder(tmpOrderDict, globals.Context)

            For Each k In tmpOrderDict("Lines").ClonedKeyArray
                CopyDictValues(dictProducts(k), tmpOrderDict("Lines")(k))
            Next

            For Each errVar In globals.Context("Errors").Values
                globals.messages.Errors.Add(errVar)
            Next

            For Each errVar In globals.Context("Warnings").Values
                globals.messages.Warnings.Add(errVar)
            Next
        End Sub

        ''' <summary>
        ''' Renders the order dictionary to HTML.
        ''' </summary>
        ''' <param name="OrderDict">The order dictionary to render</param>
        ''' <returns></returns>
        ''' <remarks>Used when emailing and order confirmation from the shop.
        ''' The Company information is rendered in the lib_company.RenderCompanyInformation function.
        ''' The Company information is taken from the CompanyTable table in the database.
        ''' </remarks>
        Public Function RenderOrderDict(ByVal OrderDict As ExpDictionary, Optional OnSecurePage As Boolean = False) As String
            Dim retv As String
            '# Changed to make use of correct tax settings from web.config
            Dim taxType As Boolean = String.Equals(CType(AppSettings("SHOW_TAX_TYPE"), String), "INCL")
            Dim IsCommentShown As Boolean = CBoolEx(AppSettings("CART_COMMENT"))
            Dim IsPoNumberShown As Boolean = CBoolEx(AppSettings("CART_PONUMBER"))

            Dim fn As String = HttpContext.Current.Server.MapPath("~/App_Themes/EnterpriseBlue/Cart.css")
            Dim sr As IO.StreamReader = New StreamReader(fn)
            retv = "<style>" & sr.ReadToEnd & "</style>"
            'AM2010031501 - ORDER RENDER - Start
            'retv = retv & OrderRender.Render(OrderDict, Me, taxType, True, True, False, False, True, IsCommentShown, IsPoNumberShown, True, True, True, True, Nothing, False)


            Dim pageholder As New ExpandIT.Page()
			pageholder.RequireSSL = OnSecurePage
            Dim renderControl As New ExpandIT.UserControl
            renderControl = pageholder.LoadControl("controls/USOrderRender/CartRender.ascx")
            Dim controlType As Type = renderControl.GetType()

            Dim propertyObj As Reflection.PropertyInfo = controlType.GetProperty("IsActiveOrder")
            If Not propertyObj Is Nothing Then
                propertyObj.SetValue(renderControl, False, Nothing)
            End If
            propertyObj = controlType.GetProperty("IsEmail")
            If Not propertyObj Is Nothing Then
                propertyObj.SetValue(renderControl, True, Nothing)
            End If
            propertyObj = controlType.GetProperty("IsBrowser")
            If Not propertyObj Is Nothing Then
                propertyObj.SetValue(renderControl, False, Nothing)
            End If
            propertyObj = controlType.GetProperty("HeaderGuid")
            If Not propertyObj Is Nothing Then
                propertyObj.SetValue(renderControl, OrderDict("HeaderGuid"), Nothing)
            End If
            propertyObj = controlType.GetProperty("EditMode")
            If Not propertyObj Is Nothing Then
                propertyObj.SetValue(renderControl, False, Nothing)
            End If
            propertyObj = controlType.GetProperty("DefaultButton")
            If Not propertyObj Is Nothing Then
                propertyObj.SetValue(renderControl, Nothing, Nothing)
            End If
            propertyObj = controlType.GetProperty("ShowDate")
            If Not propertyObj Is Nothing Then
                propertyObj.SetValue(renderControl, True, Nothing)
            End If
            propertyObj = controlType.GetProperty("ShowPaymentInfo")
            If Not propertyObj Is Nothing Then
                propertyObj.SetValue(renderControl, True, Nothing)
            End If
            pageholder.Controls.Add(renderControl)
            Dim output As New StringWriter()
            HttpContext.Current.Server.Execute(pageholder, output, False)
            Dim ret As String
            ret = output.ToString()

            retv = retv & ret
            'AM2010031501 - ORDER RENDER - End
            Return retv

        End Function

        ''' <summary>
        ''' Renders the order dictionary to Text.
        ''' </summary>
        ''' <param name="OrderDict">The order dictionary to render</param>
        ''' <returns>
        ''' </returns>
        ''' <remarks>
        ''' Used when emailing and order confirmation from the shop.
        ''' The Company information is rendered in the lib_company.RenderCompanyInformationText function.
        ''' The Company information is taken from the CompanyTable table in the database.
        ''' </remarks>
        Public Function RenderOrderDictText(ByVal OrderDict As ExpDictionary) As String
            Dim retv As String = ""
            Dim builder As New StringBuilder()
            Dim i As Integer
            Dim lineguids, lineguid, OrderLine As Object
            Dim localProductName As Object

            Dim ColumnWidth As Integer() = {10, 30, 6, 17, 17}
            Dim TotalColumnWidth As Integer() = {45, 18, 17}

            ' Customer Reference, Date and Company information.
            builder.Append(Resources.Language.LABEL_ORDER_NUMBER)
            builder.Append(" ")
            builder.AppendLine(OrderDict("CustomerReference"))
            builder.Append(Resources.Language.LABEL_DATE)
            builder.Append(": ")
            builder.AppendLine(FormatDate(OrderDict("HeaderDate")))
            builder.AppendLine()
            builder.AppendLine(RenderCompanyInformationText("1"))

            ' Bill-To information.                
            builder.AppendLine(Resources.Language.LABEL_BILL_TO)
            builder.AppendLine(CStrEx(OrderDict("ContactName")))
            builder.Append(IIf(Len(CStrEx(OrderDict("CompanyName"))) > 0, CStrEx(OrderDict("CompanyName")) & vbCrLf, ""))

            builder.AppendLine(CStrEx(OrderDict("Address1")))
            builder.Append(IIf(Len(CStrEx(OrderDict("Address2"))) > 0, CStrEx(OrderDict("Address2")) & vbCrLf, ""))
            builder.Append(CStrEx(OrderDict("ZipCode")))
            builder.Append("  ")
            builder.AppendLine(CStrEx(OrderDict("CityName")))
            builder.AppendLine()

            ' Ship-To information.
            builder.AppendLine(Resources.Language.LABEL_SHIP_TO)
            builder.AppendLine(CStrEx(OrderDict("ShipToContactName")))
            builder.Append(IIf(Len(CStrEx(OrderDict("ShipToCompanyName"))) > 0, CStrEx(OrderDict("ShipToCompanyName")) & vbCrLf, ""))
            builder.AppendLine(CStrEx(OrderDict("ShipToAddress1")))
            builder.Append(IIf(Len(CStrEx(OrderDict("ShipToAddress2"))) > 0, CStrEx(OrderDict("ShipToAddress2")) & vbCrLf, ""))
            builder.Append(CStrEx(OrderDict("ShipToZipCode")))
            builder.Append("  ")
            builder.AppendLine(CStrEx(OrderDict("ShipToCityName")))
            builder.AppendLine()

            ' Payment information
            builder.AppendLine(Resources.Language.LABEL_PAYMENT_INFORMATION)
            builder.AppendLine(GetPaymentTypeName(CStrEx(OrderDict("PaymentType"))))

            'JA2011030701 - PAYMENT TABLE - START
            'If CStrEx(OrderDict("PaymentTransactionID")) <> "" Then
            '    builder.Append(Resources.Language.LABEL_TRANSACTION_ID)
            '    builder.Append(" : ")
            '    builder.AppendLine(CStrEx(OrderDict("PaymentTransactionID")))
            'End If
            If CStrEx(globals.eis.getPaymentTableColumn(OrderDict("HeaderGuid"), "PaymentTransactionID")) <> "" Then
                builder.Append(Resources.Language.LABEL_TRANSACTION_ID)
                builder.Append(" : ")
                builder.AppendLine(CStrEx(getPaymentTableColumn(OrderDict("HeaderGuid"), "PaymentTransactionID")))
            End If
            'JA2011030701 - PAYMENT TABLE - END

            builder.AppendLine()

            If CBoolEx(AppSettings("CART_DISPLAY_PRICES_INCL_TAX")) Then
                ' Order Headers                 
                builder.Append(RPad(Resources.Language.LABEL_PRODUCT, " ", ColumnWidth(0) + ColumnWidth(1)))
                builder.Append(LPad(Resources.Language.LABEL_QUANTITY, " ", ColumnWidth(2)))
                builder.Append(LPad(Resources.Language.LABEL_PRICE & " " & Resources.Language.LABEL_INCLUSIVE_VAT, " ", ColumnWidth(3)))
                builder.AppendLine(LPad(Resources.Language.LABEL_TOTAL, " ", ColumnWidth(3)))

                ' Line information
                If TypeOf OrderDict("Lines").keys Is Dictionary(Of Object, Object).KeyCollection Then
                    Dim data As Dictionary(Of Object, Object).KeyCollection = CType(OrderDict("Lines").keys(), Dictionary(Of Object, Object).KeyCollection)
                    ReDim Preserve lineguids(data.Count - 1)
                    data.CopyTo(lineguids, 0)
                Else
                    lineguids = OrderDict("Lines").keys
                End If

                For i = 0 To UBound(lineguids)
                    lineguid = lineguids(i)
                    OrderLine = OrderDict("Lines")(lineguid)
                    builder.Append(RPad(OrderLine("ProductGuid"), " ", ColumnWidth(0)))
                    localProductName = OrderLine("ProductName")
                    If Not IsNull(OrderLine("VariantCode")) Then
                        localProductName = localProductName & _
                         " " & OrderLine("VariantName")
                    End If
                    builder.Append(RPad(localProductName, " ", ColumnWidth(1)))

                    builder.Append(LPad(OrderLine("Quantity"), " ", ColumnWidth(2)))
                    builder.Append(LPad(CurrencyFormatter.FormatCurrency(OrderLine("ListPriceInclTax"), OrderDict("CurrencyGuid")), " ", ColumnWidth(3)))
                    builder.AppendLine(LPad(CurrencyFormatter.FormatCurrency(OrderLine("TotalInclTax"), OrderDict("CurrencyGuid")), " ", ColumnWidth(4)))
                Next

                ' Order totals.

                builder.Append(" ", TotalColumnWidth(0))
                builder.Append(RPad(Resources.Language.LABEL_SUB_TOTAL, " ", TotalColumnWidth(1)))
                builder.AppendLine(LPad(CurrencyFormatter.FormatCurrency(OrderDict("SubTotalInclTax"), OrderDict("CurrencyGuid")), " ", TotalColumnWidth(2)))
                builder.Append(" ", TotalColumnWidth(0))
                builder.Append(RPad(Resources.Language.LABEL_INVOICE_DISCOUNT, " ", TotalColumnWidth(1)))
                builder.AppendLine(LPad(CurrencyFormatter.FormatCurrency(OrderDict("InvoiceDiscountInclTax"), OrderDict("CurrencyGuid")), " ", TotalColumnWidth(2)))
                builder.Append(" ", TotalColumnWidth(0))
                builder.Append(RPad(Resources.Language.LABEL_SERVICE_CHARGE, " ", TotalColumnWidth(1)))
                builder.AppendLine(LPad(CurrencyFormatter.FormatCurrency(OrderDict("ServiceChargeInclTax"), OrderDict("CurrencyGuid")), " ", TotalColumnWidth(2)))
                builder.Append(" ", TotalColumnWidth(0))
                builder.Append(RPad(Resources.Language.LABEL_SHIPPING, " ", TotalColumnWidth(1)))
                builder.AppendLine(LPad(CurrencyFormatter.FormatCurrency(OrderDict("ShippingAmountInclTax"), OrderDict("CurrencyGuid")), " ", TotalColumnWidth(2)))
                builder.Append(" ", TotalColumnWidth(0))
                builder.Append(RPad(Resources.Language.LABEL_HANDLING, " ", TotalColumnWidth(1)))
                builder.AppendLine(LPad(CurrencyFormatter.FormatCurrency(OrderDict("HandlingAmountInclTax"), OrderDict("CurrencyGuid")), " ", TotalColumnWidth(2)))

                If OrderDict("PaymentFeeAmountInclTax") > 0 Then

                    builder.Append(" ", TotalColumnWidth(0))
                    builder.Append(RPad(Resources.Language.LABEL_PAYMENT_FEE, " ", TotalColumnWidth(1)))
                    builder.AppendLine(LPad(CurrencyFormatter.FormatCurrency(OrderDict("PaymentFeeAmountInclTax"), OrderDict("CurrencyGuid")), " ", TotalColumnWidth(2)))
                End If

                'builder.Append(" ", TotalColumnWidth(0))
                'builder.Append(RPad(Resources.Language.LABEL_TAXAMOUNT, " ", TotalColumnWidth(1)))
                'builder.AppendLine(LPad(CurrencyFormatter.FormatCurrency(OrderDict("TaxAmount"), OrderDict("CurrencyGuid")).ToString(), " ", TotalColumnWidth(2)))

                'builder.Append(" ", TotalColumnWidth(0))
                'builder.Append(RPad(Resources.Language.LABEL_TOTAL & " " & Resources.Language.LABEL_INCLUSIVE_VAT, " ", TotalColumnWidth(1)))
                'builder.AppendLine(LPad(CurrencyFormatter.FormatCurrency(OrderDict("TotalInclTax"), OrderDict("CurrencyGuid")), " ", TotalColumnWidth(2)))

            Else
                ' Order Headers
                builder.Append(RPad(Resources.Language.LABEL_PRODUCT, " ", ColumnWidth(0) + ColumnWidth(1)))
                builder.Append(RPad(Resources.Language.LABEL_QUANTITY, " ", ColumnWidth(2)))
                builder.Append(RPad(Resources.Language.LABEL_PRICE, " ", ColumnWidth(3)))
                builder.AppendLine(Resources.Language.LABEL_TOTAL)

                ' Line information
                For Each lineguid In OrderDict("Lines").keys
                    OrderLine = OrderDict("Lines")(lineguid)
                    builder.Append(RPad(OrderLine("ProductGuid"), " ", ColumnWidth(0)))
                    localProductName = OrderLine("ProductName")
                    If Not IsNull(OrderLine("VariantCode")) Then
                        localProductName = localProductName & _
                         OrderLine("VariantName")
                    End If
                    builder.Append(RPad(localProductName, " ", ColumnWidth(1)))
                    builder.Append(LPad(OrderLine("Quantity"), " ", ColumnWidth(2)))
                    builder.Append(LPad(CurrencyFormatter.FormatCurrency(OrderLine("ListPrice"), OrderDict("CurrencyGuid")), " ", ColumnWidth(3)))
                    builder.AppendLine(LPad(CurrencyFormatter.FormatCurrency(OrderLine("LineTotal"), OrderDict("CurrencyGuid")), " ", ColumnWidth(4)))
                Next

                builder.Append(" ", TotalColumnWidth(0))
                builder.Append(RPad(Resources.Language.LABEL_SUB_TOTAL, " ", TotalColumnWidth(1)))
                builder.AppendLine(LPad(CurrencyFormatter.FormatCurrency(OrderDict("SubTotal"), OrderDict("CurrencyGuid")), " ", TotalColumnWidth(2)))
                builder.Append(" ", TotalColumnWidth(0))
                builder.Append(RPad(Resources.Language.LABEL_INVOICE_DISCOUNT, " ", TotalColumnWidth(1)))
                builder.AppendLine(LPad(CurrencyFormatter.FormatCurrency(OrderDict("InvoiceDiscount"), OrderDict("CurrencyGuid")), " ", TotalColumnWidth(2)))
                builder.Append(" ", TotalColumnWidth(0))
                builder.Append(RPad(Resources.Language.LABEL_SERVICE_CHARGE, " ", TotalColumnWidth(1)))
                builder.AppendLine(LPad(CurrencyFormatter.FormatCurrency(OrderDict("ServiceCharge"), OrderDict("CurrencyGuid")), " ", TotalColumnWidth(2)))
                builder.Append(" ", TotalColumnWidth(0))
                builder.Append(RPad(Resources.Language.LABEL_SHIPPING, " ", TotalColumnWidth(1)))
                builder.AppendLine(LPad(CurrencyFormatter.FormatCurrency(OrderDict("ShippingAmount"), OrderDict("CurrencyGuid")), " ", TotalColumnWidth(2)))
                builder.Append(" ", TotalColumnWidth(0))
                builder.Append(RPad(Resources.Language.LABEL_HANDLING, " ", TotalColumnWidth(1)))
                builder.AppendLine(LPad(CurrencyFormatter.FormatCurrency(OrderDict("HandlingAmount"), OrderDict("CurrencyGuid")), " ", TotalColumnWidth(2)))

                If OrderDict("PaymentFeeAmount") > 0 Then
                    builder.Append(" ", TotalColumnWidth(0))
                    builder.Append(RPad(Resources.Language.LABEL_PAYMENT_FEE, " ", TotalColumnWidth(1)))
                    builder.AppendLine(LPad(CurrencyFormatter.FormatCurrency(OrderDict("PaymentFeeAmount"), OrderDict("CurrencyGuid")), " ", TotalColumnWidth(2)))
                End If


            End If
            ' Order totals. 

            builder.Append(" ", TotalColumnWidth(0))
            builder.Append(RPad(Resources.Language.LABEL_TOTAL & " " & Resources.Language.LABEL_EXCLUSIVE_VAT, " ", TotalColumnWidth(1)))
            builder.AppendLine(LPad(CurrencyFormatter.FormatCurrency(OrderDict("Total"), OrderDict("CurrencyGuid")), " ", TotalColumnWidth(2)))

            ' ---------- TAX ----------
            If AppSettings("SHOW_TAX") Then
                Dim taxAmounts As New ExpDictionary()
                Dim invoiceDiscount2 As Decimal = CDblEx(OrderDict("InvoiceDiscountPct"))
                If Not String.IsNullOrEmpty(CType(DBNull2Nothing(OrderDict("VatTypes")), String)) Then
                    Try
                        Dim x As ExpDictionary = UnmarshallDictionary(OrderDict("VatTypes"))
                        For Each vatType As Decimal In x.Keys
                            taxAmounts(vatType) = x(vatType)
                        Next
                    Catch ex As Exception
                        FileLogger.log(ex.Message)
                    End Try
                Else
                    For Each dictOrderLine As ExpDictionary In OrderDict("Lines").values
                        Dim dictOrderLineTaxPct As Decimal = CDblEx(dictOrderLine("TaxPct"))
                        If Not taxAmounts.Exists(dictOrderLineTaxPct) Then
                            taxAmounts.Add(dictOrderLineTaxPct, CDblEx(dictOrderLine("TaxAmount")) * (100 - invoiceDiscount2) / 100)
                        Else
                            Dim calcLine As Decimal = (CDblEx(dictOrderLine("TaxAmount")) * (100 - invoiceDiscount2) / 100)
                            taxAmounts(dictOrderLineTaxPct) = CDblEx(taxAmounts(dictOrderLineTaxPct)) + calcLine
                        End If
                    Next
                End If
                ' Check also the Header for
                ' 1. Handling tax amount
                ' 2. Shipping tax amount
                ' 3. Service charge tax amount

                Dim shippingTaxAmount As Decimal = CDblEx(OrderDict("ShippingAmountInclTax")) - CDblEx(OrderDict("ShippingAmount"))
                Dim handlingTaxAmount As Decimal = CDblEx(OrderDict("HandlingAmountInclTax")) - CDblEx(OrderDict("HandlingAmount"))
                Dim serviceChargeAmount As Decimal = CDblEx(OrderDict("ServiceChargeInclTax")) - CDblEx(OrderDict("ServiceCharge"))

                Dim shippingTaxPct As Decimal = CDblEx(AppSettings("SHIPPING_TAX_PCT"))
                Dim handlingTaxPct As Decimal = CDblEx(OrderDict("TaxPct"))
                Dim serviceChargeTacPct As Decimal = CDblEx(OrderDict("TaxPct")) ' This might be a different value - must be investigated

                ' Add Shipping Tax amounts
                If Not taxAmounts.Exists(shippingTaxPct) Then
                    taxAmounts.Add(shippingTaxPct, shippingTaxAmount)
                Else
                    taxAmounts(shippingTaxPct) = taxAmounts(shippingTaxPct) + shippingTaxAmount
                End If

                ' Add Handling Tax Amounts
                If Not taxAmounts.Exists(handlingTaxPct) Then
                    taxAmounts.Add(handlingTaxPct, handlingTaxAmount)
                Else
                    taxAmounts(handlingTaxPct) = taxAmounts(handlingTaxPct) + handlingTaxAmount
                End If

                ' Add ServiceCharge Tax Amounts
                If Not taxAmounts.Exists(serviceChargeTacPct) Then
                    taxAmounts.Add(serviceChargeTacPct, serviceChargeAmount)
                Else
                    taxAmounts(serviceChargeTacPct) = taxAmounts(serviceChargeTacPct) + serviceChargeAmount
                End If

                For Each kvPair As KeyValuePair(Of Object, Object) In taxAmounts
                    If Not CDblEx(kvPair.Key) = 0 And Not CDblEx(kvPair.Value) = 0 Then
                        builder.Append(" ", TotalColumnWidth(0))
                        builder.Append(RPad(Resources.Language.LABEL_TAXAMOUNT & " " & kvPair.Key & "%", " ", TotalColumnWidth(1)))
                        builder.AppendLine(LPad(CurrencyFormatter.FormatCurrency(kvPair.Value, OrderDict("CurrencyGuid")), " ", TotalColumnWidth(2)))
                    End If
                Next

                'Debug.DebugValue(OrderDict, "OrderDict")
                '---------- TAX END ----------                    

                'builder.Append(" ", TotalColumnWidth(0))
                'builder.Append(RPad(Resources.Language.LABEL_TOTAL & " " & Resources.Language.LABEL_EXCLUSIVE_VAT, " ", TotalColumnWidth(1)))
                'builder.AppendLine(LPad(CurrencyFormatter.FormatCurrency(OrderDict("Total"), OrderDict("CurrencyGuid")), " ", TotalColumnWidth(2)))

                'builder.Append(" ", TotalColumnWidth(0))
                'builder.Append(RPad(Resources.Language.LABEL_TAXAMOUNT, " ", TotalColumnWidth(1)))
                'builder.AppendLine(LPad(CurrencyFormatter.FormatCurrency(OrderDict("TaxAmount"), OrderDict("CurrencyGuid")).ToString(), " ", TotalColumnWidth(2)))

                builder.Append(" ", TotalColumnWidth(0))
                builder.Append(RPad(Resources.Language.LABEL_TOTAL & " " & Resources.Language.LABEL_INCLUSIVE_VAT, " ", TotalColumnWidth(1)))
                builder.AppendLine(LPad(CurrencyFormatter.FormatCurrency(OrderDict("TotalInclTax"), OrderDict("CurrencyGuid")), " ", TotalColumnWidth(2)))

            End If

            Return builder.ToString()
        End Function

        ''' <summary>
        ''' Sends the orderconfirmation in an email
        ''' </summary>
        ''' <param name="OrderDict">The dictionary holding the order</param>
        ''' <param name="BillTo">Billing information dictionary</param>
        ''' <param name="ShipTo">Shipping information dictionary</param>
        ''' <returns>Returns a boolean saying whether the send was successfull or not</returns>
        ''' <remarks></remarks>
        Public Function SendOrderConfirmationEmail(ByVal OrderDict As ExpDictionary, ByVal BillTo As ExpDictionary, ByVal ShipTo As ExpDictionary, Optional OnSecurePage As Boolean = False) As Boolean

            Dim orderhtml As String
            MailUtils.ConfigMailer()
            ' Get the recipients for the email
            Dim recipients As New ExpDictionary()

            If CStrEx(BillTo("EmailAddress")) <> "" Then
                recipients.Add(BillTo("EmailAddress"), BillTo("ContactName"))
            End If
            If CStrEx(ShipTo("ShipToEmailAddress")) <> "" Then
                If CStrEx(ShipTo("ShipToEmailAddress")) <> CStrEx(BillTo("EmailAddress")) Then
                    recipients.Add(ShipTo("ShipToEmailAddress"), ShipTo("ShipToContactName"))
                End If
            End If

            'AM2010060801 - BCC ORDER CONFIRMATION - Start
            ' Log confirmations
            Dim bccAddr As String = AppSettings("MAIL_EMAIL_CONFIRM_LOG")
            Dim bccRecips As ExpDictionary = Nothing
            Dim ccRecips As ExpDictionary = Nothing
            If Not String.IsNullOrEmpty(bccAddr) Then
                bccRecips = New ExpDictionary()
                bccRecips.Add(bccAddr, bccAddr)
            End If

            If CBoolEx(AppSettings("USE_EMBEDDED_CSS_MAIL_METHOD")) Then
                'AM2010060801 - BCC ORDER CONFIRMATION - End
                'create mail
                Dim mail As MailMessage = Mailer.CreateMailMessage(CStrEx(AppSettings("ORDER_EMAIL_FROM")), recipients, _
                    Resources.Language.LABEL_EMAIL_ORDER_CONFIRMATION, CStrEx(AppSettings("MAIL_FORMAT")) = "0", Encoding.UTF8, _
                    Nothing, bccRecips)

                ' Make a HTML representation of the order
                If CStrEx(AppSettings("MAIL_FORMAT")) = "0" Then
                    orderhtml = RenderOrderDict(OrderDict, OnSecurePage)
                    Dim replacements As Dictionary(Of String, String) = New Dictionary(Of String, String)
                    Dim templDir As String = String.Format("~/{0}/", AppSettings("CONFIRM_MAIL_FOLDER"))
                    Dim languageISO As String = CStrEx(HttpContext.Current.Session("LanguageGuid")).ToLower()
                    Dim templateFilePath As String = String.Format("{0}order_{1}.html", templDir, languageISO)
                    replacements.Add("%CONTENT%", orderhtml)
                    replacements.Add("%REFER_NUM%", CStrEx(OrderDict("CustomerReference")))
                    Dim mailHtml As String = MailUtils.MakeReplacements(MailUtils.EmbedCss(templateFilePath, Nothing), replacements)
                    MailUtils.EmbedImages(mailHtml, templDir, mail)

                Else
                    orderhtml = RenderOrderDictText(OrderDict)
                    mail.Body = orderhtml
                End If
                ' Send the mail
                Return Mailer.SendEmail(AppSettings("MAIL_REMOTE_SERVER"), AppSettings("MAIL_REMOTE_SERVER_PORT"), mail, globals.messages)
            Else
                ' Make a HTML representation of the order
                If CStrEx(AppSettings("MAIL_FORMAT")) = "0" Then
                    orderhtml = RenderOrderDict(OrderDict, OnSecurePage)
                Else
                    orderhtml = RenderOrderDictText(OrderDict)
                End If
                ' Send the mail
                'AM2010060801 - BCC ORDER CONFIRMATION - Start
                Return Mailer.SendEmail(AppSettings("MAIL_REMOTE_SERVER"), AppSettings("MAIL_REMOTE_SERVER_PORT"), CStrEx(AppSettings("ORDER_EMAIL_FROM")), recipients, _
                    Resources.Language.LABEL_EMAIL_ORDER_CONFIRMATION, orderhtml, CStrEx(AppSettings("MAIL_FORMAT")) = "0", Encoding.UTF8, globals.messages, bccRecips)
                'AM2010060801 - BCC ORDER CONFIRMATION - End
            End If

        End Function


        'JA2010092201 - RECURRING ORDERS - START
        ''' <summary>
        ''' Sends the cancelled order confirmation in an email
        ''' </summary>
        ''' <param name="OrderDict">The dictionary holding the order</param>
        ''' <returns>Returns a boolean saying whether the send was successfull or not</returns>
        ''' <remarks></remarks>
        Public Function SendCancelledOrderConfirmationEmail(ByVal OrderDict As ExpDictionary, ByVal recurrency As Boolean) As Boolean

            Dim orderhtml As String
            MailUtils.ConfigMailer()
            ' Get the recipients for the email
            Dim recipients As New ExpDictionary()
            Dim subject As String

            If recurrency Then
                subject = Resources.Language.LABEL_EMAIL_CANCELLED_RECURRENCY_CONFIRMATION
            Else
                subject = Resources.Language.LABEL_EMAIL_CANCELLED_ORDER_CONFIRMATION
            End If

            recipients.Add(AppSettings("MAIL_CANCELLED_ORDERS"), AppSettings("MAIL_CANCELLED_ORDERS"))

            If CBoolEx(AppSettings("USE_EMBEDDED_CSS_MAIL_METHOD")) Then
                Dim bccRecips As ExpDictionary = Nothing

                'create mail
                Dim mail As MailMessage = Mailer.CreateMailMessage(CStrEx(AppSettings("ORDER_EMAIL_FROM")), recipients, _
                    subject, CStrEx(AppSettings("MAIL_FORMAT")) = "0", Encoding.UTF8, _
                    Nothing, bccRecips)

                ' Make a HTML representation of the order
                If CStrEx(AppSettings("MAIL_FORMAT")) = "0" Then
                    orderhtml = RenderOrderDict(OrderDict)
                    Dim replacements As Dictionary(Of String, String) = New Dictionary(Of String, String)
                    Dim templDir As String = String.Format("~/{0}/", AppSettings("CONFIRM_MAIL_FOLDER"))
                    Dim languageISO As String = CStrEx(HttpContext.Current.Session("LanguageGuid")).ToLower()
                    Dim templateFilePath As String = String.Format("{0}order_{1}.html", templDir, languageISO)
                    replacements.Add("%CONTENT%", orderhtml)
                    replacements.Add("%REFER_NUM%", CStrEx(OrderDict("CustomerReference")))
                    Dim mailHtml As String = MailUtils.MakeReplacements(MailUtils.EmbedCss(templateFilePath, Nothing), replacements)
                    MailUtils.EmbedImages(mailHtml, templDir, mail)

                Else
                    orderhtml = RenderOrderDictText(OrderDict)
                    mail.Body = orderhtml
                End If
                ' Send the mail
                Return Mailer.SendEmail(AppSettings("MAIL_REMOTE_SERVER"), AppSettings("MAIL_REMOTE_SERVER_PORT"), mail, globals.messages)
            Else
                ' Make a HTML representation of the order
                If CStrEx(AppSettings("MAIL_FORMAT")) = "0" Then
                    orderhtml = RenderOrderDict(OrderDict)
                Else
                    orderhtml = RenderOrderDictText(OrderDict)
                End If
                ' Send the mail
                Return Mailer.SendEmail(AppSettings("MAIL_REMOTE_SERVER"), AppSettings("MAIL_REMOTE_SERVER_PORT"), CStrEx(AppSettings("ORDER_EMAIL_FROM")), recipients, _
                    subject, orderhtml, CStrEx(AppSettings("MAIL_FORMAT")) = "0", Encoding.UTF8, globals.messages, Nothing)
            End If

        End Function
        'JA2010092201 - RECURRING ORDERS - END


        ''' <summary>
        ''' Update MiniCartInfo dictionary and saves info in Cookie.
        ''' </summary>
        ''' <param name="OrderDict">The dictionary holding the order.</param>
        ''' <remarks>
        ''' The values will be stored in the ("MiniCart") cookie and in the MiniCartInfo dictionary.
        ''' Use this MiniCartInfo dictionary from every
        ''' page that need to display MiniCart information. The values stored are:
        ''' Counter: Counter of lines or quantities depending on AppSettings("MINICART_DISPLAY_LINECOUNT")
        ''' Total: Total of order
        ''' TotalInclTax: Total of order Incl Tax
        ''' CurrencyGuid: CurrencyGuid of order, for displaying the values.
        ''' </remarks>
        Public Sub UpdateMiniCartInfo(ByVal OrderDict As ExpDictionary)
            Dim bLineCount As Boolean
            Dim bShowMiniCartTotal As Boolean
            Dim TotalInclTax, Total As Decimal
            Dim Counter As Integer
            Dim key As Object
            Dim CurrencyGuid As String

            bLineCount = CBoolEx(AppSettings("MINICART_DISPLAY_LINECOUNT"))
            bShowMiniCartTotal = CBoolEx(AppSettings("MINICART_DISPLAY_TOTAL"))

            ' If OrderDict is not empty - update info. Else use 0 values
            If OrderDict IsNot Nothing Then
                If bShowMiniCartTotal AndAlso Not CBoolEx(OrderDict("IsCalculated")) Then
                    CalculateOrder(OrderDict)
                End If
                TotalInclTax = CDblEx(OrderDict("TotalInclTax"))
                Total = CDblEx(OrderDict("Total"))
                CurrencyGuid = CStrEx(OrderDict("CurrencyGuid"))

                If OrderDict("Lines") IsNot Nothing Then
                    If OrderDict("Lines").count = 0 Then
                        Counter = 0
                        TotalInclTax = 0
                        Total = 0
                        CurrencyGuid = ""
                    Else
                        If bLineCount Then
                            Counter = OrderDict("Lines").Count
                        Else
                            Counter = 0
                            For Each key In OrderDict("Lines").Keys
                                If IsDict(OrderDict("Lines")(key)) Then
                                    Counter = Counter + CDblEx(OrderDict("Lines")(key)("Quantity"))
                                End If
                            Next
                        End If
                    End If
                Else
                    Counter = 0
                    TotalInclTax = 0
                    Total = 0
                    CurrencyGuid = ""
                End If
            Else
                Counter = 0
                TotalInclTax = 0
                Total = 0
                CurrencyGuid = ""
            End If

            globals.MiniCartInfo = New ExpDictionary()

            ' Set values in MiniCartInfo
            globals.MiniCartInfo.Add("Counter", Counter)
            globals.MiniCartInfo.Add("TotalInclTax", TotalInclTax)
            globals.MiniCartInfo.Add("Total", Total)
            globals.MiniCartInfo.Add("CurrencyGuid", CurrencyGuid)

            ' Set values in non perisistent cookie.
            HttpContext.Current.Response.Cookies("MiniCart")("MiniCartVersionNo") = "EIS30"
            HttpContext.Current.Response.Cookies("MiniCart")("MarshallMiniCartInfo") = MarshallDictionary(globals.MiniCartInfo)

            HttpContext.Current.Response.Cookies("MiniCart").Path = HttpContext.Current.Server.UrlPathEncode(VRoot) & "/"
        End Sub

        ''' <summary>
        ''' Helper function to enable recalculation of the cart.
        ''' </summary>
        ''' <param name="UserGuid">The UserGuid belonging to the user that needs to get the cart recalculated.</param>
        ''' <remarks></remarks>
        Public Shared Sub SetRecalcUserCart(ByVal UserGuid As String)
            Dim sql As String = "UPDATE CartHeader SET IsCalculated = 0, VersionGuid = " & SafeString(GenGuid) & _
                " WHERE MultiCartStatus='ACTIVE' AND UserGuid = " & SafeString(UserGuid)
            excecuteNonQueryDb(sql)
        End Sub

        ''' <summary>
        ''' Helper function - return the correct label describing the Payment Type.
        ''' </summary>
        ''' <param name="PaymentType">PaymentType</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetPaymentTypeName(ByVal PaymentType As String) As String
            Dim resName As String
            Dim retVal As String = PaymentType
            Const SEL_TEMPL As String = "SELECT DISTINCT PaymentName FROM PaymentType WHERE PaymentType = {0}"
            Dim sql As String = String.Format(SEL_TEMPL, SafeString(PaymentType, True))
            resName = CStrEx(getSingleValueDB(sql))
            If resName <> "" Then
                Try
                    retVal = HttpContext.GetGlobalResourceObject("Language", resName).ToString()
                Catch ex As Exception
                    retVal = PaymentType
                End Try
            End If
            Return retVal
        End Function

#End Region
#Region "lib_product"
        ''' <summary>
        ''' Retrieve most bought items.
        ''' </summary>
        ''' <param name="MaxEntries">The number of items to return.</param>
        ''' <returns></returns>
        ''' <remarks>This function caches the result in order to increase performance. 
        ''' Every time the statistics must be updated the cached value must be deleted.
        ''' </remarks>
        Public Function GetMostPopularItems(ByVal MaxEntries As Integer) As ExpDictionary
            Dim sqlString As String
            Dim products As ExpDictionary

            'JA2010030901 - PERFORMANCE -Start
            Dim cachekey As String
            Dim cacheDict As ExpDictionary

            cachekey = "MostPopularItems-" & globals.User("CatalogNo") & "-" & MaxEntries
            '*****CUSTOMER SPECIFIC CATALOGS***** - START
            If CBoolEx(AppSettings("EXPANDIT_US_USE_CSC")) Then
                cachekey &= "-" & CStrEx(globals.User("CustomerGuid"))
            End If
            '*****CUSTOMER SPECIFIC CATALOGS***** - END
            If cachekey <> "" Then cacheDict = EISClass.CacheGet(cachekey) Else cacheDict = Nothing
            If Not cacheDict Is Nothing Then
                Return cacheDict
            Else
                '*****CUSTOMER SPECIFIC CATALOGS***** - START
                If AppSettings("EXPANDIT_US_USE_CSC") Then
                    cacheDict = obj.GetMostPopularItemsCSC(MaxEntries)
                Else
                    '*****CUSTOMER SPECIFIC CATALOGS***** - END
                    'AM2010071901 - ENTERPRISE CSC - Start
                    sqlString = "SELECT TOP " & MaxEntries & " PT.ProductGuid As ProductGuid, COUNT(SL.ProductGuid) AS CountProduct " & _
                        "FROM ShopSalesLine As SL INNER JOIN ProductTable As PT " & _
                        "ON SL.ProductGuid=PT.ProductGuid " & _
                        "INNER JOIN GroupProduct As GP ON PT.ProductGuid=GP.PRoductGuid " & _
                        "WHERE GP.CatalogNo=" & SafeString(globals.User("CatalogNo")) & " " & _
                        "GROUP BY SL.ProductGuid, PT.ProductGuid, PT.ProductName " & _
                        "ORDER BY COUNT(SL.ProductGuid) DESC"

                    'sqlString = "SELECT TOP " & MaxEntries & " ProductTable.ProductGuid As ProductGuid, COUNT(ShopSalesLine.ProductGuid) AS CountProduct " & _
                    '    "FROM ShopSalesLine, ProductTable, GroupProduct " & _
                    '    "WHERE ShopSalesLine.ProductGuid=ProductTable.ProductGuid AND ProductTable.ProductGuid=GroupProduct.ProductGuid " & _
                    '    "GROUP BY ShopSalesLine.ProductGuid, ProductTable.ProductGuid, ProductTable.ProductName " & _
                    '    "ORDER BY COUNT(ShopSalesLine.ProductGuid) DESC"
                    'AM2010071901 - ENTERPRISE CSC - End

                    products = Sql2Dictionaries(sqlString, New String() {"ProductGuid"})
                    If Not products Is Nothing AndAlso Not products Is DBNull.Value AndAlso products.Count > 0 Then
                        CatDefaultLoadProducts(products, False, False, False, New String() {"ProductName"}, Nothing, Nothing)
                    End If
                    cacheDict = products
                End If
                ' Set cache
                If cachekey <> "" Then
                    Dim tableNames As String() = {"ShopSalesLine", "ProductTable", "GroupProduct"}
                    EISClass.CacheSetAggregated(cachekey, cacheDict, tableNames)
                End If
                Return cacheDict
                'JA2010030901 - PERFORMANCE -End
            End If
        End Function

        ''' <summary>
        ''' Retrieve most bought items by the current user.
        ''' </summary>
        ''' <param name="MaxEntries">The number of items to return.</param>
        ''' <returns></returns>
        ''' <remarks>
        ''' This function caches the result in order to increase performance. 
        ''' Every time the statistics must be updated the cached value must be deleted.
        ''' </remarks>
        Public Function GetMyMostPopularItems(ByVal MaxEntries As Integer) As ExpDictionary
            Dim sql As String
            Dim products As ExpDictionary
            'JA2010030901 - PERFORMANCE -Start
            Dim cachekey As String
            Dim cacheDict As ExpDictionary

            cachekey = "MyMostPopularItems-" & globals.User("CatalogNo") & "-" & MaxEntries & "-" & CStrEx(HttpContext.Current.Session("UserGuid"))
            If cachekey <> "" Then cacheDict = EISClass.CacheGet(cachekey) Else cacheDict = Nothing
            If Not cacheDict Is Nothing Then
                Return cacheDict
            Else
                '*****CUSTOMER SPECIFIC CATALOGS***** - START
                If AppSettings("EXPANDIT_US_USE_CSC") Then
                    cacheDict = obj.GetMyMostPopularItemsCSC(MaxEntries)
                Else
                    '*****CUSTOMER SPECIFIC CATALOGS***** - END
                    'AM2010071901 - ENTERPRISE CSC - Start
                    sql = "SELECT TOP " & MaxEntries & " ProductTable.ProductGuid, ProductTable.ProductName , COUNT(ShopSalesLine.ProductGuid) AS CountProduct " & _
                        "FROM ShopSalesLine, ProductTable, GroupProduct, ShopSalesHeader " & _
                        "WHERE ShopSalesLine.ProductGuid=ProductTable.ProductGuid " & _
                        "AND ProductTable.ProductGuid=GroupProduct.ProductGuid " & _
                        "AND ShopSalesHeader.HeaderGuid = ShopSalesLine.HeaderGuid " & _
                        "AND ShopSalesHeader.UserGuid = " & SafeString(HttpContext.Current.Session("UserGuid")) & " " & _
                        "AND GroupProduct.CatalogNo=" & SafeString(globals.User("CatalogNo")) & " " & _
                        "GROUP BY ShopSalesLine.ProductGuid, ProductTable.ProductGuid, ProductTable.ProductName " & _
                        "ORDER BY COUNT(ShopSalesLine.ProductGuid) DESC, ProductTable.ProductGuid"

                    'sql = "SELECT TOP " & MaxEntries & " ProductTable.ProductGuid, ProductTable.ProductName , COUNT(ShopSalesLine.ProductGuid) AS CountProduct " & _
                    '    "FROM ShopSalesLine, ProductTable, GroupProduct, ShopSalesHeader " & _
                    '    "WHERE ShopSalesLine.ProductGuid=ProductTable.ProductGuid " & _
                    '    "AND ProductTable.ProductGuid=GroupProduct.ProductGuid " & _
                    '    "AND ShopSalesHeader.HeaderGuid = ShopSalesLine.HeaderGuid " & _
                    '    "AND ShopSalesHeader.UserGuid = " & SafeString(HttpContext.Current.Session("UserGuid")) & " " & _
                    '    "GROUP BY ShopSalesLine.ProductGuid, ProductTable.ProductGuid, ProductTable.ProductName " & _
                    '    "ORDER BY COUNT(ShopSalesLine.ProductGuid) DESC, ProductTable.ProductGuid"
                    'AM2010071901 - ENTERPRISE CSC - End

                    products = Sql2Dictionaries(sql, New String() {"ProductGuid"})
                    If Not products Is Nothing AndAlso Not products Is DBNull.Value AndAlso products.Count > 0 Then
                        CatDefaultLoadProducts(products, False, False, False, New String() {"ProductName"}, Nothing, Nothing)
                    End If
                    cacheDict = products
                End If
                ' Set cache
                If cachekey <> "" Then
                    Dim tableNames As String() = {"ShopSalesLine", "ProductTable", "GroupProduct", "ShopSalesHeader"}
                    EISClass.CacheSetAggregated(cachekey, cacheDict, tableNames)
                End If
                Return cacheDict
                'JA2010030901 - PERFORMANCE -End
            End If
        End Function

        Public Function GetMostPopularItemsByCategory(ByVal groupGuid As String) As ExpDictionary
            Dim sqlString As String
            Dim products As ExpDictionary
            Dim MaxEntries As Integer
            'JA2010030901 - PERFORMANCE -Start
            Dim cachekey As String
            Dim cacheDict As ExpDictionary

            cachekey = "MostPopularItemsByCategory-" & groupGuid & "-" & globals.User("CatalogNo")
            '*****CUSTOMER SPECIFIC CATALOGS***** - START
            If CBoolEx(AppSettings("EXPANDIT_US_USE_CSC")) Then
                cachekey &= "-" & CStrEx(globals.User("CustomerGuid"))
            End If
            '*****CUSTOMER SPECIFIC CATALOGS***** - END
            If cachekey <> "" Then cacheDict = EISClass.CacheGet(cachekey) Else cacheDict = Nothing
            If Not cacheDict Is Nothing Then
                Return cacheDict
            Else

                '*****CUSTOMER SPECIFIC CATALOGS*****-START
                Dim strCSCAvailableItemsSQL As String
                If CBoolEx(AppSettings("EXPANDIT_US_USE_CSC")) Then
                    strCSCAvailableItemsSQL = obj.getListAvailableItems(globals.User)
                Else
                    strCSCAvailableItemsSQL = "SELECT ProductGuid FROM ProductTable"
                End If
                '*****CUSTOMER SPECIFIC CATALOGS***** - END
                'AM2010071901 - ENTERPRISE CSC - Start
                sqlString = "EXEC [EISEBestSellersByCategory] @strCatalog=" & SafeString(globals.User("CatalogNo")) & ", @strCSCAvailableItemsSQL=" & SafeString(strCSCAvailableItemsSQL) & ", @intParentGuid=" & groupGuid
                'AM2010071901 - ENTERPRISE CSC - End
                products = Sql2Dictionaries(sqlString, New String() {"ProductGuid"})
                'JA2010030901 - PERFORMANCE -Start
                If Not products Is Nothing AndAlso Not products Is DBNull.Value AndAlso products.Count > 0 Then
                    CatDefaultLoadProducts(products, False, False, False, New String() {"ProductName"}, Nothing, Nothing)
                End If
                'JA2010030901 - PERFORMANCE -End
                cacheDict = products
                ' Set cache
                If cachekey <> "" Then
                    Dim tableNames As String() = {"ProductTable", "GroupProduct"}
                    EISClass.CacheSetAggregated(cachekey, cacheDict, tableNames)
                End If
                Return cacheDict
                'JA2010030901 - PERFORMANCE -End
            End If
        End Function


        Public Function GetRelationTypeName(ByVal relationtype As Object) As String
            Dim retv As String

            '     0 = "Customers who bought this product also bought"
            '     1 = "Similar products"
            '     2 = "See also"
            ' Other = "Unknown relation"
            If Len(relationtype) > 2 Then
                relationtype = CLngEx(Mid(relationtype, 3, Len(relationtype) - 3))
            Else
                relationtype = -1
            End If
            Dim rtype As Integer = CType(relationtype, Integer)
            Select Case rtype
                Case 0 : retv = Resources.Language.LABEL_CUSTOMERS_WHO_BOUGHT_THIS_ALSO_BOUGHT
                Case 1 : retv = Resources.Language.LABEL_SIMILAR_PRODUCTS
                Case 2 : retv = Resources.Language.LABEL_SEE_ALSO
                Case Else : retv = Resources.Language.LABEL_UNKNOWN_RELATION
            End Select

            Return retv
        End Function
        '**************UOM*********************START
        'Public Shared Function GetProductUOMQuantity(ByVal prdGuid As String, ByVal UOM As String)

        '    Dim sql As String

        '    sql = "SELECT QtyPerUnitOfMeasure FROM ProductTable INNER JOIN ItemUnitOfMeasure ON (ProductTable.ProductGuid = ItemUnitOfMeasure.ItemNo)  WHERE ProductTable.ProductGuid=" & SafeString(prdGuid) & " AND ItemUnitOfMeasure.Code=" & SafeString(UOM)

        '    If CDblEx(getSingleValueDB(sql)) <= 0 Then
        '        GetProductUOMQuantity = 1
        '    Else
        '        GetProductUOMQuantity = CDblEx(getSingleValueDB(sql))
        '    End If

        'End Function

        'Public Function populateUOM(ByVal productGuid As String)

        '    Dim SQL As String = "SELECT DISTINCT Code FROM ItemUnitOfMeasure WHERE ItemNo=" & SafeString(productGuid)
        '    Dim UOMDict As ExpDictionary

        '    UOMDict = SQL2Dicts(SQL)

        '    Return UOMDict

        'End Function
        '**************UOM*********************END

        '******************Resizer Functonality******************START
        Public Function ThumbnailURLStyleMethod(ByVal image As Object, ByVal wd As Integer, ByVal hd As Integer) As String
            Dim retv As String = CStrEx(image)
            Dim ret As String = ""
            Dim w As Integer = 0
            Dim h As Integer = 0
            If retv <> "" Then
                Dim retv2 As String = ApplicationRoot() & "/" & retv
                If System.IO.File.Exists(retv2) Then
                    Dim img As Bitmap
                    img = New Bitmap(retv2)
                    w = img.Width()
                    h = img.Height()
                    Dim Height As Integer
                    Dim Width As Integer
                    Height = wd * h / w
                    If Height > hd Then
                        Width = hd * (w / h)
                        If Width <= wd Then
                            ret = "height:" & hd & "px; border:0px;"
                        End If
                    Else
                        ret = "width:" & wd & "px; border:0px;"
                    End If
                Else
                    ret = "height:" & hd & "px;width:" & wd & "px;border:0px;"
                    Return ret
                End If
            Else
                ret = "height:" & hd & "px;width:" & wd & "px;border:0px;"
                Return ret
            End If
            'If w <= wd And h <= hd Then
            ' ret = "border:0px;"
            'End If
            Return ret
        End Function
        '******************Resizer Functonality******************END


#End Region
#Region "lib_shipping"

        ' This field is used to specify the field that is calculated.
        Const CalculatedFieldName As String = "TotalInclTax"

        ''' <summary>
        ''' Get Shipping and Handling Companies
        ''' </summary>
        ''' <param name="ProviderGuid">Unique identifier of the shipping and handling provider.</param>
        ''' <param name="bAllData">If true all providers are returned</param>
        ''' <returns>
        ''' Returns a dictionary with the selected provider(s). The resulting dictionary will
        ''' be ordered by the Sortindex field.
        ''' If ProviderGuid is -1 then all the providers are returned.
        ''' </returns>
        ''' <remarks></remarks>
        Public Function GetShippingHandlingProviders(ByVal ProviderGuid As Object, ByVal bAllData As Boolean) As ExpDictionary
            Return GetShippingHandlingProviders(ProviderGuid, bAllData, "")
        End Function

        Public Function GetShippingHandlingProviders(ByVal ProviderGuid As Object, ByVal bAllData As Boolean, ByVal constraint As String) As ExpDictionary
            Dim sql As String
            Dim retDict As New ExpDictionary()

            If globals.User IsNot Nothing Then globals.User = LoadUser(HttpContext.Current.Session("UserGuid"))
            If bAllData Then
                sql = "SELECT * FROM ShippingHandlingProvider"
            Else
                sql = "SELECT ShippingHandlingProviderGuid, ProviderName FROM ShippingHandlingProvider"
            End If
            If CStrEx(ProviderGuid) <> "-1" Then
                sql = sql & " WHERE ShippingHandlingProviderGuid = '" & CStrEx(ProviderGuid) & "'"
            End If
            sql = sql & " " & constraint
            sql = sql & " ORDER BY SortIndex"

            Try
                retDict = SQL2Dicts(sql, "ShippingHandlingProviderGuid", -1)
                If retDict(CStrEx(ProviderGuid)).Exists("ProviderName") Then
                    retDict(CStrEx(ProviderGuid))("ProviderName") = HttpContext.GetGlobalResourceObject("Language", CType(retDict(CStrEx(ProviderGuid))("ProviderName"), String))
                End If
                If retDict(CStrEx(ProviderGuid)).Exists("ProviderDescription") Then
                    retDict(CStrEx(ProviderGuid))("ProviderDescription") = HttpContext.GetGlobalResourceObject("Language", CType(retDict(CStrEx(ProviderGuid))("ProviderDescription"), String))
                End If
            Catch ex As Exception

            End Try

            Return retDict

        End Function

        ''' <summary>
        ''' Function for getting all the prices for a provider
        ''' </summary>
        ''' <param name="ProviderGuid">Unique identifier for the shipping/handling provider.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function GetShippingAndHandlingData(ByVal ProviderGuid As String) As ExpDictionary
            Dim sqlThis As String
            Dim retval As ExpDictionary = GetShippingHandlingProviders(ProviderGuid, True)
            Dim sql As String = "SELECT * FROM ShippingHandlingPrice WHERE ShippingHandlingProviderGuid = '%PROVIDERGUID%'"
            For Each aKey As KeyValuePair(Of Object, Object) In retval
                sqlThis = Replace(sql, "%PROVIDERGUID%", retval(aKey.Key)("ShippingHandlingProviderGuid"))
                retval(aKey.Key)("Pricelist") = SQL2Dicts(sqlThis, "", -1)
            Next
            Return retval
        End Function

        ''' <summary>
        ''' Calculate Shipping and Handling Prices
        ''' </summary>
        ''' <param name="OrderDict"></param>
        ''' <param name="Context"></param>
        ''' <remarks>Prices are stored in the table using the default site currency.
        ''' The tax pct is taken from the business logic. (OrderDict("TaxPct"))
        ''' </remarks>
        Public Sub CalcShippingHandling(ByVal OrderDict As ExpDictionary, ByVal Context As ExpDictionary)
            Dim key As String
            Dim sum As Decimal
            Dim sql As String
            Dim ProviderGuid As String
            Dim tmp As ExpDictionary
            'AM2010042601 - ENTERPRISE SHIPPING - Start
            Dim weight As Decimal
            Dim orderBy As String
            Dim foundCountry As Boolean = False
            Dim foundState As Boolean = False
            Dim foundZipCode As Boolean = False

            '******DA.AM*******  OrderDict("Total") = CDblEx(OrderDict("Total")) - CDblEx(OrderDict("ShippingAmount")) - CDblEx(OrderDict("HandlingAmount"))
            ProviderGuid = CStrEx(OrderDict("ShippingHandlingProviderGuid"))
            'Check if there is a line for the country, otherwise the main query will look for NULL or blank on the country.
            If CBoolEx(AppSettings("ENTERPRISE_SHIPPING_USE_COUNTRIES")) Then
                If CStrEx(OrderDict("ShipToCountryGuid")) <> "" Then
                    sql = "SELECT COUNT(*) FROM ShippingHandlingPrice WHERE ShippingHandlingProviderGuid = '" & ProviderGuid & "' AND CountryGuid LIKE " & SafeString("%" & CStrEx(OrderDict("ShipToCountryGuid")) & "%")
                    If getSingleValueDB(sql) > 0 Then
                        foundCountry = True
                    End If
                End If
            End If
            'Check if there is a line for the state, otherwise the main query will look for NULL or blank on the state.
            If CBoolEx(AppSettings("ENTERPRISE_SHIPPING_USE_STATES")) Then
                If CStrEx(OrderDict("ShipToStateName")) <> "" Then
                    sql = "SELECT COUNT(*) FROM ShippingHandlingPrice WHERE ShippingHandlingProviderGuid = '" & ProviderGuid & "' AND "
                    If foundCountry Then
                        sql &= "CountryGuid LIKE " & SafeString("%" & CStrEx(OrderDict("ShipToCountryGuid")) & "%") & " AND "
                    Else
                        sql &= " CountryGuid IS NULL or CountryGuid=''  AND  "
                    End If
                    sql &= "StateGuid LIKE " & SafeString("%" & CStrEx(OrderDict("ShipToStateName")) & "%")
                    If getSingleValueDB(sql) > 0 Then
                        foundState = True
                    End If
                End If
            End If
            'Check if there is a line for the post code, otherwise the main query will look for NULL or blank on the post code.
            'This will be implemented upon customer's request
            'AM2010042601 - ENTERPRISE SHIPPING - End

            Dim nSelKey, aTaxPct As Object

            If Not IsDict(OrderDict("Lines")) Then Exit Sub

            ' Only recalculate the shipping and handling if it has changed, e.i. ReCalculated is true.
            If Not CBoolEx(OrderDict("ReCalculated")) Then Exit Sub

            ' Find the calculated value
            sum = 0
            'AM2010042601 - ENTERPRISE SHIPPING - Start
            weight = 0
            For Each key In OrderDict("Lines").keys
                weight = weight + CDblEx(OrderDict("Lines")(key)("ShippingWeight")) * OrderDict("Lines")(key)("Quantity")
                'AM2010042601 - ENTERPRISE SHIPPING - End
                sum = sum + OrderDict("Lines")(key)(CalculatedFieldName)
            Next

            OrderDict("CalculatedValue") = sum

            ' Convert the value to the site value, for making a correct lookup.
            sum = currency.ConvertCurrency(sum, Context("CurrencyGuid"), Context("DefaultCurrency"))

            'AM2010042601 - ENTERPRISE SHIPPING - Start
            ' Make a lookup of the price value. These values are considered not to include tax
            sql = "SELECT * FROM ShippingHandlingPrice " & _
                " WHERE ShippingHandlingProviderGuid = '" & ProviderGuid & "'"

            'Check the order's amount, if configured to do so
            If CBoolEx(AppSettings("ENTERPRISE_SHIPPING_USE_ORDER_VALUE")) Then
                sql &= "   AND ( CalculatedValue >= " & SafeFloat(sum) & _
                                "       OR CalculatedValue IS NULL ) "
                orderBy = " ORDER BY CalculatedValue"
                'Check the order's weight, if configured to do so
            ElseIf CBoolEx(AppSettings("ENTERPRISE_SHIPPING_USE_WEIGHT")) Then
                sql &= "   AND ( Weight >= " & SafeFloat(weight) & _
                                "       OR Weight IS NULL ) "
                orderBy = " ORDER BY Weight"
            End If
            'Check the country, if configured to do so
            'If CBoolEx(AppSettings("ENTERPRISE_SHIPPING_USE_COUNTRIES")) Then
            If foundCountry Then
                sql &= "   AND ( CountryGuid LIKE " & SafeString("%" & CStrEx(OrderDict("ShipToCountryGuid")) & "%") & " ) "
            Else
                sql &= "   AND ( CountryGuid IS NULL or CountryGuid=''  ) "
            End If
            'End If
            'Check the state, if configured to do so
            'If CBoolEx(AppSettings("ENTERPRISE_SHIPPING_USE_STATES")) Then
            If foundState Then
                sql &= "   AND ( StateGuid LIKE " & SafeString("%" & CStrEx(OrderDict("ShipToStateName")) & "%") & " ) "
            Else
                sql &= "   AND ( StateGuid IS NULL or StateGuid=''  ) "
            End If
            'End If
            'Check the post code, if configured to do so
            'This will be implemented upon customer's request

            'Add the order by portion
            If CStrEx(orderBy) = "" Then
                orderBy = " ORDER BY ShippingAmount, HandlingAmount"
            Else
                orderBy &= ", ShippingAmount, HandlingAmount"
            End If
            sql &= CStrEx(orderBy)

            'AM2010042601 - ENTERPRISE SHIPPING - End


            tmp = SQL2Dicts(sql, "", -1)

            'AM2010042601 - ENTERPRISE SHIPPING - Start
            ' Find the price to be used, if none then add 0 for shipping and handling
            If tmp.Count = 0 Then
                OrderDict("ShippingAmount") = RoundEx(currency.ConvertCurrency(0, Context("DefaultCurrency"), Context("CurrencyGuid")), CIntEx(AppSettings("NUMDECIMALPLACES")))
                OrderDict("HandlingAmount") = RoundEx(currency.ConvertCurrency(0, Context("DefaultCurrency"), Context("CurrencyGuid")), CIntEx(AppSettings("NUMDECIMALPLACES")))
            Else
                ' If there are more than one value then ensure that a null value is not
                ' selected if a more correct value exists.
                Dim arrKeys(tmp.Count - 1) As Object
                tmp.Keys.CopyTo(arrKeys, 0)
                If tmp.Count = 1 Then
                    nSelKey = 0
                Else
                    nSelKey = 0
                    'AM2010042601 - ENTERPRISE SHIPPING - Start
                    If CBoolEx(AppSettings("ENTERPRISE_SHIPPING_USE_ORDER_VALUE")) Then
                        If IsNull(tmp(arrKeys(0))("CalculatedValue")) Then nSelKey = 1
                    ElseIf CBoolEx(AppSettings("ENTERPRISE_SHIPPING_USE_WEIGHT")) Then
                        If IsNull(tmp(arrKeys(0))("Weight")) Then nSelKey = 1
                    End If
                    'AM2010042601 - ENTERPRISE SHIPPING - End
                End If

                ' Convert values to the customers currency
                OrderDict("ShippingAmount") = RoundEx(currency.ConvertCurrency(tmp(arrKeys(nSelKey))("ShippingAmount"), Context("DefaultCurrency"), Context("CurrencyGuid")), CIntEx(AppSettings("NUMDECIMALPLACES")))
                OrderDict("HandlingAmount") = RoundEx(currency.ConvertCurrency(tmp(arrKeys(nSelKey))("HandlingAmount"), Context("DefaultCurrency"), Context("CurrencyGuid")), CIntEx(AppSettings("NUMDECIMALPLACES")))
            End If
            'AM2010042601 - ENTERPRISE SHIPPING - End
            '*****promotions*****-start
            If AppSettings("EXPANDIT_US_USE_PROMOTION_CODES") Then
                promotions.CalcShippingHandlingDiscount(OrderDict)
            End If
            '*****promotions*****-end

            '<!-- ' ExpandIT US - ANA - US Sales Tax Shipping - Start -->
            ' Use shipping tax pct from AppSettings

            'AM2010042901 - US SALES TAX ON HANDLING - Start
            If Not CBoolEx(AppSettings("EXPANDIT_US_USE_US_SALES_TAX")) Then
                'AM2010042901 - US SALES TAX ON HANDLING - End
                If OrderDict("TaxPct") Is Nothing Or OrderDict("TaxPct") Is DBNull.Value Then
                    OrderDict("TaxPct") = 0
                End If
                aTaxPct = (OrderDict("TaxPct") / 100)
                OrderDict("TaxAmount") = OrderDict("TaxAmount") + (aTaxPct * OrderDict("HandlingAmount"))
                OrderDict("HandlingAmountInclTax") = RoundEx(OrderDict("HandlingAmount") * (1 + aTaxPct), CLngEx(AppSettings("NUMDECIMALPLACES")))
                OrderDict("ShippingAmountInclTax") = RoundEx(OrderDict("ShippingAmount") * (1 + CDblEx(AppSettings("SHIPPING_TAX_PCT")) / 100), CLngEx(AppSettings("NUMDECIMALPLACES")))
                'Test
                Dim shippingTaxAmount As Decimal = CDblEx(OrderDict("ShippingAmountInclTax")) - CDblEx(OrderDict("ShippingAmount"))
                OrderDict("TaxAmount") = CDblEx(OrderDict("TaxAmount")) + shippingTaxAmount
                ' The total including tax
                OrderDict("TotalInclTax") = CDblEx(OrderDict("TotalInclTax")) + CDblEx(OrderDict("HandlingAmountInclTax")) + CDblEx(OrderDict("ShippingAmountInclTax"))
                OrderDict("ReCalculated") = False
            End If
            '<!-- ' ExpandIT US - ANA - US Sales Tax Shipping - End -->
            ' The value whithout tax            
            OrderDict("Total") = CDblEx(OrderDict("Total")) + CDblEx(OrderDict("ShippingAmount")) + CDblEx(OrderDict("HandlingAmount"))


            '<!-- ' ExpandIT US - ANA - US Sales Tax Shipping - Start -->
            If AppSettings("EXPANDIT_US_USE_SHIPPING_TAX") Then
                OrderDict("HandlingAmountInclTax") = OrderDict("HandlingAmount")
                OrderDict("ShippingAmountInclTax") = OrderDict("ShippingAmount")
                OrderDict("TotalInclTax") = CDblEx(OrderDict("TotalInclTax")) + CDblEx(OrderDict("HandlingAmountInclTax")) + CDblEx(OrderDict("ShippingAmountInclTax"))
                Dim USSalesTax As New USSalesTax()
                USSalesTax.NFCalculateTaxShipping(OrderDict, Context)
            End If
            '<!-- ' ExpandIT US - ANA - US Sales Tax Shipping - End -->
            'AM2010042901 - US SALES TAX ON HANDLING - Start
            If AppSettings("EXPANDIT_US_USE_HANDLING_TAX") Then
                If Not CBoolEx(AppSettings("EXPANDIT_US_USE_SHIPPING_TAX")) Then
                    OrderDict("HandlingAmountInclTax") = OrderDict("HandlingAmount")
                    OrderDict("ShippingAmountInclTax") = OrderDict("ShippingAmount")
                    OrderDict("TotalInclTax") = CDblEx(OrderDict("TotalInclTax")) + CDblEx(OrderDict("HandlingAmountInclTax")) + CDblEx(OrderDict("ShippingAmountInclTax"))
                End If
                Dim USSalesTax As New USSalesTax()
                USSalesTax.NFCalculateTaxHandling(OrderDict, Context)
            End If
            'AM2010042901 - US SALES TAX ON HANDLING - End

        End Sub

        ''' <summary>
        ''' Calculate the shipping and handling costs based on different shipping providers 
        ''' </summary>
        ''' <param name="OrderDict"></param>
        ''' <param name="Context"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function CalcShippingHandlingOptions(ByVal OrderDict As ExpDictionary, ByVal Context As ExpDictionary, ByVal providerGuid As String) As ExpDictionary
            Dim key As String
            Dim sum As Decimal
            Dim sql As String
            Dim tmp As ExpDictionary
            Dim shippingAmounts As New ExpDictionary()
            Dim nSelKey, aTaxPct As Object

            'AM2010042601 - ENTERPRISE SHIPPING - Start
            Dim weight As Decimal
            Dim orderBy As String
            Dim foundCountry As Boolean = False
            Dim foundState As Boolean = False
            Dim foundZipCode As Boolean = False
            'Check if there is a line for the country, otherwise the main query will look for NULL or blank on the country.
            If CBoolEx(AppSettings("ENTERPRISE_SHIPPING_USE_COUNTRIES")) Then
                If CStrEx(OrderDict("ShipToCountryGuid")) <> "" Then
                    sql = "SELECT COUNT(*) FROM ShippingHandlingPrice WHERE ShippingHandlingProviderGuid = '" & providerGuid & "' AND CountryGuid LIKE " & SafeString("%" & CStrEx(OrderDict("ShipToCountryGuid")) & "%")
                    If getSingleValueDB(sql) > 0 Then
                        foundCountry = True
                    End If
                End If
            End If
            'Check if there is a line for the state, otherwise the main query will look for NULL or blank on the state.
            If CBoolEx(AppSettings("ENTERPRISE_SHIPPING_USE_STATES")) Then
                If CStrEx(OrderDict("ShipToStateName")) <> "" Then
                    sql = "SELECT COUNT(*) FROM ShippingHandlingPrice WHERE ShippingHandlingProviderGuid = '" & providerGuid & "' AND "
                    If foundCountry Then
                        sql &= "CountryGuid LIKE " & SafeString("%" & CStrEx(OrderDict("ShipToCountryGuid")) & "%") & " AND "
                    Else
                        sql &= " CountryGuid IS NULL or CountryGuid=''  AND  "
                    End If
                    sql &= "StateGuid LIKE " & SafeString("%" & CStrEx(OrderDict("ShipToStateName")) & "%")
                    If getSingleValueDB(sql) > 0 Then
                        foundState = True
                    End If
                End If
            End If
            'Check if there is a line for the post code, otherwise the main query will look for NULL or blank on the post code.
            'This will be implemented upon customer's request
            'AM2010042601 - ENTERPRISE SHIPPING - End

            ' Find the calculated value
            sum = 0
            'AM2010042601 - ENTERPRISE SHIPPING - Start
            weight = 0
            For Each key In OrderDict("Lines").keys
                weight = weight + CDblEx(OrderDict("Lines")(key)("ShippingWeight")) * OrderDict("Lines")(key)("Quantity")
                'AM2010042601 - ENTERPRISE SHIPPING - End
                sum = sum + OrderDict("Lines")(key)(CalculatedFieldName)
            Next

            shippingAmounts("CalculatedValue") = sum

            ' Convert the value to the site value, for making a correct lookup.
            sum = currency.ConvertCurrency(sum, Context("CurrencyGuid"), Context("DefaultCurrency"))

            'AM2010042601 - ENTERPRISE SHIPPING - Start
            ' Make a lookup of the price value. These values are considered not to include tax
            sql = "SELECT * FROM ShippingHandlingPrice " & _
                " WHERE ShippingHandlingProviderGuid = '" & providerGuid & "'"

            'Check the order's amount, if configured to do so
            If CBoolEx(AppSettings("ENTERPRISE_SHIPPING_USE_ORDER_VALUE")) Then
                sql &= "   AND ( CalculatedValue >= " & SafeFloat(sum) & _
                                "       OR CalculatedValue IS NULL ) "
                orderBy = " ORDER BY CalculatedValue"
                'Check the order's weight, if configured to do so
            ElseIf CBoolEx(AppSettings("ENTERPRISE_SHIPPING_USE_WEIGHT")) Then
                sql &= "   AND ( Weight >= " & SafeFloat(weight) & _
                                "       OR Weight IS NULL ) "
                orderBy = " ORDER BY Weight"
            End If
            'Check the country, if configured to do so
            'If CBoolEx(AppSettings("ENTERPRISE_SHIPPING_USE_COUNTRIES")) Then
            If foundCountry Then
                sql &= "   AND ( CountryGuid LIKE " & SafeString("%" & CStrEx(OrderDict("ShipToCountryGuid")) & "%") & " ) "
            Else
                sql &= "   AND ( CountryGuid IS NULL or CountryGuid=''  ) "
            End If
            'End If
            'Check the state, if configured to do so
            'If CBoolEx(AppSettings("ENTERPRISE_SHIPPING_USE_STATES")) Then
            If foundState Then
                sql &= "   AND ( StateGuid LIKE " & SafeString("%" & CStrEx(OrderDict("ShipToStateName")) & "%") & " ) "
            Else
                sql &= "   AND ( StateGuid IS NULL or StateGuid=''  ) "
            End If
            'End If
            'Check the post code, if configured to do so
            'This will be implemented upon customer's request

            'Add the order by portion
            If CStrEx(orderBy) = "" Then
                orderBy = " ORDER BY ShippingAmount, HandlingAmount"
            Else
                orderBy &= ", ShippingAmount, HandlingAmount"
            End If
            sql &= CStrEx(orderBy)

            'AM2010042601 - ENTERPRISE SHIPPING - End


            tmp = SQL2Dicts(sql, "", -1)

            'AM2010042601 - ENTERPRISE SHIPPING - Start
            ' Find the price to be used, if none use 0
            If tmp.Count = 0 Then
                shippingAmounts("ShippingAmount") = RoundEx(currency.ConvertCurrency(0, Context("DefaultCurrency"), Context("CurrencyGuid")), CIntEx(AppSettings("NUMDECIMALPLACES")))
                shippingAmounts("HandlingAmount") = RoundEx(currency.ConvertCurrency(0, Context("DefaultCurrency"), Context("CurrencyGuid")), CIntEx(AppSettings("NUMDECIMALPLACES")))
            Else
                ' If there are more than one value then ensure that a null value is not
                ' selected if a more correct value exists.
                Dim arrKeys(tmp.Count - 1) As Object
                tmp.Keys.CopyTo(arrKeys, 0)
                If tmp.Count = 1 Then
                    nSelKey = 0
                Else
                    nSelKey = 0
                    If CBoolEx(AppSettings("ENTERPRISE_SHIPPING_USE_ORDER_VALUE")) Then
                        If IsNull(tmp(arrKeys(0))("CalculatedValue")) Then nSelKey = 1
                    ElseIf CBoolEx(AppSettings("ENTERPRISE_SHIPPING_USE_WEIGHT")) Then
                        If IsNull(tmp(arrKeys(0))("Weight")) Then nSelKey = 1
                    End If
                End If

                ' Convert values to the customers currency
                shippingAmounts("ShippingAmount") = RoundEx(currency.ConvertCurrency(tmp(arrKeys(nSelKey))("ShippingAmount"), Context("DefaultCurrency"), Context("CurrencyGuid")), CIntEx(AppSettings("NUMDECIMALPLACES")))
                shippingAmounts("HandlingAmount") = RoundEx(currency.ConvertCurrency(tmp(arrKeys(nSelKey))("HandlingAmount"), Context("DefaultCurrency"), Context("CurrencyGuid")), CIntEx(AppSettings("NUMDECIMALPLACES")))
            End If
            'AM2010042601 - ENTERPRISE SHIPPING - End
            '*****promotions*****-start
            If AppSettings("EXPANDIT_US_USE_PROMOTION_CODES") Then
                promotions.CalcShippingHandlingDiscount(OrderDict)
            End If
            '*****promotions*****-end

            ' Use shipping tax pct from AppSettings
            If OrderDict("TaxPct") Is Nothing Or OrderDict("TaxPct") Is DBNull.Value Then
                OrderDict("TaxPct") = 0
            End If
            aTaxPct = (OrderDict("TaxPct") / 100)
            shippingAmounts("TaxAmount") = OrderDict("TaxAmount") + (aTaxPct * shippingAmounts("HandlingAmount"))
            shippingAmounts("HandlingAmountInclTax") = RoundEx(shippingAmounts("HandlingAmount") * (1 + aTaxPct), CLngEx(AppSettings("NUMDECIMALPLACES")))
            shippingAmounts("ShippingAmountInclTax") = RoundEx(shippingAmounts("ShippingAmount") * (1 + CDblEx(AppSettings("SHIPPING_TAX_PCT")) / 100), CLngEx(AppSettings("NUMDECIMALPLACES")))

            ' The value whithout tax
            shippingAmounts("Total") = RoundEx(shippingAmounts("Total") + shippingAmounts("ShippingAmount") + shippingAmounts("HandlingAmount"), CLngEx(AppSettings("NUMDECIMALPLACES")))

            ' The total including tax
            shippingAmounts("TotalInclTax") = RoundEx(shippingAmounts("TotalInclTax") + shippingAmounts("HandlingAmountInclTax") + shippingAmounts("ShippingAmountInclTax"), CLngEx(AppSettings("NUMDECIMALPLACES")))

            Return shippingAmounts
        End Function

#End Region

        ''' <summary>
        ''' Find a control with a specific programatic ID. All levels of sub controls under parent are searched
        ''' </summary>
        ''' <param name="parent"></param>
        ''' <param name="ID"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function FindChildControl(ByVal parent As Control, ByVal ID As String) As Control
            Dim retv As Control = Nothing
            For Each child As Control In parent.Controls
                If child.ID = ID Then Return child
                retv = FindChildControl(child, ID)
                If retv IsNot Nothing Then Return retv
            Next
            Return Nothing
        End Function

        ''' <summary>
        ''' Adds a value to the OnKeyDown Event on an input field. 
        ''' This event will fire when the enter button is pressed.
        ''' </summary>
        ''' <param name="id">The id of the control to fire the click event on</param>
        ''' <returns>The function returns a javascript string</returns>
        ''' <remarks>
        ''' This function returns a script string that will make it possible
        ''' to fire a click event with the use of the enter button
        ''' </remarks>
        Public Function addOnKeyDownAttributeValue(ByVal id As String) As String
            Return "if(event.which || event.keyCode){if ((event.which == 13) || (event.keyCode == 13))" & _
                "{document.getElementById('" + id + "').click();return false;}} else {return true}; "
        End Function

        Public Function replaceParametersWithUserDictValues(ByVal value As String, ByVal userDict As ExpDictionary) As ExpDictionary
            Dim keyValArray() As String = Split(AppSettings(value), "�")
            Dim replaceDict As New ExpDictionary()
            For i As Integer = 0 To keyValArray.Length - 1
                Dim temp() As String = Split(keyValArray(i), "|")
                replaceDict.Add(Trim(temp(0)), Trim(CStrEx(userDict(temp(1)))))
            Next
            Return replaceDict
        End Function

        ''' <summary>
        ''' Returns the content of a html file and a css file as a string representation
        ''' of one html file with css styles applied, in the requested encoding.
        ''' </summary>
        ''' <param name="htmlStr"></param>    
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function replaceHtmlFileContent(ByVal htmlStr As String, ByVal replaceDict As ExpDictionary) As String
            Dim returnStr As String = Nothing
            Try
                For Each item As KeyValuePair(Of Object, Object) In replaceDict
                    htmlStr = Replace(htmlStr, CStrEx(item.Key), CStrEx(item.Value))
                Next
                returnStr = htmlStr
            Catch ex As Exception

            End Try

            Return returnStr

        End Function


        '<!--JM2010061001 - LINK TO - Start-->
        Public Function getLinkTo(ByVal PropTransText As String) As ExpDictionary
            Dim sqlPropGrp As String = ""
            Dim groupsPrd As New ExpDictionary
            'ExpandIT US - USE CSC - Start
            If CBoolEx(AppSettings("EXPANDIT_US_USE_CSC")) Then
                Return obj.getLinkToCSC(PropTransText)
            End If
            'ExpandIT US - USE CSC - End
            'AM2010071901 - ENTERPRISE CSC - Start
            sqlPropGrp = "SELECT TOP 1 PropOwnerRefGuid, PropOwnerTypeGuid FROM PropVal INNER JOIN PropTrans" & _
                       " ON PropVal.PropValGuid=PropTrans.PropValGuid" & _
                       " WHERE (PropLangGuid IS NULL OR PropLangGuid =" & SafeString(HttpContext.Current.Session("LanguageGuid").ToString) & ")" & _
                       " AND (PropGuid='LINKTO')" & _
                       " AND (PropTransText=" & SafeString(PropTransText) & ")" & _
                       " AND ((PropOwnerTypeGuid='PRD' AND PropOwnerRefGuid IN (SELECT DISTINCT ProductGuid FROM GroupProduct WHERE CatalogNo=" & SafeString(globals.User("CatalogNo")) & "))" & _
                       " OR (PropOwnerTypeGuid='GRP' AND PropOwnerRefGuid IN (SELECT DISTINCT GroupGuid FROM GroupTable WHERE CatalogNo=" & SafeString(globals.User("CatalogNo")) & ")))"
            'AM2010071901 - ENTERPRISE CSC - End
            groupsPrd = SQL2Dicts(sqlPropGrp)

            Return groupsPrd

        End Function

        Public Function findLinkToHtml(ByVal html As String)

            html = Replace(html, "%linkto%", "%LINKTO%")
            html = Replace(html, "%LINKTO%", VRoot & "/link.aspx?GetGroup=1&LinkTo=")

            html = Replace(html, "%vroot%", "%VROOT%")
            html = Replace(html, "%VROOT%", VRoot)
            Return html

        End Function
        '<!--JM2010061001 - LINK TO - End-->

        'AM2010071901 - ENTERPRISE CSC - Start
        Public Function getCSC()
            Dim catalogNo As String = ""
            Dim sql As String = ""

            sql = "SELECT CatalogNo FROM EECMCatalog WHERE (CustomerGuid = '')"
            catalogNo = CStrEx(getSingleValueDB(sql))

            Return catalogNo
        End Function
        'AM2010071901 - ENTERPRISE CSC - End

        'JA2010060101 - ALERTS - Start
        Public Function getAlerts()

            Dim SQL As String = ""
            If CBoolEx(globals.User("Anonymous")) Then
                SQL = "SELECT * FROM EEMCAlerts WHERE Disabled = 0 AND AlertType='SITEMESSAGE' AND (CustomerType = 0) AND (StartingDate <= getDate()) AND (EndingDate >= getDate() OR EndingDate IS NULL) ORDER BY CustomerType ASC"
            ElseIf CBoolEx(globals.User("B2B")) Then
                SQL = "SELECT * FROM EEMCAlerts WHERE Disabled = 0 AND AlertType='SITEMESSAGE' AND (CustomerType = 0 Or (CustomerType = 1 AND CustomerNo=" & SafeString(globals.User("CustomerGuid")) & ")) AND (StartingDate <= getDate()) AND (EndingDate >= getDate() OR EndingDate IS NULL) ORDER BY CustomerType ASC"
            ElseIf CBoolEx(globals.User("B2C")) Then
                SQL = "SELECT * FROM EEMCAlerts WHERE Disabled = 0 AND AlertType='SITEMESSAGE' AND (CustomerType = 0 Or CustomerType = 2) AND (StartingDate <= getDate()) AND (EndingDate >= getDate() OR EndingDate IS NULL) ORDER BY CustomerType ASC"
            End If
            Dim retv As ExpDictionary = SQL2Dicts(SQL, "AlertNo")

            Return retv
        End Function
        'JA2010060101 - ALERTS - End

        'JA2010061701 - SLIDE SHOW - Start
        Public Function CreateRelatedItemsDictForSlide(ByVal products As ExpDictionary, Optional ByVal relationType As Integer = -1) As ExpDictionary
            Dim sql As String = ""
            Dim sql2 As String = ""
            Dim SlideDict As ExpDictionary
            Dim counter As Integer = 0
            Dim numberOfItemsReq As Integer = AppSettings("RELATED_SLIDE_NUMBER_OF_ITEMS")
            Dim numberOfItems As Integer = 0

            If products.Count > 1 Then
                While numberOfItems < numberOfItemsReq
                    sql = ""
                    sql2 = ""
                    counter += 1
                    For Each product As ExpDictionary In products.Values
                        If sql <> "" Then
                            sql &= " UNION "
                        End If
                        If AppSettings("REQUIRE_CATALOG_ENTRY") Then
                            'JA2010030901 - PERFORMANCE -Start
                            'AM2010071901 - ENTERPRISE CSC - Start
                            sql &= "(SELECT [RelatedToProductGuid] AS [ProductGuid] FROM (SELECT  TOP " & counter & " [RelatedToProductGuid] FROM " & _
                                "[ProductRelation] INNER JOIN (SELECT DISTINCT [GroupProduct].[ProductGuid] FROM [GroupProduct] "
                            'ExpandIT US - USE CSC - Start
                            If CBoolEx(AppSettings("EXPANDIT_US_USE_CSC")) Then
                                If globals.Context Is Nothing Then
                                    globals.Context = LoadContext(globals.User)
                                End If
                                sql &= " INNER JOIN (" & obj.getListAvailableItems(globals.User) & ") AS AvailableItems ON GroupProduct.ProductGuid=AvailableItems.ProductGuid "
                            End If
                            'ExpandIT US - USE CSC - End
                            sql &= " WHERE CatalogNo=" & SafeString(globals.User("CatalogNo")) & " ) AS GP ON [RelatedToProductGuid]=GP.[ProductGuid] WHERE [ProductRelation]." & _
                                "[ProductGuid]=" & SafeString(product("ProductGuid"))
                            'AM2010071901 - ENTERPRISE CSC - End
                            'JA2010030901 - PERFORMANCE -End
                        Else
                            sql &= "(SELECT [RelatedToProductGuid] AS [ProductGuid] FROM (SELECT  TOP " & counter & " [RelatedToProductGuid] FROM [ProductRelation] WHERE" & _
                                " [ProductRelation].[ProductGuid]=" & SafeString(product("ProductGuid"))
                        End If
                        If relationType <> -1 Then
                            sql &= " AND ProductRelationType=" & relationType & " ORDER BY SortIndex) AS TEMP)"
                        Else
                            sql &= " ORDER BY SortIndex) AS TEMP)"
                        End If
                    Next
                    sql2 = "SELECT COUNT (DISTINCT [ProductGuid]) FROM ( " & sql & " ) AS TEMP1"
                    numberOfItems = getSingleValueDB(sql2)
                    If counter = numberOfItemsReq Then
                        Exit While
                    End If
                End While
            Else
                For Each product As ExpDictionary In products.Values
                    If AppSettings("REQUIRE_CATALOG_ENTRY") Then
                        'JA2010030901 - PERFORMANCE -Start
                        sql = "(SELECT [RelatedToProductGuid] AS [ProductGuid] FROM (SELECT  TOP " & numberOfItemsReq & " [RelatedToProductGuid] FROM " & _
                            "[ProductRelation] INNER JOIN (SELECT DISTINCT [GroupProduct].[ProductGuid] FROM [GroupProduct] "
                        'ExpandIT US - USE CSC - Start
                        If CBoolEx(AppSettings("EXPANDIT_US_USE_CSC")) Then
                            If globals.Context Is Nothing Then
                                globals.Context = LoadContext(globals.User)
                            End If
                            sql &= " INNER JOIN (" & obj.getListAvailableItems(globals.User) & ") AS AvailableItems ON GroupProduct.ProductGuid=AvailableItems.ProductGuid "
                        End If
                        'ExpandIT US - USE CSC - End
                        'AM2010071901 - ENTERPRISE CSC - Start
                        sql &= "WHERE CatalogNo=" & SafeString(globals.User("CatalogNo")) & " ) AS GP ON [RelatedToProductGuid]=GP.[ProductGuid] WHERE [ProductRelation]." & _
                            "[ProductGuid]=" & SafeString(product("ProductGuid"))
                        'AM2010071901 - ENTERPRISE CSC - End
                        'JA2010030901 - PERFORMANCE -End
                    Else
                        sql = "(SELECT [RelatedToProductGuid] AS [ProductGuid] FROM (SELECT  TOP " & numberOfItemsReq & " [RelatedToProductGuid] FROM [ProductRelation] WHERE" & _
                            " [ProductRelation].[ProductGuid]=" & SafeString(product("ProductGuid"))
                    End If
                    If relationType <> -1 Then
                        sql &= " AND ProductRelationType=" & relationType & " ORDER BY SortIndex) AS TEMP)"
                    Else
                        sql &= " ORDER BY SortIndex) AS TEMP)"
                    End If
                Next
            End If

            SlideDict = Sql2Dictionaries(sql, "ProductGuid")
            If SlideDict.Count > 0 Then
                Return SlideDict
            Else
                Return Nothing
            End If
        End Function
        'JA2010061701 - SLIDE SHOW - End

        Function InitQuickPad(ByVal User)
            Dim retval, sql, HeaderGuid, key
            Dim Cart As ExpDictionary

            retval = New ExpDictionary
            retval("Lines") = New ExpDictionary

            'If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And CStrEx(getSalesPersonGuid()) <> "" Then
            '    Cart = Sql2Dictionary("SELECT * FROM CartHeader WHERE (SalesPersonGuid =" & SafeString(getSalesPersonGuid()) & ") AND (MultiCartStatus='ACTIVE')")
            'Else
            Cart = Sql2Dictionary("SELECT * FROM CartHeader WHERE (UserGuid=" & SafeString(User("UserGuid")) & ") AND (MultiCartStatus='ACTIVE')")
            'End If

            If Cart Is Nothing Then
                REM -- If no active cart exists Then create it.
                HeaderGuid = GetHeaderGuidEx(User("UserGuid"), False)

                REM -- Generate empty cart dictionary
                retval("HeaderGuid") = HeaderGuid
                retval("CreatedDate") = Now
                retval("ModifiedDate") = Now
                retval("UserGuid") = User("UserGuid")
                retval("MultiCartStatus") = "ACTIVE"
                retval("MultiCartDescription") = GetNewMultiCartDescription()
                globals.IsCartEmpty = True

            Else
                For Each key In Cart.Keys
                    retval(key) = Cart(key)
                Next

                sql = "SELECT * FROM CartLine" & _
                      "  WHERE HeaderGuid=" & SafeString(Cart("HeaderGuid")) & _
                      " ORDER BY LineNumber"
                retval("Lines") = SQL2Dicts(sql, "LineGuid", -1)

                globals.IsCartEmpty = (retval("Lines").Count = 0)
            End If
            Return retval

        End Function

        'Authorize.NET Credentials
        Public Shared Function getAuthorizeNetUrl()
            Dim simulation As Boolean = False
            Dim url As String = ""
            'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - Start
            'Dim sql As String = "SELECT [CCGatewayUserLive],[CCGatewayPasswordLive], [CCGatewayUserTest],[CCGatewayPasswordTest],[Website],[TestingWebsite],[CCSimulationMode] FROM [EESetup] INNER JOIN EECCGateway ON EESetup.CCGatewayCode=EECCGateway.Code"
            Dim pageobj As ExpandIT.Page = CType(System.Web.HttpContext.Current.Handler, ExpandIT.Page)
            Dim ccGatewayCode As String = ""
            Try
                ccGatewayCode = CBoolEx(pageobj.eis.getEnterpriseConfigurationValue("CCGatewayCode"))
            Catch ex As Exception
            End Try
            Dim sql As String = "SELECT [Website],[TestingWebsite] FROM EECCGateway WHERE Code=" & SafeString(ccGatewayCode)
            Dim retval As ExpDictionary = SQL2Dicts(sql)
            If Not retval Is Nothing AndAlso Not retval Is DBNull.Value AndAlso retval.Count > 0 Then
                For Each item As ExpDictionary In retval.Values
                    'simulation = CBoolEx(item("CCSimulationMode"))
                    simulation = CBoolEx(pageobj.eis.getEnterpriseConfigurationValue("CCSimulationMode"))
                    'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - End
                    If simulation Then
                        url = CStrEx(item("TestingWebsite"))
                    Else
                        url = CStrEx(item("Website"))
                    End If
                    Exit For
                Next
                Return url
            Else
                Return ""
            End If
        End Function

        Public Shared Function getAuthorizeNetLogin()
            Dim simulation As Boolean = False
            Dim login As String = ""
            'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - Start
            'Dim sql As String = "SELECT [CCGatewayUserLive],[CCGatewayPasswordLive], [CCGatewayUserTest],[CCGatewayPasswordTest],[Website],[TestingWebsite],[CCSimulationMode] FROM [EESetup] INNER JOIN EECCGateway ON EESetup.CCGatewayCode=EECCGateway.Code"
            Dim pageobj As ExpandIT.Page = CType(System.Web.HttpContext.Current.Handler, ExpandIT.Page)
            Dim ccGatewayCode As String = ""
            Try
                ccGatewayCode = CBoolEx(pageobj.eis.getEnterpriseConfigurationValue("CCGatewayCode"))
            Catch ex As Exception
            End Try
            Dim sql As String = "SELECT * FROM EECCGateway WHERE Code=" & SafeString(ccGatewayCode)
            Dim retval As ExpDictionary = SQL2Dicts(sql)
            If Not retval Is Nothing AndAlso Not retval Is DBNull.Value AndAlso retval.Count > 0 Then
                For Each item As ExpDictionary In retval.Values
                    'simulation = CBoolEx(item("CCSimulationMode"))
                    simulation = CBoolEx(pageobj.eis.getEnterpriseConfigurationValue("CCSimulationMode"))
                    If simulation Then
                        'login = CStrEx(item("CCGatewayUserTest"))
                        login = CStrEx(pageobj.eis.getEnterpriseConfigurationValue("CCGatewayUserTest"))
                    Else
                        'login = CStrEx(item("CCGatewayUserLive"))
                        login = CStrEx(pageobj.eis.getEnterpriseConfigurationValue("CCGatewayUserLive"))
                        'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - End
                    End If
                    Exit For
                Next
                Return login
            Else
                Return ""
            End If
        End Function

        Public Shared Function getAuthorizeNetTransactionKey()
            Dim sql As String = "SELECT KeyValue FROM EECCGatewayAdditionalKeys WHERE (GatewayCode = '1') AND (KeyCode = 'x_tran_key') "
            Dim key As String = CStrEx(getSingleValueDB(sql))
            Return key
        End Function


        '<!--JA2010031101 - CATMAN SLIDE - Start-->

        Public Function getSlideImages()
            Dim item, item2, item3
            Dim sqlPrd As String = ""
            Dim sqlGrp As String = ""
            Dim sqlPropGrp As String
            Dim products As ExpDictionary
            Dim groups As ExpDictionary
            Dim groupsProperties As New ExpDictionary
            Dim groupsPrdFinal As New ExpDictionary
            Dim guid As String = ""
            Dim sortedproducts As New System.Collections.Generic.SortedDictionary(Of String, Object)

            'If AppSettings("SHOW_ENTERPRISE_SLIDE_MANAGER") Then


            'AM2010071901 - ENTERPRISE CSC - Start
            sqlGrp = "SELECT * FROM CatalogSlideShows WHERE CatalogNo=" & SafeString(globals.User("CatalogNo"))
            'sqlGrp = "SELECT * FROM EECMCatalogSlideShow "
            'AM2010071901 - ENTERPRISE CSC - End
            groups = SQL2Dicts(sqlGrp, "SlideNo")
            'Load the information about the groups
            For Each item3 In groups.Values
                Dim groupsPrd As New ExpDictionary
                sqlPropGrp = "SELECT PropOwnerRefGuid, PropGuid, PropLangGuid, PropTransText FROM PropVal INNER JOIN PropTrans" & _
                                       " ON PropVal.PropValGuid=PropTrans.PropValGuid" & _
                                       " WHERE PropOwnerTypeGuid='SLD'" & _
                                       " AND (PropLangGuid IS NULL OR PropLangGuid =" & SafeString(HttpContext.Current.Session("LanguageGuid").ToString) & ")" & _
                                       " AND (PropGuid='SLIDE_IMAGE' OR PropGuid='SLIDE_HTML' OR PropGuid='SLIDE_ALTEXT')" & _
                                       " AND PropOwnerRefGuid=" & SafeString(item3("SlideNo"))
                groupsProperties = SQL2Dicts(sqlPropGrp)
                If Not groupsProperties Is Nothing Then
                    For Each item2 In groupsProperties.Values
                        groupsPrd.Add(item2("PropGuid"), item2("PropTransText"))
                    Next
                    If item3("PropertyGroupCode") = "GROUP" Then
                        groupsPrd.Add("LINK", GroupLink(item3("ID")))
                    Else
                        groupsPrd.Add("LINK", "link.aspx?ProductGuid=" & item3("ID") & "&GetGroup=1")
                    End If
                    groupsPrd.Add("SLIDE_INDEX", GroupLink(item3("SortIndex")))
                    groupsPrd.Add("GroupGuid", item3("ID"))
                    groupsPrd.Add("SlideNo", item3("SlideNo"))
                    groupsPrdFinal.Add(item3("SlideNo"), groupsPrd)
                End If
            Next
            ' Sort the list of favorites
            If Not groupsPrdFinal Is Nothing And Not groupsPrdFinal Is DBNull.Value Then
                For Each groups2 As ExpDictionary In groupsPrdFinal.Values
                    sortedproducts.Add(groups2("SlideNo"), groups2)
                Next
            End If

            'Else
            ''AM2010071901 - ENTERPRISE CSC - Start
            'sqlPrd = "SELECT PropOwnerRefGuid As ProductGuid FROM PropVal WHERE (PropGuid = 'SLIDE_INDEX') AND (PropOwnerTypeGuid = 'PRD') AND (CatalogNo=" & SafeString(globals.User("CatalogNo")) & " )"
            'sqlGrp = "SELECT PropOwnerRefGuid As ProductGuid FROM PropVal WHERE (PropGuid = 'SLIDE_INDEX') AND (PropOwnerTypeGuid = 'GRP') AND (CatalogNo=" & SafeString(globals.User("CatalogNo")) & " )"
            ''sqlPrd = "SELECT PropOwnerRefGuid As ProductGuid FROM PropVal WHERE (PropGuid = 'SLIDE_INDEX') AND (PropOwnerTypeGuid = 'PRD') "
            ''sqlGrp = "SELECT PropOwnerRefGuid As ProductGuid FROM PropVal WHERE (PropGuid = 'SLIDE_INDEX') AND (PropOwnerTypeGuid = 'GRP') "
            ''AM2010071901 - ENTERPRISE CSC - End

            'products = SQL2Dicts(sqlPrd, "ProductGuid")
            '' Load the information about the products
            'CatDefaultLoadProducts(products, False, False, False, New String() {"ProductName"}, New String() {"SLIDE_IMAGE", "SLIDE_HTML", "SLIDE_ALTEXT", "SLIDE_INDEX"}, "")


            'groups = SQL2Dicts(sqlGrp, "ProductGuid")
            '' Load the information about the Groups


            'For Each item3 In groups.Values
            '    Dim groupsPrd As New ExpDictionary
            '    'AM2010071901 - ENTERPRISE CSC - Start
            '    sqlPropGrp = "SELECT PropOwnerRefGuid, PropGuid, PropLangGuid, PropTransText, CatalogNo FROM PropVal INNER JOIN PropTrans" & _
            '                               " ON PropVal.PropValGuid=PropTrans.PropValGuid" & _
            '                               " WHERE PropOwnerTypeGuid='GRP'" & _
            '                               " AND (PropLangGuid IS NULL" & _
            '                               " OR PropLangGuid=" & SafeString(HttpContext.Current.Session("LanguageGuid").ToString) & _
            '                               " OR PropLangGuid=" & SafeString(AppSettings("LANGUAGE_DEFAULT")) & _
            '                               ")" & _
            '                               " AND (PropGuid='SLIDE_IMAGE' OR PropGuid='SLIDE_HTML' OR PropGuid='SLIDE_ALTEXT' OR PropGuid='SLIDE_INDEX' )" & _
            '                               " AND (PropOwnerRefGuid=" & SafeString(item3("ProductGuid")) & " )" & _
            '                               " AND (CatalogNo=" & SafeString(globals.User("CatalogNo")) & " )"
            '    'sqlPropGrp = "SELECT PropOwnerRefGuid, PropGuid, PropLangGuid, PropTransText, CatalogNo FROM PropVal INNER JOIN PropTrans" & _
            '    '                           " ON PropVal.PropValGuid=PropTrans.PropValGuid" & _
            '    '                           " WHERE PropOwnerTypeGuid='GRP'" & _
            '    '                           " AND (PropLangGuid IS NULL" & _
            '    '                           " OR PropLangGuid=" & SafeString(HttpContext.Current.Session("LanguageGuid").ToString) & _
            '    '                           " OR PropLangGuid=" & SafeString(AppSettings("LANGUAGE_DEFAULT")) & _
            '    '                           ")" & _
            '    '                           " AND (PropGuid='SLIDE_IMAGE' OR PropGuid='SLIDE_HTML' OR PropGuid='SLIDE_ALTEXT' OR PropGuid='SLIDE_INDEX' )" & _
            '    '                           " AND (PropOwnerRefGuid=" & SafeString(item3("ProductGuid")) & " )"
            '    'AM2010071901 - ENTERPRISE CSC - End
            '    groupsProperties = SQL2Dicts(sqlPropGrp)
            '    If Not groupsProperties Is Nothing Then
            '        For Each item2 In groupsProperties.Values
            '            groupsPrd.Add(item2("PropGuid"), item2("PropTransText"))
            '        Next
            '        groupsPrd.Add("LINK", GroupLink(item3("ProductGuid")))
            '        groupsPrd.Add("GroupGuid", item3("ProductGuid"))
            '        groupsPrdFinal.Add(item3("ProductGuid"), groupsPrd)
            '    End If
            'Next
            'Dim item4
            'For Each item4 In groupsPrdFinal.Values
            '    products.Add(item4("GroupGuid"), item4)
            'Next
            '' Sort the list of favorites
            'Dim product
            'If Not products Is Nothing And Not products Is DBNull.Value Then
            '    For Each product In products.Values
            '        sortedproducts.Add(product("SLIDE_INDEX"), product)
            '    Next
            'End If
            'End If

            Dim str As String = ""
            Dim cont As Integer = 0
            If Not sortedproducts Is Nothing AndAlso sortedproducts.Count > 0 Then
                str = str & " <script language=""JavaScript1.2"" type=""text/javascript"">"
                str = str & " function renderBanners(){ renderBanners; try{ buildBannerSet( { banners: [ {"
                For Each item In sortedproducts.Values
                    str = str & "id:""banner" & cont & ""","
                    str = str & "imageSrc:""" & item("SLIDE_IMAGE") & ""","
                    If CStrEx(item("LINK")) = "" Then
                        str = str & "landingPageUrl:""link.aspx?ProductGuid=" & item("ProductGuid") & "&GetGroup=1"","
                    Else
                        str = str & "landingPageUrl:""" & item("LINK") & ""","
                    End If
                    str = str & "imageName:""" & item("SLIDE_ALTEXT") & ""","
                    str = str & "thumbnailImageSrc:""images/" & cont + 1 & "Gray.PNG"","
                    str = str & "thumbnailSelectedImageSrc:""images/" & cont + 1 & "Green.PNG"","
                    str = str & "html:""" & item("SLIDE_HTML") & """"
                    str = str & "},{"
                    cont = cont + 1
                Next

                str = str.Substring(0, str.Length - 3)
                str = str & "}]});}catch(e){}}"
                str = str & " </script>"
            End If

            Return str
        End Function
        '<!--JA2010031101 - CATMAN SLIDE - End-->

        'JA2011030701 - PAYMENT TABLE - START       
        Public Function getPaymentTableColumn(ByVal header As String, ByVal column As String)
            Dim sql As String = "SELECT " & CStrEx(column) & " FROM PaymentTable WHERE HeaderGuid=" & SafeString(CStrEx(header))
            Return CStrEx(getSingleValueDB(sql))
        End Function
        'JA2011030701 - PAYMENT TABLE - END

        'JA2010030901 - PERFORMANCE -Start
        Public Sub SetControlProperties(ByRef objMyControl As Control, ByVal strProperty As String, ByVal objValue As Object)
            Dim controlProperty As System.Reflection.PropertyInfo
            Dim controlType As Type = objMyControl.GetType()

            controlProperty = controlType.GetProperty(strProperty)
            If Not controlProperty Is Nothing Then
                controlProperty.SetValue(objMyControl, objValue, Nothing)
            End If
        End Sub
        'JA2010030901 - PERFORMANCE -End

        'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - Start
        Public Function getEnterpriseConfigurationValue(ByVal keyName As String) As Object
            Try
                Return HttpContext.Current.Application("Config")(globals.User("CatalogNo"))(keyName)
            Catch ex As Exception
                Return Nothing
            End Try
        End Function
        'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - End






        'JA0531201001 - STAY IN CATALOG - START
        Public Function getHtmlAddToCartBox(ByVal image As String, ByVal ProductGuid As String, ByVal VariantName As String, ByVal VariantCode As String)
            Dim name As String = ""
            Dim price As String = ""
            Dim qty As String = ""
            Dim str As String = ""
            Dim uom As String = ""
            Dim discount As String = ""


            If globals.OrderDict Is Nothing Then
                globals.OrderDict = LoadOrderDictionary(globals.User)
            End If

            If Not globals.OrderDict("Lines") Is Nothing AndAlso Not globals.OrderDict("Lines") Is DBNull.Value Then
                For Each item As ExpDictionary In globals.OrderDict("Lines").values
                    If CStrEx(VariantCode) <> "" Then
                        If CStrEx(item("ProductGuid")) = ProductGuid And CStrEx(item("VariantCode")) = VariantCode Then
                            name = item("ProductName")
                            If price = "" Then
                                price = CurrencyFormatter.FormatCurrency(item("TotalInclTax"), HttpContext.Current.Session("UserCurrencyGuid").ToString)
                            Else
                                price = price & "/" & CurrencyFormatter.FormatCurrency(item("TotalInclTax"), HttpContext.Current.Session("UserCurrencyGuid").ToString)
                            End If
                            If qty = "" Then
                                qty = CStrEx(item("Quantity"))
                            Else
                                qty = qty & "/" & CStrEx(item("Quantity"))
                            End If
                            If CStrEx(item("UOM")) <> "" Then
                                If uom = "" Then
                                    uom = item("UOM")
                                Else
                                    uom = uom & "/" & item("UOM")
                                End If
                            End If
                            If CDblEx(item("LineDiscountAmount")) <> 0 Then
                                discount = CurrencyFormatter.FormatCurrency(item("ListPrice") * qty, HttpContext.Current.Session("UserCurrencyGuid").ToString) & " - (" & CurrencyFormatter.FormatCurrency(item("LineDiscountAmount"), HttpContext.Current.Session("UserCurrencyGuid").ToString) & ")"
                            End If
                        End If
                    ElseIf CStrEx(item("ProductGuid")) = ProductGuid Then
                        name = item("ProductName")
                        If price = "" Then
                            price = CurrencyFormatter.FormatCurrency(item("TotalInclTax"), HttpContext.Current.Session("UserCurrencyGuid").ToString)
                        Else
                            price = price & "/" & CurrencyFormatter.FormatCurrency(item("TotalInclTax"), HttpContext.Current.Session("UserCurrencyGuid").ToString)
                        End If
                        If qty = "" Then
                            qty = CStrEx(item("Quantity"))
                        Else
                            qty = qty & "/" & CStrEx(item("Quantity"))
                        End If
                        If CStrEx(item("UOM")) <> "" Then
                            If uom = "" Then
                                uom = item("UOM")
                            Else
                                uom = uom & "/" & item("UOM")
                            End If
                        End If
                        If CDblEx(item("LineDiscountAmount")) <> 0 Then
                            discount = CurrencyFormatter.FormatCurrency(item("ListPrice") * qty, HttpContext.Current.Session("UserCurrencyGuid").ToString) & " - (" & CurrencyFormatter.FormatCurrency(item("LineDiscountAmount"), HttpContext.Current.Session("UserCurrencyGuid").ToString) & ")"
                        End If
                    End If
                Next



                str = "<table id='myDiv' width='300' height='250'><tr><td><table width='300'>"

                If image <> "" AndAlso Not image.Contains(VRoot) Then
                    image = VRoot & "/" & image
                End If

                str &= "<tr><td colspan='2' style='text-align:center;'><img style='width:100px;' alt='' src='" & image & "' /> </td></tr>"

                str &= "<tr><td style='vertical-align:middle;text-align:right;width:50%;padding-right:3px;'><b>" & Resources.Language.LABEL_PRODUCT_GUID_LIGTH_BOX & ":</b></td><td style='vertical-align:middle;text-align:left;'>" & ProductGuid & "</td></tr>"

                str &= "<tr><td style='vertical-align:middle;text-align:right;width:50%;padding-right:3px;'><b>" & Resources.Language.LABEL_NAME_LIGTH_BOX & ":</b></td><td style='vertical-align:middle;text-align:left;'>" & name & "</td></tr>"

                If CStrEx(VariantName) <> "" And CStrEx(VariantName) <> "undefined" Then
                    str &= "<tr><td style='vertical-align:middle;text-align:right;width:50%;padding-right:3px;'><b>" & Resources.Language.LABEL_VARIANT_NAME_LIGTH_BOX & ":</b></td><td style='vertical-align:middle;text-align:left;'>" & VariantName & "</td></tr>"
                End If

                If uom <> "" Then
                    str &= "<tr><td style='vertical-align:middle;text-align:right;width:50%;padding-right:3px;'><b>" & Resources.Language.LABEL_UOM & ":</b></td><td style='vertical-align:middle;text-align:left;'>" & uom & "</td></tr>"
                End If

                str &= "<tr><td style='vertical-align:middle;text-align:right;width:50%;padding-right:3px;'><b>" & Resources.Language.LABEL_QUANTITY_LIGTH_BOX & ":</b></td><td style='vertical-align:middle;text-align:left;'>" & qty & "</td></tr>"

                If discount <> "" Then
                    str &= "<tr><td style='vertical-align:middle;text-align:right;width:50%;padding-right:3px;'><b>" & Resources.Language.LABEL_DISCOUNT_LIGTH_BOX & ":</b></td><td style='vertical-align:middle;text-align:left;'>" & discount & "</td></tr>"
                End If

                str &= "<tr><td style='vertical-align:middle;text-align:right;width:50%;padding-right:3px;'><b>" & Resources.Language.LABEL_PRICE_LIGTH_BOX & ":</b></td><td style='vertical-align:middle;text-align:left;'>" & price & "</td></tr>"

                str &= "</table></td></tr>"


                str &= "<tr><td><table width='300'><tr>"

                str &= "<td><input onclick='stayInPage();' id='StayInPageBox' type='button' class='AddButton' value='" & Resources.Language.LABEL_STAY_IN_PAGE_LIGTH_BOX & "' /></td>"

                str &= "<td><input onclick='goToCart();' id='ShoppingCartBox' type='button' class='AddButton' value='" & Resources.Language.LABEL_SHOPPING_CART_LIGTH_BOX & "' /></td>"

                str &= "</tr></table>"

                str &= "</td></tr></table>"

            End If

            Return str

        End Function
        'JA0531201001 - STAY IN CATALOG - END




    End Class

End Namespace