Imports System.Configuration.ConfigurationManager
Imports Microsoft.VisualBasic
Imports System.IO
Imports ExpandIT.GlobalsClass
Imports ExpandIT.B2BBaseClass
Imports ExpandIT.BuslogicBaseClass
Imports ExpandIT.DatabasePathClass
Imports ExpandIT.CurrencyBaseClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT.Debug
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Generic
Imports System.Net.Mail.SmtpClient
Imports System.IO.Compression
Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Xml
Imports ExpandIT.SectionSettingsManager

Namespace ExpandIT
    '*****CUSTOMER SPECIFIC CATALOGS***** - START
    Public Class csc

        Private globals As GlobalsClass
        Dim pageobj As ExpandIT.Page = CType(System.Web.HttpContext.Current.Handler, ExpandIT.Page)
        Dim objSearch As New USSearch

        Public Sub New(Optional ByVal _globals As GlobalsClass = Nothing)
            globals = _globals
        End Sub

        Public Sub IntLoadGroupsCSC(ByVal info As ExpDictionary)
            Dim groupinfo As ExpDictionary
            Dim subgroupinfo As ExpDictionary
            Dim proparray() As String = {}
            Dim grouparray() As String = {}
            Dim sql As String
            Dim subgroups As ExpDictionary = Nothing
            Dim grouplist As String
            Dim newinfo As ExpDictionary
            Dim Group As ExpDictionary
            Dim groups As ExpDictionary
            Dim subgroups2 As ExpDictionary
            Dim productinfo As ExpDictionary
            Dim products As ExpDictionary = Nothing
            Dim i As Integer
            Dim key As String
            Dim groupguid As String
            Dim langguid As String
            Dim defaultlangguid As String
            Dim general As ExpDictionary
            Dim item As ExpDictionary
            Dim properties As ExpDictionary
            Dim subgroup As ExpDictionary
            Dim products2 As ExpDictionary = Nothing
            Dim product As ExpDictionary = Nothing


            subgroupinfo = Nothing
            productinfo = Nothing
            groupinfo = Nothing

            ' Read parameters
            general = info("General")
            langguid = CStrEx(general("LanguageGuid"))
            defaultlangguid = CStrEx(general("DefaultLanguageGuid"))

            If info.Exists("GroupInfo") Then
                groupinfo = info("GroupInfo")
                groups = groupinfo("Groups")
                proparray = groupinfo("PropertyArray")
                If groupinfo.Exists("SubgroupInfo") Then
                    subgroupinfo = groupinfo("SubgroupInfo")
                End If
                If groupinfo.Exists("ProductInfo") Then
                    productinfo = groupinfo("ProductInfo")
                End If
            Else
                groups = New ExpDictionary
            End If

            ' Make a group list
            grouparray = New String() {}
            grouplist = ""
            i = 0
            For Each item In groups.Values
                groupguid = CStrEx(item("GroupGuid"))
                If groupguid <> "" Then
                    grouplist = grouplist & ", " & groupguid
                    ReDim Preserve grouparray(i)
                    grouparray(i) = groupguid
                    i = i + 1
                End If
            Next
            grouplist = Mid(grouplist, 3)

            ' Exit function if no groups were specified
            If grouplist = "" Then
                Exit Sub
            End If

            ' Load Properties
            properties = EISClass.IntLoadProperties("GRP", grouparray, proparray, langguid, defaultlangguid)

            ' Load Subgroups
            If Not subgroupinfo Is Nothing Then
                'JA2010030901 - PERFORMANCE -Start
                sql = groupsWithProductsSQL(grouplist)
                'JA2010030901 - PERFORMANCE -End
                subgroups = Sql2Dictionaries2(sql, New String() {"ParentGuid", "GroupGuid"})
                ' Create a new dictionary containing references to all subgroups
                subgroups2 = New ExpDictionary()
                For Each Group In subgroups.Values
                    For Each subgroup In Group.Values
                        subgroups2(subgroup("GroupGuid") & "," & subgroup("ParentGuid")) = subgroup
                    Next
                Next

                ' Make recursive call to load subgroups
                subgroupinfo.Add("Groups", subgroups2)
                newinfo = New ExpDictionary()
                newinfo.Add("General", general)
                newinfo.Add("GroupInfo", subgroupinfo)
                IntLoadGroupsCSC(newinfo)
            End If

            ' Load Products
            If Not productinfo Is Nothing Then
                ' Find the products for the specified groups
                'sql = "SELECT GroupProduct.GroupGuid, GroupProduct.ProductGuid FROM GroupProduct INNER JOIN ProductTable ON GroupProduct.ProductGuid=ProductTable.ProductGuid WHERE GroupProduct.GroupGuid IN (" & grouplist & ") ORDER BY GroupGuid, SortIndex"

                'Find the products for specific customers
                If globals.Context Is Nothing Then
                    globals.Context = pageobj.eis.LoadContext(globals.User)
                End If
                'JA2010030901 - PERFORMANCE -Start
                Dim productsList As String
                productsList = getListAvailableItems(globals.User)
                'AM2010071901 - ENTERPRISE CSC - Start
                sql = "SELECT GroupProduct.GroupGuid, GroupProduct.ProductGuid FROM GroupProduct INNER JOIN (" & productsList & ") CSCAvailableItems" & _
                    " ON GroupProduct.ProductGuid=CSCAvailableItems.ProductGuid WHERE GroupProduct.GroupGuid IN (" & grouplist & ") AND " & _
                    "GroupProduct.CatalogNo=" & SafeString(globals.User("CatalogNo")) & " ORDER BY GroupGuid, SortIndex"
                'AM2010071901 - ENTERPRISE CSC - End
                'JA2010030901 - PERFORMANCE -End

                products = Sql2Dictionaries2(sql, New String() {"GroupGuid", "ProductGuid"})

                ' Create a new flat dictionary containing references to all products
                products2 = New ExpDictionary()
                For Each Group In products.Values
                    For Each product In Group.Values
                        products2(product("GroupGuid") & "," & product("ProductGuid")) = product
                    Next
                Next

                ' Make call to load products
                productinfo.Add("Products", products2)
                newinfo = New ExpDictionary()
                newinfo.Add("General", general)
                newinfo.Add("ProductInfo", productinfo)
                pageobj.eis.IntLoadProductsInfoCSC(newinfo)
            End If

            ' Assing info to groups
            For Each Group In groups.Values
                groupguid = CStrEx(Group("GroupGuid"))
                If groupguid <> "" Then
                    ' Set properties
                    CopyDictValues(Group, properties(groupguid))

                    ' Set subgroups
                    If Not subgroupinfo Is Nothing Then
                        key = SafeString(groupguid)
                        If subgroups.Exists(key) Then
                            Group("Subgroups") = subgroups(key)
                        End If
                    End If

                    ' Set products
                    If Not productinfo Is Nothing Then
                        key = SafeString(groupguid)
                        If products.Exists(key) Then
                            Group("Products") = products(key)
                        End If
                    End If
                End If
            Next

            ' Remove the products from productinfo
            If Not productinfo Is Nothing Then
                productinfo.Remove("Products")
            End If
        End Sub

        'JA2010030901 - PERFORMANCE -Start
        Public Function loadSpecials()
            Dim productsList As String
            Dim Sql As String
            productsList = getListAvailableItems(globals.User)
            'AM2010071901 - ENTERPRISE CSC - Start
            Sql = "SELECT A.ProductGuid FROM Attain_ProductPrice A INNER JOIN (SELECT DISTINCT " & _
                "GroupProduct.ProductGuid FROM GroupProduct WHERE GroupProduct.CatalogNo=" & SafeString(globals.User("CatalogNo")) & _
                ") AS GP ON A.ProductGuid=GP.ProductGuid INNER JOIN (" & productsList & ") CSCAvailableItems ON GP.ProductGuid=CSCAvailableItems.ProductGuid WHERE "
            'AM2010071901 - ENTERPRISE CSC - End
            Return Sql
        End Function

        Public Function loadWhatsNew()
            Dim productsList As String
            Dim Sql As String
            productsList = getListAvailableItems(globals.User)
            'AM2010071901 - ENTERPRISE CSC - Start
            Sql = "SELECT ProductTable.ProductGuid FROM ProductTable INNER JOIN (SELECT DISTINCT " & _
                "GroupProduct.ProductGuid FROM GroupProduct WHERE GroupProduct.CatalogNo=" & SafeString(globals.User("CatalogNo")) & _
                ") AS GP ON ProductTable.ProductGuid=GP.ProductGuid INNER JOIN (" & productsList & ") CSCAvailableItems ON GP.ProductGuid=CSCAvailableItems.ProductGuid WHERE "
            'AM2010071901 - ENTERPRISE CSC - End
            Return Sql
        End Function
        'JA2010030901 - PERFORMANCE -End

        Public Sub AddItemToOrder2CSC(ByVal OrderObject As ExpDictionary, ByVal ProductGuid As String, ByVal Quantity As Decimal, ByVal Comment As String, ByVal VariantCode As String, ByVal UOM As String) 'AM0122201001 - UOM - Start
            'AM0122201001 - UOM - End
            Dim dtProduct As DataTable
            Dim NewQuantity As Decimal
            Dim InCart As Boolean
            Dim OrderLine As ExpDictionary
            Dim NextLineNumber As Integer
            Dim sql As String

            If Quantity > 0 Then
                If CBool(AppSettings("CART_ALLOW_DECIMAL")) Then
                    NewQuantity = CDblEx(SafeUFloat(CDblEx(Quantity)))
                Else
                    NewQuantity = CLngEx(SafeULong(CDblEx(Quantity)))
                End If

                ' Check If the product is already in the cart - If compress
                InCart = False
                If CBool(AppSettings("COMPRESS_CART")) Then
                    For Each OrderLine In OrderObject("Lines").values
                        'AM0122201001 - UOM - Start
                        If Trim(OrderLine("ProductGuid")).ToUpper = Trim(ProductGuid).ToUpper And Trim(CStrEx(OrderLine("VariantCode"))) = Trim(CStrEx(VariantCode)) And Not InCart And Trim(CStrEx(OrderLine("UOM"))) = Trim(CStrEx(UOM)) Then
                            'AM0122201001 - UOM - End
                            OrderLine("Quantity") = NewQuantity + CDblEx(OrderLine("Quantity"))
                            OrderLine("IsCalculated") = False
                            OrderLine("VersionGuid") = GenGuid()

                            ' Only set the comment if there was not one before
                            If CStrEx(OrderLine("LineComment")) = "" Then OrderLine("LineComment") = Comment
                            InCart = True
                            Exit For
                        End If
                    Next
                End If

                ' If Not in the cart already Then add it! or not to Compress
                If Not InCart Then
                    OrderLine = New ExpDictionary
                    NextLineNumber = EISClass.GetMaxLineNumber(OrderObject("Lines")) + 10000
                    If AppSettings("REQUIRE_CATALOG_ENTRY") Then
                        'JA2010030901 - PERFORMANCE -Start
                        'AM2010071901 - ENTERPRISE CSC - Start
                        sql = "SELECT ProductTable.* FROM ProductTable INNER JOIN GroupProduct ON ProductTable.ProductGuid=GroupProduct.ProductGuid " & _
                            " INNER JOIN (" & CStrEx(getListAvailableItems(globals.User)) & ") CSCAvailableItems ON GroupProduct.ProductGuid=CSCAvailableItems.ProductGuid " & _
                            " WHERE ProductTable.ProductGuid=" & SafeString(ProductGuid) & " AND GroupProduct.CatalogNo=" & SafeString(globals.User("CatalogNo"))
                        'AM2010071901 - ENTERPRISE CSC - End
                    Else
                        sql = "SELECT ProductTable.* FROM ProductTable " & _
                            " INNER JOIN (" & CStrEx(getListAvailableItems(globals.User)) & ") CSCAvailableItems ON ProductTable.ProductGuid=CSCAvailableItems.ProductGuid " & _
                            " WHERE ProductGuid=" & SafeString(ProductGuid)
                    End If
                    'JA2010030901 - PERFORMANCE -End
                    dtProduct = SQL2DataTable(sql)
                    If dtProduct.Rows.Count > 0 Then
                        Dim row As DataRow = dtProduct.Rows(0)
                        OrderLine("LineGuid") = GenGuid()
                        OrderLine("HeaderGuid") = OrderObject("HeaderGuid")
                        OrderLine("VariantCode") = CStrEx(VariantCode)
                        OrderLine("ProductGuid") = row("ProductGuid")
                        OrderLine("ProductName") = row("ProductName")
                        OrderLine("ListPrice") = row("ListPrice")
                        OrderLine("LineNumber") = NextLineNumber
                        OrderLine("Quantity") = NewQuantity
                        OrderLine("LineComment") = Comment
                        'AM0122201001 - UOM - Start
                        If AppSettings("EXPANDIT_US_USE_UOM") Then
                            OrderLine("UOM") = CStrEx(UOM)
                        End If
                        'AM0122201001 - UOM - End

                        'AM2010061801 - KITTING - START
                        If CBoolEx(AppSettings("EXPANDIT_US_USE_KITTING")) Then
                            OrderLine("KitBOMNo") = CStrEx(row("KitBOMNo"))
                        End If
                        'AM2010061801 - KITTING - END

                        '<!-- ' ExpandIT US - MPF - US Sales Tax - Start -->
                        ' 20080529 - ExpandIT US - MPF - US Sales Tax Modification
                        If (AppSettings("EXPANDIT_US_USE_US_SALES_TAX")) Then
                            OrderLine("TaxGroupCode") = row("TaxGroupCode")
                        End If
                        '<!-- ' ExpandIT US - MPF - US Sales Tax - Finish -->

                        '*****shipping*****-start
                        'AM2010042601 - ENTERPRISE SHIPPING - Start
                        If CBoolEx(AppSettings("EXPANDIT_US_USE_SHIPPING")) Or Not CBoolEx(AppSettings("EXPANDIT_US_USE_SHIPPING")) And CBoolEx(AppSettings("ENTERPRISE_SHIPPING_USE_WEIGHT")) Then
                            'AM2010042601 - ENTERPRISE SHIPPING - End
                            OrderLine("ShippingWeight") = row("ShippingWeight")
                        End If
                        '*****shipping*****-end
                        ' For performance reasons.
                        OrderLine("IsCalculated") = False
                        OrderLine("VersionGuid") = GenGuid()
                        If OrderObject("Lines") Is Nothing Then
                            OrderObject.Add("Lines", New ExpDictionary)
                        End If
                        OrderObject("Lines").Add(OrderLine("LineGuid"), OrderLine)
                    Else
                        globals.messages.Warnings.Add(Replace(Resources.Language.MESSAGE_PRODUCT_DOES_NOT_EXISTS, "%1", ProductGuid))
                    End If
                End If
            End If
        End Sub

        'JA2010092201 - RECURRING ORDERS - START
        Public Sub AddItemToOrder2RecurringCSC(ByVal OrderObject As ExpDictionary, ByVal ProductGuid As String, ByVal Quantity As Decimal, ByVal Comment As String, ByVal VariantCode As String, ByVal RNumber As Integer, ByVal RPeriod As String, ByVal RQuantity As String, ByVal RFinalDate As String, ByVal UOM As String) 'AM0122201001 - UOM - Start

            'AM0122201001 - UOM - End
            Dim dtProduct As DataTable
            Dim NewQuantity As Decimal
            Dim InCart As Boolean
            Dim OrderLine As ExpDictionary
            Dim NextLineNumber As Integer
            Dim sql As String

            If Quantity > 0 Then
                If CBool(AppSettings("CART_ALLOW_DECIMAL")) Then
                    NewQuantity = CDblEx(SafeUFloat(CDblEx(Quantity)))
                Else
                    NewQuantity = CLngEx(SafeULong(CDblEx(Quantity)))
                End If

                ' Check If the product is already in the cart - If compress
                InCart = False
                If CBool(AppSettings("COMPRESS_CART")) Then
                    For Each OrderLine In OrderObject("Lines").values
                        'AM0122201001 - UOM - Start
                        If Trim(OrderLine("ProductGuid")) = Trim(ProductGuid) And Trim(CStrEx(OrderLine("VariantCode"))) = Trim(CStrEx(VariantCode)) And Not InCart And Trim(CStrEx(OrderLine("UOM"))) = Trim(CStrEx(UOM)) Then
                            'AM0122201001 - UOM - End
                            OrderLine("Quantity") = NewQuantity + CDblEx(OrderLine("Quantity"))
                            OrderLine("IsCalculated") = False
                            OrderLine("VersionGuid") = GenGuid()

                            ' Only set the comment if there was not one before
                            If CStrEx(OrderLine("LineComment")) = "" Then OrderLine("LineComment") = Comment
                            InCart = True
                            Exit For
                        End If
                    Next
                End If

                ' If Not in the cart already Then add it! or not to Compress
                If Not InCart Then
                    OrderLine = New ExpDictionary
                    NextLineNumber = EISClass.GetMaxLineNumber(OrderObject("Lines")) + 10000
                    If AppSettings("REQUIRE_CATALOG_ENTRY") Then
                        'JA2010030901 - PERFORMANCE -Start
                        'AM2010071901 - ENTERPRISE CSC - Start
                        sql = "SELECT ProductTable.* FROM ProductTable INNER JOIN GroupProduct ON ProductTable.ProductGuid=GroupProduct.ProductGuid " & _
                            " INNER JOIN (" & CStrEx(getListAvailableItems(globals.User)) & ") CSCAvailableItems ON GroupProduct.ProductGuid=CSCAvailableItems.ProductGuid " & _
                            " WHERE ProductTable.ProductGuid=" & SafeString(ProductGuid) & " AND GroupProduct.CatalogNo=" & SafeString(globals.User("CatalogNo"))
                    'AM2010071901 - ENTERPRISE CSC - End
                Else
                        sql = "SELECT ProductTable.* FROM ProductTable " & _
                            " INNER JOIN (" & CStrEx(getListAvailableItems(globals.User)) & ") CSCAvailableItems ON ProductTable.ProductGuid=CSCAvailableItems.ProductGuid " & _
                            " WHERE ProductGuid=" & SafeString(ProductGuid)
                End If
                'JA2010030901 - PERFORMANCE -End
                dtProduct = SQL2DataTable(sql)
                If dtProduct.Rows.Count > 0 Then
                    Dim row As DataRow = dtProduct.Rows(0)
                    OrderLine("LineGuid") = GenGuid()
                    OrderLine("HeaderGuid") = OrderObject("HeaderGuid")
                    OrderLine("VariantCode") = CStrEx(VariantCode)
                    OrderLine("ProductGuid") = row("ProductGuid")
                    OrderLine("ProductName") = row("ProductName")
                    OrderLine("ListPrice") = row("ListPrice")
                    OrderLine("LineNumber") = NextLineNumber
                    OrderLine("Quantity") = NewQuantity
                    OrderLine("LineComment") = Comment
                    OrderLine("RNumber") = RNumber
                    OrderLine("RPeriod") = RPeriod
                    OrderLine("RQuantity") = RQuantity
                    OrderLine("RFinalDate") = RFinalDate
                    'AM0122201001 - UOM - Start
                    If AppSettings("EXPANDIT_US_USE_UOM") Then
                        OrderLine("UOM") = CStrEx(UOM)
                    End If
                    'AM0122201001 - UOM - End

                    'AM2010061801 - KITTING - START
                    If CBoolEx(AppSettings("EXPANDIT_US_USE_KITTING")) Then
                        OrderLine("KitBOMNo") = CStrEx(row("KitBOMNo"))
                    End If
                    'AM2010061801 - KITTING - END

                    'OrderLine("MembershipItem") = row("MembershipItem")
                    'OrderLine("MembershipMonths") = row("MembershipMonths")


                    '<!-- ' ExpandIT US - MPF - US Sales Tax - Start -->
                    ' 20080529 - ExpandIT US - MPF - US Sales Tax Modification
                    If (AppSettings("EXPANDIT_US_USE_US_SALES_TAX")) Then
                        OrderLine("TaxGroupCode") = row("TaxGroupCode")
                    End If
                    '<!-- ' ExpandIT US - MPF - US Sales Tax - Finish -->

                    '*****shipping*****-start
                    'AM2010042601 - ENTERPRISE SHIPPING - Start
                    If CBoolEx(AppSettings("EXPANDIT_US_USE_SHIPPING")) Or Not CBoolEx(AppSettings("EXPANDIT_US_USE_SHIPPING")) And CBoolEx(AppSettings("ENTERPRISE_SHIPPING_USE_WEIGHT")) Then
                        'AM2010042601 - ENTERPRISE SHIPPING - End
                        OrderLine("ShippingWeight") = row("ShippingWeight")
                    End If
                    '*****shipping*****-end
                    ' For performance reasons.
                    OrderLine("IsCalculated") = False
                    OrderLine("VersionGuid") = GenGuid()
                    If OrderObject("Lines") Is Nothing Then
                        OrderObject.Add("Lines", New ExpDictionary)
                    End If
                    OrderObject("Lines").Add(OrderLine("LineGuid"), OrderLine)
                Else
                    globals.messages.Warnings.Add(Replace(Resources.Language.MESSAGE_PRODUCT_DOES_NOT_EXISTS, "%1", ProductGuid))
                End If
            End If
            End If

        End Sub
        'JA2010092201 - RECURRING ORDERS - END

        Public Function GetMostPopularItemsCSC(ByVal MaxEntries As Integer) As ExpDictionary
            Dim sqlString As String
            Dim products As ExpDictionary

            'JA2010030901 - PERFORMANCE -Start
            Dim productsList As String
            productsList = getListAvailableItems(globals.User)
            'AM2010071901 - ENTERPRISE CSC - Start
            sqlString = "SELECT TOP " & MaxEntries & " ProductTable.ProductGuid As ProductGuid, COUNT(ShopSalesLine.ProductGuid) AS CountProduct " & _
                        "FROM ShopSalesLine INNER JOIN ProductTable ON ShopSalesLine.ProductGuid=ProductTable.ProductGuid INNER JOIN GroupProduct " & _
                        "ON ProductTable.ProductGuid=GroupProduct.ProductGuid " & _
                        " INNER JOIN (" & productsList & ") CSCAvailableItems ON ProductTable.ProductGuid=CSCAvailableItems.ProductGuid " & _
                        "WHERE GroupProduct.CatalogNo=" & SafeString(globals.User("CatalogNo")) & _
                        " GROUP BY ShopSalesLine.ProductGuid, ProductTable.ProductGuid, ProductTable.ProductName " & _
                        "ORDER BY COUNT(ShopSalesLine.ProductGuid) DESC"
            'AM2010071901 - ENTERPRISE CSC - End
            'JA2010030901 - PERFORMANCE -End
            products = Sql2Dictionaries(sqlString, New String() {"ProductGuid"})
            'JA2010030901 - PERFORMANCE -Start
            If Not products Is Nothing AndAlso Not products Is DBNull.Value AndAlso products.Count > 0 Then
                pageobj.eis.CatDefaultLoadProducts(products, False, False, False, New String() {"ProductName"}, Nothing, Nothing)
            End If
            'JA2010030901 - PERFORMANCE -End
            GetMostPopularItemsCSC = products
        End Function

        Public Function GetMyMostPopularItemsCSC(ByVal MaxEntries As Integer) As ExpDictionary
            Dim sql As String
            Dim products As ExpDictionary

            'JA2010030901 - PERFORMANCE -Start
            Dim productsList As String
            productsList = getListAvailableItems(globals.User)
            'AM2010071901 - ENTERPRISE CSC - Start
            sql = "SELECT TOP " & MaxEntries & " ProductTable.ProductGuid, ProductTable.ProductName , COUNT(ShopSalesLine.ProductGuid) AS CountProduct " & _
                "FROM ShopSalesLine INNER JOIN ProductTable ON ShopSalesLine.ProductGuid=ProductTable.ProductGuid INNER JOIN GroupProduct ON " & _
                "ProductTable.ProductGuid=GroupProduct.ProductGuid INNER JOIN ShopSalesHeader ON ShopSalesHeader.HeaderGuid = ShopSalesLine.HeaderGuid " & _
                " INNER JOIN (" & productsList & ") CSCAvailableItems ON ProductTable.ProductGuid=CSCAvailableItems.ProductGuid " & _
                "WHERE ShopSalesHeader.UserGuid = " & SafeString(HttpContext.Current.Session("UserGuid")) & _
                " AND GroupProduct.CatalogNo=" & SafeString(globals.User("CatalogNo")) & _
                " GROUP BY ShopSalesLine.ProductGuid, ProductTable.ProductGuid, ProductTable.ProductName " & _
                "ORDER BY COUNT(ShopSalesLine.ProductGuid) DESC, ProductTable.ProductGuid"
            'AM2010071901 - ENTERPRISE CSC - End
            'JA2010030901 - PERFORMANCE -End
            products = Sql2Dictionaries(sql, New String() {"ProductGuid"})
            'JA2010030901 - PERFORMANCE -Start
            If Not products Is Nothing AndAlso Not products Is DBNull.Value AndAlso products.Count > 0 Then
                pageobj.eis.CatDefaultLoadProducts(products, False, False, False, New String() {"ProductName"}, Nothing, Nothing)
            End If
            'JA2010030901 - PERFORMANCE -End
            GetMyMostPopularItemsCSC = products
        End Function

        Public Function historyAvailableItemsSQL(ByVal productGuid As String)
            Dim sql As String
            'JA2010030901 - PERFORMANCE -Start
            Dim productsList As String
            productsList = getListAvailableItems(globals.User)
            'AM2010071901 - ENTERPRISE CSC - Start
            sql = "SELECT DISTINCT ProductTable.ProductGuid FROM ProductTable INNER JOIN GroupProduct ON ProductTable.ProductGuid=GroupProduct.ProductGuid " & _
                "INNER JOIN (" & productsList & ") CSCAvailableItems ON GroupProduct.ProductGuid=CSCAvailableItems.ProductGuid " & _
                "WHERE ProductTable.ProductGuid=" & SafeString(productGuid) & " AND CatalogNo=" & SafeString(globals.User("CatalogNo"))
            'AM2010071901 - ENTERPRISE CSC - End
            'JA2010030901 - PERFORMANCE -End
            Return sql
        End Function

        Public Function featuredItemsForSlideSQL()
            Dim sql As String
            'JA2010030901 - PERFORMANCE -Start
            Dim productsList As String
            productsList = getListAvailableItems(globals.User)
            'AM2010071901 - ENTERPRISE CSC - Start
            sql = "SELECT P.ProductGuid FROM ProductTable P INNER JOIN (SELECT DISTINCT GroupProduct.ProductGuid FROM GroupProduct " & _
                "INNER JOIN (" & productsList & ") CSCAvailableItems ON GroupProduct.ProductGuid=CSCAvailableItems.ProductGuid " & _
                "WHERE CatalogNo=" & SafeString(globals.User("CatalogNo")) & ") AS GP ON P.ProductGuid=GP.ProductGuid "
            'AM2010071901 - ENTERPRISE CSC - End
            'JA2010030901 - PERFORMANCE -End
            Return sql
        End Function



        '<!--JM2010061001 - LINK TO - Start-->
        Public Function getLinkToCSC(ByVal PropTransText As String)
            Dim sqlPropGrp As String = ""
            Dim groupsPrd As New ExpDictionary

            'JA2010030901 - PERFORMANCE -Start
            Dim productsList As String
            productsList = getListAvailableItems(globals.User)
            'AM2010071901 - ENTERPRISE CSC - Start
            sqlPropGrp = "EXEC EISECSCGETLINKTO @strCatalog=" & SafeString(globals.User("CatalogNo")) & ", @strIEAvailableItemsSQL=" & SafeString(productsList) & ", @strPropTransText=" & SafeString(PropTransText) & ", @strLanguageGuid=" & SafeString(HttpContext.Current.Session("LanguageGuid").ToString)
            'AM2010071901 - ENTERPRISE CSC - End
            'JA2010030901 - PERFORMANCE -End

            groupsPrd = SQL2Dicts(sqlPropGrp)

            Return groupsPrd

        End Function

        Public Function getListAvailableItems(ByVal user As ExpDictionary) As String
            Dim itemsList As String = ""
            Dim custGroups As String = ""
            'JA2010030901 - PERFORMANCE -Start
            'Dim dictProducts As ExpDictionary
            Dim cachekey As String
            Dim cacheString As String

            If globals.eis.CheckPageAccess("Catalog") Then

                cachekey = "CSCAvailableItems-" & CStrEx(user("CustomerGuid")) & "-" & HttpContext.Current.Session("LanguageGuid").ToString
                If cachekey <> "" Then cacheString = EISClass.CacheGet(cachekey) Else cacheString = Nothing
                If CStrEx(cacheString) <> "" Then
                    Return cacheString
                Else
                    'JA2010030901 - PERFORMANCE -End
                    If globals.Context Is Nothing Then
                        globals.Context = pageobj.eis.LoadContext(globals.User)
                    End If
                    cacheString = " SELECT DISTINCT Attain_ProductPrice.ProductGuid FROM Attain_ProductPrice " & _
                        "inner join producttable on Attain_ProductPrice.ProductGuid=ProductTable.ProductGuid " & _
                        "WHERE ((CustomerRelType = 0 AND CustomerRelGuid = " & SafeString(globals.Context("CustomerGuid")) & ") OR " & _
                        "(CustomerRelType = 1 AND CustomerRelGuid = (SELECT  ISNULL(PriceGroupCode, '') AS PriceGroupCode FROM  " & _
                        "CustomerTable WHERE CustomerGuid=" & SafeString(globals.Context("CustomerGuid")) & ")) OR (CustomerRelType = 2))" & _
                        " AND (StartingDate <= " & SafeDatetime(DateValue(Now)) & " OR StartingDate IS NULL) AND " & _
                        "(EndingDate >= " & SafeDatetime(DateValue(Now)) & " OR EndingDate IS NULL) AND " & _
                        "(CurrencyGuid IS NULL OR CurrencyGuid = '' OR CurrencyGuid = " & SafeString(globals.Context("DefaultCurrency")) & ") "

                    'JA2010030901 - PERFORMANCE -Start
                    ' Set cache
                    If cachekey <> "" Then
                        Dim tableNames As String() = {"Attain_ProductPrice", "ProductTable"}
                        EISClass.CacheSetAggregated(cachekey, cacheString, tableNames)
                    End If
                End If
            Else
                cacheString = "SELECT DISTINCT ProductGuid FROM GroupProduct WHERE CatalogNo = " & SafeString(globals.User("CatalogNo"))
            End If

            Return cacheString
            'JA2010030901 - PERFORMANCE -End

        End Function

        Public Function groupsWithProductsSQL(ByVal parentGuids As String) As String
            Dim strCSCAvailableItemsSQL As String
            
            strCSCAvailableItemsSQL = getListAvailableItems(globals.User)
            Return "EXEC EISECSCGroupsWithProducts @strCatalog=" & SafeString(globals.User("CatalogNo")) & ", @strCSCAvailableItemsSQL=" & SafeString(strCSCAvailableItemsSQL) & ", @strParentGuids=" & parentGuids

        End Function

        Protected Overrides Sub Finalize()
            MyBase.Finalize()
        End Sub
    End Class
    '*****CUSTOMER SPECIFIC CATALOGS***** - END

End Namespace