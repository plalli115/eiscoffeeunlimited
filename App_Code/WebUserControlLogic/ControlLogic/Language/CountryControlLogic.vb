﻿Option Strict On
Imports ExpandIT
Imports ExpandIT.ExpandITLib
Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Configuration.ConfigurationManager
Imports System.ComponentModel

Namespace ExpandIT

    Public Class CountryControlLogic

        Private CountryDataTable As DataTable

        ''' <summary>
        ''' Loads all the availale countries in the shop.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks>Uses caching.</remarks>
        Public Function LoadCountries() As DataTable
            Dim sql As String
            CountryDataTable = Nothing

            sql = "SELECT CountryGuid, CountryName FROM CountryTable ORDER BY CountryName"
            CountryDataTable = SQL2DataTable(sql)

            Return CountryDataTable
        End Function

        Public Function SelectedValue(ByVal userDict As ExpDictionary) As String
            If hasCountries() Then
                If isUserCountryGuidInDataTable(userDict) Then
                    Return UCase(CStrEx(userDict("CountryGuid")))
                Else
                    Return UCase(CStrEx(AppSettings("DEFAULT_COUNTRYGUID")))
                End If
            End If
            Return Nothing
        End Function

        Protected Function hasCountries() As Boolean
            Try
                Return CountryDataTable.Rows.Count > 0
            Catch ex As Exception

            End Try
        End Function

        Protected Function isUserCountryGuidInDataTable(ByVal userDict As ExpDictionary) As Boolean
            For Each row As DataRow In CountryDataTable.Rows
                If row(0).Equals(userDict("CountryGuid")) Then
                    Return True
                End If
            Next
        End Function

    End Class

End Namespace