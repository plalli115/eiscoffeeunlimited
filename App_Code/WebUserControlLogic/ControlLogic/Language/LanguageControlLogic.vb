﻿Option Strict On
Imports ExpandIT
Imports ExpandIT.ExpandITLib
Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Configuration.ConfigurationManager
Imports System.ComponentModel

Namespace ExpandIT

    <DataObjectAttribute()> _
    Public Class LanguageControlLogic

        ''' <summary>
        ''' Loads all the availible languages in the shop.
        ''' </summary>
        ''' <returns>DataTable with all languages</returns>
        ''' <remarks></remarks>
        Public Function LoadLanguages(ByVal load As Boolean) As DataTable

            Dim dt As DataTable

            dt = CType(EISClass.CacheGet("DataTableSiteLanguages"), DataTable)
            If dt Is Nothing Then
                dt = ExpandITLib.SQL2DataTable(loadLanguagesSQLString())
                EISClass.CacheSet("DataTableSiteLanguages", dt, "LanguageTable")
            End If

            Return dt

        End Function

        ''' <summary>
        ''' Returns a SQL-Statement used to load all availible languages in the shop
        ''' </summary>
        ''' <returns>A string as SQL-Statement</returns>
        ''' <remarks></remarks>
        Function loadLanguagesSQLString() As String
            Dim sb As New StringBuilder()
            sb.AppendLine(" SELECT LanguageGuid, LanguageNameNative as LanguageName FROM LanguageTable WHERE LanguageEnabled <> 0 ")
            sb.AppendLine(" AND LanguageNameNative is not null ")
            sb.AppendLine("UNION ")
            sb.AppendLine("SELECT LanguageGuid, LanguageName FROM LanguageTable WHERE LanguageEnabled <> 0 ")
            sb.AppendLine("AND LanguageNameNative is null ")
            sb.AppendLine(" ORDER BY LanguageName ")
            Return sb.ToString()
        End Function

    End Class

End Namespace