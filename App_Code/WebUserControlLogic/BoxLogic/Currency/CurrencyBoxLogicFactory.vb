﻿Imports Microsoft.VisualBasic
Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass

Namespace ExpandIT

    Public Class CurrencyBoxLogicFactory

        Public Shared Function CreateInstance() As CurrencyBoxLogic
            Dim backEndType As String = AppSettings("BackendType")
            Select Case backEndType
                Case "AX"
                    Return New AxCurrencyBoxLogic()
                Case "C5"
                    Return New C5CurrencyBoxLogic()
                Case "NAV"
                    Return New NavCurrencyBoxLogic()
                Case "XAL"
                    Return New XALCurrencyBoxLogic()
                Case Else
                    Return New NoneCurrencyBoxLogic()
            End Select
        End Function

    End Class

End Namespace