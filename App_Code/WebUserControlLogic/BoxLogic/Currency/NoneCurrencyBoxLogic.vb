﻿Imports ExpandIT.ExpandITLib
Imports Microsoft.VisualBasic
Imports System.Configuration.ConfigurationManager

Namespace ExpandIT
    Public Class NoneCurrencyBoxLogic
        Inherits CurrencyBoxLogic

        Protected Overrides Function b2bCurrency() As Boolean
            Return False
        End Function
    End Class
End Namespace