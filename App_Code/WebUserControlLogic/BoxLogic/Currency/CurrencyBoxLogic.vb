﻿Imports ExpandIT
Imports ExpandIT.ExpandITLib
Imports Microsoft.VisualBasic
Imports System.Configuration.ConfigurationManager

Namespace ExpandIT

Public MustInherit Class CurrencyBoxLogic

        Private Const sql As String = "SELECT COUNT(CurrencyCode) FROM CurrencyTable " & _
        " WHERE CurrencyEnabled <> 0 "

        Public Function isVisible(ByVal globalUser As ExpDictionary) As Boolean

            If CBoolEx(globalUser("B2B")) Then Return False
            If Not CBoolEx(AppSettings("MultiCurrencyEnabled")) Then Return False
            If b2bCurrency() Then Return False
            'If only one or no currency enabled then return false
            If CurrencyControlLogicFactory.CreateInstance(globalUser).CurrencyTable().Rows.Count < 2 Then Return False
            Return True

        End Function

        Protected MustOverride Function b2bCurrency() As Boolean

    End Class

End Namespace