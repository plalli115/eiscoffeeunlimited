﻿Imports ExpandIT.ExpandITLib
Imports Microsoft.VisualBasic
Imports System.Configuration.ConfigurationManager

Namespace ExpandIT

    Public Class NavCurrencyBoxLogic
        Inherits CurrencyBoxLogic

        Protected Overrides Function b2bCurrency() As Boolean
            Return CBoolEx(AppSettings("ATTAIN_USE_CURRENCY_FROM_BILLTO_CUSTOMER"))
        End Function
    End Class

End Namespace