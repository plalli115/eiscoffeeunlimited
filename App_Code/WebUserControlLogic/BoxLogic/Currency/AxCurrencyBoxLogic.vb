﻿Imports ExpandIT.ExpandITLib
Imports Microsoft.VisualBasic
Imports System.Configuration.ConfigurationManager

Namespace ExpandIT

    Public Class AxCurrencyBoxLogic
        Inherits CurrencyBoxLogic

        Protected Overrides Function b2bCurrency() As Boolean
            Return CBoolEx(AppSettings("AXAPTA_USE_CURRENCY_FROM_CUSTOMER"))
        End Function
    End Class

End Namespace