﻿Imports System.Configuration.ConfigurationManager
Imports ExpandIT.ExpandITLib

Namespace ExpandIT

    Public Class LanguageBoxLogic

        Public Function isVisible(ByVal user As ExpandIT.ExpDictionary) As Boolean
            If Not CBoolEx(AppSettings("MultiLanguageEnabled")) Then
                Return False
            End If
            Return True
        End Function

    End Class

End Namespace