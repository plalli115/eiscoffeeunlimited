Imports Microsoft.VisualBasic

Public Class ClientInfo
    'AM2010040601 - GEOIP - Start
    Public Function getIPAddress(ByVal objRequest As HttpRequest) As String

        Dim strIP As String
        'Dim ipRange As String

        strIP = objRequest.ServerVariables("HTTP_X_FORWARDED_FOR")
        If (strIP <> "") And Not IsNothing(strIP) Then
            Dim ipRange As String() = strIP.Split(",")
            strIP = ipRange(0)
        Else
            strIP = objRequest.ServerVariables("REMOTE_ADDR")
        End If

        getIPAddress = strIP

    End Function
    'AM2010040601 - GEOIP - End
End Class
