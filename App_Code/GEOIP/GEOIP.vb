Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Net
Imports System.Configuration.ConfigurationManager
Imports ExpandIT.EISClass
Imports ExpandIT
Imports ExpandIT.ExpandITLib

Public Class GEOIP
    Inherits RequestResponse

    Public Function ValidateCountry()

        Dim strRequest As String
        Dim strIPAddress As String
        Dim clientInfoObj As New ClientInfo()

        strIPAddress = clientInfoObj.getIPAddress(HttpContext.Current.Request)
        strRequest = CStrEx(AppSettings("GEOIP_URL"))
        If strRequest <> "" Then
            If CStrEx(AppSettings("GEOIP_LICENSE_PARAMETER")) <> "" Then
                strRequest &= "?" & CStrEx(AppSettings("GEOIP_LICENSE_PARAMETER"))
                If CStrEx(AppSettings("GEOIP_IP_PARAMETER")) <> "" Then
                    strRequest &= "&" & CStrEx(AppSettings("GEOIP_IP_PARAMETER")) & strIPAddress
                    Return Send(strRequest)
                End If
            End If
        End If
        Return getSingleValueDB("SELECT TOP 1 URL FROM GEOIPRedirections WHERE CountryGuid=''")
    End Function

    Protected Overrides Function validate()
        Dim url As String = ""

        If CStrEx(Response()) <> CStrEx(AppSettings("GEOIP_ALLOWED_COUNTRY")) Then
            'The customer is not allowed to access this site based on its location
            'Getting the URL to redirect him to from the DB
            url = getSingleValueDB("SELECT TOP 1 URL FROM GEOIPRedirections WHERE CountryGuid='' OR CountryGuid=" & SafeString(CStrEx(Response())) & " ORDER BY CountryGuid DESC")
            Return url
        End If
        Return url
    End Function

    Public Sub GEOIPRedirection()
        Dim urlRedirection As String
        urlRedirection = ValidateCountry()
        If urlRedirection <> "" Then
            Response.Redirect(urlRedirection)
        End If
    End Sub


End Class
