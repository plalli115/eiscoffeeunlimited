Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Collections.Generic
Imports System.Data
Imports System.Net
Imports ExpandIT.GlobalsClass

Namespace ExpandIT

    Public Class USUOM

        Public Shared Function GetProductUOMQuantity(ByVal prdGuid As String, ByVal UOM As String)

            Dim sql As String

            sql = "SELECT QtyPerUnitOfMeasure FROM ProductTable INNER JOIN ItemUnitOfMeasure ON (ProductTable.ProductGuid = ItemUnitOfMeasure.ItemNo)  WHERE ProductTable.ProductGuid=" & SafeString(prdGuid) & " AND ItemUnitOfMeasure.Code=" & SafeString(UOM)

            If CDblEx(getSingleValueDB(sql)) <= 0 Then
                GetProductUOMQuantity = 1
            Else
                GetProductUOMQuantity = CDblEx(getSingleValueDB(sql))
            End If

        End Function

        

        'AM2010051801 - SPECIAL ITEMS - Start
        Public Sub NFSpecialPrice(ByVal orderobject As ExpDictionary, ByVal Context As ExpDictionary)
            Dim sql As String
            Dim item As ExpDictionary
            Dim orderline As ExpDictionary
            Dim key As Object
            Dim resultDict As ExpDictionary
            Dim ProductQuantities As ExpDictionary
            Dim VariantTotalQuantity As Decimal
            Dim ProductVariantCode As String
            Dim ProductTotalQuantity As Decimal
            Dim ItemMinimumQuantity As Decimal
            Dim ItemVariantCode As String
            Dim dtResult As DataTable
            Dim currProduct As String
            Dim counter As Integer
            Dim lowPrice As Decimal
            Dim lowKey As String
            Dim priceDict As ExpDictionary
            Dim key2 As String
            Dim lowPriceC As Decimal
            Dim lowKeyC As String
            Dim isCurrencyPrice As Boolean
            Dim unitPrice As Double
            Dim campaign As String = ""


            'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - Start
            'campaign = CStrEx(getSingleValueDB("SELECT SpecialsCampaign FROM EESetup"))
            Try
                Dim pageobj As ExpandIT.Page = CType(System.Web.HttpContext.Current.Handler, ExpandIT.Page)
                campaign = CStrEx(pageobj.eis.getEnterpriseConfigurationValue("SpecialsCampaign"))
            Catch ex As Exception
            End Try
            'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - End

            ProductQuantities = Context("ProductQuantities")

            sql = "SELECT ProductGuid, AllowInvoiceDiscount, Attain_VATBusPostingGrPrice, CustomerRelType, " & _
                "MinimumQuantity, EndingDate, CustomerRelGuid, CurrencyGuid, StartingDate, UnitPrice, " & _
                "UOMGuid, VariantCode, PriceInclTax, AllowLineDiscount FROM Attain_ProductPrice " & _
                "WHERE ProductGuid IN (" & Context("ProductList") & ") AND " & _
                "(CustomerRelType = 3 AND CustomerRelGuid = " & SafeString(campaign) & ") AND " & _
                "(StartingDate <= " & SafeDatetime(DateValue(Now)) & " OR StartingDate IS NULL) AND " & _
                "(EndingDate >= " & SafeDatetime(DateValue(Now)) & " OR EndingDate IS NULL) AND "
            sql = sql & "(CurrencyGuid = " & SafeString(orderobject("CurrencyGuid")) & " OR CurrencyGuid IS NULL OR CurrencyGuid = '' OR CurrencyGuid = " & SafeString(Context("DefaultCurrency")) & ")"
            sql = sql & " ORDER BY ProductGuid"

            ' Header conditions: IN ProductGuidList, CustomerRelType, CustomerRelGuid, StartingDate, EndingDate, CurrencyGuid
            ' Line conditions: ProductGuid, UOMGuid, VariantCode, MinimumQuantity

            ' ProdcutGuid      IN ProductList
            ' CustomerRetType  Customer, Group, All
            ' CustomerRetGuid  (CustomerRelType == Customer AND CustomerRelGuid == User("CustomerGuid")) OR
            '                  (CustomerRelType == CustomerGrp AND CustomerRelGuid == User("CustomerGuid")) OR
            '                  (CustomerRelType == All)
            ' StartingDate        (NOW > StartingDate AND Now < EndingDate)
            ' EndingDate
            ' CurrencyGuid        CurrencyGuid == User("CurrencyGuid") (If Blank CurrencyGuid == Default Currency guid - Take into consideration
            ' VariantCode      VarinatCode = Item("VarinatCode") or not used
            ' UOMGuid          UOMGuid = Item(UnitOfMeasureGuid)
            ' MinimumQuantity  OrderLine("Qunatity") >= MinimumQuantity

            ' If one or more prices is found in the customers currency (different from default currency) then the best currency price is always used.

            dtResult = SQL2DataTable(sql)
            resultDict = New ExpDictionary

            currProduct = ""
            counter = 0

            ' ***************************************************************
            ' *** Add the prducts to the result dictionary.
            ' ***************************************************************
            For Each row As DataRow In dtResult.Rows
                If currProduct <> CStrEx(row("ProductGuid")) Then
                    currProduct = CStrEx(row("ProductGuid"))
                    counter = 1
                    resultDict.Add(currProduct, New ExpDictionary())
                End If
                resultDict(currProduct).Add(CStrEx(counter), SetDictionary(row))
                counter = counter + 1
            Next

            For Each key In orderobject("Lines").keys
                orderline = orderobject("Lines")(key)

                Dim UOMItem As String = ""
                UOMItem = CStrEx(orderline("UOM"))
                If UOMItem = "" Then
                    UOMItem = CStrEx(orderline("BaseUOM"))
                End If

                ' Search and find the lowest price.
                ' Line conditions: ProductGuid, UOMGuid, VariantCode, MinimumQuantity
                'If Not UOMItem Is Nothing Then
                '    orderline("ListPrice") = GetProductUOMQuantity(orderline("ProductGuid"), UOMItem) * orderline("ListPrice")
                'End If
                lowPrice = orderline("ListPrice")
                lowKey = ""
                lowPriceC = -1
                lowKeyC = ""

                If Not resultDict(CStrEx(orderline("ProductGuid"))) Is Nothing Then
                    priceDict = resultDict(CStrEx(orderline("ProductGuid")))

                    ' Search the results. Remeber the lowest price.
                    For Each key2 In priceDict.Keys
                        item = priceDict(key2)
                        If CStrEx(item("UOMGuid")) = "" Or CStrEx(item("UOMGuid")) = CStrEx(UOMItem) Then
                            If CStrEx(item("UOMGuid")) = "" And CStrEx(UOMItem) <> "" Then
                                unitPrice = GetProductUOMQuantity(orderline("ProductGuid"), UOMItem) * item("UnitPrice")
                                If AppSettings("NAV_COLLECT_QUANTITES_FOR_LINE_DISCOUNTS") Then
                                    ProductTotalQuantity = CDblEx(ProductQuantities(orderline("ProductGuid"))("Total")) * GetProductUOMQuantity(orderline("ProductGuid"), UOMItem)
                                Else
                                    ProductTotalQuantity = orderline("Quantity") * GetProductUOMQuantity(orderline("ProductGuid"), UOMItem)
                                End If
                            Else
                                unitPrice = item("UnitPrice")
                                If AppSettings("NAV_COLLECT_QUANTITES_FOR_LINE_DISCOUNTS") Then
                                    ProductTotalQuantity = CDblEx(ProductQuantities(orderline("ProductGuid"))("Total"))
                                Else
                                    ProductTotalQuantity = orderline("Quantity")
                                End If
                            End If


                            ItemMinimumQuantity = CDblEx(item("MinimumQuantity"))
                            ItemVariantCode = CStrEx(item("VariantCode"))
                            ProductVariantCode = CStrEx(orderline("VariantCode"))

                            isCurrencyPrice = (CStrEx(item("CurrencyGuid")) = CStrEx(orderobject("CurrencyGuid")) And CStrEx(item("CurrencyGuid")) <> CStrEx(Context("DefaultCurrency")))

                            If ProductVariantCode = "" Then
                                If (ProductTotalQuantity >= ItemMinimumQuantity) And _
                                    (ItemVariantCode = "") _
                                Then
                                    If isCurrencyPrice Then
                                        If (unitPrice < lowPriceC And lowKeyC <> "") Or (lowKeyC = "") Then
                                            lowPriceC = unitPrice
                                            lowKeyC = key2
                                        End If
                                    Else
                                        If (unitPrice < lowPrice And lowKeyC = "") Or (lowKey = "" And lowKeyC = "") Then
                                            lowPrice = unitPrice
                                            lowKey = key2
                                        End If
                                    End If
                                End If
                            Else
                                If AppSettings("NAV_COLLECT_QUANTITES_FOR_LINE_DISCOUNTS") Then
                                    VariantTotalQuantity = CDblEx(ProductQuantities(orderline("ProductGuid"))("Variants")(CStrEx(orderline("VariantCode"))))
                                Else
                                    VariantTotalQuantity = orderline("Quantity")
                                End If

                                If (((ProductTotalQuantity >= ItemMinimumQuantity) And _
                                    (ItemVariantCode = "")) Or _
                                    ((VariantTotalQuantity >= ItemMinimumQuantity) And _
                                    (ItemVariantCode = ProductVariantCode))) _
                                Then
                                    If isCurrencyPrice Then
                                        If (unitPrice < lowPriceC And lowKeyC <> "") Or (lowKeyC = "") Then
                                            lowPriceC = unitPrice
                                            lowKeyC = key2
                                        End If
                                    Else
                                        If (unitPrice < lowPrice And lowKeyC = "") Or (lowKey = "" And lowKeyC = "") Then
                                            lowPrice = unitPrice
                                            lowKey = key2
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    Next

                    ' always use a Currency price, if found.
                    If lowKeyC <> "" Then
                        lowKey = lowKeyC
                        lowPrice = lowPriceC
                    End If

                    ' Use the low price, if found.
                    If lowKey <> "" Then
                        item = priceDict(lowKey)

                        orderline("originalListPrice") = orderline("ListPrice")
                        'orderline("ListPrice") = item("UnitPrice")
                        orderline("ListPrice") = lowPrice
                        orderline("ListPriceIsInclTax") = item("PriceInclTax")
                        orderline("AllowLineDisc") = item("AllowLineDiscount")
                        orderline("AllowInvoiceDiscount") = item("AllowInvoiceDiscount")
                        orderline("Attain_VATBusPostingGrPrice") = item("Attain_VATBusPostingGrPrice")
                        orderline("CurrencyGuid") = item("CurrencyGuid")
                    Else
                        ' Do nothing. Use the list price.
                    End If
                End If
            Next
        End Sub
        'AM2010051801 - SPECIAL ITEMS - End


    End Class

End Namespace
