Imports System.Configuration.ConfigurationManager
Imports Microsoft.VisualBasic
Imports ExpandIT.EISClass

Namespace ExpandIT
    Public Class Page
        Inherits System.Web.UI.Page

        Public globals As GlobalsClass
        Public eis As EISClass
        Public currency As CurrencyBaseClass
        Public store As StoreClass
        Public cart As CartClass
        Public b2b As B2BBaseClass
        ' Page Properties
        Protected description, keywords As String
        Protected m_RuntimeMasterPageFile As String = Nothing
        Protected m_CrumbName As String = ""

        Private _requiresSSL As Boolean

        ''' <summary>
        ''' Use property to set the page to use SSL
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <ComponentModel.Browsable(True)> _
        <ComponentModel.Description("Indicates if the page should use SSL or not")> _
        Public Overridable Property RequireSSL() As Boolean
            Get
                Return _requiresSSL
            End Get
            Set(ByVal value As Boolean)
                _requiresSSL = value
            End Set
        End Property

        ''' <summary>
        ''' Handles Init event
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks>Added 080825</remarks>
        Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
            PushSSL()
        End Sub

        ''' <summary>
        ''' Force request into the desired channel, http or https.
        ''' </summary>
        ''' <remarks>Added 080825</remarks>       
        Private Sub PushSSL()
            Const SECURE As String = "https://"
            Const UNSECURE As String = "http://"

            'Force required into secure channel
            If RequireSSL AndAlso Request.IsSecureConnection = False Then
					ExpandIT.FileLogger.log("RequireSSL true, but insecure connection, redirecting to HTTPS" )
                  Response.Redirect(Request.Url.ToString().Replace(UNSECURE, SECURE))
            End If

            'Force non-required out of secure channel
            If Not RequireSSL AndAlso Request.IsSecureConnection = True Then
					ExpandIT.FileLogger.log("RequireSSL false, but secure connection, redirecting to HTTP" )
                  Response.Redirect(Request.Url.ToString().Replace(SECURE, UNSECURE))
             End If
        End Sub

        Public Property RuntimeMasterPageFile() As String
            Get
                Return m_RuntimeMasterPageFile
            End Get
            Set(ByVal value As String)
                m_RuntimeMasterPageFile = value
            End Set
        End Property

        Public Property CrumbName() As String
            Get
                Return m_CrumbName
            End Get
            Set(ByVal value As String)
                m_CrumbName = value
            End Set
        End Property


        Protected Overrides Sub InitializeCulture()

            Dim langGuid As String = String.Empty

            ' Try to get LanguageGuid from session
            Try
                langGuid = HttpContext.Current.Session("LanguageGuid").ToString()
            Catch ex As Exception

            End Try

            ' If session is empty then look if LanguageGuid is set in cookie
            If String.IsNullOrEmpty(langGuid) Then
                Try
                    langGuid = HttpContext.Current.Request.Cookies("user")("LanguageGuid")
                Catch ex As Exception

                End Try
            End If

            Dim ci As System.Globalization.CultureInfo
            Try
                ci = New System.Globalization.CultureInfo(langGuid)

                Threading.Thread.CurrentThread.CurrentUICulture = ci
            Catch ex As Exception

            End Try

            MyBase.InitializeCulture()

        End Sub


        Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreInit
            If Not m_RuntimeMasterPageFile Is Nothing Then
                If m_RuntimeMasterPageFile = "" Then m_RuntimeMasterPageFile = "main.master"
                MasterPageFile = ConfigurationManager.AppSettings("MasterPageFolder") & "/" & m_RuntimeMasterPageFile
            End If
        End Sub

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            globals = New GlobalsClass(True)
            eis = globals.eis
            currency = globals.currency
            cart = globals.cart
            store = globals.store
            b2b = globals.b2b

            ' Set header information
            If Page.Header IsNot Nothing Then
                Title = Resources.Language.SITE_TITLE
            End If
        End Sub

    End Class

End Namespace

