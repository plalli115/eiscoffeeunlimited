Imports System.Configuration.ConfigurationManager
Imports Microsoft.VisualBasic
Imports System.IO

Namespace ExpandIT

    Public Class FileLogger

        'Private Shared logpath As String = HttpContext.Current.Server.MapPath("~/App_Data/filelog.txt")
        Private Shared logpath As String = ExpandIT.ExpandITLib.ApplicationRoot & "\App_Data\filelog.txt"
        Private Shared format As String = "{0}: {1}"

        Public Shared Sub log(ByVal message As String)
            Try
                Dim fs As New FileStream(logpath, FileMode.Append, FileAccess.Write, FileShare.ReadWrite)
                Dim fw As New StreamWriter(fs)
                Try
                    fw.WriteLine(String.Format(format, System.DateTime.Now().ToLongTimeString(), message))
                    fw.Flush()
                Catch ex As IOException
                    Finally
                    fw.Close()
                    fs.Close()
                End Try
            Catch ex As Exception

            End Try

        End Sub

        Public Shared Sub log(ByVal message As Object)
            Try
                Dim fs As New FileStream(logpath, FileMode.Append, FileAccess.Write, FileShare.ReadWrite)
                Dim fw As New StreamWriter(fs)
                Try
                    fw.WriteLine(String.Format(format, System.DateTime.Now().ToLongTimeString(), message))
                    fw.Flush()
                Catch ex As IOException
                    Finally
                    fw.Close()
                    fs.Close()
                End Try
            Catch ex As Exception

            End Try

        End Sub

        Public Shared Sub test(ByVal d As ExpDictionary)
            Dim kvPair As System.Collections.Generic.KeyValuePair(Of Object, Object)
            For Each kvPair In d
                If kvPair.Value IsNot Nothing Then
                    If Not kvPair.Value.Equals(DBNull.Value) Then
                        If kvPair.Value.GetType().Name.Equals("ExpDictionary") Then
                            test(kvPair.Value)
                        Else
                            WriteDict(kvPair.Value)
                        End If
                    End If
                End If
            Next
        End Sub

        Public Shared Sub WriteDict(ByVal message As Object)
            Try
                Dim fs As New FileStream(logpath, FileMode.Append, FileAccess.Write, FileShare.ReadWrite)
                Dim fw As New StreamWriter(fs)
                Try
                    fw.WriteLine(String.Format(format, System.DateTime.Now().ToLongTimeString(), message))
                    fw.Flush()
                Catch ex As IOException
                    Finally
                    fw.Close()
                    fs.Close()
                End Try
            Catch ex As Exception

            End Try

        End Sub


    End Class
End Namespace

