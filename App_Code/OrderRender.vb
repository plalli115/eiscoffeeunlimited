Imports System.Configuration.ConfigurationManager
Imports System.Text
Imports System.Web
Imports Microsoft.VisualBasic
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
' For access to international render strategy
Imports ExpandIT.GlobalsClass
Imports System.Collections.Generic


Public Class OrderRender

    Private Shared globals As GlobalsClass

    Public Shared Sub SetGlobals(ByVal _globals As GlobalsClass)
        globals = _globals
    End Sub

    Private Shared Function RenderAddress(ByVal OrderDict As ExpDictionary, ByVal eis As EISClass, ByVal prefix As String, ByVal header As String) As String
        Dim retv As New StringBuilder

        retv.Append("<table cellpadding=""0"" cellspacing=""0"" border=""0"" class=""Order_" & prefix & "Address"">")
        retv.Append("<tr class=""Order_" & prefix & "AddressHeader""><td class=""Order_" & prefix & "AddressHeader"">" & header & "</td></tr><tr><td>")
        retv.Append(LineIf(CStrEx(OrderDict(prefix & "CompanyName"))))
        retv.Append(LineIf(CStrEx(OrderDict(prefix & "ContactName"))))
        retv.Append(LineIf(CStrEx(OrderDict(prefix & "Address1"))))
        retv.Append(LineIf(CStrEx(OrderDict(prefix & "Address2"))))
        '<< Changed to respect setting in web.config >>
        If CBoolEx(AppSettings("SHOW_STATE_ON_SHIPPING_ADDRESS")) Then
            retv.Append(CStrEx(OrderDict(prefix & "CityName")) & ", " & CStrEx(OrderDict(prefix & "StateName")) & " " & CStrEx(OrderDict(prefix & "ZipCode")) & " " & "<br />")
        Else
            retv.Append(CStrEx(OrderDict(prefix & "ZipCode")) & " " & CStrEx(OrderDict(prefix & "CityName")) & "<br />")
        End If
        retv.Append(LineIf(GetCountryName(CStrEx(OrderDict(prefix & "CountryGuid")))))
        retv.Append(LineIf(CStrEx(OrderDict(prefix & "EmailAddress"))))
        retv.Append("</td></tr></table>")

        Return retv.ToString
    End Function

    '<!-- ' ExpandIT US - MPF - US Sales Tax - Start -->
    Public Shared Function Render(ByVal OrderDict As ExpDictionary, _
        ByVal eis As EISClass, ByVal ShowIncludingTax As Boolean, _
        ByVal ShowShippingAddress As Boolean, ByVal ShowBillingAddress As Boolean, _
        ByVal EditMode As Boolean, ByVal ShowImages As Boolean, ByVal ShowOrderNumber As Boolean, _
        ByVal ShowComment As Boolean, ByVal ShowPONumber As Boolean, ByVal ShowDate As Boolean, _
        ByVal ShowPaymentInfo As Boolean, ByVal ShowShippingInfo As Boolean, _
        ByVal isEmail As Boolean, _
        ByVal ShProvider As Object, ByVal ShowShippingMethodAsLink As Boolean, _
        Optional ByVal defaultButton As Button = Nothing, Optional ByVal blnHideTaxLine As Boolean = False) As String ' 20080602 - ExpandIT US - MPF - US Sales Tax Modification

        '<!-- ' ExpandIT US - MPF - US Sales Tax - End -->

        Dim retv As New StringBuilder

        If isEmail Then retv.Append("<div class=""Email"">") Else retv.Append("<div class=""Browser"">")
        retv.Append("<table cellpadding=""0"" cellspacing=""0"" border=""0"" class=""Order"" style=""width:100%;""><tr class=""Order""><td class=""Order"">")
        If ShowShippingAddress Or ShowBillingAddress Then
            retv.Append("<table cellpadding=""0"" cellspacing=""0"" border=""0"" class=""Order_Header""><tr class=""Order_Header"">")
            If ShowBillingAddress Then
                retv.Append("<td class=""OrderHeader_Address"">")
                retv.Append(RenderAddress(OrderDict, eis, "", Resources.Language.LABEL_BILL_TO))
                retv.Append("</td>")
            End If
            If ShowShippingAddress Then
                retv.Append("<td class=""OrderHeader_ShipToAddress"">")
                retv.Append(RenderAddress(OrderDict, eis, "ShipTo", Resources.Language.LABEL_SHIP_TO))
                retv.Append("</td>")
            End If
            retv.Append("</tr></table>")
        End If
        If ShowDate Then
            retv.Append("<table cellpadding=""0"" cellspacing=""0"" border=""0"" class=""Order_Date""><tr class=""Order_Date""><td class=""Order_DateCaption"">")
            retv.Append(Resources.Language.LABEL_DATE & "</td><td class=""Order_DateValue"">" & CDateEx(OrderDict("HeaderDate")).Date)
            retv.Append("</td></tr></table>")
        End If
        If ShowPaymentInfo Then
            retv.Append("<table cellpadding=""0"" cellspacing=""0"" border=""0"" class=""Order_PaymentInfo"">")
            retv.Append("<tr><td class=""Order_PaymentTypeCaption"">")
            retv.Append(Resources.Language.LABEL_PAYMENT_INFORMATION)
            retv.Append("</td>")
            retv.Append("<td class=""Order_PaymentTypeValue"">")
            retv.Append(HTMLEncode(GetPaymentTypeName(OrderDict("PaymentType"))))
            retv.Append("</td></tr>")

            'JA2011030701 - PAYMENT TABLE - START
            'If CStrEx(OrderDict("PaymentTransactionID")) <> "" Then
            '    retv.Append("<tr><td class=""Order_PaymentTransactionCaption"">")
            '    retv.Append(Resources.Language.LABEL_TRANSACTION_ID)
            '    retv.Append("</td>")
            '    retv.Append("<td class=""Order_PaymentTransactionValue"">")
            '    retv.Append(HTMLEncode(OrderDict("PaymentTransactionID")))
            '    retv.Append("</td>")
            '    retv.Append("</tr>")
            'End If
            If CStrEx(eis.getPaymentTableColumn(OrderDict("HeaderGuid"), "PaymentTransactionID")) <> "" Then
                retv.Append("<tr><td class=""Order_PaymentTransactionCaption"">")
                retv.Append(Resources.Language.LABEL_TRANSACTION_ID)
                retv.Append("</td>")
                retv.Append("<td class=""Order_PaymentTransactionValue"">")
                retv.Append(HTMLEncode(eis.getPaymentTableColumn(OrderDict("HeaderGuid"), "PaymentTransactionID")))
                retv.Append("</td>")
                retv.Append("</tr>")
            End If
            'JA2011030701 - PAYMENT TABLE - END
            retv.Append("</table>")
        End If
        'Display the order number
        If ShowOrderNumber Then
            retv.Append("<table cellpadding=""0"" cellspacing=""0"" border=""0"" class=""Order_PaymentInfo"">")
            retv.Append("<tr class=""Order_PONumber""><td class=""Order_PaymentTypeCaption"">")
            retv.Append(Resources.Language.LABEL_ORDER_NUMBER & "</td><td class=""Order_PaymentTypeValue"">" & CStrEx(OrderDict("CustomerReference")))
            retv.Append("</td></tr>")
            retv.Append("</table>")
        End If
        If ShowComment Or ShowPONumber Then
            retv.Append("<table cellpadding=""0"" cellspacing=""0"" border=""0"" class=""Order_AdditionalInfo"">")
            retv.Append("<tr class=""Order_AdditionalInfo_Label""><td colspan=""2"" class=""Order_AdditionalInfo_Label"">")
            retv.Append(Resources.Language.LABEL_ADDTIONAL_INFO)
            retv.Append("</td></tr>")
            If ShowPONumber Then
                retv.Append("<tr class=""Order_PONumber""><td class=""Order_PONumberCaption"">")
                retv.Append(Resources.Language.LABEL_INPUT_PONUMBER & "</td><td class=""Order_PONumberValue"">" & CStrEx(OrderDict("CustomerPONumber")))
                retv.Append("</td></tr>")
            End If
            If ShowComment Then
                retv.Append("<tr class=""Order_Comment""><td class=""Order_CommentCaption"">")
                retv.Append(Resources.Language.LABEL_INPUT_COMMENT & "</td><td class=""Order_CommentValue"">" & CStrEx(OrderDict("HeaderComment")))
                retv.Append("</td></tr>")
            End If
            retv.Append("</table>")
        End If

        retv.Append("<table cellpadding=""0"" cellspacing=""0"" border=""0"" class=""OrderLines"">")
        ' Headers
        retv.Append("<tr class=""CartLineHeaders"">")

        If AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_THUMBNAIL") = "1" And Not isEmail Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_THUMBNAIL") = "1" And isEmail Then _
            retv.Append("<th class=""CartLineHeader_Thumbnail""></th>")

        If AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_PRODUCTGUID") = "1" And Not isEmail Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_PRODUCTGUID") = "1" And isEmail Then _
            retv.Append("<th class=""CartLineHeader_ProductGuid"">" & Resources.Language.LABEL_PRODUCT & "</th>")

        If (AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_NAME") = "1" Or AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_NAMEANDVARIANT") = "1") And Not isEmail Or _
            (AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_NAME") = "1" Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_NAMEANDVARIANT") = "1") And isEmail Then _
            retv.Append("<th class=""CartLineHeader_Name"">" & Resources.Language.LABEL_NAME & "</th>")

        If AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_QUANTITY") = "1" And Not isEmail Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_QUANTITY") = "1" And isEmail Then _
            retv.Append("<th class=""CartLineHeader_Quantity"">" & Resources.Language.LABEL_QUANTITY & "</th>")

        'AM0122201001 - UOM - Start        
        If AppSettings("EXPANDIT_US_USE_UOM") = "1" And (AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_UOM") = "1" And Not isEmail Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_UOM") = "1" And isEmail) Then _
        retv.Append("<th class=""CartLineHeader_Price"">" & Resources.Language.LABEL_UOM & "</th>")
        'AM0122201001 - UOM - End

        If AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_PRICE") = "1" And Not isEmail Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_PRICE") = "1" And isEmail Then _
                    retv.Append("<th class=""CartLineHeader_Price"">" & Resources.Language.LABEL_PRICE & "</th>")

        If AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_DISCOUNTAMOUNT") = "1" And Not isEmail Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_DISCOUNTAMOUNT") = "1" And isEmail Then _
            retv.Append("<th class=""CartLineHeader_DiscountAmount"">" & Resources.Language.LABEL_DISC_AMOUNT & "</th>")

        If AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_DISCOUNTPCT") = "1" And Not isEmail Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_DISCOUNTPCT") = "1" And isEmail Then _
            retv.Append("<th class=""CartLineHeader_DiscountPct"">" & Resources.Language.LABEL_DISC_PROCENT & "</th>")

        If AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_TOTAL") = "1" And Not isEmail Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_TOTAL") = "1" And isEmail Then _
            retv.Append("<th class=""CartLineHeader_Total"">" & Resources.Language.LABEL_LINE_TOTAL & "</th>")

        If AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_COMMENT") = "1" And Not isEmail Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_COMMENT") = "1" And isEmail Then _
            retv.Append("<th class=""CartLineHeader_Comment"">" & Resources.Language.LABEL_LINE_COMMENT & "</th>")

        retv.Append("</tr>")

        Dim linecnt As Integer = 0
        Dim rowclass As String
        For Each orderline As ExpDictionary In OrderDict("Lines").values
            Dim product As New ExpandIT.ProductClass(orderline)

            linecnt = linecnt + 1
            If linecnt Mod 2 = 1 Then
                rowclass = "CartLineFields"
            Else
                rowclass = "CartLineFieldsAlt"
            End If
            retv.Append("<tr class=""" & rowclass & """>")
            ' Thumbnail
            If AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_THUMBNAIL") = "1" And Not isEmail Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_THUMBNAIL") = "1" And isEmail Then
                If ShowImages Then
                    retv.Append("<td class=""CartLineField_Thumbnail"">")
                    retv.Append("<a href=""" & product.ProductLinkURL & """>")
                    retv.Append("<img style=""" & product.ThumbnailURLStyle(100, 100) & """  src=""" & product.ThumbnailURL & """ alt=""" & product.ThumbnailText & """ border=""0"" /></a>")
                    retv.Append("</td>")
                End If
            End If
            ' ProductGuid
            If AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_PRODUCTGUID") = "1" And Not isEmail Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_PRODUCTGUID") = "1" And isEmail Then
                If Not isEmail Then retv.Append("<td class=""CartLineCaption_ProductGuid"">" & HTMLEncode(Resources.Language.LABEL_PRODUCT) & "</td>")
                retv.Append("<td class=""CartLineField_ProductGuid"">")
                retv.Append("<a href=""" & ProductLink(orderline("ProductGuid"), 0) & """>" & HTMLEncode(orderline("ProductGuid")) & "</a>")
                retv.Append("</td>")
            End If
            ' ProductName
            If AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_NAME") = "1" And Not isEmail Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_NAME") = "1" And isEmail Then
                retv.Append("<td class=""CartLineCaption_Name"">" & HTMLEncode(Resources.Language.LABEL_NAME) & "</td>")
                retv.Append("<td class=""CartLineField_Name"">")
                retv.Append("<a href=""" & ProductLink(orderline("ProductGuid"), 0) & """>" & HTMLEncode(orderline("ProductName")) & "</a>")
                retv.Append("</td>")
                ' VariantName
                If CStrEx(orderline("VariantCode")) <> "" Then
                    If AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_VARIANTNAME") = "1" And Not isEmail Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_VARIANTNAME") = "1" And isEmail Then
                        If Not isEmail Then retv.Append("<td class=""CartLineCaption_VariantName"">" & HTMLEncode(HttpContext.GetGlobalResourceObject("Language", "LABEL_VARIANTNAME")) & "</td>")
                        retv.Append("<td class=""CartLineField_VariantName"">")
                        retv.Append("<a href=""" & ProductLink(orderline("ProductGuid"), 0) & """>" & HTMLEncode(orderline("VariantName")) & "</a>")
                        retv.Append("</td>")
                    End If
                End If
            End If
            ' ProductNameAndVariant
            If AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_NAMEANDVARIANT") = "1" And Not isEmail Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_NAMEANDVARIANT") = "1" And isEmail Then
                retv.Append("<td class=""CartLineField_NameAndVariant""><div class=""CartLineField_NameAndVariant"">")
                retv.Append("<a href=""" & ProductLink(orderline("ProductGuid"), 0) & """>" & HTMLEncode(orderline("ProductName")))
                If CStrEx(orderline("VariantCode")) <> "" Then
                    retv.Append(" (" & HTMLEncode(orderline("VariantName")) & ")")
                End If
                retv.Append("</a></div></td>")
            End If
            ' ProductQuantity
            If EditMode Then
                retv.Append("<td class=""CartLineCaption_EditQuantity"">" & HTMLEncode(Resources.Language.LABEL_QUANTITY) & "</td>")
                retv.Append("<td class=""CartLineField_EditQuantity"">")
                retv.Append("<input type=""hidden"" name=""LineGuid"" value=""" & HTMLEncode(orderline("LineGuid")) & """ />")
                retv.Append("<input type=""hidden"" name=""VersionGuid"" value=""" & HTMLEncode(orderline("VersionGuid")) & """ />")
                retv.Append("<input type=""hidden"" name=""SKU"" value=""" & HTMLEncode(orderline("ProductGuid")) & """ />")
                retv.Append("<input type=""hidden"" name=""VariantCode"" value=""" & HTMLEncode(orderline("VariantCode")) & """ />")
                retv.Append("<input type=""hidden"" name=""Quantity_prev"" value=""" & HTMLEncode(orderline("Quantity")) & """ />")
                retv.Append("<input size=""5"" maxlength=""5"" class=""CartLineField_EditQuantity cartQuantity"" type=""text"" name=""Quantity"" value=""" & orderline("Quantity") & """")
                If defaultButton IsNot Nothing Then
                    retv.Append(" onkeydown=""" & eis.addOnKeyDownAttributeValue(defaultButton.ClientID) & """")
                End If
                retv.Append(" />")
                retv.Append("</td>")
            Else
                If AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_QUANTITY") = "1" And Not isEmail Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_QUANTITY") = "1" And isEmail Then
                    If Not isEmail Then retv.Append("<td class=""CartLineCaption_Quantity"">" & HTMLEncode(Resources.Language.LABEL_QUANTITY) & "</td>")
                    retv.Append("<td class=""CartLineField_Quantity"">")
                    retv.Append(HTMLEncode(orderline("Quantity")))
                    retv.Append("</td>")
                End If
            End If
            'AM0122201001 - UOM - Start
            If AppSettings("EXPANDIT_US_USE_UOM") = "1" And (AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_UOM") = "1" And Not isEmail Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_UOM") = "1" And isEmail) Then
                retv.Append("<td class=""CartLineField_Price"" style=""white-space:nowrap;"">")
                Dim uom As String = ""
                uom = CStrEx(getSingleValueDB("SELECT Description FROM UnitOfMeasure WHERE Code=" & SafeString(orderline("UOM"))))
                If uom <> "" Then
                    uom &= "(s)"
                End If
                retv.Append(HTMLEncode(uom))
                retv.Append("</td>")
            End If
            'AM0122201001 - UOM - End
            ' Unit Price
            If AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_PRICE") = "1" And Not isEmail Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_PRICE") = "1" And isEmail Then
                Dim unitprice As Decimal
                If ShowIncludingTax Then
                    unitprice = RoundEx(CDblEx(orderline("ListPriceInclTax")), AppSettings("NUMDECIMALPLACES"))
                Else
                    unitprice = RoundEx(CDblEx(orderline("ListPrice")), AppSettings("NUMDECIMALPLACES"))
                End If
                If Not isEmail Then retv.Append("<td class=""CartLineCaption_Price"">" & HTMLEncode(Resources.Language.LABEL_PRICE) & "</td>")
                retv.Append("<td class=""CartLineField_Price"" style=""white-space:nowrap;"">")
                retv.Append(HTMLEncode(CurrencyFormatter.FormatCurrency(unitprice, orderline("CurrencyGuid"))))
                retv.Append("</td>")
            End If
            ' Discounts
            If AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_DISCOUNTAMOUNT") = "1" And Not isEmail Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_DISCOUNTAMOUNT") = "1" And isEmail Then
                Dim discountamount As Decimal
                If ShowIncludingTax Then
                    discountamount = CDblEx(orderline("LineDiscountAmountInclTax"))
                Else
                    discountamount = CDblEx(orderline("LineDiscountAmount"))
                End If
                If Not isEmail Then retv.Append("<td class=""CartLineCaption_DiscountAmount"">" & HTMLEncode(Resources.Language.LABEL_DISC_AMOUNT) & "</td>")
                retv.Append("<td class=""CartLineField_DiscountAmount"">")
                retv.Append(HTMLEncode(CurrencyFormatter.FormatCurrency(discountamount, orderline("CurrencyGuid"))))
                retv.Append("</td>")
            End If
            If AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_DISCOUNTPCT") = "1" And Not isEmail Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_DISCOUNTPCT") = "1" And isEmail Then
                If Not isEmail Then retv.Append("<td class=""CartLineCaption_DiscountPct"">" & HTMLEncode(Resources.Language.LABEL_DISC_PROCENT) & "</td>")
                retv.Append("<td class=""CartLineField_DiscountPct"">")
                retv.Append(HTMLEncode(RoundEx(CDblEx(orderline("LineDiscount")), 4)) & "%")
                retv.Append("</td>")
            End If
            ' Line Total
            If AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_TOTAL") = "1" And Not isEmail Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_TOTAL") = "1" And isEmail Then
                Dim linetotal As Decimal
                If ShowIncludingTax Then
                    linetotal = CDblEx(orderline("TotalInclTax"))
                Else
                    linetotal = CDblEx(orderline("LineTotal"))
                End If
                If Not isEmail Then retv.Append("<td class=""CartLineCaption_Total"">" & HTMLEncode(Resources.Language.LABEL_LINE_TOTAL) & "</td>")
                retv.Append("<td class=""CartLineField_Total"">")
                retv.Append(HTMLEncode(CurrencyFormatter.FormatCurrency(linetotal, orderline("CurrencyGuid"))))
                retv.Append("</td>")
            End If
            ' Line Comments
            If AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_COMMENT") = "1" And Not isEmail Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_COMMENT") = "1" And isEmail Then
                If Not isEmail Then retv.Append("<td class=""CartLineCaption_Comment"">" & HTMLEncode(Resources.Language.LABEL_LINE_COMMENT) & "</td>")
                If EditMode Then
                    retv.Append("<td class=""CartLineField_EditComment""><div class=""CartLineField_EditComment"">")
                    retv.Append("<input class=""CartLineField_EditComment"" type=""text"" name=""LineComment"" size=""10"" maxlength=""50"" value=""" & HTMLEncode(orderline("LineComment")) & """><input type=""hidden"" name=""LineComment_prev"" value=""" & HTMLEncode(orderline("LineComment")) & """ />")
                    retv.Append("</div></td>")
                Else
                    retv.Append("<td class=""CartLineField_Comment""><div class=""CartLineField_Comment"">")
                    retv.Append(HTMLEncode(orderline("LineComment")))
                    retv.Append("</div></td>")
                End If
            End If
            ' Delete mark
            If AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_DELETE") = "1" And Not isEmail Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_DELETE") = "1" And isEmail Then
                If EditMode Then
                    Dim deletelink As String = "cart.aspx?cartaction=OutOfBasket&amp;LineGuid=" & HttpContext.Current.Server.UrlEncode(orderline("LineGuid").ToString)
                    If Not isEmail Then retv.Append("<td class=""CartLineCaption_Delete"">" & HTMLEncode(Resources.Language.LABEL_DELETE_ITEM_FROM_ORDER_PAD) & "</td>")
                    retv.Append("<td class=""CartLineField_Delete""  style=""padding-left: 10px; vertical-align: top;"" >")
                    retv.Append("<div class=""CartLineField_DeleteImage""><a href=""" & deletelink & """><img class=""CartLineField_DeleteImage"" border=""0"" src=""" & VRoot & "/images/p.gif"" alt=""" & Resources.Language.LABEL_DELETE_ITEM_FROM_ORDER_PAD & """ /></a></div>")
                    retv.Append("<div class=""CartLineField_DeleteText""><a href=""" & deletelink & """>" & Resources.Language.LABEL_DELETE_ITEM_FROM_ORDER_PAD & "</a></div>")
                    retv.Append("</td>")
                End If
            End If
            retv.Append("</tr>")
        Next

        retv.Append("</table>")



        retv.Append("<table cellpadding=""0"" cellspacing=""0"" border=""0"" class=""OrderTotals"">")
        retv.Append("<tr><td align=""right""><table>")
        ' Sub Total
        Dim subtotal As Decimal
        If ShowIncludingTax Then
            subtotal = CDblEx(OrderDict("SubTotalInclTax"))
        Else
            subtotal = CDblEx(OrderDict("SubTotal"))
        End If
        retv.Append("<tr class=""CartTotals_SubTotal"">")
        retv.Append("<td class=""CartTotals_SubTotalCaption"">" & Resources.Language.LABEL_SUB_TOTAL & "</td>")
        retv.Append("<td class=""CartTotals_SubTotalValue"">" & CurrencyFormatter.FormatCurrency(subtotal, OrderDict("CurrencyGuid")) & "</td>")
        retv.Append("</tr>")
        ' Discounts
        If AppSettings("SHOW_DISCOUNTS") Then
            Dim invoicediscount As Decimal
            If ShowIncludingTax Then
                invoicediscount = CDblEx(OrderDict("InvoiceDiscountInclTax"))
            Else
                invoicediscount = CDblEx(OrderDict("InvoiceDiscount"))
            End If
            retv.Append("<tr class=""CartTotals_InvoiceDiscount"">")
            retv.Append("<td class=""CartTotals_InvoiceDiscountCaption"">" & Resources.Language.LABEL_INVOICE_DISCOUNT & "</td>")
            retv.Append("<td class=""CartTotals_InvoiceDiscountValue"">" & CurrencyFormatter.FormatCurrency(invoicediscount, OrderDict("CurrencyGuid")) & "</td>")
            retv.Append("</tr>")
        End If
        ' Service Charge
        Dim servicecharge As Decimal
        If ShowIncludingTax Then
            servicecharge = CDblEx(OrderDict("ServiceChargeInclTax"))
        Else
            servicecharge = CDblEx(OrderDict("ServiceCharge"))
        End If
        retv.Append("<tr class=""CartTotals_ServiceCharge"">")
        retv.Append("<td class=""CartTotals_ServiceChargeCaption"">" & Resources.Language.LABEL_SERVICE_CHARGE & "</td>")
        retv.Append("<td class=""CartTotals_ServiceChargeValue"">" & CurrencyFormatter.FormatCurrency(servicecharge, OrderDict("CurrencyGuid")) & "</td>")
        retv.Append("</tr>")
        ' Shipping and Handling
        If AppSettings("SHIPPING_HANDLING_ENABLED") Then
            Dim shippingamount As Decimal
            If ShowIncludingTax Then
                shippingamount = CDblEx(OrderDict("ShippingAmountInclTax"))
            Else
                shippingamount = CDblEx(OrderDict("ShippingAmount"))
            End If
            retv.Append("<tr class=""CartTotals_Shipping"">")
            retv.Append("<td class=""CartTotals_ShippingCaption"">" & Resources.Language.LABEL_SHIPPING & "</td>")
            If shippingamount = 0 Then
                retv.Append("<td class=""CartTotals_FreeShippingValue"">" & Resources.Language.LABEL_FREE_SHIPPING_HANDLING_VALUE & "</td>")
            Else
                retv.Append("<td class=""CartTotals_ShippingValue"">" & CurrencyFormatter.FormatCurrency(shippingamount, OrderDict("CurrencyGuid")) & "</td>")
            End If
            retv.Append("</tr>")

            Dim handlingamount As Decimal
            If ShowIncludingTax Then
                handlingamount = CDblEx(OrderDict("HandlingAmountInclTax"))
            Else
                handlingamount = CDblEx(OrderDict("HandlingAmount"))
            End If
            retv.Append("<tr class=""CartTotals_Handling"">")
            retv.Append("<td class=""CartTotals_HandlingCaption"">" & Resources.Language.LABEL_HANDLING & "</td>")
            If handlingamount = 0 Then
                retv.Append("<td class=""CartTotals_FreeHandlingValue"">" & Resources.Language.LABEL_FREE_SHIPPING_HANDLING_VALUE & "</td>")
            Else
                retv.Append("<td class=""CartTotals_HandlingValue"">" & CurrencyFormatter.FormatCurrency(handlingamount, OrderDict("CurrencyGuid")) & "</td>")
            End If
            retv.Append("</tr>")
        End If

        ' Payment Fee
        If CDblEx(OrderDict("PaymentFeeAmount")) > 0 Then
            retv.Append("<tr class=""CartTotals_PaymentFee"">")
            retv.Append("<td class=""CartTotals_PaymentFeeCaption"">" & Resources.Language.LABEL_PAYMENT_FEE & "</td>")
            retv.Append("<td class=""CartTotals_PaymentFeeValue"">" & CurrencyFormatter.FormatCurrency(CDblEx(OrderDict("PaymentFeeAmount")), OrderDict("CurrencyGuid")) & "</td>")
            retv.Append("</tr>")
        End If

        '<!-- ' ExpandIT US - MPF - US Sales Tax - Start -->
        ' TAX
        ' 20080530 - ExpandIT US - MPF - US Sales Tax Modification
        If (AppSettings("SHOW_TAX") And AppSettings("EXPANDIT_US_USE_US_SALES_TAX")) Then
            retv.Append("<tr class=""CartTotals_Tax"">")
            retv.Append("<td class=""CartTotals_TaxCaption"">" & Resources.Language.LABEL_TAXAMOUNT & "</td>")
            retv.Append("<td class=""CartTotals_TaxValue"">" & CurrencyFormatter.FormatCurrency(OrderDict("TaxAmount"), OrderDict("CurrencyGuid")) & "</td>")
            retv.Append("</tr>")
        End If

        ' Total
        Dim totalamount As Decimal
        'If ShowIncludingTax Then
        '    totalamount = CDblEx(OrderDict("TotalInclTax"))
        'Else
        totalamount = CDblEx(OrderDict("Total"))
        'End If

        If AppSettings("ORDERRENDER_BROWSER_SHOWTOTAL_ICLTAX") Then
            retv.Append("<tr class=""CartTotals_Total"">")
            retv.Append("<td class=""CartTotals_TotalCaption"">" & Resources.Language.LABEL_TOTAL & " " & Resources.Language.LABEL_EXCLUSIVE_VAT & "</td>")
            retv.Append("<td class=""CartTotals_TotalValue"">" & CurrencyFormatter.FormatCurrency(totalamount, OrderDict("CurrencyGuid")) & "</td>")
            retv.Append("</tr>")
        End If

        '<!-- ' ExpandIT US - MPF - US Sales Tax - Start -->
        ' ---------- TAX ----------
        If AppSettings("SHOW_TAX") And Not AppSettings("EXPANDIT_US_USE_US_SALES_TAX") Then
            '<!-- ' ExpandIT US - MPF - US Sales Tax - End -->
            Dim taxAmounts As New ExpDictionary()
            Dim invoiceDiscount2 As Decimal = CDblEx(OrderDict("InvoiceDiscountPct"))
            If Not String.IsNullOrEmpty(CType(DBNull2Nothing(OrderDict("VatTypes")), String)) Then
                Try
                    Dim x As ExpDictionary = UnmarshallDictionary(OrderDict("VatTypes"))
                    For Each vatType As Decimal In x.Keys
                        taxAmounts(vatType) = x(vatType)
                    Next
                Catch ex As Exception
                    FileLogger.log(ex.Message)
                End Try
            Else
                For Each orderline As ExpDictionary In OrderDict("Lines").values
                    Dim orderLineTaxPct As Decimal = CDblEx(orderline("TaxPct"))
                    If Not taxAmounts.Exists(orderLineTaxPct) Then
                        taxAmounts.Add(orderLineTaxPct, CDblEx(orderline("TaxAmount")) * (100 - invoiceDiscount2) / 100)
                    Else
                        Dim calcLine As Decimal = (CDblEx(orderline("TaxAmount")) * (100 - invoiceDiscount2) / 100)
                        taxAmounts(orderLineTaxPct) = CDblEx(taxAmounts(orderLineTaxPct)) + calcLine
                    End If
                Next
            End If
            ' Check also the Header for
            ' 1. Handling tax amount
            ' 2. Shipping tax amount
            ' 3. Service charge tax amount 

            Dim shippingTaxAmount As Decimal = CDblEx(OrderDict("ShippingAmountInclTax")) - CDblEx(OrderDict("ShippingAmount"))
            Dim handlingTaxAmount As Decimal = CDblEx(OrderDict("HandlingAmountInclTax")) - CDblEx(OrderDict("HandlingAmount"))
            Dim serviceChargeAmount As Decimal = CDblEx(OrderDict("ServiceChargeInclTax")) - CDblEx(OrderDict("ServiceCharge"))

            Dim shippingTaxPct As Decimal = CDblEx(AppSettings("SHIPPING_TAX_PCT"))
            Dim handlingTaxPct As Decimal = CDblEx(OrderDict("TaxPct"))
            Dim serviceChargeTacPct As Decimal = CDblEx(OrderDict("TaxPct")) ' This might be a different value - must be investigated

            ' Add Shipping Tax amounts
            If Not taxAmounts.Exists(shippingTaxPct) Then
                taxAmounts.Add(shippingTaxPct, shippingTaxAmount)
            Else
                taxAmounts(shippingTaxPct) = taxAmounts(shippingTaxPct) + shippingTaxAmount
            End If

            ' Add Handling Tax Amounts
            If Not taxAmounts.Exists(handlingTaxPct) Then
                taxAmounts.Add(handlingTaxPct, handlingTaxAmount)
            Else
                taxAmounts(handlingTaxPct) = taxAmounts(handlingTaxPct) + handlingTaxAmount
            End If

            ' Add ServiceCharge Tax Amounts
            If Not taxAmounts.Exists(serviceChargeTacPct) Then
                taxAmounts.Add(serviceChargeTacPct, serviceChargeAmount)
            Else
                taxAmounts(serviceChargeTacPct) = taxAmounts(serviceChargeTacPct) + serviceChargeAmount
            End If

            For Each kvPair As KeyValuePair(Of Object, Object) In taxAmounts
                If Not CDblEx(kvPair.Key) = 0 And Not CDblEx(kvPair.Value) = 0 Then
                    retv.Append("<tr class=""CartTotals_Total"">")
                    retv.Append("<td class=""CartTotals_TotalCaption"">" & Resources.Language.LABEL_TAXAMOUNT & " " & kvPair.Key & "%" & "</td>")
                    retv.Append("<td class=""CartTotals_TotalValue"">" & CurrencyFormatter.FormatCurrency(kvPair.Value, OrderDict("CurrencyGuid")) & "</td>")
                    retv.Append("</tr>")
                End If
            Next

        End If
        retv.Append("<tr class=""CartTotals_Total"">")
        If AppSettings("ORDERRENDER_BROWSER_SHOWTOTAL_ICLTAX") Then
            retv.Append("<td class=""CartTotals_TotalCaption"">" & Resources.Language.LABEL_TOTAL & " " & Resources.Language.LABEL_INCLUSIVE_VAT & "</td>")
        Else
            retv.Append("<td class=""CartTotals_TotalCaption"">" & Resources.Language.LABEL_TOTAL & "</td>")
        End If
        retv.Append("<td class=""CartTotals_TotalValue"">" & CurrencyFormatter.FormatCurrency(CDblEx(OrderDict("TotalInclTax")), OrderDict("CurrencyGuid")) & "</td>")
        retv.Append("</tr>")

        'Debug.DebugValue(OrderDict, "OrderDict")
        '---------- TAX END ----------

        ' Shipping information
        If ShowShippingInfo Then
            Dim dictSHProviders As Object
            dictSHProviders = eis.GetShippingHandlingProviders(OrderDict("ShippingHandlingProviderGuid"), False)
            Dim dictSHProvider As ExpDictionary = GetFirst(dictSHProviders).Value
            Dim sql = "SELECT     COUNT(*) FROM ShippingHandlingProvider"
            Dim ret = getSingleValueDB(sql)
            If Not ret Is Nothing Then
                If ret > 1 Then
                    '*****shipping*****-start
                    If Not CBoolEx(AppSettings("EXPANDIT_US_USE_SHIPPING")) Or (CBoolEx(AppSettings("EXPANDIT_US_USE_SHIPPING")) And Not EditMode) Then
                        retv.Append("<tr class=""Order_ShippingInfo"">")
                        retv.Append("<td class=""Order_ShippingInfoCaption"">")
                        retv.Append(Resources.Language.LABEL_HANDLING_AND_SHIPPING)
                        retv.Append("</td>")
                        retv.Append("<td class=""Order_ShippingInfoValue"">")
                        If ShowShippingMethodAsLink Then retv.Append("<a href=""" & VRoot & "/shippingprovider.aspx?returl=" & HttpUtility.UrlEncode(HttpContext.Current.Request.Path) & """>")
                        If dictSHProvider IsNot Nothing Then
                            retv.Append(IIf(CStrEx(dictSHProvider("ProviderName")) <> "", HTMLEncode(CStrEx(dictSHProvider("ProviderName"))), Resources.Language.LABEL_NOT_SELECTED))
                        Else
                            retv.Append(Resources.Language.LABEL_NOT_SELECTED)
                        End If
                        If ShowShippingMethodAsLink Then retv.Append("</a>")
                        retv.Append("</td>")
                        retv.Append("</tr>")
                    End If
                    '*****shipping*****-end
                End If
            End If
        End If


        If Not EditMode Or isEmail Then
            retv.Append("</table></td></tr>")
        Else

            If CStrEx(AppSettings("MasterPageFolder")).Contains("Enterprise") Then
                retv.Append("</table></td><td style=""width:20px;""></td></tr>")
            Else
                retv.Append("</table></td><td style=""width:39px;""></td></tr>")
            End If

        End If

        retv.Append("</table>")
        ' Tax text
        retv.Append("<table cellpadding=""0"" cellspacing=""0"" border=""0"" class=""Order_TAX_Message""><tr class=""Order_TAX_Message""><td class=""Order_TAX_Message"">")
        If ShowIncludingTax Then
            retv.Append("* " & Resources.Language.LABEL_PRICES_ARE_INLUDING_TAX)
        Else
            retv.Append("* " & Resources.Language.LABEL_PRICES_ARE_EXLUDING_TAX)
        End If
        retv.Append("</td></tr></table>")

        retv.Append("</td></tr></table>")
        retv.Append("</div>")
        Return retv.ToString
    End Function

    Protected Shared Sub RenderTaxLine(ByVal LabelText As String, ByVal Value As String, ByRef retv As StringBuilder)
        retv.Append("<tr class=""CartTotals_Tax"">")
        retv.Append("<td class=""CartTotals_TaxCaption"">" & LabelText & "</td>")
        retv.Append("<td class=""CartTotals_TaxValue"">" & Value & "</td>")
        retv.Append("</tr>")
    End Sub

    Private Shared Function LineIf(ByVal linetext As String) As String
        If linetext <> "" Then
            Return linetext & "<br />"
        Else
            Return ""
        End If
    End Function


    Private Shared Function GetMiniCartTotalText(ByVal MiniCartInfo As ExpDictionary) As String
        Dim result As String = ""
        Dim totalValue As Decimal = IIf(AppSettings("SHOW_TAX_TYPE") = "INCL", MiniCartInfo("TotalInclTax"), MiniCartInfo("Total"))
        If CBoolEx(AppSettings("MINICART_DISPLAY_TOTAL")) Then
            result = String.Format(" /{0}", HTMLEncode(CurrencyFormatter.FormatCurrency(totalValue, MiniCartInfo("CurrencyGuid"))))
        End If
        Return result
    End Function


    Public Shared Function RenderMiniCart(ByVal MiniCartInfo As ExpDictionary, ByVal ShowPrice As Boolean) As String
        Dim result As String = ""
        If MiniCartInfo("Counter") > 1 Then
            If ShowPrice Then
                result = Replace(Resources.Language.LABEL_SEVERAL_ITEMS_IN_CART, "%1", MiniCartInfo("Counter")) & GetMiniCartTotalText(MiniCartInfo)
            Else
                result = Replace(Resources.Language.LABEL_SEVERAL_ITEMS_IN_CART, "%1", MiniCartInfo("Counter"))
            End If
        ElseIf MiniCartInfo("Counter") = 1 Then
            If ShowPrice Then
                result = Replace(Resources.Language.LABEL_ONE_ITEM_IN_CART, "%1", MiniCartInfo("Counter")) & GetMiniCartTotalText(MiniCartInfo)
            Else
                result = Replace(Resources.Language.LABEL_ONE_ITEM_IN_CART, "%1", MiniCartInfo("Counter"))
            End If
        Else
            result = Resources.Language.LABEL_ORDER_PAD_IS_EMPTY
        End If
        Return result
    End Function

End Class
