﻿Imports Microsoft.VisualBasic
Imports ExpandIT
Imports ExpandIT.EISClass

Namespace ExpandIT.Buttons


    Public Class ExpButton
        Inherits Button

        Public globals As GlobalsClass
        Public eis As EISClass
        Private m_ButtonText As String

        Public Property ButtonText() As String
            Get
                Dim str As String = ViewState(Me.UniqueID & "ButtonText")
                If String.IsNullOrEmpty(str) Then
                    If String.IsNullOrEmpty(m_ButtonText) Then
                        Return String.Empty
                    Else
                        Return m_ButtonText
                    End If
                Else
                    Return str
                End If
            End Get
            Set(ByVal value As String)
                ViewState(Me.UniqueID & "ButtonText") = value
                m_ButtonText = value
            End Set
        End Property

        Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
            MyBase.OnLoad(e)
            Dim mypage As ExpandIT.Page = Me.Page
            If mypage Is Nothing Then
                globals = New GlobalsClass(True)
            Else
                globals = mypage.globals
            End If

            eis = globals.eis
        End Sub

        Protected Overrides Sub OnPreRender(ByVal e As System.EventArgs)
            MyBase.OnPreRender(e)
            If Not String.IsNullOrEmpty(ButtonText) Then
                Me.Text = HttpContext.GetGlobalResourceObject("Language", ButtonText)
            End If
        End Sub

        Protected Overrides Sub AddAttributesToRender(ByVal writer As System.Web.UI.HtmlTextWriter)
            MyBase.AddAttributesToRender(writer)
        End Sub

    End Class
End Namespace
