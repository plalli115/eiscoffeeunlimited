Imports Microsoft.VisualBasic
Imports System
Imports System.Web.UI.WebControls
Imports ExpandIT
Imports ExpandIT.EISClass
Imports System.Globalization

Namespace ExpandIT.Buttons
    Public Class ExpLinkButton
        Inherits LinkButton

        Public globals As GlobalsClass
        Public eis As EISClass
        Private m_labelText As String

        Public Property LabelText() As String
            Get
                Dim str As String = ViewState(Me.UniqueID & "LabelText")
                If Not String.IsNullOrEmpty(str) Then
                Return ViewState(Me.UniqueID & "LabelText")
                Else
                    str = m_labelText
                    If Not String.IsNullOrEmpty(str) Then
                        Return m_labelText
                    End If
                End If
                Return String.Empty
            End Get
            Set(ByVal value As String)
                ViewState(Me.UniqueID & "LabelText") = value
                m_labelText = value
            End Set
        End Property

        Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)

            Page.ClientScript.RegisterStartupScript(Me.GetType(), "addClickFunctionScript", _addClickFunctionScript, True)

            Dim script As String = String.Format(_addClickScript, ClientID)
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "click_" + ClientID, _
                script, True)
            MyBase.OnLoad(e)
            Dim mypage As ExpandIT.Page = Me.Page
            If mypage Is Nothing Then
                Globals = New GlobalsClass(True)
            Else
                Globals = mypage.globals
            End If

            eis = globals.eis

        End Sub

        Protected Overrides Sub OnPreRender(ByVal e As System.EventArgs)
            MyBase.OnPreRender(e)
            If Not String.IsNullOrEmpty(LabelText) Then
                Me.Text = HttpContext.GetGlobalResourceObject("Language", LabelText)
            End If
        End Sub

        Const _addClickScript As String = "addClickFunction('{0}');"

        Const _addClickFunctionScript As String = _
            "  function addClickFunction(id) {{var b = document.getElementById(id);if (b && typeof(b.click) == 'undefined') b.click = function() {{var result = true; if (b.onclick) result = b.onclick();if (typeof(result) == 'undefined' || result) {{ eval(b.getAttribute('href')); }}}}}};"
    End Class
End Namespace