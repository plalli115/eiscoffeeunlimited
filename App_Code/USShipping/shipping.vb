﻿Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Collections.Generic
Imports System.Data
Imports System.Net
Imports ExpandIT.GlobalsClass
Imports System.Math
Imports Microsoft.VisualBasic


Namespace ExpandIT

    Public Class liveShipping
        Inherits RequestResponse

        Private sqlString As String = String.Empty
        Private agentCalculation As Integer = 0
        Private shippingService As String = ""
        Private errorMessage As String = ""
        Private selectedProvider As String
        Private selectedService As String
        Const CalculatedFieldName As String = "TotalInclTax"
        Private provider As String
        Private residential As Boolean
        Private SaturdayDelivery As Boolean
        Private insurance As Boolean
        '*****promotions*****-start
        Private promotions As promotions
        '*****promotions*****-end


        Public WriteOnly Property SqlStatement() As String
            Set(ByVal value As String)
                sqlString = value
            End Set
        End Property

#Region "ShippingCalculations"
        Public Function shippingProviderSelection(ByVal strProvider As String, ByVal strService As String, ByRef orderDict As ExpDictionary, ByRef context As ExpDictionary, ByVal ZipCode As String, ByVal globals As GlobalsClass, Optional ByVal updated As Boolean = False)

            Dim service As String
            Dim reader As System.Data.SqlClient.SqlDataReader
            Dim agentCalculation As Integer

            'If Not orderDict("IsCalculated") Or orderDict("ShippingAmount") = 0 Then
            '*****promotions*****-start
            promotions = New promotions(globals)
            '*****promotions*****-end
            selectedProvider = CStrEx(strProvider)
            selectedService = CStrEx(strService)
            If selectedProvider <> "NONE" And selectedService <> "NONE" Then
                orderDict("ShippingHandlingProviderGuid") = selectedProvider
                orderDict("ServiceCode") = CStrEx(selectedService)
                reader = GetDR("SELECT SHIPPINGSERVICE, AGENTCALCULATION FROM USSHIPPINGHANDLINGPROVIDER WHERE SHIPPINGHANDLINGPROVIDERGUID=" & SafeString(selectedProvider) & " AND SHIPPINGPROVIDERSERVICECODE=" & SafeString(strService) & "")
                If Not reader Is Nothing And reader.HasRows() Then
                    reader.Read()
                    service = reader("SHIPPINGSERVICE")
                    agentCalculation = reader("AGENTCALCULATION")
                    orderDict("ReCalculated") = True
                    If ZipCode <> "" Then
                        Select Case agentCalculation
                            Case 1 'UPS
                                provider = "UPS"
                                CalcShippingHandling(orderDict, context, service, ZipCode, updated)
                            Case 2 'FEDEX
                                provider = "FEDEX"
                                CalcShippingHandling(orderDict, context, service, ZipCode, updated)
                            Case 3 'USPS
                                provider = "USPS"
                                CalcShippingHandling(orderDict, context, service, ZipCode, updated)
                            Case 4 'DHL
                                provider = "DHL"
                                CalcShippingHandling(orderDict, context, service, ZipCode, updated, strService)
                        End Select
                        If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And CBoolEx(AppSettings("EXPANDIT_US_USE_ADJUSTMENT")) Then
                            orderDict("Total") += CDblEx(orderDict("AdjustmentAmount"))
                            orderDict("TotalInclTax") += CDblEx(orderDict("AdjustmentAmount"))
                        End If
                    End If
                Else
                    If Not orderDict("ShippingAmount") Is Nothing Then
                        orderDict("Total") = CDblEx(orderDict("Total")) - CDblEx(orderDict("ShippingAmount"))
                        orderDict("TotalInclTax") = CDblEx(orderDict("TotalInclTax")) - CDblEx(orderDict("ShippingAmountInclTax"))
                        orderDict("ShippingAmount") = 0
                        orderDict("ShippingAmountInclTax") = 0
                    End If
                End If
            Else
                If Not orderDict("ShippingAmount") Is Nothing Then
                    orderDict("Total") = CDblEx(orderDict("Total")) - CDblEx(orderDict("ShippingAmount"))
                    orderDict("TotalInclTax") = CDblEx(orderDict("TotalInclTax")) - CDblEx(orderDict("ShippingAmountInclTax"))
                    orderDict("ShippingAmount") = 0
                    orderDict("ShippingAmountInclTax") = 0
                End If
                If CStrEx(selectedProvider) = "" Then
                    orderDict("ShippingHandlingProviderGuid") = "NONE"
                End If
                If CStrEx(selectedService) = "" Then
                    orderDict("ServiceCode") = "NONE"
                End If
            End If

            Return errorMessage



            'End If
        End Function

        ''' <summary>
        ''' Calculate Shipping and Handling Prices
        ''' </summary>
        ''' <param name="OrderDict"></param>
        ''' <param name="Context"></param>
        ''' <param name="service"></param>
        ''' <param name="providerservice"></param>
        ''' <remarks>Prices are stored in the table using the default site currency.
        ''' Tax: handling is taxable, shipping is not taxable.
        ''' The tax pct is taken from the business logic. (OrderDict("TaxPct"))
        ''' </remarks>
        Public Sub CalcShippingHandling(ByRef OrderDict As ExpDictionary, ByRef Context As ExpDictionary, ByVal service As String, ByVal zipCode As String, ByVal updated As Boolean, Optional ByVal providerservice As String = "")
            Dim key As String
            Dim sum As Double
            Dim sql As String
            Dim ProviderGuid As String
            Dim tmp As ExpDictionary
            Dim aKey, arrKeys
            Dim XMLRequest, XMLResponse, XMLStatus, ErrorDescription, providerError, country As String
            Dim ShipAmt As Double
            Dim reader As System.Data.SqlClient.SqlDataReader
            Dim agentCalculation As Integer
            Dim shippingWeight As Double
            Dim InsuredValue As Double


            Dim nSelKey, aTaxPct As Object

            ProviderGuid = CStrEx(OrderDict("ShippingHandlingProviderGuid"))

            If Not IsDict(OrderDict("Lines")) Then Exit Sub

            ' Only recalculate the shipping and handling if it has changed, e.i. ReCalculated is true.
            If Not CBoolEx(OrderDict("ReCalculated")) Then Exit Sub

            ' Find the calculated value
            sum = 0

            For Each key In OrderDict("Lines").keys
                sum = sum + OrderDict("Lines")(key)(CalculatedFieldName)
                shippingWeight += OrderDict("Lines")(key)("ShippingWeight") * OrderDict("Lines")(key)("Quantity")
            Next

            OrderDict("CalculatedValue") = sum

            '' Convert the value to the site value, for making a correct lookup.
            'sum = currency.ConvertCurrency(sum, Context("CurrencyGuid"), Context("DefaultCurrency"))

            ShipAmt = 0
            XMLRequest = ""
            XMLResponse = ""
            XMLStatus = ""
            ErrorDescription = ""
            If OrderDict("CountryGuid") Is DBNull.Value Then
                OrderDict("CountryGuid") = AppSettings("DEFAULT_COUNTRYGUID")
            End If

            providerError = 1

            On Error Resume Next

            If Not OrderDict("ResidentialShipping") Is Nothing And CBoolEx(OrderDict("ResidentialShipping")) Then
                residential = True
            End If
            If Not OrderDict("SaturdayDelivery") Is Nothing And CBoolEx(OrderDict("SaturdayDelivery")) Then
                SaturdayDelivery = True
            End If
            If Not OrderDict("Insurance") Is Nothing And CBoolEx(OrderDict("Insurance")) Then
                insurance = True
                InsuredValue = CDblEx(OrderDict("InsuredValue"))
            End If

            'Handles UPS 
            If provider = "UPS" Then
                XMLRequest = UPSCreateXML(service, zipCode, OrderDict("CountryGuid"), OrderDict("CurrencyGuid"), shippingWeight, OrderDict("CalculatedValue"))
                If Send(XMLRequest, AppSettings("UPS_URL")) Then
                    providerError = 0
                    ShipAmt = GetShipAmt()
                    errorMessage = ""
                Else
                    ShipAmt = 0
                    errorMessage = GetError()
                End If

            ElseIf provider = "FEDEX" Then
                'Handles FEDEX
                XMLRequest = FEDEXCreateXML(service, zipCode, OrderDict("CountryGuid"), OrderDict("CurrencyGuid"), shippingWeight, InsuredValue)
                If Send(XMLRequest, AppSettings("FEDEX_URL")) Then
                    providerError = 0
                    ShipAmt = GetShipAmt()
                    errorMessage = ""
                Else
                    ShipAmt = 0
                    errorMessage = GetError()
                End If

            ElseIf provider = "USPS" Then
                'Handles USPS
                XMLRequest = USPSCreateXML(service, zipCode, shippingWeight, OrderDict)
                If Send(XMLRequest, AppSettings("USPS_URL")) Then
                    providerError = 0
                    ShipAmt = GetShipAmt()
                    errorMessage = ""
                Else
                    ShipAmt = 0
                    errorMessage = GetError()
                End If

            ElseIf provider = "DHL" Then
                'Handles DHL
                XMLRequest = DHLCreateXML(service, zipCode, OrderDict("CountryGuid"), OrderDict("CurrencyGuid"), shippingWeight, providerservice)
                If Send(XMLRequest, AppSettings("DHL_URL")) Then
                    providerError = 0
                    ShipAmt = GetShipAmt()
                    errorMessage = ""
                Else
                    ShipAmt = 0
                    errorMessage = GetError()
                End If

            End If

            OrderDict("Total") = RoundEx(OrderDict("Total") - CDblEx(OrderDict("ShippingAmount")), CLngEx(AppSettings("NUMDECIMALPLACES")))
            OrderDict("TotalInclTax") = RoundEx(OrderDict("TotalInclTax") - CDblEx(OrderDict("ShippingAmountInclTax")), CLngEx(AppSettings("NUMDECIMALPLACES")))



            Dim shippingDiscount As String = CStrEx(AppSettings("DISCOUNT_SHIPPING_AMOUNT"))
            Dim shippingDiscountAmount As Double = 0
            If shippingDiscount.Contains("%") Then
                shippingDiscount = shippingDiscount.TrimEnd("%")
                shippingDiscountAmount = CDblEx(shippingDiscount) / 100
                OrderDict("ShippingAmount") = ShipAmt * shippingDiscountAmount
            Else
                shippingDiscountAmount = CDblEx(shippingDiscount)
                OrderDict("ShippingAmount") = ShipAmt - shippingDiscountAmount
            End If



            '*****promotions*****-start
            If AppSettings("EXPANDIT_US_USE_PROMOTION_CODES") Then
                promotions.CalcShippingHandlingDiscount(OrderDict)
            End If
            '*****promotions*****-end

            '<!-- ' ExpandIT US - ANA - US Sales Tax Shipping - Start -->
            ' Use shipping tax pct from AppSettings

            'AM2010042901 - US SALES TAX ON HANDLING - Start
            If Not CBoolEx(AppSettings("EXPANDIT_US_USE_US_SALES_TAX")) Then
                'AM2010042901 - US SALES TAX ON HANDLING - End
                If OrderDict("TaxPct") Is Nothing Or OrderDict("TaxPct") Is DBNull.Value Then
                    OrderDict("TaxPct") = 0
                End If
                aTaxPct = (OrderDict("TaxPct") / 100)
                OrderDict("TaxAmount") = OrderDict("TaxAmount") + (aTaxPct * OrderDict("HandlingAmount"))
                OrderDict("HandlingAmountInclTax") = RoundEx(OrderDict("HandlingAmount") * (1 + aTaxPct), CLngEx(AppSettings("NUMDECIMALPLACES")))
                OrderDict("ShippingAmountInclTax") = RoundEx(OrderDict("ShippingAmount") * (1 + CDblEx(AppSettings("SHIPPING_TAX_PCT")) / 100), CLngEx(AppSettings("NUMDECIMALPLACES")))
                'Test
                Dim shippingTaxAmount As Decimal = CDblEx(OrderDict("ShippingAmountInclTax")) - CDblEx(OrderDict("ShippingAmount"))
                OrderDict("TaxAmount") = OrderDict("TaxAmount") + shippingTaxAmount
                ' The total including tax
                OrderDict("TotalInclTax") = OrderDict("TotalInclTax") + OrderDict("HandlingAmountInclTax") + OrderDict("ShippingAmountInclTax")
                OrderDict("ReCalculated") = False
            End If
            '<!-- ' ExpandIT US - ANA - US Sales Tax Shipping - End -->


            'The value whithout tax
            OrderDict("Total") = RoundEx(OrderDict("Total") + OrderDict("ShippingAmount") + CDblEx(OrderDict("HandlingAmount")), CLngEx(AppSettings("NUMDECIMALPLACES"))) '+ OrderDict("HandlingAmount"),CLngEx(Cfg("NUMDECIMALPLACES")))

            '<!-- ' ExpandIT US - ANA - US Sales Tax Shipping - Start -->
            If AppSettings("EXPANDIT_US_USE_SHIPPING_TAX") Then
                OrderDict("HandlingAmountInclTax") = CDblEx(OrderDict("HandlingAmount"))
                OrderDict("ShippingAmountInclTax") = CDblEx(OrderDict("ShippingAmount"))
                OrderDict("TotalInclTax") = OrderDict("TotalInclTax") + OrderDict("HandlingAmountInclTax") + OrderDict("ShippingAmountInclTax")
                Dim USSalesTax As New USSalesTax()
                USSalesTax.NFCalculateTaxShipping(OrderDict, Context)
            End If
            '<!-- ' ExpandIT US - ANA - US Sales Tax Shipping - End -->

            'AM2010042901 - US SALES TAX ON HANDLING - Start
            If AppSettings("EXPANDIT_US_USE_HANDLING_TAX") Then
                If Not CBoolEx(AppSettings("EXPANDIT_US_USE_SHIPPING_TAX")) Then
                    OrderDict("HandlingAmountInclTax") = CDblEx(OrderDict("HandlingAmount"))
                    OrderDict("ShippingAmountInclTax") = CDblEx(OrderDict("ShippingAmount"))
                    OrderDict("TotalInclTax") = OrderDict("TotalInclTax") + OrderDict("HandlingAmountInclTax") + OrderDict("ShippingAmountInclTax")
                End If
                Dim USSalesTax As New USSalesTax()
                USSalesTax.NFCalculateTaxHandling(OrderDict, Context)
            End If
            'AM2010042901 - US SALES TAX ON HANDLING - End

            'OrderDict("IsCalculated") = True

        End Sub

        ''' <summary>
        ''' Create XML Rate request for UPS 
        ''' </summary>
        ''' <param name="ServiceCode"></param>
        ''' <param name="strShipToZip"></param>
        ''' <param name="strShipToCountry"></param>
        ''' <param name="strCurrency"></param>
        ''' <returns>XML String</returns>
        ''' <remarks></remarks>
        Public Function UPSCreateXML(ByVal ServiceCode, ByVal strShipToZip, ByVal strShipToCountry, ByVal strCurrency, ByVal GrossWeight, ByVal declaredValue)
            Dim XmlStr, Version, Access, RatingOpen, RatingClose, ShipOpen, ShipClose, Request
            Dim PickupType, Shipper, ShipTo, Service, Package, Saturday

            Version = "<?xml version='1.0'?>"

            Access = "<AccessRequest xml:lang='en-US'>" & _
                "<AccessLicenseNumber>" & AppSettings("UPS_LICENSE") & "</AccessLicenseNumber>" & _
                "<UserId>" & AppSettings("UPS_USER") & "</UserId>" & _
                "<Password>" & AppSettings("UPS_PASSWORD") & "</Password>" & _
                "</AccessRequest>"

            RatingOpen = "<RatingServiceSelectionRequest xml:lang='en-US'>"

            Request = "<Request>" & _
                "<TransactionReference>" & _
                "<CustomerContext>Rating and Service</CustomerContext>" & _
                "<XpciVersion>1.0</XpciVersion>" & _
                "</TransactionReference>" & _
                "<RequestAction>Rate</RequestAction>" & _
                "<RequestOption>Rate</RequestOption>" & _
                "</Request>"

            PickupType = "<PickupType>" & _
                "<Code>" & AppSettings("UPS_PICKUP_TYPE") & "</Code>" & _
                "<Description>" & "" & "</Description>" & _
                "</PickupType>"

            ShipOpen = "<Shipment>"

            Shipper = "<Shipper>" & _
                "<Address>" & _
                "<PostalCode>" & AppSettings("UPS_SHIPPER_POSTCODE") & "</PostalCode>" & _
                "<CountryCode>" & AppSettings("UPS_SHIPPER_COUNTRY") & "</CountryCode>" & _
                "</Address>" & _
                "</Shipper>"

            ShipTo = "<ShipTo>" & _
            "<Address>" & _
            "<PostalCode>" & CStr(strShipToZip) & "</PostalCode>" & _
            "<CountryCode>" & strShipToCountry & "</CountryCode>"

            If residential Then
                ShipTo &= "<ResidentialAddressIndicator></ResidentialAddressIndicator>"
            End If

            ShipTo &= "</Address></ShipTo>"

            Service = "<Service><Code>" & ServiceCode & "</Code></Service>"

            Package = "<Package>" & _
                "<PackagingType>" & _
                "<Code>" & AppSettings("UPS_PACKAGING_TYPE") & "</Code>" & _
                "</PackagingType>" & _
                "<PackageWeight>" & _
                "<UnitOfMeasure>" & _
                "<Code>" & "LBS" & "</Code>" & _
                "</UnitOfMeasure>" & _
                "<Weight>" & GrossWeight & "</Weight>" & _
                "</PackageWeight>"

            If insurance Then
                Package &= "<PackageServiceOptions><InsuredValue><CurrencyCode>" & strCurrency & "</CurrencyCode>" & _
                    "<MonetaryValue>" & CDblEx(declaredValue) & "</MonetaryValue></InsuredValue></PackageServiceOptions>"
            End If

            Package &= "</Package>"

            If SaturdayDelivery Then
                Saturday = "<ShipmentServiceOptions>" & _
                           "<SaturdayDelivery></SaturdayDelivery>" & _
                           "</ShipmentServiceOptions>"
            End If

            ShipClose = "</Shipment>"

            RatingClose = "</RatingServiceSelectionRequest>"

            If SaturdayDelivery Then
                XmlStr = Version & Access & Version & RatingOpen & Request & PickupType & ShipOpen & Shipper & ShipTo & Service & Package & Saturday & _
            ShipClose & RatingClose
            Else
                XmlStr = Version & Access & Version & RatingOpen & Request & PickupType & ShipOpen & Shipper & ShipTo & Service & Package & _
                ShipClose & RatingClose
            End If


            Return XmlStr

        End Function


        ''' <summary>
        ''' Create XML Rate request for FEDEX 
        ''' </summary>
        ''' <param name="ServiceCode"></param>
        ''' <param name="strShipToZip"></param>
        ''' <param name="strShipToCountry"></param>
        ''' <param name="strCurrency"></param>
        ''' <returns>XML String</returns>
        ''' <remarks></remarks>
        Public Function FEDEXCreateXML(ByVal ServiceCode As String, ByVal strShipToZip As String, ByVal strShipToCountry As String, ByVal strCurrency As String, ByVal GrossWeight As String, ByVal InsuredValue As Double)
            Dim n1, n2, OrderDate, Version, Authentication
            Dim XmlStr, RequestOpen, RequestClose, ClientDetail, RequestedShipment

            n1 = DatePart("m", Date.Today.AddDays(1))
            n2 = DatePart("d", Date.Today.AddDays(1))
            Dim weight As Integer = Math.Ceiling(CDblEx(GrossWeight))

            OrderDate = DatePart("yyyy", Date.Today.AddDays(1)) & "-" & n1.ToString.PadLeft(2).Replace(" ", "0") & "-" & n2.ToString.PadLeft(2).Replace(" ", "0") & "T"
            OrderDate = DateTime.Now.ToString("o")


            RequestOpen = "<RateRequest xmlns=""http://fedex.com/ws/rate/v6"">"

            Authentication = "<WebAuthenticationDetail>" & _
                "<UserCredential>" & _
                "<Key>" & AppSettings("FEDEX_USER_KEY") & "</Key>" & _
                "<Password>" & AppSettings("FEDEX_USER_PASSWORD") & "</Password>" & _
                "</UserCredential>" & _
                "</WebAuthenticationDetail>"

            ClientDetail = "<ClientDetail>" & _
                "<AccountNumber>" & AppSettings("FEDEX_ACCOUNT_NUMBER") & "</AccountNumber>" & _
                "<MeterNumber>" & AppSettings("FEDEX_METER_NUMBER") & "</MeterNumber>" & _
                "</ClientDetail>"

            Version = "<Version>" & _
                "<ServiceId>crs</ServiceId>" & _
                "<Major>6</Major>" & _
                "<Intermediate>0</Intermediate>" & _
                "<Minor>0</Minor>" & _
                "</Version>"

            RequestedShipment = "<RequestedShipment>" & _
                "<ShipTimestamp>" & OrderDate & "</ShipTimestamp>" & _
                "<DropoffType>" & AppSettings("FEDEX_DROPOFF_TYPE") & "</DropoffType>"

            If residential And ServiceCode = "FEDEX_GROUND" Then
                RequestedShipment &= "<ServiceType>GROUND_HOME_DELIVERY</ServiceType>"
            Else
                RequestedShipment &= "<ServiceType>" & ServiceCode & "</ServiceType>"
            End If

            RequestedShipment &= "<PackagingType>" & AppSettings("FEDEX_PACKAGING_TYPE") & "</PackagingType>"



            RequestedShipment &= "<Shipper>" & _
                "<Address>" & _
                "<PostalCode>" & AppSettings("FEDEX_SHIPPER_POSTCODE") & "</PostalCode>" & _
                "<CountryCode>" & AppSettings("FEDEX_SHIPPER_COUNTRY") & "</CountryCode>" & _
                "</Address>" & _
                "</Shipper>" & _
                "<Recipient>" & _
                "<Address>" & _
                "<PostalCode>" & CStr(strShipToZip) & "</PostalCode>" & _
                "<CountryCode>" & strShipToCountry & "</CountryCode>"
            If residential Then
                RequestedShipment &= "<Residential>1</Residential>"
            End If
            RequestedShipment &= "</Address>" & _
                "</Recipient>" & _
                "<ShippingChargesPayment>" & _
                "<PaymentType>SENDER</PaymentType>" & _
                "</ShippingChargesPayment>"


            'SaturdayDelivery page 27
            If SaturdayDelivery Then
                RequestedShipment &= "<SpecialServicesRequested><SpecialServiceTypes>SATURDAY_DELIVERY</SpecialServiceTypes></SpecialServicesRequested>"
            End If

            RequestedShipment &= "<RateRequestTypes>LIST</RateRequestTypes>" & _
                "<PackageCount>1</PackageCount>" & _
                "<RequestedPackages>"
            If insurance Then
                RequestedShipment &= "<InsuredValue><Currency>" & strCurrency & "</Currency><Amount>" & InsuredValue & "</Amount></InsuredValue>"
            End If
            RequestedShipment &= "<Weight>" & _
                "<Units>LB</Units>" & _
                "<Value>" & GrossWeight & "</Value>" & _
                "</Weight>" & _
                "</RequestedPackages>"



            RequestClose = "</RequestedShipment></RateRequest>"
            XmlStr = RequestOpen & Authentication & ClientDetail & Version & RequestedShipment & RequestClose

            Return XmlStr

        End Function

        ''' <summary>
        ''' Create XML Rate request for USPS 
        ''' </summary>
        ''' <param name="ServiceCode"></param>
        ''' <param name="strShipToZip"></param>
        ''' <param name="GrossWeight"></param>
        ''' <returns>XML String</returns>
        ''' <remarks></remarks>
        Public Function USPSCreateXML(ByVal ServiceCode As String, ByVal strShipToZip As String, ByVal GrossWeight As String, ByRef OrderDict As ExpDictionary)
            Dim Authentication
            Dim Package, XmlStr, RequestClose, Version
            Dim pounds As Integer = 0
            Dim ounces As Double = 0

            pounds = Math.Floor(Double.Parse(GrossWeight))
            ounces = Double.Parse(GrossWeight) - pounds
            ounces = Math.Round(ounces * 16, 2)

            'n1 = DatePart("m", Date.Today.AddDays(1))
            'n2 = DatePart("d", Date.Today.AddDays(1))
            'OrderDate = DatePart("yyyy", Date.Today.AddDays(1)) & "-" & n1.ToString.PadLeft(2).Replace(" ", "0") & "-" & n2.ToString.PadLeft(2).Replace(" ", "0")


            Version = "API=RateV3&XML="

            Authentication = "<RateV3Request USERID=""" & AppSettings("USPS_USERID") & """ PASSWORD=""" & AppSettings("USPS_PASSWORD") & """>"

            Package = "<Package ID=""USPS"">"

            Package &= "<Service>" & ServiceCode & "</Service>"

            If ServiceCode = "FIRST CLASS" Then
                Package &= "<FirstClassMailType>PARCEL</FirstClassMailType>"
            End If
            Package &= "<ZipOrigination>" & AppSettings("USPS_SHIPPER_POSTCODE") & "</ZipOrigination>" & _
                "<ZipDestination>" & CStr(strShipToZip) & "</ZipDestination>" & _
                "<Pounds>" & pounds & "</Pounds>" & _
                "<Ounces>" & ounces & "</Ounces>" & _
                "<Container />" & _
                "<Size>REGULAR</Size>"
            If ServiceCode = "PARCEL" Then
                Package &= "<Machinable>true</Machinable>"
            End If

            ''Insurace Page 30 - USPSRateCalculatorsv20.pdf
            'If insurance Then
            '    Package &= "<ValueOfContents>" & OrderDict("CalculatedValue") & "</ValueOfContents>"
            'End If


            Package &= "</Package>"

            RequestClose = "</RateV3Request>"
            XmlStr = Version & Authentication & Package & RequestClose

            Return XmlStr

        End Function

        ''' <summary>
        ''' Create XML Rate request for DHL 
        ''' </summary>
        ''' <param name="ServiceCode"></param>
        ''' <param name="strShipToZip"></param>
        ''' <param name="strShipToCountry"></param>
        ''' <param name="strCurrency"></param>
        ''' <returns>XML String</returns>
        ''' <remarks></remarks>
        Public Function DHLCreateXML(ByVal ServiceCode As String, ByVal strShipToZip As String, ByVal strShipToCountry As String, ByVal strCurrency As String, ByVal GrossWeight As String, ByVal providerService As String)
            Dim n1, n2, OrderDate, Authentication, Destination, state
            Dim Payment, XmlStr, RequestOpen, RequestClose, ShipInfo

            n1 = DatePart("m", Date.Today.AddDays(1))
            n2 = DatePart("d", Date.Today.AddDays(1))
            OrderDate = DatePart("yyyy", Date.Today.AddDays(1)) & "-" & n1.ToString.PadLeft(2).Replace(" ", "0") & "-" & n2.ToString.PadLeft(2).Replace(" ", "0")
            state = AppSettings("DHL_DEFAULT_STATEGUID")

            RequestOpen = "<?xml version='1.0'?><Shipment action=""RateEstimate"" version=""1.0"">"

            Authentication = "<ShippingCredentials><ShippingKey>" & AppSettings("DHL_USER_KEY") & "</ShippingKey>" & _
                "<AccountNbr>" & AppSettings("DHL_ACCOUNT_NUMBER") & "</AccountNbr></ShippingCredentials>"

            ShipInfo = "<ShipmentDetail><ShipDate>" & OrderDate & "</ShipDate>" & _
                "<Service><Code>" & ServiceCode & "</Code></Service>" & _
                "<ShipmentType><Code>P</Code></ShipmentType>" & _
                "<Weight>" & GrossWeight & "</Weight>"

            If providerService = "EXP1030AM" Then
                ShipInfo &= "<SpecialServices><SpecialService><Code>1030</Code></SpecialService></SpecialServices>"
            ElseIf providerService = "EXPSATUR" Then
                ShipInfo &= "<SpecialServices><SpecialService><Code>SAT</Code></SpecialService></SpecialServices>"
            End If

            ShipInfo &= "</ShipmentDetail>"

            Payment = "<Billing><Party><Code>S</Code>" & _
                "</Billing>"

            Destination = "<Receiver><Address>" & _
               "<State>" & state & "</State>" & _
               "<Country>" & strShipToCountry & "</Country>" & _
               "<PostalCode>" & CStr(strShipToZip) & "</PostalCode>" & _
               "</Address></Receiver>"

            RequestClose = "</Shipment>"
            XmlStr = RequestOpen & Authentication & ShipInfo & Payment & Destination & RequestClose

            Return XmlStr

        End Function


        ''' <summary>
        ''' Parse response status 
        ''' </summary>
        ''' <param name="strResponse"></param>
        ''' <returns>Status string</returns>
        ''' <remarks></remarks>
        Function GetXMLStatus(ByVal strResponse)
            Dim ResponseStatus
            Dim xmlDoc As MSXML.DOMDocument

            ResponseStatus = ""

            xmlDoc = CreateObject("Microsoft.XMLDOM")
            xmlDoc.async = False
            xmlDoc.loadXML(strResponse)

            If (xmlDoc.parseError.errorCode <> 0) Then
                ResponseStatus = ""
            Else
                If provider = "UPS" Then
                    ResponseStatus = xmlDoc.getElementsByTagName("ResponseStatusDescription").item(0).text
                ElseIf provider = "FEDEX" Then
                    ResponseStatus = xmlDoc.getElementsByTagName("v6:HighestSeverity").item(0).text
                ElseIf provider = "USPS" Then
                    ResponseStatus = xmlDoc.getElementsByTagName("Rate").item(0)
                    If Not ResponseStatus Is Nothing Then Return "RATE"
                    Return "ERROR"
                ElseIf provider = "DHL" Then
                    ResponseStatus = xmlDoc.getElementsByTagName("Fault").item(0)
                    If Not ResponseStatus Is Nothing Then Return "ERROR"
                    Return "RATE"
                End If
            End If

            Return ResponseStatus

        End Function

        Protected Overrides Function validate() As Object

            Dim ResponseStatus
            Dim xmlDoc As MSXML.DOMDocument

            ResponseStatus = ""

            xmlDoc = CreateObject("Microsoft.XMLDOM")
            xmlDoc.async = False
            xmlDoc.loadXML(Response())

            If (xmlDoc.parseError.errorCode <> 0) Then
                Return False
            Else
                If provider = "UPS" Then
                    ResponseStatus = xmlDoc.getElementsByTagName("ResponseStatusDescription").item(0).text
                    If ResponseStatus.ToString().ToUpper().Contains("SUCCESS") Then
                        Return True
                    Else
                        Return False
                    End If
                ElseIf provider = "FEDEX" Then
                    ResponseStatus = xmlDoc.getElementsByTagName("v6:HighestSeverity").item(0).text
                    If ResponseStatus.ToString().ToUpper().Contains("SUCCESS") Or ResponseStatus.ToString().Contains("NOTE") Then
                        Return True
                    Else
                        Return False
                    End If
                ElseIf provider = "USPS" Then
                    ResponseStatus = xmlDoc.getElementsByTagName("Rate").item(0)
                    If Not ResponseStatus Is Nothing Then Return True
                    Return False
                ElseIf provider = "DHL" Then
                    ResponseStatus = xmlDoc.getElementsByTagName("Fault").item(0)
                    If Not ResponseStatus Is Nothing Then Return False
                    Return True
                End If
            End If

            Return False

        End Function



        ''' <summary>
        ''' Get error description if not successful 
        ''' </summary>
        ''' <returns>Error description string</returns>
        ''' <remarks></remarks>
        Function GetError()
            Dim xmlDoc, Description

            Description = ""

            xmlDoc = CreateObject("Microsoft.XMLDOM")
            xmlDoc.async = False
            xmlDoc.loadXML(Response())

            If (xmlDoc.parseError.errorCode <> 0) Then
                Description = ""
            Else
                If provider = "UPS" Then
                    xmlDoc.loadXML(xmlDoc.getElementsByTagName("Error").Item(0).xml)
                    Description = xmlDoc.getElementsByTagName("ErrorDescription").Item(0).text
                ElseIf provider = "FEDEX" Then
                    xmlDoc.loadXML(xmlDoc.getElementsByTagName("v6:Notifications").Item(0).xml)
                    Description = xmlDoc.getElementsByTagName("v6:Message").Item(0).text
                ElseIf provider = "USPS" Then
                    xmlDoc.loadXML(xmlDoc.getElementsByTagName("Error").Item(0).xml)
                    Description = xmlDoc.getElementsByTagName("Description").Item(0).text
                ElseIf provider = "DHL" Then
                    xmlDoc.loadXML(xmlDoc.getElementsByTagName("Fault").Item(0).xml)
                    Description = xmlDoc.getElementsByTagName("Description").Item(0).text
                End If
            End If

            Return Description

        End Function


        ''' <summary>
        ''' Parse Shipment amount
        ''' </summary>
        ''' <returns>Error Ship Amount</returns>
        ''' <remarks></remarks>
        Function GetShipAmt()
            Dim xmlDoc, ShipAmt, UPSError

            xmlDoc = CreateObject("Microsoft.XMLDOM")
            xmlDoc.async = False
            xmlDoc.loadXML(Response())

            If (xmlDoc.parseError.errorCode <> 0) Then
                UPSError = xmlDoc.parseError
                'Display error!
                ShipAmt = 0
            Else
                If provider = "UPS" Then
                    xmlDoc.loadXML(xmlDoc.getElementsByTagName("RatedShipment").Item(0).xml)
                    xmlDoc.loadXML(xmlDoc.getElementsByTagName("TotalCharges").Item(0).xml)
                    ShipAmt = xmlDoc.getElementsByTagName("MonetaryValue").Item(0).text
                ElseIf provider = "FEDEX" Then
                    xmlDoc.loadXML(xmlDoc.getElementsByTagName("v6:TotalNetCharge").Item(0).xml)
                    ShipAmt = xmlDoc.getElementsByTagName("v6:Amount").Item(0).text
                ElseIf provider = "USPS" Then
                    'If residential Then
                    xmlDoc.loadXML(xmlDoc.getElementsByTagName("Rate").Item(0).xml)
                    ShipAmt = xmlDoc.getElementsByTagName("Rate").Item(0).text

                    'Else
                    '    xmlDoc.loadXML(xmlDoc.getElementsByTagName("CommercialRate").Item(0).xml)
                    '    ShipAmt = xmlDoc.getElementsByTagName("CommercialRate").Item(0).text
                    'End If
                ElseIf provider = "DHL" Then
                    xmlDoc.loadXML(xmlDoc.getElementsByTagName("RateEstimate").Item(0).xml)
                    ShipAmt = xmlDoc.getElementsByTagName("TotalChargeEstimate").Item(0).text
                End If
            End If


            Return ShipAmt

        End Function

#End Region
        Public Function GetCustomerShippingService()
            Dim retval As Object = Nothing
            Try
                retval = getSingleValueDB("SELECT ServiceCode FROM CartHeader Where UserGuid = " & SafeString(HttpContext.Current.Session("UserGuid")))
            Catch ex As Exception

            End Try
            Return retval
        End Function

        Public Sub SetCustomerShippingService(ByVal value As Object)
            Try
                excecuteNonQueryDb("Update CartHeader Set ServiceCode = " & SafeString(value) & " Where UserGuid = " & SafeString(HttpContext.Current.Session("UserGuid")))
            Catch ex As Exception

            End Try
        End Sub

        Public Sub setCustomerShippingProvider(ByVal value As Object)
            Try
                excecuteNonQueryDb("Update CartHeader Set ShippingHandlingProviderGuid = " & SafeString(value) & " Where UserGuid = " & SafeString(HttpContext.Current.Session("UserGuid")))
            Catch ex As Exception

            End Try
        End Sub

        Public Function customerShippingProviderName(ByVal shGuid As Object)
            Dim shService As Object = GetCustomerShippingService()
            If shGuid IsNot Nothing AndAlso Not shGuid.Equals(DBNull.Value) AndAlso shService IsNot Nothing AndAlso Not shService.Equals(DBNull.Value) Then
                Return getSingleValueDB("SELECT ShippingHandlingProviderGuid + ' ' + ProviderDescription as ProviderName FROM USShippingHandlingProvider WHERE ShippingHandlingProviderGuid=" & SafeString(shGuid) & " and ShippingProviderServiceCode=" & SafeString(shService) & "")
            End If
            Return Nothing
        End Function

        Public Function PossibleShippingProvidersSQLString(ByVal isAppendConstraint As Boolean, ByVal userStatus As String) As String

            Dim sql As New StringBuilder()

            ' Find out if there is data in the PaymentType_ShippingHandlingProvider table
            ' If there is no data in the PaymentType_ShippingHandlingProvider table this means 
            ' that no relations are defined on valid combinations of payment types and shipping handling providers
            Dim o As Object = getSingleValueDB("SELECT TOP 1 ShippingProviderGuid FROM PaymentType_ShippingHandlingProvider")

            If o IsNot Nothing Then
                ' We have a relationship between payment types and shipping handling providers
                sql.Append("SELECT ShippingHandlingProviderGuid ,ShippingProviderServiceCode as ServiceCode, ProviderDescription FROM USShippingHandlingProvider " & _
                            "WHERE ShippingHandlingProviderGuid IN(SELECT DISTINCT ShippingProviderGuid FROM PaymentType_ShippingHandlingProvider " & _
                            "WHERE PaymentType IN(SELECT PaymentType FROM PaymentType WHERE UserType = '" & userStatus & "')) ")
                If isAppendConstraint Then
                    sql.Append(" AND ShippingHandlingProviderGuid NOT IN ('NONE') ORDER BY SortIndex")
                End If
            Else
                ' We don't have any relationships between payment types and shipping handling providers,
                ' so we just want to get all of the providers in the ShippingHandlingProvider table
                sql.Append("SELECT ShippingHandlingProviderGuid ,ShippingProviderServiceCode as ServiceCode, ProviderDescription FROM USShippingHandlingProvider ")
            End If

            Return sql.ToString()

        End Function

        Public Sub ApplyShipping(ByVal globals As GlobalsClass, ByVal selectedProvider As String, ByVal selectedService As String, ByVal zipcode As String, ByVal eis As EISClass, ByRef customerShippingProvider As Object)
            globals.OrderDict("ShippingHandlingProviderGuid") = selectedProvider
            globals.OrderDict("ServiceCode") = selectedService
            globals.OrderDict("ShipToZipCode") = zipcode
            eis.CalculateOrder()
            Dim custProvider As Object = customerShippingProvider
            ' if not - add a possible shipping provider to cartheader
            If custProvider.Equals(DBNull.Value) OrElse custProvider Is Nothing Then
                setCustomerShippingProvider("NONE")
            End If
            If custProvider <> globals.OrderDict("ShippingHandlingProviderGuid") And Not globals.OrderDict("ShippingHandlingProviderGuid") Is Nothing Then
                setCustomerShippingProvider(globals.OrderDict("ShippingHandlingProviderGuid"))
                custProvider = globals.OrderDict("ShippingHandlingProviderGuid")
            End If
            Dim custShService As Object = GetCustomerShippingService()
            ' if not - add a possible shipping provider to cartheader
            If custShService.Equals(DBNull.Value) OrElse custShService Is Nothing Then
                SetCustomerShippingService("NONE")
                custShService = "NONE"
            End If
            If custShService <> globals.OrderDict("ServiceCode") And Not globals.OrderDict("ServiceCode") Is Nothing Then
                SetCustomerShippingService(globals.OrderDict("ServiceCode"))
                custShService = globals.OrderDict("ServiceCode")
            End If
        End Sub

        Public Sub clearShippingData(ByVal globals As GlobalsClass, ByVal eis As EISClass)

            If globals.OrderDict Is Nothing Then globals.OrderDict = eis.LoadOrderDictionary(globals.User)
            globals.OrderDict("ShippingHandlingProviderGuid") = "NONE"
            globals.OrderDict("ServiceCode") = "NONE"
            globals.OrderDict("ShippingAmount") = 0
            globals.OrderDict("ShipToZipCode") = ""
            globals.OrderDict("ResidentialShipping") = False
            eis.SaveOrderDictionary(globals.OrderDict)
            globals.shippingError = ""
            Me.ErrorsHandler(globals)
        End Sub

        Public Sub checkShippingProvider(ByVal globals As GlobalsClass, ByVal custProvider As Object, ByVal eis As EISClass, ByVal customerShippingProvider As Object)
            Dim custShService As Object = GetCustomerShippingService()
            ' If customer has a cart

            If globals.OrderDict Is Nothing Then globals.OrderDict = eis.LoadOrderDictionary(globals.User)
            If custProvider IsNot Nothing Then

                If Not globals.OrderDict("ShippingHandlingProviderGuid") Is Nothing And Not globals.OrderDict("ShippingHandlingProviderGuid") Is DBNull.Value Then
                    If custProvider <> globals.OrderDict("ShippingHandlingProviderGuid") Then
                        custProvider = globals.OrderDict("ShippingHandlingProviderGuid")
                        setCustomerShippingProvider(globals.OrderDict("ShippingHandlingProviderGuid"))
                    End If
                End If
                ' find out if there is a shipping provider selected
                If custProvider.Equals(DBNull.Value) Then
                    ' No provider is selected, add one here
                    setCustomerShippingProvider("NONE")
                End If
            End If
            If custShService IsNot Nothing Then

                If Not globals.OrderDict("ServiceCode") Is Nothing And Not globals.OrderDict("ServiceCode") Is DBNull.Value Then
                    If custShService <> globals.OrderDict("ServiceCode") Then
                        custShService = globals.OrderDict("ServiceCode")
                        SetCustomerShippingService(globals.OrderDict("ServiceCode"))
                    End If
                End If
                ' find out if there is a shipping provider selected
                If custShService.Equals(DBNull.Value) Then
                    ' No provider is selected, add one here
                    SetCustomerShippingService("NONE")
                End If
            End If
        End Sub

        Public Sub cartLoad(ByVal globals As GlobalsClass, ByVal customerShippingProvider As String)
            If GetCustomerShippingService() IsNot Nothing Then
                If Not globals.OrderDict("ServiceCode").Equals(GetCustomerShippingService) Then
                    globals.OrderDict("ServiceCode") = IIf(GetCustomerShippingService() IsNot Nothing, GetCustomerShippingService, DBNull.Value)
                    globals.OrderDict("IsCalculated") = 0
                    globals.OrderDict("ReCalculated") = 1
                End If
            End If
            If Not globals.OrderDict("ShippingHandlingProviderGuid") Is Nothing And Not globals.OrderDict("ShippingHandlingProviderGuid") Is DBNull.Value Then
                If globals.OrderDict("ShippingHandlingProviderGuid") <> customerShippingProvider Then
                    customerShippingProvider = globals.OrderDict("ShippingHandlingProviderGuid")
                End If
            End If
            If Not globals.OrderDict("ServiceCode") Is Nothing And Not globals.OrderDict("ServiceCode") Is DBNull.Value Then
                If globals.OrderDict("ServiceCode") <> GetCustomerShippingService() Then
                    SetCustomerShippingService(globals.OrderDict("ServiceCode"))
                End If
            End If
        End Sub

        Public Sub ErrorsHandler(ByVal globals As GlobalsClass)
            If globals.messages.Errors.Count > 0 Then
                Dim index As Integer = 0
                Dim items2Delete As String = ""
                For index = 0 To globals.messages.Errors.Count - 1
                    If globals.messages.Errors.Item(index).StartsWith(Resources.Language.LABEL_SHIPPING_ERROR) Then
                        items2Delete &= "|" & globals.messages.Errors.Item(index)
                    End If
                Next
                items2Delete.Trim("|")
                If items2Delete <> "" Then
                    Dim items2DeleteArray() As String
                    items2DeleteArray = items2Delete.Split("|")
                    For index = 0 To items2DeleteArray.Length - 1
                        globals.messages.Errors.Remove(items2DeleteArray(index))
                    Next
                End If
            End If
            If globals.shippingError <> "" And Not globals.messages.Errors.Contains(globals.shippingError) Then
                globals.messages.Errors.Add(Resources.Language.LABEL_SHIPPING_ERROR & " " & globals.shippingError)
            End If
        End Sub

        Public Function hasShippingError(ByVal globals As GlobalsClass) As Boolean
            If globals.messages.Errors.Count > 0 Then
                Dim index As Integer = 0
                For index = 0 To globals.messages.Errors.Count - 1
                    If globals.messages.Errors.Item(index).StartsWith(Resources.Language.LABEL_SHIPPING_ERROR) Then
                        Return True
                    End If
                Next
            End If
            Return False
        End Function

    End Class
End Namespace