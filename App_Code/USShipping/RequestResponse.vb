Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Net

Public Class RequestResponse

    Private m_request As String
    Private m_server As String
    Private m_response As String

    Public Function Send(ByVal Str As String, ByVal Server As String)

        m_request = Str
        m_server = Server
        Return Resend()

    End Function

    'AM2010040601 - GEOIP - Start
    Public Function Send(ByVal Str As String)

        m_request = Str
        Return SendRequest()

    End Function
    'AM2010040601 - GEOIP - End

    Public Function Resend()

        Dim bytes As Byte()
        Dim request As HttpWebRequest
        Dim requeststream As System.IO.Stream
        Dim response As HttpWebResponse
        Dim reader As System.IO.StreamReader

        bytes = Encoding.UTF8.GetBytes(m_request)
        request = WebRequest.Create(m_server)

        request.Method = "POST"
        request.ContentLength = bytes.Length
        request.ContentType = "text/xml"
        requeststream = request.GetRequestStream()
        requeststream.Write(bytes, 0, bytes.Length)

        response = request.GetResponse()
        reader = New System.IO.StreamReader(response.GetResponseStream())

        m_response = reader.ReadToEnd()

        Return validate()

    End Function

    'AM2010040601 - GEOIP - Start
    Public Function SendRequest()
        Dim request As HttpWebRequest
        Dim response As HttpWebResponse
        Dim reader As System.IO.StreamReader

        request = WebRequest.Create(m_request)
        response = request.GetResponse()
        reader = New System.IO.StreamReader(response.GetResponseStream())

        m_response = reader.ReadToEnd()

        Return validate()
    End Function
    'AM2010040601 - GEOIP - End

    Public Function Resend(ByVal str As String)

        m_request = str
        Return Resend()

    End Function

    Public Function Response()

        Return m_response

    End Function

    Protected Overridable Function validate()

        Return True

    End Function

End Class
