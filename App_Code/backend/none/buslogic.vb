Imports System.Configuration.ConfigurationManager
Imports Microsoft.VisualBasic
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

Namespace ExpandIT
    ' TYPE NONE

    Public Class BuslogicNoneClass
        Inherits BuslogicBaseClass

        Public Sub New(ByVal _globals As GlobalsClass)
            MyBase.New(_globals)
        End Sub

        Public Overrides Function CalcOrder(ByVal OrderDict As ExpDictionary, ByVal Context As ExpDictionary) As String
            Dim orderLine As ExpDictionary
            Dim sumSubTotal As Decimal = 0
            Dim IsCalculated As Boolean
            Dim ProductList As String
            Dim sql As String
            Dim RecalculateLines As Boolean = False
            Dim retv As String = ""

            IsCalculated = CBoolEx(OrderDict("IsCalculated"))
            If OrderDict("Lines").Count > 0 Then
                If Not IsCalculated Then

                    Context("listprice_include_tax") = AppSettings("LISTPRICE_INCLUDE_TAX")

                    ProductList = GetSQLInString(OrderDict("Lines"), "ProductGuid")

                    If ProductList <> "" Then
                        sql = "SELECT * FROM ProductTable WHERE ProductGuid IN (" & ProductList & ") ORDER BY ProductGuid"
                        Context("DictProducts") = Sql2Dictionaries(sql, New String() {"ProductGuid"})
                    Else
                        Context("DictProducts") = New ExpDictionary()
                    End If

                    If CStrEx(Context("CurrencyGuid")) = "" Then Context("CurrencyGuid") = Context("DefaultCurrency")

                    If CStrEx(OrderDict("CurrencyGuid")) <> CStrEx(Context("CurrencyGuid")) Then
                        RecalculateLines = True
                    End If

                    OrderDict("CurrencyGuid") = Context("CurrencyGuid")
                    OrderDict("TaxPct") = CDblEx(AppSettings("TAX_PCT"))

                    For Each orderLine In OrderDict("Lines").values
                        CalcLine(OrderDict, orderLine, Context, RecalculateLines)
                        sumSubTotal = sumSubTotal + orderLine("LineTotal")
                    Next
                    OrderDict("ServiceCharge") = 0
                    OrderDict("ServiceChargeInclTax") = 0

                    OrderDict("InvoiceDiscount") = 0
                    OrderDict("InvoiceDiscount") = OrderDict("InvoiceDiscount") * (1 + OrderDict("TaxPct") / 100)

                    OrderDict("SubTotal") = sumSubTotal
                    OrderDict("SubTotalInclTax") = sumSubTotal * (1 + OrderDict("TaxPct") / 100)

                    OrderDict("TaxAmount") = sumSubTotal * OrderDict("TaxPct") / 100

                    OrderDict("Total") = sumSubTotal
                    OrderDict("TotalInclTax") = sumSubTotal + OrderDict("TaxAmount")

                    OrderDict("IsCalculated") = True
                    OrderDict("ReCalculated") = True

                End If
            End If
            Return retv
        End Function

        Protected Function CalcLine(ByVal OrderDict As Object, ByVal orderLine As Object, ByVal Context As Object, _
                                                                                    ByVal RecalculateLine As Object) As String
            Dim DictProducts, DictProduct As Object
            Dim ErrString As String = ""

            DictProducts = Context("DictProducts")

            If DictProducts(SafeString(orderLine("ProductGuid"))) IsNot Nothing Then
                If (Not CBoolEx(orderLine("IsCalculated"))) Or RecalculateLine Then
                    DictProduct = DictProducts(SafeString(orderLine("ProductGuid")))
                    orderLine("TaxPct") = OrderDict("TaxPct")

                    orderLine("CurrencyGuid") = OrderDict("CurrencyGuid")
                    orderLine("ListPrice") = currency.ConvertCurrency(DictProduct("ListPrice"), Context("DefaultCurrency"), Context("CurrencyGuid"))
                    ' Remove the tax from the listprice if it includes it
                    Dim taxAmount As Decimal = 0
                    If Context("listprice_include_tax") Then
                        taxAmount = orderLine("ListPrice") * (1 - 1 / (1 + orderLine("TaxPct") / 100))

                        orderLine("ListPriceInclTax") = orderLine("ListPrice")
                        orderLine("ListPrice") = orderLine("ListPrice") - taxAmount
                    Else
                        taxAmount = orderLine("ListPrice") * (orderLine("TaxPct") / 100)
                        orderLine("ListPriceInclTax") = orderLine("ListPrice") + taxAmount
                    End If
                    orderLine("LineTotal") = orderLine("ListPrice") * orderLine("Quantity")
                    orderLine("TotalInclTax") = orderLine("LineTotal") * (1 + orderLine("TaxPct") / 100)

                    orderLine("TaxAmount") = orderLine("LineTotal") * (orderLine("TaxPct") / 100)

                    ' No discounts calculated in ExpandIT Basic Business Logic. Modify to calculate discounts.
                    orderLine("LineDiscountAmount") = 0
                    orderLine("LineDiscountAmountInclTax") = orderLine("LineDiscountAmount") * (1 + orderLine("TaxPct") / 100)

                    orderLine("LineDiscount") = 0

                    orderLine("IsCalculated") = True
                End If
            Else
                ' If item doesn't exist anymore remove from orderline.
                Context("Errors").Add(orderLine("LineGuid"), Replace(Resources.Language.MESSAGE_PRODUCT_DOES_NOT_EXISTS, "%1", orderLine("ProductGuid")))

                OrderDict("Lines").Remove(orderLine("LineGuid").ToString)
                orderLine("LineTotal") = 0
            End If
            CalcLine = ErrString
        End Function

    End Class

End Namespace
