Imports System.Configuration.ConfigurationManager
Imports Microsoft.VisualBasic
Imports ExpandIT.EISClass

Namespace ExpandIT

    Public Class B2BNoneClass
        Inherits B2BBaseClass

        Public Shadows Const B2BEnabled As Boolean = False

        Public Sub New(ByVal _globals As GlobalsClass)
            MyBase.New(_globals)
        End Sub

    End Class

End Namespace
