Imports System.Configuration.ConfigurationManager
Imports Microsoft.VisualBasic
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Data.SqlClient
Imports System.Data
Imports System.Collections.Generic

Namespace ExpandIT

    Public Class BuslogicNFClass
    Inherits BuslogicBaseClass

        Public Sub New(ByVal _globals As GlobalsClass)
            MyBase.New(_globals)
        End Sub

        Public Overrides Function CalcOrder(ByVal OrderDict As ExpDictionary, ByVal Context As ExpDictionary) As String

            Dim sql As String
            Dim IsCalculated As Boolean

            On Error Resume Next
            Err.Clear()
            If OrderDict("Lines").Count > 0 Then
                ' no need to recalculate the order dictionary.
                IsCalculated = CBoolEx(OrderDict("IsCalculated"))

                If IsCalculated Then
                    Return Nothing
                End If

                Context("ProductList") = GetSQLInString(OrderDict("Lines"), "ProductGuid")
                Context("ProductQuantities") = GetProductQuantities(OrderDict)

                sql = "SELECT LineDiscountCalculation FROM NF_SalesReceivablesSetup"

                Dim reader As SqlDataReader = GetDR(sql) 'rsSRSetup = Conn.execute(sql)
                If reader.Read() Then 'If Not rsSRSetup.EOF Then
                    Context("MultiplyDiscounts") = (reader("LineDiscountCalculation") <> 0)
                Else
                    Context("MultiplyDiscounts") = False
                End If

                NFInitOrder(OrderDict, Context)

                ' *********************
                ' Line Calculations
                ' *********************
                If Err.Number = 0 Then NFStandardPrice(OrderDict, Context)
                If Err.Number = 0 Then NFListPrice(OrderDict, Context)
                If Err.Number = 0 Then NFItemPriceFixCurrency(OrderDict, Context)
                If Err.Number = 0 Then NFQuantityDiscount(OrderDict, Context)
                If Err.Number = 0 Then NFCustomerItemDiscount(OrderDict, Context)
                If Err.Number = 0 Then NFOrderLineTotal(OrderDict, Context)

                ' Mark lines as calculated
                If Err.Number = 0 Then
                    For Each kvPair As KeyValuePair(Of Object, Object) In CType(OrderDict("Lines"), ExpDictionary)
                        OrderDict("Lines")(kvPair.Key)("IsCalculated") = True
                    Next
                End If

                ' *********************
                ' Header Calculations
                ' *********************
                If Err.Number = 0 Then NFSubTotal(OrderDict, Context)
                If Err.Number = 0 Then NFInvoiceDiscounts(OrderDict, Context)
                If Err.Number = 0 Then NFCalculateVAT(OrderDict, Context)

                ' Mark order as calculated
                If Err.Number = 0 Then
                    OrderDict("IsCalculated") = True
                    OrderDict("ReCalculated") = True
                End If

                If Err.Number <> 0 Then
                    Context("Errors").Add(Err.Source, Err.Description)
                    OrderDict("Total") = 0
                    OrderDict("TotalInclTax") = 0
                    OrderDict("InvoiceDiscount") = 0
                    OrderDict("ServiceCharge") = 0
                    OrderDict("IsCalculated") = False
                    OrderDict("ReCalculated") = False
                End If

                reader.Close()
            End If
            Return ""
        End Function

        ' --
        ' Get correct product quantities for line discount calculations.
        ' This is used to calculate the correct line discounts and prices
        ' accross lines with the same ProductGuid, with different Variants.
        ' Note the NF_COLLECT_QUANTITES_FOR_LINE_DISCOUNTS Configuration which
        ' Will disable this feature.
        ' --
        Private Function GetProductQuantities(ByVal OrderDict As ExpDictionary) As ExpDictionary
            Dim retval As New ExpDictionary()
            Dim item As Object

            For Each kvPair As KeyValuePair(Of Object, Object) In OrderDict("Lines")
                item = OrderDict("Lines")(kvPair.Key)

                If retval(item("ProductGuid")) Is Nothing Then
                    retval(item("ProductGuid")) = New ExpDictionary()
                    retval(item("ProductGuid"))("Variants") = New ExpDictionary()
                End If

                retval(item("ProductGuid"))("Total") = CDblEx(retval(item("ProductGuid"))("Total")) + item("Quantity")

                If CStrEx(item("VariantCode")) <> "" Then
                    retval(item("ProductGuid"))("Variants")(item("VariantCode")) = CDblEx(retval(item("ProductGuid"))("Variants")(item("VariantCode"))) + item("Quantity")
                End If
            Next

            Return retval
        End Function


        ' --
        '      HEADER FUNCTIONS
        ' --
        ' --
        '      1 (Header)
        ' --
        Private Sub NFInitOrder(ByVal orderobject As ExpDictionary, ByVal Context As ExpDictionary)
            Dim BillToCustomerGuid As String
            Dim sql As String = "SELECT CustomerItemDiscountGroup, AllowQuantityDiscount, PriceGroupCode, InvoiceDiscountCode, VATBusinessPostingGroup, BillToCustomerGuid " & _
                  "FROM CustomerTable WHERE CustomerGuid=" & SafeString(Context("CustomerGuid"))

            Dim reader As SqlDataReader = GetDR(sql) 'rsCustomer = Conn.execute(sql)
            If reader.Read() Then 'If Not rsCustomer.EOF Then
                If Not IsNull(reader("BillToCustomerGuid")) And CStrEx(reader("BillToCustomerGuid")) <> "" Then
                    BillToCustomerGuid = reader("BillToCustomerGuid").Value
                    sql = "SELECT CustomerItemDiscountGroup, AllowQuantityDiscount, PriceGroupCode , InvoiceDiscountCode, VATBusinessPostingGroup, BillToCustomerGuid " & _
                          "FROM CustomerTable WHERE CustomerGuid=" & SafeString(reader("BillToCustomerGuid"))
                    reader.Close()
                    reader = GetDR(sql)
                    If Not reader.Read() Then 'If rsCustomer.EOF Then
                        Err.Raise(-1, "ExpandIT NF Business Logic", "Bill-To customer [" & BillToCustomerGuid & "] for Customer [" & Context("CustomerGuid") & "] is not available")
                    End If
                End If
                orderobject("CustomerItemDiscountGroup") = reader("CustomerItemDiscountGroup")
                orderobject("AllowQuantityDiscount") = reader("AllowQuantityDiscount")
                orderobject("PriceGroupCode") = reader("PriceGroupCode")
                orderobject("InvoiceDiscountCode") = reader("InvoiceDiscountCode")
                orderobject("VATBusinessPostingGroup") = reader("VATBusinessPostingGroup")
            Else
                Err.Raise(-1, "ExpandIT NF Business Logic", "Customer [" & Context("CustomerGuid").ToString() & "] is not available")
            End If
            orderobject("CurrencyGuid") = Context("CurrencyGuid")
            If IsNull(orderobject("CurrencyGuid")) Then orderobject("CurrencyGuid") = Context("DefaultCurrency")
            If orderobject("CurrencyGuid") = "" Then orderobject("CurrencyGuid") = Context("DefaultCurrency")

            reader.Close()
        End Sub

        ' --
        '      2 (Header)
        ' --
        Private Sub NFSubTotal(ByVal orderobject As ExpDictionary, ByVal Context As ExpDictionary)
            Dim AmountSum, InvDiscSum As Integer
            Dim orderline As Object

            For Each orderline In CType(orderobject("Lines"), ExpDictionary).DictItems
                AmountSum = AmountSum + orderline("LineTotal")
                If orderline("AllowInvoiceDiscount") Then InvDiscSum = InvDiscSum + orderline("LineTotal")
            Next
            orderobject("SubTotal") = AmountSum
            orderobject("SubTotal_InvoiceDiscount") = InvDiscSum
            orderobject("Total") = AmountSum

        End Sub

        ' --
        '      3 (Header)
        ' --
        Private Sub NFInvoiceDiscounts(ByVal orderobject As ExpDictionary, ByVal Context As ExpDictionary)
            Dim DiscountPct, ServiceCharge, tmpInvSubTotal, tmpSubTotal As Object
            Dim resDict As ExpDictionary
            Dim key, item As Object
            Dim SameCurr, DiscountFound, ServiceChargeFound As Boolean

            DiscountPct = 0
            ServiceCharge = 0
            ' Calculation of Service chage and invoice discount.
            ' Invoice discount should be calculated on SubTotal_InvoiceDiscount (Found in Function NFSubTotal)
            ' Service Charge should be calculated on SubTotal - not considering if invoice dicounts are allowed or not.

            ' Read the table
            Dim sql As String = "SELECT DiscountPct, CurrencyCode, ServiceCharge, MinimumAmount FROM NF_CustomerInvoiceDiscount WHERE" & _
                  " InvoiceDiscountCode=" & SafeString(orderobject("InvoiceDiscountCode")) & " AND" & _
                  " ((CurrencyCode='') Or (CurrencyCode IS NULL) OR (CurrencyCode = " & SafeString(orderobject("CurrencyGuid")) & "))" & _
                  " ORDER BY CurrencyCode DESC, MinimumAmount DESC"

            resDict = Sql2Dictionaries2(sql, New Object() {"CurrencyCode", "MinimumAmount"})

            DiscountPct = 0
            ServiceCharge = 0

            ' Try with same currency
            If resDict(SafeString(orderobject("CurrencyGuid"))) IsNot Nothing Then
                SameCurr = True
                For Each key In CType(resDict(SafeString(orderobject("CurrencyGuid"))), ExpDictionary).DictKeys
                    item = resDict(SafeString(orderobject("CurrencyGuid")))(key)

                    If (Not DiscountFound) And (orderobject("SubTotal_InvoiceDiscount") >= item("MinimumAmount")) Then
                        DiscountFound = True
                        DiscountPct = item("DiscountPct")
                    End If

                    If (Not ServiceChargeFound) And (orderobject("SubTotal") >= item("MinimumAmount")) Then
                        ServiceChargeFound = True
                        ServiceCharge = item("ServiceCharge")
                    End If

                    If ServiceChargeFound And DiscountFound Then Exit For
                Next
            End If

            ' Try with no currency.
            If (Not SameCurr) And (resDict(SafeString(""))) IsNot Nothing Then
                For Each key In CType(resDict(SafeString("")), ExpDictionary).DictKeys
                    item = resDict(SafeString(""))(key)
                    tmpInvSubTotal = globals.currency.ConvertCurrency(orderobject("SubTotal_InvoiceDiscount"), orderobject("CurrencyGuid"), "")
                    tmpSubTotal = globals.currency.ConvertCurrency(orderobject("SubTotal"), orderobject("CurrencyGuid"), "")

                    If (Not DiscountFound) And (tmpInvSubTotal >= item("MinimumAmount")) Then
                        DiscountFound = True
                        DiscountPct = item("DiscountPct")
                    End If

                    If (Not ServiceChargeFound) And (tmpSubTotal >= item("MinimumAmount")) Then
                        ServiceChargeFound = True
                        ServiceCharge = globals.currency.ConvertCurrency(item("ServiceCharge"), "", orderobject("CurrencyGuid"))
                    End If

                    If ServiceChargeFound And DiscountFound Then Exit For
                Next
            End If

            orderobject("InvoiceDiscountPct") = RoundEx(DiscountPct, 2)
            orderobject("ServiceCharge") = RoundEx(ServiceCharge, 2)
            orderobject("InvoiceDiscount") = orderobject("SubTotal_InvoiceDiscount") * orderobject("InvoiceDiscountPct") / 100
            orderobject("Total") = orderobject("SubTotal") + orderobject("ServiceCharge") - orderobject("InvoiceDiscount")
        End Sub

        ' --
        '      4 (Header)
        ' --
        Private Sub NFCalculateVAT(ByVal orderobject As ExpDictionary, ByVal Context As ExpDictionary)
            Dim orderline, VATPct As Object
            Dim resDict As ExpDictionary
            Dim VATSum, InvoiceDiscountAmountInclTaxSum, TotalInclTaxSum, TotalSum, InvoiceDiscountAmountSum As Object

            Dim sql As String = "SELECT ProductGuid, VATPct FROM ProductTable LEFT JOIN NF_VATPostingSetup ON " & _
                  "ProductTable.VATProductPostingGroup = NF_VATPostingSetup.ProductPostingGroup " & _
                  "WHERE (BusinessPostingGroup=" & SafeString(orderobject("VATBusinessPostingGroup")) & ") AND " & _
                  "ProductGuid IN (" & Context("ProductList") & ")"

            resDict = Sql2Dictionaries(sql, "ProductGuid")

            VATSum = 0
            InvoiceDiscountAmountInclTaxSum = 0
            InvoiceDiscountAmountSum = 0
            TotalInclTaxSum = 0
            TotalSum = 0

            For Each orderline In CType(orderobject("Lines"), ExpDictionary).DictItems
                VATPct = 0

                If resDict(SafeString(orderline("ProductGuid"))) IsNot Nothing Then
                    VATPct = resDict(SafeString(orderline("ProductGuid")))("VATPct")
                End If

                orderline("TaxPct") = VATPct

                If orderline("ListPriceIsInclTax") Then
                    ' redraw the taxAmount from ListPrice, LineTotal, LineDiscountAmount and SubTotal

                    orderline("ListPrice") = orderline("ListPrice") * (1 / (1 + VATPct / 100))
                    orderline("SubTotal") = orderline("SubTotal") * (1 / (1 + VATPct / 100))
                    orderline("LineTotal") = orderline("LineTotal") * (1 / (1 + VATPct / 100))
                    orderline("LineDiscountAmount") = orderline("LineDiscountAmount") * (1 / (1 + VATPct / 100))
                End If

                orderline("ListPriceInclTax") = orderline("ListPrice") * (1 + VATPct / 100)
                orderline("SubTotalInclTax") = orderline("SubTotal") * (1 + VATPct / 100)
                orderline("TotalInclTax") = orderline("LineTotal") * (1 + VATPct / 100)
                orderline("LineDiscountAmountInclTax") = orderline("LineDiscountAmount") * (1 + VATPct / 100)

                ' Calculate the InvoiceDiscount part of this line.
                If orderline("AllowInvoiceDiscount") Then
                    orderline("InvoiceDiscountAmount") = orderline("LineTotal") * (orderobject("InvoiceDiscountPct") / 100)
                Else
                    orderline("InvoiceDiscountAmount") = 0
                End If

                orderline("TaxAmount") = orderline("LineTotal") * VATPct / 100

                orderline("InvoiceDiscountAmountInclTax") = orderline("InvoiceDiscountAmount") * (1 + VATPct / 100)

                ' Accummulate sums for header
                VATSum = VATSum + orderline("TaxAmount") - orderline("InvoiceDiscountAmount") * (VATPct / 100)

                InvoiceDiscountAmountSum = InvoiceDiscountAmountSum + orderline("InvoiceDiscountAmount")
                InvoiceDiscountAmountInclTaxSum = InvoiceDiscountAmountInclTaxSum + orderline("InvoiceDiscountAmountInclTax")

                TotalSum = TotalSum + orderline("LineTotal")
                TotalInclTaxSum = TotalInclTaxSum + orderline("TotalInclTax")
            Next

            ' --
            ' Service Charge calculations. Lookup the tax pct for service charge according to cfg.
            ' --
            sql = "SELECT VATPct FROM NF_VATPostingSetup WHERE (BusinessPostingGroup=" & _
                  SafeString(orderobject("VATBusinessPostingGroup")) & ") AND " & _
                  "(ProductPostingGroup=" & SafeString(AppSettings("NF_SERVICECHARGE_PRODUCTPOSTINGGROUP")) & ")"

            Dim reader As SqlDataReader = GetDR(sql) 'rsVATSetup = Conn.execute(sql)

            VATPct = 0
            If reader.Read Then 'If Not rsVATSetup.EOF Then
                VATPct = reader("VATPct").Value
            End If

            ' Set the VATPct in the orderobject for the Shipping and Handling calculations.
            orderobject("TaxPct") = VATPct

            ' Calculate the VAT on the Service Charge, if any
            If orderobject("ServiceCharge") > 0 Then
                VATSum = VATSum + orderobject("ServiceCharge") * VATPct / 100
            End If

            ' Tax fields
            orderobject("TaxAmount") = VATSum

            orderobject("ServiceChargeInclTax") = orderobject("ServiceCharge") * (1 + VATPct / 100)

            orderobject("InvoiceDiscount") = InvoiceDiscountAmountSum
            orderobject("InvoiceDiscountInclTax") = InvoiceDiscountAmountInclTaxSum
            orderobject("SubTotalInclTax") = TotalInclTaxSum

            orderobject("Total") = TotalSum + orderobject("ServiceCharge") - orderobject("InvoiceDiscount")
            orderobject("TotalInclTax") = TotalInclTaxSum + orderobject("ServiceChargeInclTax") - orderobject("InvoiceDiscountInclTax")

            reader.Close()
        End Sub

        ' --
        '      LINE FUNCTIONS
        ' --
        ' --
        '      1 (Line)
        ' --
        Private Sub NFStandardPrice(ByVal orderobject As ExpDictionary, ByVal Context As ExpDictionary)
            Dim item, key As Object
            Dim orderline, resultDict As ExpDictionary

            Dim sql As String = "SELECT ProductGuid, ListPrice, AllowInvoiceDiscount, VATProductPostingGroup, PriceIncludesVAT FROM ProductTable WHERE ProductGuid IN (" & Context("ProductList") & ")"

            resultDict = Sql2Dictionaries(sql, "ProductGuid")

            For Each key In CType(orderobject("Lines"), ExpDictionary).DictKeys
                orderline = orderobject("Lines")(key)
                If resultDict(SafeString(orderline("ProductGuid"))) IsNot Nothing Then
                    item = resultDict(SafeString(orderline("ProductGuid")))

                    orderline("ListPrice") = item("ListPrice")
                    orderline("CurrencyGuid") = Context("DefaultCurrencyGuid")
                    orderline("AllowInvoiceDiscount") = item("AllowInvoiceDiscount")
                    orderline("AllowQuantityDiscount") = True
                    orderline("AllowCustomerItemDiscount") = True
                    orderline("VATProductPostingGroup") = item("VATProductPostingGroup")
                    orderline("ListPriceIsInclTax") = item("PriceIncludesVAT")
                Else
                    Context("Errors").Add("ExpandIT NF Business Logic, NFStandardPrice", Resources.Language.LABEL_PRODUCT & " [" & orderline("ProductGuid") & "] " & Resources.Language.MESSAGE_IS_NOT_AVAILABLE)
                    orderobject("Lines").Remove(key)
                End If
            Next
        End Sub

        ' --
        '      2 (Line)
        ' --
        Private Sub NFListPrice(ByVal orderobject As ExpDictionary, ByVal Context As ExpDictionary)
            Dim item, key As Object
            Dim orderline, resultDict As ExpDictionary

            Dim sql As String = "SELECT ItemNo as ProductGuid, UnitPrice, CurrencyCode, AllowInvoiceDiscount, AllowQuantityDiscount, AllowCustomerItemDiscount, " & _
                  "PriceInclTax FROM NF_ItemPrice " & _
                  "WHERE ItemNo IN (" & Context("ProductList") & ") AND " & _
                  "((PriceGroupCode = " & SafeString(orderobject("PriceGroupCode")) & ") OR (PriceGroupCode = '') OR (PriceGroupCode IS NULL)) AND " & _
                  "((CurrencyCode = " & SafeString(orderobject("CurrencyGuid")) & ") OR (CurrencyCode = '') OR (CurrencyCode IS NULL)) AND " & _
                  "((StartingDate <= " & SafeDatetime(CLng(CDblEx(Now) - 0.5)) & ") OR (StartingDate IS NULL)) " & _
                  "ORDER BY PriceGroupCode, CurrencyCode, StartingDate"

            resultDict = Sql2Dictionaries(sql, "ProductGuid")

            For Each key In CType(orderobject("Lines"), ExpDictionary).DictKeys
                orderline = orderobject("Lines")(key)
                If resultDict(SafeString(orderline("ProductGuid"))) IsNot Nothing Then
                    item = resultDict(SafeString(orderline("ProductGuid")))

                    orderline("ListPrice") = item("UnitPrice")
                    orderline("ListPriceIsInclTax") = item("PriceInclTax")
                    orderline("CurrencyGuid") = item("CurrencyCode")
                    orderline("AllowInvoiceDiscount") = item("AllowInvoiceDiscount")
                    orderline("AllowQuantityDiscount") = item("AllowQuantityDiscount")
                    orderline("AllowCustomerItemDiscount") = item("AllowCustomerItemDiscount")
                    If IsNull(orderline("CurrencyGuid")) Then orderline("CurrencyGuid") = Context("DefaultCurrency")
                    If orderline("CurrencyGuid") = "" Then orderline("CurrencyGuid") = Context("DefaultCurrency")
                End If
            Next
        End Sub

        ' --
        '      3 (Line)
        ' --
        Private Sub NFItemPriceFixCurrency(ByVal orderobject As ExpDictionary, ByVal Context As ExpDictionary)
            Dim key, item As Object
            Dim orderline As ExpDictionary

            For Each key In CType(orderobject("Lines"), ExpDictionary).DictKeys
                item = orderobject("Lines")(key)
                orderline = orderobject("Lines")(key)

                If orderline("CurrencyGuid") <> orderobject("CurrencyGuid") Then
                    orderline("ListPrice") = globals.currency.ConvertCurrency(orderline("ListPrice"), orderline("CurrencyGuid"), orderobject("CurrencyGuid"))
                    orderline("CurrencyGuid") = orderobject("CurrencyGuid")
                End If
            Next
        End Sub

        ' --
        '      4 (Line)
        ' --
        Private Sub NFQuantityDiscount(ByVal orderobject As ExpDictionary, ByVal Context As ExpDictionary)
            Dim ProductTotalQuantity, DiscPct, key, key2, item As Object
            Dim DictProductQuantities, orderline, resultDict As ExpDictionary
            Dim DiscFound As Boolean

            DictProductQuantities = Context("ProductQuantities")

            Dim sql As String = "SELECT ProductGuid, DiscountPct, MinimumQuantity, ProductTable.QtyDiscountGroup FROM NF_Item_Qty_Disc " & _
                  "LEFT JOIN ProductTable ON NF_Item_Qty_Disc.QtyDiscountGroup = ProductTable.QtyDiscountGroup " & _
                  "WHERE ProductTable.ProductGuid IN (" & Context("ProductList") & ") " & _
                  "ORDER BY ProductGuid, MinimumQuantity DESC"

            resultDict = Sql2Dictionaries2(sql, New Object() {"ProductGuid", "MinimumQuantity"})

            For Each key In CType(orderobject("Lines"), ExpDictionary).DictKeys
                orderline = orderobject("Lines")(key)

                DiscPct = 0
                DiscFound = False

                If AppSettings("NF_COLLECT_QUANTITES_FOR_LINE_DISCOUNTS") Then
                    ProductTotalQuantity = CDblEx(DictProductQuantities(orderline("ProductGuid"))("Total"))
                Else
                    ProductTotalQuantity = orderline("Quantity")
                End If

                If resultDict(SafeString(orderline("ProductGuid"))) IsNot Nothing Then
                    For Each key2 In CType(resultDict(SafeString(orderline("ProductGuid"))), ExpDictionary).DictKeys
                        item = resultDict(SafeString(orderline("ProductGuid")))(key2)

                        If ProductTotalQuantity >= item("MinimumQuantity") And Not DiscFound Then
                            DiscPct = item("DiscountPct")
                            DiscFound = True
                        End If
                    Next
                End If
                orderline("QuantityDiscountPct") = DiscPct
            Next
        End Sub

        ' --
        '      5 (Line)
        ' --
        Private Sub NFCustomerItemDiscount(ByVal orderobject As ExpDictionary, ByVal Context As ExpDictionary)
            Dim key, item As Object
            Dim orderline, resultDict As ExpDictionary

            Dim sql As String = "SELECT ProductTable.ProductGuid, ProductTable.ItemCustomerDiscountGroup, NF_CustomerItemDiscount.CustomerDiscountGroup, DiscountPct " & _
                  "FROM ProductTable LEFT JOIN NF_CustomerItemDiscount ON ProductTable.ItemCustomerDiscountGroup = NF_CustomerItemDiscount.ItemDiscountGroup " & _
                  "WHERE (CustomerDiscountGroup=" & SafeString(orderobject("CustomerItemDiscountGroup")) & ") AND " & _
                  "ProductTable.ProductGuid IN (" & Context("ProductList") & ")"

            resultDict = Sql2Dictionaries(sql, "ProductGuid")

            For Each key In CType(orderobject("Lines"), ExpDictionary).DictKeys
                orderline = orderobject("Lines")(key)
                If resultDict(SafeString(orderline("ProductGuid"))) IsNot Nothing Then
                    item = resultDict(SafeString(orderline("ProductGuid")))
                    orderline("CustomerItemDiscountPct") = item("DiscountPct")
                Else
                    orderline("CustomerItemDiscountPct") = 0
                End If
            Next
        End Sub

        ' --
        '      6 (Line)
        ' --
        Private Sub NFOrderLineTotal(ByVal orderobject As ExpDictionary, ByVal Context As ExpDictionary)
            Dim orderline As ExpDictionary, key As Object

            For Each key In CType(orderobject("Lines"), ExpDictionary).DictKeys
                orderline = orderobject("Lines")(key)

                If Not (orderobject("AllowQuantityDiscount") And orderline("AllowQuantityDiscount")) Then
                    orderline("QuantityDiscountPct") = 0
                End If
                If Not orderline("AllowCustomerItemDiscount") Then
                    orderline("CustomerItemDiscountPct") = 0
                End If

                If Context("MultiplyDiscounts") Then
                    orderline("LineDiscount") = 100 - 100 * ((1 - orderline("QuantityDiscountPct") / 100) * (1 - orderline("CustomerItemDiscountPct") / 100))
                Else
                    orderline("LineDiscount") = orderline("QuantityDiscountPct") + orderline("CustomerItemDiscountPct")
                End If
                If orderline("LineDiscount") > 100 Then orderline("LineDiscount") = 100

                orderline("SubTotal") = orderline("Quantity") * orderline("ListPrice")
                orderline("LineDiscountAmount") = orderline("SubTotal") * orderline("LineDiscount") / 100
                orderline("LineTotal") = orderline("SubTotal") - orderline("LineDiscountAmount")
            Next
        End Sub

    End Class
End Namespace

