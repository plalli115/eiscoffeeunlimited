Imports System.Configuration.ConfigurationManager
Imports Microsoft.VisualBasic
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

Namespace ExpandIT

    Public Class B2BNFClass
        Inherits B2BBaseClass

        Public Sub New(ByVal _globals As GlobalsClass)
            MyBase.New(_globals)
        End Sub

        ' Customer ledger entries:
        ' NF: Table 21. + additional information from the customer table 21.
        ' To use this file the corresponding eic file must be importet into the
        ' BE configuration using Configuration Tool. The fields can also be set up manually.

        ' ---------------------------------------------------------------
        ' Load the customer ledger entries for a selected customer.
        REM ---------------------------------------------------------------
        Public Overrides Function GetCustomerLedgerEntries(ByVal User As ExpDictionary) As ExpDictionary
            Dim sql As String
            Dim retval As ExpDictionary

            ' Select the related customer, the customer table contains some calculated fields of interest
            sql = "SELECT * FROM CustomerTable WHERE CustomerGuid = '" & User("CustomerGuid") & "'"
            retval = Sql2Dictionary(sql)

            ' Select the ledger entries for the customer.
            sql = "SELECT * FROM CustomerLedgerEntry WHERE CustomerGuid = '" & User("CustomerGuid") & "' ORDER BY PostingDate,EntryGuid"
            On Error Resume Next
            retval("Lines") = SQL2Dicts(sql, "EntryGuid")
            If Err.Number <> 0 Then
                HttpContext.Current.Response.Write("<b>An error occured.</b><br />Please ensure that you have extracted the Customer Ledger Entry " & _
                    "table.<br /><br />" & _
                    "<b>The following error was returned:</b> <br />" & vbCrLf & Err.Description & "(" & Err.Number & ")")
                HttpContext.Current.Response.End()
            End If
            On Error GoTo 0
            GetCustomerLedgerEntries = retval
        End Function

        ' ---------------------------------------------------------------
        ' Returns the document type description for a document type id.
        ' Blank is returned if the type id wasn't found.
        ' ---------------------------------------------------------------
        Public Overrides Function GetDocumentTypeDescription(ByVal lngType As Integer) As String
            Dim retv As String = ""
            Dim arrTypes As String() = New String() {"", "Payment", "Invoice", "Credit Memo", "Finance Charge Memo", "Reminder", "Refund"}

            ' Set the return value if indexes aren't out of bounds.
            If LBound(arrTypes) <= lngType And UBound(arrTypes) >= lngType Then
                retv = arrTypes(lngType)
            End If

            Return retv
        End Function

    End Class

End Namespace
