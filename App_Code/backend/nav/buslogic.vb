Imports System.Configuration.ConfigurationManager
Imports Microsoft.VisualBasic
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.Data

Namespace ExpandIT

    Public Class VatNode
        Private _vatSum As Decimal
        Private _invoiceDiscountAmountSumExclVAT As Decimal
        Private _invoiceDiscountAmountSumInclVAT As Decimal


        Public Property VatSum() As Decimal
            Get
                Return _vatSum
            End Get
            Set(ByVal value As Decimal)
                _vatSum = value
            End Set
        End Property

        Public Property InvoiceDiscountAmountSumExclVAT() As Decimal
            Get
                Return _invoiceDiscountAmountSumExclVAT
            End Get
            Set(ByVal value As Decimal)
                _invoiceDiscountAmountSumExclVAT = value
            End Set
        End Property

        Public Property InvoiceDiscountAmountSumInclVAT() As Decimal
            Get
                Return _invoiceDiscountAmountSumInclVAT
            End Get
            Set(ByVal value As Decimal)
                _invoiceDiscountAmountSumInclVAT = value
            End Set
        End Property
    End Class
    ' TYPE NONE

    ''' <summary>
    ''' Business logic
    ''' This code depend on these tables. Please make sure that these tables are extracted.
    ''' The c:\program files\ExpandIT\Internet Shop\Attain_CP.eic file contains the nesscacary extract information.
    ''' 
    ''' Tableno  Table name in web database.
    '''    19: Attain_CustomerInvoiceDiscount
    '''  7002: Attain_ProductPrice
    '''  7004: Attain_SalesLineDiscount
    '''   325: Attain_VATPostingSetup
    ''' </summary>
    ''' <remarks></remarks>
    Public Class BuslogicNAVClass
        Inherits BuslogicBaseClass
        '*****promotions*****-start
        Private promotions As promotions
        '*****promotions*****-end
        'AM0122201001 - UOM - Start
        Private USUOM As USUOM
        'AM0122201001 - UOM - End
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
        Private csrObj As USCSR
        Private isReturn As Boolean
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - End

        Public Sub New(ByVal _globals As GlobalsClass)
            MyBase.New(_globals)
            '*****promotions*****-start
            promotions = New promotions(_globals)
            '*****promotions*****-end
            'AM0122201001 - UOM - Start
            USUOM = New USUOM()
            'AM0122201001 - UOM - End
            'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
            csrObj = New USCSR(globals)
            'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
        End Sub


        ''' <summary>
        ''' This function is called by lib_order->CalculateOrder. It calculates all nessacary fields of the order according to
        ''' Navision attain (version 3.6 and higher) business logic rules.
        ''' </summary>
        ''' <param name="OrderDict">Orderobject</param>
        ''' <param name="Context">Context dictionary</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Overrides Function CalcOrder(ByVal OrderDict As ExpDictionary, ByVal Context As ExpDictionary) As String
            Dim IsCalculated As Boolean
            'AM2010051801 - SPECIAL ITEMS - Start
            Dim useSpecials As Boolean = False
            'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - Start
            'useSpecials = CBoolEx(getSingleValueDB("SELECT SpecialsEnabled FROM EESetup"))
            Try
                'useSpecials = CBoolEx(HttpContext.Current.Application("Config")(globals.User("CatalogNo"))("SpecialsEnabled"))
                Dim pageobj As ExpandIT.Page = CType(System.Web.HttpContext.Current.Handler, ExpandIT.Page)
                useSpecials = CBoolEx(globals.eis.getEnterpriseConfigurationValue("SpecialsEnabled"))
            Catch ex As Exception
            End Try
            'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - End
            'AM2010051801 - SPECIAL ITEMS - End

            ' No need to recalculate the order dictionary.
            IsCalculated = CBoolEx(OrderDict("IsCalculated"))

            If IsCalculated Then
                Return ""
            End If

            If OrderDict("Lines").Count = 0 Then
                Return ""
            End If

            Try
                Context("ProductList") = GetSQLInString(OrderDict("Lines"), "ProductGuid")
                Context("ProductQuantities") = GetProductQuantities(OrderDict)

                '
                ' Initialization
                '
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
                If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
                    isReturn = csrObj.isReturn(OrderDict)
                End If
                If Not CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) OrElse Not isReturn Then
                    NFInitOrder(OrderDict, Context)
                End If
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
                '
                ' Line(Calculations)
                '
                NFStandardPrice(OrderDict, Context)

                'AM2010051801 - SPECIAL ITEMS - Start
                If useSpecials Then
                    'AM0122201001 - UOM - Start
                    'If AppSettings("EXPANDIT_US_USE_UOM") Then
                    '    USUOM.NFSpecialPrice(OrderDict, Context)
                    'Else 'AM0122201001 - UOM - End
                    '    NFSpecialPrice(OrderDict, Context)
                    'End If

                    NFSpecialPrice(OrderDict, Context)

                End If
                'AM2010051801 - SPECIAL ITEMS - End


                NFListPrice(OrderDict, Context)

                NFItemPriceFixCurrency(OrderDict, Context)


                'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
                If Not CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) OrElse Not isReturn Then
                    '*****promotions*****-START
                    If AppSettings("EXPANDIT_US_USE_PROMOTION_CODES") Then
                        promotions.NFLineDiscount(OrderDict, Context)
                    Else
                        NFLineDiscount(OrderDict, Context)
                    End If
                    '*****promotions*****-END
                End If
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - End

                NFOrderLineTotal(OrderDict, Context)
                ' Mark lines as calculated
                For Each line As ExpDictionary In OrderDict("Lines").values
                    line("IsCalculated") = True
                Next

                '
                ' Header Calculations
                '
                NFSubTotal(OrderDict, Context)

                'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
                If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And isReturn Then
                    ' The value whithout tax            
                    OrderDict("Total") = RoundEx(CDblEx(OrderDict("Total")) + CDblEx(OrderDict("ShippingAmount")) + CDblEx(OrderDict("HandlingAmount")), CLngEx(AppSettings("NUMDECIMALPLACES")))
                    ' The total including tax
                    'OrderDict("TotalInclTax") = CDblEx(OrderDict("TotalInclTax")) + CDblEx(OrderDict("HandlingAmountInclTax")) + CDblEx(OrderDict("ShippingAmountInclTax"))
                    OrderDict("TotalInclTax") = RoundEx(CDblEx(OrderDict("TotalInclTax")) + CDblEx(OrderDict("ShippingAmount")) + CDblEx(OrderDict("HandlingAmount")), CLngEx(AppSettings("NUMDECIMALPLACES")))
                Else
                    '*****promotions*****-START
                    If AppSettings("EXPANDIT_US_USE_PROMOTION_CODES") Then
                        promotions.NFInvoiceDiscounts(OrderDict, Context)
                    Else
                        NFInvoiceDiscounts(OrderDict, Context)
                    End If
                    '*****promotions*****-END

                    '<!-- ' ExpandIT US - MPF - US Sales Tax - Start -->
                    If (AppSettings("EXPANDIT_US_USE_US_SALES_TAX")) Then
                        Dim USSalesTax As New USSalesTax()
                        If (OrderDict("TaxGroupCode") Is Nothing) Then OrderDict("TaxGroupCode") = ""
                        USSalesTax.NFCalculateTax(OrderDict, Context)
                    Else
                        NFCalculateVAT(OrderDict, Context)
                    End If
                    '<!-- ' ExpandIT US - MPF - US Sales Tax - Finish -->
                End If
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - End

                ' Mark order as calculated
                OrderDict("IsCalculated") = True
                OrderDict("ReCalculated") = True

                'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
                If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
                    csrObj.applyAdjustment(OrderDict)
                End If
                If Not CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) OrElse Not isReturn Then
                    '*****shipping*****-start
                    If AppSettings("EXPANDIT_US_USE_SHIPPING") Then
                        OrderDict("ShippingAmount") = 0
                        OrderDict("ShippingAmountInclTax") = 0
                    End If
                End If
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - End

            Catch ex As Exception
                Context("Errors").Add(Guid.NewGuid, ex.StackTrace)
                OrderDict("Total") = 0
                If AppSettings("EXPANDIT_US_USE_SHIPPING") Then
                    OrderDict("ShippingAmount") = 0
                    OrderDict("ShippingAmountInclTax") = 0
                End If
                '*****shipping*****-end
                OrderDict("TotalInclTax") = 0
                OrderDict("InvoiceDiscount") = 0
                OrderDict("ServiceCharge") = 0
                OrderDict("IsCalculated") = False
                OrderDict("ReCalculated") = False
            End Try

            Return ""
        End Function

        ''' <summary>
        ''' Get correct product quantities for line discount calculations.
        ''' This is used to calculate the correct line discounts and prices
        ''' accross lines with the same ProductGuid, with different Variants.
        ''' Not the NAV_COLLECT_QUANTITES_FOR_LINE_DISCOUNTS Configuration which
        ''' Will disable this feature.
        ''' </summary>
        ''' <param name="OrderDict"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Function GetProductQuantities(ByVal OrderDict As ExpDictionary) As ExpDictionary
            Dim retv As ExpDictionary
            Dim item As ExpDictionary

            retv = New ExpDictionary
            For Each item In OrderDict("Lines").values
                If Not retv.ContainsKey(item("ProductGuid")) Then
                    retv(item("ProductGuid")) = New ExpDictionary
                    retv(item("ProductGuid"))("Variants") = New ExpDictionary
                End If

                retv(item("ProductGuid"))("Total") = CDblEx(retv(item("ProductGuid"))("Total")) + item("Quantity")

                If CStrEx(item("VariantCode")) <> "" Then
                    retv(item("ProductGuid"))("Variants")(item("VariantCode")) = CDblEx(retv(item("ProductGuid"))("Variants")(item("VariantCode"))) + item("Quantity")
                End If
            Next

            Return retv
        End Function

        ' --
        '    HEADER FUNCTIONS
        ' --

        ''' <summary>
        ''' Initialize the order, reads information from Customer record.
        ''' </summary>
        ''' <param name="orderobject"></param>
        ''' <param name="Context"></param>
        ''' <remarks></remarks>
        Protected Sub NFInitOrder(ByVal orderobject As ExpDictionary, ByVal Context As ExpDictionary)
            Dim sql As String
            Dim dtCustomer As DataTable
            Dim BillToCustomerGuid As String

            sql = "SELECT CustomerGuid, CustomerDiscountGroup, AllowLineDisc, PriceGroupCode, InvoiceDiscountCode, VATBusinessPostingGroup, BillToCustomerGuid, CurrencyGuid, PricesIncludingVat " & _
                  "FROM CustomerTable WHERE CustomerGuid=" & SafeString(Context("CustomerGuid"))

            ' ExpandIT US - MPF - Promotions Code - Start
            If AppSettings("EXPANDIT_US_USE_PROMOTION_CODES") Then
                orderobject("ShippingDiscount") = 0
            End If
            ' ExpandIT US - MPF - Promotions Code - Finish

            dtCustomer = SQL2DataTable(sql)
            If dtCustomer.Rows.Count > 0 Then
                BillToCustomerGuid = CStrEx(dtCustomer.Rows(0)("BillToCustomerGuid"))
                If BillToCustomerGuid <> "" Then
                    REM -- ADDED "PricesIncludingVat" TO TEST
                    sql = "SELECT CustomerGuid, CustomerDiscountGroup, AllowLineDisc, PriceGroupCode, InvoiceDiscountCode, VATBusinessPostingGroup, BillToCustomerGuid, CurrencyGuid, PricesIncludingVat " & _
                          "FROM CustomerTable WHERE CustomerGuid=" & SafeString(BillToCustomerGuid)

                    dtCustomer = SQL2DataTable(sql)
                    If dtCustomer.Rows.Count = 0 Then
                        Throw New Exception("ExpandIT Attain Business Logic: Bill-To customer [" & BillToCustomerGuid & "] for Customer [" & Context("CustomerGuid") & "] is not available")
                    End If
                End If
                orderobject("CustomerGuid") = CStrEx(dtCustomer.Rows(0)("CustomerGuid"))
                orderobject("CustomerDiscountGroup") = CStrEx(dtCustomer.Rows(0)("CustomerDiscountGroup"))
                orderobject("AllowLineDisc") = CBoolEx(dtCustomer.Rows(0)("AllowLineDisc"))
                orderobject("PriceGroupCode") = CStrEx(dtCustomer.Rows(0)("PriceGroupCode"))
                orderobject("InvoiceDiscountCode") = CStrEx(dtCustomer.Rows(0)("InvoiceDiscountCode"))
                orderobject("VATBusinessPostingGroup") = CStrEx(dtCustomer.Rows(0)("VATBusinessPostingGroup"))
                REM -- ADDED "PricesIncludingVat" TO TEST
                orderobject("PricesIncludingVat") = CBoolEx(dtCustomer.Rows(0)("PricesIncludingVat"))
            Else
                Throw New Exception("ExpandIT Attain Business Logic: Customer [" & Context("CustomerGuid") & "] is not available")
            End If

            If AppSettings("ATTAIN_USE_CURRENCY_FROM_BILLTO_CUSTOMER") Then
                orderobject("CurrencyGuid") = CStrEx(dtCustomer.Rows(0)("CurrencyGuid"))
            Else
                orderobject("CurrencyGuid") = CStrEx(Context("CurrencyGuid"))
            End If

            If orderobject("CurrencyGuid") = "" Then orderobject("CurrencyGuid") = Context("DefaultCurrency")
        End Sub

        ' --
        '    2 (Header)
        ' --
        Protected Sub NFSubTotal(ByVal orderobject As ExpDictionary, ByVal Context As ExpDictionary)
            Dim AmountSum As Decimal = 0
            Dim InvDiscSum As Decimal = 0
            Dim orderline As ExpDictionary
            'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
            Dim AmountSumInclTax As Decimal = 0
            'AM2011031801 - ENTERPRISE CSR STAND ALONE - End

            For Each orderline In orderobject("Lines").values
                AmountSum = AmountSum + CDblEx(orderline("LineTotal"))
                If CBoolEx(orderline("AllowInvoiceDiscount")) Then InvDiscSum = InvDiscSum + CDblEx(orderline("LineTotal"))
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
                If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
                    csrObj.buslogicSumInclTax(AmountSumInclTax, orderline)
                End If
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
            Next
            orderobject("SubTotal") = AmountSum
            orderobject("SubTotal_InvoiceDiscount") = InvDiscSum
            orderobject("Total") = AmountSum
            'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
            If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
                csrObj.buslogicSetTotals(orderobject, AmountSumInclTax, AmountSum)
            End If
            'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
        End Sub

        ' --
        '    3 (Header)
        ' --
        Protected Sub NFInvoiceDiscounts(ByVal orderobject As ExpDictionary, ByVal Context As ExpDictionary)
            Dim sql As String
            Dim DiscountPct, ServiceCharge, tmpInvSubTotal, tmpSubTotal As Decimal
            Dim resDict As ExpDictionary
            Dim item As Object
            Dim SameCurr, DiscountFound, ServiceChargeFound As Boolean

            DiscountPct = 0
            ServiceCharge = 0
            ' Calculation of Service chage and invoice discount.
            ' Invoice discount should be calculated on SubTotal_InvoiceDiscount (Found in Function NFSubTotal)
            ' Service Charge should be calculated on SubTotal - not considering if invoice dicounts are allowed or not.

            ' Read the table
            sql = "SELECT DiscountPct, CurrencyCode, ServiceCharge, MinimumAmount FROM Attain_CustomerInvoiceDiscount WHERE" & _
                  " InvoiceDiscountCode=" & SafeString(orderobject("InvoiceDiscountCode")) & " AND" & _
                  " ((CurrencyCode='') Or (CurrencyCode IS NULL) OR (CurrencyCode = " & SafeString(orderobject("CurrencyGuid")) & "))" & _
                  " ORDER BY CurrencyCode DESC, MinimumAmount DESC"

            resDict = Sql2Dictionaries2(sql, New String() {"CurrencyCode", "MinimumAmount"})

            DiscountPct = 0
            ServiceCharge = 0
            DiscountFound = False
            ServiceChargeFound = False
            SameCurr = False

            ' Try with same currency
            If resDict.ContainsKey(SafeString(orderobject("CurrencyGuid"))) Then
                SameCurr = True
                For Each item In resDict(SafeString(orderobject("CurrencyGuid"))).values

                    If (Not DiscountFound) And (orderobject("SubTotal_InvoiceDiscount") >= item("MinimumAmount")) Then
                        DiscountFound = True
                        DiscountPct = item("DiscountPct")
                    End If

                    If (Not ServiceChargeFound) And (orderobject("SubTotal") >= item("MinimumAmount")) Then
                        ServiceChargeFound = True
                        ServiceCharge = item("ServiceCharge")
                    End If

                    If ServiceChargeFound And DiscountFound Then Exit For
                Next
            End If

            ' Try with no currency.
            If (Not SameCurr) And resDict.ContainsKey(SafeString("")) Then
                For Each item In resDict(SafeString("")).values
                    tmpInvSubTotal = globals.currency.ConvertCurrency(orderobject("SubTotal_InvoiceDiscount"), orderobject("CurrencyGuid"), "")
                    tmpSubTotal = globals.currency.ConvertCurrency(orderobject("SubTotal"), orderobject("CurrencyGuid"), "")

                    If (Not DiscountFound) And (tmpInvSubTotal >= item("MinimumAmount")) Then
                        DiscountFound = True
                        DiscountPct = item("DiscountPct")
                    End If

                    If (Not ServiceChargeFound) And (tmpSubTotal >= item("MinimumAmount")) Then
                        ServiceChargeFound = True
                        ServiceCharge = globals.currency.ConvertCurrency(item("ServiceCharge"), "", orderobject("CurrencyGuid"))
                    End If

                    If ServiceChargeFound And DiscountFound Then Exit For
                Next
            End If

            orderobject("InvoiceDiscountPct") = RoundEx(DiscountPct, 5) 'MAY BE WRONG MUST CHECK THIS!!
            orderobject("ServiceCharge") = RoundEx(ServiceCharge, 2)
            orderobject("InvoiceDiscount") = orderobject("SubTotal_InvoiceDiscount") * orderobject("InvoiceDiscountPct") / 100
            orderobject("Total") = orderobject("SubTotal") + orderobject("ServiceCharge") - orderobject("InvoiceDiscount")
        End Sub

        ' --
        '    4 (Header)
        ' --
        Protected Sub NFCalculateVAT(ByVal orderobject As ExpDictionary, ByVal Context As ExpDictionary)
            Dim dtVATSetup As DataTable
            Dim sql As String
            Dim VATPct As Decimal
            Dim resDict As ExpDictionary
            Dim VATSum, InvoiceDiscountAmountInclTaxSum, TotalInclTaxSum As Decimal
            Dim TotalSum, InvoiceDiscountAmountSum As Decimal
            Dim vatSums As New Dictionary(Of Decimal, VatNode)
            Dim subTotalSum As Decimal = 0
            Dim subTotalInclVatSum As Decimal = 0

            sql = "SELECT ProductGuid, VATPct FROM ProductTable LEFT JOIN Attain_VATPostingSetup ON " & _
                     "ProductTable.VATProductPostingGroup = Attain_VATPostingSetup.ProductPostingGroup " & _
                     "WHERE (BusinessPostingGroup=" & SafeString(orderobject("VATBusinessPostingGroup")) & ") AND " & _
                     "ProductGuid IN (" & Context("ProductList") & ")"

            resDict = Sql2Dictionaries(sql, "ProductGuid")

            VATSum = 0
            InvoiceDiscountAmountInclTaxSum = 0
            TotalInclTaxSum = 0

            InvoiceDiscountAmountSum = 0

            Dim orderLineListPriceInclTax As Decimal = 0
            Dim orderLineListPriceExclTax As Decimal = 0
            Dim orderLineSubTotalInclTax As Decimal = 0
            Dim orderLineSubTotalExclTax As Decimal = 0
            Dim orderLineTotalInclTax As Decimal = 0
            Dim orderLineTotalExclTax As Decimal = 0
            Dim orderLineLineDiscountAmountInclTax As Decimal = 0
            Dim orderLineLineDiscountAmountExclTax As Decimal = 0
            Dim orderLineInvoiceDiscountAmountInclTax As Decimal = 0
            Dim orderLineInvoiceDiscountAmountExclTax As Decimal = 0
            Dim orderLineTaxAmount As Decimal = 0
            Dim LineDiscountAmountExclTaxSum As Decimal = 0
            Dim LineDiscountAmountInclTaxSum As Decimal = 0

            For Each orderline As ExpDictionary In orderobject("Lines").values
                orderLineListPriceInclTax = 0
                orderLineListPriceExclTax = 0
                orderLineSubTotalInclTax = 0
                orderLineSubTotalExclTax = 0
                orderLineTotalInclTax = 0
                orderLineTotalExclTax = 0
                orderLineLineDiscountAmountInclTax = 0
                orderLineLineDiscountAmountExclTax = 0
                orderLineInvoiceDiscountAmountInclTax = 0
                orderLineInvoiceDiscountAmountExclTax = 0
                orderLineTaxAmount = 0
                VATPct = 0

                If Not resDict(SafeString(orderline("ProductGuid"))) Is Nothing Then
                    VATPct = CDblEx(resDict(SafeString(orderline("ProductGuid")))("VATPct"))
                End If

                If orderline("ListPriceIsInclTax") Then
                    orderLineListPriceInclTax = orderline("ListPrice")
                    orderLineListPriceExclTax = orderline("ListPrice") * (1 / (1 + VATPct / 100))
                    orderLineSubTotalInclTax = orderline("SubTotal")
                    orderLineSubTotalExclTax = orderline("SubTotal") * (1 / (1 + VATPct / 100))
                    orderLineTotalInclTax = orderline("LineTotal")
                    orderLineTotalExclTax = orderline("LineTotal") * (1 / (1 + VATPct / 100))
                    orderLineLineDiscountAmountInclTax = orderline("LineDiscountAmount")
                    orderLineLineDiscountAmountExclTax = orderline("LineDiscountAmount") * (1 / (1 + VATPct / 100))
                Else
                    orderLineListPriceInclTax = orderline("ListPrice") * (1 + VATPct / 100)
                    orderLineListPriceExclTax = orderline("ListPrice")
                    orderLineSubTotalInclTax = orderline("SubTotal") * (1 + VATPct / 100)
                    orderLineSubTotalExclTax = orderline("SubTotal")
                    orderLineTotalInclTax = orderline("LineTotal") * (1 + VATPct / 100)
                    orderLineTotalExclTax = orderline("LineTotal")
                    orderLineLineDiscountAmountInclTax = orderline("LineDiscountAmount") * (1 + VATPct / 100)
                    orderLineLineDiscountAmountExclTax = orderline("LineDiscountAmount")
                End If

                'Special case when customer prices is incl VAT - needs different calculation of line discounts
                If Not orderline("ListPriceIsInclTax") Then
                    If CBoolEx(orderobject("PricesIncludingVat")) Then
                        orderLineLineDiscountAmountInclTax = orderline("LineDiscountAmountInclTax")
                        orderLineTotalInclTax = orderline("TotalInclTax")
                    End If
                End If

                If orderline("AllowInvoiceDiscount") Then
                    orderLineInvoiceDiscountAmountInclTax = orderLineTotalInclTax * (orderobject("InvoiceDiscountPct") / 100)
                    orderLineInvoiceDiscountAmountExclTax = orderLineTotalExclTax * (orderobject("InvoiceDiscountPct") / 100)
                End If

                orderLineTaxAmount = orderLineTotalExclTax * (VATPct / 100)

                ' Add the correct values to the dictionary
                orderline("ListPrice") = RoundEx(orderLineListPriceExclTax, 5)
                orderline("ListPriceInclTax") = RoundEx(orderLineListPriceInclTax, 5)
                orderline("SubTotal") = RoundEx(orderLineSubTotalExclTax, 5)
                orderline("SubTotalInclTax") = RoundEx(orderLineSubTotalInclTax, 5)
                orderline("LineTotal") = RoundEx(orderLineTotalExclTax, 5)
                orderline("TotalInclTax") = RoundEx(orderLineTotalInclTax, 5)
                orderline("LineDiscountAmount") = RoundEx(orderLineLineDiscountAmountExclTax, 5)
                orderline("LineDiscountAmountInclTax") = RoundEx(orderLineLineDiscountAmountInclTax, 5)
                orderline("InvoiceDiscountAmount") = RoundEx(orderLineInvoiceDiscountAmountExclTax, 5)
                orderline("InvoiceDiscountAmountInclTax") = RoundEx(orderLineInvoiceDiscountAmountInclTax, 5)
                orderline("TaxAmount") = RoundEx(orderLineTaxAmount, 5)

                If orderobject("PricesIncludingVat") Then
                    If vatSums.ContainsKey(VATPct) Then
                        vatSums(VATPct).VatSum = vatSums(VATPct).VatSum + RoundEx(orderLineTotalInclTax, 2)
                        vatSums(VATPct).InvoiceDiscountAmountSumExclVAT = vatSums(VATPct).InvoiceDiscountAmountSumExclVAT + orderLineInvoiceDiscountAmountExclTax
                        vatSums(VATPct).InvoiceDiscountAmountSumInclVAT = vatSums(VATPct).InvoiceDiscountAmountSumInclVAT + orderLineInvoiceDiscountAmountInclTax
                    Else
                        Dim node As New VatNode()
                        node.VatSum = RoundEx(orderLineTotalInclTax, 2)
                        node.InvoiceDiscountAmountSumExclVAT = orderLineInvoiceDiscountAmountExclTax
                        node.InvoiceDiscountAmountSumInclVAT = orderLineInvoiceDiscountAmountInclTax
                        vatSums.Add(VATPct, node)
                    End If
                Else
                    If vatSums.ContainsKey(VATPct) Then
                        vatSums(VATPct).VatSum = vatSums(VATPct).VatSum + RoundEx(orderLineTotalExclTax, 2)
                        vatSums(VATPct).InvoiceDiscountAmountSumExclVAT = vatSums(VATPct).InvoiceDiscountAmountSumExclVAT + orderLineInvoiceDiscountAmountExclTax
                        vatSums(VATPct).InvoiceDiscountAmountSumInclVAT = vatSums(VATPct).InvoiceDiscountAmountSumInclVAT + orderLineInvoiceDiscountAmountInclTax
                    Else
                        Dim node As New VatNode()
                        node.VatSum = RoundEx(orderLineTotalExclTax, 2)
                        node.InvoiceDiscountAmountSumExclVAT = orderLineInvoiceDiscountAmountExclTax
                        node.InvoiceDiscountAmountSumInclVAT = orderLineInvoiceDiscountAmountInclTax
                        vatSums.Add(VATPct, node)
                    End If
                End If

                InvoiceDiscountAmountSum = InvoiceDiscountAmountSum + orderLineInvoiceDiscountAmountExclTax
                InvoiceDiscountAmountInclTaxSum = InvoiceDiscountAmountInclTaxSum + orderLineInvoiceDiscountAmountInclTax
                LineDiscountAmountInclTaxSum = LineDiscountAmountInclTaxSum + RoundEx(orderLineLineDiscountAmountInclTax, 2)
                LineDiscountAmountExclTaxSum = LineDiscountAmountExclTaxSum + RoundEx(orderLineLineDiscountAmountExclTax, 2)
                subTotalSum = subTotalSum + RoundEx(orderLineSubTotalExclTax, 2)
                subTotalInclVatSum = subTotalInclVatSum + RoundEx(orderLineSubTotalInclTax, 2)
                TotalSum = TotalSum + RoundEx(orderLineTotalExclTax, 2)
                TotalInclTaxSum = TotalInclTaxSum + RoundEx(orderLineTotalInclTax, 2)
                orderline("TaxPct") = VATPct
            Next

            InvoiceDiscountAmountSum = RoundEx(InvoiceDiscountAmountSum, 2)
            InvoiceDiscountAmountInclTaxSum = RoundEx(InvoiceDiscountAmountInclTaxSum, 2)

            Dim VATxSum As Decimal = 0
            Dim VATexDiscount As Decimal = 0
            Dim VATexDiscountSum As Decimal = 0
            VATSum = 0
            Dim calculatedVatValuesDict As New ExpDictionary()
            For Each vatType As Decimal In vatSums.Keys
                If orderobject("PricesIncludingVat") Then
                    VATxSum = RoundEx(vatSums(vatType).VatSum, 2)
                    VATexDiscount = RoundEx(VATxSum * (1 - (1 / (1 + vatType / 100))), 2)
                    VATxSum = RoundEx((VATxSum - RoundEx(vatSums(vatType).InvoiceDiscountAmountSumInclVAT, 2)) * (1 - (1 / (1 + vatType / 100))), 2)
                    VATSum = VATSum + VATxSum
                Else
                    VATxSum = RoundEx(vatSums(vatType).VatSum, 2)
                    VATexDiscount = VATxSum * (vatType / 100)
                    VATxSum = RoundEx((VATxSum - RoundEx(vatSums(vatType).InvoiceDiscountAmountSumExclVAT, 2)) * (vatType / 100), 2)
                    VATSum = VATSum + VATxSum
                End If
                If orderobject.ContainsKey("HeaderGuid") Then
                    If Not calculatedVatValuesDict.ContainsKey("VatTypes") Then
                        calculatedVatValuesDict("VatTypes") = New ExpDictionary()
                    End If
                    calculatedVatValuesDict("VatTypes")(vatType) = VATxSum
                End If
                VATexDiscountSum = VATexDiscountSum + VATexDiscount
            Next

            If orderobject("PricesIncludingVat") Then
                TotalInclTaxSum = (TotalInclTaxSum - InvoiceDiscountAmountInclTaxSum)
            Else
                TotalInclTaxSum = (TotalSum - InvoiceDiscountAmountSum) + VATSum
                subTotalInclVatSum = RoundEx(subTotalSum + VATexDiscountSum, 2)
            End If

            ' --
            ' Service Charge calculations. Lookup the tax pct for service charge according to cfg.
            ' --
            sql = "SELECT TOP 1 VATPct FROM Attain_VATPostingSetup WHERE (BusinessPostingGroup=" & _
                  SafeString(orderobject("VATBusinessPostingGroup")) & ") AND " & _
                  "(ProductPostingGroup=" & SafeString(AppSettings("ATTAIN_SERVICECHARGE_PRODUCTPOSTINGGROUP")) & ")"

            dtVATSetup = SQL2DataTable(sql)
            If dtVATSetup.Rows.Count > 0 Then
                VATPct = CDblEx(dtVATSetup.Rows(0)("VATPct"))
            Else
                VATPct = 0
            End If
            dtVATSetup.Dispose()

            ' Set the VATPct in the orderobject for the Shipping and Handling calculations.
            orderobject("TaxPct") = VATPct

            ' Calculate the VAT on the Service Charge, if any      
            orderobject("ServiceChargeInclTax") = orderobject("ServiceCharge") * (1 + VATPct / 100)
            VATSum = VATSum + orderobject("ServiceCharge") * VATPct / 100

            ' Tax fields
            orderobject("TaxAmount") = VATSum

            orderobject("InvoiceDiscount") = InvoiceDiscountAmountSum
            orderobject("InvoiceDiscountInclTax") = InvoiceDiscountAmountInclTaxSum
            orderobject("SubTotalInclTax") = subTotalInclVatSum - LineDiscountAmountInclTaxSum

            If orderobject("PricesIncludingVat") Then
                orderobject("SubTotal") = subTotalInclVatSum - VATexDiscountSum - LineDiscountAmountInclTaxSum
                orderobject("Total") = TotalInclTaxSum + CDblEx(orderobject("ServiceChargeInclTax")) - VATSum
            Else
                orderobject("SubTotal") = subTotalSum - LineDiscountAmountExclTaxSum
                orderobject("Total") = TotalSum + CDblEx(orderobject("ServiceCharge")) - InvoiceDiscountAmountSum
            End If

            orderobject("TotalInclTax") = TotalInclTaxSum + CDblEx(orderobject("ServiceChargeInclTax"))

            If orderobject.ContainsKey("HeaderGuid") Then
                orderobject("VatTypes") = MarshallDictionary(calculatedVatValuesDict("VatTypes"))
            End If

        End Sub

        ' --
        '    LINE FUNCTIONS
        ' --

        ' --
        '    1 (Line)
        ' --
        Protected Sub NFStandardPrice(ByVal orderobject As ExpDictionary, ByVal Context As ExpDictionary)
            Dim sql As String
            Dim resultDict As ExpDictionary
            Dim item, key As Object
            Dim orderlines As ExpDictionary

            sql = "SELECT DISTINCT ProductGuid, ListPrice, AllowInvoiceDiscount, VATProductPostingGroup, ItemCustomerDiscountGroup, PriceIncludesVAT FROM ProductTable " & _
                  "WHERE ProductGuid IN (" & Context("ProductList") & ")"

            resultDict = Sql2Dictionaries(sql, "ProductGuid")

            orderlines = orderobject("Lines")
            For Each key In orderlines.ClonedKeyArray
                Dim orderline As ExpDictionary = orderlines(key)
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
                If Not CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) OrElse Not csrObj.isReturn(orderline) Then
                    If resultDict(SafeString(orderline("ProductGuid"))) IsNot Nothing Then
                        item = resultDict(SafeString(orderline("ProductGuid")))

                        orderline("ListPrice") = item("ListPrice")
                        orderline("CurrencyGuid") = Context("DefaultCurrency")

                        ' Allow line discount should follow the customer.
                        ' This is overruled by AllowLineDiscount from prices found in then NFListPrice function.
                        orderline("AllowLineDisc") = orderobject("AllowLineDisc")
                        orderline("AllowInvoiceDiscount") = item("AllowInvoiceDiscount")
                        orderline("ItemCustomerDiscountGroup") = item("ItemCustomerDiscountGroup")
                        orderline("VATProductPostingGroup") = item("VATProductPostingGroup")
                        orderline("ListPriceIsInclTax") = item("PriceIncludesVAT")
                    Else
                        orderlines.Remove(key)
                        Context("Warnings")("LINESREMOVED") = Resources.Language.LABEL_LINES_HAVE_BEEN_REMOVED_FROM_ORDER_PAD
                        Context("Warnings").Add(Context("Warnings").count + 1, "ExpandIT Attain Business Logic, NFStandardPrice: " & _
                            Resources.Language.LABEL_PRODUCT & " [" & orderline("ProductGuid") & "] " & _
                            Resources.Language.MESSAGE_IS_NOT_AVAILABLE)
                    End If
                End If
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
            Next

            ' ItemCustomerDiscountGroupList
            Context("ItemCustomerDiscountGroupList") = GetSQLInString(orderobject("Lines"), "ItemCustomerDiscountGroup")
        End Sub

        ' --
        '    2 (Line)
        ' --
        Protected Sub NFListPrice(ByVal orderobject As ExpDictionary, ByVal Context As ExpDictionary)
            Dim sql As String
            Dim lowPrice As Decimal = 0
            Dim UOMPrice As Decimal = 0

            Dim UomDict As String = ""
            Dim BaseUOM As String = ""
            Dim PriceInclTaxValue As Double
            Dim AllowLineDiscountValue As Boolean
            Dim AllowInvoiceDiscountValue As Boolean
            Dim AttainVATBusPostingGrPriceValue As String = "'"
            Dim CurrencyGuidValue As String = ""

            Dim currentGuid As String = ""
            Dim currentVariantCode As String = ""
            Dim currentUom As String = ""
            Dim currentQty As Integer = 0

            Dim filterdict1 As New ExpDictionary
            Dim filterdict2 As New ExpDictionary
            Dim filterdict3 As New ExpDictionary
            Dim filterdict4 As New ExpDictionary

            Dim LinkedItems As Boolean

            'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - Start
            Try
                Dim pageobj As ExpandIT.Page = CType(System.Web.HttpContext.Current.Handler, ExpandIT.Page)
                LinkedItems = CBoolEx(globals.eis.getEnterpriseConfigurationValue("LinkedItemsEnabled"))
            Catch ex As Exception
            End Try
            'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - End

            Dim minqty As Integer = 0
            Dim UomQty As Double = 0

            For Each itemLine As ExpDictionary In orderobject("Lines").values
                lowPrice = 0
                'If CDblEx(itemLine("ListPrice")) = 0 Then
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
                If Not CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) OrElse Not csrObj.isReturn(itemLine) Then
                    currentGuid = CStrEx(itemLine("ProductGuid"))
                    currentVariantCode = CStrEx(itemLine("VariantCode"))
                    currentUom = CStrEx(itemLine("UOM"))
                    currentQty = CIntEx(itemLine("Quantity"))

                    'AM2011061701 - ITEM LINKING - START
                    If LinkedItems And currentVariantCode <> "" Then
                        currentGuid = currentVariantCode
                        currentVariantCode = ""
                    End If
                    'AM2011061701 - ITEM LINKING - END


                    BaseUOM = CStrEx(getSingleValueDB("SELECT BaseUOM FROM ProductTable WHERE ProductGuid=" & SafeString(currentGuid)))
                    'AM0122201001 - UOM - Start
                    'If CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) And 
                    If currentUom = "" Then
                        currentUom = BaseUOM
                    End If
                    'AM0122201001 - UOM - End
                    'Checking Currency
                    sql = "SELECT ProductGuid, AllowInvoiceDiscount, Attain_VATBusPostingGrPrice, CustomerRelType, " & _
                        "MinimumQuantity, EndingDate, CustomerRelGuid, CurrencyGuid, StartingDate, UnitPrice, " & _
                        "UOMGuid, VariantCode, PriceInclTax, AllowLineDiscount FROM Attain_ProductPrice " & _
                        "WHERE (ProductGuid =" & SafeString(currentGuid)
                    If CStrEx(itemLine("ProductGroupCode")) <> "" Then sql &= " OR ProductGroup=" & SafeString(itemLine("ProductGroupCode"))
                    sql &= ") AND ((CustomerRelType = 0 AND CustomerRelGuid = " & SafeString(Context("CustomerGuid")) & ") OR " & _
                    "(CustomerRelType = 1 AND CustomerRelGuid = " & SafeString(orderobject("PriceGroupCode")) & ") OR " & _
                    "(CustomerRelType = 2)) AND " & _
                    "(StartingDate <= " & SafeDatetime(DateValue(Now)) & " OR StartingDate IS NULL) AND " & _
                    "(EndingDate >= " & SafeDatetime(DateValue(Now)) & " OR EndingDate IS NULL) "
                    sql = sql & "AND (CurrencyGuid = " & SafeString(CStrEx(itemLine("CurrencyGuid"))) & ") "
                    sql = sql & "AND (VariantCode = " & SafeString(currentVariantCode) & " OR VariantCode='') "
                    sql = sql & "AND (UOMGuid = " & SafeString(currentUom) & " OR UOMGuid='') "
                    sql = sql & " ORDER BY ProductGuid,VariantCode"

                    Dim currencyDict As ExpDictionary = SQL2Dicts(sql)

                    If Not currencyDict Is Nothing AndAlso Not currencyDict Is DBNull.Value AndAlso currencyDict.Count > 0 Then
                        UomQty = 0
                        For Each itemCurrency As ExpDictionary In currencyDict.Values
                            Dim currentQty2 As Double = currentQty
                            UOMPrice = CDblEx(itemCurrency("UnitPrice"))
                            If currentUom <> "" AndAlso CStrEx(itemCurrency("UOMGuid")) <> currentUom Then
                                UomQty = GetProductUOMQuantity(currentGuid, currentUom)
                                currentQty2 = currentQty2 * UomQty
                                UOMPrice = UOMPrice * UomQty
                            End If
                            If currentQty2 >= CDblEx(itemCurrency("MinimumQuantity")) Then
                                If lowPrice = 0 Or UOMPrice < lowPrice Then
                                    UomDict = CStrEx(itemCurrency("UOMGuid"))
                                    lowPrice = UOMPrice
                                    PriceInclTaxValue = CDblEx(itemCurrency("PriceInclTax")) * UomQty
                                    AllowLineDiscountValue = CBoolEx(itemCurrency("AllowLineDiscount"))
                                    AllowInvoiceDiscountValue = CBoolEx(itemCurrency("AllowInvoiceDiscount"))
                                    AttainVATBusPostingGrPriceValue = CStrEx(itemCurrency("AttainVATBusPostingGrPrice"))
                                    CurrencyGuidValue = CStrEx(itemCurrency("CurrencyGuid"))
                                End If
                            End If
                        Next
                    Else

                        'Checking Not Currency

                        'First Filter - Start
                        If currentVariantCode <> "" Then
                            sql = "SELECT ProductGuid, AllowInvoiceDiscount, Attain_VATBusPostingGrPrice, CustomerRelType, " & _
                                "MinimumQuantity, EndingDate, CustomerRelGuid, CurrencyGuid, StartingDate, UnitPrice, " & _
                                "UOMGuid, VariantCode, PriceInclTax, AllowLineDiscount FROM Attain_ProductPrice " & _
                                "WHERE (ProductGuid =" & SafeString(currentGuid)
                            If CStrEx(itemLine("ProductGroupCode")) <> "" Then sql &= " OR ProductGroup=" & SafeString(itemLine("ProductGroupCode"))
                            sql &= ") AND ((CustomerRelType = 0 AND CustomerRelGuid = " & SafeString(Context("CustomerGuid")) & ") OR " & _
                                "(CustomerRelType = 1 AND CustomerRelGuid = " & SafeString(orderobject("PriceGroupCode")) & ") OR " & _
                                "(CustomerRelType = 2)) AND " & _
                                "(StartingDate <= " & SafeDatetime(DateValue(Now)) & " OR StartingDate IS NULL) AND " & _
                                "(EndingDate >= " & SafeDatetime(DateValue(Now)) & " OR EndingDate IS NULL) "
                            sql = sql & "AND (CurrencyGuid = '') "
                            sql = sql & "AND (VariantCode = " & SafeString(currentVariantCode) & ") "
                            sql = sql & "AND (UOMGuid = " & SafeString(currentUom) & " OR UOMGuid = '') "
                            sql = sql & " ORDER BY ProductGuid,VariantCode"

                            filterdict1 = SQL2Dicts(sql)
                            If Not filterdict1 Is Nothing AndAlso Not filterdict1 Is DBNull.Value AndAlso filterdict1.Count > 0 Then

                                For Each itemFilterdict1 As ExpDictionary In filterdict1.Values
                                    UOMPrice = CDblEx(itemFilterdict1("UnitPrice"))
                                    If currentUom <> "" AndAlso itemFilterdict1("UOMGuid") <> currentUom Then
                                        UomQty = GetProductUOMQuantity(currentGuid, currentUom)
                                        minqty = UomQty * currentQty
                                        UOMPrice = UOMPrice * UomQty
                                    Else
                                        minqty = currentQty
                                    End If
                                    If itemFilterdict1("MinimumQuantity") <= minqty Then
                                        If lowPrice = 0 Or UOMPrice < lowPrice Then
                                            UomDict = CStrEx(itemFilterdict1("UOMGuid"))
                                            lowPrice = UOMPrice
                                            PriceInclTaxValue = CDblEx(itemFilterdict1("PriceInclTax")) * UomQty
                                            AllowLineDiscountValue = CBoolEx(itemFilterdict1("AllowLineDiscount"))
                                            AllowInvoiceDiscountValue = CBoolEx(itemFilterdict1("AllowInvoiceDiscount"))
                                            AttainVATBusPostingGrPriceValue = CStrEx(itemFilterdict1("AttainVATBusPostingGrPrice"))
                                            CurrencyGuidValue = CStrEx(itemFilterdict1("CurrencyGuid"))
                                        End If
                                    End If
                                Next
                            End If
                        End If
                        'First Filter - End


                        'Second Filter - Start
                        If lowPrice = 0 AndAlso currentUom <> "" Then
                            sql = "SELECT ProductGuid, AllowInvoiceDiscount, Attain_VATBusPostingGrPrice, CustomerRelType, " & _
                                "MinimumQuantity, EndingDate, CustomerRelGuid, CurrencyGuid, StartingDate, UnitPrice, " & _
                                "UOMGuid, VariantCode, PriceInclTax, AllowLineDiscount FROM Attain_ProductPrice " & _
                                "WHERE (ProductGuid =" & SafeString(currentGuid)
                            If CStrEx(itemLine("ProductGroupCode")) <> "" Then sql &= " OR ProductGroup=" & SafeString(itemLine("ProductGroupCode"))
                            sql &= ") AND ((CustomerRelType = 0 AND CustomerRelGuid = " & SafeString(Context("CustomerGuid")) & ") OR " & _
                                "(CustomerRelType = 1 AND CustomerRelGuid = " & SafeString(orderobject("PriceGroupCode")) & ") OR " & _
                                "(CustomerRelType = 2)) AND " & _
                                "(StartingDate <= " & SafeDatetime(DateValue(Now)) & " OR StartingDate IS NULL) AND " & _
                                "(EndingDate >= " & SafeDatetime(DateValue(Now)) & " OR EndingDate IS NULL) "
                            sql = sql & "AND (CurrencyGuid = '') "
                            sql = sql & "AND (VariantCode = '') "
                            sql = sql & "AND (UOMGuid = " & SafeString(currentUom) & ") "
                            sql = sql & " ORDER BY ProductGuid,VariantCode"

                            filterdict3 = SQL2Dicts(sql)

                            If Not filterdict3 Is Nothing AndAlso Not filterdict3 Is DBNull.Value AndAlso filterdict3.Count > 0 Then
                                For Each itemFilterdict3 As ExpDictionary In filterdict3.Values
                                    UOMPrice = CDblEx(itemFilterdict3("UnitPrice"))
                                    If currentUom <> "" AndAlso itemFilterdict3("UOMGuid") <> currentUom Then
                                        UomQty = GetProductUOMQuantity(currentGuid, currentUom)
                                        minqty = UomQty * currentQty
                                        UOMPrice = UOMPrice * UomQty
                                    Else
                                        minqty = currentQty
                                    End If
                                    If itemFilterdict3("MinimumQuantity") <= minqty Then
                                        If lowPrice = 0 Or UOMPrice < lowPrice Then
                                            UomDict = CStrEx(itemFilterdict3("UOMGuid"))
                                            lowPrice = UOMPrice
                                            PriceInclTaxValue = CDblEx(itemFilterdict3("PriceInclTax")) * UomQty
                                            AllowLineDiscountValue = CBoolEx(itemFilterdict3("AllowLineDiscount"))
                                            AllowInvoiceDiscountValue = CBoolEx(itemFilterdict3("AllowInvoiceDiscount"))
                                            AttainVATBusPostingGrPriceValue = CStrEx(itemFilterdict3("AttainVATBusPostingGrPrice"))
                                            CurrencyGuidValue = CStrEx(itemFilterdict3("CurrencyGuid"))
                                        End If
                                    End If
                                Next
                            End If
                        End If
                        'Second Filter - End

                        'Third Filter - Start
                        If lowPrice = 0 Then
                            sql = "SELECT ProductGuid, AllowInvoiceDiscount, Attain_VATBusPostingGrPrice, CustomerRelType, " & _
                                "MinimumQuantity, EndingDate, CustomerRelGuid, CurrencyGuid, StartingDate, UnitPrice, " & _
                                "UOMGuid, VariantCode, PriceInclTax, AllowLineDiscount FROM Attain_ProductPrice " & _
                                "WHERE (ProductGuid =" & SafeString(currentGuid)
                            If CStrEx(itemLine("ProductGroupCode")) <> "" Then sql &= " OR ProductGroup=" & SafeString(itemLine("ProductGroupCode"))
                            sql &= ") AND ((CustomerRelType = 0 AND CustomerRelGuid = " & SafeString(Context("CustomerGuid")) & ") OR " & _
                                "(CustomerRelType = 1 AND CustomerRelGuid = " & SafeString(orderobject("PriceGroupCode")) & ") OR " & _
                                "(CustomerRelType = 2)) AND " & _
                                "(StartingDate <= " & SafeDatetime(DateValue(Now)) & " OR StartingDate IS NULL) AND " & _
                                "(EndingDate >= " & SafeDatetime(DateValue(Now)) & " OR EndingDate IS NULL) "
                            sql = sql & "AND (CurrencyGuid = '') "
                            'sql = sql & "AND (VariantCode = '') "
                            'sql = sql & "AND (UOMGuid = '') "
                            'If Not CBoolEx(AppSettings("EXPANDIT_US_USE_VARIANTS")) Then
                            sql &= " AND (VariantCode IS NULL OR VariantCode='') "
                            'If Not CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) Then 
                            'sql &= " AND (UOMGuid IS NULL OR UOMGuid='' OR UOMGuid=" & SafeString(BaseUOM) & " ) "
                            sql &= " AND (UOMGuid IS NULL OR UOMGuid='') "

                            sql = sql & " ORDER BY ProductGuid,VariantCode"

                            filterdict4 = SQL2Dicts(sql)

                            If Not filterdict4 Is Nothing AndAlso Not filterdict4 Is DBNull.Value AndAlso filterdict4.Count > 0 Then
                                For Each itemFilterdict4 As ExpDictionary In filterdict4.Values
                                    UOMPrice = CDblEx(itemFilterdict4("UnitPrice"))
                                    If currentUom <> "" AndAlso itemFilterdict4("UOMGuid") <> currentUom Then
                                        UomQty = GetProductUOMQuantity(currentGuid, currentUom)
                                        minqty = UomQty * currentQty
                                        UOMPrice = UOMPrice * UomQty
                                    Else
                                        minqty = currentQty
                                    End If
                                    If itemFilterdict4("MinimumQuantity") <= minqty Then
                                        If lowPrice = 0 Or UOMPrice < lowPrice Then
                                            UomDict = CStrEx(itemFilterdict4("UOMGuid"))
                                            lowPrice = UOMPrice
                                            PriceInclTaxValue = CDblEx(itemFilterdict4("PriceInclTax")) * UomQty
                                            AllowLineDiscountValue = CBoolEx(itemFilterdict4("AllowLineDiscount"))
                                            AllowInvoiceDiscountValue = CBoolEx(itemFilterdict4("AllowInvoiceDiscount"))
                                            AttainVATBusPostingGrPriceValue = CStrEx(itemFilterdict4("AttainVATBusPostingGrPrice"))
                                            CurrencyGuidValue = CStrEx(itemFilterdict4("CurrencyGuid"))
                                        End If
                                    End If
                                Next
                            End If
                        End If
                        'Third Filter - End
                    End If
                    If lowPrice = 0 Then
                        'AM2011061701 - ITEM LINKING - START
                        If LinkedItems And currentGuid <> "" Then
                            lowPrice = CDblEx(getSingleValueDB("SELECT ListPrice FROM ProductTable WHERE ProductGuid=" & SafeString(currentGuid)))
                        Else
                            lowPrice = itemLine("ListPrice")
                        End If
                        'AM2011061701 - ITEM LINKING - END
                        If currentUom <> "" AndAlso BaseUOM <> currentUom Then
                            lowPrice = lowPrice * GetProductUOMQuantity(currentGuid, currentUom)
                        End If
                        itemLine("ListPrice") = lowPrice
                    Else
                        itemLine("ListPrice") = lowPrice
                        itemLine("ListPriceIsInclTax") = PriceInclTaxValue
                        itemLine("AllowLineDisc") = AllowLineDiscountValue
                        itemLine("AllowInvoiceDiscount") = AllowInvoiceDiscountValue
                        itemLine("Attain_VATBusPostingGrPrice") = AttainVATBusPostingGrPriceValue
                        itemLine("CurrencyGuid") = CurrencyGuidValue
                    End If
                End If
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
                'End If
            Next

        End Sub

        Protected Function onlinePriceAvailable(ByVal guid As String)
            Dim sql As String = "SELECT OnlinePrice FROM ProductTable WHERE ProductGuid=" & SafeString(guid)
            Return CDblEx(getSingleValueDB(sql))
        End Function


        'AM2010051801 - SPECIAL ITEMS - Start
        ' --
        '    2.1 (Specials)
        ' --
        Protected Sub NFSpecialPrice(ByVal orderobject As ExpDictionary, ByVal Context As ExpDictionary)


            Dim sql As String
            Dim lowPrice As Decimal = 0

            Dim PriceInclTaxValue As Double
            Dim AllowLineDiscountValue As Boolean
            Dim AllowInvoiceDiscountValue As Boolean
            Dim AttainVATBusPostingGrPriceValue As String = "'"
            Dim CurrencyGuidValue As String = ""

            Dim currentGuid As String = ""
            Dim currentVariantCode As String = ""
            Dim currentUom As String = ""
            Dim currentQty As Integer = 0

            Dim filterdict1 As New ExpDictionary
            Dim filterdict2 As New ExpDictionary
            Dim filterdict3 As New ExpDictionary
            Dim filterdict4 As New ExpDictionary

            Dim LinkedItems As Boolean
            Dim campaign As String = ""
            'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - Start
            Try
                LinkedItems = CBoolEx(globals.eis.getEnterpriseConfigurationValue("LinkedItemsEnabled"))
                campaign = CStrEx(globals.eis.getEnterpriseConfigurationValue("SpecialsCampaign"))
            Catch ex As Exception
            End Try
            'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - End


            Dim minqty As Integer = 0

            For Each itemLine As ExpDictionary In orderobject("Lines").values


                'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
                If Not CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) OrElse Not csrObj.isReturn(itemLine) Then
                    currentGuid = CStrEx(itemLine("ProductGuid"))
                    currentVariantCode = CStrEx(itemLine("VariantCode"))
                    currentUom = CStrEx(itemLine("UOM"))
                    currentQty = CIntEx(itemLine("Quantity"))

                    'AM2011061701 - ITEM LINKING - START
                    If LinkedItems And currentVariantCode <> "" Then
                        currentGuid = currentVariantCode
                        currentVariantCode = ""
                    End If
                    'AM2011061701 - ITEM LINKING - END

                    'Checking Currency
                    sql = "SELECT ProductGuid, AllowInvoiceDiscount, Attain_VATBusPostingGrPrice, CustomerRelType, " & _
                    "MinimumQuantity, EndingDate, CustomerRelGuid, CurrencyGuid, StartingDate, UnitPrice, " & _
                    "UOMGuid, VariantCode, PriceInclTax, AllowLineDiscount FROM Attain_ProductPrice " & _
                    "WHERE ProductGuid ='" & currentGuid & "' AND " & _
                    "(CustomerRelType = 3 AND CustomerRelGuid = " & SafeString(campaign) & ") AND " & _
                    "(StartingDate <= " & SafeDatetime(DateValue(Now)) & " OR StartingDate IS NULL) AND " & _
                    "(EndingDate >= " & SafeDatetime(DateValue(Now)) & " OR EndingDate IS NULL) "
                    sql = sql & "AND (CurrencyGuid = " & SafeString(CStrEx(itemLine("CurrencyGuid"))) & ") "
                    sql = sql & "AND (VariantCode = " & SafeString(currentVariantCode) & " OR VariantCode='') "
                    sql = sql & "AND (UOMGuid = " & SafeString(currentUom) & " OR UOMGuid='') "
                    sql = sql & " ORDER BY ProductGuid,VariantCode"

                    Dim currencyDict As ExpDictionary = SQL2Dicts(sql)

                    If Not currencyDict Is Nothing AndAlso Not currencyDict Is DBNull.Value AndAlso currencyDict.Count > 0 Then
                        Dim UomQty As Integer = 0
                        For Each itemCurrency As ExpDictionary In currencyDict.Values
                            Dim currentQty2 As Integer = currentQty
                            If CStrEx(itemCurrency("UOMGuid")) <> currentUom Then
                                UomQty = GetProductUOMQuantity(currentGuid, currentUom)
                                currentQty2 = currentQty2 * UomQty
                            End If
                            If currentQty2 >= CDblEx(itemCurrency("MinimumQuantity")) Then
                                If lowPrice = 0 Or CDblEx(itemCurrency("UnitPrice")) < lowPrice Then
                                    lowPrice = CDblEx(itemCurrency("UnitPrice"))
                                    PriceInclTaxValue = CDblEx(itemCurrency("PriceInclTax"))
                                    AllowLineDiscountValue = CBoolEx(itemCurrency("AllowLineDiscount"))
                                    AllowInvoiceDiscountValue = CBoolEx(itemCurrency("AllowInvoiceDiscount"))
                                    AttainVATBusPostingGrPriceValue = CStrEx(itemCurrency("AttainVATBusPostingGrPrice"))
                                    CurrencyGuidValue = CStrEx(itemCurrency("CurrencyGuid"))
                                End If
                            End If
                        Next
                    Else

                        'Checking Not Currency

                        'First Filter - Start
                        If currentVariantCode <> "" Then
                            sql = "SELECT ProductGuid, AllowInvoiceDiscount, Attain_VATBusPostingGrPrice, CustomerRelType, " & _
                            "MinimumQuantity, EndingDate, CustomerRelGuid, CurrencyGuid, StartingDate, UnitPrice, " & _
                            "UOMGuid, VariantCode, PriceInclTax, AllowLineDiscount FROM Attain_ProductPrice " & _
                            "WHERE ProductGuid ='" & currentGuid & "' AND " & _
                            "(CustomerRelType = 3 AND CustomerRelGuid = " & SafeString(campaign) & ") AND " & _
                            "(StartingDate <= " & SafeDatetime(DateValue(Now)) & " OR StartingDate IS NULL) AND " & _
                            "(EndingDate >= " & SafeDatetime(DateValue(Now)) & " OR EndingDate IS NULL) "
                            sql = sql & "AND (CurrencyGuid = '') "
                            sql = sql & "AND (VariantCode = " & SafeString(currentVariantCode) & ") "
                            sql = sql & "AND (UOMGuid = " & SafeString(currentUom) & " OR UOMGuid = '') "
                            sql = sql & " ORDER BY ProductGuid,VariantCode"

                            filterdict1 = SQL2Dicts(sql)
                            If Not filterdict1 Is Nothing AndAlso Not filterdict1 Is DBNull.Value AndAlso filterdict1.Count > 0 Then

                                For Each itemFilterdict1 As ExpDictionary In filterdict1.Values
                                    If itemFilterdict1("UOMGuid") <> currentUom Then
                                        minqty = GetProductUOMQuantity(currentGuid, currentUom) * currentQty
                                    Else
                                        minqty = currentQty
                                    End If
                                    If itemFilterdict1("MinimumQuantity") <= minqty Then
                                        If lowPrice = 0 Or CDblEx(itemFilterdict1("UnitPrice")) < lowPrice Then
                                            lowPrice = CDblEx(itemFilterdict1("UnitPrice"))
                                            PriceInclTaxValue = CDblEx(itemFilterdict1("PriceInclTax"))
                                            AllowLineDiscountValue = CBoolEx(itemFilterdict1("AllowLineDiscount"))
                                            AllowInvoiceDiscountValue = CBoolEx(itemFilterdict1("AllowInvoiceDiscount"))
                                            AttainVATBusPostingGrPriceValue = CStrEx(itemFilterdict1("AttainVATBusPostingGrPrice"))
                                            CurrencyGuidValue = CStrEx(itemFilterdict1("CurrencyGuid"))
                                        End If
                                    End If
                                Next
                            End If
                        End If
                        'First Filter - End


                        'Second Filter - Start
                        If lowPrice = 0 AndAlso currentUom <> "" Then
                            sql = "SELECT ProductGuid, AllowInvoiceDiscount, Attain_VATBusPostingGrPrice, CustomerRelType, " & _
                            "MinimumQuantity, EndingDate, CustomerRelGuid, CurrencyGuid, StartingDate, UnitPrice, " & _
                            "UOMGuid, VariantCode, PriceInclTax, AllowLineDiscount FROM Attain_ProductPrice " & _
                            "WHERE ProductGuid ='" & currentGuid & "' AND " & _
                            "(CustomerRelType = 3 AND CustomerRelGuid = " & SafeString(campaign) & ") AND " & _
                            "(StartingDate <= " & SafeDatetime(DateValue(Now)) & " OR StartingDate IS NULL) AND " & _
                            "(EndingDate >= " & SafeDatetime(DateValue(Now)) & " OR EndingDate IS NULL) "
                            sql = sql & "AND (CurrencyGuid = '') "
                            sql = sql & "AND (VariantCode = '') "
                            sql = sql & "AND (UOMGuid = " & SafeString(currentUom) & ") "
                            sql = sql & " ORDER BY ProductGuid,VariantCode"

                            filterdict3 = SQL2Dicts(sql)

                            If Not filterdict3 Is Nothing AndAlso Not filterdict3 Is DBNull.Value AndAlso filterdict3.Count > 0 Then
                                For Each itemFilterdict3 As ExpDictionary In filterdict3.Values
                                    If itemFilterdict3("UOMGuid") <> currentUom Then
                                        minqty = GetProductUOMQuantity(currentGuid, currentUom) * currentQty
                                    Else
                                        minqty = currentQty
                                    End If
                                    If itemFilterdict3("MinimumQuantity") <= minqty Then
                                        If lowPrice = 0 Or CDblEx(itemFilterdict3("UnitPrice")) < lowPrice Then
                                            lowPrice = CDblEx(itemFilterdict3("UnitPrice"))
                                            PriceInclTaxValue = CDblEx(itemFilterdict3("PriceInclTax"))
                                            AllowLineDiscountValue = CBoolEx(itemFilterdict3("AllowLineDiscount"))
                                            AllowInvoiceDiscountValue = CBoolEx(itemFilterdict3("AllowInvoiceDiscount"))
                                            AttainVATBusPostingGrPriceValue = CStrEx(itemFilterdict3("AttainVATBusPostingGrPrice"))
                                            CurrencyGuidValue = CStrEx(itemFilterdict3("CurrencyGuid"))
                                        End If
                                    End If
                                Next
                            End If
                        End If
                        'Second Filter - End

                        'Third Filter - Start
                        If lowPrice = 0 Then
                            sql = "SELECT ProductGuid, AllowInvoiceDiscount, Attain_VATBusPostingGrPrice, CustomerRelType, " & _
                            "MinimumQuantity, EndingDate, CustomerRelGuid, CurrencyGuid, StartingDate, UnitPrice, " & _
                            "UOMGuid, VariantCode, PriceInclTax, AllowLineDiscount FROM Attain_ProductPrice " & _
                            "WHERE ProductGuid ='" & currentGuid & "' AND " & _
                            "(CustomerRelType = 3 AND CustomerRelGuid = " & SafeString(campaign) & ") AND " & _
                            "(StartingDate <= " & SafeDatetime(DateValue(Now)) & " OR StartingDate IS NULL) AND " & _
                            "(EndingDate >= " & SafeDatetime(DateValue(Now)) & " OR EndingDate IS NULL) "
                            sql = sql & "AND (CurrencyGuid = '') "
                            sql = sql & "AND (VariantCode = '') "
                            sql = sql & "AND (UOMGuid = '') "
                            sql = sql & " ORDER BY ProductGuid,VariantCode"

                            filterdict4 = SQL2Dicts(sql)

                            If Not filterdict4 Is Nothing AndAlso Not filterdict4 Is DBNull.Value AndAlso filterdict4.Count > 0 Then
                                For Each itemFilterdict4 As ExpDictionary In filterdict4.Values
                                    If itemFilterdict4("UOMGuid") <> currentUom Then
                                        minqty = GetProductUOMQuantity(currentGuid, currentUom) * currentQty
                                    Else
                                        minqty = currentQty
                                    End If
                                    If itemFilterdict4("MinimumQuantity") <= minqty Then
                                        If lowPrice = 0 Or CDblEx(itemFilterdict4("UnitPrice")) < lowPrice Then
                                            lowPrice = CDblEx(itemFilterdict4("UnitPrice"))
                                            PriceInclTaxValue = CDblEx(itemFilterdict4("PriceInclTax"))
                                            AllowLineDiscountValue = CBoolEx(itemFilterdict4("AllowLineDiscount"))
                                            AllowInvoiceDiscountValue = CBoolEx(itemFilterdict4("AllowInvoiceDiscount"))
                                            AttainVATBusPostingGrPriceValue = CStrEx(itemFilterdict4("AttainVATBusPostingGrPrice"))
                                            CurrencyGuidValue = CStrEx(itemFilterdict4("CurrencyGuid"))
                                        End If
                                    End If
                                Next
                            End If
                        End If
                        'Third Filter - End

                    End If

                    If lowPrice = 0 Then
                        'AM2011061701 - ITEM LINKING - START
                        If LinkedItems And currentGuid <> "" Then
                            itemLine("ListPrice") = CDblEx(getSingleValueDB("SELECT ListPrice FROM ProductTable WHERE ProductGuid=" & SafeString(currentGuid)))
                        Else
                            itemLine("ListPrice") = CDblEx(itemLine("ListPrice"))
                        End If
                        'AM2011061701 - ITEM LINKING - END
                    Else

                        itemLine("originalListPrice") = itemLine("ListPrice")

                        itemLine("ListPrice") = lowPrice * GetProductUOMQuantity(currentGuid, currentUom)
                        itemLine("ListPriceIsInclTax") = PriceInclTaxValue
                        itemLine("AllowLineDisc") = AllowLineDiscountValue
                        itemLine("AllowInvoiceDiscount") = AllowInvoiceDiscountValue
                        itemLine("Attain_VATBusPostingGrPrice") = AttainVATBusPostingGrPriceValue
                        itemLine("CurrencyGuid") = CurrencyGuidValue
                    End If
                End If
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
            Next
        End Sub
        'AM2010051801 - SPECIAL ITEMS - End

        ' --
        '    3 (Line)
        ' --
        Protected Sub NFItemPriceFixCurrency(ByVal orderobject As ExpDictionary, ByVal Context As ExpDictionary)
            For Each orderline As ExpDictionary In orderobject("Lines").values
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
                If Not CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) OrElse Not csrObj.isReturn(orderline) Then
                    If orderline("CurrencyGuid") <> orderobject("CurrencyGuid") Then
                        orderline("ListPrice") = globals.currency.ConvertCurrency(orderline("ListPrice"), orderline("CurrencyGuid"), orderobject("CurrencyGuid"))
                        orderline("CurrencyGuid") = orderobject("CurrencyGuid")
                    End If
                End If
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
            Next
        End Sub

        ' --
        '    4 (Line)
        ' --
        Protected Sub NFLineDiscount(ByVal orderobject As ExpDictionary, ByVal Context As ExpDictionary)
            Dim DiscPct As Decimal
            Dim sql, key, key2 As String
            Dim item, orderline, resultDict As ExpDictionary
            Dim dtResult As DataTable
            Dim currProduct As String
            Dim currGroup As String
            Dim ProductQuantities As ExpDictionary
            Dim GroupCounter As Integer
            Dim VariantTotalQuantity, ProductTotalQuantity, ItemLineDiscountPct, ItemMinimumQuantity As Decimal
            Dim ProductVariantCode, ItemVariantCode As String
            Dim ProductCounter As Integer
            Dim isCurrencyDiscount As Boolean
            Dim DiscPctC As Decimal

            ProductQuantities = Context("ProductQuantities")

            ' Line Discount
            ' Header conditions: CurrencyCode, StartingDate, EndingDate, SalesType, SalesCode
            ' Line conditions: (Type = 0)ItemNo OR (Type=1)ItemDiscGroup OR (Type=2), MinimumQuantity, VariantCode

            sql = "SELECT Code, SalesType, MinimumQuantity, EndingDate, SalesCode, Type, CurrencyCode AS CurrencyGuid, StartingDate, LineDiscountPct, VariantCode FROM Attain_SalesLineDiscount WHERE " & vbCrLf & _
           "(" & vbCrLf & _
          "   (Type = 0 AND Code IN (" & Context("ProductList") & ")) OR " & vbCrLf

            If Context("ItemCustomerDiscountGroupList") <> "" Then
                sql = sql & "   (Type = 1 AND Code IN (" & Context("ItemCustomerDiscountGroupList") & ")) OR " & vbCrLf
            End If

            sql = sql & "   (Type = 2)" & vbCrLf & _
  ")" & vbCrLf & _
  "AND" & vbCrLf & _
       "(" & vbCrLf & _
        "   (SalesType = 0 AND SalesCode " & SafeEqualString(orderobject("CustomerGuid")) & ") OR" & vbCrLf

            If orderobject("CustomerDiscountGroup") <> "" Then
                sql = sql & "   (SalesType = 1 AND SalesCode " & SafeEqualString(orderobject("CustomerDiscountGroup")) & ") OR" & vbCrLf
            End If

            sql = sql & "   (SalesType = 2)" & vbCrLf & _
                   ")" & vbCrLf & _
                   "AND" & vbCrLf & _
                   "(" & vbCrLf & _
                   "   (StartingDate <= " & SafeDatetime(DateValue(Now)) & ") OR" & vbCrLf & _
                   "   (StartingDate IS NULL)" & vbCrLf & _
                   ")" & vbCrLf & _
                   "AND" & vbCrLf & _
                   "(" & vbCrLf & _
                   "   (EndingDate >= " & SafeDatetime(DateValue(Now)) & ") OR" & vbCrLf & _
                   "   (EndingDate IS NULL) " & vbCrLf & _
                   ") AND " & vbCrLf

            sql = sql & "(CurrencyCode " & SafeEqualString(orderobject("CurrencyGuid")) & " OR CurrencyCode IS NULL OR CurrencyCode = '' OR CurrencyCode = " & SafeString(Context("DefaultCurrency")) & ")"

            sql = sql & " ORDER BY Type, Code"

            dtResult = SQL2DataTable(sql)
            resultDict = New ExpDictionary
            resultDict("Product") = New ExpDictionary
            resultDict("Group") = New ExpDictionary

            currProduct = ""
            currGroup = ""
            '            ProdCounter = 0
            GroupCounter = 0

            ' Generate result dictionary
            For Each row As DataRow In dtResult.Rows
                Select Case CStrEx(row("Type"))
                    ' Type = Item
                    Case "0"
                        If currProduct <> CStrEx(row("Code")) Then
                            currProduct = CStrEx(row("Code"))
                            ProductCounter = 1
                            resultDict("Product").Add(currProduct, New ExpDictionary())
                        End If
                        resultDict("Product")(currProduct).Add(CStrEx(ProductCounter), SetDictionary(row))
                        ProductCounter = ProductCounter + 1
                        ' Type = Item Disc. Group
                    Case "1"
                        If currGroup <> CStrEx(row("Code")) Then
                            currGroup = CStrEx(row("Code"))
                            GroupCounter = 1
                            resultDict("Group").Add(currGroup, New ExpDictionary)
                        End If
                        resultDict("Group")(currGroup).Add(CStrEx(GroupCounter), SetDictionary(row))
                        GroupCounter = GroupCounter + 1
                    Case Else
                        ' Ignore
                End Select
            Next

            For Each key In orderobject("Lines").keys
                orderline = orderobject("Lines")(key)

                DiscPct = 0
                DiscPctC = 0

                isCurrencyDiscount = False

                ' Search discounts for item
                If Not resultDict("Product")(orderline("ProductGuid")) Is Nothing Then
                    For Each key2 In resultDict("Product")(orderline("ProductGuid")).keys
                        item = resultDict("Product")(orderline("ProductGuid"))(key2)

                        If AppSettings("NAV_COLLECT_QUANTITES_FOR_LINE_DISCOUNTS") Then
                            ProductTotalQuantity = CDblEx(ProductQuantities(orderline("ProductGuid"))("Total"))
                        Else
                            ProductTotalQuantity = orderline("Quantity")
                        End If

                        ItemMinimumQuantity = CDblEx(item("MinimumQuantity"))
                        ItemVariantCode = CStrEx(item("VariantCode"))
                        ItemLineDiscountPct = CDblEx(item("LineDiscountPct"))
                        ProductVariantCode = CStrEx(orderline("VariantCode"))

                        isCurrencyDiscount = (CStrEx(item("CurrencyGuid")) = CStrEx(orderobject("CurrencyGuid")) And CStrEx(item("CurrencyGuid")) <> CStrEx(Context("DefaultCurrency")))

                        If ProductVariantCode = "" Then
                            If (ProductTotalQuantity >= ItemMinimumQuantity) And _
                             (ItemVariantCode = "") _
                            Then
                                If isCurrencyDiscount Then
                                    If ItemLineDiscountPct > DiscPctC Then
                                        DiscPctC = CDblEx(item("LineDiscountPct"))
                                    End If
                                Else
                                    If ItemLineDiscountPct > DiscPct Then
                                        DiscPct = CDblEx(item("LineDiscountPct"))
                                    End If
                                End If
                            End If
                        Else
                            If AppSettings("NAV_COLLECT_QUANTITES_FOR_LINE_DISCOUNTS") Then
                                VariantTotalQuantity = CDblEx(ProductQuantities(orderline("ProductGuid"))("Variants")(CStrEx(orderline("VariantCode"))))
                            Else
                                VariantTotalQuantity = orderline("Quantity")
                            End If

                            If (((ProductTotalQuantity >= ItemMinimumQuantity) And _
                             (ItemVariantCode = "")) Or _
                             ((VariantTotalQuantity >= ItemMinimumQuantity) And _
                             (ItemVariantCode = ProductVariantCode))) _
                            Then
                                If isCurrencyDiscount Then
                                    If ItemLineDiscountPct > DiscPctC Then
                                        DiscPctC = CDblEx(item("LineDiscountPct"))
                                    End If
                                Else
                                    If ItemLineDiscountPct > DiscPct Then
                                        DiscPct = CDblEx(item("LineDiscountPct"))
                                    End If
                                End If
                            End If
                        End If
                    Next
                End If

                ' Search discounts for item Grp
                If Not resultDict("Group")(orderline("ItemCustomerDiscountGroup")) Is Nothing Then
                    For Each key2 In resultDict("Group")(orderline("ItemCustomerDiscountGroup")).keys
                        item = resultDict("Group")(orderline("ItemCustomerDiscountGroup"))(key2)

                        If AppSettings("NAV_COLLECT_QUANTITES_FOR_LINE_DISCOUNTS") Then
                            ProductTotalQuantity = CDblEx(ProductQuantities(orderline("ProductGuid"))("Total"))
                        Else
                            ProductTotalQuantity = orderline("Quantity")
                        End If

                        ItemMinimumQuantity = CDblEx(item("MinimumQuantity"))
                        ItemVariantCode = CStrEx(item("VariantCode"))
                        ItemLineDiscountPct = CDblEx(item("LineDiscountPct"))
                        ProductVariantCode = CStrEx(orderline("VariantCode"))

                        isCurrencyDiscount = CBool(item("CurrencyGuid") = orderobject("CurrencyGuid") And item("CurrencyGuid") <> Context("DefaultCurrency"))

                        If ProductVariantCode = "" Then
                            If (ProductTotalQuantity >= ItemMinimumQuantity) And _
                                (ItemVariantCode = "") _
                            Then
                                If isCurrencyDiscount Then
                                    If ItemLineDiscountPct > DiscPctC Then
                                        DiscPctC = CDblEx(item("LineDiscountPct"))
                                    End If
                                Else
                                    If ItemLineDiscountPct > DiscPct Then
                                        DiscPct = CDblEx(item("LineDiscountPct"))
                                    End If
                                End If
                            End If
                        Else
                            If AppSettings("NAV_COLLECT_QUANTITES_FOR_LINE_DISCOUNTS") Then
                                VariantTotalQuantity = CDblEx(ProductQuantities(orderline("ProductGuid"))("Variants")(CStrEx(orderline("VariantCode"))))
                            Else
                                VariantTotalQuantity = orderline("Quantity")
                            End If

                            If (((ProductTotalQuantity >= ItemMinimumQuantity) And _
                                (ItemVariantCode = "")) Or _
                                ((VariantTotalQuantity >= ItemMinimumQuantity) And _
                                (ItemVariantCode = ProductVariantCode))) _
                            Then
                                If isCurrencyDiscount Then
                                    If ItemLineDiscountPct > DiscPctC Then
                                        DiscPctC = CDblEx(item("LineDiscountPct"))
                                    End If
                                Else
                                    If ItemLineDiscountPct > DiscPct Then
                                        DiscPct = CDblEx(item("LineDiscountPct"))
                                    End If
                                End If
                            End If
                        End If
                    Next
                End If

                If DiscPctC > 0 Then
                    orderline("LineDiscountPct") = DiscPctC
                Else
                    orderline("LineDiscountPct") = DiscPct
                End If
            Next
        End Sub

        ' --
        '    5 (Line)
        ' --
        Protected Sub NFOrderLineTotal(ByVal orderobject As ExpDictionary, ByVal Context As ExpDictionary)
            'Needed for the customer prices incl VAT calculation          
            Dim Sql As String = "SELECT ProductGuid, VATPct FROM ProductTable LEFT JOIN Attain_VATPostingSetup ON " & _
                     "ProductTable.VATProductPostingGroup = Attain_VATPostingSetup.ProductPostingGroup " & _
                     "WHERE (BusinessPostingGroup=" & SafeString(orderobject("VATBusinessPostingGroup")) & ") AND " & _
                     "ProductGuid IN (" & Context("ProductList") & ")"

            Dim resDict As ExpDictionary = Sql2Dictionaries(Sql, "ProductGuid")
            Dim VATPct As Decimal = 0

            For Each orderline As ExpDictionary In orderobject("Lines").values
                If Not orderline("AllowLineDisc") Then
                    orderline("LineDiscountPct") = 0
                End If

                If orderline("LineDiscountPct") > 100 Then orderline("LineDiscountPct") = 100

                'orderline("SubTotal") = orderline("Quantity") * orderline("ListPrice")
                orderline("SubTotal") = orderline("Quantity") * RoundEx(CDblEx(orderline("ListPrice")), 2)

                'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
                If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
                    csrObj.buslogicSetOrderLineTotals(orderline)
                End If
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
                '*****promotions*****-start
                ' ExpandIT US - MPF - Promotions Code - Start
                If AppSettings("EXPANDIT_US_USE_PROMOTION_CODES") Then
                    If (orderline("LineDiscountAmount") > 0) Then
                        orderline("LineDiscountPct") = ((orderline("LineDiscountAmount") / (orderline("ListPrice") * orderline("Quantity"))) * 100)
                    End If
                End If
                ' ExpandIT US - MPF - Promotions Code - Finish
                '*****promotions*****-end
                orderline("LineDiscountAmount") = orderline("SubTotal") * orderline("LineDiscountPct") / 100
                orderline("LineDiscount") = orderline("LineDiscountPct")
                orderline("LineTotal") = RoundEx(orderline("SubTotal"), 2) - RoundEx(orderline("LineDiscountAmount"), 2)

                'If the listprice is not including VAT
                If Not orderline("ListPriceIsInclTax") Then
                    'If the customers prices is including VAT
                    If CBoolEx(orderobject("PricesIncludingVat")) Then
                        'Get VAT %
                        If Not resDict(SafeString(orderline("ProductGuid"))) Is Nothing Then
                            VATPct = CDblEx(resDict(SafeString(orderline("ProductGuid")))("VATPct"))
                        End If
                        'Do a Navision style line amount and line discount calculation by recalculating the list price and line discount to include VAT, then
                        'round to two decimals and subtract the discount from the price.
                        orderline("SubTotalInclTax") = orderline("Quantity") * (orderline("ListPrice") * (1 + VATPct / 100))
                        orderline("LineDiscountAmountInclTax") = orderline("SubTotalInclTax") * orderline("LineDiscountPct") / 100
                        orderline("TotalInclTax") = RoundEx(orderline("SubTotalInclTax"), 2) - RoundEx(orderline("LineDiscountAmountInclTax"), 2)

                    End If
                End If

            Next
        End Sub


        Public Shared Function GetProductUOMQuantity(ByVal prdGuid As String, ByVal UOM As String)

            Dim sql As String

            sql = "SELECT QtyPerUnitOfMeasure FROM ProductTable INNER JOIN ItemUnitOfMeasure ON (ProductTable.ProductGuid = ItemUnitOfMeasure.ItemNo)  WHERE ProductTable.ProductGuid=" & SafeString(prdGuid) & " AND ItemUnitOfMeasure.Code=" & SafeString(UOM)

            If CDblEx(getSingleValueDB(sql)) <= 0 Then
                GetProductUOMQuantity = 1
            Else
                GetProductUOMQuantity = CDblEx(getSingleValueDB(sql))
            End If

        End Function

    End Class

End Namespace
