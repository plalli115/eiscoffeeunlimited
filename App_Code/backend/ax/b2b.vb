Imports System.Configuration.ConfigurationManager
Imports Microsoft.VisualBasic
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

Namespace ExpandIT

    Public Class B2BAxaptaClass
        Inherits B2BBaseClass

        Public Sub New(ByVal _globals As GlobalsClass)
            MyBase.New(_globals)
        End Sub

        Public Overrides Sub DecodeAddress(ByRef dict As Object)
            Dim p As Integer, address1 As String
            address1 = dict("Address1")
            p = InStr(1, address1, Chr(10))
            If p > 0 Then address1 = Left(address1, p - 1)
            dict("LanguageGuid") = UCase(Left(CStrEx(dict("LanguageGuid")), 2))
            dict("Address1") = address1
        End Sub

        ' ---------------------------------------------------------------
        ' Load the customer ledger entries for a selected customer.
        ' ---------------------------------------------------------------
        Public Overrides Function GetCustomerLedgerEntries(ByVal User As ExpDictionary) As ExpDictionary

            Dim sql As String
            Dim retval, dicLedgerEntry, dicOpenEntries As ExpDictionary

            ' Select the related customer, the customer table contains some calculated fields of interest
            sql = "SELECT * FROM CustomerTable WHERE CustomerGuid = '" & User("CustomerGuid") & "'"
            retval = Sql2Dictionary(sql)
            ' Select the ledger entries for the customer.
            sql = "SELECT * FROM CustomerLedgerEntry WHERE CustomerGuid = '" & User("CustomerGuid") & "' ORDER BY PostingDate,EntryGuid"

            Try
                retval("Lines") = SQL2Dicts(sql, "EntryGuid")
                sql = "SELECT * FROM AXAPTA_CustomerLedgerEntryOpen WHERE CustomerGuid = '" & User("CustomerGuid") & "'"
                dicOpenEntries = SQL2Dicts(sql, "RecId")
                ' Calculate totals
                retval("BalanceAmountLCY") = 0
                retval("BalanceDueLCY") = 0
                For Each dicLedgerEntry In dicOpenEntries.Values
                    retval("BalanceAmountLCY") = retval("BalanceAmountLCY") + dicLedgerEntry.Item("Amount")
                    If (Now > dicLedgerEntry.Item("DueDate")) Then
                        retval("BalanceDueLCY") = retval("BalanceDueLCY") + dicLedgerEntry.Item("Amount")
                    End If
                Next
            Catch ex As Exception
                HttpContext.Current.Response.Write("<b>An error occured.</b><br />Please ensure that you have extracted the Customer Ledger Entry " & _
                "table.<br /><br />" & _
                "<b>The following error was returned:</b> <br />" & vbCrLf & ex.Message & "(" & ex.Source & ")")
                HttpContext.Current.Response.End()
            End Try

            Return retval

        End Function

        ' ---------------------------------------------------------------
        ' Returns the document type description for a document type id.
        ' Blank is returned if the type id wasn't found.
        ' ---------------------------------------------------------------
        Public Overrides Function GetDocumentTypeDescription(ByVal lngType As Integer) As String
            Dim arrTypes As String() = {"", "Transfer", "Sales order", "Purchase order", "Inventory", "Production", "Project", "Interest", "Customer", "Exchange Adjustment", "Totaled", "Payroll", "Fixed Assets", "Collection Letter", "Vendor", "Payment", "Sales Tax", "Bank"}

            ' Set the return value if indexes aren't out of bounds.
            If LBound(arrTypes) <= lngType And UBound(arrTypes) >= lngType Then
                Return arrTypes(lngType)
            Else
                Return ""
            End If
        End Function

    End Class

End Namespace
