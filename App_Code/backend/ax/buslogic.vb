Imports System.Configuration.ConfigurationManager
Imports Microsoft.VisualBasic
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Data.SqlClient
Imports System.Data
Imports System.Collections.Generic

Namespace ExpandIT

    Public Class BuslogicAXClass
            Inherits BuslogicBaseClass

        Public Sub New(ByVal _globals As GlobalsClass)
            MyBase.New(_globals)
        End Sub

        Public Overrides Function CalcOrder(ByVal OrderDict As ExpDictionary, ByVal Context As ExpDictionary) As String
            Dim infoDict As ExpDictionary
            Dim IsCalculated As Boolean

            If OrderDict("Lines").Count > 0 Then
                ' no need to recalculate the order dictionary.
                IsCalculated = CBoolEx(OrderDict("IsCalculated"))

                If Not IsCalculated Then
                    infoDict = New ExpDictionary

                    infoDict("SystemDate") = DateTime.Today.ToShortDateString()
                    infoDict("ProductList") = GetSQLInString(OrderDict("Lines"), "ProductGuid")
                    infoDict("ProductsDict") = GetProductsDict(infoDict)
                    infoDict("ProductGroupList") = GetSQLInString(infoDict("ProductsDict"), "ProductGrp")
                    infoDict("ProductQuantities") = GetProductQuantities(OrderDict)
                    infoDict("ProductDiscountList") = GetSQLInString(infoDict("ProductsDict"), "MultilnDisc")

                    If infoDict("ProductDiscountList") <> "" Then infoDict("ProductDiscountList") = infoDict("ProductDiscountList") & ","

                    infoDict("ProductDiscountList") = infoDict("ProductDiscountList") & GetSQLInString(infoDict("ProductsDict"), "LineDisc")

                    If infoDict("ProductDiscountList") = "," Or infoDict("ProductDiscountList") = "" Then infoDict("ProductDiscountList") = "''"
                    If CType(infoDict("ProductDiscountList"), String).EndsWith(",") Then
                        Dim lastIndex As Integer = CType(infoDict("ProductDiscountList"), String).LastIndexOf(",")
                        infoDict("ProductDiscountList") = CType(infoDict("ProductDiscountList"), String).Remove(lastIndex, 1)
                    End If

                    Try
                        AXAPTAInitOrder(OrderDict, infoDict, Context)
                    Catch ex As Exception
                        Context("Errors").Add(Guid.NewGuid(), ex.Message)
                        SetOrderDictOnError(OrderDict)
                        Return Nothing
                    End Try

                    Try
                        infoDict("DictAXAPTA_PriceDisc") = GetDictAXAPTA_PriceDisc(OrderDict, infoDict)
                    Catch ex As Exception
                        CType(Context("Errors"), ExpDictionary).Add(ex.Message, ex.Source)
                    End Try

                    Try
                        AXAPTAInitTradeAgreements(OrderDict, infoDict)
                    Catch ex As Exception
                        Context("Errors").Add(Guid.NewGuid(), ex.Message)
                        SetOrderDictOnError(OrderDict)
                        Return Nothing
                    End Try

                    Try
                        For Each olKey As KeyValuePair(Of Object, Object) In CType(OrderDict("Lines"), ExpDictionary)
                            CalcLine(OrderDict, OrderDict("Lines")(olKey.Key), olKey.Key, infoDict, Context)
                        Next
                    Catch ex As Exception
                        Context("Errors").Add(Guid.NewGuid(), ex.Message)
                        SetOrderDictOnError(OrderDict)
                        Return Nothing
                    End Try

                    Try
                        AXAPTATotalOrderDisc(OrderDict, infoDict)
                    Catch ex As Exception
                        Context("Errors").Add(Guid.NewGuid(), ex.Message)
                        SetOrderDictOnError(OrderDict)
                        Return Nothing
                    End Try

                    Try
                        AXAPTAOrderTotal(OrderDict, infoDict)
                    Catch ex As Exception
                        Context("Errors").Add(Guid.NewGuid(), ex.Message)
                        SetOrderDictOnError(OrderDict)
                        Return Nothing
                    End Try

                    ' Mark order as calculated
                    OrderDict("IsCalculated") = True
                    OrderDict("ReCalculated") = True

                End If
            End If
            Return Nothing
        End Function

        Private Sub SetOrderDictOnError(ByVal OrderDict As ExpDictionary)
            OrderDict("Total") = 0
            OrderDict("TotalInclTax") = 0
            OrderDict("InvoiceDiscount") = 0
            OrderDict("ServiceCharge") = 0
            OrderDict("IsCalculated") = False
            OrderDict("ReCalculated") = False
        End Sub

        Private Sub CalcLine(ByVal orderDict As ExpDictionary, ByVal orderline As Object, ByVal LineGuid As Object, ByVal infoDict As ExpDictionary, ByVal Context As ExpDictionary)
            Try
                AXAPTAInitLine(orderDict, orderline, infoDict, Context)
            Catch ex As Exception
                CType(Context("Errors"), ExpDictionary).Add(ex.Source, ex.StackTrace)
                Throw New Exception()
            End Try

            Try
                AXAPTALineDisc(orderDict, orderline, infoDict)
            Catch ex As Exception
                CType(Context("Errors"), ExpDictionary).Add(ex.Source, ex.StackTrace)
                Throw New Exception()
            End Try

            Try
                AXAPTALineAmount(orderDict)
            Catch ex As Exception
                CType(Context("Errors"), ExpDictionary).Add(ex.Source, ex.StackTrace)
                Throw New Exception()
            End Try

            Try
                AXAPTALineDiscountAmount(orderDict)
            Catch ex As Exception
                CType(Context("Errors"), ExpDictionary).Add(ex.Source, ex.StackTrace)
                Throw New Exception()
            End Try

            ' Mark order line as calculated
            orderline("IsCalculated") = True
        End Sub

        ' --
        ' Helper functions
        ' --

        ' --
        ' Get correct product quantities for line discount calculations.
        ' This is used to calculate the correct line discounts and prices
        ' accross lines with the same ProductGuid, with different Variants.
        ' Not the AX_COLLECT_QUANTITES_FOR_LINE_DISCOUNTS Configuration which
        ' Will disable this feature.
        ' --
        Private Function GetProductQuantities(ByVal orderDict As ExpDictionary) As ExpDictionary
            Dim item, retval As New ExpDictionary()

            For Each keyVal As KeyValuePair(Of Object, Object) In orderDict("Lines")
                item = orderDict("Lines")(keyVal.Key)

                If retval(item("ProductGuid")) Is Nothing Then
                    retval(item("ProductGuid")) = New ExpDictionary()
                    retval(item("ProductGuid"))("Variants") = New ExpDictionary()
                End If

                retval(item("ProductGuid"))("Total") = CDblEx(retval(item("ProductGuid"))("Total")) + item("Quantity")

                If CStrEx(item("VariantCode")) <> "" Then
                    retval(item("ProductGuid"))("Variants")(item("VariantCode")) = CDblEx(retval(item("ProductGuid"))("Variants")(item("VariantCode"))) + item("Quantity")
                End If
            Next

            Return retval
        End Function

        ' Cache relevant data from the PriceDisc table. 
        Function GetDictAXAPTA_PriceDisc(ByVal orderDict As ExpDictionary, ByVal infoDict As ExpDictionary) As ExpDictionary
            Dim SystemDate As Object
            Dim currentkey, lastKey As String
            Dim keycounter As Integer = 0

            SystemDate = infoDict("SystemDate")

            Dim sql As String = "SELECT * FROM AXAPTA_PriceDisc WHERE " & _
                    "((ItemCode = 0 AND ItemRelation IN (" & infoDict("ProductList") & ")) OR " & _
                    "(ItemCode = 1 AND ItemRelation IN (" & infoDict("ProductDiscountList") & ")) OR " & _
                    "(ItemCode = 2)) AND " & _
                    "(" & SafeDatetime(SystemDate) & " >= FromDate OR FromDate IS NULL)" & _
                    " AND (" & SafeDatetime(SystemDate) & " <= ToDate OR ToDate IS NULL)" & _
                    " AND Exchange = " & SafeString(orderDict("CurrencyGuid")) & _
                    " ORDER BY Relation, ItemCode, ItemRelation, AccountCode, AccountRelation, Exchange, QuantityAmount"

            currentkey = ""
            lastKey = ""
            Dim dictAXAPTA_PriceDisc As New ExpDictionary()

            Dim reader As SqlDataReader = GetDR(sql)
            While reader.Read
                currentkey = SafeString(reader("AccountCode")) & "," & _
                             SafeString(reader("ItemCode")) & "," & _
                             SafeString(reader("Relation")) & "," & _
                             SafeString(reader("AccountRelation")) & "," & _
                             SafeString(reader("ItemRelation"))
                If currentkey <> lastKey Then
                    keycounter = 0
                    dictAXAPTA_PriceDisc.Add(currentkey, New ExpDictionary())
                    lastKey = currentkey
                End If

                keycounter = keycounter + 1
                dictAXAPTA_PriceDisc(currentkey).Add(CStrEx(keycounter), SetDictionary(reader))

            End While
            reader.Close()

            Return dictAXAPTA_PriceDisc
        End Function

        Function GetProductsDict(ByVal infoDict As ExpDictionary) As ExpDictionary
            Dim sql As String = "SELECT * FROM ProductTable WHERE ProductGuid IN (" & infoDict("ProductList") & ")"
            Return Sql2Dictionaries(sql, New Object() {"ProductGuid"})
        End Function


        ' --
        ' Private functions
        ' --

        ' --
        ' Initiates values on the order. Loads values from customer record.
        ' --
        Sub AXAPTAInitOrder(ByVal orderDict As ExpDictionary, ByVal infoDict As ExpDictionary, ByVal Context As ExpDictionary)
            Dim CustomerDict As ExpDictionary

            orderDict("CustomerGuid") = Context("CustomerGuid")

            Dim sql As String = "SELECT * FROM CustomerTable WHERE CustomerGuid = " & SafeString(orderDict("CustomerGuid"))

            CustomerDict = Sql2Dictionary(sql)

            If CustomerDict Is Nothing OrElse CustomerDict.Count <= 0 Then
                Throw New Exception("Error initializing order - CustomerAccount [" & orderDict("CustomerGuid") & "] not found. ")
            End If

            ' Discount groups for this customer.
            orderDict("MultilnDisc") = CStrEx(CustomerDict("MultilnDisc"))
            orderDict("EndDisc") = CStrEx(CustomerDict("EndDisc"))

            orderDict("TaxGroup") = CStrEx(CustomerDict("TaxGroup"))
            orderDict("Vatdutiable") = CBool(orderDict("TaxGroup") <> "")

            ' This Tax Pct is used for the shipping and handling module.
            orderDict("TaxPct") = AppSettings("AXAPTA_VAT_PCT")
            orderDict("ServiceCharge") = 0
            orderDict("InvoiceDiscount") = 0
            If CStrEx(Context("CurrencyGuid")) <> "" Then
                If CBoolEx(AppSettings("AXAPTA_USE_CURRENCY_FROM_CUSTOMER")) Then
                    orderDict("CurrencyGuid") = CustomerDict("CurrencyGuid")
                Else
                    orderDict("CurrencyGuid") = Context("CurrencyGuid")
                End If
            Else
                orderDict("CurrencyGuid") = CustomerDict("CurrencyGuid")
            End If

            infoDict("CustomerRecord") = CustomerDict
        End Sub

        Sub AXAPTATotalOrderDisc(ByVal orderDict As ExpDictionary, ByVal infoDict As ExpDictionary)
            ' Calculates total discount, multiline discounts and fees

            Dim PDMAc, SystemDate, LRunning, PDMDisc, PDMi As Object
            Dim PDMDiscAmount, PDMPercent1, PDMPercent2, PDMBalance, PDMBalanceTotal As Decimal
            Dim currentkey, currMultilnDisc As Object
            Dim items(), keys(), orderline, PriceDiscItem As Object
            Dim key As KeyValuePair(Of Object, Object)
            Dim mulkeys, mulitems As Object
            Dim i, j, k, fAccountcode, fItemCode, PDMAcode As Integer
            Dim PDMReset, PDMFee, PDMEndfound As Boolean
            Dim fItemrelation As String = ""
            Dim fAccountrelation As String = ""
            Dim PDMArel As String = ""
            Dim multiLineSum, dictAXAPTA_PriceDisc As ExpDictionary

            items = CType(infoDict("PriceParameters"), ExpDictionary).DictItems
            keys = CType(infoDict("PriceParameters"), ExpDictionary).DictKeys

            PDMReset = False
            PDMFee = True

            SystemDate = infoDict("SystemDate")

            dictAXAPTA_PriceDisc = CType(infoDict("DictAXAPTA_PriceDisc"), ExpDictionary)

            '
            ' Multiline Discount
            '

            ' Find all MultiLine discount groups in use on lines.
            ' Sum the quantaties for these groups.
            ' Find the total quantity of all items.

            multiLineSum = New ExpDictionary()
            multiLineSum("Groups") = New ExpDictionary()

            For Each orderline In CType(orderDict("Lines"), ExpDictionary).Values
                currMultilnDisc = CStrEx(orderline("MultilnDisc"))

                If PDMReset Then
                    orderline("LineDiscount") = 0
                    orderline("LineDiscountAmount") = 0
                End If

                multiLineSum("Groups")(currMultilnDisc) = CDblEx(multiLineSum("Groups")(currMultilnDisc)) + orderline("Quantity")
            Next

            If multiLineSum("Groups").Count > 0 Then
                mulkeys = multiLineSum("Groups").DictKeys
                mulitems = multiLineSum("Groups").DictItems

                ' for each Multiline Discount group get the discount
                For i = 0 To multiLineSum("Groups").Count - 1
                    PDMDisc = mulkeys(i)
                    PDMBalance = mulitems(i)
                    PDMDiscAmount = 0
                    PDMPercent1 = 0
                    PDMPercent2 = 0
                    PDMi = 1

                    For j = 5 To 19 Step 7
                        For k = 0 To 1
                            If items(j + k - 1) Then
                                Select Case k
                                    Case 0
                                        ' Item Grp 1
                                        fItemCode = 1
                                        fItemrelation = mulkeys(i)
                                    Case 1
                                        ' All Items 2
                                        fItemCode = 2
                                        fItemrelation = ""
                                End Select

                                Select Case j
                                    Case 5
                                        ' CustomerAccount = 0
                                        fAccountcode = 0
                                        fAccountrelation = orderDict("CustomerGuid")
                                    Case 12
                                        ' MultiLineDisc = 1
                                        fAccountcode = 1
                                        fAccountrelation = orderDict("MultilnDisc")
                                    Case 19
                                        ' All = 2
                                        fAccountcode = 2
                                        fAccountrelation = ""
                                End Select

                                currentkey = SafeString(fAccountcode) & "," & _
                                             SafeString(fItemCode) & "," & _
                                             SafeString(6) & "," & _
                                             SafeString(fAccountrelation) & "," & _
                                             SafeString(fItemrelation)

                                If dictAXAPTA_PriceDisc(currentkey) IsNot Nothing Then
                                    For Each key In dictAXAPTA_PriceDisc(currentkey)
                                        PriceDiscItem = dictAXAPTA_PriceDisc(currentkey)(key.Key)

                                        If PDMBalance >= PriceDiscItem("QuantityAmount") Then

                                            PDMDiscAmount = PDMDiscAmount + PriceDiscItem("Amount")
                                            PDMPercent1 = PDMPercent1 + PriceDiscItem("Percent1")
                                            PDMPercent2 = PDMPercent2 + PriceDiscItem("Percent2")

                                            If Not CBoolEx(PriceDiscItem("LookforForward")) Then
                                                ' Stop search
                                                j = 26
                                                k = 2
                                            End If
                                        End If
                                    Next
                                End If
                            End If
                        Next
                    Next

                    PDMPercent1 = 100 * (1 - (1 - PDMPercent1 / 100) * (1 - PDMPercent2 / 100))

                    ' For each line in order - add the discount.
                    For Each orderline In orderDict("Lines").DictItems
                        If CStrEx(orderline("MultilnDisc")) = CStrEx(mulkeys(i)) Then
                            orderline("LineDiscount") = orderline("LineDiscount") + PDMPercent1
                            orderline("LineDiscountAmount") = orderline("LineDiscountAmount") + PDMDiscAmount

                            orderline("LineTotal") = _
                                orderline("Quantity") * (orderline("ListPrice") - orderline("LineDiscountAmount")) * (100 - orderline("LineDiscount")) / 100
                        End If
                    Next

                Next
            End If

            '
            ' Balance Total for use in Total discount
            '

            PDMBalance = 0

            For Each orderline In orderDict("Lines").DictItems
                If orderline("EndDisc") = "1" Then
                    PDMBalance = PDMBalance + orderline("LineTotal")
                    PDMEndfound = True
                End If
                PDMBalanceTotal = PDMBalanceTotal + orderline("LineTotal")
            Next

            '
            ' Total Order Discount
            '

            PDMDiscAmount = 0
            PDMPercent1 = 0
            PDMPercent2 = 0

            If PDMEndfound Then
                For i = 7 To 21 Step 7
                    Select Case i
                        Case 7
                            ' CustomerAccount 0
                            PDMAcode = 0
                            PDMArel = orderDict("CustomerGuid")
                        Case 14
                            ' EndDisc = 1
                            PDMAcode = 1
                            PDMArel = orderDict("EndDisc")
                        Case 21
                            ' All = 2
                            PDMAcode = 2
                            PDMArel = ""
                    End Select

                    If items(i - 1) Then
                        currentkey = SafeString(PDMAcode) & "," & _
                                     SafeString(2) & "," & _
                                     SafeString(7) & "," & _
                                     SafeString(PDMArel) & "," & _
                                     SafeString("")

                        LRunning = True

                        If dictAXAPTA_PriceDisc(currentkey) IsNot Nothing Then
                            For Each key In dictAXAPTA_PriceDisc(currentkey)
                                PriceDiscItem = dictAXAPTA_PriceDisc(currentkey)(key.Key)

                                If PDMBalance >= PriceDiscItem("QuantityAmount") Then
                                    If PriceDiscItem("Amount") <> 0 Then PDMDiscAmount = PDMDiscAmount + PriceDiscItem("Amount")
                                    If PriceDiscItem("Percent1") <> 0 Then PDMPercent1 = PDMPercent1 + PriceDiscItem("Percent1")
                                    If PriceDiscItem("Percent2") <> 0 Then PDMPercent2 = PDMPercent2 + PriceDiscItem("Percent2")

                                    If Not CBoolEx(PriceDiscItem("LookforForward")) Then
                                        PDMAc = 28
                                        LRunning = False
                                        Exit For
                                    End If
                                End If
                            Next
                        End If
                    End If
                Next
            End If

            PDMDiscAmount = PDMDiscAmount + (PDMBalance * (1 - (1 - PDMPercent1 / 100) * (1 - PDMPercent2 / 100)))

            '
            ' Total Fee or not fee
            '

            If PDMDiscAmount * PDMBalanceTotal > 0 Or Not PDMFee Then
                If PDMFee Then
                    orderDict("ServiceCharge") = 0
                End If
                If PDMBalanceTotal > 0 Then
                    orderDict("InvoiceDiscountPct") = 100 * PDMDiscAmount / PDMBalanceTotal
                    orderDict("InvoiceDiscount") = PDMDiscAmount
                Else
                    orderDict("InvoiceDiscountPct") = 0
                    orderDict("InvoiceDiscount") = 0
                End If
            Else
                orderDict("InvoiceDiscountPct") = 0
                orderDict("InvoiceDiscount") = 0
                orderDict("ServiceCharge") = -PDMDiscAmount
            End If
        End Sub

        Sub AXAPTAOrderTotal(ByVal orderDict As ExpDictionary, ByVal infoDict As ExpDictionary)
            Dim AmountSum, InvDiscSum, orderline, SystemDate, TaxPct, DiscTaxPct As Object
            Dim AmountSumVat As Object
            Dim CalcVatOnDisc As Boolean

            SystemDate = infoDict("SystemDate")

            AmountSum = 0
            AmountSumVat = 0
            InvDiscSum = 0
            TaxPct = 0
            orderDict("HasDiscount") = False ' Indicates if the order has discount items

            SetOrderLineVatPct(infoDict("ProductsDict"), orderDict("CustomerGuid").ToString(), orderDict)

            For Each orderline In orderDict("Lines").DictItems
                TaxPct = orderline("TaxPct")

                orderline("TaxAmount") = (orderline("LineTotal") * (TaxPct / 100))
                AmountSumVat = AmountSumVat + orderline("LineTotal") + orderline("TaxAmount")

                orderline("ListPriceInclTax") = (orderline("ListPrice") * (1 + TaxPct / 100))
                orderline("LineDiscountAmountInclTax") = (orderline("LineDiscountAmount") * (1 + (TaxPct / 100)))
                If CDblEx(orderline("LineDiscountAmountInclTax")) > 0 Then
                    orderDict("HasDiscount") = True
                End If
                orderline("TotalInclTax") = (orderline("LineTotal") + orderline("TaxAmount"))

                AmountSum = AmountSum + orderline("LineTotal")
            Next

            ' Sum of lines
            orderDict("SubTotal") = AmountSum
            orderDict("SubTotalInclTax") = AmountSumVat

            CalcVatOnDisc = AppSettings("AXAPTA_VAT_ON_TOTAL_DISCOUNT")

            If CalcVatOnDisc Then
                DiscTaxPct = AppSettings("AXAPTA_VAT_PCT")

                orderDict("InvoiceDiscountInclTax") = orderDict("InvoiceDiscount") * (1 + (DiscTaxPct / 100))
            Else
                orderDict("InvoiceDiscountInclTax") = orderDict("InvoiceDiscount")
            End If

            orderDict("ServiceChargeInclTax") = orderDict("ServiceCharge") * (1 + (AppSettings("AXAPTA_VAT_PCT") / 100))

            orderDict("Total") = AmountSum - orderDict("InvoiceDiscount") + orderDict("ServiceCharge")
            orderDict("TotalInclTax") = AmountSumVat - orderDict("InvoiceDiscountInclTax") + orderDict("ServiceChargeInclTax")

            orderDict("TaxAmount") = orderDict("TotalInclTax") - orderDict("Total")
        End Sub

        Sub AXAPTAInitTradeAgreements(ByVal orderDict As ExpDictionary, ByVal infoDict As ExpDictionary)
            ' AXAPTA
            Dim rsAXAPTA_Parameters As DataTable
            Dim sql As String
            Dim tempDict As New ExpDictionary()
            Dim tempDict2 As New ExpDictionary()

            sql = "SELECT * FROM AXAPTA_PriceParameters"

            rsAXAPTA_Parameters = SQL2DataTable(sql)

            If Not rsAXAPTA_Parameters.Rows.Count = 0 Then

                tempDict("SALESORDERPRICEACCOUNTITEM") = CLngEx(rsAXAPTA_Parameters.Rows(0).Item("SALESORDERPRICEACCOUNTITEM")).Equals(1)
                tempDict("SALESORDERLINEACCOUNTITEM") = CLngEx(rsAXAPTA_Parameters.Rows(0).Item("SALESORDERLINEACCOUNTITEM")).Equals(1)
                tempDict("SALESORDERLINEACCOUNTGROUP") = CLngEx(rsAXAPTA_Parameters.Rows(0).Item("SALESORDERLINEACCOUNTGROUP")).Equals(1)
                tempDict("SALESORDERLINEACCOUNTALL") = CLngEx(rsAXAPTA_Parameters.Rows(0).Item("SALESORDERLINEACCOUNTALL")).Equals(1)
                tempDict("SALESORDERMULTILNACCOUNTGROUP") = CLngEx(rsAXAPTA_Parameters.Rows(0).Item("SALESORDERMULTILNACCOUNTGROUP")).Equals(1)
                tempDict("SALESORDERMULTILNACCOUNTALL") = CLngEx(rsAXAPTA_Parameters.Rows(0).Item("SALESORDERMULTILNACCOUNTALL")).Equals(1)
                tempDict("SALESORDERENDACCOUNTALL") = CLngEx(rsAXAPTA_Parameters.Rows(0).Item("SALESORDERENDACCOUNTALL")).Equals(1)

                tempDict("SALESORDERPRICEGROUPITEM") = CLngEx(rsAXAPTA_Parameters.Rows(0).Item("SALESORDERPRICEGROUPITEM")).Equals(1)
                tempDict("SALESORDERLINEGROUPITEM") = CLngEx(rsAXAPTA_Parameters.Rows(0).Item("SALESORDERLINEGROUPITEM")).Equals(1)
                tempDict("SALESORDERLINEGROUPGROUP") = CLngEx(rsAXAPTA_Parameters.Rows(0).Item("SALESORDERLINEGROUPGROUP")).Equals(1)
                tempDict("SALESORDERLINEGROUPALL") = CLngEx(rsAXAPTA_Parameters.Rows(0).Item("SALESORDERLINEGROUPALL")).Equals(1)
                tempDict("SALESORDERMULTILNGROUPGROUP") = CLngEx(rsAXAPTA_Parameters.Rows(0).Item("SALESORDERMULTILNGROUPGROUP")).Equals(1)
                tempDict("SALESORDERMULTILNGROUPALL") = CLngEx(rsAXAPTA_Parameters.Rows(0).Item("SALESORDERMULTILNGROUPALL")).Equals(1)
                tempDict("SALESORDERENDGROUPALL") = CLngEx(rsAXAPTA_Parameters.Rows(0).Item("SALESORDERENDGROUPALL")).Equals(1)

                tempDict("SALESORDERPRICEALLITEM") = CLngEx(rsAXAPTA_Parameters.Rows(0).Item("SALESORDERPRICEALLITEM")).Equals(1)
                tempDict("SALESORDERLINEALLITEM") = CLngEx(rsAXAPTA_Parameters.Rows(0).Item("SALESORDERLINEALLITEM")).Equals(1)
                tempDict("SALESORDERLINEALLGROUP") = CLngEx(rsAXAPTA_Parameters.Rows(0).Item("SALESORDERLINEALLGROUP")).Equals(1)
                tempDict("SALESORDERLINEALLALL") = CLngEx(rsAXAPTA_Parameters.Rows(0).Item("SALESORDERLINEALLALL")).Equals(1)
                tempDict("SALESORDERMULTILNALLGROUP") = CLngEx(rsAXAPTA_Parameters.Rows(0).Item("SALESORDERMULTILNALLGROUP")).Equals(1)
                tempDict("SALESORDERMULTILNALLALL") = CLngEx(rsAXAPTA_Parameters.Rows(0).Item("SALESORDERMULTILNALLALL")).Equals(1)
                tempDict("SALESORDERENDALLALL") = CLngEx(rsAXAPTA_Parameters.Rows(0).Item("SALESORDERENDALLALL")).Equals(1)
            Else
                Throw New Exception("ExpandIT AXAPTA Business Logic " & Resources.Language.MESSAGE_INTERNAL_TRADEAGREEMENTS)
            End If

            ' --
            ' Trade Agreement search order...
            ' --

            ' Customer Account
            tempDict2("SALESORDERPRICEACCOUNTITEM") = "004"       'Price 1
            tempDict2("SALESORDERLINEACCOUNTITEM") = "005"        'Line  2
            tempDict2("SALESORDERLINEACCOUNTGROUP") = "015"       'Line  3
            tempDict2("SALESORDERLINEACCOUNTALL") = "025"         'Line  4
            tempDict2("SALESORDERMULTILNACCOUNTGROUP") = "016"    'Multi 5
            tempDict2("SALESORDERMULTILNACCOUNTALL") = "026"      'Multi 6
            tempDict2("SALESORDERENDACCOUNTALL") = "027"          'All (Incoice) 7

            ' Customer Group
            tempDict2("SALESORDERPRICEGROUPITEM") = "104"         'Price 8
            tempDict2("SALESORDERLINEGROUPITEM") = "105"          'Line 9
            tempDict2("SALESORDERLINEGROUPGROUP") = "115"         'Line 10
            tempDict2("SALESORDERLINEGROUPALL") = "125"           'Line 11
            tempDict2("SALESORDERMULTILNGROUPGROUP") = "116"      'Multi 12
            tempDict2("SALESORDERMULTILNGROUPALL") = "126"        'Multi 13
            tempDict2("SALESORDERENDGROUPALL") = "127"            'All (Incoice) 14

            ' All Customers
            tempDict2("SALESORDERPRICEALLITEM") = "204"           'Price 15
            tempDict2("SALESORDERLINEALLITEM") = "205"            'Line 16
            tempDict2("SALESORDERLINEALLGROUP") = "215"           'Line 17
            tempDict2("SALESORDERLINEALLALL") = "225"             'Line 18
            tempDict2("SALESORDERMULTILNALLGROUP") = "216"        'Multi 19
            tempDict2("SALESORDERMULTILNALLALL") = "226"          'Multi 20
            tempDict2("SALESORDERENDALLALL") = "227"              'All (Incoice) 21

            infoDict("PriceParameters") = tempDict
            infoDict("PriceMatrix") = tempDict2
            infoDict("BackEnd") = "AXAPTA"

        End Sub

        ' --
        '  Line functions
        ' --

        Sub AXAPTAInitLine(ByVal orderDict As ExpDictionary, ByVal orderline As ExpDictionary, _
                                                        ByVal infoDict As ExpDictionary, ByVal Context As ExpDictionary)
            Dim ProductDict As ExpDictionary

            ProductDict = infoDict("ProductsDict")(SafeString(orderline("ProductGuid")))

            If ProductDict Is Nothing Then
                CType(orderDict("Lines"), ExpDictionary).Remove(orderline("LineGuid"))
                Throw New Exception(Resources.Language.MESSAGE_INTERNAL_PRODUCTGUID & " [" & orderline("ProductGuid") & "] " & Resources.Language.MESSAGE_IS_NOT_AVAILABLE)
            End If

            orderline("CurrencyGuid") = orderDict("CurrencyGuid")

            ' Make sure that currency is the same on all lines on order.
            orderline("ListPrice") = AxaptaAmount(globals.currency.ConvertCurrency(ProductDict("ListPrice"), Context("DefaultCurrency"), orderline("CurrencyGuid")))

            orderline("LineDiscGrp") = ProductDict("LineDisc")
            orderline("MultilnDisc") = ProductDict("MultilnDisc")
            orderline("EndDisc") = ProductDict("EndDisc")

            orderline("TaxItemGroupID") = ProductDict("TaxItemGroupID")

            orderline("LineDiscount") = 0
            orderline("LineDiscountAmount") = 0
            orderline("LineTotal") = 0
        End Sub

        Sub AXAPTALineDisc(ByVal orderDict As ExpDictionary, ByVal orderline As ExpDictionary, ByVal infoDict As ExpDictionary)

            ' AXAPTA/Axapta tradeagreement description
            ' 
            '                      ACCOUNT NO.        ACCOUNT GROUP       ALL ACCOUNTS
            '                    Item Grp  All       Item Grp   All      Item Grp   All
            '  SALES
            '  +------------------------------------------------------------------------+
            '  +Price........:  1( )                8( )               15( )            +
            '  +Line disc....:  2( ) 3( ) 4( )      9( )10( )11( )     16( )17( )18( )  +
            '  +Multi disc...:       5( ) 6( )          12( )13( )          19( )20( )  +
            '  +Total disc...:            7( )               14( )               21( )  +
            '  +------------------------------------------------------------------------+
            '                    Search direction ---->
            ' 

            Dim TradeAgrValue, running, PDel As Boolean
            Dim fAccountcode As String = ""
            Dim fItemCode As String = ""
            Dim fRelation As String = ""
            Dim fAccountrelation As String = ""
            Dim fItemrelation As String = ""
            Dim currentkey, TradeArgName As String
            Dim SystemDate, ProductTotalQuantity, TradeAgrItem, tmpAmount As Object
            Dim key As KeyValuePair(Of Object, Object)
            Dim PDPercent1, PDPercent2, i, j, PLook, LLook As Integer
            Dim items(), keys() As Object

            Dim ProductDict As ExpDictionary = infoDict("ProductsDict")(SafeString(orderline("ProductGuid")))
            Dim CustomerDict As ExpDictionary = infoDict("CustomerRecord")

            Dim ProductQuantities As ExpDictionary = infoDict("ProductQuantities")
            Dim dictAXAPTA_PriceDisc As ExpDictionary = infoDict("DictAXAPTA_PriceDisc")

            PDPercent1 = 0
            PDPercent2 = 0
            PLook = 1
            LLook = 1

            items = CType(infoDict("PriceParameters"), ExpDictionary).DictItems
            keys = CType(infoDict("PriceParameters"), ExpDictionary).DictKeys

            If ProductDict IsNot Nothing And CustomerDict IsNot Nothing Then
                ' Customer Account No.

                SystemDate = infoDict("SystemDate")
                running = True

                For i = 1 To 15 Step 7
                    For j = 0 To 3
                        TradeArgName = keys((i + j) - 1)
                        TradeAgrValue = items((i + j) - 1)

                        If TradeAgrValue Then
                            fAccountcode = Mid(infoDict("PriceMatrix")(TradeArgName), 1, 1)
                            fItemCode = Mid(infoDict("PriceMatrix")(TradeArgName), 2, 1)
                            fRelation = Mid(infoDict("PriceMatrix")(TradeArgName), 3, 1)

                            Select Case fAccountcode
                                Case "0"
                                    ' CustomerGuid
                                    fAccountrelation = DBNull2Nothing(CustomerDict("CustomerGuid"))
                                Case "1"
                                    ' CustomerGroup
                                    Select Case fRelation
                                        Case 4
                                            ' Price
                                            fAccountrelation = DBNull2Nothing(CustomerDict("PriceGroup"))
                                        Case 5
                                            ' Line
                                            fAccountrelation = DBNull2Nothing(CustomerDict("LineDisc"))
                                    End Select
                                Case "2"
                                    ' All
                                    fAccountrelation = ""
                            End Select

                            Select Case fItemCode
                                Case "0"
                                    ' ItemNumber
                                    fItemrelation = DBNull2Nothing(ProductDict("ProductGuid"))
                                Case "1"
                                    ' ItemGroup
                                    fItemrelation = DBNull2Nothing(ProductDict("LineDisc"))
                                Case "2"
                                    ' All
                                    fItemrelation = ""
                            End Select

                            currentkey = SafeString(fAccountcode) & "," & _
                                         SafeString(fItemCode) & "," & _
                                         SafeString(fRelation) & "," & _
                                         SafeString(fAccountrelation) & "," & _
                                         SafeString(fItemrelation)

                            If dictAXAPTA_PriceDisc(currentkey) IsNot Nothing Then
                                For Each key In dictAXAPTA_PriceDisc(currentkey)
                                    TradeAgrItem = dictAXAPTA_PriceDisc(currentkey)(key.Key)
                                    If PDel Then
                                        PDel = False
                                        If fRelation = 4 Then
                                            orderline("ListPrice") = 0
                                        ElseIf fRelation = 5 Then
                                            orderline("LineDiscountAmount") = 0
                                        End If

                                    End If

                                    If AppSettings("AX_COLLECT_QUANTITES_FOR_LINE_DISCOUNTS") Then
                                        ProductTotalQuantity = CDblEx(ProductQuantities(orderline("ProductGuid"))("Total"))
                                    Else
                                        ProductTotalQuantity = orderline("Quantity")
                                    End If

                                    If TradeAgrItem("QuantityAmount") <= ProductTotalQuantity Then
                                        If (fRelation = 4) And PLook Then
                                            ' fRelation 4 = Sales(Price)
                                            If CBoolEx(TradeAgrItem("PriceUnit")) Then
                                                tmpAmount = TradeAgrItem("Amount") / TradeAgrItem("PriceUnit")
                                            Else
                                                tmpAmount = TradeAgrItem("Amount")
                                            End If

                                            If CBoolEx(TradeAgrItem("Amount")) And (tmpAmount < orderline("ListPrice") Or Not CBoolEx(orderline("ListPrice"))) Then
                                                If Not CBoolEx(TradeAgrItem("PriceUnit")) Then
                                                    orderline("PriceUnit") = 1
                                                Else
                                                    orderline("PriceUnit") = TradeAgrItem("PriceUnit")
                                                End If

                                                orderline("ListPrice") = TradeAgrItem("Amount") / orderline("PriceUnit")

                                                If CBoolEx(TradeAgrItem("DeliveryTime")) Then
                                                    If (SystemDate + TradeAgrItem("DeliveryTime")) > orderline("ConfirmedDel") Then
                                                        orderline("ConfirmedDel") = SystemDate + TradeAgrItem("DeliveryTime")
                                                    End If
                                                End If
                                            End If
                                        ElseIf (fRelation = 5) And LLook Then
                                            ' fRelation 5 = Sales(Line Disc.)
                                            orderline("LineDiscountAmount") = orderline("LineDiscountAmount") + TradeAgrItem("Amount")
                                            PDPercent1 = PDPercent1 + TradeAgrItem("Percent1")
                                            PDPercent2 = PDPercent2 + TradeAgrItem("Percent2")
                                        End If
                                    End If

                                    If TradeAgrItem("LookforForward") <> 1 Then
                                        PLook = PLook And Not (fRelation = 4)
                                        LLook = LLook And Not (fRelation = 5)
                                        Exit For
                                    End If
                                Next
                            End If
                        End If
                    Next

                Next

                If Not PDel Then
                    orderline("LineDiscount") = 100 * (1 - (1 - PDPercent1 / 100) * (1 - PDPercent2 / 100))
                End If

                If orderline("ListPrice") = 0 Then
                    orderline("ListPrice") = ProductDict("ListPrice")
                End If
            Else
                If ProductDict Is Nothing Then
                    Throw New Exception(Resources.Language.MESSAGE_INTERNAL_PRODUCTGUID & " [" & orderline("ProductGuid") & "] " & Resources.Language.MESSAGE_IS_NOT_AVAILABLE)
                Else
                    Throw New Exception("CustomerAccount [" & orderDict("CustomerGuid") & "] not found. ")
                End If
            End If
        End Sub

        Sub AXAPTALineAmount(ByVal orderline As ExpDictionary)
            ' SalesLineAmount

            If orderline("ListPrice") <> 0 Then
                orderline("LineTotal") = _
                    orderline("Quantity") * (orderline("ListPrice") - orderline("LineDiscountAmount")) * (100 - orderline("LineDiscount")) / 100
            End If
        End Sub

        Sub AXAPTALineDiscountAmount(ByVal orderline As ExpDictionary)
            orderline("LineDiscountAmount") = orderline("LineDiscountAmount")
        End Sub

        Function AxaptaAmount(ByVal val As Decimal) As Decimal
            Return RoundEx(val, 2)
        End Function

        ' -- Axapta VAT Calculations --

        ''' <summary> 
        ''' Sets the vat percent on each orderline 
        ''' </summary> 
        ''' <param name="dictProduct">Dictionary with all products in the order</param> 
        ''' <param name="customerGuid"></param> 
        ''' <param name="dictOrder">The order dictionary</param>
        Public Sub SetOrderLineVatPct(ByVal dictProduct As ExpDictionary, ByVal customerGuid As String, ByRef dictOrder As ExpDictionary)
            Dim productGuid2SQL As New List(Of String)()

            ' Prepares a list with productguids 
            For Each key As String In dictProduct.Keys
                If Not productGuid2SQL.Contains(key) Then
                    productGuid2SQL.Add(key)
                End If
            Next

            ' 1. Get table with all taxcodes, productguids, taxpct and limits 
            ' Gets datatable with all vat codes for each product 
            Dim dtProducts As DataTable = GetProductTaxCodes(productGuid2SQL.ToArray(), customerGuid)

            ' 2. Summarized LineTotal for each productguid in order dictionary 
            ' Get new dictionary with amount summarized for each product 
            Dim dictProductAmount As Dictionary(Of String, Decimal) = SumProductAmount(dictOrder)

            ' 3. Creates a dictionary with taxcodes and taxpct 
            Dim dictTaxCodePct As New Dictionary(Of String, Decimal)()

            Dim lastTaxCode As String = Nothing
            Dim totalProductTaxCodeAmount As Decimal = 0, lastMinTaxAmount As Decimal = 0, lastMaxTaxAmount As Decimal = 0, lastTaxValue As Decimal = 0, totalTaxAmount As Decimal = 0
            For i As Integer = 0 To dtProducts.Rows.Count - 1
                Dim dr As DataRow = dtProducts.Rows(i)

                ' Checks if the vat code has changed and has been set 
                If Not dr("TaxCode").ToString().Equals(lastTaxCode) Then
                    ' Adds vat percent for the current vat code to the dictionary holding taxcodes and taxpct 
                    ' This is not nessesary for the first item, because we want the summarized totalProductTaxCodeAmount 
                    If i <> 0 Then
                        'totalTaxAmount += CalcTaxAmount(totalProductTaxCodeAmount, lastMinTaxAmount, lastMaxTaxAmount, lastTaxValue); 
                        dictTaxCodePct.Add(lastTaxCode, CalcTaxPct(totalProductTaxCodeAmount, lastMinTaxAmount, lastMaxTaxAmount, lastTaxValue))
                    End If

                    ' Stores old values 
                    lastMinTaxAmount = Decimal.Parse(dr("TaxLimitMin").ToString())
                    lastMaxTaxAmount = Decimal.Parse(dr("TaxLimitMax").ToString())
                    lastTaxValue = Decimal.Parse(dr("TaxValue").ToString())
                    lastTaxCode = dr("TaxCode").ToString()
                End If

                ' Adds the total amount for the current product 
                If dictProductAmount.ContainsKey(dr("ProductGuid").ToString()) Then
                    totalProductTaxCodeAmount += dictProductAmount(dr("ProductGuid").ToString())
                End If

                ' Adds vatpct for the last vat code to the dictionary 
                If i = (dtProducts.Rows.Count - 1) Then
                    'totalTaxAmount += CalcTaxAmount(totalProductTaxCodeAmount, lastMinTaxAmount, lastMaxTaxAmount, lastTaxValue); 
                    dictTaxCodePct.Add(lastTaxCode, CalcTaxPct(totalProductTaxCodeAmount, lastMinTaxAmount, lastMaxTaxAmount, lastTaxValue))
                End If
            Next

            ' 4. Set the tax pct for each order line i orderDict 
            For Each dr As DataRow In dtProducts.Rows
                SetOrderLineTaxPct(dictTaxCodePct(dr("TaxCode").ToString()), dr("ProductGuid").ToString(), dictOrder)
            Next

            'return totalTaxAmount; 
        End Sub

        ''' <summary> 
        ''' Sets the vat percent for each line in the order dictionary 
        ''' </summary> 
        ''' <param name="taxPct">vat percent for the tax code</param> 
        ''' <param name="productGuid"></param> 
        ''' <param name="dictOrder">the full order dictionary</param> 
        Private Sub SetOrderLineTaxPct(ByVal taxPct As Decimal, ByVal productGuid As String, ByRef dictOrder As ExpDictionary)
            Dim _dictLine As Dictionary(Of Object, Object)
            Dim _dictOrderLines As Dictionary(Of Object, Object) = DirectCast(dictOrder("Lines"), Dictionary(Of Object, Object))

            For Each key As String In _dictOrderLines.Keys
                _dictLine = DirectCast(_dictOrderLines(key), Dictionary(Of Object, Object))

                If _dictLine("ProductGuid").ToString().Equals(productGuid) Then
                    Dim oldTaxPct As Decimal
                    If Decimal.TryParse(_dictLine("TaxPct").ToString(), oldTaxPct) Then
                        ' Adds vat percentage 
                        _dictLine("TaxPct") = oldTaxPct + taxPct
                    End If
                End If
            Next
        End Sub

        ''' <summary> 
        ''' Calculates the vat amount for a tax code 
        ''' </summary> 
        ''' <param name="totalProductTaxCodeAmount"></param> 
        ''' <param name="lastMinTaxAmount"></param> 
        ''' <param name="lastMaxTaxAmount"></param> 
        ''' <param name="lastTaxValue"></param> 
        ''' <returns></returns> 
        Private Function CalcTaxAmount(ByVal totalProductTaxCodeAmount As Decimal, ByVal lastMinTaxAmount As Decimal, ByVal lastMaxTaxAmount As Decimal, ByVal lastTaxValue As Decimal) As Decimal
            Dim taxAmount As Decimal = 0
            ' Checks if the vat amount is in the range for adding vat to the order 
            If totalProductTaxCodeAmount > lastMinTaxAmount AndAlso (totalProductTaxCodeAmount < lastMaxTaxAmount OrElse lastMaxTaxAmount = 0) Then
                taxAmount = totalProductTaxCodeAmount * (lastTaxValue / 100)
            End If
            Return taxAmount
        End Function

        ''' <summary> 
        ''' Calculates the vat percent for a tax code 
        ''' </summary> 
        ''' <param name="totalProductTaxCodeAmount"></param> 
        ''' <param name="lastMinTaxAmount"></param> 
        ''' <param name="lastMaxTaxAmount"></param> 
        ''' <param name="lastTaxValue"></param> 
        ''' <returns></returns> 
        Private Function CalcTaxPct(ByVal totalProductTaxCodeAmount As Decimal, ByVal lastMinTaxAmount As Decimal, ByVal lastMaxTaxAmount As Decimal, ByVal lastTaxValue As Decimal) As Decimal
            Dim taxPct As Decimal = 0
            ' Checks if the vat amount is in the range for adding vat to the order 
            If totalProductTaxCodeAmount > lastMinTaxAmount AndAlso (totalProductTaxCodeAmount < lastMaxTaxAmount OrElse lastMaxTaxAmount = 0) Then
                taxPct = lastTaxValue
            End If
            Return taxPct
        End Function

        ''' <summary> 
        ''' Summarizes the LineTotal for each productguid in the order dictionary 
        ''' </summary> 
        ''' <param name="dictOrder"></param> 
        ''' <returns>Dictionary with productguid as key and total amount as value</returns> 
        Private Function SumProductAmount(ByRef dictOrder As ExpDictionary) As Dictionary(Of String, Decimal)
            Dim _dictLine As ExpDictionary
            Dim _dictOrderLines As ExpDictionary = DirectCast(dictOrder("Lines"), ExpDictionary)
            Dim dictProductAmount As New Dictionary(Of String, Decimal)()

            For Each key As String In _dictOrderLines.Keys
                _dictLine = DirectCast(_dictOrderLines(key), ExpDictionary)

                ' Clear previous vat amount and vat pct 
                _dictLine("TaxPct") = 0
                _dictLine("TaxAmount") = 0

                ' Checks if the productguid allready is present in the dictionary 
                If dictProductAmount.ContainsKey(_dictLine("ProductGuid").ToString()) Then
                    dictProductAmount(_dictLine("ProductGuid").ToString()) += Decimal.Parse(_dictLine("LineTotal").ToString())
                Else
                    dictProductAmount.Add(_dictLine("ProductGuid").ToString(), Decimal.Parse(_dictLine("LineTotal").ToString()))
                End If
            Next
            Return dictProductAmount
        End Function

        ''' <summary> 
        ''' Datatable with all nessasary tax values 
        ''' </summary> 
        ''' <param name="productGuids"></param> 
        ''' <param name="customerGuid"></param> 
        ''' <returns></returns> 
        Private Function GetProductTaxCodes(ByVal productGuids As String(), ByVal customerGuid As String) As DataTable
            Dim taxCodes As New List(Of String)()
            Dim sql As String = "SELECT ProductTable.ProductGuid, AXAPTA_TaxOnItem.TaxCode, AXAPTA_TaxData.TaxLimitMax, AXAPTA_TaxData.TaxLimitMin, AXAPTA_TaxData.TaxValue " & _
            "FROM ProductTable " & "INNER JOIN AXAPTA_TaxOnItem ON (ProductTable.TaxItemGroupId = AXAPTA_TaxOnItem.TaxItemGroup) " & _
            "INNER JOIN AXAPTA_TaxGroupData ON (AXAPTA_TaxGroupData.TaxCode = AXAPTA_TaxOnItem.TaxCode) " & _
            "INNER JOIN CustomerTable ON (AXAPTA_TaxGroupData.TaxGroup = CustomerTable.TaxGroup) " & _
            "INNER JOIN AXAPTA_TaxData ON (AXAPTA_TaxData.TaxCode = AXAPTA_TaxOnItem.TaxCode) " & _
            "WHERE ProductTable.ProductGuid IN (" & String.Join(",", productGuids) & ") " & _
            "AND CustomerTable.CustomerGuid = " & SafeString(customerGuid) & " " & _
            "AND (" & SafeDatetime(DateTime.Today.ToShortDateString()) & _
            " >= AXAPTA_TaxData.TaxFromDate OR AXAPTA_TaxData.TaxFromDate IS NULL) " & _
            "AND (" & SafeDatetime(DateTime.Today.ToShortDateString()) & _
            " <= AXAPTA_TaxData.TaxToDate OR AXAPTA_TaxData.TaxToDate IS NULL) " & _
            "ORDER BY AXAPTA_TaxOnItem.TaxCode, ProductTable.ProductGuid "

            Return SQL2DataTable(sql)
        End Function

    End Class

End Namespace