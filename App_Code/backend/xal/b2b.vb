Imports System.Configuration.ConfigurationManager
Imports Microsoft.VisualBasic
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

Namespace ExpandIT

    Public Class B2BXALClass
        Inherits B2BBaseClass

        Public Sub New(ByVal _globals As GlobalsClass)
            MyBase.New(_globals)
        End Sub

        Public Overrides Sub DecodeAddress(ByRef dict As Object)
            Dim sql, strCountryName As String
            Dim retval As ExpDictionary
            ' For performance reasons on heavy loaded systems the 
            ' Country table might be cached in the application object.
            ' Changing the following: Make the change for C5 as well.

            ' On XAL systems and C5 systems the CustomerTable holds
            ' the CountryName in the CountryGuid field. This piece of
            ' of code replaces the name with the Guid. 
            ' This funciton is used 2 places in lib_user.LoadUser.
            ' Get the name, held in the Guid field
            strCountryName = CStrEx(dict("CountryGuid"))
            sql = "SELECT CountryGuid FROM CountryTable WHERE CountryName = " & SafeString(strCountryName)
            retval = Sql2Dictionary(sql)
            ' Replace name with guid
            dict("CountryGuid") = retval("CountryGuid")
            ' Close the object used
            retval = Nothing
        End Sub


        ' ---------------------------------------------------------------
        ' Load the customer ledger entries for a selected customer.
        ' ---------------------------------------------------------------
        Public Overrides Function GetCustomerLedgerEntries(ByVal User As ExpDictionary) As ExpDictionary
            Dim sql As String
            Dim retval As ExpDictionary
            Dim dicLedgerEntry, dicOpenEntries As ExpDictionary

            ' Select the related customer, the customer table contains some calculated fields of interest
            sql = "SELECT * FROM CustomerTable WHERE CustomerGuid = '" & User("CustomerGuid") & "'"
            retval = Sql2Dictionary(sql)
            ' Select the ledger entries for the customer.
            sql = "SELECT * FROM CustomerLedgerEntry WHERE CustomerGuid = '" & User("CustomerGuid") & "' ORDER BY PostingDate,EntryGuid"
            On Error Resume Next
            retval("Lines") = SQL2Dicts(sql, "EntryGuid")
            sql = "SELECT * FROM CustomerLedgerEntry WHERE CustomerGuid = " & SafeString(User("CustomerGuid")) & " AND IsOpen = 1"

            dicOpenEntries = SQL2Dicts(sql, "EntryGuid")
            ' Calculate totals
            retval("BalanceAmountLCY") = 0
            retval("BalanceDueLCY") = 0
            For Each dicLedgerEntry In dicOpenEntries.Values
                retval("BalanceAmountLCY") = retval("BalanceAmountLCY") + dicLedgerEntry.Item("Amount")
                If (Now > dicLedgerEntry.Item("DueDate")) Then
                    retval("BalanceDueLCY") = retval("BalanceDueLCY") + dicLedgerEntry.Item("Amount")
                End If
            Next
            If Err.Number <> 0 Then
                HttpContext.Current.Response.Write("<b>An error occured.</b><br />Please ensure that you have extracted the Customer Ledger Entry " & _
                    "table.<br /><br />" & _
                    "<b>The following error was returned:</b> <br />" & vbCrLf & Err.Description & "(" & Err.Number & ")")
                HttpContext.Current.Response.End()
            End If
            On Error GoTo 0
            Return retval
        End Function

        ' ---------------------------------------------------------------
        ' Returns the document type description for a document type id.
        ' Blank is returned if the type id wasn't found.
        ' ---------------------------------------------------------------
        Public Overrides Function GetDocumentTypeDescription(ByVal lngType As Integer) As String
            Dim arrTypes As String() = {"", "Ompostering", "Ordre", "Indk&oslash;b", "Lager", "Produktion", "Sag", "Renter", "Debitor", "Kursregulering", "Summeret", "L&oslash;n", "Finansiel", "Diverse", "Rykker", "Afrunding"}

            ' Set the return value if indexes aren't out of bounds.
            If LBound(arrTypes) <= lngType And UBound(arrTypes) >= lngType Then
                Return arrTypes(lngType)
            Else
                Return ""
            End If
        End Function

    End Class

End Namespace
