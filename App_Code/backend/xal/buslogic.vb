﻿Imports System.Configuration.ConfigurationManager
Imports Microsoft.VisualBasic
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Data.SqlClient
Imports System.Data

Namespace ExpandIT

    Public Class BuslogicXALClass
        Inherits BuslogicBaseClass

        Public Sub New(ByVal _globals As GlobalsClass)
            MyBase.New(_globals)
        End Sub

        Public Overrides Function CalcOrder(ByVal OrderDict As ExpDictionary, ByVal Context As ExpDictionary) As String
            Dim i As Integer
            Dim olKeys As Object
            Dim infoDict As ExpDictionary

            If CType(OrderDict("Lines"), ExpDictionary).Count = 0 Then
                'OrderDict is empty - leave
                Return ""
            End If

            If CBoolEx(OrderDict("IsCalculated")) Then
                ' No need to recalculate order - leave
                Return ""
            End If

            infoDict = New ExpDictionary()

            Try
                XALInitOrder(OrderDict, infoDict, Context)
                XALInitTradeAgreements(OrderDict, infoDict)
                olKeys = CType(OrderDict("Lines"), ExpDictionary).DictKeys
                For i = 0 To OrderDict("Lines").Count - 1
                    CalcLine(OrderDict, OrderDict("Lines")(olKeys(i)), olKeys(i), infoDict, Context)
                Next
                XALTotalOrderDisc(OrderDict, infoDict)
                XALOrderTotal(OrderDict, infoDict)
                REM -- Mark order as calculated
                OrderDict("IsCalculated") = True
                OrderDict("ReCalculated") = True
            Catch ex As Exception
                cleanUpOnError(OrderDict, Context, ex)
                Return Nothing
            End Try

            Return Nothing

        End Function

        Sub cleanUpOnError(ByVal OrderDict As ExpDictionary, ByVal Context As ExpDictionary, ByVal ex As Exception)
            OrderDict("Total") = 0
            OrderDict("TotalInclTax") = 0
            OrderDict("InvoiceDiscount") = 0
            OrderDict("ServiceCharge") = 0
            If Not CType(Context("Errors"), ExpDictionary).ContainsKey(ex.Source) Then
                Context("Errors").Add(ex.Source, ex.Message)
            End If
        End Sub


        Function CalcLine(ByVal OrderObject As ExpDictionary, ByVal orderLine As ExpDictionary, ByVal LineGuid As Object, ByVal infoDict As ExpDictionary, ByVal Context As ExpDictionary) As Object
            Try
                XALInitLine(OrderObject, orderLine, infoDict)
                XALLineDisc(OrderObject, orderLine, infoDict)
                XALLineAmount(OrderObject, orderLine, infoDict)
                XALLineDiscountAmount(OrderObject, orderLine, infoDict)
            Catch ex As Exception
                Context("Errors").Add(ex.Source, ex.Message)
            End Try
            Return Nothing
        End Function

        REM --
        REM -- Private functions
        REM --

        Sub XALInitOrder(ByVal orderDict As ExpDictionary, ByVal infoDict As ExpDictionary, ByVal Context As ExpDictionary)
            Dim rsCust As SqlDataReader
            Dim CustNo As String
            Dim sql As String
            REM XAL Business logic Settings
            infoDict("XALVatOnTotalDiscount") = AppSettings("XAL_VAT_ON_TOTAL_DISCOUNT")
            infoDict("XALResetLineDiscounts") = AppSettings("XAL_RESET_LINE_DISCOUNTS")
            infoDict("XALNegDiscAsFee") = AppSettings("XAL_NEG_DISC_AS_FEE")
            infoDict("XALUseInvoiceToAccount") = AppSettings("XAL_USE_INVOICE_TO_ACCOUNT")

            rsCust = GetDR("SELECT CustomerGuid, Invoiceaccount, Multilndisc, Enddisc, Vatdutiable, VatCode, CurrencyGuid FROM CustomerTable WHERE CustomerGuid = " & SafeString(Context("CustomerGuid")))
            If Not rsCust.Read() Then
                rsCust.Close()
                Throw New Exception("Error initializing order - CustomerAccount [" & Context("CustomerGuid") & "] not found. ")
            End If

            If infoDict("XALUseInvoiceToAccount") And (Trim(CStrEx(rsCust("Invoiceaccount"))) <> "") Then
                CustNo = CStrEx(rsCust("Invoiceaccount"))
                sql = "SELECT * FROM CustomerTable WHERE CustomerGuid = " & SafeString(CustNo)
                rsCust.Close()

                rsCust = GetDR(sql) 'conn.execute(sql)

                If Not rsCust.Read() Then
                    rsCust.Close()
                    Throw New Exception("Error initializing order - Invoice to CustomerAccount [" & Trim(CustNo) & "] not found. ")
                End If
            End If

            orderDict("CustomerGuid") = rsCust("CustomerGuid")
            orderDict("Multilndisc") = rsCust("Multilndisc")
            orderDict("Enddisc") = rsCust("Enddisc")
            orderDict("Vatdutiable") = rsCust("Vatdutiable")
            orderDict("VatCode") = rsCust("VatCode")

            REM -- TaxPct used for the shipping and handling module
            orderDict("TaxPct") = XALGetTaxPct(orderDict("VatCode"), infoDict)
            orderDict("ServiceCharge") = 0
            orderDict("InvoiceDiscount") = 0

            REM -- Set currency according to rules in web.config
            If AppSettings("XAL_USE_CURRENCY_FROM_BILLTO_CUSTOMER") Then
                orderDict("CurrencyGuid") = rsCust("CurrencyGuid")
            Else
                orderDict("CurrencyGuid") = CStrEx(Context("CurrencyGuid"))
            End If

            If CStrEx(orderDict("CurrencyGuid")) = "" Then orderDict("CurrencyGuid") = Context("DefaultCurrency")
            infoDict("CustomerRecord") = SetDictionary(rsCust)
            rsCust.Close()

        End Sub

        Sub XALTotalOrderDisc(ByVal orderDict As ExpDictionary, ByVal infoDict As ExpDictionary)
            REM Calculates total discount, multiline discounts and fees

            Dim PDMAc, PDMDiscAmount, PDMPercent1, PDMPercent2 As Decimal
            Dim PDMDisc, PDMi As Object
            Dim SystemDate As Object
            Dim PDMArel As String
            Dim LRunning As Boolean
            Dim multiLineSum As ExpDictionary
            Dim fItemCode As String
            Dim fAccountcode As String
            Dim PDMAllItem As Boolean
            Dim PDMEndfound As Boolean
            Dim PDMBalance, PDMBalanceTotal As Decimal
            Dim TotalDiscPct As Decimal
            Dim i, j, k As Integer
            Dim items As Object()
            Dim keys As Object()
            Dim mulkeys As Object()
            Dim mulitems As Object()

            Dim rsPriceDisc As SqlDataReader
            Dim sql As String

            items = CType(infoDict("PriceParameters"), ExpDictionary).DictItems
            keys = CType(infoDict("PriceParameters"), ExpDictionary).DictKeys

            SystemDate = Now() 'Date

            multiLineSum = New ExpDictionary()

            REM
            REM Multiline Discount
            REM

            REM Find all MultiLine discount groups in use on lines.

            For Each orderLine As ExpDictionary In CType(orderDict("Lines"), ExpDictionary).Values
                If Not String.IsNullOrEmpty(CStrEx(orderLine("Multilndisc"))) Then
                    If infoDict("XALResetLineDiscounts") Then
                        orderLine("LineDiscount") = 0
                        orderLine("LineDiscountAmount") = 0
                    End If

                    multiLineSum(orderLine("Multilndisc")) = multiLineSum(orderLine("Multilndisc")) + orderLine("Quantity")
                End If
            Next

            If multiLineSum.Count > 0 Then

                mulkeys = multiLineSum.DictKeys
                mulitems = multiLineSum.DictItems

                For i = 0 To multiLineSum.Count - 1
                    PDMDisc = mulkeys(i)
                    PDMDiscAmount = 0
                    PDMPercent1 = 0
                    PDMPercent2 = 0
                    PDMi = 1

                    REM for each Multiline Discount group get the discount

                    For j = 5 To 19 Step 7
                        For k = 0 To 1
                            If items(j + k - 1) Then
                                Select Case k
                                    Case 0
                                        REM -- "0" = Item Grp 1
                                        fItemCode = "ItemCode = 1 AND ItemRelation = " & SafeString(mulkeys(i))
                                    Case 1
                                        REM -- "1" = All Items 2
                                        fItemCode = "ItemCode = 2 AND ItemRelation is null"
                                    Case Else
                                        fItemCode = ""
                                End Select

                                Select Case j
                                    Case 5
                                        REM -- CustomerAccount = 0
                                        fAccountcode = "AccountCode = 0 AND AccountRelation = " & SafeString(orderDict("CustomerGuid"))
                                    Case 12
                                        REM -- MultiLineDisc = 1
                                        fAccountcode = "AccountCode = 1 AND AccountRelation = " & SafeString(orderDict("Multilndisc"))
                                    Case 19
                                        REM -- All = 2
                                        fAccountcode = "AccountCode = 2 AND AccountRelation is null "
                                    Case Else
                                        fAccountcode = ""
                                End Select

                                sql = "SELECT Amount, Percent1, Percent2, LookforForward FROM XAL_PriceDisc" & _
                                      " WHERE Relation = 6" & _
                                      " AND " & fItemCode & _
                                      " AND " & fAccountcode & _
                                      " AND QuantityAmount <= " & CDblEx(mulitems(i)) & _
                                      " AND (" & SafeDatetime(SystemDate) & " >= FromDate OR FromDate = " & GetZeroDate() & ")" & _
                                      " AND (" & SafeDatetime(SystemDate) & " <= ToDate OR ToDate = " & GetZeroDate() & ")" & _
                                      " AND (Exchange = " & SafeString(Trim(orderDict("CurrencyGuid")))

                                If Trim(orderDict("CurrencyGuid")) = Trim(AppSettings("MULTICURRENCY_SITE_CURRENCY")) Then
                                    sql = sql & " OR Exchange is NULL"
                                End If

                                sql = sql & ") ORDER BY Relation, ItemCode, ItemRelation, AccountCode, AccountRelation, Exchange, QuantityAmount"

                                rsPriceDisc = GetDR(sql)
                                If rsPriceDisc.Read() Then 'If Not rsPriceDisc.EOF Then
                                    PDMDiscAmount = PDMDiscAmount + rsPriceDisc("Amount")
                                    PDMPercent1 = PDMPercent1 + rsPriceDisc("Percent1")
                                    PDMPercent2 = PDMPercent2 + rsPriceDisc("Percent2")

                                    REM -- All Items ItemCode = 2
                                    If k = 1 Then
                                        PDMAllItem = True
                                    End If

                                    If Not CBoolEx(rsPriceDisc("LookforForward")) Then
                                        REM -- Stop search
                                        j = 26
                                        k = 2
                                    End If
                                End If
                                rsPriceDisc.Close()
                            End If
                            REM -- Next k
                        Next
                        REM -- Next j
                    Next

                    PDMPercent1 = 100 * (1 - (1 - PDMPercent1 / 100) * (1 - PDMPercent2 / 100))

                    REM For each line in order - add the discount.
                    For Each orderLine As ExpDictionary In CType(orderDict("Lines"), ExpDictionary).Values
                        If orderLine("Multilndisc") = mulkeys(i) Or PDMAllItem Then
                            If PDMPercent1 > orderLine("LineDiscount") Then
                                orderLine("LineDiscount") = PDMPercent1
                            End If

                            If PDMDiscAmount > orderLine("LineDiscountAmount") Then
                                orderLine("LineDiscountAmount") = PDMDiscAmount
                            End If

                            orderLine("LineTotal") = globals.currency.ConvertCurrency(orderLine("Quantity") * (orderLine("ListPrice") - orderLine("LineDiscountAmount")) * (100 - orderLine("LineDiscount")) / 100, orderDict("CurrencyGuid"), orderLine("CurrencyGuid"))
                        End If
                    Next

                Next
            End If

            REM
            REM Balance Total for use in Total discount
            REM

            For Each orderLine As ExpDictionary In CType(orderDict("Lines"), ExpDictionary).Values

                If orderLine("Enddisc") = "1" Then
                    PDMBalance = PDMBalance + CDblEx(globals.currency.ConvertCurrency(orderLine("LineTotal"), orderLine("CurrencyGuid"), orderDict("CurrencyGuid")))
                    PDMEndfound = True
                End If
                PDMBalanceTotal = PDMBalanceTotal + CDblEx(globals.currency.ConvertCurrency(orderLine("LineTotal"), orderLine("CurrencyGuid"), orderDict("CurrencyGuid")))
            Next

            REM
            REM Total Order Discount
            REM

            PDMDiscAmount = 0
            PDMPercent1 = 0
            PDMPercent2 = 0

            If PDMEndfound Then
                For i = 7 To 21 Step 7

                    Select Case i
                        Case 7
                            REM -- CustomerAccount = 0
                            PDMArel = "AccountCode = 0 AND AccountRelation = " & SafeString(orderDict("CustomerGuid"))
                        Case 14
                            REM -- EndDisc = 1
                            PDMArel = "AccountCode = 1 AND AccountRelation = " & SafeString(orderDict("Enddisc"))
                        Case 21
                            REM -- All = 2
                            PDMArel = "AccountCode = 2 AND AccountRelation is null "
                        Case Else
                            PDMArel = ""
                    End Select

                    If items(i - 1) Then

                        sql = "SELECT Amount, Percent1, Percent2, LookforForward FROM XAL_PriceDisc" & _
                              " WHERE Relation = 7" & _
                              " AND ItemCode = 2 AND ItemRelation is null" & _
                              " AND " & PDMArel & _
                              " AND QuantityAmount <= " & SafeFloat(Abs(PDMBalance)) & _
                              " AND (" & SafeDatetime(SystemDate) & " >= FromDate OR FromDate = " & GetZeroDate() & ")" & _
                              " AND (" & SafeDatetime(SystemDate) & " <= ToDate OR ToDate = " & GetZeroDate() & ")" & _
                              " AND (Exchange = " & SafeString(Trim(orderDict("CurrencyGuid")))

                        If Trim(orderDict("CurrencyGuid")) = Trim(AppSettings("MULTICURRENCY_SITE_CURRENCY")) Then
                            sql = sql & " OR Exchange is NULL"
                        End If

                        sql = sql & ") ORDER BY Relation, ItemCode, ItemRelation, AccountCode, AccountRelation, Exchange, QuantityAmount"

                        rsPriceDisc = GetDR(sql)

                        LRunning = True
                        While rsPriceDisc.Read() And LRunning 'While Not rsPriceDisc.EOF And LRunning
                            If rsPriceDisc("Amount") Then PDMDiscAmount = PDMDiscAmount + rsPriceDisc("Amount")
                            If rsPriceDisc("Percent1") Then PDMPercent1 = PDMPercent1 + rsPriceDisc("Percent1")
                            If rsPriceDisc("Percent2") Then PDMPercent2 = PDMPercent2 + rsPriceDisc("Percent2")

                            If Not CBoolEx(rsPriceDisc("LookforForward").Value) Then
                                PDMAc = 28
                                LRunning = False
                            End If
                            'rsPriceDisc.MoveNext()
                        End While
                        rsPriceDisc.Close()
                    End If
                Next
            End If

            REM -- calculate total Discount Pct, round to one decimal, and calculate DiscountAmount. This is the way XAL does it.
            If PDMPercent1 <> 0 Or PDMPercent2 <> 0 Then
                If PDMBalance > 0 Then
                    TotalDiscPct = RoundEx(((PDMDiscAmount / PDMBalance) + (1 - (1 - PDMPercent1 / 100) * (1 - PDMPercent2 / 100))) * 100, 1)
                End If
                PDMDiscAmount = PDMBalance * (TotalDiscPct / 100)
            End If

            REM
            REM Total Fee or not fee
            REM

            If PDMDiscAmount * PDMBalanceTotal > 0 Or Not infoDict("XALNegDiscAsFee") Then
                If infoDict("XALNegDiscAsFee") Then
                    orderDict("ServiceCharge") = 0
                End If
                If PDMBalanceTotal > 0 Then
                    orderDict("InvoiceDiscountPct") = 100 * PDMDiscAmount / PDMBalanceTotal
                    orderDict("InvoiceDiscount") = PDMDiscAmount
                Else
                    orderDict("InvoiceDiscountPct") = 0
                    orderDict("InvoiceDiscount") = 0
                End If
            Else
                orderDict("InvoiceDiscountPct") = 0
                orderDict("InvoiceDiscount") = 0
                orderDict("ServiceCharge") = -PDMDiscAmount
            End If
        End Sub

        Function XALGetTaxPct(ByVal VatCode As Object, ByVal infoDict As ExpDictionary) As Object
            Dim sql As String
            Dim dictTax, cachekey, SystemDate As Object

            SystemDate = Now() 'Date

            cachekey = "dictTax_" & CStrEx(VatCode)
            If Not IsDBNullOrNothing(infoDict(cachekey)) Then ' If Not IsEmpty(infoDict(cachekey)) Then
                dictTax = infoDict(cachekey)
            Else
                sql = "SELECT VatPct FROM XAL_VatCodeRate" & _
                      " WHERE VatCode = " & SafeString(VatCode) & _
                      " AND (" & SafeDatetime(SystemDate) & " >= FromDate OR FromDate = " & GetZeroDate() & ")" & _
                      " AND (" & SafeDatetime(SystemDate) & " <= ToDate OR ToDate = " & GetZeroDate() & ")"

                dictTax = Sql2Dictionary(sql)
                dictTax("VatPct") = CDblEx(dictTax("VatPct"))

                infoDict(cachekey) = dictTax
            End If

            Return dictTax("VatPct")
        End Function

        Sub XALOrderTotal(ByVal orderDict As ExpDictionary, ByVal infoDict As ExpDictionary)
            Dim AmountSum, AmountSumVat, InvDiscSum, TaxPct As Decimal
            Dim SystemDate As Object

            SystemDate = Now() 'Date

            AmountSum = 0
            AmountSumVat = 0
            InvDiscSum = 0
            TaxPct = 0
            For Each orderLine As ExpDictionary In CType(orderDict("Lines"), ExpDictionary).Values
                If orderDict("Vatdutiable") Then
                    If CStrEx(orderLine("VatCode")) <> "" Then
                        If CStrEx(orderDict("VatCode")) <> "" Then
                            REM -- If vatcode on order - use that
                            orderLine("VatCode") = orderDict("VatCode")
                        End If

                        TaxPct = XALGetTaxPct(orderLine("VatCode"), infoDict)

                        orderLine("TaxAmount") = orderLine("LineTotal") * (TaxPct / 100)
                    Else
                        REM Calculate no VAT on order if not VATCode specified
                        orderLine("TaxAmount") = 0
                    End If
                Else
                    orderLine("TaxAmount") = 0
                End If

                orderLine("ListPriceInclTax") = orderLine("ListPrice") * (1 + TaxPct / 100)
                orderLine("LineDiscountAmountInclTax") = orderLine("LineDiscountAmount") * (1 + (TaxPct / 100))
                orderLine("TotalInclTax") = orderLine("LineTotal") + orderLine("TaxAmount")

                ' Write TaxPct to line
                orderLine("TaxPct") = TaxPct

                AmountSumVat = AmountSumVat + orderLine("TotalInclTax")
                AmountSum = AmountSum + orderLine("LineTotal")
            Next

            REM -- Sum of lines
            orderDict("SubTotal") = AmountSum
            orderDict("SubTotalInclTax") = AmountSumVat

            If infoDict("XALVatOnTotalDiscount") Then
                TaxPct = XALGetTaxPct(orderDict("VatCode"), infoDict)

                orderDict("InvoiceDiscountInclTax") = orderDict("InvoiceDiscount") * (1 + (TaxPct / 100))
            Else
                orderDict("InvoiceDiscountInclTax") = orderDict("InvoiceDiscount")
            End If

            orderDict("ServiceChargeInclTax") = orderDict("ServiceCharge") * (1 + (TaxPct / 100))

            orderDict("Total") = AmountSum - orderDict("InvoiceDiscount") + orderDict("ServiceCharge")
            orderDict("TotalInclTax") = AmountSumVat - orderDict("InvoiceDiscountInclTax") + orderDict("ServiceChargeInclTax")

            orderDict("TaxAmount") = (AmountSumVat - AmountSum) + (orderDict("ServiceChargeInclTax") - orderDict("ServiceCharge")) + (orderDict("InvoiceDiscountInclVat") - orderDict("InvoiceDiscount"))
        End Sub

        Sub XALInitTradeAgreements(ByVal orderDict As ExpDictionary, ByVal infoDict As ExpDictionary)
            REM XAL

            Dim PD_SalesPrLi, PD_SalesMuEn As Decimal
            Dim Code_SaPr, Code_SaLi, Code_SaMu, Code_SaEn As Object
            Dim rsXAL_Parameters As SqlDataReader
            Dim dictXAL_Parameters, tempDict, tempDict2 As ExpDictionary

            rsXAL_Parameters = GetDR("SELECT * FROM XAL_Parameters WHERE Name='SYSTEMPARAMETERS' OR Name='SYSTEMPARAMETRE' ORDER BY UserID, [Name]")

            REM INSERTED CALL HERE
            If Not rsXAL_Parameters.Read() Then 'If rsXAL_Parameters.EOF Then
                rsXAL_Parameters.Close()
                Throw New Exception("Problem calculating order : XAL_Parameters table not found - please make sure that all tables are extracted.")
                'Err.Raise(-1, "ExpandIT XAL Business Logic", "Problem calculating order : XAL_Parameters table not found - please make sure that all tables are extracted.")
            End If

            dictXAL_Parameters = SetDictionary(rsXAL_Parameters)

            tempDict = New ExpDictionary() 'newdict
            tempDict2 = New ExpDictionary() 'newdict

            REM ORIGINAL CALL HERE
            'If Not rsXAL_Parameters.Read() Then 'If rsXAL_Parameters.EOF Then
            '    Err.Raise(-1, "ExpandIT XAL Business Logic", "Problem calculating order : XAL_Parameters table not found - please make sure that all tables are extracted.")
            'End If

            PD_SalesPrLi = CDblEx(rsXAL_Parameters("Real1"))
            PD_SalesMuEn = CDblEx(rsXAL_Parameters("Real2"))

            rsXAL_Parameters.Close()
            REM --
            REM -- Generate Trade agreement parameters from XAL
            REM --

            Code_SaPr = PD_SalesPrLi And 65535
            Code_SaLi = (PD_SalesPrLi \ 65536) And 65535
            Code_SaMu = PD_SalesMuEn And 65535
            Code_SaEn = (PD_SalesMuEn \ 65536) And 65535

            tempDict.Add("SALESORDERPRICEACCOUNTITEM", (Code_SaPr And 1) <> 0)
            tempDict.Add("SALESORDERLINEACCOUNTITEM", (Code_SaLi And 1) <> 0)
            tempDict.Add("SALESORDERLINEACCOUNTGROUP", (Code_SaLi And 2) <> 0)
            tempDict.Add("SALESORDERLINEACCOUNTALL", (Code_SaLi And 4) <> 0)
            tempDict.Add("SALESORDERMULTILNACCOUNTGROUP", (Code_SaMu And 2) <> 0)
            tempDict.Add("SALESORDERMULTILNACCOUNTALL", (Code_SaMu And 4) <> 0)
            tempDict.Add("SALESORDERENDACCOUNTALL", (Code_SaEn And 4) <> 0)

            tempDict.Add("SALESORDERPRICEGROUPITEM", (Code_SaPr And 8) <> 0)
            tempDict.Add("SALESORDERLINEGROUPITEM", (Code_SaLi And 8) <> 0)
            tempDict.Add("SALESORDERLINEGROUPGROUP", (Code_SaLi And 16) <> 0)
            tempDict.Add("SALESORDERLINEGROUPALL", (Code_SaLi And 32) <> 0)
            tempDict.Add("SALESORDERMULTILNGROUPGROUP", (Code_SaMu And 16) <> 0)
            tempDict.Add("SALESORDERMULTILNGROUPALL", (Code_SaMu And 32) <> 0)
            tempDict.Add("SALESORDERENDGROUPALL", (Code_SaEn And 32) <> 0)

            tempDict.Add("SALESORDERPRICEALLITEM", (Code_SaPr And 64) <> 0)
            tempDict.Add("SALESORDERLINEALLITEM", (Code_SaLi And 64) <> 0)
            tempDict.Add("SALESORDERLINEALLGROUP", (Code_SaLi And 128) <> 0)
            tempDict.Add("SALESORDERLINEALLALL", (Code_SaLi And 256) <> 0)
            tempDict.Add("SALESORDERMULTILNALLGROUP", (Code_SaMu And 128) <> 0)
            tempDict.Add("SALESORDERMULTILNALLALL", (Code_SaMu And 256) <> 0)
            tempDict.Add("SALESORDERENDALLALL", (Code_SaEn And 256) <> 0)

            REM --
            REM -- Trade Agreement search order...
            REM --

            REM Customer Account
            tempDict2("SALESORDERPRICEACCOUNTITEM") = "004"       'Rem -- Price 1
            tempDict2("SALESORDERLINEACCOUNTITEM") = "005"        'Rem -- Line  2
            tempDict2("SALESORDERLINEACCOUNTGROUP") = "015"       'Rem -- Line  3
            tempDict2("SALESORDERLINEACCOUNTALL") = "025"         'Rem -- Line  4
            tempDict2("SALESORDERMULTILNACCOUNTGROUP") = "016"    'Rem -- Multi 5
            tempDict2("SALESORDERMULTILNACCOUNTALL") = "026"      'Rem -- Multi 6
            tempDict2("SALESORDERENDACCOUNTALL") = "027"          'Rem -- All (Incoice) 7

            REM Customer Group
            tempDict2("SALESORDERPRICEGROUPITEM") = "104"         'Rem -- Price 8
            tempDict2("SALESORDERLINEGROUPITEM") = "105"          'Rem -- Line 9
            tempDict2("SALESORDERLINEGROUPGROUP") = "115"         'Rem -- Line 10
            tempDict2("SALESORDERLINEGROUPALL") = "125"           'Rem -- Line 11
            tempDict2("SALESORDERMULTILNGROUPGROUP") = "116"      'Rem -- Multi 12
            tempDict2("SALESORDERMULTILNGROUPALL") = "126"        'Rem -- Multi 13
            tempDict2("SALESORDERENDGROUPALL") = "127"            'Rem -- All (Incoice) 14

            REM All Customers
            tempDict2("SALESORDERPRICEALLITEM") = "204"           'Rem -- Price 15
            tempDict2("SALESORDERLINEALLITEM") = "205"            'Rem -- Line 16
            tempDict2("SALESORDERLINEALLGROUP") = "215"           'Rem -- Line 17
            tempDict2("SALESORDERLINEALLALL") = "225"             'Rem -- Line 18
            tempDict2("SALESORDERMULTILNALLGROUP") = "216"        'Rem -- Multi 19
            tempDict2("SALESORDERMULTILNALLALL") = "226"          'Rem -- Multi 20
            tempDict2("SALESORDERENDALLALL") = "227"              'Rem -- All (Incoice) 21

            infoDict("PriceParameters") = tempDict
            infoDict("PriceMatrix") = tempDict2
            infoDict.Add("BackEnd", "XAL270")

            'Debug.DebugValue(infoDict, "infoDict")

        End Sub

        REM --  Line functions

        Sub XALInitLine(ByVal OrderDict As ExpDictionary, ByVal orderLine As ExpDictionary, ByVal infoDict As ExpDictionary)
            Dim rsProduct As SqlDataReader
            Dim sql As String = String.Empty

            sql = "SELECT ListPrice, Linedisc, Multilndisc, Enddisc, SalesVatCode FROM ProductTable WHERE ProductGuid = " & SafeString(orderLine("ProductGuid"))

            rsProduct = GetDR(sql)

            If Not rsProduct.Read() Then 'If rsProduct.EOF Then
                OrderDict("Lines").Remove(orderLine("LineGuid"))
                rsProduct.Close()
                Err.Raise(-1, "ExpandIT XAL Business Logic", Resources.Language.MESSAGE_INTERNAL_PRODUCTGUID & " [" & orderLine("ProductGuid") & "] " & Resources.Language.MESSAGE_IS_NOT_AVAILABLE)
            End If

            orderLine("CurrencyGuid") = OrderDict("CurrencyGuid")

            orderLine("ListPrice") = globals.currency.ConvertCurrency(rsProduct("ListPrice"), "", orderLine("CurrencyGuid"))

            orderLine("LineDiscGrp") = rsProduct("Linedisc")
            orderLine("Multilndisc") = rsProduct("Multilndisc")
            orderLine("Enddisc") = rsProduct("Enddisc")
            orderLine("VatCode") = rsProduct("SalesVatCode")

            orderLine("LineDiscount") = 0
            orderLine("LineDiscountAmount") = 0
            orderLine("LineTotal") = 0

            rsProduct.Close()

        End Sub

        Sub XALLineDisc(ByVal OrderObject As ExpDictionary, ByVal orderLine As ExpDictionary, ByVal infoDict As ExpDictionary)
            REM XAL/Axapta tradeagreement description
            REM
            REM                     ACCOUNT NO.        ACCOUNT GROUP       ALL ACCOUNTS
            REM                   Item Grp  All       Item Grp   All      Item Grp   All
            REM SALES
            REM +------------------------------------------------------------------------+
            REM +Price........:  0( )                7( )               14( )            +
            REM +Line disc....:  1( ) 2( ) 3( )      8( ) 9( )10( )     15( )16( )17( )  +
            REM +Multi disc...:       4( ) 5( )          11( )12( )          18( )19( )  +
            REM +Total disc...:            6( )               13( )               20( )  +
            REM +------------------------------------------------------------------------+
            REM                   Search direction ---->
            REM

            Dim TradeAgrName, TradeAgrValue As Object
            Dim SystemDate As Object
            Dim PLook, LLook As Boolean
            Dim items As Object()
            Dim keys As Object()
            Dim PDel As Boolean
            Dim tmpAmount As Object

            Dim sql As String
            Dim fAccountcode As String = String.Empty
            Dim fItemocode As String = String.Empty
            Dim fRelation As String = String.Empty
            Dim fAccountrelation As String = String.Empty
            Dim fItemrelation As String = String.Empty
            Dim rsTradeAgr, rsProduct, rsCustomer As SqlDataReader
            Dim i, j As Integer
            Dim PDPercent1, PDPercent2 As Decimal
            Dim SearchKeys(2)() As Integer
            SearchKeys(0) = New Integer() {0, 7, 14}
            SearchKeys(1) = New Integer() {1, 2, 3, 8, 9, 10, 15, 16, 17}


            rsProduct = GetDR("SELECT ProductGuid, Linedisc, ListPrice FROM ProductTable WHERE ProductGuid = " & SafeString(orderLine("ProductGuid")))
            rsCustomer = GetDR("SELECT CustomerGuid, Pricegroup, Linedisc FROM CustomerTable WHERE CustomerGuid = " & SafeString(OrderObject("CustomerGuid")))

            PDPercent1 = 0
            PDPercent2 = 0
            PLook = True
            LLook = True

            items = CType(infoDict("PriceParameters"), ExpDictionary).DictItems
            keys = CType(infoDict("PriceParameters"), ExpDictionary).DictKeys

            'SearchKeys = Array(Array(0, 7, 14), Array(1, 2, 3, 8, 9, 10, 15, 16, 17))
            REM - Search these combinations to get the price.
            REM - Search these combinations to get the line discount
            If rsProduct.Read() AndAlso rsCustomer.Read() Then 'If Not rsProduct.EOF And Not rsCustomer.EOF Then
                REM Customer Account No.

                SystemDate = DateTime.Now

                REM 1 = Price, 2 = LineDiscount.
                For i = 0 To 1
                    REM Search price
                    For j = LBound(SearchKeys(i)) To UBound(SearchKeys(i))

                        TradeAgrName = keys(SearchKeys(i)(j))
                        TradeAgrValue = items(SearchKeys(i)(j))

                        If TradeAgrValue Then
                            fAccountcode = Mid(infoDict("PriceMatrix")(TradeAgrName), 1, 1)
                            fItemocode = Mid(infoDict("PriceMatrix")(TradeAgrName), 2, 1)
                            fRelation = Mid(infoDict("PriceMatrix")(TradeAgrName), 3, 1)

                            Select Case fAccountcode
                                Case "0"
                                    REM -- "0" = CustomerGuid
                                    fAccountrelation = "=" & SafeString(Trim(CStrEx(rsCustomer("CustomerGuid"))))
                                Case "1"
                                    REM -- "1" = CustomerGroup
                                    Select Case fRelation
                                        Case 4
                                            REM -- Price
                                            fAccountrelation = "=" & SafeString(Trim(CStrEx(rsCustomer("Pricegroup"))))
                                        Case 5
                                            REM -- Line
                                            fAccountrelation = "=" & SafeString(Trim(CStrEx(rsCustomer("Linedisc"))))
                                    End Select
                                Case "2"
                                    REM -- "2" = All
                                    fAccountrelation = " is null "
                                Case Else
                                    fAccountrelation = ""
                            End Select

                            Select Case fItemocode
                                Case "0"
                                    REM -- "0" = ItemNumber
                                    fItemrelation = "=" & SafeString(Trim(CStrEx(rsProduct("ProductGuid"))))
                                Case "1"
                                    REM -- "1" = ItemGroup
                                    fItemrelation = "=" & SafeString(Trim(CStrEx(rsProduct("Linedisc"))))
                                Case "2"
                                    REM -- "2" = All
                                    fItemrelation = " is null"
                                Case Else
                                    fItemrelation = ""
                            End Select

                            sql = "SELECT QuantityAmount, PriceUnit, Amount, Deliverytime, ExternalItemNo, Percent1, Percent2, LookforForward FROM XAL_PriceDisc WHERE " & _
                                  "AccountCode = " & fAccountcode & _
                                  " AND ItemCode = " & fItemocode & _
                                  " AND Relation = " & fRelation & _
                                  " AND AccountRelation " & fAccountrelation & _
                                  " AND ItemRelation " & fItemrelation & _
                                  " AND (" & SafeDatetime(SystemDate) & " >= FromDate OR FromDate = " & GetZeroDate() & ")" & _
                                  " AND (" & SafeDatetime(SystemDate) & " <= ToDate OR ToDate = " & GetZeroDate() & ")" & _
                                  " AND (Exchange = " & SafeString(Trim(OrderObject("CurrencyGuid")))

                            If Trim(CStrEx(OrderObject("CurrencyGuid"))) = Trim(CStrEx(AppSettings("MULTICURRENCY_SITE_CURRENCY"))) Then
                                sql = sql & " OR Exchange is NULL"
                            End If

                            sql = sql & ") ORDER BY Relation, ItemCode, ItemRelation, AccountCode, AccountRelation, Exchange, QuantityAmount"

                            rsTradeAgr = GetDR(sql)

                            While rsTradeAgr.Read() ' While Not rsTradeAgr.EOF
                                If PDel Then
                                    PDel = False
                                    If fRelation = 4 Then
                                        orderLine("ListPrice") = 0
                                    ElseIf fRelation = 5 Then
                                        orderLine("LineDiscountAmount") = 0
                                    End If

                                End If

                                If rsTradeAgr("QuantityAmount") <= orderLine("Quantity") Then
                                    If (fRelation = 4) And PLook Then
                                        REM relation = Sales(Price)
                                        If rsTradeAgr("PriceUnit") Then
                                            tmpAmount = rsTradeAgr("Amount") / rsTradeAgr("PriceUnit").Value
                                        Else
                                            tmpAmount = rsTradeAgr("Amount")
                                        End If

                                        If rsTradeAgr("Amount") And (tmpAmount < orderLine("ListPrice") Or Not orderLine("ListPrice")) Then
                                            If Not rsTradeAgr("PriceUnit") Then
                                                orderLine("PriceUnit") = 1
                                            Else
                                                orderLine("PriceUnit") = rsTradeAgr("PriceUnit")
                                            End If

                                            orderLine("ListPrice") = rsTradeAgr("Amount") / orderLine("PriceUnit")

                                            If rsTradeAgr("Deliverytime") Then
                                                If (SystemDate + rsTradeAgr("Deliverytime")) > orderLine("ConfirmedDel") Then
                                                    orderLine("ConfirmedDel") = SystemDate + rsTradeAgr("Deliverytime")
                                                End If
                                            End If
                                        End If

                                        If Not orderLine("ExternalItemNo") Then
                                            orderLine("ExternalItemNo") = rsTradeAgr("ExternalItemNo")
                                        End If
                                    ElseIf (fRelation = 5) And LLook Then
                                        REM Relation = Sales(Line)
                                        orderLine("LineDiscountAmount") = orderLine("LineDiscountAmount") + rsTradeAgr("Amount")
                                        PDPercent1 = PDPercent1 + rsTradeAgr("Percent1")
                                        PDPercent2 = PDPercent2 + rsTradeAgr("Percent2")
                                    End If
                                End If

                                If Not CBoolEx(rsTradeAgr("LookforForward")) Then
                                    PLook = PLook And Not (fRelation = 4)
                                    LLook = LLook And Not (fRelation = 5)
                                    Exit For
                                End If
                                'rsTradeAgr.MoveNext()
                            End While
                            rsTradeAgr.Close()
                        End If
                    Next
                Next

                If Not PDel Then
                    orderLine("LineDiscount") = 100 * (1 - (1 - PDPercent1 / 100) * (1 - PDPercent2 / 100))
                End If

                If orderLine("ListPrice") = 0 Then
                    orderLine("ListPrice") = rsProduct("ListPrice").Value
                End If
            Else
                If Not rsProduct.Read() Then
                    rsProduct.Close()
                    rsCustomer.Close()
                    Throw New Exception(globals.eis.GetLabel("MESSAGE_INTERNAL_PRODUCTGUID") & " [" & orderLine("ProductGuid") & "] " & globals.eis.GetLabel("MESSAGE_IS_NOT_AVAILABLE"))
                Else
                    rsProduct.Close()
                    rsCustomer.Close()
                    Throw New Exception("CustomerAccount [" & OrderObject("CustomerGuid") & "] not found. ")
                End If
            End If
            rsProduct.Close()
            rsCustomer.Close()
        End Sub

        Sub XALLineAmount(ByVal OrderObject As ExpDictionary, ByVal orderLine As ExpDictionary, ByVal infoDict As ExpDictionary)
            REM SalesLineAmount
            If orderLine("ListPrice") <> 0 Then
                orderLine("LineTotal") = orderLine("Quantity") * (orderLine("ListPrice") - orderLine("LineDiscountAmount")) * (100 - orderLine("LineDiscount")) / 100
            End If
        End Sub

        Sub XALLineDiscountAmount(ByVal OrderObject As ExpDictionary, ByVal orderLine As ExpDictionary, ByVal infoDict As ExpDictionary)
            orderLine("LineDiscountAmount") = orderLine("LineDiscountAmount")
        End Sub

    End Class

End Namespace