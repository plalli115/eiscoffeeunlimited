Imports System.Configuration.ConfigurationManager
Imports Microsoft.VisualBasic
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Data.SqlClient
Imports System.Data
Imports System.Collections.Generic

Namespace ExpandIT

    Public Class BuslogicC5Class
        Inherits BuslogicBaseClass

        Public Sub New(ByVal _globals As GlobalsClass)
            MyBase.New(_globals)
        End Sub

        Public Overrides Function CalcOrder(ByVal OrderDict As ExpDictionary, ByVal Context As ExpDictionary) As String
            Dim olKeys As Object()
            Dim dictC5_PriceDisc, infoDict As ExpDictionary
            Dim sql, orderby As String
            Dim IsCalculated As Boolean
            Dim SystemDate, productList As String
            Dim currentKey, lastKey As String
            Dim keycounter As Integer

            SystemDate = DateTime.Today.ToShortDateString()
            If OrderDict("Lines").Count = 0 Then
                Return ""
            End If

            ' no need to recalculate the order dictionary.
            IsCalculated = CBoolEx(OrderDict("IsCalculated"))

            If IsCalculated Then
                Return ""
            End If

            infoDict = New ExpDictionary()

            infoDict("SystemDate") = SystemDate

            '            Try

            C5InitOrder(OrderDict, infoDict, Context)
            C5InitTradeAgreements(OrderDict, infoDict)

            ' Generate productlist
            productList = GetSQLInString(OrderDict("Lines"), "ProductGuid")
            infoDict.Add("ProductList", productList)

            ' Cache tables used in price calculation
            olKeys = CType(OrderDict("Lines"), ExpDictionary).DictKeys
            orderby = "ItemCode,ItemRelation,AccountCode,AccountRelation,Type,QuantityAmount"

            sql = "SELECT * FROM C5_PriceDisc WHERE " & _
                  " (" & SafeDatetime(SystemDate) & " >= FromDate OR FromDate = " & GetZeroDate() & ")" & _
                  " AND (" & SafeDatetime(SystemDate) & " <= ToDate OR ToDate = " & GetZeroDate() & ")" & _
                  " ORDER BY " & orderby

            Dim reader As SqlDataReader = GetDR(sql) ' conn.execute(sql)
            dictC5_PriceDisc = New ExpDictionary()

            currentKey = ""
            lastKey = ""

            While reader.Read
                currentKey = SafeString(reader("ItemCode")) & "," & _
                             SafeString(Trim(reader("ItemRelation"))) & "," & _
                             SafeString(reader("AccountCode")) & "," & _
                             SafeString(Trim(reader("AccountRelation")))

                If currentKey <> lastKey Then
                    keycounter = 0
                    dictC5_PriceDisc.Add(currentKey, New ExpDictionary())
                    lastKey = currentKey
                End If

                keycounter = keycounter + 1
                dictC5_PriceDisc(currentKey).Add(CStrEx(keycounter), SetDictionary(reader))

            End While
            reader.Close()

            infoDict.Add("dictC5_PriceDisc", dictC5_PriceDisc)

            ' Load ProductTable into dictionary
            sql = "SELECT * FROM ProductTable WHERE ProductGuid IN (" & productList & ") ORDER BY ProductGuid"
            infoDict.Add("dictProductTable", Sql2Dictionaries(sql, New Object() {"ProductGuid"}))

            ' Load ProductPrices into Dictionary
            sql = "SELECT ProductGuid, C5_StockPrice.C5PriceGroup, ListPrice, CurrencyGuid, InclVat " & _
                  "FROM C5_StockPrice LEFT JOIN C5_StockPriceGroup ON C5_StockPrice.C5PriceGroup = C5_StockPriceGroup.C5PriceGroup " & _
                  "WHERE ltrim(rtrim(ProductGuid)) IN (" & productList & ") " & _
                  "ORDER BY C5_StockPrice.ProductGuid, C5_StockPrice.C5PriceGroup"
            infoDict.Add("dictC5_StockPrice", Sql2Dictionaries(sql, New Object() {"ProductGuid", "C5PriceGroup"}))

            ' Calculate lines
            For i As Integer = 0 To OrderDict("Lines").Count - 1
                CalcLine(OrderDict, OrderDict("Lines")(olKeys(i)), olKeys(i), infoDict, Context)
                'Err.Clear()
            Next

            C5OrderTotal(OrderDict, infoDict)

            ' Mark order as calculated
            OrderDict("IsCalculated") = True
            OrderDict("ReCalculated") = True

            '            Catch ex As Exception
            '				httpcontext.current.response.write(ex.Message)
            '                Context("Errors")(Context.Count.ToString) = ex.Message
            '                OrderDict("Total") = 0
            '                OrderDict("TotalInclTax") = 0
            '                OrderDict("InvoiceDiscount") = 0
            '                OrderDict("ServiceCharge") = 0
            '                OrderDict("IsCalculated") = False
            '                OrderDict("ReCalculated") = False
            '            End Try

            Return ""
        End Function

        Sub CalcLine(ByVal OrderObject As ExpDictionary, ByVal orderLine As ExpDictionary, ByVal LineGuid As String, ByVal infoDict As ExpDictionary, ByVal Context As ExpDictionary)
            C5InitLine(OrderObject, orderLine, infoDict)
            C5LinePrice(OrderObject, orderLine, infoDict)
            C5LineDisc(OrderObject, orderLine, infoDict)
            C5LineAmount(OrderObject, orderLine, infoDict)
            orderLine("IsCalculated") = True
        End Sub

        ' --
        ' Functions
        ' --

        Sub C5InitTradeAgreements(ByVal orderDict As ExpDictionary, ByVal infoDict As ExpDictionary)
            ' C5

            Dim tempDict As New ExpDictionary()
            Dim tempDict2 As New ExpDictionary()

            ' --
            ' Generate Trade agreement parameters from C5 -
            ' THis gives the search order
            ' --

            tempDict("SALESORDERPRICEACCOUNTITEM") = True
            tempDict("SALESORDERLINEACCOUNTITEM") = True
            tempDict("SALESORDERLINEACCOUNTGROUP") = True
            tempDict("SALESORDERLINEACCOUNTALL") = True
            tempDict("SALESORDERMULTILNACCOUNTGROUP") = False
            tempDict("SALESORDERMULTILNACCOUNTALL") = False
            tempDict("SALESORDERENDACCOUNTALL") = False

            tempDict("SALESORDERPRICEGROUPITEM") = True
            tempDict("SALESORDERLINEGROUPITEM") = True
            tempDict("SALESORDERLINEGROUPGROUP") = True
            tempDict("SALESORDERLINEGROUPALL") = True
            tempDict("SALESORDERMULTILNGROUPGROUP") = False
            tempDict("SALESORDERMULTILNGROUPALL") = False
            tempDict("SALESORDERENDGROUPALL") = False

            tempDict("SALESORDERPRICEALLITEM") = True
            tempDict("SALESORDERLINEALLITEM") = True
            tempDict("SALESORDERLINEALLGROUP") = True
            tempDict("SALESORDERLINEALLALL") = True
            tempDict("SALESORDERMULTILNALLGROUP") = False
            tempDict("SALESORDERMULTILNALLALL") = False
            tempDict("SALESORDERENDALLALL") = False

            ' --
            ' Trade Agreement search order...
            ' --

            tempDict2("SALESORDERPRICEACCOUNTITEM") = "004"       'Price 1
            tempDict2("SALESORDERLINEACCOUNTITEM") = "005"        'Line  2
            tempDict2("SALESORDERLINEACCOUNTGROUP") = "015"       'Line  3
            tempDict2("SALESORDERLINEACCOUNTALL") = "025"         'Line  4
            tempDict2("SALESORDERMULTILNACCOUNTGROUP") = "016"    'Multi 5
            tempDict2("SALESORDERMULTILNACCOUNTALL") = "026"      'Multi 6
            tempDict2("SALESORDERENDACCOUNTALL") = "027"          'All (Incoice) 7

            tempDict2("SALESORDERPRICEGROUPITEM") = "104"         'Price 8
            tempDict2("SALESORDERLINEGROUPITEM") = "105"          'Line 9
            tempDict2("SALESORDERLINEGROUPGROUP") = "115"         'Line 10
            tempDict2("SALESORDERLINEGROUPALL") = "125"           'Line 11
            tempDict2("SALESORDERMULTILNGROUPGROUP") = "116"      'Multi 12
            tempDict2("SALESORDERMULTILNGROUPALL") = "126"        'Multi 13
            tempDict2("SALESORDERENDGROUPALL") = "127"            'All (Incoice) 14

            tempDict2("SALESORDERPRICEALLITEM") = "204"           'Price 15
            tempDict2("SALESORDERLINEALLITEM") = "205"            'Line 16
            tempDict2("SALESORDERLINEALLGROUP") = "215"           'Line 17
            tempDict2("SALESORDERLINEALLALL") = "225"             'Line 18
            tempDict2("SALESORDERMULTILNALLGROUP") = "216"        'Multi 19
            tempDict2("SALESORDERMULTILNALLALL") = "226"          'Multi 20
            tempDict2("SALESORDERENDALLALL") = "227"              'All (Incoice) 21


            infoDict("PriceParameters") = tempDict
            infoDict("PriceMatrix") = tempDict2
            infoDict.Add("BackEnd", "C5180")

        End Sub

        ' --
        ' Header Functions
        ' --

        Sub C5InitOrder(ByVal orderDict As ExpDictionary, ByVal infoDict As ExpDictionary, ByVal Context As ExpDictionary)
            Dim dtCustomer As DataTable
            Dim CustNo, sql As String

            ' Settings for C5 Buisness Logic
            infoDict("C5UsePrimaryAccount") = AppSettings("C5_USE_PRIMARY_ACCOUNT")
            infoDict("C5UseInvoiceToAccount") = AppSettings("C5_USE_INVOICETO_ACCOUNT")
            ' Settings end...

            sql = "SELECT * FROM CustomerTable WHERE ltrim(rtrim(CustomerGuid)) = " & SafeString(Trim(Context("CustomerGuid")))
            dtCustomer = SQL2DataTable(sql)
            If dtCustomer.Rows.Count = 0 Then
                Throw New Exception("Error initializing order - CustomerAccount [" & orderDict("CustomerGuid") & "] not found.")
            End If

            If CBoolEx(infoDict("C5UseInvoiceToAccount")) And (Trim(CStrEx(dtCustomer.Rows(0)("Invoiceaccount"))) <> "") Then
                CustNo = CStrEx(dtCustomer.Rows(0)("Invoiceaccount"))
                sql = "SELECT * FROM CustomerTable WHERE ltrim(rtrim(CustomerGuid)) = " & SafeString(Trim(CustNo))

                dtCustomer = SQL2DataTable(sql)

                If dtCustomer.Rows.Count = 0 Then
                    Throw New Exception("Error initializing order - Invoice to CustomerAccount [" & Trim(CustNo) & "] not found. ")
                End If
            End If

            orderDict("CustomerGuid") = CStrEx(dtCustomer.Rows(0)("CustomerGuid"))

            orderDict("Vatdutiable") = CStrEx(dtCustomer.Rows(0)("VatCode")) <> ""
            orderDict("TaxCode") = CStrEx(dtCustomer.Rows(0)("VatCode"))
            ' TaxPct used for the shipping and handling module
            orderDict("TaxPct") = getTaxPct(CStrEx(orderDict("TaxCode")), infoDict)
            orderDict("C5PriceGroup") = CStrEx(dtCustomer.Rows(0)("C5PriceGroup"))
            orderDict("ServiceCharge") = 0
            orderDict("InvoiceDiscount") = 0
            orderDict("DiscountGroup") = CStrEx(dtCustomer.Rows(0)("PriceGroup"))

            If AppSettings("C5_USE_CURRENCY_FROM_BILLTO_CUSTOMER") Then
                orderDict("CurrencyGuid") = CStrEx(dtCustomer.Rows(0)("CurrencyGuid"))
            Else
                orderDict("CurrencyGuid") = CStrEx(Context("CurrencyGuid"))
            End If

            If CStrEx(orderDict("CurrencyGuid")) = "" Then orderDict("CurrencyGuid") = Context("DefaultCurrency")

            infoDict("CustomerRecord") = SetDictionary(dtCustomer.Rows(0))
        End Sub

        Function getTaxPct(ByVal TaxCode As String, ByVal infoDict As ExpDictionary) As Decimal
            Dim sql, cachekey As String
            Dim dictTax As ExpDictionary

            ' Return 0 pct if the vat code rate was not found
            If TaxCode Is Nothing Then Return 0

            ' Return 0 pct if the vat code is blank
            If TaxCode = "" Then Return 0

            TaxCode = TaxCode.Trim

            cachekey = "dictTax_" & TaxCode.Trim

            If infoDict(cachekey) IsNot Nothing Then
                dictTax = infoDict(cachekey)
            Else
                sql = "SELECT VatPct FROM C5_VatCodeRate WHERE LTRIM(RTRIM(VatCode)) = " & SafeString(TaxCode.Trim)
                Try
                    dictTax = Sql2Dictionary(sql)
                    infoDict(cachekey) = dictTax
                Catch ex As Exception
                    Throw New Exception("TaxCode '" & TaxCode & "' was not found in table C5_VatCodeRate. Check the tax code on the customer in C5.")
                End Try
            End If

            Return CDblEx(dictTax("VatPct"))
        End Function

        Sub C5OrderTotal(ByVal orderDict As ExpDictionary, ByVal infoDict As ExpDictionary)
            Dim AmountSum, AmountSumVat As Decimal
            Dim orderLine As ExpDictionary
            Dim TaxPct As Decimal
            Dim SystemDate As String = DateTime.Today.ToShortDateString()

            For Each orderLine In CType(orderDict("Lines"), ExpDictionary).DictItems
                If orderDict("Vatdutiable") Then
                    If orderLine("Vatdutiable") Then
                        ' Use tax pct from line (Product) if set
                        TaxPct = getTaxPct(CStrEx(orderLine("TaxCode")), infoDict)
                    Else
                        TaxPct = getTaxPct(CStrEx(orderDict("TaxCode")), infoDict)
                    End If

                    If orderLine("ListPriceIsInclTax") Then
                        ' Substract the Tax from the ListPrice and Line Total
                        orderLine("LineTotal") = orderLine("LineTotal") * 1 / (1 + TaxPct / 100)
                        orderLine("ListPriceInclTax") = orderLine("ListPrice")
                        orderLine("ListPrice") = orderLine("ListPrice") * 1 / (1 + TaxPct / 100)

                        orderLine("LineDiscountAmountInclTax") = orderLine("LineDiscountAmount")
                        orderLine("LineDiscountAmount") = orderLine("LineDiscountAmount") * 1 / (1 + (TaxPct / 100))
                    Else
                        orderLine("ListPriceInclTax") = orderLine("ListPrice") * (1 + TaxPct / 100)
                        orderLine("LineDiscountAmountInclTax") = orderLine("LineDiscountAmount") * (1 + (TaxPct / 100))
                    End If

                    orderLine("TaxAmount") = orderLine("LineTotal") * (TaxPct / 100)
                Else
                    TaxPct = getTaxPct(CStrEx(orderLine("TaxCode")), infoDict)

                    If orderLine("Vatdutiable") And orderLine("ListPriceIsInclTax") Then
                        ' Substract the Tax from the ListPrice and Line Total
                        orderLine("LineTotal") = orderLine("LineTotal") * 1 / (1 + TaxPct / 100)
                        orderLine("ListPriceInclTax") = orderLine("ListPrice")
                        orderLine("ListPrice") = orderLine("ListPrice") * 1 / (1 + TaxPct / 100)
                    Else
                        orderLine("ListPriceInclTax") = orderLine("ListPrice") * (1 + TaxPct / 100)
                    End If

                    orderLine("TaxAmount") = 0
                End If

                orderLine("TaxPct") = TaxPct

                orderLine("TotalInclTax") = orderLine("LineTotal") + orderLine("TaxAmount")

                AmountSumVat = AmountSumVat + orderLine("TotalInclTax")
                AmountSum = AmountSum + orderLine("LineTotal")
            Next

            orderDict("SubTotal") = AmountSum ' Sum of lines
            orderDict("SubTotalInclTax") = AmountSumVat ' Sum of lines

            orderDict("InvoiceDiscountInclTax") = orderDict("InvoiceDiscount") * (1 + (orderDict("TaxPct") / 100))
            orderDict("ServiceChargeInclTax") = orderDict("ServiceCharge") * (1 + (orderDict("TaxPct") / 100))

            orderDict("TotalInclTax") = AmountSumVat - orderDict("InvoiceDiscountInclTax") + orderDict("ServiceChargeInclTax")
            orderDict("Total") = AmountSum - orderDict("InvoiceDiscount") + orderDict("ServiceCharge")
            orderDict("TaxAmount") = orderDict("TotalInclTax") - orderDict("Total")
        End Sub

        ' --
        '  Line functions
        ' --
        Sub C5InitLine(ByVal OrderObject As ExpDictionary, ByVal orderLine As ExpDictionary, ByVal infoDict As ExpDictionary)
            Dim dictProduct As ExpDictionary = infoDict("dictProductTable")(SafeString(orderLine("ProductGuid")))

            If dictProduct IsNot Nothing Then
                orderLine("CurrencyGuid") = OrderObject("CurrencyGuid")
                orderLine("ListPrice") = globals.currency.ConvertCurrency(dictProduct("ListPrice"), "", orderLine("CurrencyGuid"))
                orderLine("LineDiscGrp") = dictProduct("Linedisc")
                orderLine("TaxCode") = dictProduct("SalesVatCode")
                orderLine("Vatdutiable") = (CStrEx(orderLine("TaxCode")) <> "")
                orderLine("LineDiscount") = 0
                orderLine("LineDiscountAmount") = 0
                orderLine("LineTotal") = 0
                orderLine("TaxAmount") = 0
                orderLine("TotalInclTax") = 0
            Else
                CType(OrderObject("Lines"), ExpDictionary).Remove(orderLine("LineGuid"))
                Throw New Exception("ExpandIT C5 Business Logic -" & Resources.Language.MESSAGE_INTERNAL_PRODUCTGUID & " [" & orderLine("ProductGuid") & "] " & Resources.Language.MESSAGE_IS_NOT_AVAILABLE)
            End If
        End Sub

        Sub C5LinePrice(ByVal OrderObject As ExpDictionary, ByVal orderLine As ExpDictionary, ByVal infoDict As ExpDictionary)
            ' Get line price from C5_StockPrice table according to Customer information
            Dim custDict, dictProductPrice As ExpDictionary
            Dim currKey As String

            custDict = infoDict("CustomerRecord")

            If CStrEx(OrderObject("C5PriceGroup")) = "" Then
                Throw New Exception("ExpandIT C5 Business Logic - Customer Pricegroup not defined for Customer.")
            End If

            ' Load price according to Customer Price Group
            currKey = SafeString(Trim(orderLine("ProductGuid"))) & "," & SafeString(Trim(OrderObject("C5PriceGroup")))

            If infoDict("dictC5_StockPrice")(currKey) Is Nothing Then
                ' Use default price if Customer Price not found
                currKey = SafeString(Trim(orderLine("ProductGuid"))) & "," & SafeString(Trim(AppSettings("C5_PriceGroup")))

                If infoDict("dictC5_StockPrice")(currKey) Is Nothing Then
                    Throw New Exception("ExpandIT C5 Business Logic - Price was not found - default group [" & AppSettings("C5_PriceGroup") & "] not found")
                End If
            End If

            dictProductPrice = infoDict("dictC5_StockPrice")(currKey)

            orderLine("ListPrice") = globals.currency.ConvertCurrency(dictProductPrice("ListPrice"), dictProductPrice("CurrencyGuid"), orderLine("CurrencyGuid"))
            orderLine("ListPriceIsInclTax") = CBoolEx(dictProductPrice("InclVat") = 1)
        End Sub

        Sub C5LineDisc(ByVal OrderObject As ExpDictionary, ByVal orderLine As ExpDictionary, ByVal infoDict As ExpDictionary)
            ' C5 tradeagreement description

            '  Hvis du vaelger 'Ja', soeges efter varesalgsrabatter i denne raekkefoelge:
            '                        cust               cust                 cust
            '                     ACCOUNT NO.        ACCOUNT GROUP       ALL ACCOUNTS
            '                   Item Grp  All       Item Grp   All      Item Grp   All
            ' SALES
            ' +------------------------------------------------------------------------+
            ' +Price........:  1( )                5( )                9( )            +
            ' +Line disc....:  2( ) 3( ) 4( )      6( ) 7( ) 8( )     10( )11( )12( )  +
            ' +------------------------------------------------------------------------+
            '                   Search direction ---->

            '
            '  1. Varenummer      / Debitorkonto
            '  2. Varerabatgruppe / Debitorkonto
            '  3. Alle varer      / Debitorkonto
            '  4. Varenummer      / Debitorrabatgruppe
            '  5. Varerabatgruppe / Debitorrabatgruppe
            '  6. Alle varer      / Debitorrabatgruppe
            '  7. Varenummer      / Alle debitorkonti
            '  8. Varerabatgruppe / Alle debitorkonti
            '  9. Alle varer      / Alle debitorkonti
            '
            '  Hvis du vaelger 'Nej', soeges efter varesalgsrabatter i denne raekkefoelge:

            '                        cust               cust                 cust
            '                     ACCOUNT NO.        ACCOUNT GROUP       ALL ACCOUNTS
            '                   Item Grp  All       Item Grp   All      Item Grp   All
            ' SALES
            ' +------------------------------------------------------------------------+
            ' +Price........:  1( )                2( )                3( )            +
            ' +Line disc....:  4( ) 7( )10( )      5( )  8( )11( )     6( ) 9( )12( )  +
            ' +------------------------------------------------------------------------+

            '  1. Varenummer      / Debitorkonto
            '  2. Varenummer      / Debitorrabatgruppe
            '  3. Varenummer      / Alle debitorkonti
            '  4. Varerabatgruppe / Debitorkonto
            '  5. Varerabatgruppe / Debitorrabatgruppe
            '  6. Varerabatgruppe / Alle debitorkonti
            '  7. Alle varer      / Debitorkonto
            '  8. Alle varer      / Debitorrabatgruppe
            '  9. Alle varer      / Alle debitorkonti

            '

            Dim fAccountrelation As String = ""
            Dim fItemrelation As String = ""
            Dim fItemocode, fAccountcode As String
            Dim fRelation As Integer
            Dim OrgListPrice, OrgListPriceInclTax, PDPercent1, PDPercent2, key, item As Object
            Dim fromb, tob, stepb, froma, toa, stepa As Integer
            Dim orderby, SystemDate, currentKey As String
            Dim dictC5_PriceDisc As ExpDictionary
            Dim items, keys As Object()
            Dim TradeAgrValue, PDel, LLook, running, SameRunning As Boolean
            Dim TradeAgrName As String

            SystemDate = infoDict("SystemDate")
            dictC5_PriceDisc = infoDict("dictC5_PriceDisc")

            PDPercent1 = 0
            PDPercent2 = 0

            items = CType(infoDict("PriceParameters"), ExpDictionary).DictItems
            keys = CType(infoDict("PriceParameters"), ExpDictionary).DictKeys


            OrgListPrice = orderLine("ListPrice")
            OrgListPriceInclTax = orderLine("ListPriceInclTax")

            ' Customer Account No.

            running = True

            '  1  2  3  4
            '  8  9 10 11
            ' 15 16 17 18

            '  1  8  15
            '  2  9  16
            '  3 10  17
            '  4 11  18

            If infoDict("C5UsePrimaryAccount") Then
                froma = 1
                toa = 15
                stepa = 7
                fromb = 0
                tob = 3
                stepb = 1
                orderby = "ItemCode, ItemRelation, AccountCode, AccountRelation, Type, QuantityAmount"
            Else
                froma = 1
                toa = 4
                stepa = 1
                fromb = 0
                tob = 14
                stepb = 7
                orderby = "AccountCode, AccountRelation, ItemCode, ItemRelation, Type, QuantityAmount"
            End If

            LLook = True
            PDel = True

            For i As Integer = froma To toa Step stepa
                If Not LLook Then Exit For
                For j As Integer = fromb To tob Step stepb
                    TradeAgrName = CStrEx(keys(i + j))
                    TradeAgrValue = CBoolEx(items(i + j))


                    If TradeAgrValue Then
                        fAccountcode = Mid(infoDict("PriceMatrix")(TradeAgrName), 1, 1)
                        fItemocode = Mid(infoDict("PriceMatrix")(TradeAgrName), 2, 1)

                        Select Case fAccountcode
                            Case "0" ' CustomerGuid
                                fAccountrelation = Trim(CStrEx(OrderObject("CustomerGuid")))
                            Case "1" ' CustomerGroup
                                fAccountrelation = Trim(CStrEx(OrderObject("DiscountGroup")))
                            Case "2" ' All
                                fAccountrelation = ""
                        End Select

                        Select Case fItemocode
                            Case "0" ' ItemNumber
                                fItemrelation = Trim(CStrEx(orderLine("ProductGuid")))
                            Case "1" ' ItemGroup
                                fItemrelation = Trim(CStrEx(orderLine("LineDiscGrp")))
                            Case "2" ' All
                                fItemrelation = ""
                        End Select

                        currentKey = SafeString(fItemocode) & "," & _
                                     SafeString(fItemrelation) & "," & _
                                     SafeString(fAccountcode) & "," & _
                                     SafeString(fAccountrelation)

                        SameRunning = True

                        If dictC5_PriceDisc(currentKey) IsNot Nothing Then
                            For Each key In CType(dictC5_PriceDisc(currentKey), ExpDictionary).Keys
                                If Not SameRunning Then Exit For
                                item = dictC5_PriceDisc(currentKey)(key)

                                fRelation = item("Type")

                                If PDel Then
                                    PDel = False
                                    If fRelation = 0 Then ' Percent
                                        orderLine("LineDiscount") = 0
                                    ElseIf fRelation = 1 Then ' DiscAmount
                                        orderLine("LineDiscountAmount") = 0
                                    ElseIf fRelation = 2 Then ' Price
                                        orderLine("ListPrice") = 0
                                    End If
                                End If

                                If item("QuantityAmount") <= orderLine("Quantity") Then
                                    Select Case fRelation
                                        Case 2 ' relation = Sales(Price)
                                            If item("Amount") And Not orderLine("ListPrice") Then
                                                orderLine("ListPrice") = item("Amount")
                                            End If
                                        Case 0 ' DiscPercent
                                            PDPercent1 = PDPercent1 + item("Amount")
                                            PDPercent2 = 0
                                        Case 1 ' Discamount
                                            orderLine("LineDiscountAmount") = orderLine("LineDiscountAmount") + item("Amount")
                                    End Select
                                End If

                                If item("LookforForwardSame") <> 1 Then
                                    SameRunning = False
                                End If

                                If item("LookforForward") <> 1 Then
                                    LLook = False
                                    Exit For
                                End If
                            Next
                        End If
                    End If
                    If Not LLook Then Exit For
                Next
            Next

            If Not PDel Then
                orderLine("LineDiscount") = 100 * (1 - (1 - PDPercent1 / 100) * (1 - PDPercent2 / 100))
            End If

            If orderLine("ListPrice") = 0 Then
                orderLine("ListPriceInclTax") = OrgListPriceInclTax
                orderLine("ListPrice") = OrgListPrice
            End If
        End Sub

        Sub C5LineAmount(ByVal OrderObject As ExpDictionary, ByVal orderLine As ExpDictionary, ByVal infoDict As ExpDictionary)
            ' SalesLineAmount
            If orderLine("ListPrice") <> 0 Then
                orderLine("LineTotal") = orderLine("Quantity") * (orderLine("ListPrice") - orderLine("LineDiscountAmount")) * (100 - orderLine("LineDiscount")) / 100
            End If
        End Sub

    End Class
End Namespace
