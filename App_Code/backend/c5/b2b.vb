Imports System.Configuration.ConfigurationManager
Imports Microsoft.VisualBasic
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

Namespace ExpandIT

    Public Class B2BC5Class
        Inherits B2BBaseClass

        Public Sub New(ByVal _globals As GlobalsClass)
            MyBase.New(_globals)
        End Sub

        Public Overrides Sub DecodeAddress(ByRef dict As Object)
            Dim a, retval As Object
            Dim city, zip, sql, strCountryName As String
            Dim i As Integer

            ' Split City to City and ZipCode
            city = CStrEx(dict("CityName"))
            a = Split(city, " ")
            city = ""
            zip = ""
            For i = 0 To UBound(a)
                If zip = "" Then
                    If CStrEx(a(i)) = CStrEx(CLngEx(a(i))) Then
                        zip = a(i)
                    Else
                        If city <> "" Then city = city & " "
                        city = city & a(i)
                    End If
                Else
                    If city <> "" Then city = city & " "
                    city = city & a(i)
                End If
            Next
            dict("CityName") = city
            dict("ZipCode") = zip

            ' For performance reasons on heavy loaded systems the 
            ' Country table might be cached in the application object.
            ' Changing the following: Make the change for XAL as well.

            ' On XAL systems and C5 systems the CustomerTable holds
            ' the CountryName in the CountryGuid field. This piece of
            ' of code replaces the name with the Guid. 
            ' This funciton is used 2 places in lib_user.LoadUser.
            ' Get the name, held in the Guid field

            strCountryName = CStrEx(dict("CountryGuid"))
            sql = "SELECT CountryGuid FROM CountryTable WHERE CountryName = " & SafeString(strCountryName)
            retval = Sql2Dictionary(sql)
            ' Replace name with guid
            dict("CountryGuid") = retval("CountryGuid")
            ' Close the object used
            retval = Nothing
        End Sub


        ' ---------------------------------------------------------------
        ' Load the customer ledger entries for a selected customer.
        ' ---------------------------------------------------------------
        Public Overrides Function GetCustomerLedgerEntries(ByVal User As ExpDictionary) As ExpDictionary
            Dim sql As String, retval As Object
            ' Select the related customer, the customer table contains some calculated fields of interest
            sql = "SELECT * FROM CustomerTable WHERE CustomerGuid = '" & User("CustomerGuid") & "'"
            retval = Sql2Dictionary(sql)
            ' Select the ledger entries for the customer.
            sql = "SELECT * FROM CustomerLedgerEntry WHERE CustomerGuid = '" & User("CustomerGuid") & "' ORDER BY PostingDate,EntryGuid"
            On Error Resume Next
            retval("Lines") = SQL2Dicts(sql, "EntryGuid")
            If Err.Number <> 0 Then
                HttpContext.Current.Response.Write("<b>An error occured.</b><br />Please ensure that you have extracted the Customer Ledger Entry " & _
                    "table.<br /><br />" & _
                    "<b>The following error was returned:</b> <br />" & vbCrLf & Err.Description & "(" & Err.Number & ")")
                HttpContext.Current.Response.End()
            End If
            On Error GoTo 0
            GetCustomerLedgerEntries = retval
        End Function

        ' ---------------------------------------------------------------
        ' Returns the document type description for a document type id.
        ' Blank is returned if the type id wasn't found.
        ' ---------------------------------------------------------------
        Public Overrides Function GetDocumentTypeDescription(ByVal lngType As Integer) As String
            Dim arrTypes As String() = {"", "Faktura", "Kreditnota", "Betaling", "Rente", "Difference", "Regulering", "Kasserabat", "F&oslash;lgeseddel", "Projekt", "Sekund&aelig;r afrunding"}

            ' Set the return value if indexes aren't out of bounds.
            If LBound(arrTypes) <= lngType And UBound(arrTypes) >= lngType Then
                Return arrTypes(lngType)
            Else
                Return ""
            End If
        End Function

    End Class

End Namespace
