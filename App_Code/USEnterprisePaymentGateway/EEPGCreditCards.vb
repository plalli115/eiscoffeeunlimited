Imports System.Configuration.ConfigurationManager
Imports Microsoft.VisualBasic
Imports System.IO
Imports ExpandIT.GlobalsClass
Imports ExpandIT.B2BBaseClass
Imports ExpandIT.BuslogicBaseClass
Imports ExpandIT.DatabasePathClass
Imports ExpandIT.CurrencyBaseClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT.Debug
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Generic
Imports System.Net.Mail.SmtpClient
Imports System.IO.Compression
Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Xml
Imports ExpandIT.SectionSettingsManager

Namespace ExpandIT
    '<!-- JA2011042101 - EEPG USE EXISTING CREDIT CARDS - Start -->
    Public Class EEPGCreditCards

        Private globals As GlobalsClass
        Dim pageobj As ExpandIT.Page = CType(System.Web.HttpContext.Current.Handler, ExpandIT.Page)
        Dim objSearch As New USSearch

        Public Sub New(Optional ByVal _globals As GlobalsClass = Nothing)
            globals = _globals
        End Sub



        Public Function getCreditCardList()
            Dim str As String = ""
            Dim infoArray As String = ""
            Dim sql As String = "SELECT * FROM PaymentTable WHERE UserGuid=" & SafeString(globals.User("UserGuid")) & " AND PaymentType='EEPG'"
            'Dim sql As String = ""
            Dim CCdict As ExpDictionary = SQL2Dicts(sql)
            'Dim CCdict As ExpDictionary = Nothing
            If Not CCdict Is Nothing AndAlso Not CCdict Is DBNull.Value AndAlso CCdict.Count > 0 Then
                str = "<table cellpadding=""0"" cellspacing=""0"" style=""width:95%;margin:10px;"">"
                str = str & "<tr><td align=""center"" style=""height:40px;font-weight:bold;"">" & Resources.Language.LABEL_EEPG_SELECT_CC & "</td></tr>"
                For Each item As ExpDictionary In CCdict.Values
                    str = str & "<tr><td style=""padding-bottom:10px;"">"
                    infoArray = item("PaymentFName") & "|" & item("PaymentLName") & "|" & item("PaymentLastNumbers") & "|" & item("PaymentEncryptedData") & "|" & item("PaymentAddress1No") & " " & item("PaymentAddress1St") & "|" & item("PaymentAddress2") & "|" & item("PaymentCity") & "|" & item("PaymentState") & "|" & item("PaymentPhone") & "|" & item("PaymentEmail") & "|" & item("PaymentZipCode")
                    str = str & "<a onclick=""animatedcollapse.hide('ExistingCreditCards');SetParametersEEPG('" & infoArray & "')"" style=""cursor: pointer;"">" & item("PaymentLastNumbers") & "</a>"
                    str = str & "</td></tr>"
                Next
                str = str & "</table>"
                Return str
            Else
                Return str
            End If
        End Function

    End Class
    '<!-- JA2011042101 - EEPG USE EXISTING CREDIT CARDS - End -->
End Namespace