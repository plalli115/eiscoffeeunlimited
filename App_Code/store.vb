Imports System.Configuration.ConfigurationManager
Imports Microsoft.VisualBasic
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

Namespace ExpandIT

    Public Class StoreClass

        Private globals As GlobalsClass
        Private eis As EISClass

        Public Sub New(ByVal _globals As GlobalsClass)
            Dim url As Object

            globals = _globals
            eis = New EISClass(globals)

            ' Check if the website is closed. If so then redirect to closed.aspx.
            If HttpContext.Current.Application("Closed") Then
                HttpContext.Current.Response.Write(Closed.Render())
                HttpContext.Current.Response.End()
            End If

            ' Initialize the Application, Databasen and the Shop.
            eis.InitializeShop()

            ' Redirect if case dosn't match the virtual root. Otherwise cookies wont work.
            url = HttpContext.Current.Request.ServerVariables("URL")
            If HttpContext.Current.Request.ServerVariables("QUERY_STRING") <> "" Then url = url & "?" & HttpContext.Current.Request.ServerVariables("QUERY_STRING")
            If Mid(url, 1, Len(VRoot)) <> VRoot Then
                url = VRoot & Mid(url, Len(VRoot) + 1)
                HttpContext.Current.Response.Redirect(url)
            End If

            ' Activate randomize
            Randomize()

            ' Get the LangaugeGuid from a cookie.
            If Not HttpContext.Current.Request.Cookies("user") Is Nothing Then
                HttpContext.Current.Session("LanguageGuid") = CStrEx(HttpContext.Current.Request.Cookies("user").Item("LanguageGuid"))
            End If

            ' Load GroupGuid from request.
            globals.GroupGuid = CLngEx(HttpContext.Current.Request("GroupGuid"))

            ' Set the UserID.
            eis.SetUserID()

            ' Load the user. This will not result in double load, when the function is used on a weblogic page.
            ' This is because the first line in the LoadUser function leaves the function if a user is already
            ' loaded. 
            globals.User = eis.LoadUser(HttpContext.Current.Session("UserGuid"))
            If globals.User.ContainsKey("SecondaryCurrencyGuid") Then HttpContext.Current.Session("UserSecondaryCurrencyGuid") = globals.User("SecondaryCurrencyGuid")
            If globals.User.ContainsKey("CurrencyGuid") Then HttpContext.Current.Session("UserCurrencyGuid") = globals.User("CurrencyGuid")

            ' Overwrite the language if it was specified in the request
            If HttpContext.Current.Request.QueryString("LanguageGuid") <> "" Then HttpContext.Current.Session("LanguageGuid") = HttpContext.Current.Request.QueryString("LanguageGuid")

            ' Load the minicart counter
            globals.MiniCartInfo = GetMiniCartFromCookie("MiniCart")

            If globals.MiniCartInfo Is Nothing Then
                globals.OrderDict = eis.LoadOrderDictionary(globals.User)
                eis.UpdateMiniCartInfo(globals.OrderDict)
            End If
        End Sub

    End Class

End Namespace
