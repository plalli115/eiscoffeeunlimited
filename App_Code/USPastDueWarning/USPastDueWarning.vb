Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Collections.Generic
Imports System.Data
Imports System.Net
Imports ExpandIT.GlobalsClass

Namespace ExpandIT

    Public Class USPastDueWarning

        Private globals As GlobalsClass

        Public Sub New(ByVal _globals As GlobalsClass)
            globals = _globals
        End Sub

        Public Sub IsPastDue(ByVal customerGuid As String)
            Dim sql As String
            Dim dueDays As Double
            Dim message As String
            Dim cleDict As ExpDictionary
            Dim payments As Double = 0
            Dim sql2 As String = ""

            dueDays = -CDblEx(AppSettings("PAST_DUE_DAYS"))

            sql = "SELECT COUNT(*) FROM CustomerLedgerEntry WHERE CustomerGuid=" & SafeString(customerGuid) & _
                " AND DueDate<=" & SafeString(DateTime.Now.Date.AddDays(dueDays)) & " AND IsOpen=1 AND DocumentType=2"
            If getSingleValueDB(sql) > 0 Then
                sql = "SELECT DocumentGuid,OriginalAmountLCY FROM CustomerLedgerEntry WHERE CustomerGuid=" & SafeString(customerGuid) & _
                " AND DueDate<=" & SafeString(DateTime.Now.Date.AddDays(dueDays)) & " AND IsOpen=1 AND DocumentType=2"
                cleDict = SQL2Dicts(sql)
                If Not cleDict Is Nothing AndAlso Not cleDict Is DBNull.Value AndAlso cleDict.Count > 0 Then
                    For Each item As ExpDictionary In cleDict.Values
                        'JA2011030701 - PAYMENT TABLE - START
                        'sql2 = "SELECT SUM(PaymentTransactionAmount) as 'PaymentTransactionAmount' FROM [EEPGOnlinePayments] WHERE DocumentGuid=" & SafeString(CStrEx(item("DocumentGuid"))) & " AND PaymentGuid NOT IN (SELECT PaymentGuid FROM EEPGProcessedOnlinePayments)"
                        sql2 = "SELECT SUM(PaymentTransactionAmount) as 'PaymentTransactionAmount' FROM [PaymentTable] WHERE LedgerPayment=1 AND DocumentGuid=" & SafeString(CStrEx(item("DocumentGuid"))) & " AND PaymentGuid NOT IN (SELECT PaymentGuid FROM EEPGProcessedOnlinePayments)"
                        'JA2011030701 - PAYMENT TABLE - END
                        payments = CDblEx(getSingleValueDB(sql2))

                        If payments < CDblEx(item("OriginalAmountLCY")) Then

                            message = Resources.Language.LABEL_PAST_DUE_WARNING
                            If message.Contains("[%par%]") Then
                                message = message.Replace("[%par%]", CStrEx(AppSettings("PAST_DUE_DAYS")))
                            End If
                            globals.messages.Errors.Add(message)

                            Exit For
                        End If
                    Next
                End If
            End If
        End Sub

    End Class

End Namespace
