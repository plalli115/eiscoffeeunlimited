Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Collections.Generic
Imports System.Data
Imports System.Net
Imports ExpandIT.GlobalsClass

Namespace ExpandIT

    Public Class USCreditLimit

        Private globals As GlobalsClass

        Public Sub New(ByVal _globals As GlobalsClass)
            globals = _globals
        End Sub

        Public Sub overCreditLimit(ByVal user As ExpDictionary)
            If CDblEx(user("BalanceAmountLCY")) >= CDblEx(user("CreditLimitLCY")) Then
                globals.messages.Errors.Add(Resources.Language.LABEL_OVER_CREDIT_LIMIT)
            End If
        End Sub

    End Class

End Namespace
