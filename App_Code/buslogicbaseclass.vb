Imports System.Configuration.ConfigurationManager
Imports Microsoft.VisualBasic
Imports ExpandIT.EISClass
Imports ExpandIT

Namespace ExpandIT
    ' TYPE NONE

    Public MustInherit Class BuslogicBaseClass

        Protected globals As GlobalsClass
        'Changed here to make use of both currency classes
        Protected currency As CurrencyBaseClass

        Public Sub New(ByVal _globals As GlobalsClass)
            globals = _globals
            currency = globals.currency
        End Sub

        ' --
        ' Business logic
        ' --

        ' --
        ' Public functions
        ' --
        Public MustOverride Function CalcOrder(ByVal OrderDict As ExpDictionary, ByVal Context As ExpDictionary) As String

    End Class

End Namespace
