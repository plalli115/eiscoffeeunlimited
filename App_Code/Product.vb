Imports System.Configuration.ConfigurationManager
Imports Microsoft.VisualBasic
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports system.Drawing
Imports System.Drawing.Drawing2D
Imports System.Drawing.Imaging
Imports ExpandIT.Debug
Imports ExpandIT.GlobalsClass
Imports System.IO

Namespace ExpandIT

    Public Class ProductClass
        Private m_data As ExpDictionary
        Private m_groupguid As String = Nothing
        Private m_description As String
        Dim pageobj As ExpandIT.Page = CType(System.Web.HttpContext.Current.Handler, ExpandIT.Page)


        Public Sub New(ByVal data As ExpDictionary)
            m_data = data
        End Sub

        Public ReadOnly Property Self() As ProductClass
            Get
                Return Me
            End Get
        End Property

        Default Public Property Item(ByVal key As Object) As Object
            Get
                Return m_data(key)
            End Get
            Set(ByVal value As Object)
                m_data(key) = value
            End Set
        End Property

        Public Property Data() As ExpDictionary
            Get
                Return m_data
            End Get
            Set(ByVal value As ExpDictionary)
                m_data = value
            End Set
        End Property

        Public ReadOnly Property Guid() As String
            Get
                Return CStrEx(m_data("ProductGuid"))
            End Get
        End Property

        Public ReadOnly Property Name() As String
            Get
                Return CStrEx(m_data("ProductName"))
            End Get
        End Property

        Public ReadOnly Property ThumbnailURL() As String
            Get
                Dim retv As String = CStrEx(m_data("PICTURE2"))
                If retv <> "" And System.IO.File.Exists(ApplicationRoot() & "/" & retv) Then
                    retv = PictureLink(retv)
                Else
                    retv = ""
                    Dim di1 As New System.IO.DirectoryInfo(ApplicationRoot() & "/catalog/images/thumbnails/")
                    Dim fi1() As System.IO.FileInfo = di1.GetFiles(Guid() & ".*")
                    If Not fi1 Is Nothing AndAlso fi1.Length > 0 Then
                        For Each f1 As Object In fi1
                            retv = PictureLink("catalog/images/thumbnails/" & f1.Name)
                            Exit For
                        Next
                    End If
                    If retv = "" Then
                        Dim di As New System.IO.DirectoryInfo(ApplicationRoot() & "/catalog/images/zoom/Product_Small")
                        Dim fi() As System.IO.FileInfo = di.GetFiles(Guid() & "_1_*.*")
                        If Not fi Is Nothing AndAlso fi.Length > 0 Then
                            For Each f As Object In fi
                                retv = PictureLink("catalog/images/zoom/Product_Small/" & f.Name)
                                Exit For
                            Next
                        End If
                    End If
                End If
                If retv = "" Then
                    retv = PictureLink("images/nopix100.png")
                End If
                Return retv
            End Get
        End Property

        Public ReadOnly Property ThumbnailURLStyle(ByVal wd As Integer, ByVal hd As Integer) As String
            Get
                Return CStrEx(pageobj.eis.ThumbnailURLStyleMethod(m_data("PICTURE2"), wd, hd))
            End Get
        End Property
        

        Public ReadOnly Property PictureURL() As String
            Get
                Dim retv As String = CStrEx(m_data("PICTURE1"))
                If retv <> "" And System.IO.File.Exists(ApplicationRoot() & "/" & retv) Then
                    retv = PictureLink(retv)
                Else
                    retv = PictureLink("images/nopix75.png")
                End If
                Return retv
            End Get
        End Property

        Public ReadOnly Property QtyOnHand() As Double
            Get
                Return m_data("QtyOnHand")
            End Get
        End Property

        Public ReadOnly Property PriceFormatted() As String
            Get
                Return CurrencyFormatter.FormatCurrency(Price, HttpContext.Current.Session("UserCurrencyGuid").ToString)
            End Get
        End Property

        Public ReadOnly Property PictureTAG() As String
            Get
                Dim retv As String = CStrEx(m_data("PICTURE1"))
                If retv <> "" And System.IO.File.Exists(ApplicationRoot() & "/" & retv) Then
                    retv = PictureLink(retv)
                Else
                    retv = PictureLink("images/nopix75.png")
                End If
                Return retv
            End Get
        End Property

        Public ReadOnly Property HTMLDescription() As String
            Get
                Dim retv As String = CStrEx(m_data("HTMLDESC"))
                Dim newHtml As String = ""
                If retv <> "" Then
                    '<!--JM2010061001 - LINK TO - Start-->
                    newHtml = pageobj.eis.findLinkToHtml(retv)
                    '<!--JM2010061001 - LINK TO - End-->
                Else
                    newHtml = Description()
                End If
                Return newHtml
            End Get
        End Property

        Public ReadOnly Property Description() As String
            Get
                Dim retv As String = CStrEx(m_data("DESCRIPTION"))
                Return Replace(HTMLEncode(retv), vbCrLf, "<br/>")
            End Get
        End Property

        Public Property Description2() As String
            Get
                Return m_description
            End Get
            Set(ByVal value As String)
                m_description = value
            End Set
        End Property

        Public ReadOnly Property ThumbnailText() As String
            Get
                Return Name
            End Get
        End Property

        Public ReadOnly Property ProductLinkURL() As String
            Get
                Return ProductLink(Guid, 0) & IIf(m_groupguid Is Nothing, "", "&GroupGuid=" & m_groupguid)
            End Get
        End Property

        Public Property GroupGuid() As String
            Get
                Return m_groupguid
            End Get
            Set(ByVal value As String)
                m_groupguid = value
            End Set
        End Property

        Public ReadOnly Property Price() As Decimal
            Get
                If AppSettings("SHOW_TAX_TYPE") = "EXCL" Then
                    Return CDblEx(m_data("LineTotal"))
                Else
                    Return CDblEx(m_data("TotalInclTax"))
                End If
            End Get
        End Property

        Public ReadOnly Property SecondaryPrice() As Decimal
            Get
                ' Get the type that is not primary
                If AppSettings("SHOW_TAX_TYPE") = "EXCL" Then
                    Return CDblEx(m_data("TotalInclTax"))
                Else
                    Return CDblEx(m_data("LineTotal"))
                End If
            End Get
        End Property

        Public ReadOnly Property ShowSecondaryPrice() As Boolean
            Get
                If AppSettings("SHOW_TAX_TYPE") = "EXCL" Then
                    Return CBoolEx(AppSettings("SHOW_INCL_TAX"))
                Else
                    Return CBoolEx(AppSettings("SHOW_EXCL_TAX"))
                End If
            End Get
        End Property

        ' AM2010051802 - WHAT's NEW - START
        Public ReadOnly Property newItem() As String
            Get
                Dim dateNew As Object
                Dim whatsNew As Boolean = CBoolEx(m_data("NewItem"))
                If whatsNew Then
                    Return CBoolEx(whatsNew)
                Else
                    dateNew = m_data("DateAdded")
                    If dateNew Is Nothing Or dateNew Is DBNull.Value Then
                        Return False
                    Else
                        If Today().Subtract(CDateEx(dateNew)).Days < 30 Then
                            Return True
                        Else
                            Return False
                        End If
                    End If
                End If
            End Get
        End Property
        'AM2010051802 - WHAT's NEW - End

        'AM2010051801 - SPECIAL ITEMS - Start
        Public ReadOnly Property OriginalPrice() As Decimal
            Get
                Return CDblEx(m_data("originalListPrice"))
            End Get
        End Property

        Public ReadOnly Property IsSpecial() As Boolean
            Get
                Return CBoolEx(m_data("IsSpecial"))
            End Get
        End Property
        'AM2010051802 - WHAT's NEW - Start
        Public ReadOnly Property LastName() As String
            Get
                Return CStrEx(m_data("LastName"))
            End Get
        End Property
        'AM2010051802 - WHAT's NEW - End
        'AM2010051801 - SPECIAL ITEMS - End

        'AM2010051802 - WHAT's NEW - Start
        Public ReadOnly Property IsNew() As Boolean
            Get
                Return CBoolEx(m_data("IsNew"))
            End Get
        End Property
        'AM2010051802 - WHAT's NEW - End

        ' **** Product Zoom Functionality **** START
        Public ReadOnly Property ProductPictureBig1() As String
            Get
                Dim retv As String = ""
                If CStrEx(m_data("PRODUCTBIG1")) <> "" Then
                    retv = "../" & CStrEx(m_data("PRODUCTBIG1"))
                End If
                Return retv
            End Get
        End Property
        Public ReadOnly Property ProductPictureBig2() As String
            Get
                Dim retv As String = ""
                If CStrEx(m_data("PRODUCTBIG2")) <> "" Then
                    retv = "../" & CStrEx(m_data("PRODUCTBIG2"))
                End If
                Return retv
            End Get
        End Property
        Public ReadOnly Property ProductPictureBig3() As String
            Get
                Dim retv As String = ""
                If CStrEx(m_data("PRODUCTBIG3")) <> "" Then
                    retv = "../" & CStrEx(m_data("PRODUCTBIG3"))
                End If
                Return retv
            End Get
        End Property
        Public ReadOnly Property ProductPictureSmall1() As String
            Get
                Dim retv As String = ""
                If CStrEx(m_data("PRODUCTSMALL1")) <> "" Then
                    retv = "../" & CStrEx(m_data("PRODUCTSMALL1"))
                End If
                Return retv
            End Get
        End Property
        Public ReadOnly Property ProductPictureSmall2() As String
            Get
                Dim retv As String = ""
                If CStrEx(m_data("PRODUCTSMALL2")) <> "" Then
                    retv = "../" & CStrEx(m_data("PRODUCTSMALL2"))
                End If
                Return retv
            End Get
        End Property
        Public ReadOnly Property ProductPictureSmall3() As String
            Get
                Dim retv As String = ""
                If CStrEx(m_data("PRODUCTSMALL3")) <> "" Then
                    retv = "../" & CStrEx(m_data("PRODUCTSMALL3"))
                End If
                Return retv
            End Get
        End Property
        Public ReadOnly Property ProductPictureAlt1() As String
            Get
                Dim retv As String = ""
                If CStrEx(m_data("PRODUCTALT1")) <> "" Then
                    retv = CStrEx(m_data("PRODUCTALT1"))
                End If
                Return retv
            End Get
        End Property
        Public ReadOnly Property ProductPictureAlt2() As String
            Get
                Dim retv As String = ""
                If CStrEx(m_data("PRODUCTALT2")) <> "" Then
                    retv = CStrEx(m_data("PRODUCTALT2"))
                End If
                Return retv
            End Get
        End Property
        Public ReadOnly Property ProductPictureAlt3() As String
            Get
                Dim retv As String = ""
                If CStrEx(m_data("PRODUCTALT3")) <> "" Then
                    retv = CStrEx(m_data("PRODUCTALT3"))
                End If
                Return retv
            End Get
        End Property
        ' **** Product Zoom Functionality **** End




        'JA2010032101 - META TAGS -Start
        Public ReadOnly Property ProductMetaTagDescription() As String
            Get
                Dim retv As String = ""
                If CStrEx(m_data("METATAGDESCRIPTION")) <> "" Then
                    retv = CStrEx(m_data("METATAGDESCRIPTION"))
                End If
                Return retv
            End Get
        End Property

        Public ReadOnly Property ProductMetaTagKeywords() As String
            Get
                Dim retv As String = ""
                If CStrEx(m_data("METATAGKEYWORDS")) <> "" Then
                    retv = CStrEx(m_data("METATAGKEYWORDS"))
                End If
                Return retv
            End Get
        End Property
        'JA2010032101 - META TAGS -End




    End Class

End Namespace
