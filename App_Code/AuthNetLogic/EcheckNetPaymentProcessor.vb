﻿Imports Microsoft.VisualBasic
Imports System.Configuration.ConfigurationManager
Imports System.Net
Imports System.IO
Imports ExpandIT.ExpandITLib

Namespace ExpandIT
    '<!--JA2010022601 - ECHECK - Start-->

    ''' <summary>
    ''' Handles the communication between the shop and the authorize.net payment gateway
    ''' during integrated credit card payments.
    ''' The class also handles the validation of authnet gateway responses.    
    ''' </summary>
    ''' <remarks></remarks>
    Public Class EcheckNetPaymentProcessor

        'The Authorize.net response is a delimited string.
        'These Const values reflects how the delimited response string is ordered,
        'starting at 0, making it easier to split the content into an array.
        Const responseCode As Integer = 0
        Const responseSubCode As Integer = 1
        Const responseReasonCode As Integer = 2
        Const responseReasonText As Integer = 3
        Const authorizationCode As Integer = 4
        Const avsResponse As Integer = 5
        Const transactionID As Integer = 6
        Const invoiceNumber As Integer = 7
        Const amount As Integer = 9
        Const method As Integer = 10
        Const transactionType As Integer = 11
        Const MD5Hash As Integer = 37

        'Authorize.net web.config values
        Private AuthNetHashValue As String = AppSettings("AUTHORIZENET_MD5_HASH_VALUE")
        Private AuthNetLogin As String = EISClass.getAuthorizeNetLogin()
        'Private AuthNetLogin As String = AppSettings("AUTHORIZENET_LOGIN")

        ''' <summary>
        ''' The class's public entry point.
        ''' Issues a request by calling EcheckNetRequest. 
        ''' Sends the authNet response to validation.
        ''' </summary>
        ''' <param name="url"></param>
        ''' <param name="OrderDict"></param>
        ''' <param name="ErrorString"></param>
        ''' <returns>True if authorize.net response passes validation</returns>
        ''' <remarks></remarks>
        Public Function EcheckNetProcessPayment(ByVal url As String, ByVal routing As String, ByVal accountNumber As String, ByVal typeBank As String, ByVal BankName As String, ByVal associatedName As String, ByVal ErrorString As String, ByVal OrderDict As ExpDictionary) As String
            Dim result As String = String.Empty
            Dim response As String
            Dim authNetResponse As String = EcheckNetRequest(url, routing, accountNumber, typeBank, BankName, associatedName, OrderDict)
            Dim responseFields() As String = authNetResponse.Split("|")
            Dim success As Boolean = ValidatePayment(responseFields, ErrorString)
            If success Then
                response = "True*" & responseFields(transactionID) & "*" & responseFields(amount) & "*" & responseFields(MD5Hash) & "*" & responseFields(responseReasonCode) & "*" & responseFields(responseReasonText) & "*" & responseFields(responseCode) & "*" & responseFields(responseSubCode)
                Return response
            Else
                Return ErrorString
            End If
        End Function


        ''' <summary>
        ''' Builds and sends the request to authorize.net payment gateway,
        ''' and reads the response.        
        ''' </summary>
        ''' <param name="url"></param>
        ''' <param name="OrderDict"></param>
        ''' <returns>The response from authorize.net payment gateway</returns>
        ''' <remarks></remarks>
        Private Function EcheckNetRequest(ByVal url As String, ByVal routing As String, ByVal accountNumber As String, ByVal typeBank As String, ByVal BankName As String, ByVal associatedName As String, ByVal OrderDict As ExpDictionary) As String
            Dim authResponse As String = ""
            Dim sb As New StringBuilder()

            'Add fields to be sent with the request. 
            'Using StringBuilder for simplicity and readability.
            'sb.Append("x_login=" & AppSettings("AUTHORIZENET_LOGIN"))
            sb.Append("x_login=" & EISClass.getAuthorizeNetLogin())
            'sb.Append("&x_tran_key=" & AppSettings("AUTHORIZENET_TRANSACTION_KEY"))
            sb.Append("&x_tran_key=" & EISClass.getAuthorizeNetTransactionKey())
            sb.Append("&x_method=ECHECK&x_type=AUTH_CAPTURE")
            sb.Append("&x_amount=" & AuthorizeNet_FormatAmount(OrderDict("TotalInclTax")))
            sb.Append("&x_delim_data=TRUE&x_delim_char=|&x_relay_response=FALSE")
            sb.Append("&x_bank_aba_code=" & routing)
            sb.Append("&x_bank_acct_num=" & accountNumber)
            sb.Append("&x_bank_acct_type=" & typeBank)
            sb.Append("&x_bank_name=" & BankName)
            sb.Append("&x_bank_acct_name=" & associatedName)
            'sb.Append("&x_echeck_type=" & typeCheck)
            'If typeCheck = "ARC" Or typeCheck = "BOC" Then
            '    sb.Append("&x_bank_check_number=" & CheckNumber)
            'End If

            'If typeCheck = "WEB" Then
            sb.Append("&x_recurring_billing=F")
            'End If
            sb.Append("&x_version=3.1")




            Dim strPost As String = sb.ToString()
            Dim myWriter As StreamWriter = Nothing

            'Create the request
            Dim objRequest As HttpWebRequest = CType(WebRequest.Create(url), HttpWebRequest)
            objRequest.Method = "POST"
            objRequest.ContentLength = strPost.Length
            objRequest.ContentType = "application/x-www-form-urlencoded"

            Try
                myWriter = New StreamWriter(objRequest.GetRequestStream())
                myWriter.Write(strPost)
            Catch e As Exception
                Return e.Message
            Finally
                myWriter.Close()
            End Try

            'Read the response
            Dim objResponse As HttpWebResponse = CType(objRequest.GetResponse(), HttpWebResponse)
            Dim sr As New StreamReader(objResponse.GetResponseStream())
            authResponse = sr.ReadToEnd()

            'Close and clean up the StreamReader
            sr.Close()

            'Return the response
            Return authResponse

        End Function

        ''' <summary>
        ''' Validates the payment
        ''' </summary>
        ''' <param name="responseFields"></param>
        ''' <param name="errString"></param>
        ''' <returns>True if all validation steps succeeds</returns>
        ''' <remarks></remarks>
        Private Function ValidatePayment(ByVal responseFields() As String, ByRef errString As String) As Boolean

            'Read response code
            If responseFields(responseCode) <> "1" Then
                'The transaction has been declined, abort
                errString = responseFields(responseCode) & ", " & responseFields(responseReasonText)
                Return False
            End If

            'Validate MD5 Hash
            If Not validateHash(AuthNetHashValue, AuthNetLogin, responseFields(MD5Hash), responseFields(transactionID), responseFields(amount)) Then
                'MD5 Hash validation failed, abort
                Return False
            End If

            'All validation steps succeeded
            Return True

        End Function

        ''' <summary>
        ''' Validates the authorize.net response hashed amount against 
        ''' a hashed version of the order amount        
        ''' </summary>
        ''' <param name="storedHashValue"></param>
        ''' <param name="login"></param>
        ''' <param name="responseHashValue"></param>
        ''' <param name="transID"></param>
        ''' <param name="transAmount"></param>
        ''' <returns>True if validation succeeded</returns>
        ''' <remarks></remarks>
        Private Function validateHash(ByVal storedHashValue As String, ByVal login As String, ByVal responseHashValue As String, ByVal transID As String, ByVal transAmount As String) As Boolean
            Dim x_MD5_Hash_Calc As String = UCase(ExpandIT.Crypto.coreMD5(storedHashValue & login & transID & transAmount))
            If responseHashValue = x_MD5_Hash_Calc Then
                Return True
            End If
        End Function

        REM --- Internal helper functions

        Function AuthorizeNet_FormatAmount(ByVal aValue As Double) As String
            Return Replace(FormatNumber(aValue, -1, -1, 0, 0), ",", ".")
        End Function

        Private Sub splitNames(ByRef fName As String, ByRef lName As String, ByVal strs As String())

            If strs IsNot Nothing Then
                Dim sbFirstName As New StringBuilder()
                Dim strLastName As String = String.Empty
                Dim strlen As Integer = strs.Length
                If strlen > 1 Then
                    For i As Integer = 0 To strlen - 2
                        sbFirstName.Append(strs(i))
                        If i < strlen - 2 Then
                            sbFirstName.Append(" ")
                        End If
                    Next
                    strLastName = strs(strs.Length - 1)
                ElseIf strs.Length = 1 Then
                    sbFirstName.Append(strs(0))
                    strLastName = strs(0)
                End If
                fName = sbFirstName.ToString
                lName = strLastName
            End If

        End Sub

        Private Function split(ByVal str As String) As String()
            Return str.Split(New Char() {CChar(" ")})
        End Function

    End Class
    '<!--JA2010022601 - ECHECK - End-->
End Namespace

