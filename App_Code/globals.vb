Imports ExpandIT.EISClass
Imports ExpandIT31.ExpanditFramework.Infrastructure
Imports Microsoft.VisualBasic
Imports System.Configuration.ConfigurationManager

Namespace ExpandIT

    Public Class GlobalsClass

        ' Global variables.
        Public ApplicationIsAlreadyLocked As Boolean = False
        Public bCartLoaded As Boolean
        Public bUseMultiCurrency As Boolean
        Public bUserLoaded As Boolean = False
        Public catalogTreeHTML As String = Nothing
        Public breadcrumbTrail As BreadcrumbTrail = Nothing
        Public Context As ExpDictionary
        Public DebugLogic As Boolean
        Public DebugStartTimer As Decimal
        Public dictErrorInfo As ExpDictionary
        Public dictFormError As ExpDictionary
        Public dictFormOutput As ExpDictionary
        Public dictFormSuccess As ExpDictionary
        Public messages As PageMessages = New PageMessages
        Public errstr As String
        Public g_LastExchangeRate As Decimal
        Public g_LastFromCurrency As String
        Public g_LastToCurrency As String
        Public GroupGuid As String
        Public IsCartEmpty As Boolean
        Public Menu As SiteMenu
        Public MenuBottom As SiteMenuBottom
        Public MiniCartInfo As ExpDictionary
        Public ModuleName As String
        Public MulticurrencyEnabled As Boolean
        Public OrderDict As ExpDictionary
        Public strReturl As String
        Public URL As String
        Public Shadows User As ExpDictionary
        Public WebConnectionString As String
        Public WebDBFilename As String
        Public bShowUpdateableForm As Boolean

        Public DebugMode As Boolean
        Public dictRequest As ExpDictionary
        Public dictFieldInfo As ExpDictionary
        Public MailError As ExpDictionary
        Public StyleSheetRowCounter As Integer = 0

        Public currency As CurrencyBaseClass
        Public cart As CartClass
        Public buslogic As BuslogicBaseClass
        Public b2b As B2BBaseClass
        Public eis As EISClass
        Public store As StoreClass
        'ShopID
        Public shopID As String = ""
        '*****SHIPPING*****-START
        Public shippingError As String = ""
        '*****SHIPPING*****-END

        Public Sub New(ByVal initStore As Boolean)
            Dim BackendType As String = ConfigurationManager.AppSettings("BackendType")
            ApplicationIsAlreadyLocked = False
            MailError = New ExpDictionary
            dictFormSuccess = New ExpDictionary
            dictFormOutput = New ExpDictionary

            If ExpandITLib.CBoolEx(ConfigurationManager.AppSettings("MultiCurrencyEnabled")) Then
                currency = New MultiCurrencyClass(Me)
            Else
                currency = New SingleCurrencyClass(Me)
            End If

            cart = New CartClass(Me)
            Select Case BackendType
                Case "NAV"
                    b2b = New B2BNAVClass(Me)
                    buslogic = New BuslogicNAVClass(Me)
                Case "AX"
                    b2b = New B2BAxaptaClass(Me)
                    buslogic = New BuslogicAXClass(Me)
                Case "C5"
                    b2b = New B2BC5Class(Me)
                    buslogic = New BuslogicC5Class(Me)
                Case "NF"
                    b2b = New B2BNFClass(Me)
                    buslogic = New BuslogicNFClass(Me)
                Case "XAL"
                    b2b = New B2BXALClass(Me)
                    buslogic = New BuslogicXALClass(Me)
                Case Else
                    b2b = New B2BNoneClass(Me)
                    buslogic = New BuslogicNoneClass(Me)
            End Select

            eis = New EISClass(Me)
            If initStore Then
                store = New StoreClass(Me)
                Menu = New SiteMenu(Me)
                MenuBottom = New SiteMenuBottom(Me)
            End If

            If Not AppSettings("SHOP_ID") Is Nothing Then
              shopID = AppSettings("SHOP_ID")
            End If
        End Sub

    End Class

End Namespace