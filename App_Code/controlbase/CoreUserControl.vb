Imports Microsoft.VisualBasic
Imports System.Reflection
Imports System.Reflection.Emit
Imports System.Threading

Namespace ExpandIT

    Public Class CoreUserControl
        Inherits System.Web.UI.UserControl
        Implements ICallbackEventHandler

        Private callBackResult As String
        Private controlToChangeOnCallback As Control
        Private exec As System.Delegate

        ''' <summary>
        ''' Public Property sets the control that will receive and present the updated value when a callback returns
        ''' </summary>
        ''' <value></value>
        ''' <remarks></remarks>
        Public Property ControlToUpdateOnCallback() As Object
            Get
                Return controlToChangeOnCallback
            End Get
            Set(ByVal value As Object)
                Try
                    If value.GetType().Name.Equals("String") Then
                        SetControlToChangeOnCallback(findNamedControl(Me.Page, value, New Control()))
                    Else
                        SetControlToChangeOnCallback(value)
                    End If
                Catch ex As Exception
                    If Trace.IsEnabled Then
                        Trace.Warn(ex.Message)
                    End If
                End Try
            End Set
        End Property

        ''' <summary>
        ''' Internal function to set the control that will receive and present the updated value when a callback returns
        ''' </summary>
        ''' <param name="ctrl"></param>
        ''' <remarks></remarks>
        Private Sub SetControlToChangeOnCallback(ByVal ctrl As Control)
            Try
                If ctrl IsNot Nothing Then
                    controlToChangeOnCallback = ctrl
                End If
            Catch ex As InvalidCastException
                If Trace.IsEnabled Then
                    Trace.Warn(ex.Message)
                End If
            End Try
        End Sub

        ''' <summary>
        ''' Instructs the private delegate what method to execute
        ''' </summary>
        ''' <value></value>
        ''' <remarks></remarks>
        Public WriteOnly Property ExecFunction() As System.Delegate
            Set(ByVal value As System.Delegate)
                exec = value
            End Set
        End Property

        Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
            Return callBackResult
        End Function

        Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
            Thread.Sleep(200)
            callBackResult = exec.DynamicInvoke(eventArgument)
        End Sub

        Public Function setCallScript(ByVal arg As Object, ByVal controlToSet As Control, Optional ByVal returnStr As String = "") As String
            Dim strJSCallbackPrefix As String = Me.ClientID
            Dim callScript As String = strJSCallbackPrefix & "CallServer(" & arg & ", document.getElementById('" & controlToSet.ClientID & "'));"

            ' Return the script
            Return callScript
        End Function

        Public Sub registerCallbackScript()
            Dim strJSCallbackPrefix As String = Me.ClientID

            Dim cm As ClientScriptManager = Page.ClientScript
            Dim cbReference As String
            cbReference = cm.GetCallbackEventReference(Me, "arg", strJSCallbackPrefix & "ReceiveServerData", _
                "context")
            ' Declare the function that will be called to fire off a
            ' client callback to the server.
            Dim callbackScript As String = _
                "function " & strJSCallbackPrefix & "CallServer(arg, context){" & cbReference & "; }"
            cm.RegisterClientScriptBlock(Me.GetType(), strJSCallbackPrefix & "CallServer", callbackScript, True)

            ' Declare the function that will recieve the client callback
            ' results from the server.
            Dim strReceiveServerData As String = _
                "function " & strJSCallbackPrefix & "ReceiveServerData(arg, context){context.innerHTML = arg;}"
            cm.RegisterClientScriptBlock(Me.GetType, strJSCallbackPrefix & "ReceiveServerData", strReceiveServerData, True)
        End Sub

        Public Function findNamedControl(ByVal Parent As Control, ByVal name As String, ByRef c As Control) As Control
            Dim child As Control = Parent
            For Each child In Parent.Controls
                If child.ID = name Then
                    c = child
                Else
                    findNamedControl(child, name, c)
                End If
            Next
            Return c
        End Function

    End Class

End Namespace
