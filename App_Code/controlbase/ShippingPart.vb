Imports System.Configuration.ConfigurationManager
Imports Microsoft.VisualBasic
Imports ExpandIT
Imports ExpandIT.EISClass
Imports System.Collections.Generic

Namespace ExpandIT

    Public MustInherit Class ShippingPart
        Inherits ExpandIT.ShippingUserControl

        Public Overridable Function convertToChecked(ByVal isDefault As Boolean) As Boolean
            If isDefault Then
                Return True
            End If
        End Function

        Public Overridable Function convertToChecked(ByVal isDefault As DBNull) As Boolean

        End Function

    End Class

End Namespace
