Imports System.Configuration.ConfigurationManager
Imports Microsoft.VisualBasic
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Collections.Generic

Namespace ExpandIT
    ''' <summary>
    ''' Abstract Class that serves as the base for all ShippingUserConrols
    ''' </summary>
    ''' <remarks></remarks>
    Public MustInherit Class ShippingUserControl
        Inherits ExpandIT.ShippingPaymentUserControl

        Public Overridable ReadOnly Property GetDataList() As DataList
            Get
                Return Nothing
            End Get
        End Property

        Public Overridable ReadOnly Property GetDataList2() As DataList
            Get
                Return Nothing
            End Get
        End Property

        Public Overridable ReadOnly Property shipAddrData() As ExpDictionary
            Get
                Return Nothing
            End Get
        End Property

        Public ReadOnly Property MySelf() As ExpandIT.ShippingUserControl
            Get
                Return Me
            End Get
        End Property

        Function CountryGuidsToText(ByVal guid As Object, ByVal CountryGuids As ExpDictionary, _
            ByVal CountryNames As ExpDictionary) As String
            Dim a As Object
            For Each a In CountryNames.Values
                If CStrEx(CountryGuids(a)).Equals(CStrEx(guid)) Then
                    ' Changed to get rid of HTMLEncode that caused problem with unicode characters
                    Return a
                End If
            Next
            Return ""
        End Function

        Public Function CountryGuidsToText(ByVal guid As Object) As String
            ' Changed to make use of a better method call
            Try
                Return getSingleValueDB("SELECT CountryName FROM CountryTable WHERE CountryGuid = " & SafeString(guid))
            Catch ex As Exception

            End Try
            Return String.Empty
        End Function

    End Class
End Namespace