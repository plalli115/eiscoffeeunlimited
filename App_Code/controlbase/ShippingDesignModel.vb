Imports System.Configuration.ConfigurationManager
Imports Microsoft.VisualBasic
Imports ExpandIT
Imports ExpandIT.EISClass
Imports System.Collections.Generic

Namespace ExpandIT

    Public MustInherit Class ShippingDesignModel
        Inherits ExpandIT.ShippingUserControl

        Private m_IsMultiplePaymentShippingOption As String = "block"

        Private functionTag As String = "<script type=""text/javascript"">" + Environment.NewLine + _
            "    function {0}{1}" + Environment.NewLine + "{2,4}{3,4}" + Environment.NewLine + "</script>"

        Private tag As String = "<script type=""text/javascript"">" + Environment.NewLine + _
            "    {0}" + Environment.NewLine + "</script>"

        Protected fString As String = "<script type=""text/javascript"">" & Environment.NewLine & "{0}" & _
            Environment.NewLine & "{1}{2}" & Environment.NewLine & "{3}" & Environment.NewLine & "</script>"

        Public Property IsMultiplePaymentShippingOption() As String
            Get
                Return m_IsMultiplePaymentShippingOption
            End Get
            Set(ByVal value As String)
                m_IsMultiplePaymentShippingOption = value
            End Set
        End Property

        Public ReadOnly Property ScriptBlockFunction(ByVal functionName As String, ByVal functionBody As String) As String
            Get
                Return String.Format(functionTag, functionName, "{", functionBody, "}") + Environment.NewLine
            End Get
        End Property

        Public ReadOnly Property ScriptBlock(ByVal block As String) As String
            Get
                Return String.Format(tag, block) + Environment.NewLine
            End Get
        End Property

        Protected Sub controlIdCopy(ByRef str As String, ByRef CreateString As String, ByVal dataLst As DataList, _
            ByVal textBoxDictSrc As Dictionary(Of String, Control), ByVal drpDwnListDest As Dictionary(Of String, Control), _
            ByVal textBoxDictDest As Dictionary(Of String, Control), ByVal drpDwnListSrc As Dictionary(Of String, Control), _
            ByVal hiddendict As Dictionary(Of String, Control), ByVal inx As Integer, ByVal hit As Integer, _
            ByVal lblDictDest As Dictionary(Of String, Control), ByRef str2 As String, ByRef CreateString2 As String, _
            ByVal inx2 As Integer, ByVal hit2 As Integer)

            CreateString += createNewJSArray(inx)
            If CreateString2 IsNot Nothing Then
                CreateString2 += createNewJSArray2(inx)
            End If
            ' loop through output textBoxDict, ie values from the output part of the page
            For Each item As KeyValuePair(Of String, Control) In textBoxDictSrc
                Dim en As Dictionary(Of String, Control).KeyCollection.Enumerator = drpDwnListDest.Keys.GetEnumerator
                Dim en2 As Dictionary(Of String, Control).KeyCollection.Enumerator = textBoxDictDest.Keys.GetEnumerator
                ' first check if there are any dropDownLists with the same ID's in the input part
                For i As Integer = 0 To drpDwnListDest.Count - 1
                    en.MoveNext()
                    If item.Key.Contains(drpDwnListDest.Item(en.Current).ID) Then
                        str += createJSArray(item.Value.ClientID, drpDwnListDest.Item(en.Current).ClientID, inx, hit)
                        hit += 1
                    End If
                Next
                ' then check if there are any textBoxes with the same ID's in the input part
                For i As Integer = 0 To textBoxDictDest.Count - 1
                    en2.MoveNext()
                    If item.Key.Contains(textBoxDictDest.Item(en2.Current).ID) Then
                        str += createJSArray(item.Value.ClientID, textBoxDictDest.Item(en2.Current).ClientID, inx, hit)
                        hit += 1
                    End If
                Next
                If lblDictDest IsNot Nothing Then
                    Dim en3 As Dictionary(Of String, Control).KeyCollection.Enumerator = lblDictDest.Keys.GetEnumerator
                    ' check if there are any labels with the same (almost) ID's in the input part
                    For i As Integer = 0 To lblDictDest.Count - 1
                        en3.MoveNext()
                        Dim strTemp As String = lblDictDest.Item(en3.Current).ID
                        If strTemp.Contains("LabelForOutput") Then
                            Dim strTemp2 As String = strTemp.Substring(0, strTemp.Length - "LabelForOutput".Length)

                            If item.Key.Contains(strTemp2) Then
                                str2 += createJSArray2(item.Value.ClientID, lblDictDest.Item(en3.Current).ClientID, inx2, hit2)
                                hit2 += 1
                            End If
                        End If
                    Next
                End If
            Next
            ' loop through output dropDownListDict, ie values from the output part of the page
            For Each item As KeyValuePair(Of String, Control) In drpDwnListSrc
                Dim en As Dictionary(Of String, Control).KeyCollection.Enumerator = drpDwnListDest.Keys.GetEnumerator
                Dim en2 As Dictionary(Of String, Control).KeyCollection.Enumerator = textBoxDictDest.Keys.GetEnumerator
                ' first check if there are any dropDownLists with the same ID's in the input part
                For i As Integer = 0 To drpDwnListDest.Count - 1
                    en.MoveNext()
                    If item.Key.Contains(drpDwnListDest.Item(en.Current).ID) Then
                        str += createJSArray(item.Value.ClientID, drpDwnListDest.Item(en.Current).ClientID, inx, hit)
                        hit += 1
                    End If
                Next
                ' then check if there are any textBoxes with the same ID's in the input part
                For i As Integer = 0 To textBoxDictDest.Count - 1
                    en2.MoveNext()
                    If item.Key.Contains(textBoxDictDest.Item(en2.Current).ID) Then
                        str += createJSArray(item.Value.ClientID, textBoxDictDest.Item(en2.Current).ClientID, inx, hit)
                        hit += 1
                    End If
                Next
                If lblDictDest IsNot Nothing Then
                    Dim en3 As Dictionary(Of String, Control).KeyCollection.Enumerator = lblDictDest.Keys.GetEnumerator
                    ' check if there are any labels with the same (almost) ID's in the input part
                    For i As Integer = 0 To lblDictDest.Count - 1
                        en3.MoveNext()
                        Dim strTemp As String = lblDictDest.Item(en3.Current).ID
                        If strTemp.Contains("LabelForOutput") Then
                            Dim strTemp2 As String = strTemp.Substring(0, strTemp.Length - "LabelForOutput".Length)

                            If item.Key.Contains(strTemp2) Then
                                str2 += createJSArray2(item.Value.ClientID, lblDictDest.Item(en3.Current).ClientID, inx2, hit2)
                                hit2 += 1
                            End If
                        End If
                    Next
                End If
            Next
            ' loop through output hiddenfields to set input textboxes and dropDownLists
            For Each item As KeyValuePair(Of String, Control) In hiddendict
                Dim en As Dictionary(Of String, Control).KeyCollection.Enumerator = drpDwnListDest.Keys.GetEnumerator
                Dim en2 As Dictionary(Of String, Control).KeyCollection.Enumerator = textBoxDictDest.Keys.GetEnumerator
                ' first check if there are any dropDownLists with the same ID's in the input part
                For i As Integer = 0 To drpDwnListDest.Count - 1
                    en.MoveNext()
                    If item.Key.Contains(drpDwnListDest.Item(en.Current).ID) Then
                        str += createJSArray(item.Value.ClientID, drpDwnListDest.Item(en.Current).ClientID, inx, hit)
                        hit += 1
                    End If
                Next
                ' then check if there are any textBoxes with the same ID's in the input part
                For i As Integer = 0 To textBoxDictDest.Count - 1
                    en2.MoveNext()
                    If item.Key.Contains(textBoxDictDest.Item(en2.Current).ID) Then
                        str += createJSArray(item.Value.ClientID, textBoxDictDest.Item(en2.Current).ClientID, inx, hit)
                        hit += 1
                    End If
                Next
                If lblDictDest IsNot Nothing Then
                    Dim en3 As Dictionary(Of String, Control).KeyCollection.Enumerator = lblDictDest.Keys.GetEnumerator
                    ' check if there are any labels with the same (almost) ID's in the input part
                    For i As Integer = 0 To lblDictDest.Count - 1
                        en3.MoveNext()
                       Dim strTemp As String = lblDictDest.Item(en3.Current).ID
                        If strTemp.Contains("LabelForOutput") Then
                            Dim strTemp2 As String = strTemp.Substring(0, strTemp.Length - "LabelForOutput".Length)

                            If item.Key.Contains(strTemp2) Then
                                str2 += createJSArray2(item.Value.ClientID, lblDictDest.Item(en3.Current).ClientID, inx2, hit2)
                                hit2 += 1
                            End If
                        End If
                    Next
                End If
            Next
        End Sub

        Protected Sub controlIdCopy(ByRef str As String, ByRef CreateString As String, ByVal dataLst As DataList, _
            ByVal textBoxDictSrc As Dictionary(Of String, Control), ByVal drpDwnListDest As Dictionary(Of String, Control), _
            ByVal textBoxDictDest As Dictionary(Of String, Control), ByVal drpDwnListSrc As Dictionary(Of String, Control), _
            ByVal hiddendict As Dictionary(Of String, Control), ByVal inx As Integer, ByVal hit As Integer)

            controlIdCopy(str, CreateString, dataLst, textBoxDictSrc, drpDwnListDest, textBoxDictDest, drpDwnListSrc, hiddendict, inx, hit, Nothing, Nothing, Nothing, 0, 0)
        End Sub

        Protected Function createNewJSArray2(ByVal index As Integer) As String
            Dim sb As StringBuilder = New StringBuilder()
            sb.AppendLine("sArrayB[" & index & "] = new Array();")
            sb.AppendLine("dArrayB[" & index & "] = new Array();")
            Return sb.ToString()
        End Function

        Protected Function createJSArray2(ByVal strSource As String, ByVal strDest As String, ByVal aNr As Integer, ByVal index As Integer) As String
            Dim sb As StringBuilder = New StringBuilder()
            sb.AppendLine(String.Format("sArrayB[{0}][{1}] = ""{2}"";", aNr, index, strSource))
            sb.AppendLine(String.Format("dArrayB[{0}][{1}] = ""{2}"";", aNr, index, strDest))
            Return sb.ToString()
        End Function

        Protected Function createNewJSArray(ByVal index As Integer) As String
            Dim sb As StringBuilder = New StringBuilder()
            sb.AppendLine("sArray[" & index & "] = new Array();")
            sb.AppendLine("dArray[" & index & "] = new Array();")
            Return sb.ToString()
        End Function

        Protected Function createJSArray(ByVal strSource As String, ByVal strDest As String, ByVal aNr As Integer, ByVal index As Integer) As String
            Dim sb As StringBuilder = New StringBuilder()
            sb.AppendLine(String.Format("sArray[{0}][{1}] = ""{2}"";", aNr, index, strSource))
            sb.AppendLine(String.Format("dArray[{0}][{1}] = ""{2}"";", aNr, index, strDest))
            Return sb.ToString()
        End Function

        Protected Function makeCopyScript() As String
            Dim sb As New StringBuilder()
            sb.AppendLine("    var i;")
            sb.AppendLine("    if(arg < sArray.length){")
            sb.AppendLine("        for (i = 0; i < sArray[arg].length; i++){")
            sb.AppendLine("        var x = dArray[arg][i]")
            sb.AppendLine("        var y = sArray[arg][i]")
            sb.AppendLine("        document.getElementById(x).value = document.getElementById(y).value;")
            sb.AppendLine("        }")
            sb.AppendLine("    }")
            Return Me.ScriptBlockFunction("copy(arg)", sb.ToString())
        End Function

        Protected Function makeCopyScript2() As String
            Dim sb As New StringBuilder()
            sb.AppendLine("    var i;")

            sb.AppendLine("        for (i = 0; i < sArrayB[arg].length; i++){")
            sb.AppendLine("        var x = dArrayB[arg][i]")
            sb.AppendLine("        var y = sArrayB[arg][i]")
            sb.AppendLine("        if(document.all)")
            sb.AppendLine("            document.getElementById(x).innerText = document.getElementById(y).value;")
            sb.AppendLine("        else")
            sb.AppendLine("            document.getElementById(x).textContent = document.getElementById(y).value;")
            sb.AppendLine("    }")
            Return Me.ScriptBlockFunction("copyB(arg)", sb.ToString())
        End Function

        Protected Function onLoadActionScript(Optional ByVal isActionB As Boolean = False) As String
            Dim sb As New StringBuilder()
            sb.AppendLine("<script type=""text/javascript"">")
            sb.AppendLine("    function onLoadAction(){")
            sb.AppendLine("        SetArray();")
            sb.AppendLine("        copy(getSelectedRadioButton());")
            If isActionB Then
                sb.AppendLine("        SetArrayB();")
                sb.AppendLine("        copyB(getSelectedRadioButton());")
            End If
            sb.AppendLine("    }")
            sb.AppendLine("</script>")
            Return sb.ToString()
        End Function

        Protected Function setHighLightScript() As String
            Dim sb As New StringBuilder()
            sb.AppendLine("<script type=""text/javascript"">")
            sb.AppendLine("var lastId = '';")
            sb.AppendLine("var lastParentColor = '';")
            sb.AppendLine("    function highLight(elementId, color){")
            sb.AppendLine("        if(lastId == ''){")
            sb.AppendLine("            lastId = elementId;")
            sb.AppendLine("            lastParentColor = document.getElementById(elementId).parentNode.style.backgroundColor;")
            sb.AppendLine("            document.getElementById(elementId).parentNode.style.backgroundColor=color;")
            sb.AppendLine("            document.getElementById(elementId).parentNode.style.borderColor=color;")
            sb.AppendLine("         }")
            sb.AppendLine("         else if(elementId != lastId){")
            sb.AppendLine("             document.getElementById(lastId).parentNode.style.backgroundColor=lastParentColor;")
            sb.AppendLine("             document.getElementById(lastId).parentNode.style.borderColor=lastParentColor;")
            sb.AppendLine("            lastParentColor = document.getElementById(elementId).parentNode.style.backgroundColor;")
            sb.AppendLine("             document.getElementById(elementId).parentNode.style.backgroundColor=color;")
            sb.AppendLine("             document.getElementById(elementId).parentNode.style.borderColor=color;")
            sb.AppendLine("             lastId = elementId;")
            sb.AppendLine("         }")
            sb.AppendLine("        return false;")
            sb.AppendLine("    }")
            sb.AppendLine("</script>")
            Return sb.ToString()
        End Function

    End Class

End Namespace

