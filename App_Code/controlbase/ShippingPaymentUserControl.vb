Imports Microsoft.VisualBasic
Imports System.Collections.Generic


Namespace ExpandIT
    Public Class ShippingPaymentUserControl
        Inherits ExpandIT.UserControl

        Private cDict As Dictionary(Of String, Control)
        Private ctrlRefParameter As Control
        Private Shared _fptr As System.Delegate

        Public Property SetPtr() As System.Delegate
            Set(ByVal value As System.Delegate)
                _fptr = value
            End Set
            Get
                Return _fptr
            End Get
        End Property

        Private Sub findCtrl(ByVal parent As Control, ByVal child As Control, ByVal ctrlName As String)
            For Each child In parent.Controls
                If child.GetType.Name = ctrlName Then
                    cDict.Add(child.UniqueID, child)
                End If
                findCtrl(child, child, ctrlName)
            Next
        End Sub

        Private Sub findVisibleCtrl(ByVal parent As Control, ByVal child As Control, ByVal ctrlName As String)
            For Each child In parent.Controls
                If child.GetType.Name = ctrlName Then
                    If child.Visible Then
                        cDict.Add(child.UniqueID, child)
                    End If
                End If
                findVisibleCtrl(child, child, ctrlName)
            Next
        End Sub
        ''' <summary>
        ''' Gets a Dictionary(Of String, Control) containing all the controls of the type specified by name.
        ''' </summary>
        ''' <param name="containerControl">The Control containing child controls to search for</param>
        ''' <param name="controlName">The type name of the controls to search for</param>
        ''' <param name="Visible"></param>        
        ''' <value></value>
        ''' <returns>A Dictionary(Of String, Control)</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property ControlDict(ByVal containerControl As Control, ByVal controlName As String, Optional ByVal Visible As Boolean = False) As Dictionary(Of String, Control)
            Get
                cDict = New Dictionary(Of String, Control)
                If Visible Then
                    findVisibleCtrl(containerControl, ctrlRefParameter, controlName)
                Else
                    findCtrl(containerControl, ctrlRefParameter, controlName)
                End If
                Return cDict
            End Get
        End Property

    End Class
End Namespace