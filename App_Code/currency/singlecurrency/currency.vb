Imports System.Configuration.ConfigurationManager
Imports Microsoft.VisualBasic
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

Namespace ExpandIT

    Public Class SingleCurrencyClass
        Inherits CurrencyBaseClass

        Public Sub New(ByVal _globals As GlobalsClass)
            MyBase.New(_globals)
            globals = _globals

            globals.MulticurrencyEnabled = False
            globals.bUseMultiCurrency = False

        End Sub

        ' --
        ' Exposed functions
        ' --
        Public Overrides Function ConvertCurrency(ByVal InputAmount As Object, ByVal InputCurrency As String, ByVal OutputCurrency As String) As Object
            Dim retval As Object
            Dim anInCurrency, anOutCurrency As String

            anInCurrency = CStrEx(InputCurrency)
            anOutCurrency = CStrEx(OutputCurrency)

            ' As this site only alows for one currency then it is ensured here:
            If anInCurrency <> AppSettings("MULTICURRENCY_SITE_CURRENCY") Then anInCurrency = AppSettings("MULTICURRENCY_SITE_CURRENCY")
            If anOutCurrency <> AppSettings("MULTICURRENCY_SITE_CURRENCY") Then anOutCurrency = AppSettings("MULTICURRENCY_SITE_CURRENCY")

            If anInCurrency = anOutCurrency Then
                If TypeName(InputAmount) <> "ExpDictionary" Then
                    retval = InputAmount
                Else
                    For Each key As String In InputAmount.ClonedKeyArray
                        If TypeName(InputAmount(key)) = "ExpDictionary" And Not key = "Lines" Then
                            ' Header and Line
                            InputAmount(key)("TotalInclTax_" & anOutCurrency) = InputAmount(key)("TotalInclTax")

                            ' Line
                            InputAmount(key)("ListPrice_" & anOutCurrency) = InputAmount(key)("ListPrice")
                            InputAmount(key)("ListPriceInclTax_" & anOutCurrency) = InputAmount(key)("ListPriceInclTax")
                            InputAmount(key)("LineTotal_" & anOutCurrency) = InputAmount(key)("LineTotal")

                            ' Header
                            InputAmount(key)("Total_" & anOutCurrency) = InputAmount(key)("Total")
                            InputAmount(key)("SubTotal_" & anOutCurrency) = InputAmount(key)("SubTotal")
                            InputAmount(key)("SubTotalInclTax_" & anOutCurrency) = InputAmount(key)("SubTotalInclTax")
                        Else
                            ' Header and Line
                            InputAmount("TotalInclTax_" & anOutCurrency) = InputAmount("TotalInclTax")

                            ' Line
                            InputAmount("ListPrice_" & anOutCurrency) = InputAmount("ListPrice")
                            InputAmount("ListPriceInclTax_" & anOutCurrency) = InputAmount("ListPriceInclTax")
                            InputAmount("LineTotal_" & anOutCurrency) = InputAmount("LineTotal")

                            ' Header
                            InputAmount("Total_" & anOutCurrency) = InputAmount("Total")
                            InputAmount("SubTotal_" & anOutCurrency) = InputAmount("SubTotal")
                            InputAmount("SubTotalInclTax_" & anOutCurrency) = InputAmount("SubTotalInclTax")
                        End If
                    Next
                    retval = 0
                End If
            Else
                Throw New Exception("ExpandIT Currency Module is not installed (trying to convert between '" & anInCurrency & "' and '" & anOutCurrency & "')")
            End If
            Return retval
        End Function

        Public Overrides Function GetExchangeFactor(ByVal FromCurrency As String, ByVal ToCurrency As String) As Decimal
            Return 1
        End Function

    End Class

End Namespace