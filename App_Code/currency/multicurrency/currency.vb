Imports System.Configuration.ConfigurationManager
Imports Microsoft.VisualBasic
Imports ExpandIT.EISClass
Imports ExpandIT
Imports ExpandIT.ExpandITLib
Imports System.Data.SqlClient
Imports System.Data

Namespace ExpandIT

    Public Class MultiCurrencyClass
        Inherits CurrencyBaseClass

        Public Sub New(ByVal _globals As GlobalsClass)
            MyBase.New(_globals)
            globals = _globals

            globals.MulticurrencyEnabled = True
            globals.bUseMultiCurrency = True

        End Sub

        ' --
        ' Exposed functions
        ' --
        Public Overrides Function ConvertCurrency(ByVal InputAmount As Object, ByVal InputCurrency As String, ByVal OutputCurrency As String) As Object
            Dim retval As Object
            Dim exchFactor As Decimal
            Dim anInCurrency As String
            Dim anOutCurrency As String

            anInCurrency = CStrEx(InputCurrency)
            anOutCurrency = CStrEx(OutputCurrency)

            If anInCurrency = "" Then anInCurrency = AppSettings("MULTICURRENCY_SITE_CURRENCY")
            If anOutCurrency = "" Then anOutCurrency = AppSettings("MULTICURRENCY_SITE_CURRENCY")

            ' Use single currency calculation if bUseMultiCurrency is false (due to previous errors)
            If anInCurrency = anOutCurrency Then
                If Not IsDict(InputAmount) Then
                    retval = InputAmount
                Else
                    For Each key As String In InputAmount.ClonedKeyArray

                        If IsDict(InputAmount(key)) And Not key = "Lines" Then
                            ' Header and Line
                            InputAmount(key)("TotalInclTax_" & anOutCurrency) = InputAmount(key)("TotalInclTax")

                            ' Line
                            InputAmount(key)("ListPrice_" & anOutCurrency) = InputAmount(key)("ListPrice")
                            InputAmount(key)("ListPriceInclTax_" & anOutCurrency) = InputAmount(key)("ListPriceInclTax")
                            InputAmount(key)("LineTotal_" & anOutCurrency) = InputAmount(key)("LineTotal")

                            ' Header
                            InputAmount(key)("Total_" & anOutCurrency) = InputAmount(key)("Total")
                            InputAmount(key)("SubTotal_" & anOutCurrency) = InputAmount(key)("SubTotal")
                            InputAmount(key)("SubTotalInclTax_" & anOutCurrency) = InputAmount(key)("SubTotalInclTax")
                        Else
                            ' Header and Line
                            InputAmount("TotalInclTax_" & anOutCurrency) = InputAmount("TotalInclTax")

                            ' Line
                            InputAmount("ListPrice_" & anOutCurrency) = InputAmount("ListPrice")
                            InputAmount("ListPriceInclTax_" & anOutCurrency) = InputAmount("ListPriceInclTax")
                            InputAmount("LineTotal_" & anOutCurrency) = InputAmount("LineTotal")

                            ' Header
                            InputAmount("Total_" & anOutCurrency) = InputAmount("Total")
                            InputAmount("SubTotal_" & anOutCurrency) = InputAmount("SubTotal")
                            InputAmount("SubTotalInclTax_" & anOutCurrency) = InputAmount("SubTotalInclTax")
                        End If
                    Next
                    retval = 0
                End If
            Else
                exchFactor = GetExchangeFactor(anInCurrency, anOutCurrency)
                If exchFactor <= 0 Then
                    ' In this case some kind of error happend, or the exchange factor was not calculated due to missing data.
                    ' Set bUseMultiCurrency = false to prevent displaying of Secondary Currency in the shop. Set exchFactor = 1
                    ' to complete the calculation.
                    globals.bUseMultiCurrency = False
                    exchFactor = 1
                End If

                If InStr(1, TypeName(InputAmount), "()") > 0 Then
                    ' Array handling
                    ReDim retval(UBound(InputAmount))
                    For i As Integer = LBound(InputAmount) To UBound(InputAmount)
                        retval(i) = InputAmount(i) * exchFactor
                    Next
                Else
                    ' Dictionary handling
                    If IsDict(InputAmount) Then
                        For Each key As String In InputAmount.ClonedKeyArray
                            If IsDict(InputAmount(key)) And Not key = "Lines" Then
                                ' Header and Line
                                InputAmount(key)("TotalInclTax_" & anOutCurrency) = CDblEx(InputAmount(key)("TotalInclTax")) * exchFactor

                                ' Line
                                InputAmount(key)("ListPrice_" & anOutCurrency) = CDblEx(InputAmount(key)("ListPrice")) * exchFactor
                                InputAmount(key)("ListPriceInclTax_" & anOutCurrency) = CDblEx(InputAmount(key)("ListPriceInclTax")) * exchFactor
                                InputAmount(key)("LineTotal_" & anOutCurrency) = CDblEx(InputAmount(key)("LineTotal")) * exchFactor

                                ' Header
                                InputAmount(key)("Total_" & anOutCurrency) = CDblEx(InputAmount(key)("Total")) * exchFactor
                                InputAmount(key)("SubTotal_" & anOutCurrency) = CDblEx(InputAmount(key)("SubTotal")) * exchFactor
                                InputAmount(key)("SubTotalInclTax_" & anOutCurrency) = CDblEx(InputAmount(key)("SubTotalInclTax")) * exchFactor

                            Else
                                ' Header and Line
                                InputAmount("TotalInclTax_" & anOutCurrency) = CDblEx(InputAmount("TotalInclTax")) * exchFactor

                                ' Line
                                InputAmount("ListPrice_" & anOutCurrency) = CDblEx(InputAmount("ListPrice")) * exchFactor
                                InputAmount("ListPriceInclTax_" & anOutCurrency) = CDblEx(InputAmount("ListPriceInclTax")) * exchFactor
                                InputAmount("LineTotal_" & anOutCurrency) = CDblEx(InputAmount("LineTotal")) * exchFactor

                                ' Header
                                InputAmount("Total_" & anOutCurrency) = CDblEx(InputAmount("Total")) * exchFactor
                                InputAmount("SubTotal_" & anOutCurrency) = CDblEx(InputAmount("SubTotal")) * exchFactor
                                InputAmount("SubTotalInclTax_" & anOutCurrency) = CDblEx(InputAmount("SubTotalInclTax")) * exchFactor
                            End If
                        Next
                        retval = 0
                    Else
                        ' Simple return value
                        retval = InputAmount * exchFactor
                    End If
                End If
            End If
            ConvertCurrency = retval
        End Function

        Public Overrides Function GetExchangeFactor(ByVal FromCurrency As String, ByVal ToCurrency As String) As Decimal
            Dim retval, triang1, triang2 As Decimal

            ' Check cached values        
            If FromCurrency = globals.g_LastFromCurrency And ToCurrency = globals.g_LastToCurrency Then
                GetExchangeFactor = globals.g_LastExchangeRate
                Exit Function
            End If

            ' The easy case
            If FromCurrency = ToCurrency Then
                GetExchangeFactor = 1
                Exit Function
            End If

            ' Try lookup
            retval = LookupExchangeRate(FromCurrency, ToCurrency)

            ' Try triangulation
            If retval = 0 Then
                If AppSettings("MULTICURRENCY_TRIANG_CURRENCY").ToString <> "" Then
                    triang1 = LookupExchangeRate(FromCurrency, AppSettings("MULTICURRENCY_TRIANG_CURRENCY"))
                    triang2 = LookupExchangeRate(AppSettings("MULTICURRENCY_TRIANG_CURRENCY"), ToCurrency)
                    retval = triang1 * triang2
                End If
            End If

            If retval = 0 Then
                ' Changed here to show "nice" error message instead of "yellow page" 
                globals.messages.Errors.Add("Unable to convert currency from " & FromCurrency & " to " & ToCurrency)
            End If

            GetExchangeFactor = retval
        End Function

        ' --
        ' Internal functions
        ' --
        Protected Function LookupExchangeRate(ByVal FromCurrency As String, ByVal ToCurrency As String) As Decimal
            Dim retv As Decimal
            Dim cachevalue As Object
            Dim dt, dt2 As DataTable
            Dim sql As String
            Dim sqlFrom, sqlTo, cachekey As String

            cachekey = "Exchage-" & FromCurrency & "-" & ToCurrency
            cachevalue = CacheGet(cachekey)
            If cachevalue IsNot Nothing Then
                retv = CDblEx(cachevalue)
            Else
                sqlFrom = ""
                sqlTo = ""
                If FromCurrency = AppSettings("MULTICURRENCY_SITE_CURRENCY") Then sqlFrom = " OR (LTrim(RTrim(FromCurrency))='') OR (FromCurrency IS NULL)"
                If ToCurrency = AppSettings("MULTICURRENCY_SITE_CURRENCY") Then sqlTo = " OR (LTrim(RTrim(ToCurrency))='') OR (ToCurrency IS NULL)"
                sql = "SELECT ExchangeRate FROM CurrencyExchange WHERE ((FromCurrency=" & SafeString(FromCurrency) & ")" & sqlFrom & ") AND ((ToCurrency=" & SafeString(ToCurrency) & ")" & sqlTo & ")"
                dt = SQL2DataTable(sql)
                If dt.Rows.Count = 0 Then
                    sqlFrom = ""
                    sqlTo = ""
                    If FromCurrency = AppSettings("MULTICURRENCY_SITE_CURRENCY") Then sqlTo = " OR (LTrim(RTrim(ToCurrency))='') OR (ToCurrency IS NULL)"
                    If ToCurrency = AppSettings("MULTICURRENCY_SITE_CURRENCY") Then sqlFrom = " OR (LTrim(RTrim(FromCurrency))='') OR (FromCurrency IS NULL)"
                    sql = "SELECT ExchangeRate FROM CurrencyExchange WHERE ((FromCurrency=" & SafeString(ToCurrency) & ")" & sqlFrom & ") AND ((ToCurrency=" & SafeString(FromCurrency) & ")" & sqlTo & ")"
                    dt2 = SQL2DataTable(sql)
                    If dt2.Rows.Count > 0 Then
                        retv = CDblEx(dt2.Rows(0)("ExchangeRate")) / 100
                    Else
                        retv = 0
                    End If
                Else
                    retv = 100 / CDblEx(dt.Rows(0)("ExchangeRate"))
                End If

                CacheSetAggregated(cachekey, retv, New String() {"CurrencyTable", "CurrencyExchange"})
            End If

            Return retv
        End Function

        Protected Sub ConvertCurrencyDefault(ByVal InputDict As Object)
            ConvertCurrency(InputDict, AppSettings("MULTICURRENCY_SITE_CURRENCY"), HttpContext.Current.Session("UserCurrencyGuid"))
            If Not IsNull(HttpContext.Current.Session("UserSecondaryCurrencyGuid").ToString) Then
                ConvertCurrency(InputDict, AppSettings("MULTICURRENCY_SITE_CURRENCY"), globals.User("SecondaryCurrencyGuid"))
            End If
        End Sub

    End Class

End Namespace
