Imports System.Configuration.ConfigurationManager
Imports Microsoft.VisualBasic
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Collections.Generic
Imports System.Web

Namespace ExpandIT

    Public Class MenuItemBottom
        Private m_id As String = ""
        Private m_caption As String = ""
        Private m_url As String = ""

        Public Property Id() As String
            Get
                Return m_id
            End Get
            Set(ByVal value As String)
                m_id = value
            End Set
        End Property

        Public Property Caption() As String
            Get
                Return m_caption
            End Get
            Set(ByVal value As String)
                m_caption = value
            End Set
        End Property

        Public Property URL() As String
            Get
                Return m_url
            End Get
            Set(ByVal value As String)
                m_url = value
            End Set
        End Property
    End Class

    Public Class SiteMenuBottom
        Inherits System.Collections.Generic.List(Of MenuItemBottom)

        Private globals As GlobalsClass
        Private eis As EISClass

        Public Sub New(ByVal _globals As GlobalsClass)
            Dim itm As MenuItemBottom

            globals = _globals
            eis = globals.eis

            'If eis.CheckPageAccess("HomePage") Then
            '    itm = New MenuItemBottom
            '    itm.Id = "Home"
            '    itm.URL = VRoot & "/default.aspx"
            '    itm.Caption = Resources.Language.LABEL_MENU_HOME
            '    Add(itm)
            'End If

            'If eis.CheckPageAccess("Favorites") Then
            '    itm = New MenuItemBottom
            '    itm.Id = "Favorites"
            '    itm.URL = VRoot & "/favorites.aspx"
            '    itm.Caption = Resources.Language.LABEL_FAVORITES
            '    Add(itm)
            'End If

            'If eis.CheckPageAccess("Cart") Then
            '    itm = New MenuItemBottom
            '    itm.Id = "Cart"
            '    itm.URL = VRoot & "/cart.aspx"
            '    itm.Caption = Resources.Language.LABEL_MENU_ORDER_PAD
            '    Add(itm)
            'End If

            'If eis.CheckPageAccess("Catalog") Then
            '    itm = New MenuItemBottom
            '    itm.Id = "Search"
            '    itm.URL = VRoot & "/Search.aspx"
            '    itm.Caption = Resources.Language.LABEL_MENU_SEARCH
            '    Add(itm)
            'End If

            If eis.CheckPageAccess("HomePage") Then
                itm = New MenuItemBottom
                itm.Id = "InformationPage"
                itm.URL = VRoot & "/info/info.aspx"
                itm.Caption = Resources.Language.LABEL_INFORMATION
                Add(itm)
            End If

            'If eis.CheckPageAccess("Catalog") Then
            '    itm = New MenuItemBottom
            '    itm.Id = "Catalog"
            '    itm.URL = VRoot & "/catalog_overview.aspx"
            '    itm.Caption = Resources.Language.LABEL_MENU_CATALOG
            '    Add(itm)
            'End If

            ' WLB 10/10/2012 BEGIN - Remove "Purchase" menu item.
            '            If eis.CheckPageAccess("Order") Then
            '                itm = New MenuItemBottom
            '                itm.Id = "Order"
            '                itm.URL = VRoot & "/shipping.aspx"
            '                itm.Caption = Resources.Language.LABEL_MENU_PURCHASE
            '                Add(itm)
            '            End If
            ' WLB 10/10/2012 BEGIN - Remove "Purchase" menu item.

            'If (globals.User("B2B") Or globals.User("B2C")) And eis.CheckPageAccess("HomePage") Then
            '    itm = New MenuItemBottom
            '    itm.Id = "Account"
            '    itm.URL = VRoot & "/account.aspx"
            '    itm.Caption = Resources.Language.LABEL_MENU_ACCOUNT
            '    Add(itm)
            'End If


            If CBoolEx(AppSettings("SHOW_QUICK_CART")) Then
                If Not CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) Then
                    itm = New MenuItemBottom
                    itm.Id = "quickcart"
                    itm.URL = VRoot & "/quickcart.aspx"
                    itm.Caption = Resources.Language.QUICK_CART
                    Add(itm)
                End If
            End If

            'JA2011021701 - STORE LOCATOR - Start
            If CBoolEx(AppSettings("SHOW_STORE_LOCATOR")) Then
                itm = New MenuItemBottom
                itm.Id = "StoreLocator"
                itm.URL = VRoot & "/mapShow.aspx"
                itm.Caption = Resources.Language.LABEL_STORE_LOCATOR
                Add(itm)
            End If
            'JA2011021701 - STORE LOCATOR - End

            ''AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
            'If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
            '    Dim csrObj As New USCSR(globals)
            '    itm = csrObj.csrBottomMenu()
            '    If Not itm Is Nothing Then
            '        Add(itm)
            '    End If
            'End If
            ''AM2011031801 - ENTERPRISE CSR STAND ALONE - End



            'If eis.CheckPageAccess("Order") And globals.User("Anonymous") Then
            '    itm = New MenuItemBottom
            '    itm.Id = "HistoryLookup"
            '    itm.URL = VRoot & "/history_lookup.aspx"
            '    itm.Caption = Resources.Language.LABEL_MENU_HISTORY
            '    Add(itm)
            'End If

            'If AppSettings("SHOW_SIGN_IN_MENU") Then
            '    If eis.ShowLoginLink() Then
            '        itm = New MenuItemBottom
            '        itm.Id = "Login"
            '        itm.URL = VRoot & "/user_login.aspx"
            '        itm.Caption = Resources.Language.LABEL_MENU_LOGIN
            '        Add(itm)
            '    End If
            'End If

            'If AppSettings("SHOW_NEW_ACCOUNT_MENU") Then
            '    If globals.User("Anonymous") And eis.CheckPageAccess("OpenSite") Then
            '        itm = New MenuItemBottom
            '        itm.Id = "Register"
            '        itm.URL = VRoot & "/user_new.aspx"
            '        itm.Caption = Resources.Language.MENU_SIGNUP
            '        Add(itm)
            '    End If
            'End If

            'If AppSettings("SHOW_SIGN_OFF_MENU") Then
            '    If Not AppSettings("USE_NTLM") And Not globals.User("Anonymous") Then
            '        itm = New MenuItemBottom
            '        itm.Id = "Logout"
            '        itm.URL = VRoot & "/_user_logout.aspx"
            '        itm.Caption = Resources.Language.LABEL_MENU_LOGOUT
            '        Add(itm)
            '    End If
            'End If
        End Sub

    End Class

End Namespace
