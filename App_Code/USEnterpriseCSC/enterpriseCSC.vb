'AM2010071901 - ENTERPRISE CSC - Start
Imports System.Configuration.ConfigurationManager
Imports Microsoft.VisualBasic
Imports ExpandIT.GlobalsClass
Imports ExpandIT.ExpandITLib


Namespace ExpandIT

    Public Class enterpriseCSC

        Private globals As GlobalsClass

        Public Sub New(Optional ByVal _globals As GlobalsClass = Nothing)
            globals = _globals
        End Sub

        Public Function getCustomerCatalog(ByVal customerGuid As String) As String
            Dim catalogNo As String = ""
            Dim sql As String = ""
            If CStrEx(customerGuid) <> "" Then
                sql = "SELECT CatalogNo FROM EECMCatalog WHERE (CustomerGuid = " & SafeString(customerGuid) & " )"
            Else
                sql = "SELECT CatalogNo FROM EECMCatalog WHERE (CustomerGuid = " & SafeString(AppSettings("ANONYMOUS_CUSTOMERGUID")) & " ) "
            End If
            catalogNo = CStrEx(getSingleValueDB(sql))

            If catalogNo = "" Then
                sql = "SELECT CatalogNo FROM EECMCatalog WHERE (CustomerGuid = '')"
                catalogNo = CStrEx(getSingleValueDB(sql))
            End If

            Return catalogNo
        End Function

    End Class
End Namespace

'AM2010071901 - ENTERPRISE CSC - End