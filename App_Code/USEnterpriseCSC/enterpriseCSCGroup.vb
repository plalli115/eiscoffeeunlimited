'WLB.01/09/2015 - COU-WEB-L01 - Begin
Imports System.Configuration.ConfigurationManager
Imports Microsoft.VisualBasic
Imports ExpandIT.GlobalsClass
Imports ExpandIT.ExpandITLib


Namespace ExpandIT

    Public Class enterpriseCSCGroup

        Private globals As GlobalsClass

        Public Sub New(Optional ByVal _globals As GlobalsClass = Nothing)
            globals = _globals
        End Sub

        Public Function getCustomerCatalog(ByVal customerGroupGuid As String) As String
            Dim catalogNo As String = ""
            Dim sql As String = ""
            If CStrEx(customerGroupGuid) <> "" Then
                sql = "SELECT CatalogNo FROM EECMCatalog WHERE (InetCustomerGroupGuid = " & SafeString(customerGroupGuid) & " )"
                'Else
                '    sql = "SELECT CatalogNo FROM EECMCatalog WHERE (InetCustomerGroupGuid = " & SafeString(AppSettings("ANONYMOUS_CUSTOMERGUID")) & " ) "
            End If
            catalogNo = CStrEx(getSingleValueDB(sql))

            If catalogNo = "" Then
                sql = "SELECT CatalogNo FROM EECMCatalog WHERE (InetCustomerGroupGuid = '')"
                catalogNo = CStrEx(getSingleValueDB(sql))
            End If

            Return catalogNo
        End Function

    End Class
End Namespace

'WLB.01/09/2015 - COU-WEB-L01 - End