Imports System.Configuration.ConfigurationManager
Imports Microsoft.VisualBasic
Imports System.Collections.Generic

Public Class Breadcrumb
    Private m_caption As String = ""
    Private m_url As String = ""

    Public Property Caption() As String
        Get
            Return m_caption
        End Get
        Set(ByVal value As String)
            m_caption = value
        End Set
    End Property

    Public Property URL() As String
        Get
            Return m_url
        End Get
        Set(ByVal value As String)
            m_url = value
        End Set
    End Property
End Class

Public Class BreadcrumbTrail
    Inherits System.Collections.Generic.List(Of Breadcrumb)

    Public Sub AddCurrentLocation(ByVal Title As String)
        Dim bc As New Breadcrumb

        bc.Caption = Title
        bc.URL = HttpContext.Current.Request.RawUrl
        Me.Add(bc)
    End Sub

    Public Function Clone() As BreadcrumbTrail
        Dim retv As New BreadcrumbTrail

        For Each bc As Breadcrumb In Me
            retv.Add(bc)
        Next

        Return retv
    End Function
End Class
