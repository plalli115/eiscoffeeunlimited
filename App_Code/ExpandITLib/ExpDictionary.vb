Imports Microsoft.VisualBasic
Imports System.Collections
Imports System.Runtime.Serialization

Namespace ExpandIT
    <Serializable()> _
    Public Class ExpDictionary
        Inherits System.Collections.Generic.Dictionary(Of Object, Object)

        Public Enum FilterMethods
            Top = 1
            Bottom = 2
            Random = 3
        End Enum

        Sub New(ByVal info As SerializationInfo, ByVal context As StreamingContext)
            MyBase.New(info, context)
        End Sub

        Sub New()

        End Sub

        Default Public Shadows Property Item(ByVal key As Object) As Object
            Get
                If MyBase.ContainsKey(key) Then
                    Return MyBase.Item(key)
                Else
                    Return Nothing
                End If
            End Get

            Set(ByVal value As Object)
                If MyBase.ContainsKey(key) Then
                    MyBase.Item(key) = value
                Else
                    MyBase.Add(key, value)
                End If
            End Set
        End Property

        Public Function Exists(ByVal key As Object) As Boolean
            Return ContainsKey(key)
        End Function

        Public ReadOnly Property ClonedKeyArray() As Object()
            Get
                If Me.Count = 0 Then
                    Return New Object() {}
                Else
                    Dim retv(Me.Count - 1) As Object
                    Me.Keys.CopyTo(retv, 0)
                    Return retv
                End If
            End Get
        End Property

        Public ReadOnly Property DictItems() As Object()
            Get
                Dim itemCol(Me.Values.Count - 1) As Object
                Me.Values.CopyTo(itemCol, 0)
                Return itemCol
            End Get
        End Property

        Public ReadOnly Property DictKeys() As Object()
            Get
                Dim itemCol(MyBase.Keys.Count - 1) As Object
                MyBase.Keys.CopyTo(itemCol, 0)
                Return itemCol
            End Get
        End Property

        Public Shared Function FilterDict(ByVal dict As ExpDictionary, ByVal maxitems As Integer, ByVal filtermethod As FilterMethods) As ExpDictionary
            Dim retv As New ExpDictionary
            Dim keys As ArrayList
            Dim i As Integer

            keys = New ArrayList(dict.ClonedKeyArray)
            Select Case filtermethod
                Case FilterMethods.Top
                    While retv.Count < maxitems And keys.Count > 0
                        i = 0
                        retv.Add(keys(i), dict(keys(i)))
                        keys.RemoveAt(i)
                    End While
                Case FilterMethods.Bottom
                    While retv.Count < maxitems And keys.Count > 0
                        i = keys.Count - 1
                        retv.Add(keys(i), dict(keys(i)))
                        keys.RemoveAt(i)
                    End While
                Case FilterMethods.Random
                    While retv.Count < maxitems And keys.Count > 0
                        i = Math.Floor(Rnd() * keys.Count)
                        retv.Add(keys(i), dict(keys(i)))
                        keys.RemoveAt(i)
                    End While
            End Select
            Return retv
        End Function

    End Class

End Namespace

