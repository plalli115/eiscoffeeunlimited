Imports System
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Configuration.ConfigurationManager
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Web
Imports System.Xml
Imports System.Globalization

Namespace ExpandIT

    Public Class ExpandITLib

        Private Shared ConnStr As String
        Private Shared ConnStrName As String
        Private Shared isLocalModeVB As Boolean
        Private Shared virtualRoot As String
        Private Shared appRoot As String
        Private Shared isSqlServer2005 As Boolean
        Private Shared RoundingMode As MidpointRounding

        ''' <summary>
        ''' Finds out if the running DB is SQL Server 2005
        ''' </summary>
        ''' <value></value>
        ''' <returns>True if Server is SQL Server 2005</returns>
        ''' <remarks></remarks>
        Public Shared ReadOnly Property IsSQL2005() As Boolean
            Get
                Return isSqlServer2005
            End Get
        End Property

        Public Shared ReadOnly Property VRoot() As String
            Get
                Return virtualRoot
            End Get
        End Property

        Public Shared ReadOnly Property ApplicationRoot() As String
            Get
                Return appRoot
            End Get
        End Property

        ''' <summary>
        ''' Checks if the site is running in local or remote mode
        ''' </summary>
        ''' <value></value>
        ''' <returns>True if the site is running in local mode</returns>
        ''' <remarks></remarks>
        Public Shared ReadOnly Property IsLocalMode() As Boolean
            Get
                Return isLocalModeVB
            End Get
        End Property

        ''' <summary>
        ''' Get or Set the current ConnectionStringName used by this module
        ''' </summary>
        ''' <value>String</value>
        ''' <returns>The name of the connectionstring</returns>
        ''' <remarks></remarks>
        Public Shared Property CurrentConnectionString() As String
            Get
                Return ConnStr
            End Get
            Set(ByVal value As String)
                Try
                    ConnStr = value
                Catch ex As Exception

                End Try
            End Set
        End Property

        ''' <summary>
        ''' Get or Set the current ConnectionStringName used by this module
        ''' </summary>
        ''' <value>String</value>
        ''' <returns>The name of the connectionstring</returns>
        ''' <remarks></remarks>
        Public Shared Property CurrentConnectionStringName() As String
            Get
                Return ConnStrName
            End Get
            Set(ByVal value As String)
                Try
                    ConnStrName = value
                Catch ex As Exception

                End Try
            End Set
        End Property

        Shared Sub New()

            Try
                If Not isRemoteMode() Then
                    isLocalModeVB = True
                End If
                ConnStr = ConfigurationManager.ConnectionStrings("ExpandITConnectionString").ConnectionString
                ConnStrName = "ExpandITConnectionString"
            Catch ex As Exception

            End Try

            If String.IsNullOrEmpty(ConnStr) Then
                Dim arr As System.Configuration.ConfigurationElement() = {}
                Try
                    Array.Resize(arr, ConfigurationManager.ConnectionStrings.Count)
                    ConfigurationManager.ConnectionStrings.CopyTo(arr, 0)
                    If arr.Length > 1 Then
                        ConnStr = arr(1).ToString()
                    Else
                        ConnStr = arr(0).ToString()
                    End If
                Catch ex As Exception
                    ConnStr = ""
                End Try
            End If

            virtualRoot = getVirtualRoot()
            appRoot = getShopRoot()
            findSqlServerVersion()
            RoundingMode = IIf(AppSettings("USE_BANKER_ROUNDING") = "1", MidpointRounding.ToEven, MidpointRounding.AwayFromZero)
        End Sub

        ''' <summary>
        ''' Finds out if the running DB is SQLServer 2005 Or Newer
        ''' </summary>
        ''' <remarks></remarks>
        Private Shared Sub findSqlServerVersion()
            Try
                Dim vers As Object = getSingleValueDB("SELECT SERVERPROPERTY('productversion')")
                Dim str As String = CType(vers, String)
                Dim sArr As String() = Split(str, ".")
                If Integer.Parse(sArr(0)) > 8 Then
                    isSqlServer2005 = True
                End If
            Catch ex As Exception

            End Try
        End Sub

        ''' <summary>
        ''' Finds out if this is a remote or local site
        ''' </summary>
        ''' <returns>Boolean</returns>
        ''' <remarks></remarks>
        Private Shared Function isRemoteMode() As Boolean
            Dim str As String = HttpRuntime.AppDomainAppPath & "expandITRemote.xml"
            Return System.IO.File.Exists(str)
        End Function

        ''' <summary>
        ''' Determines the absolute path for the virtual root of the current ASP application.
        ''' </summary>
        ''' <returns>The absolute path for the virtual root</returns>
        ''' <remarks></remarks>
        Private Shared Function getVirtualRoot() As String
            Dim vpath As String = HttpRuntime.AppDomainAppVirtualPath
            If vpath.Equals("/") Then Return ""
            Return HttpRuntime.AppDomainAppVirtualPath
        End Function

        ''' <summary>
        ''' GetShopRoot determines the physical root path of the shop installation.
        ''' </summary>
        ''' <returns>The physical root path of the shop installation</returns>
        ''' <remarks></remarks>
        Private Shared Function getShopRoot() As String
            Return HttpRuntime.AppDomainAppPath.TrimEnd("\")
        End Function

#Region "DB"

        ''' <summary>
        ''' Returns an open SqlConnection Object
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetDBConnection() As SqlConnection
            Dim conn As New SqlConnection(ConnStr)
            conn.Open()
            Return conn
        End Function

        ''' <summary>
        ''' Function returns a single value as a result from a database query
        ''' </summary>
        ''' <param name="sql">The SQL-Statement to excecute</param>
        ''' <returns>A single value of type Object</returns>
        ''' <remarks></remarks>
        Public Shared Function getSingleValueDB(ByVal sql As String) As Object
            Dim oc As SqlCommand
            Dim ret As Object = Nothing
            Using localConn As New SqlConnection(ConnStr)
                Try
                    localConn.Open()
                    oc = New SqlCommand(sql, localConn)
                    oc.CommandType = CommandType.Text
                    ret = oc.ExecuteScalar()
                Catch ex As Exception
                End Try
            End Using
            Return ret
        End Function

        ''' <summary>
        ''' Creates a dictionary with the values of the fields in a single record in a recordset.
        ''' </summary>
        ''' <param name="dr">SqlDataReader</param>
        ''' <returns>The function returns the dictionary containing the values from the recordset.</returns>
        ''' <remarks></remarks>
        Public Shared Function SetDictionary(ByVal dr As SqlDataReader) As ExpDictionary
            Dim i As Integer
            Dim retv As ExpDictionary

            retv = New ExpDictionary()
            For i = 0 To dr.FieldCount - 1
                retv.Add(dr.GetName(i), dr.GetValue(i))
            Next

            Return retv
        End Function

        ''' <summary>
        ''' Creates a dictionary with the values of the fields in a single DataRow.
        ''' </summary>
        ''' <param name="row">DataRow</param>
        ''' <returns>ExpDictionary</returns>
        ''' <remarks></remarks>
        Public Shared Function SetDictionary(ByVal row As DataRow) As ExpDictionary
            Dim i As Integer
            Dim retv As New ExpDictionary()

            For i = 0 To row.Table.Columns.Count - 1
                retv.Add(row.Table.Columns(i).ColumnName, row(i))
            Next

            Return retv
        End Function

        ''' <summary>
        ''' Populates a DataTable with the result from the provided SQL-Query 
        ''' </summary>
        ''' <param name="sql"></param>
        ''' <returns>DataTable</returns>
        ''' <remarks></remarks>
        Public Shared Function SQL2DataTableWithKeys(ByVal sql As String) As DataTable
            Dim da As SqlDataAdapter
            Dim conn As SqlConnection = GetDBConnection()
            Dim cmd As New SqlCommand(sql, conn)
            cmd.CommandTimeout = 0 'Don't time out
            Dim dt As New DataTable
            da = New SqlDataAdapter(cmd)
            da.MissingSchemaAction = MissingSchemaAction.AddWithKey
            da.Fill(dt)
            da.Dispose()
            conn.Close()
            Return dt
        End Function

        ''' <summary>
        ''' Populates a DataTable with the result from the provided SQL-Query 
        ''' </summary>
        ''' <param name="sql"></param>
        ''' <returns>DataTable</returns>
        ''' <remarks></remarks>
        Public Shared Function SQL2DataTable(ByVal sql As String) As DataTable
            Dim da As SqlDataAdapter
            Dim conn As SqlConnection = GetDBConnection()
            Dim cmd As New SqlCommand(sql, conn)
            cmd.CommandTimeout = 0 'Don't time out
            Dim dt As New DataTable
            da = New SqlDataAdapter(cmd)
            da.Fill(dt)
            da.Dispose()
            conn.Close()
            Return dt
        End Function

        ''' <summary>
        ''' Returns an open dataReader
        ''' </summary>
        ''' <param name="sql"></param>
        ''' <returns>DataReader</returns>
        ''' <remarks></remarks>
        Public Shared Function GetDR(ByVal sql As String) As SqlDataReader
            Dim dr As SqlDataReader
            Dim conn As SqlConnection
            Dim cmd As SqlCommand

            conn = GetDBConnection()
            cmd = New SqlCommand(sql, conn)
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Return dr
        End Function

        ''' <summary>
        ''' Creates a dictionary of multiple dictionaries from as DataReader
        ''' </summary>
        ''' <param name="dr">SqlDataReader</param>
        ''' <param name="FieldName">Specifies the field to retreive (optional)</param>
        ''' <param name="recMax">Maxium number of records to retreive (optional)</param>
        ''' <returns>A multilevel dictionary</returns>
        ''' <remarks>
        ''' This function can retreive all records of all fields from a DB-Table. It is also possible to retreive only one selected field.
        ''' Furthermore, it'salso possible to reduce the number of retrieved records.
        ''' </remarks>
        Public Shared Function DR2Dicts(ByVal dr As SqlDataReader, Optional ByVal FieldName As String = "", Optional ByVal recMax As Integer = -1) As ExpDictionary
            Dim retv As New ExpDictionary
            Dim key As String
            Dim cnt As Integer = 0
            Dim ht As ExpDictionary
            Dim fieldno As Integer = 0

            If FieldName <> "" Then fieldno = dr.GetOrdinal(FieldName)

            While dr.Read And ((cnt < recMax) Or recMax < 0)
                cnt = cnt + 1
                If FieldName = "" Then key = cnt.ToString Else key = dr.GetValue(fieldno).ToString
                ht = SetDictionary(dr)
                If retv.ContainsKey(key) Then
                    retv(key) = ht
                Else
                    retv.Add(key, ht)
                End If
            End While

            Return retv
        End Function

        ''' <summary>
        ''' Creates a dictionary of multiple dictionaries from a DataTable
        ''' </summary>
        ''' <param name="dt">DataTable</param>
        ''' <param name="FieldName">Specifies the field to retreive (optional)</param>
        ''' <param name="recMax">Maxium number of records to retreive (optional)</param>
        ''' <returns>A multilevel dictionary</returns>
        ''' <remarks>
        ''' This function can retreive all records of all fields from a DB-Table. It is also possible to retreive only one selected field.
        ''' Furthermore, it'salso possible to reduce the number of retrieved records.
        ''' </remarks>
        Public Shared Function DT2Dicts(ByVal dt As DataTable, Optional ByVal FieldName As String = "", Optional ByVal recMax As Integer = -1) As ExpDictionary
            Dim retv As New ExpDictionary
            Dim key As String
            Dim cnt As Integer = 0
            Dim ht As ExpDictionary
            Dim fieldno As Integer = 0

            If FieldName <> "" Then fieldno = dt.Columns.IndexOf(FieldName)

            For Each row As DataRow In dt.Rows
                cnt = cnt + 1
                If FieldName = "" Then key = cnt.ToString Else key = CType(DBNull2Nothing(row(fieldno)), String)
                ht = SetDictionary(row)
                If key IsNot Nothing Then '<< Changed here >>
                    If retv.ContainsKey(key) Then
                        retv(key) = ht
                    Else
                        retv.Add(key, ht)
                    End If
                End If
            Next
            Return retv
        End Function

        Public Shared Function SQL2Dicts(ByVal sql As String) As ExpDictionary
            Return SQL2DictsEx(sql, Nothing, -1)
        End Function

        Public Shared Function SQL2Dicts(ByVal sql As String, ByVal fieldname As String) As ExpDictionary
            Return SQL2DictsEx(sql, fieldname, -1)
        End Function

        Public Shared Function SQL2Dicts(ByVal sql As String, ByVal fieldnames() As String) As ExpDictionary
            Return SQL2DictsEx(sql, fieldnames, -1)
        End Function

        Public Shared Function SQL2Dicts(ByVal sql As String, ByVal FieldName As Object, ByVal nMax As Integer) As ExpDictionary
            Return SQL2DictsEx(sql, FieldName, nMax)
        End Function

        Public Shared Function SQL2DictsEx(ByVal sql As String, ByVal FieldName As Object, ByVal nMax As Integer) As ExpDictionary
            Dim dt As DataTable = SQL2DataTable(sql)
            Return DT2Dicts(dt, FieldName, nMax)
        End Function

        Public Shared Function SQL2NameValuePairs(ByVal sql As String, ByVal levels As Integer) As ExpDictionary
            Dim retv As New ExpDictionary
            Dim dt As DataTable
            Dim i As Integer
            Dim node, parentnode As ExpDictionary
            Dim key As Object

            dt = SQL2DataTable(sql)
            For Each row As DataRow In dt.Rows
                parentnode = retv

                ' Make the path
                For i = 0 To levels - 2
                    key = row(i)
                    If parentnode.ContainsKey(key) Then
                        node = parentnode(key)
                    Else
                        node = New ExpDictionary
                        parentnode.Add(key, node)
                    End If
                    parentnode = node
                Next

                ' Add the value
                parentnode.Add(row(i), row(i + 1))

            Next
            Return retv
        End Function

        Public Shared Function SQL2List(ByVal sql As String) As List(Of String)
            Dim retList As List(Of String) = New List(Of String)()
            Dim dr As SqlDataReader = GetDR(sql)
            While dr.Read()
                Try
                    retList.Add(dr(0))
                Catch ex As Exception

                End Try
            End While
            dr.Close()
            Return retList
        End Function

        ''' <summary>
        ''' Creates a dictionary with the values of the fields in all records in a DataTable.
        ''' </summary>
        ''' <param name="rs">DataTable</param>
        ''' <param name="keyfields">Object</param>
        ''' <returns>ExpDictionary</returns>
        ''' <remarks>The function returns the dictionary containing the values from the DataTable.
        ''' Top level dictionaries are named using the values of the key fields for each record.</remarks>
        Public Shared Function SetDictionaries(ByVal rs As DataTable, ByVal keyfields As Object) As ExpDictionary
            Return SetDictionariesEx(rs, keyfields, 0)
        End Function

        ''' <summary>
        ''' This is a helper function for SetDictionaries.
        ''' </summary>
        ''' <param name="dt">DataTable</param>
        ''' <param name="keyfields">Object - Array or string with keyfields or keyfield.</param>
        ''' <param name="top">Integer - The number of records to return</param>
        ''' <returns>ExpDictionary</returns>
        ''' <remarks>The function returns the dictionary containing the values from the DataTable.
        ''' Top level dictionaries are named using the values of the key fields for each record.</remarks>
        Public Shared Function SetDictionariesEx(ByVal dt As DataTable, ByVal keyfields As Object, Optional ByVal top As Integer = -1) As ExpDictionary
            Dim retv As ExpDictionary
            Dim key As String
            Dim i As Integer
            Dim cnt As Integer
            Dim a() As Integer

            If TypeName(keyfields) = "String" Then
                ReDim a(0)
                a(0) = dt.Columns.IndexOf(keyfields)
                keyfields = a
            Else
                ReDim a(UBound(keyfields))
                For i = 0 To UBound(keyfields)
                    a(i) = dt.Columns.IndexOf(keyfields(i))
                Next
                keyfields = a
            End If

            cnt = 0
            retv = New ExpDictionary
            For Each row As DataRow In dt.Rows
                If Not (cnt < top Or top <= 0) Then Exit For
                key = ""
                For i = 0 To UBound(keyfields)
                    If i > 0 Then key = key & ","
                    key = key & SafeString(row(keyfields(i)))
                Next
                retv(key) = SetDictionary(row)
                cnt = cnt + 1
            Next
            SetDictionariesEx = retv
        End Function

        ''' <summary>
        ''' This version creates multilevel dictionaries.
        ''' </summary>
        ''' <param name="dt">Datatable</param>
        ''' <param name="keyfields">Array or string with keyfields or keyfield.</param>
        ''' <param name="top">The number of records to return.</param>
        ''' <returns>A multilevelDictionary</returns>
        ''' <remarks>
        ''' The function returns the dictionary containing the values from the recordset.
        ''' Top level dictionaries are named using the values of the key fields for each record.
        ''' One level of dictionaries will be added for each keyfield specified.
        ''' </remarks>
        Public Shared Function SetDictionaries2Ex(ByVal dt As DataTable, ByVal keyfields As Object, Optional ByVal top As Integer = -1) As ExpDictionary
            Dim retv As ExpDictionary
            Dim key As String
            Dim i As Integer
            Dim cnt As Integer
            Dim item As ExpDictionary
            Dim a() As Integer

            If TypeName(keyfields) = "String" Then
                ReDim a(0)
                a(0) = dt.Columns.IndexOf(keyfields)
                keyfields = a
            Else
                ReDim a(UBound(keyfields))
                For i = 0 To UBound(keyfields)
                    a(i) = dt.Columns.IndexOf(keyfields(i))
                Next
                keyfields = a
            End If

            cnt = 0
            retv = New ExpDictionary
            For Each row As DataRow In dt.Rows
                key = ""
                item = retv
                For i = 0 To UBound(keyfields) - 1
                    If item(SafeString(row(keyfields(i)))) Is Nothing Then
                        item(SafeString(row(keyfields(i)))) = New ExpDictionary
                    End If
                    item = item(SafeString(row(keyfields(i))))
                Next
                item(SafeString(row(keyfields(i)))) = SetDictionary(row)
                cnt = cnt + 1
            Next

            SetDictionaries2Ex = retv
        End Function

        ''' <summary>
        ''' Returns the first record from a sql result in a dictionary
        ''' </summary>
        ''' <param name="sql">SQL statement.</param>
        ''' <returns>Dictionary</returns>
        ''' <remarks>This function executes a sql query and returns the result as a dictionary.</remarks>
        Public Shared Function Sql2Dictionary(ByVal sql As String) As ExpDictionary
            Dim dt As DataTable = SQL2DataTable(sql)
            If dt.Rows.Count > 0 Then
                Return SetDictionary(dt.Rows(0))
            Else
                Return Nothing
            End If
        End Function

        ''' <summary>
        ''' Returns a dictionary with all the records from a sql result
        ''' using keyfields as top level element in dictionary
        ''' </summary>
        ''' <param name="sql">SQL statement.</param>
        ''' <param name="keyfields">Array or string with keyfields or keyfield.</param>
        ''' <returns>Dictionary</returns>
        ''' <remarks>Uses Sql2DictionariesEx internally</remarks>
        Public Shared Function Sql2Dictionaries(ByVal sql As String, ByVal keyfields As Object) As ExpDictionary
            Return Sql2DictionariesEx(sql, keyfields, 0)
        End Function

        ''' <summary>
        ''' Returns a dictionary with all the records from sql result
        ''' using each keyfield as a level in the resulting dictionary.
        ''' </summary>
        ''' <param name="sql">SQL statement.</param>
        ''' <param name="keyfields">Array or string with keyfields or keyfield.</param>
        ''' <returns>Dictionary</returns>
        ''' <remarks>Uses Sql2Dictionaries2Ex internally</remarks>
        Public Shared Function Sql2Dictionaries2(ByVal sql As String, ByVal keyfields As Object) As ExpDictionary
            Return Sql2Dictionaries2Ex(sql, keyfields, 0)
        End Function

        ''' <summary>
        ''' This function returns a number of records from the result of a sql query.
        ''' </summary>
        ''' <param name="sql">SQL statement.</param>
        ''' <param name="keyfields">Array or string with keyfields or keyfield.</param>
        ''' <param name="top">The number of records to return.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Sql2DictionariesEx(ByVal sql As String, Optional ByVal keyfields As Object = Nothing, Optional ByVal top As Integer = -1) As ExpDictionary
            Dim retv As ExpDictionary
            Dim dt As DataTable = SQL2DataTable(sql)
            retv = SetDictionariesEx(dt, keyfields, top)
            Return retv
        End Function

        ''' <summary>
        ''' This function returns a number of records from the result of a sql query.
        ''' Each keyfield will result in a level in the resulting dictionary.
        ''' </summary>
        ''' <param name="sql"></param>
        ''' <param name="keyfields"></param>
        ''' <param name="top"></param>
        ''' <returns></returns>
        ''' <remarks>The result is returned in a dictionary.</remarks>
        Public Shared Function Sql2Dictionaries2Ex(ByVal sql As String, ByVal keyfields As Object, ByVal top As Integer) As ExpDictionary
            Dim retv As ExpDictionary
            Dim dt As DataTable = SQL2DataTable(sql)
            retv = SetDictionaries2Ex(dt, keyfields, top)
            Return retv
        End Function

        ''' <summary>
        ''' Assigns values to the fields of a recordset from a dictionary.
        ''' </summary>
        ''' <param name="d">A dictionary containing the values.</param>
        ''' <param name="row">The row to receive the values.</param>
        ''' <remarks></remarks>
        Public Shared Sub SetDataRowEx(ByRef d As ExpDictionary, ByRef row As DataRow)
            Dim v As Object
            Dim byteArray As Byte()

            For i As Integer = 0 To row.Table.Columns.Count - 1
                Try
                    v = d(row.Table.Columns(i).ColumnName)
                Catch ex As KeyNotFoundException
                    v = ""
                End Try
                If Not row.Table.Columns(i).AutoIncrement Then
                    Select Case TypeName(v)
                        Case "String"
                            Dim t As Type = row.Table.Columns(i).DataType
                            If t.Name.Equals("Boolean") Then
                                v = CBoolEx(v)
                                Continue For
                            End If
                            If Not t.Name.Equals("Byte[]") Then
                                If row.Table.Columns(i).MaxLength >= 0 Then
                                    v = Left(v, row.Table.Columns(i).MaxLength) 'MaxLength property must be set on all fields!!
                                End If
                            Else
                                Try
                                    byteArray = Encoding.Default.GetBytes(v)
                                    row.Item(i) = byteArray
                                Catch ex As Exception
                                    FileLogger.log("SOME ERROR: " & ex.Message)
                                End Try
                                Continue For
                            End If
                    End Select
                    If Not v Is Nothing Then
                        Try
                            row.Item(i) = v
                        Catch ex As Exception
                            FileLogger.log("ERROR:" & ex.Message)
                        End Try
                    End If
                End If
            Next
        End Sub

        ''' <summary>
        ''' Updates the record located by the SQL statement with the values from the dict dictionary.
        ''' </summary>
        ''' <param name="dict"></param>
        ''' <param name="tableName"></param>
        ''' <param name="sql"></param>
        ''' <remarks>Overloaded Sub is acting as a wrapper around SetDataRowEx</remarks>
        Public Shared Sub SetDictUpdateEx(ByVal dict As ExpDictionary, ByVal tableName As String, _
            ByVal sql As String)

            Dim dt As New DataTable()
            Dim da As New SqlDataAdapter()
            Dim dr As DataRow
            Dim isNew As Boolean = False

            If sql = "" Then
                sql = "SELECT * FROM [" & tableName & "] WHERE 1=2"
                isNew = True
            End If

            dt = getDataTable(sql, da)

            ' Add a new row if none is available
            If dt.Rows.Count = 0 Then isNew = True

            Try
                If isNew Then
                    dr = dt.NewRow()
                    SetDataRowEx(dict, dr)
                    dt.Rows.Add(dr)
                Else
                    SetDataRowEx(dict, dt.Rows(0))
                End If
                da.Update(dt)
            Catch ex As Exception
            End Try

        End Sub

        ''' <summary>
        ''' Inserts a new record in the table given by the tablename parameter.
        ''' </summary>
        ''' <param name="dict">Dictionary with the values to insert.</param>
        ''' <param name="tableName">Name of the table that should have the new record.</param>
        ''' <remarks>Overloaded Sub is acting as a wrapper around SetDataRowEx</remarks>
        Public Shared Sub SetDictUpdateEx(ByVal dict As ExpDictionary, ByVal tableName As String)
            SetDictUpdateEx(dict, tableName, "")
        End Sub

        ''' <summary>
        ''' Executes Any SQL-statement. Does not return a reultset. 
        ''' </summary>
        ''' <param name="cmdText">SQL-Statement</param>
        ''' <remarks>
        ''' Useful for queries that don't require a resultset
        ''' </remarks>
        Public Shared Function excecuteNonQueryDb(ByVal cmdText As String) As String
            Dim oc As SqlCommand
            Dim message As String = String.Empty

            Using localConn As New SqlConnection(ConnStr)
                Try
                    localConn.Open()
                    oc = New SqlCommand(cmdText, localConn)
                    oc.CommandType = CommandType.Text
                    oc.ExecuteNonQuery()
                Catch ex As Exception
                    message = ex.Message
                End Try
            End Using
            Return message
        End Function

        ''' <summary>
        ''' This function returns a filled DataTable
        ''' </summary>
        ''' <param name="inSqlCommand">A SQL-Statement</param>
        ''' <param name="da">A SqlDataAdapter</param>
        ''' <returns>DataTable</returns>
        ''' <remarks></remarks>
        Public Shared Function getDataTable(ByVal inSqlCommand As String, Optional ByRef da As SqlDataAdapter = Nothing) As DataTable
            Dim od As SqlDataAdapter
            Dim dt As New DataTable()

            If da IsNot Nothing Then
                da = getDataAdapter(, inSqlCommand)
                da.Fill(dt)
            Else
                od = getDataAdapter(, inSqlCommand)
                od.Fill(dt)
            End If
            getDataTable = dt

        End Function

        ''' <summary>
        ''' This function returns a DataAdapter tied to a Database table
        ''' </summary>
        ''' <param name="tableName">The name of the db-table</param>
        ''' <param name="sqlSelectCommand">The SQL-Statement</param>
        ''' <returns>SqlDataAdapter</returns>
        ''' <remarks>
        ''' This Function creates commands for delete, insert and update, by using a CommandBuilder. 
        ''' It uses the MissingSchemaAction.AddWithKey option, so the db-table must contain at least
        ''' a unique index, to make the CommandBuilder work.        
        ''' </remarks>
        Public Shared Function getDataAdapter(Optional ByVal tableName As String = Nothing, _
            Optional ByVal sqlSelectCommand As String = Nothing) As SqlDataAdapter

            Dim da As SqlDataAdapter
            Dim dcmd As SqlCommandBuilder
            Dim localConnStr As String = ConnStr

            Select Case True
                Case sqlSelectCommand Is Nothing And tableName IsNot Nothing
                    da = New SqlDataAdapter("SELECT * FROM " & tableName & " WHERE 1 = 2", New String(localConnStr))
                Case sqlSelectCommand IsNot Nothing And tableName Is Nothing
                    da = New SqlDataAdapter(sqlSelectCommand, New String(localConnStr))
                Case sqlSelectCommand IsNot Nothing And tableName IsNot Nothing
                    da = New SqlDataAdapter(sqlSelectCommand & tableName, New String(localConnStr))
                Case Else
                    Return Nothing
            End Select

            Try
                dcmd = New SqlCommandBuilder(da)
                da.DeleteCommand = dcmd.GetDeleteCommand()
                da.InsertCommand = dcmd.GetInsertCommand()
                da.UpdateCommand = dcmd.GetUpdateCommand()
                da.MissingSchemaAction = MissingSchemaAction.AddWithKey
            Catch ex As Exception
            End Try

            Return da

        End Function

        ''' <summary>
        ''' Compare two dictionaries.
        ''' </summary>
        ''' <param name="d1">First dictionary.</param>
        ''' <param name="d2">Second dictionary.</param>
        ''' <param name="exclude">Array of field names to exclude in the comparison.</param>
        ''' <returns>This function returns true if the values in the dictionaries are identical.</returns>
        ''' <remarks>
        ''' All named values in d1, which are also in d2 will be compared.
        ''' The exclude parameter is an array containing names of dictionary entries, which
        ''' are excluded from the comparison.
        ''' </remarks>
        Public Shared Function CompareDict(ByVal d1 As ExpDictionary, ByVal d2 As ExpDictionary, ByVal exclude() As Object) As Boolean
            Dim n As Object

            For Each n In d1.Keys
                ' Do not compare if the name is in the exclude array
                If Not SearchArray(exclude, n) Then
                    ' Do not compare if one of the values are empty
                    If Not (d1(n) Is Nothing Or d2(n) Is Nothing) Then
                        ' Compare the values
                        If Not Compare(d1(n), d2(n)) Then
                            Return False
                        End If
                    End If
                End If
            Next
            Return True
        End Function

        ''' <summary>
        ''' Compare two values.
        ''' </summary>
        ''' <param name="v1">First value.</param>
        ''' <param name="v2">Second value.</param>
        ''' <returns>True if the string representation of the values are identical.</returns>
        ''' <remarks></remarks>
        Public Shared Function Compare(ByVal v1 As Object, ByVal v2 As Object) As Boolean
            v1 = CStrEx(v1)
            v2 = CStrEx(v2)
            Return v1 = v2
        End Function

        ''' <summary>
        ''' This function copies all values from one dictionary to another.
        ''' </summary>
        ''' <param name="dictDest">Destination dictionary.</param>
        ''' <param name="dictSrc">Source dictionary.</param>
        ''' <remarks>
        ''' If you use "a = b" for two dictionaries a and b, then a and b will point to the same dictionary.
        ''' If you change one dictionary you will change the other. To make a copy of the dictionary use this function.
        ''' </remarks>
        Public Shared Sub CopyDictValues(ByRef dictDest As ExpDictionary, ByRef dictSrc As ExpDictionary)
            Dim n As String

            ' Check that both inputs are valid dictionaries
            If dictDest Is Nothing Or dictSrc Is Nothing Then Return

            ' Transfer the values
            For Each n In dictSrc.Keys
                dictDest(n) = dictSrc(n)
            Next
        End Sub

        ''' <summary>
        ''' This function copies the value of fields from one recordset to another. 
        ''' Only fields found in both recordsets and not in the 'exclude' list is copied.
        ''' </summary>
        ''' <param name="rowDest">Destination recordset.</param>
        ''' <param name="drSrc">Source recordset.</param>
        ''' <param name="excludeFields">An array containing a list of fieldnames to exclude from the copy</param>
        ''' <remarks></remarks>
        Public Shared Sub TransferFields(ByVal rowDest As DataRow, ByVal drSrc As SqlDataReader, ByVal excludeFields() As String)
            Dim fieldname As String

            For i As Integer = 0 To drSrc.FieldCount - 1
                fieldname = drSrc.GetName(i)
                ' Check if the fieldname is in the exclude array
                If Array.IndexOf(excludeFields, fieldname) = -1 Then
                    If rowDest.Table.Columns.Contains(fieldname) Then
                        rowDest.Item(fieldname) = drSrc.GetValue(i)
                    End If
                End If
            Next

        End Sub

        ''' <summary>
        ''' The Sub sorts a dictionary. The routine uses the well known quick-sort algorithm,
        ''' and sorts the dictionary lexically. The routine can only sort in one direction a-z 0-9, 
        ''' consider negating numbers to do reverse sorting.
        ''' </summary>
        ''' <param name="d">Dictionary to be sorted</param>
        ''' <remarks></remarks>
        Public Shared Sub SortDictionary(ByRef d As ExpDictionary)
            SortDictionaryEx(d, Nothing)
        End Sub

        ''' <summary>
        ''' The function sorts a dictionary by the specified key in the specified dictionary.
        ''' The function uses the well known quick-sort algorithm and sorts the dictionary lexically.
        ''' The function can only sort in one direction a-z 0-9, consider negating numbers to do
        ''' reverse sorting.
        ''' </summary>        
        ''' <param name="d">Dictionary to be sorted</param>
        ''' <param name="keypath">Keypath: Is a string with a dot (.) separated path pointing to the value the dictionary should be sorted by.</param>
        ''' <remarks>
        ''' Returns the same dictionary sorted by the key specified.
        ''' Example: To sort the Group dictionary by ListPrice in pricelist.aspx call:
        ''' QuickSortDictionaryEx Group("Products"), "ListPrice"
        ''' </remarks>
        Public Shared Sub SortDictionary(ByRef d As ExpDictionary, ByVal keypath As String)
            SortDictionaryEx(d, keypath)
        End Sub

        ''' <summary>
        ''' The function sorts a dictionary by the specified key in the specified dictionary.
        ''' The function uses the well known quick-sort algorithm and sorts the dictionary lexically.
        ''' The function can only sort in one direction a-z 0-9, consider negating numbers to do
        ''' reverse sorting.
        ''' </summary>        
        ''' <param name="d">Dictionary to be sorted</param>
        ''' <param name="keypath">Keypath: Is a string with a dot (.) separated path pointing to the value the dictionary should be sorted by.</param>
        ''' <remarks>
        ''' Returns the same dictionary sorted by the key specified.
        ''' Example: To sort the Group dictionary by ListPrice in pricelist.aspx call:
        ''' QuickSortDictionaryEx Group("Products"), "ListPrice"
        ''' </remarks>
        Public Shared Sub SortDictionaryEx(ByRef d As ExpDictionary, ByVal keypath As String)
            If d.Count > 1 Then
                Dim keyvector(d.Count - 1) As Object
                Dim valuevector(d.Count - 1) As Object
                Dim keys(d.Count - 1) As Object
                Dim i As Integer
                Dim path As Object
                Dim tmp As Object

                ' Create 2d vector to be sorted
                d.Keys.CopyTo(keys, 0)
                If keypath Is Nothing Then
                    ' Sort the keys
                    For i = LBound(keys) To UBound(keys)
                        keyvector(i) = keys(i)
                        valuevector(i) = keys(i)
                    Next
                ElseIf keypath = "" Then
                    ' Sort the value
                    For i = LBound(keys) To UBound(keys)
                        keyvector(i) = d(keys(i))
                        valuevector(i) = keys(i)
                    Next
                Else
                    ' Sort some other value in the dict
                    path = Split(keypath, ".")
                    For i = 0 To d.Count - 1
                        keyvector(i) = ExtractValue(d(keys(i)), path)
                        valuevector(i) = keys(i)
                    Next
                End If

                ' Sort vector
                Array.Sort(keyvector, valuevector)

                ' Rebuild the dictionary
                Dim sortedDict As New ExpDictionary()
                For i = LBound(keyvector) To UBound(keyvector)
                    tmp = d(valuevector(i))
                    d.Remove(valuevector(i))
                    'd.Add(valuevector(i), tmp)  '# this is removed
                    sortedDict.Add(valuevector(i), tmp) '# this is added
                Next
                d = sortedDict '# this is added. d sent by reference is now pointing at the sortedDict
            End If
        End Sub

        ''' <summary>
        ''' The function returns true if anObj is a dictionary.
        ''' </summary>
        ''' <param name="anObj">The object to compare type with.</param>
        ''' <returns></returns>
        ''' <remarks>The shop uses dictionaries derived from Dictionary(Of Object, Object)</remarks>
        Public Shared Function IsDict(ByVal anObj As Object) As Boolean
            Return TypeName(anObj) = "ExpDictionary"
        End Function

        ''' <summary>
        ''' Returns the maximum number of records to return based on the ClientType
        ''' </summary>
        ''' <param name="Context">This object is not used.</param>
        ''' <returns></returns>
        ''' <remarks>
        ''' Return value is set to -1 since that is the maximum amount of records.
        ''' Web applications always returns all records.
        ''' </remarks>
        Public Shared Function MaxSearchResults(ByVal Context As Object) As Integer
            Dim recMax As Integer
            recMax = -1
            MaxSearchResults = recMax
        End Function

        ''' <summary>
        ''' Insert the dictionary (dict) values into a record in the TableName
        ''' </summary>
        ''' <param name="dict">Source dictionary</param>
        ''' <param name="TableName">Destination table.</param>
        ''' <param name="DeleteWhere">Insert a delete statement</param>
        ''' <returns>True on success, else false</returns>        
        ''' <remarks>Deletes a specified set of records if requested before saving data.</remarks>
        Public Shared Function Dict2Table(ByVal dict As ExpDictionary, ByVal TableName As String, ByVal DeleteWhere As String) As Boolean

            Dim sql As String
            Dim dt As New DataTable()
            Dim da As New SqlDataAdapter()
            Dim dr As DataRow
            Dim result As Boolean = True
            ' Delete records if so required
            If DeleteWhere <> "" Then
                sql = "DELETE FROM " & TableName & " WHERE " & DeleteWhere
                excecuteNonQueryDb(sql)
            End If

            sql = "SELECT * FROM " & TableName & " WHERE 1 = 2"

            dt = getDataTable(sql, da)

            Try
                dr = dt.NewRow()
                SetDataRowEx(dict, dr)
                dt.Rows.Add(dr)
                da.Update(dt)
            Catch ex As Exception
                result = False
            End Try
            Return result
        End Function

        ''' <summary>
        ''' Insert the dictionaries (dicts) and corresponding values into a number of records in the TableName
        ''' </summary>
        ''' <param name="dicts">Source dictionary with sub dictinaries.</param>
        ''' <param name="TableName">Destination table.</param>
        ''' <param name="DeleteWhere">Insert a delete statement.</param>
        ''' <remarks>Deletes a specified set of records if requested before saving data.</remarks>
        Public Shared Sub Dicts2Table(ByVal dicts As ExpDictionary, ByVal TableName As String, ByVal DeleteWhere As String)
            Dim sql As String
            Dim dict As ExpDictionary
            Dim dt As New DataTable()
            Dim da As New SqlDataAdapter()
            Dim row As DataRow

            ' Delete records if so required
            If DeleteWhere <> "" Then
                sql = "DELETE FROM [" & TableName & "] WHERE " & DeleteWhere
                excecuteNonQueryDb(sql)
            End If

            sql = "SELECT * FROM [" & TableName & "] WHERE 1=2"
            dt = getDataTable(sql, da)

            For Each dict In dicts.Values
                row = dt.NewRow()
                SetDataRowEx(dict, row)
                dt.Rows.Add(row)
            Next
            da.Update(dt)
        End Sub

        ''' <summary>
        ''' This function returns a string with a list of identifiers used in "IN" SQL statements.
        ''' </summary>
        ''' <param name="DictItems">Dictionary containing sub-dictionaries</param>
        ''' <param name="Id">Key name to use for the list.</param>
        ''' <returns>A String containing a SQL-Statement</returns>
        ''' <remarks>
        ''' The function is used to generate sql statements like this one:
        ''' SELECT * FROM ProductTable WHERE ProductGuid IN ('9646-05','9647-05','9026-05')
        ''' </remarks>
        Public Shared Function GetSQLInString(ByVal DictItems As ExpDictionary, ByVal Id As String) As String
            Dim i As Integer
            Dim retarray() As String
            Dim dictCount As ExpDictionary
            Dim productKey As String
            Dim key As String

            If DictItems.Count > 0 Then
                i = 0
                ReDim retarray(DictItems.Count - 1)
                dictCount = New ExpDictionary
                For Each key In DictItems.Keys
                    productKey = SafeString(DictItems(key)(Id))
                    If productKey <> "NULL" Then
                        dictCount(productKey) = CLngEx(dictCount(productKey)) + 1

                        ' Only add items not addded before.
                        If dictCount(productKey) = 1 Then
                            retarray(i) = productKey
                            i = i + 1
                        End If
                    End If
                Next
                ReDim Preserve retarray(i - 1)
                Return Join(retarray, ",")
            Else
                Return ""
            End If
        End Function

        ''' <summary>
        ''' Updates a SQL-DB Table with values from a dictionary. This Sub is necessary for updating a table
        ''' when no indexed fields appear in the SQL-statement         
        ''' </summary>
        ''' <param name="dict">The dictionary holding the values for update</param>
        ''' <param name="tableName">The name of the table to update</param>
        ''' <param name="conditionColumn">The column in the WHERE clause of the SQL-statement</param>
        ''' <param name="condition">The condition to try against/look for</param>
        ''' <param name="conditionColumn2">The second (optional) column in the WHERE clause of the SQL-statement</param>
        ''' <param name="condition2">The second (optional) condition to try against/look for</param>
        ''' <remarks>This method compares the keys in the dictionary with the column names in the DB-Table.
        ''' Any attempt to update the table will only be done with the relevant fields in the dictionary.</remarks>
        Public Shared Sub dict2UpdateSQL(ByVal dict As ExpDictionary, ByVal tableName As String, _
            ByVal conditionColumn As String, ByVal condition As String, _
                Optional ByVal conditionColumn2 As String = "", _
                    Optional ByVal condition2 As String = "")

            Dim sb As New StringBuilder()

            Dim colNameDict As New ExpDictionary()
            Using conn As New SqlConnection(ConnStr)
                Try
                    conn.Open()
                    Dim cmdSQLStr As String = "SELECT * FROM CartHeader WHERE 1 = 2"
                    Dim cmd As New SqlCommand(cmdSQLStr, conn)
                    Dim dr As SqlDataReader = cmd.ExecuteReader()
                    dr.Read()
                    For i As Integer = 0 To dr.FieldCount - 1
                        colNameDict.Add(dr.GetName(i), dr.GetName(i))
                    Next
                    dr.Close()
                Catch ex As Exception
                End Try
            End Using

            Dim cond As String = " WHERE " & conditionColumn & " = " & SafeString(condition) & _
                " AND " & conditionColumn2 & " = " & SafeString(condition2)

            For Each item As KeyValuePair(Of Object, Object) In dict
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
                If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
                    Dim pageobj As ExpandIT.Page = CType(System.Web.HttpContext.Current.Handler, ExpandIT.Page)
                    Dim csrObj As New USCSR(pageobj.globals)
                    csrObj.buildDict2UpdateSQLs(colNameDict, item, sb, tableName, cond)
                Else
                    If colNameDict.Exists(item.Key) Then
                        sb.Append(" UPDATE " & tableName & " SET ")
                        sb.Append(item.Key)
                        sb.Append(" = ")
                        sb.Append(SafeString(item.Value))
                        sb.Append(cond)
                        sb.AppendLine(";")
                    End If
                End If
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
            Next
            excecuteNonQueryDb(sb.ToString())

        End Sub
#End Region
#Region "wrappers"

        ''' <summary>
        ''' Replacement of the built in ASP version of server.htmlencode.
        ''' </summary>
        ''' <param name="v">Object to encode.</param>
        ''' <returns>The html encoded string.</returns>
        ''' <remarks></remarks>
        Public Shared Function HTMLEncode(ByVal v As Object) As String
            v = CStrEx(v)
            If v = "" Then HTMLEncode = "" Else HTMLEncode = HttpContext.Current.Server.HtmlEncode(v)
        End Function

        Public Shared Function GetFirst(ByVal dict As ExpDictionary) As KeyValuePair(Of Object, Object)
            Dim enumerator As Generic.IEnumerator(Of KeyValuePair(Of Object, Object))
            enumerator = dict.GetEnumerator
            If enumerator.MoveNext Then
                Return enumerator.Current
            Else
                Return Nothing
            End If
        End Function

        Public Shared Function DBNull2Nothing(ByVal v As Object) As Object
            If v Is DBNull.Value Then
                Return Nothing
            Else
                Return v
            End If
        End Function

        Public Shared Function IsDBNullOrNothing(ByVal v As Object) As Boolean
            Dim x As Object = DBNull2Nothing(v)
            Return x Is Nothing
        End Function

        Public Shared Function IsNull(ByVal v As Object) As Boolean
            Return (v Is DBNull.Value)
        End Function

        Public Shared Function Abs(ByVal v As Object) As Object
            Return Math.Abs(v)
        End Function

        Public Shared Function Round(ByVal v As Object) As Object
            Return Math.Round(v)
        End Function

        Public Shared Function GetRequest(ByVal varname As String) As String
            Dim retv As String = ""

            Try
                If Not HttpContext.Current.Request(varname) Is Nothing Then retv = HttpContext.Current.Request(varname)
            Catch
                retv = ""
            End Try

            Return retv
        End Function

        Public Shared Function QueryString2Dict() As Dictionary(Of String, ArrayList)
            Dim q, f, s As String

            q = HttpContext.Current.Request.QueryString.ToString
            f = HttpContext.Current.Request.Form.ToString

            s = q
            If f <> "" Then
                If s <> "" Then s = s & "&"
                s = s & f
            End If

            Return QueryString2Dict(s)
        End Function

        Public Shared Function QueryString2Dict(ByVal s As String) As Dictionary(Of String, ArrayList)
            Dim retv As Dictionary(Of String, ArrayList) = New Dictionary(Of String, ArrayList)
            Dim a() As String
            Dim e As String
            Dim n, v As String
            Dim i As Integer
            Dim stringlist As ArrayList

            If s = "" Then Return retv

            a = s.Split("&")

            For Each e In a
                i = e.IndexOf("=")
                If i >= 0 Then
                    n = e.Substring(0, i)
                    v = e.Substring(i + 1)
                Else
                    n = e
                    v = ""
                End If
                n = HttpUtility.UrlDecode(n)
                v = HttpUtility.UrlDecode(v)

                If retv.ContainsKey(n) Then
                    stringlist = retv(n)
                Else
                    stringlist = New ArrayList()
                    retv.Add(n, stringlist)
                End If
                stringlist.Add(v)
            Next

            Return retv
        End Function

        Public Shared Function ReadTextFile(ByVal fn As String) As String
            Dim retv As String
            Dim sr As New StreamReader(New FileStream(fn, FileMode.Open, FileAccess.Read), System.Text.Encoding.Default)
            retv = sr.ReadToEnd
            sr.Close()
            Return retv
        End Function

#End Region
#Region "lib_datamanip"

        ''' <summary>
        ''' Checks if a string is matched by a regular expression.
        ''' </summary>
        ''' <param name="testStr">String to match.</param>
        ''' <param name="pattern">Regular expression used to validate the testStr string.</param>
        ''' <returns>Boolean</returns>
        ''' <remarks></remarks>
        Public Shared Function RegExpTest(ByVal testStr As String, ByVal pattern As String) As Boolean
            Dim regex As New Regex(pattern, RegexOptions.IgnoreCase)
            Return regex.IsMatch(testStr)
        End Function

        ''' <summary>
        ''' Encrypts a string.
        ''' </summary>
        ''' <param name="s">s: This parameter specifies the string that you want to encrypt.</param>
        ''' <returns>An encrypted string</returns>
        ''' <remarks>
        ''' The encryption is actually a scrambling of the string. Scrambling makes a password unreadable, 
        ''' but it can still be decrypted by the reverse function of Encrypt.
        ''' </remarks>
        Public Shared Function EncryptXOrWay(ByVal s As Object) As String
            Dim r As String
            Dim i, ch As Integer

            r = ""
            For i = 1 To Len(s)
                ch = Asc(Mid(s, i, 1))
                ch = ch Xor 111
                r = r & Right("0" & Hex(ch), 2)
            Next
            Return r
        End Function

        ''' <summary>
        ''' Generates a unique identifier for use in database keys.
        ''' </summary>
        ''' <returns>String</returns>
        ''' <remarks>
        ''' The function generates an identifier of the form:
        ''' {DDDDTTTT-TTCC-CCRR-RRRR-RRRRRRRRRRRR}
        ''' D = 4 digit Hex representation of CLng(date())
        ''' T = 6 digit Hex representation of Timer() * 100
        ''' C = 4 digit Hex cyclic counter 0-65536
        ''' R = Random hex digit
        '''</remarks>
        Public Shared Function GenGuid() As String
            Return System.Guid.NewGuid().ToString()
        End Function

        ''' <summary>
        ''' Generates a random string of a specified length.
        ''' </summary>
        ''' <param name="resultlen">Length of the random string.</param>
        ''' <returns>String</returns>
        ''' <remarks>
        ''' The generated string will not contain letters that are easily mistaken for other letters or numbers.
        ''' This could be the case for O (The letter o) and 0 (zero).
        ''' </remarks>
        Public Shared Function RandomString(ByVal resultlen As Integer) As String
            Const validchars As String = "ABCDEFGHIJKLMNPQRSTUVWXYZ123456789"
            Dim retval As String = ""
            Dim vl As Integer

            ' 0 and O are not valid chars because they are difficult to distinguish
            vl = Len(validchars)
            For i As Integer = 1 To resultlen
                retval = retval & Mid(validchars, Int(Rnd() * vl) + 1, 1)
            Next
            Return retval
        End Function

        ''' <summary>
        ''' Generates a random number of a specified length.
        ''' </summary>
        ''' <param name="resultlen">Length of the random number.</param>
        ''' <returns>String</returns>
        ''' <remarks></remarks>
        Public Shared Function RandomNumber(ByVal resultlen As Integer) As String
            Const validchars As String = "0123456789"
            Dim retval As String = ""
            Dim vl As Integer

            vl = Len(validchars)
            For i As Integer = 1 To resultlen
                retval = retval & Mid(validchars, Int(Rnd() * vl) + 1, 1)
            Next
            Return retval
        End Function

        ''' <summary>
        ''' This function takes a string s with wildcard parameters like %1% and %2% and replaces them with the
        ''' values in the array a.
        ''' </summary>
        ''' <param name="s">String containing n wildcards in the format %x%.</param>
        ''' <param name="a">An array containing n values to be replace the wildcards in string s.</param>
        ''' <returns>String</returns>
        ''' <remarks>
        ''' Returns the string s with the values from a instead of %x%.
        ''' Example: ReplaceParams("Hello %1%", Array("World"))  Returns the sting  "Hello World"        
        ''' </remarks>
        Public Shared Function ReplaceParams(ByVal s As String, ByVal a As Object) As String
            Dim retv As String

            If Right(TypeName(a), 2) <> "()" Then a = New String() {a}
            retv = s
            For i As Integer = 0 To UBound(a)
                retv = Replace(retv, "%" & i + 1 & "%", a(i))
            Next
            ReplaceParams = retv
        End Function

        ''' <summary>
        ''' Encodes a string by replacing a delimiter character with two delimiters.
        ''' </summary>
        ''' <param name="s">String to encode.</param>
        ''' <param name="delim">Delimiter to replace.</param>
        ''' <returns>String</returns>
        ''' <remarks>This function is mostly used in conjunction with generation of SQL statements.</remarks>
        Public Shared Function Encode(ByVal s As String, ByVal delim As String) As String
            Dim retval As String = ""
            Dim ch As Char

            If InStr(1, s, delim) <> 0 Then
                For i As Integer = 1 To Len(s)
                    ch = Mid(s, i, 1)
                    If ch = delim Then ch = delim & delim
                    retval = retval & ch
                Next
            Else
                retval = s
            End If

            Return retval
        End Function

        ''' <summary>
        ''' ExtractValue
        ''' </summary>
        ''' <param name="d">dictionary</param>
        ''' <param name="path">path (array)</param>
        ''' <returns>Object</returns>
        ''' <remarks>Used by QuickSortDictionaryEx</remarks>
        Public Shared Function ExtractValue(ByVal d As ExpDictionary, ByVal path() As Object) As Object
            Dim retv As Object = d
            Dim ub As Integer = UBound(path)

            For i As Integer = 0 To ub
                If i < ub Then
                    Try
                        retv = retv(path(i))
                    Catch ex As Exception
                        retv = Nothing
                    End Try
                Else
                    Try
                        retv = retv(path(i))
                    Catch ex As Exception
                        retv = Nothing
                    End Try
                End If

                If retv Is Nothing Then Exit For
            Next
            Return retv
        End Function

        ''' <summary>
        ''' Makes a safe conversion to a string.
        ''' </summary>
        ''' <param name="v">Value to convert.</param>
        ''' <returns>String</returns>
        ''' <remarks>
        ''' Returns the string representing v. If v cannot be converted to a string,
        ''' the function returns the empty string.
        ''' </remarks>
        Public Shared Function CStrEx(ByVal v As Object) As String
            Dim retv As String
            Try
                retv = v.ToString
            Catch
                retv = ""
            End Try
            Return retv
        End Function

        ''' <summary>
        ''' Makes a safe conversion to a long.
        ''' </summary>
        ''' <param name="v">Value to convert.</param>
        ''' <returns>Int32</returns>
        ''' <remarks>
        ''' Returns the long representing v. If v cannot be converted to a long the function returns 0 (zero).
        ''' </remarks>
        Public Shared Function CLngEx(ByVal v As Object) As Int32
            Dim retv As Int32 = 0
            Try
                retv = Int32.Parse(v)
            Catch
                retv = 0
            End Try
            Return retv
        End Function

        ''' <summary>
        ''' Makes a safe conversion to an Integer.
        ''' </summary>
        ''' <param name="v">Value to convert.</param>
        ''' <returns>Integer</returns>
        ''' <remarks>
        ''' Returns the integer representing v. If v cannot be converted to a integer the function returns 0 (zero).
        ''' </remarks>
        Public Shared Function CIntEx(ByVal v As Object) As Integer
            Dim retv As Integer = 0
            Try
                retv = Integer.Parse(v)
            Catch
                retv = 0
            End Try
            Return retv
        End Function

        ''' <summary>
        ''' Makes a safe conversion to a Decimal.
        ''' </summary>
        ''' <param name="v">Value to convert.</param>
        ''' <returns>Decimal</returns>
        ''' <remarks>
        ''' Returns the Decimal representing v. If v cannot be converted to a Decimal the function returns 0 (zero).
        ''' </remarks>
        Public Shared Function CDblEx(ByVal v As Object) As Decimal
            CDblEx = 0
            On Error Resume Next
            CDblEx = CDec(v)
            Err.Clear()
        End Function

        ''' <summary>
        ''' Makes a safe conversion to a boolean.
        ''' </summary>
        ''' <param name="v">Value to convert.</param>
        ''' <returns>Boolean</returns>
        ''' <remarks>
        ''' Returns the boolean representing v. If v cannot be converted to a boolean the function returns false.
        ''' </remarks>
        Public Shared Function CBoolEx(ByVal v As Object) As Boolean
            CBoolEx = 0
            On Error Resume Next
            CBoolEx = CBool(v)
            Err.Clear()
        End Function

        ''' <summary>
        ''' Makes a safe conversion to a date.
        ''' </summary>
        ''' <param name="v">Value to convert.</param>
        ''' <returns>Date</returns>
        ''' <remarks>
        ''' Returns the date representing v. If v cannot be converted to a date the function returns CDate(0).
        ''' </remarks>
        Public Shared Function CDateEx(ByVal v As Object) As Date
            CDateEx = Date.MinValue
            On Error Resume Next
            CDateEx = CDate(v)
            Err.Clear()
        End Function

        ''' <summary>
        ''' Converts a value to a float.
        ''' </summary>
        ''' <param name="s">Value to convert.</param>
        ''' <returns>Decimal</returns>
        ''' <remarks>The function returns the float represented by the string.</remarks>
        Public Shared Function String2Float(ByVal s As Object) As Decimal
            Dim f As Object

            If Len(s) > 9 Then s = "1"
            s = Replace(s, ",", Mid(1.1, 2, 1))
            s = Replace(s, ".", Mid(1.1, 2, 1))
            f = 0
            On Error Resume Next
            f = CDec(s)
            On Error GoTo 0
            String2Float = f
        End Function

        ''' <summary>
        ''' Converts a value to an unsigned long.
        ''' </summary>
        ''' <param name="s">Value to convert.</param>
        ''' <returns>Object</returns>
        ''' <remarks>The function returns the unsigned long represented by the string. 
        ''' If the passed parameter is a negative number it will be converted to a positive number.
        ''' </remarks>
        Public Shared Function String2ULong(ByVal s As Object) As Object
            Dim l As Object

            If Len(s) > 9 Then s = "1"
            l = 0
            On Error Resume Next
            l = Abs(CLngEx(s))
            On Error GoTo 0
            String2ULong = l
        End Function

        ''' <summary>
        ''' Converts a value to an unsigned float.
        ''' </summary>
        ''' <param name="s">Value to convert.</param>
        ''' <returns>Decimal</returns>
        ''' <remarks>
        ''' The function returns the unsigned float represented by the string. 
        ''' If the passed parameter is a negative number it will be converted to a positive number.
        ''' </remarks>
        Public Shared Function String2UFloat(ByVal s As Object) As Decimal
            Return Abs(String2Float(s))
        End Function

        ''' <summary>
        ''' Makes a safe conversion to a Decimal.
        ''' </summary>
        ''' <param name="anyType">Value to convert.</param>
        ''' <returns>Decimal</returns>
        ''' <remarks>Deprecated. Use CDblEx instead.</remarks>
        Public Shared Function ToDbl(ByVal anyType As Object) As Decimal
            Return CDblEx(anyType)
        End Function

        ''' <summary>
        ''' Format a date using local 1030 (Danish)
        ''' </summary>
        ''' <param name="d">Date</param>
        ''' <returns>Returns a string containing the formatted date.</returns>
        ''' <remarks>Use FormatDateEx if you prefere another locale other than the default (1030).</remarks>
        Public Shared Function FormatDate(ByVal d As Object) As String
            Return FormatDateEx(d, 1030)
        End Function

        ''' <summary>
        ''' Format a date using a specific locale id
        ''' </summary>
        ''' <param name="d">Date</param>
        ''' <param name="nlocaleID">The locale identifier</param>
        ''' <returns>Returns a string containing the formatted date.</returns>
        ''' <remarks></remarks>
        Public Shared Function FormatDateEx(ByVal d As Object, ByVal nlocaleID As Object) As String
            Dim _d As Date = CDateEx(d)
            'Return FormatDateTime(d, vbLongDate)
            Return _d.ToString("D", CultureInfo.CreateSpecificCulture(Threading.Thread.CurrentThread.CurrentUICulture.Name))
        End Function

        ''' <summary>
        ''' Left pads a string to a specified width.
        ''' </summary>
        ''' <param name="s">String to pad.</param>
        ''' <param name="ch">Character used for padding.</param>
        ''' <param name="n">Width of resulting string.</param>
        ''' <returns>The string s padded to length n using the character ch.</returns>
        ''' <remarks></remarks>
        Public Shared Function LPad(ByVal s As String, ByVal ch As Char, ByVal n As Integer) As String
            Return s.ToString.PadLeft(n, ch)
        End Function

        ''' <summary>
        ''' Right pads a string to a specified width.
        ''' </summary>
        ''' <param name="s">String to pad.</param>
        ''' <param name="ch">Character used for padding.</param>
        ''' <param name="n">Width of resulting string.</param>
        ''' <returns>The string s padded to length n using the character ch.</returns>
        ''' <remarks></remarks>
        Public Shared Function RPad(ByVal s As String, ByVal ch As Char, ByVal n As Integer) As String
            Return s.ToString.PadRight(n, ch)
        End Function

        ''' <summary>
        ''' Encodes a string for use in a SQL statement.
        ''' </summary>
        ''' <param name="s">String to encode.</param>
        ''' <returns>The encoded string. Null values are encoded as NULL.</returns>
        ''' <remarks></remarks>
        Public Shared Function SafeString(ByVal s As Object, Optional ByVal isPreserveEmptyString As Boolean = False) As String
            s = CStrEx(s)
            If s = "" Then
                Return IIf(isPreserveEmptyString, "N'" & Replace(s, "'", "''") & "'", "NULL")
            Else
                Return "N'" & Replace(s, "'", "''") & "'"
            End If
        End Function

        ''' <summary>
        ''' This function is used to generate an equal part of a sql statement. The function handles NULL values.
        ''' </summary>
        ''' <param name="v">String to encode.</param>
        ''' <returns>String</returns>
        ''' <remarks>
        ''' If v is null (determined by SafeString) the function returns the string "IS NULL"
        ''' If v is value, the function returns the string "= 'value'"
        ''' The return value is used in SQL statements where the value can be null.
        '''</remarks>
        Public Shared Function SafeEqualString(ByVal v As Object) As String
            v = SafeString(v)
            If CStrEx(v) = "NULL" Then
                Return "IS NULL"
            End If
            Return "= " & v
        End Function

        ''' <summary>
        ''' Encodes a value to an unsigned float for use in a SQL statement.
        ''' </summary>
        ''' <param name="f">Value to encode.</param>
        ''' <returns>An encoded String</returns>
        ''' <remarks>
        ''' The function returns the encoded string. If the passed parameter is a negative number,
        ''' it will be converted to a positive number.
        ''' </remarks>
        Public Shared Function SafeUFloat(ByVal f As Object) As String
            Dim s As String

            If IsNull(f) Then f = 0
            f = Math.Round(f, 10)
            f = Replace(f, ",", Mid(1.1, 2, 1))
            f = Replace(f, ".", Mid(1.1, 2, 1))
            On Error Resume Next
            f = Abs(CDec(f))
            If Err.Number <> 0 Then f = 0
            On Error GoTo 0
            s = CStr(Int(f)) & Mid(1.1, 2, 1) & Mid(CStr(Math.Round(f - Int(f), 10)), 3)
            If Right(s, 1) = "." Or Right(s, 1) = "," Then s = s & "0"
            Return s
        End Function

        ''' <summary>
        ''' Encodes a value to a float for use in a SQL statement.
        ''' </summary>
        ''' <param name="f">Value to encode.</param>
        ''' <returns>An encoded String</returns>
        ''' <remarks></remarks>
        Public Shared Function SafeFloat(ByVal f As Object) As String
            Dim s As String

            If IsNull(f) Then f = 0
            f = RoundEx(f, 10)
            s = CStr(f)
            ' Get the commaseparator and replace that by dot (.)
            ' This is done to make ADO understand the comma.
            Dim CommaSeparator As String
            CommaSeparator = Mid(CDblEx(11 / 10), 2, 1)
            Return Replace(s, CommaSeparator, ".")
        End Function

        ''' <summary>
        ''' Encodes a value to a long for use in a SQL statement.
        ''' </summary>
        ''' <param name="v">Value to encode.</param>
        ''' <returns>Integer</returns>
        ''' <remarks></remarks>
        Public Shared Function SafeLong(ByVal v As Object) As Integer
            Return CLngEx(v)
        End Function

        ''' <summary>
        ''' Encodes a value to an unsigned long for use in a SQL statement.
        ''' </summary>
        ''' <param name="v">Value to encode.</param>
        ''' <returns>Integer</returns>
        ''' <remarks>
        ''' The function returns an Integer. If the passed parameter is a negative number,
        ''' it will be converted to a positive number.
        ''' </remarks>
        Public Shared Function SafeULong(ByVal v As Object) As Integer
            Dim retval As Integer

            retval = SafeLong(v)
            If retval < 0 Then retval = 0
            SafeULong = retval
        End Function

        ''' <summary>
        ''' Returns Zero (0) as Integer
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetZeroDate() As Integer
            Return 0
        End Function

        ''' <summary>
        ''' Encodes a value to a date for use in a SQL statement.
        ''' </summary>
        ''' <param name="d">Value to encode.</param>
        ''' <returns>The encoded string</returns>
        ''' <remarks>
        ''' The function returns the encoded string. This function returns different encodings 
        ''' based on the actual database engine. 
        '''</remarks>
        Public Shared Function SafeDatetime(ByVal d As Object) As Object
            Dim retv As String
            Dim dateval As Object

            dateval = DBNull.Value
            On Error Resume Next
            dateval = CDate(d)
            On Error GoTo 0

            If Not IsNull(dateval) Then
                retv = "{ts '" & LPad(Year(dateval), "0", 4) & "-" & LPad(Month(dateval), "0", 2) & "-" & LPad(Day(dateval), "0", 2) & " " & LPad(Hour(dateval), "0", 2) & ":" & LPad(Minute(dateval), "0", 2) & ":" & LPad(Second(dateval), "0", 2) & "'}"
            Else
                retv = "NULL"
            End If
            SafeDatetime = retv
        End Function

        ''' <summary>
        ''' Searches an array for a value.
        ''' </summary>
        ''' <param name="a">Haystack.</param>
        ''' <param name="v">Needle.</param>
        ''' <returns>The function returns true if the value is found.</returns>
        ''' <remarks></remarks>
        Public Shared Function SearchArray(ByVal a As Object, ByVal v As Object) As Boolean
            For i As Integer = 0 To UBound(a)
                If a(i) = v Then
                    Return True
                End If
            Next
            Return False
        End Function

        ''' <summary>
        ''' Determines if a value is of type string.
        ''' </summary>
        ''' <param name="v">Value to check.</param>
        ''' <returns>Boolean</returns>
        ''' <remarks>
        ''' NaS is short for Not a String. This function returns true if the type of v is string. 
        ''' Otherwise it returns false.
        ''' </remarks>
        Public Shared Function NaS(ByVal v As Object) As Boolean
            Return TypeName(v) = "String"
        End Function

        ''' <summary>
        ''' Determines if a variable contains a value.
        ''' </summary>
        ''' <param name="v">Value to check.</param>
        ''' <returns>Boolean</returns>
        ''' <remarks>
        ''' NaV is short for Not a Value. If v is a valid value this function returns true. Otherwise it returns false. 
        ''' Valid values are values that are not empty or null.
        ''' </remarks>
        Public Shared Function NaV(ByVal v As Object) As Boolean
            NaV = IsNull(v) Or v Is Nothing
        End Function

        ''' <summary>
        ''' Implementation of the Min function
        ''' </summary>
        ''' <param name="v1">variable 1</param>
        ''' <param name="v2">variable 2</param>
        ''' <returns>Object</returns>
        ''' <remarks>Returns v1 if v1 less than v2 otherwise it returns v2.</remarks>
        Public Shared Function min(ByVal v1 As Object, ByVal v2 As Object) As Object
            If v1 < v2 Then
                min = v1
            Else
                min = v2
            End If
        End Function

        ''' <summary>
        ''' This function rounds "value" to "decimal" number of decimals.
        ''' Rounding algorithm is configurable with web.config parameter USE_BANKER_ROUNDING
        ''' If param is 1, function use Banker's algorithm, otherwise using Symmetric arithmetic algorithm, always rounding half-way numbers up.               
        ''' </summary>
        ''' <param name="value">The value to do rounding on.</param>
        ''' <param name="decimals">Number of decimals to round value to.</param>
        ''' <returns>Decimal</returns>
        ''' <remarks>Returns the rounded value</remarks>
        Public Shared Function RoundEx(ByVal value As Decimal, ByVal decimals As Integer) As Decimal
            Return Math.Round(value, decimals, RoundingMode)
        End Function

        ''' <summary>
        ''' This method takes a url and checks that it refers to an address within the same domain.
        ''' If it passes the check, a redirect will occur.        
        ''' </summary>
        ''' <param name="aURL"></param>
        ''' <param name="endResponse"></param>
        ''' <remarks></remarks>
        Public Shared Sub SafeRedirect(ByVal aURL As String, Optional ByVal endResponse As Boolean = False)
            Dim serverPath As String = HttpContext.Current.Request.ApplicationPath
            Dim rawUrl As String = String.Empty
            Dim isValidQueryString As Boolean = True
            Dim isvalidUrl As Boolean = True
            Dim position As Integer = InStr(HttpContext.Current.Server.UrlEncode(aURL), HttpContext.Current.Server.UrlEncode("?"))

            If position > 0 Then
                rawUrl = Left(aURL, position - 1)
            Else
                rawUrl = aURL
            End If

            isvalidUrl = Regex.Match(HttpContext.Current.Server.UrlEncode(rawUrl), HttpContext.Current.Server.UrlEncode(serverPath)).Success

            If isvalidUrl Then
                HttpContext.Current.Response.Redirect(HttpContext.Current.Server.UrlDecode(aURL), endResponse)
            Else
                HttpContext.Current.Response.Redirect("~/default.aspx")
            End If
        End Sub

#End Region

    End Class

End Namespace