Imports System.Configuration.ConfigurationManager
Imports Microsoft.VisualBasic
Imports System.IO
Imports ExpandIT.GlobalsClass
Imports ExpandIT.B2BBaseClass
Imports ExpandIT.BuslogicBaseClass
Imports ExpandIT.DatabasePathClass
Imports ExpandIT.CurrencyBaseClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT.Debug
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Generic
Imports System.Net.Mail.SmtpClient
Imports System.IO.Compression
Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Xml
Imports ExpandIT.SectionSettingsManager
Imports ExpandIT.EISClass

Namespace ExpandIT

    Public Class promotions

        Private globals As GlobalsClass
        'AM0122201001 - UOM - Start
        Private USUOM As USUOM
        'AM0122201001 - UOM - End

        Public Sub New(ByVal _globals As GlobalsClass)
            globals = _globals
            'AM0122201001 - UOM - Start
            If AppSettings("EXPANDIT_US_USE_UOM") Then
                USUOM = New USUOM()
            End If
            'AM0122201001 - UOM - End
        End Sub

        Public Sub CalcShippingHandlingDiscount(ByVal OrderDict As ExpDictionary)

            Dim strSQL As String = 0
            Dim dblPromoShippingDisc As Double = 0
            Dim dictPromoHeader As ExpDictionary
            Dim dictPromoLine As ExpDictionary
            Dim ProviderGuid As String
            Dim subProviderGuid As String

            ProviderGuid = CStrEx(OrderDict("ShippingHandlingProviderGuid"))
            subProviderGuid = CStrEx(OrderDict("ServiceCode"))

            If (Not IsNull(OrderDict("PromotionCode"))) And (ProviderGuid <> "") Then
                If ProviderGuid = "NONE" And subProviderGuid = "" Then
                    ProviderGuid = ""
                    subProviderGuid = ""
                End If
                strSQL = "SELECT * FROM Promotion WHERE " & _
                             "PromotionCode = " & SafeString(OrderDict("PromotionCode")) & " " & _
                             "AND (([PromotionStartDate] <= " & SafeDatetime(DateValue(Now)) & ") " & _
                                    "AND " & _
                                   "([PromotionEndDate] >= " & SafeDatetime(DateValue(Now)) & ")) " & _
                             "AND (([NoOfOpportunities] > (SELECT COUNT(*) As Total FROM ShopSalesHeader WHERE " & _
                                                           "[PromotionCode] = " & SafeString(OrderDict("PromotionCode")) & " " & _
                                                           "AND (([HeaderDate] >= (SELECT [PromotionStartDate] FROM Promotion WHERE " & _
                                                                                   "[PromotionCode] = " & SafeString(OrderDict("PromotionCode")) & " " & _
                                                                                   "AND (([PromotionStartDate] <= " & SafeDatetime(DateValue(Now)) & ") " & _
                                                                                          "AND " & _
                                                                                         "([PromotionEndDate] >= " & SafeDatetime(DateValue(Now)) & "))))) " & _
                                                                    "AND " & _
                                                                 "([HeaderDate] <= (SELECT [PromotionEndDate] FROM Promotion WHERE " & _
                                                                                    "[PromotionCode] = " & SafeString(OrderDict("PromotionCode")) & " " & _
                                                                                    "AND (([PromotionStartDate] <= " & SafeDatetime(DateValue(Now)) & ") " & _
                                                                                          "AND " & _
                                                                                         "([PromotionEndDate] >= " & SafeDatetime(DateValue(Now)) & ")))))) " & _
                                        "OR " & _
                                    "([NoOfOpportunities] = 0))"
                dictPromoHeader = Sql2Dictionary(strSQL)
                If Not (dictPromoHeader Is Nothing) Then
                    strSQL = "SELECT * FROM [PromotionEntries] WHERE " & _
                                "[PromotionGuid] = " & dictPromoHeader("PromotionGuid") & " " & _
                                  "AND " & _
                                "(([PromotionType] = 4))" & _
                                  "AND " & _
                                "(([Code] = " & SafeString(ProviderGuid) & ") AND ([SubCode] = '" & subProviderGuid & "'))"
                    dictPromoLine = Sql2Dictionary(strSQL)
                    If Not (dictPromoLine Is Nothing) Then
                        If Not NaV(OrderDict("ShippingAmount")) Then
                            If (dictPromoLine("DiscountAmount") > 0) Then
                                If dictPromoLine("DiscountAmount") > dictPromoLine("MaxDiscountAmount") Then
                                    OrderDict("ShippingAmount") = RoundEx(OrderDict("ShippingAmount") - dictPromoLine("MaxDiscountAmount"), CIntEx(AppSettings("NUMDECIMALPLACES")))
                                Else
                                    OrderDict("ShippingAmount") = RoundEx(OrderDict("ShippingAmount") - dictPromoLine("DiscountAmount"), CIntEx(AppSettings("NUMDECIMALPLACES")))
                                End If
                            ElseIf (dictPromoLine("DiscountPercentage") > 0) Then
                                If (OrderDict("ShippingAmount") * dictPromoLine("DiscountPercentage") / 100) > dictPromoLine("MaxDiscountAmount") Then
                                    OrderDict("ShippingAmount") = RoundEx((OrderDict("ShippingAmount") - dictPromoLine("MaxDiscountAmount")), CIntEx(AppSettings("NUMDECIMALPLACES")))
                                Else
                                    OrderDict("ShippingAmount") = RoundEx((OrderDict("ShippingAmount") - (OrderDict("ShippingAmount") * dictPromoLine("DiscountPercentage") / 100)), CIntEx(AppSettings("NUMDECIMALPLACES")))
                                End If
                            Else
                                OrderDict("ShippingAmount") = RoundEx(0, CIntEx(AppSettings("NUMDECIMALPLACES")))
                            End If
                        End If
                    End If
                End If


                'ElseIf ProviderGuid = "NONE" Then
                '    strSQL = "SELECT * FROM Promotion WHERE " & _
                '                 "PromotionCode = " & SafeString(OrderDict("PromotionCode")) & " " & _
                '                 "AND (([PromotionStartDate] <= " & SafeDatetime(DateValue(Now)) & ") " & _
                '                        "AND " & _
                '                       "([PromotionEndDate] >= " & SafeDatetime(DateValue(Now)) & ")) " & _
                '                 "AND (([NoOfOpportunities] > (SELECT COUNT(*) As Total FROM ShopSalesHeader WHERE " & _
                '                                               "[PromotionCode] = " & SafeString(OrderDict("PromotionCode")) & " " & _
                '                                               "AND (([HeaderDate] >= (SELECT [PromotionStartDate] FROM Promotion WHERE " & _
                '                                                                       "[PromotionCode] = " & SafeString(OrderDict("PromotionCode")) & " " & _
                '                                                                       "AND (([PromotionStartDate] <= " & SafeDatetime(DateValue(Now)) & ") " & _
                '                                                                              "AND " & _
                '                                                                             "([PromotionEndDate] >= " & SafeDatetime(DateValue(Now)) & "))))) " & _
                '                                                        "AND " & _
                '                                                     "([HeaderDate] <= (SELECT [PromotionEndDate] FROM Promotion WHERE " & _
                '                                                                        "[PromotionCode] = " & SafeString(OrderDict("PromotionCode")) & " " & _
                '                                                                        "AND (([PromotionStartDate] <= " & SafeDatetime(DateValue(Now)) & ") " & _
                '                                                                              "AND " & _
                '                                                                             "([PromotionEndDate] >= " & SafeDatetime(DateValue(Now)) & ")))))) " & _
                '                            "OR " & _
                '                        "([NoOfOpportunities] = 0))"
                '    dictPromoHeader = Sql2Dictionary(strSQL)
                '    If Not (dictPromoHeader Is Nothing) Then
                '        strSQL = "SELECT * FROM [PromotionEntries] WHERE " & _
                '                    "[PromotionGuid] = " & dictPromoHeader("PromotionGuid") & " " & _
                '                      "AND " & _
                '                    "(([PromotionType] = 4))" & _
                '                      "AND " & _
                '                    "(([Code] = '') AND ([SubCode] = ''))"
                '        dictPromoLine = Sql2Dictionary(strSQL)
                '        If Not (dictPromoLine Is Nothing) Then
                '            If (dictPromoLine("DiscountAmount") > 0) Then
                '                If dictPromoLine("DiscountAmount") > dictPromoLine("MaxDiscountAmount") And dictPromoLine("MaxDiscountAmount") > 0 Then
                '                    dictPromoLine("DiscountAmount") = dictPromoLine("MaxDiscountAmount")
                '                End If
                '                OrderDict("ShippingAmount") = RoundEx(OrderDict("ShippingAmount") - dictPromoLine("DiscountAmount"), CIntEx(AppSettings("NUMDECIMALPLACES")))
                '            ElseIf (dictPromoLine("DiscountPercentage") > 0) Then
                '                If RoundEx(OrderDict("ShippingAmount") * dictPromoLine("DiscountPercentage") / 100, CIntEx(AppSettings("NUMDECIMALPLACES"))) > dictPromoLine("MaxDiscountAmount") And dictPromoLine("MaxDiscountAmount") > 0 Then
                '                    OrderDict("ShippingAmount") -= dictPromoLine("MaxDiscountAmount")
                '                Else
                '                    OrderDict("ShippingAmount") = RoundEx((OrderDict("ShippingAmount") - (OrderDict("ShippingAmount") * dictPromoLine("DiscountPercentage") / 100)), CIntEx(AppSettings("NUMDECIMALPLACES")))
                '                End If
                '            Else
                '                OrderDict("ShippingAmount") = RoundEx(0, CIntEx(AppSettings("NUMDECIMALPLACES")))
                '            End If
                '        End If
                '    End If
                'End If
            End If
            If CDblEx(OrderDict("ShippingDiscount")) > 0 Then
                OrderDict("ShippingAmount") = RoundEx(OrderDict("ShippingAmount") - OrderDict("ShippingDiscount"), CIntEx(AppSettings("NUMDECIMALPLACES")))
            End If
            If (CDblEx(OrderDict("ShippingAmount")) < 0) Then
                OrderDict("ShippingAmount") = RoundEx(0, CIntEx(AppSettings("NUMDECIMALPLACES")))
            End If
            ' ExpandIT US - MPF - Promotions Code - Finish
        End Sub

        Public Sub NFInvoiceDiscounts(ByVal orderobject As ExpDictionary, ByVal Context As ExpDictionary)
            Dim sql As String
            Dim DiscountPct, ServiceCharge, tmpInvSubTotal, tmpSubTotal As Double
            Dim resDict As ExpDictionary
            Dim item As Object
            Dim SameCurr, DiscountFound, ServiceChargeFound As Boolean

            DiscountPct = 0
            ServiceCharge = 0
            ' Calculation of Service charge and invoice discount.
            ' Invoice discount should be calculated on SubTotal_InvoiceDiscount (Found in Function NFSubTotal)
            ' Service Charge should be calculated on SubTotal - not considering if invoice dicounts are allowed or not.

            ' Read the table
            sql = "SELECT DiscountPct, CurrencyCode, ServiceCharge, MinimumAmount FROM Attain_CustomerInvoiceDiscount WHERE" & _
                  " InvoiceDiscountCode=" & SafeString(orderobject("InvoiceDiscountCode")) & " AND" & _
                  " ((CurrencyCode='') Or (CurrencyCode IS NULL) OR (CurrencyCode = " & SafeString(orderobject("CurrencyGuid")) & "))" & _
                  " ORDER BY CurrencyCode DESC, MinimumAmount DESC"

            resDict = Sql2Dictionaries2(sql, New String() {"CurrencyCode", "MinimumAmount"})

            DiscountPct = 0
            ServiceCharge = 0
            DiscountFound = False
            ServiceChargeFound = False
            SameCurr = False

            ' Try with same currency
            If resDict.ContainsKey(SafeString(orderobject("CurrencyGuid"))) Then
                SameCurr = True
                For Each item In resDict(SafeString(orderobject("CurrencyGuid"))).values

                    If (Not DiscountFound) And (orderobject("SubTotal_InvoiceDiscount") >= item("MinimumAmount")) Then
                        DiscountFound = True
                        DiscountPct = item("DiscountPct")
                    End If

                    If (Not ServiceChargeFound) And (orderobject("SubTotal") >= item("MinimumAmount")) Then
                        ServiceChargeFound = True
                        ServiceCharge = item("ServiceCharge")
                    End If

                    If ServiceChargeFound And DiscountFound Then Exit For
                Next
            End If

            ' Try with no currency.
            If (Not SameCurr) And resDict.ContainsKey(SafeString("")) Then
                For Each item In resDict(SafeString("")).values
                    tmpInvSubTotal = globals.currency.ConvertCurrency(orderobject("SubTotal_InvoiceDiscount"), orderobject("CurrencyGuid"), "")
                    tmpSubTotal = globals.currency.ConvertCurrency(orderobject("SubTotal"), orderobject("CurrencyGuid"), "")

                    If (Not DiscountFound) And (tmpInvSubTotal >= item("MinimumAmount")) Then
                        DiscountFound = True
                        DiscountPct = item("DiscountPct")
                    End If

                    If (Not ServiceChargeFound) And (tmpSubTotal >= item("MinimumAmount")) Then
                        ServiceChargeFound = True
                        ServiceCharge = globals.currency.ConvertCurrency(item("ServiceCharge"), "", orderobject("CurrencyGuid"))
                    End If

                    If ServiceChargeFound And DiscountFound Then Exit For
                Next
            End If

            ' ExpandIT US - MPF - Promotions Code - Start
            Dim strSQL As String = 0
            Dim dictPromoHeader As ExpDictionary
            Dim dictPromoLine As ExpDictionary
            ' ExpandIT US - MPF - Promotions Code - Finish

            ' ExpandIT US - MPF - Promotions Code - Start
            If (AppSettings("EXPANDIT_US_USE_PROMOTION_CODES")) Then
                dictPromoHeader = NFObtainPromotionHeader(orderobject, Context)
                If (dictPromoHeader IsNot Nothing) Then
                    strSQL = "SELECT * FROM [PromotionEntries] WHERE " & _
                                "[PromotionGuid] = " & dictPromoHeader("PromotionGuid") & " " & _
                                  "AND " & _
                                "(([PromotionType] = 5))"
                    dictPromoLine = Sql2Dictionary(strSQL)
                    If (dictPromoLine IsNot Nothing) Then
                        If dictPromoLine("DiscountAmount") > 0 Then
                            If dictPromoLine("DiscountAmount") > dictPromoLine("MaxDiscountAmount") And dictPromoLine("MaxDiscountAmount") > 0 Then
                                dictPromoLine("DiscountAmount") = dictPromoLine("MaxDiscountAmount")
                            End If
                            If (DiscountPct + ((dictPromoLine("DiscountAmount") / orderobject("SubTotal")) * 100)) > 100 Then
                                DiscountPct = 100
                            Else
                                DiscountPct = DiscountPct + ((dictPromoLine("DiscountAmount") / orderobject("SubTotal")) * 100)
                            End If
                        ElseIf dictPromoLine("DiscountPercentage") > 0 Then
                            If (orderobject("SubTotal") * dictPromoLine("DiscountPercentage") / 100) > dictPromoLine("MaxDiscountAmount") And dictPromoLine("MaxDiscountAmount") > 0 Then
                                DiscountPct = DiscountPct + ((dictPromoLine("MaxDiscountAmount") / orderobject("SubTotal")) * 100)
                            Else
                                DiscountPct = DiscountPct + dictPromoLine("DiscountPercentage")
                            End If
                        End If
                        orderobject("PromotionName") = dictPromoHeader("PromotionName")
                        orderobject("PromotionDescription") = dictPromoHeader("PromotionDescription")
                    End If

                    'Look for Product List discounts - START
                    strSQL = "SELECT * FROM [PromotionEntries] WHERE " & _
                                "[PromotionGuid] = " & dictPromoHeader("PromotionGuid") & " " & _
                                  "AND " & _
                                "[PromotionType] = 8 AND [DiscountAmount]>0"
                    Dim dictPromoLines As ExpDictionary = SQL2Dicts(strSQL)
                    If (dictPromoLines IsNot Nothing) Then
                        For Each dictPromoLine In dictPromoLines.Values
                            'Check if all items from the list are on the cart
                            If productList(orderobject, dictPromoLine("Code")) Then
                                If dictPromoLine("DiscountAmount") > dictPromoLine("MaxDiscountAmount") And dictPromoLine("MaxDiscountAmount") > 0 Then
                                    dictPromoLine("DiscountAmount") = dictPromoLine("MaxDiscountAmount")
                                    orderobject("PromotionMet") = True
                                End If
                                If (DiscountPct + ((dictPromoLine("DiscountAmount") / orderobject("SubTotal")) * 100)) > 100 Then
                                    DiscountPct = 100
                                    orderobject("PromotionMet") = True
                                Else
                                    DiscountPct = DiscountPct + ((dictPromoLine("DiscountAmount") / orderobject("SubTotal")) * 100)
                                    orderobject("PromotionMet") = True
                                End If
                            End If
                        Next
                    End If
                    'Look for Product List discounts - END

                    'Look for Promotion Gifts - START
                    strSQL = "SELECT * FROM [PromotionEntries] WHERE " & _
                                "[PromotionGuid] = " & dictPromoHeader("PromotionGuid") & " " & _
                                  "AND " & _
                                "[PromotionType] IN(6,7,9)"

                    dictPromoLines = SQL2Dicts(strSQL)
                    If (dictPromoLines IsNot Nothing) Then
                        Dim orderline As ExpDictionary
                        For Each dictPromoLine In dictPromoLines.Values
                            If dictPromoLine("PromotionType") = 6 Or dictPromoLine("PromotionType") = 7 Then
                                productList(orderobject, dictPromoLine("Code"))
                                Dim amount As Double = 0
                                Dim quantity As Integer = 0
                                For Each key As String In orderobject("Lines").keys
                                    orderline = orderobject("Lines")(key)
                                    If CStrEx(orderline("PromoList")) = dictPromoLine("Code") Then
                                        amount += orderline("LineTotal")
                                        quantity += orderline("Quantity")
                                    End If
                                Next
                                If dictPromoLine("PromotionType") = 6 Then
                                    If CDblEx(amount) >= CDblEx(dictPromoLine("AmountOrQtyRequired")) Then
                                        orderobject("PromotionMet") = True
                                    End If
                                Else
                                    If CIntEx(quantity) >= CIntEx(dictPromoLine("AmountOrQtyRequired")) Then
                                        orderobject("PromotionMet") = True
                                    End If
                                End If
                            ElseIf dictPromoLine("PromotionType") = 9 Then
                                If CDblEx(orderobject("SubTotal")) >= CDblEx(dictPromoLine("AmountOrQtyRequired")) Then
                                    orderobject("PromotionMet") = True
                                End If
                            End If
                        Next
                    End If
                    'Look for Promotion Gifts - END
                End If
            End If
            ' ExpandIT US - MPF - Promotions Code - Finish

            orderobject("InvoiceDiscountPct") = DiscountPct
            orderobject("ServiceCharge") = RoundEx(ServiceCharge, 2)
            orderobject("InvoiceDiscount") = orderobject("SubTotal_InvoiceDiscount") * orderobject("InvoiceDiscountPct") / 100
            orderobject("Total") = orderobject("SubTotal") + orderobject("ServiceCharge") - orderobject("InvoiceDiscount")
        End Sub

        Public Sub NFLineDiscount(ByVal orderobject As ExpDictionary, ByVal Context As ExpDictionary)
            Dim DiscPct As Double
            Dim sql, key, key2 As String
            Dim item, orderline, resultDict As ExpDictionary
            Dim dtResult As DataTable
            Dim currProduct As String
            Dim currGroup As String
            Dim ProductQuantities As ExpDictionary
            Dim GroupCounter As Integer
            Dim VariantTotalQuantity, ProductTotalQuantity, ItemLineDiscountPct, ItemMinimumQuantity As Double
            Dim ProductVariantCode, ItemVariantCode As String
            Dim ProductCounter As Integer
            Dim isCurrencyDiscount As Boolean
            Dim DiscPctC As Double

            ' ExpandIT US - MPF - Promotions Code - Start
            Dim strSQL As String = 0
            Dim dictPromoHeader As ExpDictionary
            Dim dictPromoLine As ExpDictionary
            Dim dictPromoLines As ExpDictionary
            Dim dictOrderLine As ExpDictionary
            ' ExpandIT US - MPF - Promotions Code - Finish

            ProductQuantities = Context("ProductQuantities")

            ' Line Discount
            ' Header conditions: CurrencyCode, StartingDate, EndingDate, SalesType, SalesCode
            ' Line conditions: (Type = 0)ItemNo OR (Type=1)ItemDiscGroup OR (Type=2), MinimumQuantity, VariantCode
            'AM0122201001 - UOM - Start
            If AppSettings("EXPANDIT_US_USE_UOM") Then
                sql = "SELECT Code, SalesType, MinimumQuantity, EndingDate, SalesCode, Type, CurrencyCode AS CurrencyGuid, StartingDate, LineDiscountPct, VariantCode, UnitOfMeasureCode FROM Attain_SalesLineDiscount WHERE " & vbCrLf
            Else
                sql = "SELECT Code, SalesType, MinimumQuantity, EndingDate, SalesCode, Type, CurrencyCode AS CurrencyGuid, StartingDate, LineDiscountPct, VariantCode FROM Attain_SalesLineDiscount WHERE " & vbCrLf
            End If
            'AM0122201001 - UOM - End

            sql &= "(" & vbCrLf & _
           "   (Type = 0 AND Code IN (" & Context("ProductList") & ")) OR " & vbCrLf

            If Context("ItemCustomerDiscountGroupList") <> "" Then
                sql = sql & "   (Type = 1 AND Code IN (" & Context("ItemCustomerDiscountGroupList") & ")) OR " & vbCrLf
            End If

            sql = sql & "   (Type = 2)" & vbCrLf & _
  ")" & vbCrLf & _
  "AND" & vbCrLf & _
       "(" & vbCrLf & _
        "   (SalesType = 0 AND SalesCode " & SafeEqualString(orderobject("CustomerGuid")) & ") OR" & vbCrLf

            If orderobject("CustomerDiscountGroup") <> "" Then
                sql = sql & "   (SalesType = 1 AND SalesCode " & SafeEqualString(orderobject("CustomerDiscountGroup")) & ") OR" & vbCrLf
            End If

            sql = sql & "   (SalesType = 2)" & vbCrLf & _
                   ")" & vbCrLf & _
                   "AND" & vbCrLf & _
                   "(" & vbCrLf & _
                   "   (StartingDate <= " & SafeDatetime(DateValue(Now)) & ") OR" & vbCrLf & _
                   "   (StartingDate IS NULL)" & vbCrLf & _
                   ")" & vbCrLf & _
                   "AND" & vbCrLf & _
                   "(" & vbCrLf & _
                   "   (EndingDate >= " & SafeDatetime(DateValue(Now)) & ") OR" & vbCrLf & _
                   "   (EndingDate IS NULL) " & vbCrLf & _
                   ") AND " & vbCrLf

            sql = sql & "(CurrencyCode " & SafeEqualString(orderobject("CurrencyGuid")) & " OR CurrencyCode IS NULL OR CurrencyCode = '' OR CurrencyCode = " & SafeString(Context("DefaultCurrency")) & ")"

            sql = sql & " ORDER BY Type, Code"

            dtResult = SQL2DataTable(sql)
            resultDict = New ExpDictionary
            resultDict("Product") = New ExpDictionary
            resultDict("Group") = New ExpDictionary

            currProduct = ""
            currGroup = ""
            '            ProdCounter = 0
            GroupCounter = 0

            ' Generate result dictionary
            For Each row As DataRow In dtResult.Rows
                Select Case CStrEx(row("Type"))
                    ' Type = Item
                    Case "0"
                        If currProduct <> CStrEx(row("Code")) Then
                            currProduct = CStrEx(row("Code"))
                            ProductCounter = 1
                            resultDict("Product").Add(currProduct, New ExpDictionary())
                        End If
                        resultDict("Product")(currProduct).Add(CStrEx(ProductCounter), SetDictionary(row))
                        ProductCounter = ProductCounter + 1
                        ' Type = Item Disc. Group
                    Case "1"
                        If currGroup <> CStrEx(row("Code")) Then
                            currGroup = CStrEx(row("Code"))
                            GroupCounter = 1
                            resultDict("Group").Add(currGroup, New ExpDictionary)
                        End If
                        resultDict("Group")(currGroup).Add(CStrEx(GroupCounter), SetDictionary(row))
                        GroupCounter = GroupCounter + 1
                    Case Else
                        ' Ignore
                End Select
            Next

            ' ExpandIT US - MPF - Promotions Code - Start
            If (AppSettings("EXPANDIT_US_USE_PROMOTION_CODES")) Then
                dictPromoHeader = NFObtainPromotionHeader(orderobject, Context)
                If (dictPromoHeader IsNot Nothing) Then
                    strSQL = "SELECT * FROM [PromotionEntries] WHERE " & _
                                "[PromotionGuid] = " & dictPromoHeader("PromotionGuid") & " " & _
                                  "AND " & _
                                "(([PromotionType] <> 0) AND ([PromotionType] <> 4) AND ([PromotionType] <> 5) AND ([PromotionType] <> 6) AND ([PromotionType] <> 7) AND ([PromotionType] <> 9))"
                    dictPromoLines = SQL2Dicts(strSQL)
                End If
            End If
            ' ExpandIT US - MPF - Promotions Code - Finish
            '*****promotions*****-start
            orderobject("PromotionMet") = False
            '*****promotions*****-finish
            For Each key In orderobject("Lines").keys
                orderline = orderobject("Lines")(key)

                DiscPct = 0
                DiscPctC = 0

                isCurrencyDiscount = False

                ' ExpandIT US - MPF - Promotions Code - Start
                orderline("LineDiscountAmount") = 0
                ' ExpandIT US - MPF - Promotions Code - Finish

                ' Search discounts for item
                If Not resultDict("Product")(orderline("ProductGuid")) Is Nothing Then
                    For Each key2 In resultDict("Product")(orderline("ProductGuid")).keys
                        item = resultDict("Product")(orderline("ProductGuid"))(key2)
                        'AM0122201001 - UOM - Start
                        If Not CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) Or CStrEx(item("UnitOfMeasureCode")) = "" Or CStrEx(item("UnitOfMeasureCode")) = CStrEx(orderline("UOM")) Then
                            If CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) And CStrEx(item("UnitOfMeasureCode")) = "" And CStrEx(orderline("UOM")) <> "" Then
                                If AppSettings("COLLECT_QUANTITES_FOR_LINE_DISCOUNTS") Then
                                    ProductTotalQuantity = CDblEx(ProductQuantities(orderline("ProductGuid"))("Total")) * USUOM.GetProductUOMQuantity(orderline("ProductGuid"), orderline("UOM"))
                                Else
                                    ProductTotalQuantity = orderline("Quantity") * USUOM.GetProductUOMQuantity(orderline("ProductGuid"), orderline("UOM"))
                                End If
                            Else
                                If AppSettings("COLLECT_QUANTITES_FOR_LINE_DISCOUNTS") Then
                                    ProductTotalQuantity = CDblEx(ProductQuantities(orderline("ProductGuid"))("Total"))
                                Else
                                    ProductTotalQuantity = orderline("Quantity")
                                End If
                            End If

                            ItemMinimumQuantity = CDblEx(item("MinimumQuantity"))
                            ItemVariantCode = CStrEx(item("VariantCode"))
                            ItemLineDiscountPct = CDblEx(item("LineDiscountPct"))
                            ProductVariantCode = CStrEx(orderline("VariantCode"))

                            isCurrencyDiscount = (CStrEx(item("CurrencyGuid")) = CStrEx(orderobject("CurrencyGuid")) And CStrEx(item("CurrencyGuid")) <> CStrEx(Context("DefaultCurrency")))

                            If ProductVariantCode = "" Then
                                If (ProductTotalQuantity >= ItemMinimumQuantity) And _
                                 (ItemVariantCode = "") _
                                Then
                                    If isCurrencyDiscount Then
                                        If ItemLineDiscountPct > DiscPctC Then
                                            DiscPctC = CDblEx(item("LineDiscountPct"))
                                        End If
                                    Else
                                        If ItemLineDiscountPct > DiscPct Then
                                            DiscPct = CDblEx(item("LineDiscountPct"))
                                        End If
                                    End If
                                End If
                            Else
                                If AppSettings("COLLECT_QUANTITES_FOR_LINE_DISCOUNTS") Then
                                    VariantTotalQuantity = CDblEx(ProductQuantities(orderline("ProductGuid"))("Variants")(CStrEx(orderline("VariantCode"))))
                                Else
                                    VariantTotalQuantity = orderline("Quantity")
                                End If

                                If (((ProductTotalQuantity >= ItemMinimumQuantity) And _
                                 (ItemVariantCode = "")) Or _
                                 ((VariantTotalQuantity >= ItemMinimumQuantity) And _
                                 (ItemVariantCode = ProductVariantCode))) _
                                Then
                                    If isCurrencyDiscount Then
                                        If ItemLineDiscountPct > DiscPctC Then
                                            DiscPctC = CDblEx(item("LineDiscountPct"))
                                        End If
                                    Else
                                        If ItemLineDiscountPct > DiscPct Then
                                            DiscPct = CDblEx(item("LineDiscountPct"))
                                        End If
                                    End If
                                End If
                            End If
                        End If
                        'AM0122201001 - UOM - End
                    Next
                End If

                ' Search discounts for item Grp
                If Not resultDict("Group")(orderline("ItemCustomerDiscountGroup")) Is Nothing Then
                    For Each key2 In resultDict("Group")(orderline("ItemCustomerDiscountGroup")).keys
                        item = resultDict("Group")(orderline("ItemCustomerDiscountGroup"))(key2)

                        If AppSettings("COLLECT_QUANTITES_FOR_LINE_DISCOUNTS") Then
                            ProductTotalQuantity = CDblEx(ProductQuantities(orderline("ProductGuid"))("Total"))
                        Else
                            ProductTotalQuantity = orderline("Quantity")
                        End If

                        ItemMinimumQuantity = CDblEx(item("MinimumQuantity"))
                        ItemVariantCode = CStrEx(item("VariantCode"))
                        ItemLineDiscountPct = CDblEx(item("LineDiscountPct"))
                        ProductVariantCode = CStrEx(orderline("VariantCode"))

                        isCurrencyDiscount = CBool(item("CurrencyGuid") = orderobject("CurrencyGuid") And item("CurrencyGuid") <> Context("DefaultCurrency"))

                        If ProductVariantCode = "" Then
                            If (ProductTotalQuantity >= ItemMinimumQuantity) And _
                                (ItemVariantCode = "") _
                            Then
                                If isCurrencyDiscount Then
                                    If ItemLineDiscountPct > DiscPctC Then
                                        DiscPctC = CDblEx(item("LineDiscountPct"))
                                    End If
                                Else
                                    If ItemLineDiscountPct > DiscPct Then
                                        DiscPct = CDblEx(item("LineDiscountPct"))
                                    End If
                                End If
                            End If
                        Else
                            If AppSettings("COLLECT_QUANTITES_FOR_LINE_DISCOUNTS") Then
                                VariantTotalQuantity = CDblEx(ProductQuantities(orderline("ProductGuid"))("Variants")(CStrEx(orderline("VariantCode"))))
                            Else
                                VariantTotalQuantity = orderline("Quantity")
                            End If

                            If (((ProductTotalQuantity >= ItemMinimumQuantity) And _
                                (ItemVariantCode = "")) Or _
                                ((VariantTotalQuantity >= ItemMinimumQuantity) And _
                                (ItemVariantCode = ProductVariantCode))) _
                            Then
                                If isCurrencyDiscount Then
                                    If ItemLineDiscountPct > DiscPctC Then
                                        DiscPctC = CDblEx(item("LineDiscountPct"))
                                    End If
                                Else
                                    If ItemLineDiscountPct > DiscPct Then
                                        DiscPct = CDblEx(item("LineDiscountPct"))
                                    End If
                                End If
                            End If
                        End If
                    Next
                End If

                ' ExpandIT US - MPF - Promotions Code - Start
                If Not (dictPromoLines Is Nothing) Then
                    For Each dictPromoLine In dictPromoLines.Values
                        Select Case dictPromoLine("PromotionType")
                            Case 1
                                If (orderline("ProductGuid") = dictPromoLine("Code")) Then
                                    orderobject("PromotionMet") = True
                                    If dictPromoLine("DiscountAmount") > 0 Then
                                        If dictPromoLine("DiscountAmount") * orderline("Quantity") > dictPromoLine("MaxDiscountAmount") And dictPromoLine("MaxDiscountAmount") > 0 Then
                                            orderline("LineDiscountAmount") = dictPromoLine("MaxDiscountAmount")
                                        Else
                                            orderline("LineDiscountAmount") = dictPromoLine("DiscountAmount")
                                        End If
                                    ElseIf dictPromoLine("DiscountPercentage") > 0 Then
                                        If (orderline("ListPrice") * orderline("Quantity") * dictPromoLine("DiscountPercentage") / 100) > dictPromoLine("MaxDiscountAmount") And dictPromoLine("MaxDiscountAmount") > 0 Then
                                            orderline("LineDiscountAmount") = dictPromoLine("MaxDiscountAmount")
                                        Else
                                            DiscPct = CDblEx(DiscPct + dictPromoLine("DiscountPercentage"))
                                        End If
                                    End If
                                End If
                            Case 2
                                If (orderline("ProductGuid") = dictPromoLine("Code")) Then
                                    For Each dictOrderLine In orderobject("Lines").Values
                                        If (dictOrderLine("ProductGuid") = dictPromoLine("LinkedCode")) Then
                                            orderobject("PromotionMet") = True
                                            If dictPromoLine("DiscountAmount") > 0 Then
                                                If dictPromoLine("DiscountAmount") * orderline("Quantity") > dictPromoLine("MaxDiscountAmount") And dictPromoLine("MaxDiscountAmount") > 0 Then
                                                    orderline("LineDiscountAmount") = dictPromoLine("MaxDiscountAmount")
                                                Else
                                                    orderline("LineDiscountAmount") = dictPromoLine("DiscountAmount")
                                                End If
                                            ElseIf dictPromoLine("DiscountPercentage") > 0 Then
                                                If (orderline("ListPrice") * orderline("Quantity") * dictPromoLine("DiscountPercentage") / 100) > dictPromoLine("MaxDiscountAmount") And dictPromoLine("MaxDiscountAmount") > 0 Then
                                                    orderline("LineDiscountAmount") = dictPromoLine("MaxDiscountAmount")
                                                Else
                                                    DiscPct = CDblEx(DiscPct + dictPromoLine("DiscountPercentage"))
                                                End If
                                            End If
                                            Exit For
                                        End If
                                    Next
                                End If
                            Case 3
                                If (orderline("ProductGuid") = dictPromoLine("LinkedCode")) Then
                                    If (CStrEx(orderobject("ShippingHandlingProviderGuid")) = CStrEx(dictPromoLine("Code")) And CStrEx(orderobject("ServiceCode")) = CStrEx(dictPromoLine("SubCode"))) Then
                                        orderobject("PromotionMet") = True
                                        If dictPromoLine("DiscountAmount") > 0 Then
                                            If dictPromoLine("DiscountAmount") * orderline("Quantity") > dictPromoLine("MaxDiscountAmount") And dictPromoLine("MaxDiscountAmount") > 0 Then
                                                orderobject("ShippingDiscount") = dictPromoLine("MaxDiscountAmount")
                                            Else
                                                orderobject("ShippingDiscount") = dictPromoLine("DiscountAmount")
                                            End If
                                        ElseIf dictPromoLine("DiscountPercentage") > 0 Then
                                            If (orderobject("ListPrice") * orderline("Quantity") * dictPromoLine("DiscountPercentage") / 100) > dictPromoLine("MaxDiscountAmount") And dictPromoLine("MaxDiscountAmount") > 0 Then
                                                orderobject("ShippingDiscount") = dictPromoLine("MaxDiscountAmount")
                                            Else
                                                orderobject("ShippingDiscount") = CDblEx(dictPromoLine("DiscountPercentage") / 100)
                                            End If
                                        End If
                                        If Not NaV(orderobject("ShippingAmount")) Then
                                            If orderobject("ShippingDiscount") > 0 Then
                                                If orderobject("ShippingDiscount") > orderobject("ShippingAmount") Then
                                                    orderobject("ShippingAmount") = 0
                                                Else
                                                    orderobject("ShippingAmount") = RoundEx(orderobject("ShippingAmount") - orderobject("ShippingDiscount"), CIntEx(AppSettings("NUMDECIMALPLACES")))
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                        End Select
                    Next

                    orderobject("PromotionName") = dictPromoHeader("PromotionName")
                    orderobject("PromotionDescription") = dictPromoHeader("PromotionDescription")
                End If
                ' ExpandIT US - MPF - Promotions Code - Finish

                If DiscPctC > 0 Then
                    orderline("LineDiscountPct") = DiscPctC
                Else
                    orderline("LineDiscountPct") = DiscPct
                End If
            Next

            'Look for Product List discounts
            Dim DiscountAmount As Double
            Dim DiscountPct As Double

            If Not (dictPromoLines Is Nothing) Then
                For Each dictPromoLine In dictPromoLines.Values
                    DiscountAmount = 0
                    DiscountPct = 0
                    If dictPromoLine("PromotionType") = 8 Then
                        'Check if the discount is a percentage to apply it line by line
                        If dictPromoLine("DiscountPercentage") > 0 Then
                            'Check if all items from the list are on the cart
                            If productList(orderobject, dictPromoLine("Code")) Then
                                orderobject("PromotionMet") = True
                                DiscountPct = dictPromoLine("DiscountPercentage")
                                For Each key In orderobject("Lines").keys
                                    orderline = orderobject("Lines")(key)
                                    If CStrEx(orderline("PromoList")) = dictPromoLine("Code") Then
                                        If (orderline("ListPrice") * DiscountPct / 100) > dictPromoLine("MaxDiscountAmount") And dictPromoLine("MaxDiscountAmount") > 0 Then
                                            orderline("LineDiscountAmount") = CDblEx(orderline("LineDiscountAmount")) + dictPromoLine("MaxDiscountAmount")
                                        Else
                                            DiscountPct = CDblEx(DiscountPct + CDblEx(orderline("LineDiscountPct")))
                                            If DiscountPct > 100 Then
                                                DiscountPct = 100
                                            End If
                                            orderline("LineDiscountPct") = DiscountPct
                                        End If
                                    End If
                                Next
                            End If
                        End If
                    End If
                Next
            End If
        End Sub

        Public Function NFObtainPromotionHeader(ByVal orderobject As ExpDictionary, ByVal Context As ExpDictionary) As ExpDictionary

            Dim strSQL As String
            Dim dictPromoHeader As ExpDictionary

            If Not IsNull(orderobject("PromotionCode")) Then
                strSQL = "SELECT * FROM Promotion WHERE " & _
                         "PromotionCode = " & SafeString(orderobject("PromotionCode")) & " " & _
                         "AND (([PromotionStartDate] <= " & SafeDatetime(DateValue(Now)) & ") " & _
                         "AND " & _
                         "([PromotionEndDate] >= " & SafeDatetime(DateValue(Now)) & ")) " & _
                         "AND (([NoOfOpportunities] > (SELECT COUNT(*) As Total FROM ShopSalesHeader WHERE " & _
                         "[PromotionCode] = " & SafeString(orderobject("PromotionCode")) & " " & _
                         "AND (([HeaderDate] >= (SELECT [PromotionStartDate] FROM Promotion WHERE " & _
                         "[PromotionCode] = " & SafeString(orderobject("PromotionCode")) & " " & _
                         "AND (([PromotionStartDate] <= " & SafeDatetime(DateValue(Now)) & ") " & _
                         "AND " & _
                         "([PromotionEndDate] >= " & SafeDatetime(DateValue(Now)) & "))))) " & _
                         "AND " & _
                         "([HeaderDate] <= (SELECT [PromotionEndDate] FROM Promotion WHERE " & _
                         "[PromotionCode] = " & SafeString(orderobject("PromotionCode")) & " " & _
                         "AND (([PromotionStartDate] <= " & SafeDatetime(DateValue(Now)) & ") " & _
                         "AND " & _
                         "([PromotionEndDate] >= " & SafeDatetime(DateValue(Now)) & ")))))) " & _
                         "OR " & _
                         "([NoOfOpportunities] = 0))"
                dictPromoHeader = Sql2Dictionary(strSQL)
                If Not dictPromoHeader Is Nothing Then
                    Dim properties As New ExpDictionary
                    strSQL = "SELECT PropOwnerRefGuid, PropGuid, PropLangGuid, PropTransText FROM PropVal INNER JOIN PropTrans" & _
                           " ON PropVal.PropValGuid=PropTrans.PropValGuid" & _
                           " WHERE PropOwnerTypeGuid='PRM'" & _
                           " AND (PropLangGuid IS NULL OR PropLangGuid =" & SafeString(HttpContext.Current.Session("LanguageGuid").ToString) & ")" & _
                           " AND (PropGuid='PROMO_DESCRIPTION' OR PropGuid='PROMO_NAME')" & _
                           " AND PropOwnerRefGuid=" & SafeString(dictPromoHeader("PromotionGuid"))
                    properties = SQL2Dicts(strSQL)
                    If Not properties Is Nothing Then
                        If properties.Count > 0 Then
                            For Each prop As ExpDictionary In properties.Values
                                If prop("PropGuid") = "PROMO_DESCRIPTION" Then
                                    prop("PropGuid") = "PromotionDescription"
                                ElseIf prop("PropGuid") = "PROMO_NAME" Then
                                    prop("PropGuid") = "PromotionName"
                                End If
                                dictPromoHeader(prop("PropGuid")) = prop("PropTransText")
                            Next
                        End If
                    End If
                End If
                Return dictPromoHeader
            End If
            Return Nothing
        End Function

        Public Function productList(ByVal orderDict As ExpDictionary, ByVal itemListCode As String)

            Dim sql As String
            Dim promoItemsDict As ExpDictionary
            Dim orderline As ExpDictionary
            Dim itemInList As Boolean = False
            Dim returnValue As Boolean = True

            sql = "SELECT ItemNo FROM PromotionItemEntry WHERE PromotionItemListCode=" & SafeString(itemListCode)
            promoItemsDict = SQL2Dicts(sql, "ItemNo")

            If promoItemsDict IsNot Nothing Then
                For Each item As ExpDictionary In promoItemsDict.Values
                    For Each key As String In orderDict("Lines").keys
                        If orderDict("Lines")(key)("ProductGuid") = item("ItemNo") Then
                            orderDict("Lines")(key)("PromoList") = itemListCode
                            itemInList = True
                            Exit For
                        Else
                            itemInList = False
                        End If
                    Next
                    If Not itemInList Then
                        returnValue = False
                    End If
                Next
            Else
                Return False
            End If

            Return returnValue

        End Function

    End Class
End Namespace