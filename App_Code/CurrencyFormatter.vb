Option Strict On
Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports System.Data
Imports System.Globalization
Imports System.Text
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT31.ExpanditFramework.Infrastructure

Namespace ExpandIT
    Public Class CurrencyFormatter

        Private Shared _currencyCultureInfo As Dictionary(Of String, CultureInfo)
        Private Shared isCacheUsed As Boolean

        Shared Sub New()
            ' Check if caching is enabled on CultureTable
            If CacheManager.CacheEnabled("CultureTable") Then
                isCacheUsed = True
            End If
            Init()
        End Sub

        Private Shared Sub Init()

            _currencyCultureInfo = New Dictionary(Of String, CultureInfo)

            ' get the list of cultures. We are not interested in neutral cultures, since
            ' currency and RegionInfo is only applicable to specific cultures
            Dim _cultures As CultureInfo() = CultureInfo.GetCultures(CultureTypes.SpecificCultures)

            ' get the user defined ISO/Culture settings from DB
            Dim dt As DataTable = SQL2DataTable("SELECT ISO, Culture, CurrencyDecimalDigits, CurrencyDecimalSeparator, " & _
                "CurrencyGroupSeparator, CurrencyNegativePattern, CurrencySymbol FROM CultureTable")

            If dt.Rows.Count > 0 Then
                For Each row As DataRow In dt.Rows
                    ' Adapt Culture with data from DB
                    Dim ci As CultureInfo = AdaptCulture(row)
                    ' Add ISO code and culture to dictionary
                    If ci IsNot Nothing Then
                        _currencyCultureInfo.Add(CType(row("ISO"), String), ci)
                     End If
                Next
            End If

            For Each ci As CultureInfo In _cultures
                ' Create a RegionInfo from culture id. 
                ' RegionInfo holds the currency ISO code
                Dim ri As RegionInfo = New RegionInfo(ci.LCID)
                ' multiple cultures can have the same currency code
                If Not _currencyCultureInfo.ContainsKey(ri.ISOCurrencySymbol) Then
                    _currencyCultureInfo.Add(ri.ISOCurrencySymbol, ci)
                End If
            Next

            ' If caching is enabled then use it to force update in case of changes to the DB-Table
            If isCacheUsed Then
                ' Try to insert cultureInfo into cache
                isCacheUsed = CacheManager.InsertIntoCache("CultureTable", _currencyCultureInfo)
            End If
        End Sub

        ''' <summary>
        ''' Lookup CultureInfo by currency ISO code
        ''' </summary>
        ''' <param name="isoCode"></param>
        ''' <returns></returns>
        Private Shared Function CultureInfoFromCurrencyISO(ByVal isoCode As String) As CultureInfo
            If (_currencyCultureInfo.ContainsKey(isoCode)) Then
                Return _currencyCultureInfo(isoCode)
            Else
                Return Nothing
            End If
        End Function

        ''' <summary>
        ''' Adapt Culture with info from DB
        ''' </summary>
        ''' <param name="dr"></param>
        ''' <returns></returns>
        Private Shared Function AdaptCulture(ByVal dr As DataRow) As CultureInfo
            Try
                Dim c As CultureInfo = New CultureInfo(CType(dr("Culture"), String), True)
                If (Not dr("CurrencyDecimalDigits").Equals(DBNull.Value)) Then
                    c.NumberFormat.CurrencyDecimalDigits = CType(dr("CurrencyDecimalDigits"), Integer)
                End If
                If Not dr("CurrencyDecimalSeparator").Equals(DBNull.Value) Then
                    If Not String.IsNullOrEmpty(CType(dr("CurrencyDecimalSeparator"), String)) Then
                        c.NumberFormat.CurrencyDecimalSeparator = CType(dr("CurrencyDecimalSeparator"), String)
                    End If
                End If
                If (Not dr("CurrencyGroupSeparator").Equals(DBNull.Value)) Then
                    c.NumberFormat.CurrencyGroupSeparator = CType(dr("CurrencyGroupSeparator"), String)
                End If
                If (Not dr("CurrencySymbol").Equals(DBNull.Value)) Then
                    c.NumberFormat.CurrencySymbol = CType(dr("CurrencySymbol"), String)
                End If
                If (Not dr("CurrencyNegativePattern").Equals(DBNull.Value)) Then
                    c.NumberFormat.CurrencyNegativePattern = CType(dr("CurrencyNegativePattern"), Integer)
                End If
                Return c
            Catch ex As Exception
                Return Nothing
            End Try
        End Function

        Public Shared Function CurrencyISO2CultureInfo(ByVal currencyISO As String) As CultureInfo
            Return CultureInfoFromCurrencyISO(currencyISO)
        End Function

        ''' <summary>
        ''' Convert currency to a string using the specified currency format
        ''' </summary>
        ''' <param name="amount"></param>
        ''' <param name="currencyISO"></param>
        ''' <returns></returns>
        Public Shared Function FormatCurrency(ByVal amount As Decimal, ByVal currencyISO As String) As String
            ' check if caching is enabled
            If isCacheUsed Then
                ' if so, and db has changed, cache = nothing
                If HttpContext.Current.Cache("CultureTable") Is Nothing Then
                    Init()
                End If
            End If

            Dim c As CultureInfo = CultureInfoFromCurrencyISO(currencyISO)
            If c Is Nothing Then
                ' if currency ISO code doesn't match any culture
                ' create a new culture without currency symbol
                ' and use the ISO code as a prefix (e.g. YEN 123,123.00)
                c = CultureInfo.CreateSpecificCulture("en-US")
                c.NumberFormat.CurrencySymbol = ""
                c.NumberFormat.CurrencyDecimalDigits = 2
                c.NumberFormat.CurrencyDecimalSeparator = "."
                c.NumberFormat.CurrencyGroupSeparator = ","

                Return String.Format("{0} {1}", currencyISO, amount.ToString("C", c.NumberFormat))
            Else
                Dim tstr As String = RoundEx(amount, 2).ToString()
                Return CDblEx(tstr).ToString("C", c.NumberFormat)
            End If
        End Function

        ''' <summary>
        ''' Overloaded FormatCurrency
        ''' </summary>
        ''' <param name="amount"></param>
        ''' <param name="currencyISO"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function FormatCurrency(ByVal amount As String, ByVal currencyISO As String) As String
            If String.IsNullOrEmpty(amount) Then
                Return FormatCurrency(0, currencyISO)
            End If
            Dim d As Decimal = 0
            Try
                d = Decimal.Parse(amount.Replace(" ", ""))
            Catch ex As System.FormatException

            End Try
            Return FormatCurrency(d, currencyISO)
        End Function

        ''' <summary>
        ''' Overloaded FormatCurrency
        ''' </summary>
        ''' <param name="amount"></param>
        ''' <param name="currencyISO"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function FormatCurrency(ByVal amount As DBNull, ByVal currencyISO As String) As String
            Return FormatCurrency(0, currencyISO)
        End Function

        ''' <summary>
        ''' Overloaded FormatCurrency
        ''' </summary>
        ''' <param name="amount"></param>
        ''' <param name="currencyISO"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function FormatCurrency(ByVal amount As DBNull, ByVal currencyISO As DBNull) As String
            Return FormatCurrency(0, "NOC")
        End Function

        ''' <summary>
        ''' Overloaded FormatCurrency
        ''' </summary>
        ''' <param name="amount"></param>
        ''' <param name="currencyISO"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function FormatCurrency(ByVal amount As Double, ByVal currencyISO As String) As String
            Dim str As String = amount.ToString("R")
            Dim dcml As Decimal = Decimal.Parse(str)
            Return FormatCurrency(dcml, currencyISO)
        End Function

        ''' <summary>
        ''' Overloaded FormatCurrency
        ''' </summary>
        ''' <param name="amount"></param>
        ''' <param name="currencyISO"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function FormatCurrency(ByVal amount As Double, ByVal currencyISO As DBNull) As String
            Dim str As String = amount.ToString("R")
            Dim dcml As Decimal = Decimal.Parse(str)
            Return FormatCurrency(dcml, "NOC")
        End Function

        Public Shared Function FormatAmount(ByVal amount As String, ByVal InputCurrency As String) As String
            Return FormatCurrency(amount, InputCurrency)
        End Function

        Public Shared Function FormatAmount(ByVal amount As DBNull, ByVal InputCurrency As String) As String
            Return FormatCurrency(amount, InputCurrency)
        End Function

        Public Shared Function FormatAmount(ByVal amount As DBNull, ByVal InputCurrency As DBNull) As String
            Return FormatCurrency(amount, InputCurrency)
        End Function


        Public Shared Function GetCurrentSymbol(ByVal currencyISO As String) As String
            Dim c As CultureInfo = CultureInfoFromCurrencyISO(currencyISO)
            If Not c Is Nothing Then
                Return c.NumberFormat.CurrencySymbol
            Else
                Return ""
            End If
        End Function

    End Class
End Namespace