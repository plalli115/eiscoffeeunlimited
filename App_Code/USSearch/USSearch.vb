﻿Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports ExpandIT
Imports ExpandIT.ExpandITLib
Imports ExpandIT.EISClass
Imports System.Data
Imports System.Configuration.ConfigurationManager

Namespace ExpandIT

    Public Class SearchDataTable
        Inherits DataTable


        Public Sub New()
            Columns.Add("ProductLinkURL")
            Columns.Add("Guid")
            Columns.Add("Name")
            Columns.Add("Description")
            Columns.Add("ThumbnailURL")
            Columns.Add("ThumbnailText")
            Columns.Add("Price", GetType(System.Decimal))
            Columns.Add("PriceFormatted")
            'AM2010051801 - SPECIAL ITEMS - Start
            'AM2010051802 - WHAT's NEW - Start
            Columns.Add("LastName")
            'AM2010051802 - WHAT's NEW - End
            'AM2010051801 - SPECIAL ITEMS - End
        End Sub
    End Class

    Public Class USSearch


        Public Shared Function SearchString() As String
            Dim retv As String = ""
            Dim searchstring1 As String = HttpContext.Current.Request.QueryString("searchstring")
            Dim searchstring2 As String = HttpContext.Current.Request.Form("searchstring")
            Dim searchstring3 As String = HttpContext.Current.Request.Form("mainsearchstring")
            Dim searchText As String = Resources.Language.LABEL_SEARCH.ToString

            If Not searchstring1 Is DBNull.Value And Not searchstring1 Is Nothing Then
                If searchstring1.ToUpper = searchText.ToUpper Then
                    searchstring1 = ""
                End If
            End If
            If Not searchstring2 Is DBNull.Value And Not searchstring2 Is Nothing Then
                If searchstring2.ToUpper = searchText.ToUpper Then
                    searchstring2 = ""
                End If
            End If
            If Not searchstring3 Is DBNull.Value And Not searchstring3 Is Nothing Then
                If searchstring3.ToUpper = searchText.ToUpper Then
                    searchstring3 = ""
                End If
            End If

            If searchstring1 <> "" Then
                retv = searchstring1
            ElseIf searchstring2 <> "" Then
                retv = searchstring2
            ElseIf searchstring3 <> "" Then
                retv = searchstring3
            End If

            Return retv
        End Function

        Public Function GetResults(ByVal searchstring1 As String, ByVal searchstring2 As String, ByVal searchstring3 As String) As SearchDataTable
            Dim retv As New SearchDataTable
            Dim searchstring As String = ""
            Dim searchText As String = Resources.Language.LABEL_SEARCH.ToString

            If Not searchstring1 Is DBNull.Value And Not searchstring1 Is Nothing Then
                If searchstring1.ToUpper = searchText.ToUpper Then
                    searchstring1 = ""
                End If
            End If
            If Not searchstring2 Is DBNull.Value And Not searchstring2 Is Nothing Then
                If searchstring2.ToUpper = searchText.ToUpper Then
                    searchstring2 = ""
                End If
            End If
            If Not searchstring3 Is DBNull.Value And Not searchstring3 Is Nothing Then
                If searchstring3.ToUpper = searchText.ToUpper Then
                    searchstring3 = ""
                End If
            End If

            If searchstring1 <> "" Then
                searchstring = searchstring1
            ElseIf searchstring2 <> "" Then
                searchstring = searchstring2
            ElseIf searchstring3 <> "" Then
                searchstring = searchstring3
            End If

            If searchstring <> "" Then
                'AM2010070201 - ENTERPRISE SEARCH - Start
                Dim searchstrings() As String
                'Make sure the searchstring doesn't contain double spaces
                While searchstring.Contains("  ")
                    searchstring = searchstring.Replace("  ", " ")
                End While
                'Get all the different words that compose the searchstring
                searchstrings = searchstring.Split(" ")
                Dim sql As String = SearchSQL(searchstring)
                Dim productsDicts As New ExpDictionary
                Dim productsSingleWords As New ExpDictionary
                Dim singleSearchString As String = ""
                Dim productDict As ExpDictionary
                Dim occurrences As Integer = 0
                Dim stringToCheck As String = ""
                Dim products As New ExpDictionary

                'Get a dictionary with the products that match the searchstring
                productsDicts(searchstring) = getProductsDict(sql)
                If searchstrings.Length > 1 Then
                    'Create a dictionary per word from the searchstring, with the products that match each word
                    For Each singleSearchString In searchstrings
                        sql = SearchSQL(singleSearchString)
                        productsDicts(singleSearchString) = getProductsDict(sql)
                    Next
                End If
                'Loop through the dictionaries with the matching products
                'Count the number of occurrences for each search string
                For Each singleSearchString In productsDicts.Keys
                    productDict = productsDicts(singleSearchString)
                    If Not productDict Is Nothing Then
                        If productDict.Count > 0 Then
                            For Each productDictKey As String In productDict.Keys
                                occurrences = 0
                                'Check the ProductGuid
                                stringToCheck = productDict(productDictKey)("ProductGuid")
                                occurrences += getOccurrences(singleSearchString, CStrEx(stringToCheck))
                                'Check the ProductName
                                stringToCheck = productDict(productDictKey)("ProductName")
                                occurrences += getOccurrences(singleSearchString, CStrEx(stringToCheck))
                                'Check the DESCRIPTION
                                stringToCheck = productDict(productDictKey)("DESCRIPTION")
                                occurrences += getOccurrences(singleSearchString, CStrEx(stringToCheck))
                                If searchstring = singleSearchString Then
                                    productDict(productDictKey)("Occurrences") = -occurrences * 1000
                                Else
                                    productDict(productDictKey)("Occurrences") = -occurrences
                                End If
                            Next
                        End If
                    End If
                Next
                'Merge the dictionaries with the single occurrences into one
                If searchstrings.Length > 1 Then
                    For Each singleSearchString In productsDicts.Keys
                        'Exit if the dictionary is the one for full matches, not single words
                        If singleSearchString = searchstring Then
                        Else
                            productDict = productsDicts(singleSearchString)
                            If Not productDict Is Nothing Then
                                If productDict.Count > 0 Then
                                    'Move the first dictionary with single words to the productsSingleWords dictionary
                                    If productsSingleWords.Count <= 0 Then
                                        productsSingleWords = productDict
                                    Else
                                        'Check if any of the products on the given ProductDict is also on the 
                                        'productsSingleWords Dictionary and add the number of occurrences
                                        For Each productsSingleWordsKey As String In productsSingleWords.Keys
                                            For Each productDictKey As String In productDict.Keys
                                                If productsSingleWordsKey = productDictKey Then
                                                    productsSingleWords(productsSingleWordsKey)("Occurrences") += productDict(productDictKey)("Occurrences")
                                                    Exit For
                                                End If
                                            Next
                                        Next
                                        'Add to the productsSingleWords Dictionary any product from the productDict
                                        'that is not already there
                                        For Each productDictKey As String In productDict.Keys
                                            If Not productsSingleWords.ContainsKey(productDictKey) Then
                                                productsSingleWords(productDictKey) = productDict(productDictKey)
                                            End If
                                        Next
                                    End If
                                End If
                            End If
                        End If
                    Next
                    'Sort the productsSingleWords Dictionary by number of occurrences
                    SortDictionaryEx(productsSingleWords, "Occurrences")
                End If
                'Add the products that match the full searchstring, which were already ordered by number of occurrences
                products = productsDicts(searchstring)
                If productsSingleWords.Count > 0 Then
                    'Check if any of the products that match the full searchstring matches any single words and add
                    'the number of single occurrences
                    For Each productsKey As String In products.Keys
                        For Each productsSingleWordsKey As String In productsSingleWords.Keys
                            If productsKey = productsSingleWordsKey Then
                                products(productsKey)("Occurrences") += productsSingleWords(productsSingleWordsKey)("Occurrences")
                                Exit For
                            End If
                        Next
                    Next
                    'Add to the products Dictionary any product from the productsSingleWords Dictionary
                    'that is not already there
                    For Each productsSingleWordsKey As String In productsSingleWords.Keys
                        If Not products.ContainsKey(productsSingleWordsKey) Then
                            productsSingleWords(productsSingleWordsKey)("Occurrences") += productsSingleWords(productsSingleWordsKey)("Occurrences")
                            products.Add(productsSingleWordsKey, productsSingleWords(productsSingleWordsKey))
                        End If
                    Next
                End If
                SortDictionaryEx(products, "Occurrences")

                Dim countProducts As Integer = 0

                For Each d As ExpDictionary In products.Values
                    If countProducts = CIntEx(AppSettings("SEARCH_MAX_RESULTS")) Then
                        Exit For
                    End If
                    'AM2010070201 - ENTERPRISE SEARCH - End
                    Dim p As ProductClass = New ProductClass(d)

                    Dim style As String = p.ThumbnailURLStyle(100, 100)
                    Dim thumbnail As String = "<img style=""" & style & """ border=""0"" src=""" & p.ThumbnailURL & """ alt="""" />"

                    Dim descriptionBkp As String = ""
                    If CStrEx(p.Description).Length > 100 Then
                        descriptionBkp = CStrEx(p.Description).Substring(0, 99) & "<ins class=""moreText"">...more</ins>"
                    Else
                        descriptionBkp = CStrEx(p.Description)
                    End If

                    'AM2010051801 - SPECIAL ITEMS - Start
                    Dim colorPrice As String = ""
                    If p.IsSpecial Then
                        colorPrice = "<a class=""SpecialPrice"">" & p.PriceFormatted & "</a>"
                    Else
                        colorPrice = "<a class=""RegularPrice"">" & p.PriceFormatted & "</a>"
                    End If
                    'AM2010070201 - ENTERPRISE SEARCH - Start
                    Dim hiliteName As String = HiLite(p.Name, searchstring, True)
                    Dim hiliteGuid As String = HiLite(p.Guid, searchstring, True)
                    Dim hiliteDescriptionBkp As String = HiLite(descriptionBkp, searchstring, True)

                    For Each singleSearchString In searchstrings
                        hiliteName = HiLite(hiliteName, singleSearchString, True)
                        hiliteGuid = HiLite(hiliteGuid, singleSearchString, True)
                        hiliteDescriptionBkp = HiLite(hiliteDescriptionBkp, singleSearchString, True)
                    Next


                    'AM2010051802 - WHAT's NEW - Start
                    retv.Rows.Add(New Object() {p.ProductLinkURL, hiliteGuid, hiliteName, FormatText(hiliteDescriptionBkp), thumbnail, p.ThumbnailText, p.Price, colorPrice, p.LastName})
                    'AM2010051802 - WHAT's NEW - End
                    'AM2010051801 - SPECIAL ITEMS - End
                    countProducts += 1
                    'AM2010070201 - ENTERPRISE SEARCH - End
                Next

            End If
            Return retv
        End Function


        Public Shared Function SearchSQL(ByVal searchstring As String) As String
            Dim pt As String
            Dim ptt As String
            Dim sql As String
            Dim include_productguid As Boolean = True
            Dim include_productname As Boolean = True
            Dim include_productdescription As Boolean = True
            'AM2011061701 - ITEM LINKING - START
            Dim include_variantguid As Boolean = True
            Dim include_variantdescription As Boolean = True
            'AM2011061701 - ITEM LINKING - END
            'AM2010070201 - ENTERPRISE SEARCH - Start
            Dim dbmaxresult As Long = AppSettings("SEARCH_MAX_RESULTS")
            'AM2010070201 - ENTERPRISE SEARCH - End
            Dim dbsearchstring As String = USSearch.PrepareSearchString(searchstring)
            Dim collation As String = AppSettings("SEARCH_COLLATION")
            Dim pageobj As ExpandIT.Page = CType(System.Web.HttpContext.Current.Handler, ExpandIT.Page)
            Dim objCSC As New csc(pageobj.globals)

            'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - End
            If collation = "" Then collation = "Latin1_General_CI_AI"

            ' Initialize to an empty result
            sql = ""
            Dim s As String = HttpContext.Current.Session("groupValue")
            If AppSettings("REQUIRE_CATALOG_ENTRY") Then
                'AM2010071901 - ENTERPRISE CSC - Start
                If s Is Nothing OrElse s = SafeString(0) Then
                    pt = "(SELECT ProductTable.ProductGuid, ProductTable.ProductName" & _
                    " FROM ProductTable INNER JOIN (SELECT DISTINCT GroupProduct.ProductGuid FROM GroupProduct WHERE GroupProduct.CatalogNo=" & SafeString(pageobj.globals.User("CatalogNo")) & " ) AS GP" & _
                    " ON ProductTable.ProductGuid=GP.ProductGuid) AS PT"
                Else
                    pt = "(SELECT ProductTable.ProductGuid, ProductTable.ProductName" & _
                    " FROM ProductTable INNER JOIN (SELECT DISTINCT GroupProduct.ProductGuid FROM GroupProduct WHERE GroupProduct.GroupGuid IN(" & s & ") AND GroupProduct.CatalogNo=" & SafeString(pageobj.globals.User("CatalogNo")) & ") AS GP" & _
                    " ON ProductTable.ProductGuid=GP.ProductGuid) AS PT"
                End If
                'AM2010071901 - ENTERPRISE CSC - End

            Else
                pt = "ProductTable AS PT"
            End If

            ptt = "SELECT * FROM ProductTranslation WHERE " & _
                HttpContext.Current.Application("EXTERNAL_LANGUAGEGUID_SQL") & "=" & _
                SafeString(HttpContext.Current.Session("ExternalLanguageGuid").ToString)

            'AM2011061701 - ITEM LINKING - START
            'If CBoolEx(AppSettings("USE_PRODUCTVARIANTTRANSLATION")) Then
            '    ptt = ptt & " AND (VariantCode IS NULL OR VariantCode='')"
            'End If
            'AM2011061701 - ITEM LINKING - END

            ' Search for ProductGuid and ProductName in ProductTable and ProductTranslation
            If include_productguid Or include_productname Then

                ' Build search SQL based on the use of the ProductTranslation table
                If CBoolEx(AppSettings("USE_PRODUCTTRANSLATION")) Then
                    sql = "SELECT TOP " & dbmaxresult & " PT.ProductGuid FROM " & pt & _
                        " LEFT JOIN (" & ptt
                    'AM2011061701 - ITEM LINKING - START
                    If CBoolEx(AppSettings("USE_PRODUCTVARIANTTRANSLATION")) Then
                        sql = sql & " AND (VariantCode IS NULL OR VariantCode='')"
                    End If
                    'AM2011061701 - ITEM LINKING - END
                    sql &= ") AS PTT" & _
                        " ON PT.ProductGuid=PTT.ProductGuid" & _
                        " WHERE ("
                    If include_productguid Then sql = sql & " (PT.ProductGuid COLLATE " & collation & " LIKE " & SafeString(dbsearchstring) & ")"
                    If include_productname Then
                        If include_productguid Then sql = sql & " OR "
                        sql = sql & _
                            " (PT.ProductName COLLATE " & collation & " LIKE " & SafeString(dbsearchstring) & " AND PTT.ProductGuid IS NULL) OR" & _
                            " (PTT.ProductName COLLATE " & collation & " LIKE " & SafeString(dbsearchstring) & ")"
                    End If
                    sql = sql & ")"
                Else
                    sql = "SELECT TOP " & dbmaxresult & " PT.ProductGuid FROM " & pt & " WHERE"
                    If include_productguid Then sql = sql & " (PT.ProductGuid COLLATE " & collation & " LIKE " & SafeString(dbsearchstring) & ")"
                    If include_productname Then
                        If include_productguid Then sql = sql & " OR "
                        sql = sql & _
                            " (PT.ProductName COLLATE " & collation & " LIKE " & SafeString(dbsearchstring) & ")"
                    End If
                End If

            End If
            'AM2011061701 - ITEM LINKING - START
            ' Search for VariantGuid and VariantDescription in ProductVariant and ProductTranslation
            If include_variantguid Or include_variantdescription Then
                Dim variantsSQL As String = ""
                ' Build search SQL based on the use of the ProductTranslation table
                If CBoolEx(AppSettings("USE_PRODUCTVARIANTTRANSLATION")) Then
                    variantsSQL = "SELECT TOP " & dbmaxresult & " PT.ProductGuid FROM " & pt & " INNER JOIN ProductVariant PV" & _
                        " ON PT.ProductGuid=PV.ProductGuid LEFT JOIN (" & ptt & ") AS PTT" & _
                        " ON PV.VariantCode=PTT.VariantCode AND PV.ProductGuid=PTT.ProductGuid" & _
                        " WHERE ("
                    If include_variantguid Then variantsSQL = variantsSQL & " (PV.VariantCode COLLATE " & collation & " LIKE " & SafeString(dbsearchstring) & ")"
                    If include_variantdescription Then
                        If include_variantguid Then variantsSQL = variantsSQL & " OR "
                        variantsSQL = variantsSQL & _
                            " (PV.VariantName COLLATE " & collation & " LIKE " & SafeString(dbsearchstring) & " AND PTT.ProductGuid IS NULL) OR" & _
                            " (PTT.ProductName COLLATE " & collation & " LIKE " & SafeString(dbsearchstring) & ")"
                    End If
                    variantsSQL = variantsSQL & ")"
                Else
                    variantsSQL = "SELECT TOP " & dbmaxresult & " PT.ProductGuid FROM " & pt & " INNER JOIN ProductVariant PV ON PT.ProductGuid=PV.ProductGuid WHERE"
                    If include_variantguid Then variantsSQL = variantsSQL & " (PV.VariantCode COLLATE " & collation & " LIKE " & SafeString(dbsearchstring) & ")"
                    If include_variantdescription Then
                        If include_variantguid Then variantsSQL = variantsSQL & " OR "
                        variantsSQL = variantsSQL & _
                            " (PV.VariantName COLLATE " & collation & " LIKE " & SafeString(dbsearchstring) & ")"
                    End If
                End If
                If sql <> "" Then sql += " UNION "
                sql += variantsSQL
            End If
            'AM2011061701 - ITEM LINKING - END

            ' Search for DESCRIPTION PropTransText in table PropTrans
            If include_productdescription Then
                Dim propertysql As String = USSearch.PropertySearchSQL("DESCRIPTION", "PRD", dbmaxresult, dbsearchstring, HttpContext.Current.Session("LanguageGuid").ToString, AppSettings("LANGUAGE_DEFAULT"))
                If propertysql <> "" Then
                    If s Is Nothing OrElse s = SafeString(0) Then
                        propertysql = "SELECT ProductTable.ProductGuid FROM ProductTable INNER JOIN (" & propertysql & ") AS PropertyHits ON ProductTable.ProductGuid=PropertyHits.PropOwnerRefGuid"
                        'JA2010030901 - PERFORMANCE -Start
                        ''*****CUSTOMER SPECIFIC CATALOGS***** - START
                        'If AppSettings("EXPANDIT_US_USE_CSC") Then
                        '    If CBoolEx(pageobj.globals.User("B2B")) Then
                        '        propertysql = objCSC.modifySearchQuery(propertysql, "ProductTable")
                        '    End If
                        'End If
                        ''*****CUSTOMER SPECIFIC CATALOGS***** - END
                        'JA2010030901 - PERFORMANCE -End
                    Else
                        propertysql = "SELECT ProductTable.ProductGuid FROM ProductTable INNER JOIN (SELECT DISTINCT GroupProduct.ProductGuid FROM GroupProduct WHERE GroupProduct.GroupGuid IN(" & s & ")) AS GP ON ProductTable.ProductGuid=GP.ProductGuid INNER JOIN(" & propertysql & ") AS PropertyHits ON ProductTable.ProductGuid=PropertyHits.PropOwnerRefGuid"
                        'JA2010030901 - PERFORMANCE -Start
                        ''*****CUSTOMER SPECIFIC CATALOGS***** - START
                        'If AppSettings("EXPANDIT_US_USE_CSC") Then
                        '    If CBoolEx(pageobj.globals.User("B2B")) Then
                        '        propertysql = objCSC.modifySearchQuery(propertysql, "ProductTable")
                        '    End If
                        'End If
                        ''*****CUSTOMER SPECIFIC CATALOGS***** - END
                        'JA2010030901 - PERFORMANCE -End
                    End If
                    If sql <> "" Then sql += " UNION "
                    sql += propertysql
                End If
            End If
            'JA2010030901 - PERFORMANCE -Start
            '*****CUSTOMER SPECIFIC CATALOGS***** - START
            If AppSettings("EXPANDIT_US_USE_CSC") Then
                Dim strCSCAvailableItemsSQL As String = objCSC.getListAvailableItems(pageobj.globals.User)

                sql = "SELECT SearchQuery.ProductGuid FROM (" & sql & ") AS SearchQuery INNER JOIN (" & strCSCAvailableItemsSQL & ") AS AvailableItems ON SearchQuery.ProductGuid=AvailableItems.ProductGuid"
            End If
            '*****CUSTOMER SPECIFIC CATALOGS***** - END
            'JA2010030901 - PERFORMANCE -End
            Return sql
        End Function

        Public Shared Function PrepareSearchString(ByVal searchstring As String) As String
            Dim dbsearchstring As String

            ' Patch wildcard chars in searchstring
            dbsearchstring = searchstring
            dbsearchstring = "%" & dbsearchstring & "%"
            dbsearchstring = Replace(dbsearchstring, "*", "%")
            dbsearchstring = Replace(dbsearchstring, "?", "_")

            ' Improvement needed to handle [ and ]

            ' Remove double LIKE_PERCENT
            While InStr(1, dbsearchstring, "%" & "%")
                dbsearchstring = Replace(dbsearchstring, "%%", "%")
            End While

            PrepareSearchString = dbsearchstring.ToLower
        End Function

        Public Shared Function PropertySearchSQL(ByVal propertyguid As String, ByVal ownertypeguid As String, ByVal dbmaxresult As Integer, _
                ByVal dbsearchstring As String, ByVal languageguid As String, ByVal defaultlanguageguid As Object) As String

            Dim sql As String
            Dim collation As String = AppSettings("SEARCH_COLLATION")
            If collation = "" Then collation = "Latin1_General_CI_AI"

            sql = _
                "SELECT TOP " & dbmaxresult & " PropOwnerRefGuid FROM " & _
                "(SELECT V1.PropOwnerRefGuid, V1.PropOwnerTypeGuid, Specific, Multi, Fallback FROM "
            sql = sql & _
                "(SELECT PropVal.PropOwnerRefGuid, PropVal.PropOwnerTypeGuid, PT1.PropTransText AS Specific FROM PropVal LEFT JOIN (SELECT PropValGuid, PropTransText FROM PropTrans WHERE PropLangGuid=" & SafeString(languageguid) & ") AS PT1 ON PropVal.PropValGuid=PT1.PropValGuid WHERE PropVal.PropGuid=" & SafeString(propertyguid) & ") AS V1 " & _
                ", " & _
                "(SELECT PropVal.PropOwnerRefGuid, PT1.PropTransText AS Multi FROM PropVal LEFT JOIN (SELECT PropValGuid, PropTransText FROM PropTrans WHERE PropLangGuid IS NULL) AS PT1 ON PropVal.PropValGuid=PT1.PropValGuid WHERE PropVal.PropGuid=" & SafeString(propertyguid) & ") AS V2 " & _
                ", " & _
                "(SELECT PropVal.PropOwnerRefGuid, PT1.PropTransText AS Fallback FROM PropVal LEFT JOIN (SELECT PropValGuid, PropTransText FROM PropTrans WHERE PropLangGuid=" & SafeString(defaultlanguageguid) & ") AS PT1 ON PropVal.PropValGuid=PT1.PropValGuid WHERE PropVal.PropGuid=" & SafeString(propertyguid) & ") AS V3 " & _
                "WHERE V1.PropOwnerRefGuid=V2.PropOwnerRefGuid AND V1.PropOwnerRefGuid=V3.PropOwnerRefGuid " & _
                "AND V1.PropOwnerTypeGuid=" & SafeString(ownertypeguid) & " AND " & _
                "(Specific COLLATE " & collation & " LIKE " & SafeString(dbsearchstring) & " OR (Specific IS NULL AND Multi COLLATE " & collation & " LIKE " & SafeString(dbsearchstring) & ") OR (Specific IS NULL AND Multi IS NULL AND Fallback COLLATE " & collation & " LIKE " & SafeString(dbsearchstring) & "))) AS T "
            If ownertypeguid = "PRD" And AppSettings("REQUIRE_CATALOG_ENTRY") Then
                'AM2010071901 - ENTERPRISE CSC - Start
                Dim pageobj As ExpandIT.Page = CType(System.Web.HttpContext.Current.Handler, ExpandIT.Page)
                sql = sql & "INNER JOIN (SELECT DISTINCT ProductGuid FROM GroupProduct WHERE CatalogNo=" & SafeString(pageobj.globals.User("CatalogNo")) & ") AS GP ON T.PropOwnerRefGuid=GP.ProductGuid "
                'AM2010071901 - ENTERPRISE CSC - End
            End If

            Return sql
        End Function

        'AM2010070201 - ENTERPRISE SEARCH - Start
        Public Function getProductsDict(ByVal sql As String) As ExpDictionary
            Dim products As ExpDictionary = Nothing
            Dim pageobj As ExpandIT.Page = CType(System.Web.HttpContext.Current.Handler, ExpandIT.Page)

            If products Is Nothing Then
                products = SQL2Dicts(sql, "ProductGuid")
                ' Load the information about the products
                pageobj.eis.CatDefaultLoadProducts(products, True, False, True, New String() {"ProductName", "ListPrice", "NewItem", "DateAdded", "ProductGroupCode"}, New String() {"DESCRIPTION", "PICTURE2"}, "")
            End If

            ' Calculate the prices
            pageobj.eis.GetCustomerPricesEx(products)
            Return products
        End Function

        Public Function getOccurrences(ByVal parSearchString As String, ByVal stringToCheck As String) As Integer
            Dim stringToCheckOccurrences As Integer = 0
            Dim index As Integer = -1

            stringToCheck = stringToCheck.ToLower()
            parSearchString = parSearchString.ToLower()
            While stringToCheck.Contains(parSearchString)
                stringToCheckOccurrences += 1
                index = stringToCheck.IndexOf(parSearchString)
                stringToCheck = Mid(stringToCheck, index + parSearchString.Length + 1)
            End While

            Return stringToCheckOccurrences
        End Function
        'AM2010070201 - ENTERPRISE SEARCH - End
    End Class
End Namespace

