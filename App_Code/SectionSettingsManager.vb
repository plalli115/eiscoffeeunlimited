﻿Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports ExpandIT
Imports System.Xml

Namespace ExpandIT

    ''' <summary>
    ''' The class is responsible for reading in section values from the web.config file. 
    ''' The results are stored in a dictionary which is kept in memory for faster lookups. 
    ''' </summary>
    ''' <remarks>
    ''' When a website is running under trust level medium, the built in asp.net class ConfigurationManager
    ''' causes security exceptions when trying to read custom configuration sections from the web.config file.
    ''' This class is mainly a workaround for these shortcomings.
    ''' </remarks>
    Public Class SectionSettingsManager

        Private Shared sectionsDict As ExpDictionary

        Shared Sub New()
            sectionsDict = New ExpDictionary
        End Sub

        Public Shared Function SectionSettings(ByVal section As String, ByVal key As String) As String
            Dim returnString As String = String.Empty

            ' If the section is not yet in sectionsDict
            If sectionsDict(section) Is Nothing Then
                ' We try to find it in the web.config file
                Dim dict As Dictionary(Of String, String) = SectionReader(section)
                If dict Is Nothing Then
                    Return String.Empty
                Else
                    ' And add it to the dict for later use
                    sectionsDict.Add(section, dict)
                End If
            End If

            Try ' Try to read the value
                returnString = sectionsDict(section)(key)
            Catch ex As Exception

            End Try

            Return returnString

        End Function

        Private Shared Function SectionReader(ByVal sectionName As String) As Dictionary(Of String, String)
            Dim sections As New Dictionary(Of String, String)

            Dim path As String = HttpContext.Current.Server.MapPath("~/web.config")
            Dim configs As New XmlDocument()

            Try
                configs.Load(path)
                Dim mgr As XmlNamespaceManager = New XmlNamespaceManager(configs.NameTable())
                Dim prefix As String = String.Empty

                If Not String.IsNullOrEmpty(configs.DocumentElement.NamespaceURI) Then
                    prefix = "x:"
                    mgr.AddNamespace("x", configs.DocumentElement.NamespaceURI())
                End If

                Dim xnl As XmlNodeList = configs.SelectNodes(prefix & "configuration/" & prefix & sectionName & "/" & prefix & "add", mgr)

                For Each xn As XmlNode In xnl
                    sections.Add(xn.Attributes.GetNamedItem("key").Value(), xn.Attributes.GetNamedItem("value").Value())
                Next
            Catch ex As Exception

            End Try
            If sections.Count > 0 Then
                Return sections
            End If
            Return Nothing
        End Function

    End Class

End Namespace
