Imports System.Configuration.ConfigurationManager
Imports Microsoft.VisualBasic
Imports ExpandIT.EISClass
Imports System.Collections.Generic

Namespace ExpandIT

    Public Class UserControl
        Inherits CoreUserControl

        Public globals As GlobalsClass
        Public eis As EISClass
        'Changed here to make use of both currency classes
        Public currency As CurrencyBaseClass
        Public store As StoreClass
        Public cart As CartClass
        Public b2b As B2BBaseClass


        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim mypage As ExpandIT.Page = Me.Page
            If mypage Is Nothing Then
                globals = New GlobalsClass(True)
            Else
                globals = mypage.globals
            End If

            eis = globals.eis
            currency = globals.currency
            cart = globals.cart
            store = globals.store
            b2b = globals.b2b
        End Sub

        Protected Function ThemeURL(ByVal URL As String) As String
            Dim retv As String = URL

            If retv(1) <> "~" And retv(1) <> "/" Then
                retv = "~/App_Themes/" & Page.Theme & "/" & retv
            End If
            Return retv
        End Function

    End Class

End Namespace
