Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Collections.Generic
Imports System.Data
Imports System.Net
Imports ExpandIT.GlobalsClass

Namespace ExpandIT

    Public Class USSalesTax

        '<!-- ' ExpandIT US - MPF - US Sales Tax - Start -->
        Public Sub NFCalculateTax(ByVal orderobject As ExpDictionary, ByVal Context As ExpDictionary)
            Dim strSQL As String
            Dim strCache As String
            Dim blnTaxOnTax As Boolean
            Dim strOrderLineTaxAreaCode As String
            Dim dictTaxLine As ExpDictionary
            Dim dblTaxPct, dblTaxPctTotal As Double
            Dim dblTotalNoTaxSum, dblTotalInclTaxSum, dblTotalSum As Double
            Dim dblTaxSum, dblInvoiceDiscountAmountNoTaxSum, dblInvoiceDiscountAmountInclTaxSum As Double
            Dim dblTotalInclTax, dblInvoiceDiscountAmountInclTax As Double
            Dim dictTaxDetails, dictTaxInformation As ExpDictionary
            Dim TaxAreaCode As String
            Dim taxableStates As String
            Dim taxable As Boolean = False


            dblTaxSum = 0
            dblTaxPctTotal = 0
            dblInvoiceDiscountAmountInclTax = 0
            dblTotalInclTax = 0

            'orderobject("ShipToZipCode") = ""

            'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - Start
            Dim pageobj As ExpandIT.Page = CType(System.Web.HttpContext.Current.Handler, ExpandIT.Page)
            'strSQL = "SELECT TOP (1) TaxableStatesString FROM ExpandITSetup"
            'taxableStates = getSingleValueDB(strSQL)
            Try
                taxableStates = CStrEx(pageobj.eis.getEnterpriseConfigurationValue("TaxableStatesString"))
            Catch ex As Exception
            End Try
            'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - End
            If Not taxableStates Is Nothing Then
                If Not NaV(orderobject("ShipToStateName")) Then
                    If orderobject("ShipToStateName").ToString.Length = 2 And taxableStates.ToUpper.Contains(orderobject("ShipToStateName").ToString.ToUpper) Then
                        If CStrEx(orderobject("ShipToShippingAddressCode")) = "" Then
                            'It means it's a B2C address, so check the customer card
                            If CBoolEx(getSingleValueDB("SELECT IsB2B FROM UserTable WHERE UserGuid=" & SafeString(HttpContext.Current.Session("UserGuid")))) Then
                                orderobject("ShipToTaxLiable") = CBoolEx(getSingleValueDB("SELECT TaxLiable FROM CustomerTable CT INNER JOIN UserTable UT ON CT.CustomerGuid=UT.CustomerGuid WHERE UserGuid=" & SafeString(HttpContext.Current.Session("UserGuid"))))
                                taxable = orderobject("ShipToTaxLiable")
                            Else
                                orderobject("ShipToTaxLiable") = True
                                taxable = True
                            End If
                        Else
                            taxable = CBoolEx(orderobject("ShipToTaxLiable"))
                        End If
                    End If
                End If
            End If


            If Not NaV(orderobject("ShipToZipCode")) And taxable Then  ' US Sales Tax is calculated by the shipping address.  No shipping address, no sales tax.
                If NaV(orderobject("ShipToTaxAreaCode")) Then
                    strSQL = "SELECT TOP (1) [PostCode].[TaxAreaCode] FROM [PostCode] WHERE ([PostCode].[PostCode] = " & SafeString(orderobject("ShipToZipCode")) & ") AND ([PostCode].[TaxAreaCode] <> '') ORDER BY [PostCode].[DefaultTaxAreaCode] DESC"
                    TaxAreaCode = getSingleValueDB(strSQL)
                    orderobject("ShipToTaxAreaCode") = TaxAreaCode
                End If
                If NaV(orderobject("ShipToTaxAreaCode")) Then
                Else

                    strSQL = "SELECT [TaxDetail].[TaxJurisdictionCode] " & _
                                     ",[TaxDetail].[TaxGroupCode] " & _
                                     ",[TaxDetail].[EffectiveDate] " & _
                                     ",[TaxDetail].[MaximumAmountQty] " & _
                                     ",[TaxDetail].[TaxBelowMaximum] " & _
                                     ",[TaxDetail].[TaxAboveMaximum] " & _
                                     ",[TaxDetail].[CalculateTaxOnTax] " & _
                                     "FROM [TaxDetail] " & _
                                     "WHERE ([TaxDetail].[EffectiveDate] < GetDate()) " & _
                                     "AND " & _
                                     "[TaxDetail].[TaxJurisdictionCode] IN " & _
                                        "(SELECT DISTINCT [TaxAreaLine].[TaxJurisdictionCode] " & _
                                            "FROM [TaxAreaLine] " & _
                                                    "WHERE ([TaxAreaLine].[TaxAreaCode] = " & SafeString(orderobject("ShipToTaxAreaCode")) & "))"
                    '"WHERE ([TaxAreaLine].[TaxAreaCode] IN (SELECT [PostCode].[TaxAreaCode] " & _
                    '                                                             "FROM [PostCode] " & _
                    '                                                             "WHERE [PostCode].[PostCode] = " & SafeString(orderobject("ShipToZipCode")) & "))) "
                    dictTaxDetails = SQL2Dicts(strSQL)
                    dictTaxInformation = New ExpDictionary
                    'If (dictTaxDetails.Count > 0) Then  ' There are Tax Details assigned to this zip code ... Taxes to compute ... Setup Dictionary
                    For Each dictTaxDetail As ExpDictionary In dictTaxDetails.Values
                        If Not IsDict(dictTaxInformation(dictTaxDetail("TaxJurisdictionCode"))) Then dictTaxInformation(dictTaxDetail("TaxJurisdictionCode")) = New ExpDictionary
                        If (dictTaxDetail("TaxGroupCode") = "") Then dictTaxDetail("TaxGroupCode") = "#EXP_ALL#"
                        If (dictTaxInformation(dictTaxDetail("TaxJurisdictionCode")).Exists(dictTaxDetail("TaxGroupCode"))) Then
                            If (dictTaxInformation(dictTaxDetail("TaxJurisdictionCode"))(dictTaxDetail("TaxGroupCode"))("EffectiveDate") < dictTaxDetail("EffectiveDate")) Then
                                dictTaxInformation(dictTaxDetail("TaxJurisdictionCode"))(dictTaxDetail("TaxGroupCode")) = dictTaxDetail
                            End If
                        Else
                            dictTaxInformation(dictTaxDetail("TaxJurisdictionCode"))(dictTaxDetail("TaxGroupCode")) = dictTaxDetail
                        End If
                    Next
                    'End If
                End If
            End If

            For Each dictOrderLine As ExpDictionary In orderobject("Lines").values
                dblTaxPct = 0
                dblTaxPctTotal = 0

                ' Calculate the InvoiceDiscount part of this line.
                If dictOrderLine("AllowInvoiceDiscount") Then
                    dictOrderLine("InvoiceDiscountAmount") = dictOrderLine("LineTotal") * (orderobject("InvoiceDiscountPct") / 100)
                Else
                    dictOrderLine("InvoiceDiscountAmount") = 0
                    dictOrderLine("InvoiceDiscountAmountInclTax") = 0
                End If

                dictOrderLine("ListPriceInclTax") = dictOrderLine("ListPrice")
                dictOrderLine("SubTotalInclTax") = dictOrderLine("SubTotal") - dictOrderLine("InvoiceDiscountAmount")
                dictOrderLine("LineDiscountAmountInclTax") = dictOrderLine("LineDiscountAmount")
                dictOrderLine("TotalInclTax") = dictOrderLine("LineTotal") - dictOrderLine("InvoiceDiscountAmount")

                'If Not SafeString(dictOrderLine("TaxGroupCode")) = "" Then
                If Not IsNothing(dictTaxInformation) Then
                    For Each dictTaxLine In dictTaxInformation.Values
                        blnTaxOnTax = False
                        If ((dictOrderLine("TaxGroupCode") Is Nothing) Or NaV(dictOrderLine("TaxGroupCode"))) Then strOrderLineTaxAreaCode = "" Else strOrderLineTaxAreaCode = dictOrderLine("TaxGroupCode")
                        If (IsDict(dictTaxLine(strOrderLineTaxAreaCode))) Or (IsDict(dictTaxLine("#EXP_ALL#"))) Then
                            If (IsDict(dictTaxLine(strOrderLineTaxAreaCode))) Then
                                dblTaxPct = CDblEx(dictTaxLine(strOrderLineTaxAreaCode)("TaxBelowMaximum"))
                                blnTaxOnTax = dictTaxLine(strOrderLineTaxAreaCode)("CalculateTaxOnTax")
                            ElseIf (IsDict(dictTaxLine("#EXP_ALL#"))) Then
                                dblTaxPct = CDblEx(dictTaxLine("#EXP_ALL#")("TaxBelowMaximum"))
                                blnTaxOnTax = dictTaxLine("#EXP_ALL#")("CalculateTaxOnTax")
                            Else
                                dblTaxPct = CDblEx(0)
                            End If
                        End If
                        If (blnTaxOnTax) Then '  The tax is calcuated on the Total + Tax if this is true.
                            dictOrderLine("ListPriceInclTax") = dictOrderLine("ListPriceInclTax") + (dictOrderLine("ListPriceInclTax") * (dblTaxPct / 100))
                            dictOrderLine("SubTotalInclTax") = dictOrderLine("SubTotalInclTax") + ((dictOrderLine("SubTotalInclTax") - dictOrderLine("InvoiceDiscountAmount")) * (dblTaxPct / 100))
                            dictOrderLine("LineDiscountAmountInclTax") = dictOrderLine("LineDiscountAmountInclTax") + (dictOrderLine("LineDiscountAmountInclTax") * (dblTaxPct / 100))
                            dblTaxSum = dblTaxSum + ((dictOrderLine("TotalInclTax") - dictOrderLine("InvoiceDiscountAmount")) * (dblTaxPct / 100))
                            dictOrderLine("TotalInclTax") = dictOrderLine("TotalInclTax") + ((dictOrderLine("TotalInclTax") - dictOrderLine("InvoiceDiscountAmount")) * (dblTaxPct / 100))
                        Else
                            dictOrderLine("ListPriceInclTax") = dictOrderLine("ListPriceInclTax") + (dictOrderLine("ListPrice") * (dblTaxPct / 100))
                            dictOrderLine("SubTotalInclTax") = dictOrderLine("SubTotalInclTax") + ((dictOrderLine("SubTotal") - dictOrderLine("InvoiceDiscountAmount")) * (dblTaxPct / 100))
                            dictOrderLine("LineDiscountAmountInclTax") = dictOrderLine("LineDiscountAmountInclTax") + (dictOrderLine("LineDiscountAmount") * (dblTaxPct / 100))
                            dblTaxSum = dblTaxSum + ((dictOrderLine("LineTotal") - dictOrderLine("InvoiceDiscountAmount")) * (dblTaxPct / 100))
                            dictOrderLine("TotalInclTax") = dictOrderLine("TotalInclTax") + ((dictOrderLine("LineTotal") - dictOrderLine("InvoiceDiscountAmount")) * (dblTaxPct / 100))
                        End If
                        dblTaxPctTotal = dblTaxPctTotal + dblTaxPct
                    Next
                Else
                    dictOrderLine("ListPriceInclTax") = dictOrderLine("ListPrice")
                    dictOrderLine("SubTotalInclTax") = dictOrderLine("SubTotal")
                    dictOrderLine("LineDiscountAmountInclTax") = dictOrderLine("LineDiscountAmount")
                    dictOrderLine("TotalInclTax") = dictOrderLine("LineTotal")
                End If
                'End If
                dictOrderLine("TaxPct") = dblTaxPctTotal
                dictOrderLine("TaxAmount") = dblTaxSum

                ' Calculate the InvoiceDiscount part of this line.
                'If dictOrderLine("AllowInvoiceDiscount") Then
                'dictOrderLine("InvoiceDiscountAmount") = dictOrderLine("LineTotal") * (orderobject("InvoiceDiscountPct") / 100)
                'dictOrderLine("InvoiceDiscountAmountInclTax") = (dictOrderLine("LineTotal") * (orderobject("InvoiceDiscountPct") / 100)) + (dictOrderLine("TotalInclTax") - dictOrderLine("LineTotal"))
                'Else
                'dictOrderLine("InvoiceDiscountAmount") = 0
                'dictOrderLine("InvoiceDiscountAmountInclTax") = 0
                'End If

                If dictOrderLine("ListPriceIsInclTax") Then
                    dictOrderLine("ListPrice") = dictOrderLine("ListPriceInclTax")
                    dictOrderLine("SubTotal") = dictOrderLine("SubTotalInclTax")
                    dictOrderLine("LineDiscountAmount") = dictOrderLine("LineDiscountAmountInclTax")
                    dictOrderLine("LineTotal") = dictOrderLine("TotalInclTax")
                End If

                ' Accummulate sums for header
                'dblTaxSum = dblTaxSum + dictOrderLine("TaxAmount")
                dblTotalSum = dblTotalSum + dictOrderLine("LineTotal")

                dblInvoiceDiscountAmountNoTaxSum = dblInvoiceDiscountAmountNoTaxSum + dictOrderLine("InvoiceDiscountAmount")
                dblInvoiceDiscountAmountInclTaxSum = dblInvoiceDiscountAmountInclTaxSum + dictOrderLine("InvoiceDiscountAmountInclTax")

                dblTotalNoTaxSum = dblTotalNoTaxSum + dictOrderLine("LineTotal")
                dblTotalInclTaxSum = dblTotalInclTaxSum + dictOrderLine("TotalInclTax")
            Next

            ' --
            ' Service Charge calculations. 
            '   Not implemented in US yet
            ' --

            ' Set the VATPct in the orderobject for the Shipping and Handling calculations.
            '   Not implemented in US yet
            ' orderobject("TaxPct") = VATPct

            ' Tax fields
            orderobject("TaxAmount") = RoundEx(dblTaxSum, CLngEx(AppSettings("NUMDECIMALPLACES")))
            orderobject("ServiceChargeInclTax") = orderobject("ServiceCharge") ' * (1 + VATPct / 100)
            orderobject("InvoiceDiscount") = dblInvoiceDiscountAmountNoTaxSum
            orderobject("InvoiceDiscountInclTax") = dblInvoiceDiscountAmountInclTaxSum
            orderobject("SubTotalInclTax") = dblTotalInclTaxSum
            orderobject("Total") = RoundEx(dblTotalSum + CDblEx(orderobject("ShippingAmount")) + CDblEx(orderobject("HandlingAmount")) - orderobject("InvoiceDiscount"), CLngEx(AppSettings("NUMDECIMALPLACES")))
            orderobject("TotalInclTax") = orderobject("Total") + orderobject("TaxAmount")

        End Sub

        Public Sub NFCalculateTaxShipping(ByVal orderobject As ExpDictionary, ByVal Context As ExpDictionary)
            Dim strSQL As String
            Dim strCache As String
            Dim blnTaxOnTax As Boolean
            Dim strOrderLineTaxAreaCode As String
            Dim dictTaxLine As ExpDictionary
            Dim dblTaxPct, dblTaxPctTotal As Double
            Dim dblTotalNoTaxSum, dblTotalInclTaxSum, dblTotalSum As Double
            Dim dblTaxSum, dblInvoiceDiscountAmountNoTaxSum, dblInvoiceDiscountAmountInclTaxSum As Double
            Dim dblTotalInclTax, dblInvoiceDiscountAmountInclTax As Double
            Dim dictTaxDetails, dictTaxInformation As ExpDictionary
            Dim taxableStates As String
            Dim taxable As Boolean = False

            NFCalculateTax(orderobject, Context)

            dblTaxSum = 0
            dblTaxPctTotal = 0
            dblInvoiceDiscountAmountInclTax = 0
            dblTotalInclTax = 0

            'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - Start
            Dim pageobj As ExpandIT.Page = CType(System.Web.HttpContext.Current.Handler, ExpandIT.Page)
            'strSQL = "SELECT TOP (1) ShippingTaxableStatesString FROM ExpandITSetup"
            'taxableStates = getSingleValueDB(strSQL)
            Try
                taxableStates = CStrEx(pageobj.eis.getEnterpriseConfigurationValue("ShippingTaxableStatesString"))
            Catch ex As Exception
            End Try
            'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - End
            If Not taxableStates Is Nothing Then
                If Not NaV(orderobject("ShipToStateName")) Then
                    If orderobject("ShipToStateName").ToString.Length = 2 And taxableStates.ToUpper.Contains(orderobject("ShipToStateName").ToString.ToUpper) Then
                        If CStrEx(orderobject("ShipToShippingAddressCode")) = "" Then
                            'It means it's a B2C address, so check the customer card
                            If CBoolEx(getSingleValueDB("SELECT IsB2B FROM UserTable WHERE UserGuid=" & SafeString(HttpContext.Current.Session("UserGuid")))) Then
                                orderobject("ShipToTaxLiable") = CBoolEx(getSingleValueDB("SELECT TaxLiable FROM CustomerTable CT INNER JOIN UserTable UT ON CT.CustomerGuid=UT.CustomerGuid WHERE UserGuid=" & SafeString(HttpContext.Current.Session("UserGuid"))))
                                taxable = orderobject("ShipToTaxLiable")
                            Else
                                orderobject("ShipToTaxLiable") = True
                                taxable = True
                            End If
                        Else
                            taxable = CBoolEx(orderobject("ShipToTaxLiable"))
                        End If
                    End If
                End If
            End If

            If Not NaV(orderobject("ShipToZipCode")) And taxable Then  ' US Sales Tax is calculated by the shipping address.  No shipping address, no sales tax.
                If NaV(orderobject("ShipToTaxAreaCode")) Then
                Else
                    strSQL = "SELECT [TaxDetail].[TaxJurisdictionCode],[TaxDetail].[TaxGroupCode],[TaxDetail].[EffectiveDate]," & _
                        "[TaxDetail].[MaximumAmountQty],[TaxDetail].[TaxBelowMaximum],[TaxDetail].[TaxAboveMaximum] ," & _
                        "[TaxDetail].[CalculateTaxOnTax] FROM [TaxDetail] INNER JOIN [TaxAreaLine] ON " & _
                        "[TaxDetail].[TaxJurisdictionCode]=[TaxAreaLine].[TaxJurisdictionCode] WHERE " & _
                        "[TaxDetail].[EffectiveDate] < GetDate() AND [TaxAreaLine].[TaxAreaCode]=  " & _
                        SafeString(orderobject("ShipToTaxAreaCode")) & " AND " & _
                        " ([TaxDetail].[TaxGroupCode]='" & AppSettings("SHIPPING_TAX_GROUP_CODE") & "' OR [TaxDetail].[TaxGroupCode] ='' OR [TaxDetail].[TaxGroupCode] IS NULL) " & _
                        "ORDER BY [TaxAreaLine].[CalculationOrder]"

                    dictTaxDetails = SQL2Dicts(strSQL)
                    dictTaxInformation = New ExpDictionary
                    For Each dictTaxDetail As ExpDictionary In dictTaxDetails.Values
                        If Not IsDict(dictTaxInformation(dictTaxDetail("TaxJurisdictionCode"))) Then dictTaxInformation(dictTaxDetail("TaxJurisdictionCode")) = New ExpDictionary
                        If (dictTaxDetail("TaxGroupCode") = "") Then dictTaxDetail("TaxGroupCode") = "#EXP_ALL#"
                        If (dictTaxInformation(dictTaxDetail("TaxJurisdictionCode")).Exists(dictTaxDetail("TaxGroupCode"))) Then
                            If (dictTaxInformation(dictTaxDetail("TaxJurisdictionCode"))(dictTaxDetail("TaxGroupCode"))("EffectiveDate") < dictTaxDetail("EffectiveDate")) Then
                                dictTaxInformation(dictTaxDetail("TaxJurisdictionCode"))(dictTaxDetail("TaxGroupCode")) = dictTaxDetail
                            End If
                        Else
                            dictTaxInformation(dictTaxDetail("TaxJurisdictionCode"))(dictTaxDetail("TaxGroupCode")) = dictTaxDetail
                        End If
                    Next
                End If
            End If

            dblTaxPct = 0
            dblTaxPctTotal = 0

            If Not IsNothing(dictTaxInformation) Then
                For Each dictTaxLine In dictTaxInformation.Values
                    blnTaxOnTax = False
                    strOrderLineTaxAreaCode = AppSettings("SHIPPING_TAX_GROUP_CODE")
                    If (IsDict(dictTaxLine(strOrderLineTaxAreaCode))) Or (IsDict(dictTaxLine("#EXP_ALL#"))) Then
                        If (IsDict(dictTaxLine(strOrderLineTaxAreaCode))) Then
                            dblTaxPct = CDblEx(dictTaxLine(strOrderLineTaxAreaCode)("TaxBelowMaximum"))
                            blnTaxOnTax = dictTaxLine(strOrderLineTaxAreaCode)("CalculateTaxOnTax")
                        ElseIf (IsDict(dictTaxLine("#EXP_ALL#"))) Then
                            dblTaxPct = CDblEx(dictTaxLine("#EXP_ALL#")("TaxBelowMaximum"))
                            blnTaxOnTax = dictTaxLine("#EXP_ALL#")("CalculateTaxOnTax")
                        Else
                            dblTaxPct = CDblEx(0)
                        End If
                    End If
                    If (blnTaxOnTax) Then '  The tax is calcuated on the Total + Tax if this is true.
                        dblTaxSum = dblTaxSum + ((orderobject("ShippingAmount") + dblTaxSum) * (dblTaxPct / 100))
                    Else
                        dblTaxSum = dblTaxSum + ((orderobject("ShippingAmount")) * (dblTaxPct / 100))
                    End If

                    dblTaxPctTotal = dblTaxPctTotal + dblTaxPct
                Next
            End If

            ' Tax fields
            orderobject("TaxAmount") += RoundEx(dblTaxSum, CLngEx(AppSettings("NUMDECIMALPLACES")))
            orderobject("TotalInclTax") = orderobject("TotalInclTax") + RoundEx(dblTaxSum, CLngEx(AppSettings("NUMDECIMALPLACES")))

        End Sub
        '<!-- ' ExpandIT US - ANA - US Sales Tax Shipping - End -->
        'AM2010042901 - US SALES TAX ON HANDLING - Start
        Public Sub NFCalculateTaxHandling(ByVal orderobject As ExpDictionary, ByVal Context As ExpDictionary)
            Dim strSQL As String
            Dim strCache As String
            Dim blnTaxOnTax As Boolean
            Dim strOrderLineTaxAreaCode As String
            Dim dictTaxLine As ExpDictionary
            Dim dblTaxPct, dblTaxPctTotal As Double
            Dim dblTotalNoTaxSum, dblTotalInclTaxSum, dblTotalSum As Double
            Dim dblTaxSum, dblInvoiceDiscountAmountNoTaxSum, dblInvoiceDiscountAmountInclTaxSum As Double
            Dim dblTotalInclTax, dblInvoiceDiscountAmountInclTax As Double
            Dim dictTaxDetails, dictTaxInformation As ExpDictionary
            Dim taxableStates As String
            Dim taxable As Boolean = False

            If Not CBoolEx(AppSettings("EXPANDIT_US_USE_SHIPPING_TAX")) Then
                NFCalculateTax(orderobject, Context)
            End If

            dblTaxSum = 0
            dblTaxPctTotal = 0
            dblInvoiceDiscountAmountInclTax = 0
            dblTotalInclTax = 0

            'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - Start
            Dim pageobj As ExpandIT.Page = CType(System.Web.HttpContext.Current.Handler, ExpandIT.Page)
            'strSQL = "SELECT TOP (1) HandlingTaxableStatesString FROM ExpandITSetup"
            'taxableStates = getSingleValueDB(strSQL)
            Try
                taxableStates = CStrEx(pageobj.eis.getEnterpriseConfigurationValue("HandlingTaxableStatesString"))
            Catch ex As Exception
            End Try
            'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - End
            If Not taxableStates Is Nothing Then
                If Not NaV(orderobject("ShipToStateName")) Then
                    If orderobject("ShipToStateName").ToString.Length = 2 And taxableStates.ToUpper.Contains(orderobject("ShipToStateName").ToString.ToUpper) Then
                        If CStrEx(orderobject("ShipToShippingAddressCode")) = "" Then
                            'It means it's a B2C address, so check the customer card
                            If CBoolEx(getSingleValueDB("SELECT IsB2B FROM UserTable WHERE UserGuid=" & SafeString(HttpContext.Current.Session("UserGuid")))) Then
                                orderobject("ShipToTaxLiable") = CBoolEx(getSingleValueDB("SELECT TaxLiable FROM CustomerTable CT INNER JOIN UserTable UT ON CT.CustomerGuid=UT.CustomerGuid WHERE UserGuid=" & SafeString(HttpContext.Current.Session("UserGuid"))))
                                taxable = orderobject("ShipToTaxLiable")
                            Else
                                orderobject("ShipToTaxLiable") = True
                                taxable = True
                            End If
                        Else
                            taxable = CBoolEx(orderobject("ShipToTaxLiable"))
                        End If
                    End If
                End If
            End If

            If Not NaV(orderobject("ShipToZipCode")) And taxable Then  ' US Sales Tax is calculated by the shipping address.  No shipping address, no sales tax.
                If NaV(orderobject("ShipToTaxAreaCode")) Then
                Else
                    strSQL = "SELECT [TaxDetail].[TaxJurisdictionCode],[TaxDetail].[TaxGroupCode],[TaxDetail].[EffectiveDate]," & _
                        "[TaxDetail].[MaximumAmountQty],[TaxDetail].[TaxBelowMaximum],[TaxDetail].[TaxAboveMaximum] ," & _
                        "[TaxDetail].[CalculateTaxOnTax] FROM [TaxDetail] INNER JOIN [TaxAreaLine] ON " & _
                        "[TaxDetail].[TaxJurisdictionCode]=[TaxAreaLine].[TaxJurisdictionCode] WHERE " & _
                        "[TaxDetail].[EffectiveDate] < GetDate() AND [TaxAreaLine].[TaxAreaCode]=  " & _
                        SafeString(orderobject("ShipToTaxAreaCode")) & " AND " & _
                        " ([TaxDetail].[TaxGroupCode]='" & AppSettings("HANDLING_TAX_GROUP_CODE") & "' OR [TaxDetail].[TaxGroupCode] ='' OR [TaxDetail].[TaxGroupCode] IS NULL) " & _
                        "ORDER BY [TaxAreaLine].[CalculationOrder]"

                    dictTaxDetails = SQL2Dicts(strSQL)
                    dictTaxInformation = New ExpDictionary
                    For Each dictTaxDetail As ExpDictionary In dictTaxDetails.Values
                        If Not IsDict(dictTaxInformation(dictTaxDetail("TaxJurisdictionCode"))) Then dictTaxInformation(dictTaxDetail("TaxJurisdictionCode")) = New ExpDictionary
                        If (dictTaxDetail("TaxGroupCode") = "") Then dictTaxDetail("TaxGroupCode") = "#EXP_ALL#"
                        If (dictTaxInformation(dictTaxDetail("TaxJurisdictionCode")).Exists(dictTaxDetail("TaxGroupCode"))) Then
                            If (dictTaxInformation(dictTaxDetail("TaxJurisdictionCode"))(dictTaxDetail("TaxGroupCode"))("EffectiveDate") < dictTaxDetail("EffectiveDate")) Then
                                dictTaxInformation(dictTaxDetail("TaxJurisdictionCode"))(dictTaxDetail("TaxGroupCode")) = dictTaxDetail
                            End If
                        Else
                            dictTaxInformation(dictTaxDetail("TaxJurisdictionCode"))(dictTaxDetail("TaxGroupCode")) = dictTaxDetail
                        End If
                    Next
                End If
            End If

            dblTaxPct = 0
            dblTaxPctTotal = 0

            If Not IsNothing(dictTaxInformation) Then
                For Each dictTaxLine In dictTaxInformation.Values
                    blnTaxOnTax = False
                    strOrderLineTaxAreaCode = AppSettings("HANDLING_TAX_GROUP_CODE")
                    If (IsDict(dictTaxLine(strOrderLineTaxAreaCode))) Or (IsDict(dictTaxLine("#EXP_ALL#"))) Then
                        If (IsDict(dictTaxLine(strOrderLineTaxAreaCode))) Then
                            dblTaxPct = CDblEx(dictTaxLine(strOrderLineTaxAreaCode)("TaxBelowMaximum"))
                            blnTaxOnTax = dictTaxLine(strOrderLineTaxAreaCode)("CalculateTaxOnTax")
                        ElseIf (IsDict(dictTaxLine("#EXP_ALL#"))) Then
                            dblTaxPct = CDblEx(dictTaxLine("#EXP_ALL#")("TaxBelowMaximum"))
                            blnTaxOnTax = dictTaxLine("#EXP_ALL#")("CalculateTaxOnTax")
                        Else
                            dblTaxPct = CDblEx(0)
                        End If
                    End If
                    If (blnTaxOnTax) Then '  The tax is calcuated on the Total + Tax if this is true.
                        dblTaxSum = dblTaxSum + ((orderobject("HandlingAmount") + dblTaxSum) * (dblTaxPct / 100))
                    Else
                        dblTaxSum = dblTaxSum + ((orderobject("HandlingAmount")) * (dblTaxPct / 100))
                    End If

                    dblTaxPctTotal = dblTaxPctTotal + dblTaxPct
                Next
            End If

            ' Tax fields
            orderobject("TaxAmount") += RoundEx(dblTaxSum, CLngEx(AppSettings("NUMDECIMALPLACES")))
            orderobject("TotalInclTax") = orderobject("TotalInclTax") + RoundEx(dblTaxSum, CLngEx(AppSettings("NUMDECIMALPLACES")))

        End Sub
        'AM2010042901 - US SALES TAX ON HANDLING - End


    End Class

End Namespace
