Imports Microsoft.VisualBasic
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Configuration.ConfigurationManager

Namespace ExpandIT.SimpleUIControls

    Public Class DibsPaymentControl
        Inherits GenericPaymentControl
        Implements INamingContainer

        Private protocol As String = "http://"
        Private port As String = ""
        Public PopupURL_DIBS As String = ShopFullPath() & "/pmt_dibs_popup.ashx"

        Public Property RenderScriptBlock() As Boolean
            Get
                Return ViewState("renderScriptBlock")
            End Get
            Set(ByVal value As Boolean)
                ViewState("renderScriptBlock") = value
            End Set
        End Property

        Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
            MyBase.OnLoad(e)
            If InStr(1, UCase(HttpContext.Current.Request.ServerVariables("SERVER_PROTOCOL")), "HTTPS") Then
                protocol = "https://"
            End If
        End Sub

        Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
            If RenderScriptBlock Then
                writer.Write(renderScript())
                writer.Write(renderScript2())
                Me.clickBtn.Attributes.Add("OnClick", "noPostBack('" & PopupURL_DIBS & "');")
            End If
            writer.Write(DIBS_ShowPaymentForm_HiddenFields())
            If Me.ShowDIBSErrorMessage <> "" Then
                Me.lblErrorText.Text = Me.ShowDIBSErrorMessage()
                Me.trError.Visible = True
            End If
            MyBase.Render(writer)
        End Sub

        Protected Function DIBS_FormatAmount(ByVal aValue As Object) As String
            Dim retval As String
            retval = CDblEx(aValue)
            Return Replace(FormatNumber(retval, -1, -1, 0, 0), ",", ".")
        End Function

        Function DIBS_ShowPaymentForm_HiddenFields() As String
            Dim PaymentCurrency_DIBS As String
            Dim PaymentMerchantID_DIBS As Object
            Dim PaymentCallBackURL_DIBS As String
            Dim PaymentAcceptURL_DIBS As String
            Dim PaymentDeclineURL_DIBS As String
            Dim PopupURL_Accept As String
            Dim PopupURL_Cancel As String
            Dim html As String = ""
            Dim LineCount As Integer
            Dim dictLine As ExpDictionary

            ' Calculate values.
            PaymentCurrency_DIBS = globals.currency.GetIso4217Code(globals.OrderDict("CurrencyGuid"))
            PaymentMerchantID_DIBS = AppSettings("DIBS_MERCHANTID")

            PaymentCallBackURL_DIBS = ShopFullPath() & "/_payment.aspx?PaymentType=DIBS&DIBSCallBack=1"
            PaymentAcceptURL_DIBS = ShopFullPath() & "/confirmed.aspx?CustomerReference=" & globals.OrderDict("CustomerReference") & "&DIBSCallAccept=1"
            PaymentDeclineURL_DIBS = ShopFullPath() & "/payment.aspx?DIBSCancel=1"
            PopupURL_Accept = PopupURL_DIBS & "?popresult=accept&popaction=PopDown"
            PopupURL_Cancel = PopupURL_DIBS & "?popresult=cancel&popaction=PopDown"

            ' DIBS Debugging.
            If AppSettings("DIBS_DEBUG") Then
                PaymentAcceptURL_DIBS = PaymentCallBackURL_DIBS
                PaymentCallBackURL_DIBS = ""
            End If

            ' Insert the Hidden fields required in the DIBS form.
            ' DIBS Fields 
            html = html & "<input type=""hidden"" name=""popaction"" value=""PopUp""/>" & vbCrLf
            html = html & "<input type=""hidden"" name=""shopwindow"" value=""shopwin""/>" & vbCrLf
            html = html & "<input type=""hidden"" name=""color"" value=""blue""/>" & vbCrLf
            If AppSettings("DIBS_TESTMODE") Then
                html = html & "	<input type=""hidden"" name=""test"" value=""foo""/>" & vbCrLf
            End If
            html = html & "<input type=""hidden"" name=""merchant"" value=""" & PaymentMerchantID_DIBS & """/>" & vbCrLf
            html = html & "<input type=""hidden"" name=""orderid"" value=""" & HTMLEncode(globals.OrderDict("CustomerReference")) & """/>" & vbCrLf
            html = html & "<input type=""hidden"" name=""lang"" value=""" & LCase(HttpContext.Current.Session("LanguageGuid").ToString) & """/>" & vbCrLf
            html = html & "<input type=""hidden"" name=""amount"" value=""" & Math.Round(CDec(globals.OrderDict("TotalInclTax")) * CDec(100), 0) & """/> " & vbCrLf
            html = html & "<input type=""hidden"" name=""currency"" value=""" & PaymentCurrency_DIBS & """/>" & vbCrLf
            html = html & "<input type=""hidden"" name=""accepturl"" value=""" & HTMLEncode(PopupURL_Accept) & """/>" & vbCrLf
            html = html & "<input type=""hidden"" name=""callbackurl"" value=""" & HTMLEncode(PaymentCallBackURL_DIBS) & """/>" & vbCrLf
            html = html & "<input type=""hidden"" name=""cancelurl"" value=""" & HTMLEncode(PopupURL_Cancel) & """/>" & vbCrLf
            html = html & "<input type=""hidden"" name=""orgaccepturl"" value=""" & HTMLEncode(PaymentAcceptURL_DIBS) & """/>" & vbCrLf
            html = html & "<input type=""hidden"" name=""orgcancelurl"" value=""" & HTMLEncode(PaymentDeclineURL_DIBS) & """/>" & vbCrLf

            html = html & "<input type=""hidden"" name=""HTTP_COOKIE"" value=""" & HTMLEncode(HttpContext.Current.Request.ServerVariables("HTTP_COOKIE")) & """/>" & vbCrLf
            If AppSettings("DIBS_CALCFEE") Then
                html = html & "	<input type=""hidden"" name=""calcfee"" value=""yes""/>" & vbCrLf
            End If

            html = html & "<input type=""hidden"" name=""delivery1." & HttpContext.GetGlobalResourceObject("Language", "LABEL_BILL_TO") & """ value=""" & vbCrLf
            html = html & _
             IIf(Len(CStrEx(globals.User("ContactName"))) > 0, "&nbsp;" & HTMLEncode(globals.User("ContactName")), "") & _
             IIf(Len(CStrEx(globals.User("CompanyName"))) > 0, "&nbsp;" & HTMLEncode(globals.User("CompanyName")), "") & _
             IIf(Len(CStrEx(globals.User("Address1"))) > 0, "&nbsp;" & HTMLEncode(globals.User("Address1")), "") & _
             IIf(Len(CStrEx(globals.User("Address2"))) > 0, "&nbsp;" & HTMLEncode(globals.User("Address2")), "") & _
             IIf(Len(CStrEx(globals.User("ZipCode"))) > 0, "&nbsp;" & HTMLEncode(globals.User("ZipCode")), "") & _
             IIf(Len(CStrEx(globals.User("CityName"))) > 0, "&nbsp;" & HTMLEncode(globals.User("CityName")), "") & _
             IIf(Len(CStrEx(globals.User("CountryGuid"))) > 0, "&nbsp;" & HTMLEncode(globals.User("CountryGuid")), "") & """/>" & vbCrLf

            If Not CStrEx(globals.User("CountryGuid")) <> "" Then
                html = html & "	<input type=""hidden"" name=""delivery5"" value=""" & GetCountryName(CStrEx(globals.User("CountryGuid"))) & """/>" & vbCrLf
            End If

            html = html & "<input type=""hidden"" name=""ordline0-1"" value=""" & HttpContext.GetGlobalResourceObject("Language", "LABEL_PRODUCT") & """/>" & vbCrLf
            html = html & "<input type=""hidden"" name=""ordline0-2"" value=""" & HttpContext.GetGlobalResourceObject("Language", "LABEL_NAME") & """/>" & vbCrLf
            html = html & "<input type=""hidden"" name=""ordline0-3"" value=""" & HttpContext.GetGlobalResourceObject("Language", "LABEL_QUANTITY") & """/>" & vbCrLf
            html = html & "<input type=""hidden"" name=""ordline0-4"" value=""" & HttpContext.GetGlobalResourceObject("Language", "LABEL_PRICE") & """/>" & vbCrLf

            LineCount = 1
            For Each dictLine In globals.OrderDict("Lines").values
                html = html & "<input type=""hidden"" name=""ordline" & LineCount & "-1"" value=""" & dictLine("ProductGuid") & """/>" & vbCrLf
                html = html & "<input type=""hidden"" name=""ordline" & LineCount & "-2"" value=""" & dictLine("ProductName") & """/> " & vbCrLf
                html = html & "<input type=""hidden"" name=""ordline" & LineCount & "-3"" value=""" & dictLine("Quantity") & """/>" & vbCrLf
                html = html & "<input type=""hidden"" name=""ordline" & LineCount & "-4"" value=""" & DIBS_FormatAmount(dictLine("TotalInclTax")) & """/> " & vbCrLf

                LineCount = LineCount + 1
            Next

            LineCount = 1

            If AppSettings("SHOW_DISCOUNTS") Then
                html = html & "<input type=""hidden"" name=""priceinfo" & LineCount & "." & HttpContext.GetGlobalResourceObject("Language", "LABEL_INVOICE_DISCOUNT") & """ value=""" & DIBS_FormatAmount(globals.OrderDict("InvoiceDiscountInclTax")) & """/> " & vbCrLf
                LineCount = LineCount + 1
            End If

            html = html & "<input type=""hidden"" name=""priceinfo" & LineCount & "." & HttpContext.GetGlobalResourceObject("Language", "LABEL_SERVICE_CHARGE") & """ value=""" & DIBS_FormatAmount(globals.OrderDict("ServiceChargeInclTax")) & """/>" & vbCrLf

            LineCount = LineCount + 1

            If AppSettings("SHIPPING_HANDLING_ENABLED") Then
                html = html & "<input type=""hidden"" name=""priceinfo" & LineCount & "." & HttpContext.GetGlobalResourceObject("Language", "LABEL_SHIPPING") & """ value=""" & DIBS_FormatAmount(globals.OrderDict("ShippingAmountInclTax")) & """/> " & vbCrLf
                LineCount = LineCount + 1
                html = html & "<input type=""hidden"" name=""priceinfo" & LineCount & "." & HttpContext.GetGlobalResourceObject("Language", "LABEL_HANDLING") & """ value=""" & DIBS_FormatAmount(globals.OrderDict("HandlingAmountInclTax")) & """/> " & vbCrLf
            End If
            Return html
        End Function

        Function ShowDIBSErrorMessage() As String
            Dim Retval As String

            Retval = ""

            If Not HttpContext.Current.Request("reason") Is Nothing Then
                Select Case Integer.Parse(GetRequest("reason"))
                    Case 0
                        Retval = Retval & HttpContext.GetGlobalResourceObject("Language", "ERROR_DIBS_1") & "."
                    Case 1, 2, 3
                        Retval = Retval & HttpContext.GetGlobalResourceObject("Language", "ERROR_DIBS_2") & "."
                    Case 4
                        Retval = Retval & HttpContext.GetGlobalResourceObject("Language", "ERROR_DIBS_3") & "."
                    Case 5
                        Retval = Retval & HttpContext.GetGlobalResourceObject("Language", "ERROR_DIBS_4") & "."
                    Case 6
                        Retval = Retval & HttpContext.GetGlobalResourceObject("Language", "ERROR_DIBS_5") & "."
                    Case 7
                        Retval = Retval & HttpContext.GetGlobalResourceObject("Language", "ERROR_DIBS_6") & "."
                    Case 8
                        Retval = Retval & HttpContext.GetGlobalResourceObject("Language", "ERROR_DIBS_7") & "."
                    Case 9
                        Retval = Retval & HttpContext.GetGlobalResourceObject("Language", "ERROR_DIBS_8") & "."
                    Case 10
                        Retval = Retval & HttpContext.GetGlobalResourceObject("Language", "ERROR_DIBS_9") & "."
                    Case Else
                        Retval = Retval & HttpContext.GetGlobalResourceObject("Language", "ERROR_DIBS_10") & " (" & GetRequest("reason") & ")"
                End Select
            End If

            If Not HttpContext.Current.Request("DIBSCancel") Is Nothing Then
                Retval = Retval & HttpContext.GetGlobalResourceObject("Language", "ERROR_PAYMENT_CANCELLED_BY_USER")
            End If

            Return Retval
        End Function

        Private Function renderScript() As String
            Dim sb As New StringBuilder()
            sb.AppendLine("")
            sb.AppendLine("<script type='text/javascript'>")
            sb.AppendLine("<!--")
            sb.AppendLine("	newwin = null;")
                sb.AppendLine("function doPopup(f)")
            sb.AppendLine("	{")
            sb.AppendLine("		newwin = window.open ('" & PopupURL_DIBS & "','Betaling','status,width=550,height=600');")
            sb.AppendLine("		newwin.focus();")
            sb.AppendLine("		window.name = 'shopwin';")
            sb.AppendLine("		return true;")
            sb.AppendLine("	}")
            sb.AppendLine("//-->")
            sb.AppendLine("</script>")

            Return sb.ToString()
        End Function

        Private Function renderScript2() As String

            Dim sb As New StringBuilder()
            sb.AppendLine("")
            sb.AppendLine("<script type='text/javascript'>")
            sb.AppendLine("<!--")
            sb.AppendLine("function noPostBack(sNewFormAction)")
            sb.AppendLine("{")
            sb.AppendLine("    if(document.layers) //The browser is Netscape 4")
            sb.AppendLine("    {")
            sb.AppendLine("        document.layers['Content'].document.forms[0].target = 'Betaling';")
            sb.AppendLine("        document.layers['Content'].document.forms[0].__VIEWSTATE.name =")
            sb.AppendLine("                                                           'NOVIEWSTATE';")
            sb.AppendLine("        document.layers['Content'].document.forms[0].action = sNewFormAction;")
            sb.AppendLine("        doPopup(this);")
            sb.AppendLine("    }")
            sb.AppendLine("    else //It is some other browser that understands the DOM")
            sb.AppendLine("    {")
            sb.AppendLine("        document.forms[0].target = 'Betaling'")
            sb.AppendLine("        document.forms[0].__VIEWSTATE.name = 'NOVIEWSTATE';")
            sb.AppendLine("        document.forms[0].action = sNewFormAction;")
            sb.AppendLine("        doPopup(this);")
            sb.AppendLine("    }")
            sb.AppendLine("}")
            sb.AppendLine("//-->")
            sb.AppendLine("</script>")

            Return sb.ToString()

        End Function

    End Class

End Namespace

