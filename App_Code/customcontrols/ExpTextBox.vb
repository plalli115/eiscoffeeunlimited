﻿Imports Microsoft.VisualBasic
Imports ExpandIT

Namespace ExpandIT.SimpleUIControls

    Public Class ExpTextBox
        Inherits TextBox

        Private _Size As String
        'Private _IsVisible As Boolean

        Public Property Size() As String
            Get
                Dim str As String = ViewState(Me.ClientID & "Size")
                If Not String.IsNullOrEmpty(str) Then
                    Return ViewState(Me.ClientID & "Size")
                ElseIf Not String.IsNullOrEmpty(_Size) Then
                    Return _Size
                Else
                    Return String.Empty
                End If
            End Get
            Set(ByVal value As String)
                ViewState(Me.ClientID & "Size") = value
                _Size = value
            End Set
        End Property

        'Public Property IsVisible() As Boolean
        '    Get
        '        Dim str As String = ViewState(Me.ClientID & "IsVisible")
        '        If Not String.IsNullOrEmpty(str) Then
        '            Return ViewState(Me.ClientID & "IsVisible")
        '        ElseIf Not String.IsNullOrEmpty(_Size) Then
        '            Return _IsVisible
        '        Else
        '            Return String.Empty
        '        End If
        '    End Get
        '    Set(ByVal value As Boolean)
        '        ViewState(Me.ClientID & "IsVisible") = value
        '        _IsVisible = value
        '    End Set
        'End Property

        'Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        '    If _IsVisible Then
        '         MyBase.Render(writer)
        '    End If
        'End Sub

        Public Overrides Sub RenderControl(ByVal writer As System.Web.UI.HtmlTextWriter)
            If Visible Then
                writer.AddAttribute(HtmlTextWriterAttribute.Type, "text")
                writer.AddAttribute(HtmlTextWriterAttribute.Id, Me.ClientID)
                writer.AddAttribute(HtmlTextWriterAttribute.Name, Me.ID)
                writer.AddAttribute(HtmlTextWriterAttribute.Value, Me.Text)
                writer.AddAttribute(HtmlTextWriterAttribute.Size, Me.Size)
                writer.RenderBeginTag(HtmlTextWriterTag.Input)
                writer.RenderEndTag()
            End If
        End Sub

    End Class

End Namespace