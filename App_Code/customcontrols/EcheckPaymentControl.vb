Imports Microsoft.VisualBasic
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Configuration.ConfigurationManager

Namespace ExpandIT.SimpleUIControls
    '<!--JA2010022601 - ECHECK - Start-->

    ''' <summary>
    ''' This is the new AuthNetAIMPaymentControl that inherits from AuthNetPaymentControl
    ''' The extended functionality involves three new properties that handles creditcard
    ''' information.
    ''' The class overrides it's baseclass CreateChildControls() method, and overloads the baseclass's 
    ''' buttonMode method.
    ''' The purpouses of these overrides/overloads is to create a slightly different user interface,
    ''' providing three textboxes to collect creditcard information from the customer.                    
    ''' </summary>
    ''' <remarks></remarks>
    Public Class EcheckPaymentControl
        Inherits AuthNetPaymentControl

        'Private tbxCCN As TextBox
        'Private tbxCCV As TextBox
        'Private tbxExpDate As TextBox
        Private tbxRouting As TextBox
        Private tbxAccount As TextBox
        Private tbxBankName As TextBox
        Private tbxAssociated As TextBox
        Private tbxCheckNumber As TextBox
        Private lblCheckNumber As Label

        Private typeBank As DropDownList
        Private typeEcheck As DropDownList

        Private sender As New Object
        Private e As New System.EventArgs

        ''' <summary>
        ''' Get the Routing number
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property RoutingNumber() As String
            Get
                Return tbxRouting.Text
            End Get
        End Property

        ''' <summary>
        ''' Get the Account number
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property AccountNumber() As String
            Get
                Return tbxAccount.Text
            End Get
        End Property

        ''' <summary>
        ''' Get the Check number on the customer's paper check. The check number is only required when a merchant
        ''' is converting a customer's paper check into an electronic check. Required only when typeEcheck(ARC - BOC)
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property CheckNumber() As String
            Get
                Return tbxCheckNumber.Text
            End Get
        End Property
        ''' <summary>
        ''' Get the Name associated with the bank account  
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property AssociatedName() As String
            Get
                Return tbxAssociated.Text
            End Get
        End Property

        ''' <summary>
        ''' Get the Bank name
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property BankName() As String
            Get
                Return tbxBankName.Text
            End Get
        End Property
        ''' <summary>
        ''' Get the Bank type
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property BankType() As String
            Get
                Return typeBank.SelectedValue
            End Get
        End Property
        ''' <summary>
        ''' Get the Bank type
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property EcheckType() As String
            Get
                Return typeEcheck.SelectedValue
            End Get
        End Property


        Protected Overrides Sub CreateChildControls()
            Dim obj As New ObjectTagBuilder()

            Dim mainTable As New Table()
            Dim mainTr As New TableRow()
            Dim mainTd1 As New TableCell()
            Dim img As New Image()
            img.ImageUrl = "~/images/cards/echeck_Big.png"
            img.Width = "200"
            mainTd1.Controls.Add(img)


            ' create the table
            Dim table As New Table()
            'table.Width = TableWidth
            table.Width = "335"

            ' create the first row
            Dim tr As New TableRow()

            ' add a cell to the row
            Dim td As New TableCell()
            td.ColumnSpan = 3
            tr.Cells.Add(td)
            table.Rows.Add(tr)

            ' create the second row
            tr = New TableRow()

            ' add a header cell to the row
            Dim th As New TableHeaderCell()
            th.ColumnSpan = 3

            ' add a label to the header cell
            hdrText = New Label()
            hdrText.Text = HeaderText
            th.Controls.Add(hdrText)
            tr.Cells.Add(th)
            table.Rows.Add(tr)

            tr = New TableRow()
            td = New TableCell()
            td.ColumnSpan = 3
            tr.Cells.Add(td)
            table.Rows.Add(tr)
            ' Show Error Text
            trError = New TableRow()
            td = New TableCell()
            td.ColumnSpan = 3
            lblErrorText = New Label()
            lblErrorText.ForeColor = Drawing.Color.Red
            td.Controls.Add(lblErrorText)
            trError.Cells.Add(td)
            trError.Visible = False
            table.Rows.Add(trError)

            Dim mainTd2 As New TableCell()
            mainTd2.Controls.Add(table)
            mainTr.Cells.Add(mainTd2)
            mainTd1.Style.Add(System.Web.UI.HtmlTextWriterStyle.PaddingTop, "65px")
            mainTr.Cells.Add(mainTd1)
            mainTable.Rows.Add(mainTr)


            If RadioButtonMode Then
                radioMode(table)
            Else
                buttonMode(table)
            End If
            ' Add the table to the control's ControlCollection
            Me.Controls.Add(mainTable)
        End Sub

        Protected Overloads Sub buttonMode(ByRef table As Table)
            ' First row
            Dim tr As New TableRow()
            Dim td As New TableCell()
            td.ColumnSpan = 3
            LblText = New Label()
            LblText.Text = LabelText
            td.Controls.Add(LblText)
            tr.Cells.Add(td)
            table.Rows.Add(tr)

            ' Second row
            tr = New TableRow()
            td = New TableCell()
            td.ColumnSpan = 3
            Dim txt As New LiteralControl()
            txt.Text = "&nbsp;"
            td.Controls.Add(txt)
            tr.Cells.Add(td)
            table.Rows.Add(tr)

            'AM2010052001 - TESTING PAYMENT CREDENTIALS - START
            If AppSettings("EXPANDIT_US_USE_TESTING_PAYMENT_CREDENTIALS") Then

                tr = New TableRow()
                td = New TableCell()
                td.ColumnSpan = 3
                td.ForeColor = Drawing.Color.Red
                td.Font.Bold = True
                Dim txtTest As New LiteralControl()
                txtTest.Text = "The e-check information has been filled out for testing purposes."
                td.Controls.Add(txtTest)
                tr.Cells.Add(td)
                table.Rows.Add(tr)
            End If
            'AM2010052001 - TESTING PAYMENT CREDENTIALS - END

            ' Third row
            tr = New TableRow()
            td = New TableCell()
            td.Width = 130
            td.HorizontalAlign = HorizontalAlign.Left
            td.Style.Add(System.Web.UI.HtmlTextWriterStyle.VerticalAlign, "middle")
            Dim lblRnumber As New Label()
            lblRnumber.Text = "Routing Number:"
            tr.Cells.Add(td)
            td.Controls.Add(lblRnumber)
            td = New TableCell()
            'td.Style.Add(System.Web.UI.HtmlTextWriterStyle.PaddingRight, "35px")
            td.Width = 160
            td.HorizontalAlign = HorizontalAlign.Left
            'td.ColumnSpan = 2
            tbxRouting = New TextBox()
            tbxRouting.Width = 160
            tbxRouting.MaxLength = 16
            'AM2010052001 - TESTING PAYMENT CREDENTIALS - START
            If AppSettings("EXPANDIT_US_USE_TESTING_PAYMENT_CREDENTIALS") Then

                tbxRouting.Text = "031100393"
                tbxRouting.ReadOnly = True
            End If
            'AM2010052001 - TESTING PAYMENT CREDENTIALS - END
            tbxRouting.CssClass = "borderTextBox"
            td.Controls.Add(tbxRouting)
            tr.Cells.Add(td)
            table.Rows.Add(tr)

            'Fourth row
            tr = New TableRow()
            td = New TableCell()
            td.Width = 130
            td.HorizontalAlign = HorizontalAlign.Left
            td.Style.Add(System.Web.UI.HtmlTextWriterStyle.VerticalAlign, "middle")
            Dim lblAccount As New Label()
            lblAccount.Text = "Account Number:"
            tr.Cells.Add(td)
            td.Controls.Add(lblAccount)
            td = New TableCell()
            td.Width = 160
            td.HorizontalAlign = HorizontalAlign.Left
            'td.ColumnSpan = 2
            tbxAccount = New TextBox()
            tbxAccount.Width = 160
            tbxAccount.MaxLength = 16
            'AM2010052001 - TESTING PAYMENT CREDENTIALS - START
            If AppSettings("EXPANDIT_US_USE_TESTING_PAYMENT_CREDENTIALS") Then

                tbxAccount.Text = "1234567890"
                tbxAccount.ReadOnly = True
            End If
            'AM2010052001 - TESTING PAYMENT CREDENTIALS - END
            tbxAccount.CssClass = "borderTextBox"
            td.Controls.Add(tbxAccount)
            tr.Cells.Add(td)
            table.Rows.Add(tr)

            'Fifth(row)
            tr = New TableRow()
            td = New TableCell() 'table cell 1
            td.Width = 130
            td.HorizontalAlign = HorizontalAlign.Left
            td.Style.Add(System.Web.UI.HtmlTextWriterStyle.VerticalAlign, "middle")
            Dim lblBankType As New Label()
            lblBankType.Text = "Type of Bank Account:"
            tr.Cells.Add(td)
            td.Controls.Add(lblBankType)
            td = New TableCell() 'table cell 2
            td.Width = 160
            td.Style.Add(System.Web.UI.HtmlTextWriterStyle.PaddingLeft, "4px")
            td.HorizontalAlign = HorizontalAlign.Left
            typeBank = New DropDownList
            typeBank.Width = 163
            typeBank.Style.Add(System.Web.UI.HtmlTextWriterStyle.FontFamily, "Tahoma")
            typeBank.Style.Add(System.Web.UI.HtmlTextWriterStyle.Color, "#323233")
            typeBank.Style.Add(System.Web.UI.HtmlTextWriterStyle.FontSize, "11px")
            Dim checking = New ListItem("CHECKING", "CHECKING")
            typeBank.Items.Add(checking)
            Dim business = New ListItem("BUSINESSCHECKING", "BUSINESSCHECKING")
            typeBank.Items.Add(business)
            Dim savings = New ListItem("SAVINGS", "SAVINGS")
            typeBank.Items.Add(savings)
            td.Controls.Add(typeBank)
            tr.Cells.Add(td)
            table.Rows.Add(tr)

            'Sixth row
            tr = New TableRow()
            td = New TableCell()
            td.Width = 130
            td.HorizontalAlign = HorizontalAlign.Left
            td.Style.Add(System.Web.UI.HtmlTextWriterStyle.VerticalAlign, "middle")
            Dim lblBankName As New Label()
            lblBankName.Text = "Bank Name:"
            tr.Cells.Add(td)
            td.Controls.Add(lblBankName)
            td = New TableCell()
            td.Width = 160
            td.HorizontalAlign = HorizontalAlign.Left
            'td.ColumnSpan = 2
            tbxBankName = New TextBox()
            tbxBankName.Width = 160
            tbxBankName.MaxLength = 16
            'AM2010052001 - TESTING PAYMENT CREDENTIALS - START
            If AppSettings("EXPANDIT_US_USE_TESTING_PAYMENT_CREDENTIALS") Then
                tbxBankName.Text = "CHASE"
                tbxBankName.ReadOnly = True
            End If
            'AM2010052001 - TESTING PAYMENT CREDENTIALS - END
            tbxBankName.CssClass = "borderTextBox"
            td.Controls.Add(tbxBankName)
            tr.Cells.Add(td)
            table.Rows.Add(tr)

            'Seventh row
            tr = New TableRow()
            td = New TableCell()
            td.Width = 130
            td.HorizontalAlign = HorizontalAlign.Left
            td.Style.Add(System.Web.UI.HtmlTextWriterStyle.VerticalAlign, "middle")
            Dim lblAssociatedName As New Label()
            lblAssociatedName.Text = "Account Holder:"
            tr.Cells.Add(td)
            td.Controls.Add(lblAssociatedName)
            td = New TableCell()
            td.Width = 160
            td.HorizontalAlign = HorizontalAlign.Left
            'td.ColumnSpan = 2
            tbxAssociated = New TextBox()
            tbxAssociated.Width = 160
            tbxAssociated.MaxLength = 16
            'AM2010052001 - TESTING PAYMENT CREDENTIALS - START
            If AppSettings("EXPANDIT_US_USE_TESTING_PAYMENT_CREDENTIALS") Then
                tbxAssociated.Text = "EXPANDIT SOLUTIONS US"
                tbxAssociated.ReadOnly = True
            End If
            'AM2010052001 - TESTING PAYMENT CREDENTIALS - END
            tbxAssociated.CssClass = "borderTextBox"
            td.Controls.Add(tbxAssociated)
            tr.Cells.Add(td)
            table.Rows.Add(tr)

            'Eighth row
            'tr = New TableRow()
            'tr.Visible = False
            'td = New TableCell() 'table cell 1
            'td.Width = 130
            'td.HorizontalAlign = HorizontalAlign.Left
            'td.Style.Add(System.Web.UI.HtmlTextWriterStyle.VerticalAlign, "middle")
            'Dim lbltypeEcheck As New Label()
            'lbltypeEcheck.Text = "Type of Bank Account:"
            'tr.Cells.Add(td)
            'td.Controls.Add(lbltypeEcheck)
            'td = New TableCell() 'table cell 2
            'td.Width = 160
            'td.Style.Add(System.Web.UI.HtmlTextWriterStyle.PaddingLeft, "4px")
            'td.HorizontalAlign = HorizontalAlign.Left
            'typeEcheck = New DropDownList
            'typeEcheck.Width = 163
            'typeEcheck.ID = "DropDownList1"
            'typeEcheck.Style.Add(System.Web.UI.HtmlTextWriterStyle.FontFamily, "Tahoma")
            'typeEcheck.Style.Add(System.Web.UI.HtmlTextWriterStyle.Color, "#323233")
            'typeEcheck.Style.Add(System.Web.UI.HtmlTextWriterStyle.FontSize, "11px")
            'If CBoolEx(globals.User("Admin")) Then
            '    Dim tel = New ListItem("TEL", "TEL")
            '    typeEcheck.Items.Add(tel)
            'Else
            '    Dim web = New ListItem("WEB", "WEB")
            '    typeEcheck.Items.Add(web)
            'End If
            ''typeEcheck.AutoPostBack = True
            ''AddHandler typeEcheck.SelectedIndexChanged, AddressOf DropDownChange
            'td.Controls.Add(typeEcheck)
            'tr.Cells.Add(td)
            'table.Rows.Add(tr)

            ''Ninth
            'tr = New TableRow()
            'td = New TableCell()
            'td.Width = 120
            'td.HorizontalAlign = HorizontalAlign.Left
            'td.Style.Add(System.Web.UI.HtmlTextWriterStyle.VerticalAlign, "middle")
            'lblCheckNumber = New Label()
            'lblCheckNumber.ID = "lblcheck"
            'lblCheckNumber.Visible = False
            'lblCheckNumber.Text = "Check Number:"
            'tr.Cells.Add(td)
            'td.Controls.Add(lblCheckNumber)
            'td = New TableCell()
            ''td.Width = 140
            'td.HorizontalAlign = HorizontalAlign.Left
            ''td.ColumnSpan = 2
            'tbxCheckNumber = New TextBox()
            'tbxCheckNumber.ID = "txbcheck"
            'tbxCheckNumber.Visible = False
            'tbxCheckNumber.Width = 160
            'tbxCheckNumber.MaxLength = 16
            'tbxCheckNumber.CssClass = "borderTextBox"
            'td.Controls.Add(tbxCheckNumber)
            'tr.Cells.Add(td)
            'table.Rows.Add(tr)


            tr = New TableRow()
            td = New TableCell()
            clickBtn = New Button()
            clickBtn.Text = ButtonText
            clickBtn.CssClass = "AddButton"
            clickBtn.ID = "cbtn"
            AddHandler clickBtn.Click, AddressOf BtnAccept_Click

            td.Controls.Add(clickBtn)

            tr.Cells.Add(td)
            table.Rows.Add(tr)

        End Sub

        'Private Sub DropDownChange(ByVal sender As Object, ByVal e As EventArgs)


        '    If typeEcheck.SelectedValue = "ARC" Or typeEcheck.SelectedValue = "BOC" Then
        '        lblCheckNumber.Visible = True
        '        tbxCheckNumber.Visible = True
        '        clickBtn.Focus()
        '    Else
        '        lblCheckNumber.Visible = False
        '        tbxCheckNumber.Visible = False
        '        clickBtn.Focus()
        '    End If

        'End Sub

    End Class
    '<!--JA2010022601 - ECHECK - End-->
End Namespace

