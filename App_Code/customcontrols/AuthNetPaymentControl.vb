Imports Microsoft.VisualBasic
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Configuration.ConfigurationManager

Namespace ExpandIT.SimpleUIControls

    Public Class AuthNetPaymentControl
        Inherits GenericPaymentControl
        Implements INamingContainer

        Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
            If ShowAuthNetErrorMessage() <> "" Then
                Me.lblErrorText.Text = ShowAuthNetErrorMessage()
                Me.trError.Visible = True
            End If
            MyBase.Render(writer)
        End Sub

        Function ShowAuthNetErrorMessage() As String

            If HttpContext.Current.Request("AuthorizeNet_ErrorString") <> "" Then
                Return HttpContext.GetGlobalResourceObject("Language", "LABEL_AUTHORIZE_NET") & "&nbsp;" & HttpContext.GetGlobalResourceObject("Language", "LABEL_ERROR") & _
                    "<br />" & HttpContext.Current.Request("AuthorizeNet_ErrorString")
            End If

            Return ""

        End Function

    End Class

End Namespace