Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Web.HttpUtility
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports ExpandIT
Imports ExpandIT.EISClass
Imports GroupRadioButton.GroupRadioButton
'<!--JA2010022601 - ECHECK - Start-->
'<!--JA2010022602 - AUTHORIZENET - Start-->
Namespace ExpandIT.SimpleUIControls
    Public Class GenericPaymentControl
        Inherits CoreCustomControl
        Implements INamingContainer

        Public clickEvent As EventHandler
        Protected lblErrorText As Label
        Protected trError As TableRow
        Protected WithEvents clickBtn As Button

        'AM2010041301 - ENTERPRISE PAYMENT GATEWAY - Start
        Public cbxChangedEvent As EventHandler
        Protected WithEvents cbxClicked As CheckBox
        'AM2010041301 - ENTERPRISE PAYMENT GATEWAY - End


        Protected radio As GroupRadioButton.GroupRadioButton
        Protected LblText As Label 'Minor change from private to protected. Neccessary for Text Property to work on subclass
        Protected hdrText As Label 'Minor change from private to protected. Neccessary for Text Property to work on subclass

        <DefaultValue("335px")> _
        Public Property TableWidth() As Unit
            Get
                Return ViewState("tableWidth")
            End Get
            Set(ByVal value As Unit)
                ViewState("tableWidth") = value
            End Set
        End Property

        Public Property HeaderText() As String
            Get
                Return ViewState("headerText")
            End Get
            Set(ByVal value As String)
                ViewState("headerText") = value
            End Set
        End Property

        Public Property LabelText() As String
            Get
                Return ViewState("labelText")
            End Get
            Set(ByVal value As String)
                ViewState("labelText") = value
            End Set
        End Property

        Public Property ButtonText() As String
            Get
                Return ViewState("buttonText")
            End Get
            Set(ByVal value As String)
                ViewState("buttonText") = value
            End Set
        End Property

        'JA2010102801 - PAPERCHECK - Start
        Public Property HideComments() As Boolean
            Get
                Return ViewState("hideComments")
            End Get
            Set(ByVal value As Boolean)
                ViewState("hideComments") = value
            End Set
        End Property
        'JA2010102801 - PAPERCHECK - End


        Public Property TextPart2() As String
            Get
                Return ViewState("textPart2")
            End Get
            Set(ByVal value As String)
                ViewState("textPart2") = value
            End Set
        End Property

        Public Property RadioButtonMode() As Boolean
            Get
                If ViewState("radioButtonMode") IsNot Nothing Then
                    Return ViewState("radioButtonMode")
                Else
                    Return False
                End If
            End Get
            Set(ByVal value As Boolean)
                ViewState("radioButtonMode") = value
            End Set
        End Property

        Public Property RadioButtonLabelMargin() As Unit
            Get
                If ViewState("radioButtonLabelMargin") IsNot Nothing Then
                    Return ViewState("radioButtonLabelMargin")
                Else
                    Return Unit.Parse("10px")
                End If
            End Get
            Set(ByVal value As Unit)
                ViewState("radioButtonLabelMargin") = value
            End Set
        End Property

        Public Property RadioButtonChecked() As Boolean
            Get
                Return ViewState("RadioButtonChecked")
            End Get
            Set(ByVal value As Boolean)
                ViewState("RadioButtonChecked") = value
            End Set
        End Property

        Private Function specialFormatting(ByVal str1 As String, ByVal str2 As String) As String
            Return Replace(eis.GetLabel(str1), "%1", eis.GetLabel(str2))
        End Function

        Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
            MyBase.OnLoad(e)
        End Sub

        Protected Overrides Sub OnPreRender(ByVal e As System.EventArgs)
            MyBase.OnPreRender(e)
            Try
                If Not RadioButtonMode Then
                    clickBtn.Text = HttpUtility.HtmlDecode(eis.GetLabel(clickBtn.Text))
                Else
                    If Me.Page.IsPostBack Then
                        If Me.radio.Checked Then RaiseEvent MyEvent(Me, EventArgs.Empty)
                    Else
                        radio.Checked = RadioButtonChecked
                    End If
                End If
                If String.IsNullOrEmpty(TextPart2) Then
                    LblText.Text = HttpUtility.HtmlDecode(eis.GetLabel(LblText.Text))
                Else
                    LblText.Text = HttpUtility.HtmlDecode(specialFormatting(LblText.Text, TextPart2))
                End If
                hdrText.Text = HttpUtility.HtmlDecode(eis.GetLabel(hdrText.Text))
            Catch ex As Exception

            End Try
        End Sub

        Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
            If Me.Visible = False Then
                Me.Style.Add("display", "none")
            End If
            MyBase.Render(writer)
        End Sub

        Public Overrides Sub RenderBeginTag(ByVal writer As System.Web.UI.HtmlTextWriter)
            writer.Write("")
        End Sub

        Public Overrides Sub RenderEndTag(ByVal writer As System.Web.UI.HtmlTextWriter)
            writer.Write("")
        End Sub

        Protected Overrides Sub CreateChildControls()
            Dim obj As New ObjectTagBuilder()
            ' create the table
            Dim table As New Table()
            table.Width = TableWidth

            ' create the first row
            Dim tr As New TableRow()

            ' add a cell to the row
            Dim td As New TableCell()
            tr.Cells.Add(td)
            table.Rows.Add(tr)

            ' create the second row
            tr = New TableRow()

            ' add a header cell to the row
            Dim th As New TableHeaderCell()
            th.ColumnSpan = 2

            ' add a label to the header cell
            hdrText = New Label()
            hdrText.Text = HeaderText
            th.Controls.Add(hdrText)
            tr.Cells.Add(th)
            table.Rows.Add(tr)
            tr = New TableRow()
            td = New TableCell()
            tr.Cells.Add(td)
            table.Rows.Add(tr)
            ' Show Error Text
            trError = New TableRow()
            td = New TableCell()
            lblErrorText = New Label()
            lblErrorText.ForeColor = Drawing.Color.Red
            td.Controls.Add(lblErrorText)
            trError.Cells.Add(td)
            trError.Visible = False
            table.Rows.Add(trError)
            If RadioButtonMode Then
                radioMode(table)
            Else
                buttonMode(table)
            End If
            ' Add the table to the control's ControlCollection
            Me.Controls.Add(table)
        End Sub

        Protected Sub buttonMode(ByRef table As Table)
            ' Show Bread Text
            Dim tr As New TableRow()
            Dim td As New TableCell()
            LblText = New Label()
            LblText.Text = LabelText
            td.Controls.Add(LblText)
            tr.Cells.Add(td)
            table.Rows.Add(tr)
            tr = New TableRow()
            td = New TableCell()
            'Accept Button
            clickBtn = New Button()
            clickBtn.Text = ButtonText
            clickBtn.CssClass = "AddButton"
            clickBtn.ID = "cbtn"
            AddHandler clickBtn.Click, AddressOf BtnAccept_Click
            td.Controls.Add(clickBtn)

            tr.Cells.Add(td)
            table.Rows.Add(tr)

        End Sub


        Protected Sub radioMode(ByRef table As Table)

            Dim tr As New TableRow()
            Dim td As New TableCell()
            td.Width = RadioButtonLabelMargin
            radio = New GroupRadioButton.GroupRadioButton()
            radio.GroupName = "genericPaymentGroup"

            td.Controls.Add(radio)
            tr.Cells.Add(td)

            td = New TableCell()
            td.HorizontalAlign = HorizontalAlign.Left
            LblText = New Label()
            LblText.Text = LabelText
            td.Controls.Add(LblText)
            tr.Cells.Add(td)
            table.Rows.Add(tr)
        End Sub

        Public Event MyEvent(ByVal sender As Object, ByVal e As EventArgs)

        Public Sub MyEventHandler(ByVal sender As Object, ByVal e As EventArgs) Handles Me.MyEvent
            Dim ea As New ExpandIT.ExpEventArgs()
            ea.Text = Me.ID
            clickEvent(clickBtn, ea)
        End Sub
        ''' <summary>
        ''' Handles Click Event on BtnAccept          
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Public Sub BtnAccept_Click(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles clickBtn.Click
            Dim ea As New ExpandIT.ExpEventArgs()
            ea.Text = Me.ID
            clickEvent(clickBtn, ea)
        End Sub

        'AM2010041301 - ENTERPRISE PAYMENT GATEWAY - Start
        ''' <summary>
        ''' Handles CheckedChanged on cbxAddress and cbxZipCode         
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Public Sub cbxChanged(ByVal sender As Object, ByVal e As EventArgs)
            Dim ea As New ExpandIT.ExpEventArgs()
            ea.Text = Me.ID
            cbxClicked = CType(sender, CheckBox)
            Me.cbxChangedEvent(cbxClicked, ea)
        End Sub
        'AM2010041301 - ENTERPRISE PAYMENT GATEWAY - End

    End Class

    '<!--JA2010022602 - AUTHORIZENET - End-->
    '<!--JA2010022601 - ECHECK - End-->
End Namespace