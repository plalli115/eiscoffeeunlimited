Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports ExpandIT
Imports ExpandIT.EISClass

Namespace ExpandIT.SimpleUIControls

    Public Class CoreCustomControl
        Inherits CompositeControl
        Implements INamingContainer

        Public globals As GlobalsClass
        Public eis As EISClass
        '#080619 Changed here to make use of both currency classes (After Release)
        Public currency As CurrencyBaseClass
        Public store As StoreClass
        Public cart As CartClass
        Public b2b As B2BBaseClass

        Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)

            MyBase.OnLoad(e)
            Dim mypage As ExpandIT.Page = Me.Page
            If mypage Is Nothing Then
                globals = New GlobalsClass(True)
            Else
                globals = mypage.globals
            End If

            eis = globals.eis
            currency = globals.currency
            cart = globals.cart
            store = globals.store
            b2b = globals.b2b

        End Sub

    End Class

End Namespace
