﻿Imports Microsoft.VisualBasic
Imports ExpandIT

Namespace ExpandIT.SimpleUIControls

    ''' <summary>
    ''' Output with ID also in MasterPage, DataList; GridView etc.
    ''' </summary>
    ''' <remarks></remarks>
    Public Class ExpHiddenField
        Inherits HiddenField

        Public Overrides Sub RenderControl(ByVal writer As System.Web.UI.HtmlTextWriter)

            writer.AddAttribute(HtmlTextWriterAttribute.Type, "hidden")
            writer.AddAttribute(HtmlTextWriterAttribute.Id, Me.ClientID)
            writer.AddAttribute(HtmlTextWriterAttribute.Name, Me.ID)
            writer.AddAttribute(HtmlTextWriterAttribute.Value, Me.Value)
            writer.RenderBeginTag(HtmlTextWriterTag.Input)
            writer.RenderEndTag()

        End Sub

    End Class

End Namespace
