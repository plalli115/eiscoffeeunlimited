'JA2010102801 - PAPERCHECK - Start
Imports Microsoft.VisualBasic
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Configuration.ConfigurationManager

Namespace ExpandIT.SimpleUIControls

    Public Class PaperCheckPaymentControl
        Inherits GenericPaymentControl

        Private lblCheckDate As New Label
        Private tbxCheckDate As TextBox
        Protected validatorCheckDate As RequiredFieldValidator
        Private lblCheckNumber As Label
        Private tbxCheckNumber As TextBox
        Protected validatorCheckNumber As RequiredFieldValidator
        Private lblComment As Label
        Private tbxComment As TextBox
        Private lblCheckTotal As Label
        Private tbxCheckTotal As TextBox
        Protected validatorCheckTotal As RequiredFieldValidator
        Private lblBoxTrack As Label
        Private lblBoxTrackSpace As Label
        Private lblBoxTrackSeperator As Label
        Private tbxTrackValue1 As TextBox
        Protected validatorTrackValue1 As RequiredFieldValidator
        Private tbxTrackValue2 As TextBox
        Protected validatorTrackValue2 As RequiredFieldValidator
        Private tbxTrackDate As TextBox
        Protected validatorTrackDate As RequiredFieldValidator



        Private sender As New Object
        Private e As New System.EventArgs

        Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
            If ShowErrorMessage() <> "" Then
                Me.lblErrorText.Text = ShowErrorMessage()
                Me.trError.Visible = True
            End If
            MyBase.Render(writer)
        End Sub

        ''' <summary>
        ''' Gets the Check Date
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property CheckDate() As String
            Get
                Return tbxCheckDate.Text
            End Get
        End Property

        ''' <summary>
        ''' Gets the Check number on the customer's paper check. ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property CheckNumber() As String
            Get
                Return tbxCheckNumber.Text
            End Get
        End Property

        Public ReadOnly Property CheckTotals() As Double
            Get
                Return CDblEx(Regex.Replace(tbxCheckTotal.Text, "[^.0-9]", ""))
            End Get
        End Property

        Public ReadOnly Property BoxTrack1() As Integer
            Get
                Return CIntEx(Regex.Replace(tbxTrackValue1.Text, "[^.0-9]", ""))
            End Get
        End Property

        Public ReadOnly Property BoxTrack2() As Integer
            Get
                Return CIntEx(Regex.Replace(tbxTrackValue2.Text, "[^.0-9]", ""))
            End Get
        End Property

        Public ReadOnly Property BoxTrackDate() As Date
            Get
                Return CDateEx(Regex.Replace(tbxTrackDate.Text, "[^/0-9]", ""))
            End Get
        End Property

        Public ReadOnly Property Comment() As String
            Get
                Return CStrEx(tbxComment.Text)
            End Get
        End Property

        

        Protected Overrides Sub CreateChildControls()
            Dim obj As New ObjectTagBuilder()

            Dim mainTable As New Table()
            Dim mainTr As New TableRow()
            Dim mainTd1 As New TableCell()

            ' create the table
            Dim table As New Table()
            'table.Width = TableWidth
            'table.Width = "650"
            'table.BorderWidth = 1

            ' create the first row
            Dim tr As New TableRow()

            ' add a cell to the row
            Dim td As New TableCell()
            td.ColumnSpan = 2
            tr.Cells.Add(td)
            table.Rows.Add(tr)

            ' create the second row
            tr = New TableRow()

            ' add a header cell to the row
            Dim th As New TableHeaderCell()
            th.ColumnSpan = 2

            ' add a label to the header cell
            hdrText = New Label()
            hdrText.Text = HeaderText
            th.Controls.Add(hdrText)
            tr.Cells.Add(th)
            table.Rows.Add(tr)

            tr = New TableRow()
            td = New TableCell()
            td.ColumnSpan = 2
            tr.Cells.Add(td)
            table.Rows.Add(tr)
            ' Show Error Text
            trError = New TableRow()
            td = New TableCell()
            td.ColumnSpan = 2
            lblErrorText = New Label()
            lblErrorText.ForeColor = Drawing.Color.Red
            td.Controls.Add(lblErrorText)
            trError.Cells.Add(td)
            trError.Visible = False
            table.Rows.Add(trError)

            Dim mainTd2 As New TableCell()
            mainTd2.Controls.Add(table)
            mainTr.Cells.Add(mainTd2)
            mainTd1.Style.Add(System.Web.UI.HtmlTextWriterStyle.PaddingTop, "65px")
            mainTr.Cells.Add(mainTd1)
            mainTable.Rows.Add(mainTr)


            If RadioButtonMode Then
                radioMode(table)
            Else
                buttonMode(table)
            End If
            ' Add the table to the control's ControlCollection
            Me.Controls.Add(mainTable)
        End Sub

        Protected Overloads Sub buttonMode(ByRef table As Table)
            ' First row
            Dim tr As New TableRow()
            Dim td As New TableCell()
            td.ColumnSpan = 2
            LblText = New Label()
            LblText.Text = LabelText
            td.Controls.Add(LblText)
            tr.Cells.Add(td)
            table.Rows.Add(tr)

            ' Second row
            tr = New TableRow()
            td = New TableCell()
            td.ColumnSpan = 2
            Dim txt As New LiteralControl()
            txt.Text = "&nbsp;"
            td.Controls.Add(txt)
            tr.Cells.Add(td)
            table.Rows.Add(tr)

            'Fourth row
            tr = New TableRow()
            td = New TableCell()
            td.HorizontalAlign = HorizontalAlign.Left
            td.Style.Add(System.Web.UI.HtmlTextWriterStyle.VerticalAlign, "middle")
            lblCheckDate = New Label()
            lblCheckDate.Text = Resources.Language.PAPERCHECK_CHECK_DATE & ":"
            tr.Cells.Add(td)
            td.Controls.Add(lblCheckDate)
            td = New TableCell()
            td.HorizontalAlign = HorizontalAlign.Left
            td.Width = 440
            tbxCheckDate = New TextBox()
            tbxCheckDate.ID = "tbxCheckDate"
            tbxCheckDate.Width = 160
            tbxCheckDate.CssClass = "borderTextBox"
            td.Controls.Add(tbxCheckDate)

            Dim lblCheckDateFormat As New Label()
            lblCheckDateFormat.Text = " (MM/DD/YYYY)"
            td.Controls.Add(lblCheckDateFormat)

            validatorCheckDate = New RequiredFieldValidator
            validatorCheckDate.Text = "Required"
            validatorCheckDate.ControlToValidate = "tbxCheckDate"
            validatorCheckDate.ID = "CheckDateValidator"
            td.Controls.Add(validatorCheckDate)

            tr.Cells.Add(td)
            td = New TableCell()
            td.HorizontalAlign = HorizontalAlign.Left
            td.Style.Add(System.Web.UI.HtmlTextWriterStyle.VerticalAlign, "middle")
            td.Style.Add(System.Web.UI.HtmlTextWriterStyle.TextAlign, "left")
            td.Style.Add(System.Web.UI.HtmlTextWriterStyle.Color, "gray")
            table.Rows.Add(tr)

            'Fifth row
            tr = New TableRow()
            td = New TableCell()
            td.HorizontalAlign = HorizontalAlign.Left
            td.Style.Add(System.Web.UI.HtmlTextWriterStyle.VerticalAlign, "middle")
            Dim lblCheckNumber As New Label()
            lblCheckNumber.Text = Resources.Language.PAPERCHECK_CHECK_NUMBER & ":"
            tr.Cells.Add(td)
            td.Controls.Add(lblCheckNumber)
            td = New TableCell()
            td.Width = 160
            td.HorizontalAlign = HorizontalAlign.Left
            'td.ColumnSpan = 2
            tbxCheckNumber = New TextBox()
            tbxCheckNumber.Width = 160
            tbxCheckNumber.CssClass = "borderTextBox"
            tbxCheckNumber.ID = "tbxCheckNumber"
            td.Controls.Add(tbxCheckNumber)

            validatorCheckNumber = New RequiredFieldValidator
            validatorCheckNumber.Text = "Required"
            validatorCheckNumber.ControlToValidate = "tbxCheckNumber"
            validatorCheckNumber.ID = "CheckNumberValidator"
            td.Controls.Add(validatorCheckNumber)

            tr.Cells.Add(td)
            td = New TableCell()
            td.HorizontalAlign = HorizontalAlign.Left
            td.Style.Add(System.Web.UI.HtmlTextWriterStyle.VerticalAlign, "middle")
            tr.Cells.Add(td)
            table.Rows.Add(tr)

            'Sixth row
            tr = New TableRow()
            td = New TableCell()
            td.HorizontalAlign = HorizontalAlign.Left
            td.Style.Add(System.Web.UI.HtmlTextWriterStyle.VerticalAlign, "middle")
            lblCheckTotal = New Label()
            lblCheckTotal.Text = Resources.Language.PAPERCHECK_CHECKS_TOTAL & ":"
            td.Controls.Add(lblCheckTotal)
            tr.Cells.Add(td)
            td = New TableCell()
            td.Width = 160
            td.HorizontalAlign = HorizontalAlign.Left
            'td.ColumnSpan = 2
            tbxCheckTotal = New TextBox()
            tbxCheckTotal.Width = 100
            tbxCheckTotal.CssClass = "borderTextBox"
            tbxCheckTotal.ID = "tbxCheckTotal"
            td.Controls.Add(tbxCheckTotal)

            validatorCheckTotal = New RequiredFieldValidator
            validatorCheckTotal.Text = "Required"
            validatorCheckTotal.ControlToValidate = "tbxCheckTotal"
            validatorCheckTotal.ID = "CheckTotalValidator"
            td.Controls.Add(validatorCheckTotal)

            tr.Cells.Add(td)
            td = New TableCell()
            td.HorizontalAlign = HorizontalAlign.Left
            td.Style.Add(System.Web.UI.HtmlTextWriterStyle.VerticalAlign, "middle")
            tr.Cells.Add(td)
            table.Rows.Add(tr)

            'Seventh row
            tr = New TableRow()
            td = New TableCell()
            td.HorizontalAlign = HorizontalAlign.Left
            td.Style.Add(System.Web.UI.HtmlTextWriterStyle.VerticalAlign, "middle")
            lblBoxTrack = New Label()
            lblBoxTrack.Text = Resources.Language.PAPERCHECK_BOX_TRACKING & ":"
            td.Controls.Add(lblBoxTrack)
            tr.Cells.Add(td)
            td = New TableCell()
            td.HorizontalAlign = HorizontalAlign.Left
            td.Width = 440
            'td.ColumnSpan = 2
            lblBoxTrackSeperator = New Label()
            lblBoxTrackSeperator.Text = " \ "
            lblBoxTrackSpace = New Label()
            lblBoxTrackSpace.Text = " "
            tbxTrackValue1 = New TextBox()
            tbxTrackValue1.Width = 20
            tbxTrackValue1.CssClass = "borderTextBox"
            tbxTrackValue1.ID = "tbxTrackValue1"
            td.Controls.Add(tbxTrackValue1)
            tbxTrackValue2 = New TextBox()
            tbxTrackValue2.Width = 20
            tbxTrackValue2.CssClass = "borderTextBox"
            tbxTrackValue2.ID = "tbxTrackValue2"
            td.Controls.Add(lblBoxTrackSeperator)
            td.Controls.Add(tbxTrackValue2)
            td.Controls.Add(lblBoxTrackSpace)
            tbxTrackDate = New TextBox()
            tbxTrackDate.Width = 95
            tbxTrackDate.CssClass = "borderTextBox"
            tbxTrackDate.ID = "tbxTrackDate"
            td.Controls.Add(tbxTrackDate)

            lblCheckDateFormat = New Label()
            lblCheckDateFormat.Text = " (MM/DD/YYYY)"
            td.Controls.Add(lblCheckDateFormat)

            validatorTrackValue1 = New RequiredFieldValidator
            validatorTrackValue1.Text = "Required"
            validatorTrackValue1.ControlToValidate = "tbxTrackValue1"
            validatorTrackValue1.ID = "TrackValue1Validator"
            td.Controls.Add(validatorTrackValue1)

            validatorTrackValue2 = New RequiredFieldValidator
            validatorTrackValue2.Text = "Required"
            validatorTrackValue2.ControlToValidate = "tbxTrackValue2"
            validatorTrackValue2.ID = "TrackValue2Validator"
            td.Controls.Add(validatorTrackValue2)

            validatorTrackDate = New RequiredFieldValidator
            validatorTrackDate.Text = "Required"
            validatorTrackDate.ControlToValidate = "tbxTrackDate"
            validatorTrackDate.ID = "TrackDateValidator"
            td.Controls.Add(validatorTrackDate)

            tr.Cells.Add(td)
            td = New TableCell()
            td.HorizontalAlign = HorizontalAlign.Left
            td.Style.Add(System.Web.UI.HtmlTextWriterStyle.VerticalAlign, "middle")
            tr.Cells.Add(td)
            table.Rows.Add(tr)

            If Not HideComments Then
                'comment line
                tr = New TableRow()
                td = New TableCell()
                td.HorizontalAlign = HorizontalAlign.Left
                td.Style.Add(System.Web.UI.HtmlTextWriterStyle.VerticalAlign, "middle")
                lblComment = New Label()
                lblComment.Text = Resources.Language.LABEL_PAPERCHECK_COMMENT & ":"
                td.Controls.Add(lblComment)
                tr.Cells.Add(td)
                td = New TableCell()
                td.Width = 160
                td.HorizontalAlign = HorizontalAlign.Left
                tbxComment = New TextBox()
                tbxComment.MaxLength = 250
                tbxComment.Width = 160
                tbxComment.CssClass = "borderTextBox"
                td.Controls.Add(tbxComment)
                tr.Cells.Add(td)
                td = New TableCell()
                td.HorizontalAlign = HorizontalAlign.Left
                td.Style.Add(System.Web.UI.HtmlTextWriterStyle.VerticalAlign, "middle")
                tr.Cells.Add(td)
                table.Rows.Add(tr)
            End If



            tr = New TableRow()
            td = New TableCell()
            td.ColumnSpan = 2
            Dim txt2 As New LiteralControl()
            txt2.Text = "&nbsp;"
            td.Controls.Add(txt2)
            tr.Cells.Add(td)
            table.Rows.Add(tr)

            tr = New TableRow()
            td = New TableCell()
            td.ColumnSpan = 2
            clickBtn = New Button()
            clickBtn.Text = ButtonText
            clickBtn.ID = "cbtn"
            AddHandler clickBtn.Click, AddressOf BtnAccept_Click

            td.Controls.Add(clickBtn)

            tr.Cells.Add(td)
            table.Rows.Add(tr)

        End Sub

        Function ShowErrorMessage() As String

            If HttpContext.Current.Request("PaperCheck_ErrorString") <> "" Then
                Return HttpContext.GetGlobalResourceObject("Language", "LABEL_PAPERCHECK") & "&nbsp;" & HttpContext.GetGlobalResourceObject("Language", "LABEL_ERROR") & _
                    "<br />" & HttpContext.Current.Request("PaperCheck_ErrorString")
            End If

            Return ""

        End Function

    End Class
    'JA2010102801 - PAPERCHECK - End

End Namespace

