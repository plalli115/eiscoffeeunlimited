﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Web.UI.WebControls
Imports ExpandIT
Imports ExpandIT.EISClass

Namespace ExpandIT.SimpleUIControls
    ''' <summary>
    ''' Label control with special property LabelText
    ''' </summary>
    ''' <remarks></remarks>
    Public Class ExpLabel
        Inherits Label

            Public globals As GlobalsClass
            Public eis As EISClass
            Private m_labelText As String

            Public Property LabelText() As String
                Get
                    Dim str As String = ViewState(Me.UniqueID & "LabelText")
                    If Not String.IsNullOrEmpty(str) Then
                    Return ViewState(Me.UniqueID & "LabelText")
                    Else
                        str = m_labelText
                        If Not String.IsNullOrEmpty(str) Then
                            Return m_labelText
                        End If
                    End If
                    Return String.Empty
                End Get
                Set(ByVal value As String)
                    ViewState(Me.UniqueID & "LabelText") = value
                    m_labelText = value
                End Set
            End Property

            Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
                MyBase.OnLoad(e)
                Dim mypage As ExpandIT.Page = Me.Page
                If mypage Is Nothing Then
                    globals = New GlobalsClass(True)
                Else
                    globals = mypage.globals
                End If

                eis = globals.eis

            End Sub

            Protected Overrides Sub OnPreRender(ByVal e As System.EventArgs)
                MyBase.OnPreRender(e)
                If Not String.IsNullOrEmpty(LabelText) Then
                    Me.Text = HttpContext.GetGlobalResourceObject("Language", LabelText)
                End If
            End Sub

    End Class
End Namespace
