Imports Microsoft.VisualBasic
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Configuration.ConfigurationManager

Namespace ExpandIT.SimpleUIControls
    Public Class PayPalPaymentControl
        Inherits GenericPaymentControl
        Implements INamingContainer

        Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
            If ShowPPErrorMessage() <> "" Then
                Me.lblErrorText.Text = ShowPPErrorMessage()
                Me.trError.Visible = True
            End If
            MyBase.Render(writer)
        End Sub

        Function ShowPPErrorMessage() As String

            If HttpContext.Current.Request("ppreason") = "paypalcancel" Then
                Return HttpContext.GetGlobalResourceObject("Language", "ERROR_PAYMENT_CANCELLED_BY_USER")
            End If

            Return ""

        End Function

    End Class
End Namespace

