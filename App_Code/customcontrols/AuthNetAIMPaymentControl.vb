Imports Microsoft.VisualBasic
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Configuration.ConfigurationManager

Namespace ExpandIT.SimpleUIControls
    '<!--JA2010022602 - AUTHORIZENET - Start-->

    ''' <summary>
    ''' This is the new AuthNetAIMPaymentControl that inherits from AuthNetPaymentControl
    ''' The extended functionality involves three new properties that handles creditcard
    ''' information.
    ''' The class overrides it's baseclass CreateChildControls() method, and overloads the baseclass's 
    ''' buttonMode method.
    ''' The purpouses of these overrides/overloads is to create a slightly different user interface,
    ''' providing three textboxes to collect creditcard information from the customer.                    
    ''' </summary>
    ''' <remarks></remarks>
    Public Class AuthNetAIMPaymentControl
        Inherits AuthNetPaymentControl

        Private tbxCCN As TextBox
        Private tbxExpDate As TextBox
        Private tbxCCV As TextBox

        ''' <summary>
        ''' Get the creditcard number
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property CCN() As String
            Get
                Return tbxCCN.Text
            End Get
        End Property

        ''' <summary>
        ''' Get the expiring date
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property EXPDATE() As String
            Get
                Return tbxExpDate.Text
            End Get
        End Property

        ''' <summary>
        ''' Retreive the CCV
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property CCV() As String
            Get
                Return tbxCCV.Text
            End Get
        End Property


        Protected Overrides Sub CreateChildControls()
            Dim obj As New ObjectTagBuilder()
            ' create the table
            Dim table As New Table()
            table.Width = TableWidth

            ' create the first row
            Dim tr As New TableRow()

            ' add a cell to the row
            Dim td As New TableCell()
            td.ColumnSpan = 3
            tr.Cells.Add(td)
            table.Rows.Add(tr)

            ' create the second row
            tr = New TableRow()

            ' add a header cell to the row
            Dim th As New TableHeaderCell()
            th.ColumnSpan = 3

            ' add a label to the header cell
            hdrText = New Label()
            hdrText.Text = HeaderText
            th.Controls.Add(hdrText)
            tr.Cells.Add(th)
            table.Rows.Add(tr)

            tr = New TableRow()
            td = New TableCell()
            td.ColumnSpan = 3
            tr.Cells.Add(td)
            table.Rows.Add(tr)
            ' Show Error Text
            trError = New TableRow()
            td = New TableCell()
            td.ColumnSpan = 3
            lblErrorText = New Label()
            lblErrorText.ForeColor = Drawing.Color.Red
            td.Controls.Add(lblErrorText)
            trError.Cells.Add(td)
            trError.Visible = False
            table.Rows.Add(trError)
            If RadioButtonMode Then
                radioMode(table)
            Else
                buttonMode(table)
            End If
            ' Add the table to the control's ControlCollection
            Me.Controls.Add(table)
        End Sub

        Protected Overloads Sub buttonMode(ByRef table As Table)
            ' First row
            Dim tr As New TableRow()
            Dim td As New TableCell()
            td.ColumnSpan = 3
            LblText = New Label()
            LblText.Text = LabelText
            td.Controls.Add(LblText)
            tr.Cells.Add(td)
            table.Rows.Add(tr)

            ' Second row
            tr = New TableRow()
            td = New TableCell()
            td.ColumnSpan = 3
            Dim txt As New LiteralControl()
            txt.Text = "&nbsp;"
            td.Controls.Add(txt)
            tr.Cells.Add(td)
            table.Rows.Add(tr)

            'AM2010052001 - TESTING PAYMENT CREDENTIALS - START
            If AppSettings("EXPANDIT_US_USE_TESTING_PAYMENT_CREDENTIALS") Then
                tr = New TableRow()
                td = New TableCell()
                td.ColumnSpan = 3
                td.ForeColor = Drawing.Color.Red
                td.Font.Bold = True
                Dim txtTest As New LiteralControl()
                txtTest.Text = "The credit card information has been filled out for testing purposes."
                td.Controls.Add(txtTest)
                tr.Cells.Add(td)
                table.Rows.Add(tr)
            End If
            'AM2010052001 - TESTING PAYMENT CREDENTIALS - END

            ' Third row
            tr = New TableRow()
            td = New TableCell()
            td.Width = 120
            td.HorizontalAlign = HorizontalAlign.Left
            Dim lblCCNR As New Label()
            lblCCNR.Text = "Credit Card Number:"
            tr.Cells.Add(td)
            td.Controls.Add(lblCCNR)
            td = New TableCell()
            td.Style.Add(System.Web.UI.HtmlTextWriterStyle.PaddingRight, "180px")
            'td.Width = 140
            td.HorizontalAlign = HorizontalAlign.Left
            'td.ColumnSpan = 2
            tbxCCN = New TextBox()
            tbxCCN.Width = 135
            tbxCCN.MaxLength = 16
            tbxCCN.CssClass = "borderTextBox"
            'AM2010052001 - TESTING PAYMENT CREDENTIALS - START
            If AppSettings("EXPANDIT_US_USE_TESTING_PAYMENT_CREDENTIALS") Then
                tbxCCN.Text = "370000000000002"
                tbxCCN.ReadOnly = True
            End If
            'AM2010052001 - TESTING PAYMENT CREDENTIALS - END
            td.Controls.Add(tbxCCN)
            tr.Cells.Add(td)
            table.Rows.Add(tr)

            'Fourth row
            tr = New TableRow()
            td = New TableCell()
            td.Width = 120
            td.HorizontalAlign = HorizontalAlign.Left
            Dim lblExpDate As New Label()
            lblExpDate.Text = "Expiration Date(MMYY):"
            tr.Cells.Add(td)
            td.Controls.Add(lblExpDate)
            td = New TableCell()
            'td.Width = 140
            td.HorizontalAlign = HorizontalAlign.Left
            'td.ColumnSpan = 2
            tbxExpDate = New TextBox()
            tbxExpDate.Width = 50
            tbxExpDate.MaxLength = 4
            tbxExpDate.CssClass = "borderTextBox"
            'AM2010052001 - TESTING PAYMENT CREDENTIALS - START
            If AppSettings("EXPANDIT_US_USE_TESTING_PAYMENT_CREDENTIALS") Then
                tbxExpDate.Text = "1211"
                tbxExpDate.ReadOnly = True
            End If
            'AM2010052001 - TESTING PAYMENT CREDENTIALS - END
            td.Controls.Add(tbxExpDate)
            tr.Cells.Add(td)
            table.Rows.Add(tr)

            'Fifth(row)
            tr = New TableRow()
            td = New TableCell() 'table cell 1
            td.Width = 120
            td.HorizontalAlign = HorizontalAlign.Left
            Dim lblCCV As New Label()
            lblCCV.Text = "CVV2:"
            tr.Cells.Add(td)
            td.Controls.Add(lblCCV)
            td = New TableCell() 'table cell 2
            'td.Width = 140
            td.HorizontalAlign = HorizontalAlign.Left
            tbxCCV = New TextBox()
            tbxCCV.Width = 35
            tbxCCV.MaxLength = 4
            'AM2010052001 - TESTING PAYMENT CREDENTIALS - START
            If AppSettings("EXPANDIT_US_USE_TESTING_PAYMENT_CREDENTIALS") Then
                tbxCCV.Text = "1234"
                tbxCCV.ReadOnly = True
            End If
            'AM2010052001 - TESTING PAYMENT CREDENTIALS - END
            tbxCCV.CssClass = "borderTextBox"
            td.Controls.Add(tbxCCV)
            tr.Cells.Add(td)
            table.Rows.Add(tr)

            tr = New TableRow()
            td = New TableCell()


            clickBtn = New Button()
            clickBtn.Text = ButtonText
            clickBtn.CssClass = "AddButton"
            clickBtn.ID = "cbtn"
            AddHandler clickBtn.Click, AddressOf BtnAccept_Click

            td.Controls.Add(clickBtn)

            tr.Cells.Add(td)
            table.Rows.Add(tr)

        End Sub


    End Class
    '<!--JA2010022602 - AUTHORIZENET - End-->
End Namespace

