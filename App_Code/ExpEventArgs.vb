Imports Microsoft.VisualBasic

Namespace ExpandIT
    Public Class ExpEventArgs
        Inherits System.EventArgs

        Private strText As String

        Public Property Text() As String
            Get
                Return strText
            End Get
            Set(ByVal value As String)
                strText = value
            End Set
        End Property

    End Class
End Namespace