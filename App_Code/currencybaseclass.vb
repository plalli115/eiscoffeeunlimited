Imports System.Configuration.ConfigurationManager
Imports Microsoft.VisualBasic
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Data.SqlClient
Imports System.Data

Namespace ExpandIT

    Public MustInherit Class CurrencyBaseClass

        Protected globals As GlobalsClass

        Public Sub New(ByVal _globals As GlobalsClass)
            globals = _globals
        End Sub

        ' --
        ' Exposed functions
        ' --
        Public MustOverride Function ConvertCurrency(ByVal InputAmount As Object, ByVal InputCurrency As String, ByVal OutputCurrency As String) As Object

        Public MustOverride Function GetExchangeFactor(ByVal FromCurrency As String, ByVal ToCurrency As String) As Decimal

        Public Overridable Function GetIso4217Code(ByVal aCurrency As Object) As String
            Dim dt, dt2 As DataTable
            Dim retv As String

            If IsNull(aCurrency) Then aCurrency = AppSettings("MULTICURRENCY_SITE_CURRENCY")
            If aCurrency = "" Then aCurrency = AppSettings("MULTICURRENCY_SITE_CURRENCY")
            dt = SQL2DataTable("SELECT Iso4217 FROM CurrencyIso4217 WHERE CurrencyGuid=" & SafeString(aCurrency))
            If dt.Rows.Count > 0 Then
                retv = CStrEx(dt.Rows(0)("Iso4217"))
            Else
                ' Unable to retrieve the Uso4217 code for the currency - use the default, as this is being displyed and is expected on the page.
                aCurrency = AppSettings("MULTICURRENCY_SITE_CURRENCY")
                dt2 = SQL2DataTable("SELECT Iso4217 FROM CurrencyIso4217 WHERE CurrencyGuid=" & SafeString(aCurrency))
                If dt2.Rows.Count > 0 Then
                    retv = CStrEx(dt2.Rows(0)("Iso4217"))
                Else
                    Throw New Exception("ExpandIT Multicurrency Module", "Unable to retrieve the Iso4217 code for currency [" & aCurrency & "]")
                End If
            End If
            Return retv
        End Function

    End Class

End Namespace
