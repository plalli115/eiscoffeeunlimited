﻿Imports ExpandIT
Imports ExpandIT.ExpandITLib
Imports Microsoft.VisualBasic
Imports System.Configuration.ConfigurationManager
Imports System.Collections.Generic
Imports System.Data

Public Class ShippingPaymentLogic

    Private shippingColumnPrefix As String = "ShipTo"
    Private _SP_Columns As String() = {}
    Private _paymentMethod As Object
    Private _userStatus As String
    Private _EisClass As EISClass
    '*****shipping*****-start
    Private _shipping As New liveShipping()
    '*****shipping*****-end
    Dim pageobj As ExpandIT.Page = CType(System.Web.HttpContext.Current.Handler, ExpandIT.Page)
    'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
    Private _csrObj As New USCSR(pageobj.globals)
    'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
    'AM2011032201 - RECURRING ORDERS STAND ALONE - START
    Private _recurringObj As New USRecurringOrders(pageobj.globals)
    'AM2011032201 - RECURRING ORDERS STAND ALONE - END

    Sub New(ByVal globalEisClass As EISClass)
        _SP_Columns = GetColumns(shippingColumnPrefix)
        _EisClass = globalEisClass
    End Sub

#Region "ShippingMethodPage"

    ''' <summary>
    ''' Gets the currently selected PaymentMethod value from CartHeader
    ''' Sets the PaymentMethod value in CartHeader         
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property CustomerPaymentMethod() As Object
        Get
            If _paymentMethod Is Nothing Then
                _paymentMethod = getSingleValueDB("SELECT PaymentMethod FROM CartHeader Where UserGuid = " & SafeString(HttpContext.Current.Session("UserGuid")) & " AND MultiCartStatus = 'ACTIVE'")
            End If
            Return _paymentMethod
        End Get
        Set(ByVal value As Object)
            excecuteNonQueryDb("Update CartHeader Set PaymentMethod = " & SafeString(value, True) & " Where UserGuid = " & SafeString(HttpContext.Current.Session("UserGuid")) & " AND MultiCartStatus = 'ACTIVE'")
        End Set
    End Property

    ''' <summary>
    ''' Gets the currently selected ShippingHandlingProviderGuid from CartHeader
    ''' Sets the ShippingHandlingProviderGuid value in CartHeader    
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property CustomerShippingProvider() As Object
        Get
            Dim retval As Object = Nothing
            Try
                retval = getSingleValueDB("SELECT ShippingHandlingProviderGuid FROM CartHeader Where UserGuid = " & SafeString(HttpContext.Current.Session("UserGuid")))
            Catch ex As Exception

            End Try
            Return CStrEx(retval)
        End Get
        Set(ByVal value As Object)
            Try
                excecuteNonQueryDb("Update CartHeader Set ShippingHandlingProviderGuid = " & SafeString(value) & " Where UserGuid = " & SafeString(HttpContext.Current.Session("UserGuid")))
            Catch ex As Exception

            End Try
        End Set
    End Property

    ''' <summary>
    ''' Gets the ProviderName from ShippingHandlingProvider
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CustomerShippingProviderName() As String
        Get
            Dim shGuid As Object = CustomerShippingProvider()
            '*****shipping*****-start
            If AppSettings("EXPANDIT_US_USE_SHIPPING") Then
                Return _shipping.customerShippingProviderName(shGuid)
            Else
                '*****shipping*****-end
                If shGuid IsNot Nothing AndAlso Not shGuid.Equals(DBNull.Value) Then
                    Return getSingleValueDB("SELECT ProviderName FROM ShippingHandlingProvider WHERE ShippingHandlingProviderGuid = " & SafeString(shGuid) & " AND LanguageGuid = " & SafeString(HttpContext.Current.Session("LanguageGuid").ToString))
                End If
                Return Nothing
            End If
        End Get
    End Property

    ''' <summary>
    ''' This function returns a SQL-Statement that will return all
    ''' possible ShippingProviderNames and ShippingProviderGuids for the current user.
    ''' It's based on user status and user laguage, and also all possible payment alternatives 
    ''' for the current user. Finally this is combined
    ''' with valid combinations of shippingProviders and PaymentMethods               
    ''' </summary>
    ''' <param name="isAppendConstraint"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function PossibleShippingProvidersSQLString(ByVal isAppendConstraint As Boolean) As String

        determineUserStatus()
        '*****shipping*****-start
        If AppSettings("EXPANDIT_US_USE_SHIPPING") Then
            Return _shipping.PossibleShippingProvidersSQLString(isAppendConstraint, _userStatus)
        Else
            '*****shipping*****-end
            Dim sql As New StringBuilder()
            'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
            If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
                Return _csrObj.PossibleShippingProvidersSQLString(isAppendConstraint, _userStatus)
            Else
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
                ' Find out if there is data in the PaymentType_ShippingHandlingProvider table
                ' If there is no data in the PaymentType_ShippingHandlingProvider table this means 
                ' that no relations are defined on valid combinations of payment types and shipping handling providers
                Dim o As Object = getSingleValueDB("SELECT TOP 1 ShippingProviderGuid FROM PaymentType_ShippingHandlingProvider")

                If o IsNot Nothing Then
                    ' We have a relationship between payment types and shipping handling providers
                    sql.Append("SELECT ShippingHandlingProviderGuid, ProviderName FROM ShippingHandlingProvider " & _
                                    "WHERE ShippingHandlingProviderGuid IN(SELECT DISTINCT ShippingProviderGuid FROM PaymentType_ShippingHandlingProvider " & _
                                        "WHERE PaymentType IN(SELECT PaymentType FROM PaymentType WHERE UserType = '" & _userStatus & "')) ")
                    If isAppendConstraint Then
                        sql.Append(" AND ShippingHandlingProviderGuid NOT IN ('NONE') ORDER BY SortIndex")
                    End If
                Else
                    ' We don't have any relationships between payment types and shipping handling providers,
                    ' so we just want to get all of the providers in the ShippingHandlingProvider table
                    sql.Append("SELECT ShippingHandlingProviderGuid, ProviderName FROM ShippingHandlingProvider ")
                End If
                Return sql.ToString()
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
            End If
            'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
        End If
    End Function

    ''' <summary>
    ''' This function returns a SQL-Statement that will return all possible
    ''' PaymentTypes for the current user.    
    ''' It's based on user status combined with valid combinations of ShippingHandlingproviders            
    ''' </summary>
    ''' <param name="isAppendConstraint"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function PossiblePaymentTypesSQLString(Optional ByVal isAppendConstraint As Boolean = False) As String

        determineUserStatus()
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
        If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
            Return _csrObj.PossiblePaymentTypesSQLString(_userStatus, CustomerShippingProvider(), isAppendConstraint)
        Else
            'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
            Dim sql As New StringBuilder()

            ' Find out if there is data in the PaymentType_ShippingHandlingProvider table
            ' If there is no data in the PaymentType_ShippingHandlingProvider table this means 
            ' that no relations are defined on valid combinations of payment types and shipping handling providers
            Dim o As Object = getSingleValueDB("SELECT TOP 1 ShippingProviderGuid FROM PaymentType_ShippingHandlingProvider")
            If o IsNot Nothing Then
                ' We have a relationship between payment types and shipping handling providers
                If IsSQL2005 Then
                    ' Use the newer SQL 2005 syntax
                    sql.Append("SELECT PaymentType FROM PaymentType WHERE UserType = '" & _userStatus & "'")
                    sql.Append(" INTERSECT SELECT PaymentType FROM PaymentType_ShippingHandlingProvider WHERE ")
                    If isAppendConstraint Then
                        sql.Append(" ShippingProviderGuid NOT IN ('NONE')")
                    Else
                        sql.Append(" ShippingProviderGuid = " & SafeString(CustomerShippingProvider))
                    End If
                Else
                    ' Use the older SQL 2000 syntax
                    sql.Append("SELECT PaymentType FROM PaymentType p WHERE UserType = '" & _userStatus & "'")
                    sql.Append(" AND EXISTS")
                    sql.Append(" (SELECT PaymentType FROM PaymentType_ShippingHandlingProvider ps WHERE ")
                    If isAppendConstraint Then
                        sql.Append(" ShippingProviderGuid NOT IN ('NONE')")
                    Else
                        sql.Append(" ShippingProviderGuid = " & SafeString(CustomerShippingProvider))
                    End If
                    sql.Append(" AND p.PaymentType = ps.PaymentType )")
                End If
            Else
                ' We don't have any relationships between payment types and shipping handling providers,
                ' so we just want to get all of the payment types in the PaymentType table
                sql.Append("SELECT PaymentType FROM PaymentType WHERE UserType = '" & _userStatus & "'")
            End If
            Return sql.ToString()
            'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
        End If
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - End

    End Function

    ''' <summary>
    ''' Finds the first possible PaymentType option for the current user
    ''' based on user status combined with valid combinations of ShippingHandlingproviders        
    ''' </summary>
    ''' <param name="addConstraint"></param>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property FirstPossiblePaymentType(ByVal addConstraint As Boolean) As String
        Get
            Dim firstProvider As String = String.Empty
            Dim dt As DataTable
            'JA2010092201 - RECURRING ORDERS - START
            If CBoolEx(AppSettings("RECURRING_ORDERS_SHOW")) AndAlso CBoolEx(AppSettings("RECURRING_ORDERS_REQUIRE_CREDIT_CARD")) Then
                If CBoolEx(pageobj.globals.OrderDict("RecurringOrder")) Then
                    dt = SQL2DataTable(_recurringObj.PossiblePaymentTypesSQLString())
                Else
                    dt = SQL2DataTable(PossiblePaymentTypesSQLString(addConstraint))
                End If
            Else
                dt = SQL2DataTable(PossiblePaymentTypesSQLString(addConstraint))
            End If
            'JA2010092201 - RECURRING ORDERS - END

            If dt.Rows.Count > 0 Then
                firstProvider = dt.Rows(0)(0)
            End If
            Return firstProvider
        End Get
    End Property

    ''' <summary>
    ''' Finds the first possible ShippingProvider option for the current user
    ''' based on user status and possible payment alternatives, combined
    ''' with valid combinations of shippingProviders and PaymentMethods              
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property FirstPossibleShippingProvider(Optional ByVal addConstraint As Boolean = False) As String
        'Check for default shipping provider guid in web.config and then compare the sanity of that value.
        Get
            Dim userType As String = determineUserStatus()
            Dim appSettingString As String = "DEFAULT_SHIPPINGPROVIDER_" & UCase(userType)
            Dim preferredProvider As String = CStrEx(AppSettings(appSettingString))
            Dim firstProvider As String = String.Empty
            Dim dt As DataTable = SQL2DataTable(PossibleShippingProvidersSQLString(addConstraint))
            If dt.Rows.Count > 0 Then
                'TEST
                If dt.Select("[ShippingHandlingProviderGuid]='" & preferredProvider & "'").Length > 0 Then
                    firstProvider = preferredProvider
                Else
                    firstProvider = dt.Rows(0)(0)
                End If
                'END TEST
            End If
            Return firstProvider
        End Get
    End Property

    ''' <summary>
    ''' Finds all the possible ShippingProviders for the current user
    ''' based on user status and possible payment alternatives, combined
    ''' with valid combinations of shippingProviders and PaymentMethods              
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property AllPossibleShippingProviders() As ArrayList
        Get
            Dim allProviders As New ArrayList
            Dim dt As DataTable = SQL2DataTable(PossibleShippingProvidersSQLString(False))
            For Each row As DataRow In dt.Rows
                allProviders.Add(row(0))
            Next
            Return allProviders
        End Get
    End Property

    ''' <summary>
    ''' Returns the current userstatus
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function determineUserStatus() As String
        ' Check User Status
        If _userStatus Is Nothing Then
            _userStatus = _EisClass.determineUserStatus()
        End If
        Return _userStatus
    End Function

    '''' <summary>
    '''' This property uses the internal class PathFinder to calculate the next url
    '''' based on the current url, combined with the rules concerning valid payment
    '''' and shippingprovider combimations defined in the DB       
    '''' </summary>
    '''' <value></value>
    '''' <returns></returns>
    '''' <remarks></remarks>
    'Public ReadOnly Property NextUrl() As String
    '    Get
    '        Dim pFinder As PathFinder = New PathFinder(Me)
    '        Return pFinder.nextUrl
    '    End Get
    'End Property

    ''AM2010080301 - CHECK OUT PROCESS MAP - Start
    'Public ReadOnly Property CheckOutPaths() As String()
    '    Get
    '        Dim pFinder As PathFinder = New PathFinder(Me)
    '        Return pFinder.PathsArray
    '    End Get
    'End Property

    'Public ReadOnly Property getCurrentLocation() As String
    '    Get
    '        Dim pFinder As PathFinder = New PathFinder(Me)
    '        Return pFinder.getLocation()
    '    End Get
    'End Property
    ''AM2010080301 - CHECK OUT PROCESS MAP - End

#End Region

#Region "PaymentMethodPage"

    Public Property CartShippingAddress() As ExpDictionary
        Get
            Return GetCartAddress(_SP_Columns(1))
        End Get
        Set(ByVal value As ExpDictionary)
            SetCartAddress(value, shippingColumnPrefix)
        End Set
    End Property

    Public Property CartPaymentAddress() As ExpDictionary
        Get
            Return GetCartAddress(_SP_Columns(0))
        End Get
        Set(ByVal value As ExpDictionary)
            SetCartAddress(value, "")
        End Set
    End Property

    Private Function SetCartAddress(ByVal Exp As ExpDictionary, ByVal format As String) As ExpDictionary
        Dim updateDict As New ExpDictionary()

        For Each item As KeyValuePair(Of Object, Object) In Exp
            If item.Key <> "UserGuid" Then
                updateDict(String.Concat(format, item.Key)) = item.Value
            Else
                updateDict("UserGuid") = HttpContext.Current.Session("UserGuid")
            End If
        Next

        Try
            dict2UpdateSQL(updateDict, "CartHeader", "MultiCartStatus", "ACTIVE", "UserGuid", HttpContext.Current.Session("UserGuid"))
        Catch ex As Exception

        End Try

        Return updateDict

    End Function

    ''' <summary>
    ''' Find all column names in the CartHeader table that are related to shipping and payment
    ''' </summary>
    ''' <param name="format"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetColumns(ByVal format As String) As String()

        Dim dt As DataTable
        Dim sb1 As New StringBuilder()
        Dim sb2 As New StringBuilder()
        dt = getDataTable("SELECT TOP 1 * FROM CartHeader")
        For Each row As DataRow In dt.Rows
            For i As Integer = 0 To dt.Columns.Count - 1
                If row.Table.Columns(i).ColumnName.Contains(format) Then
                    '<!-- ' ExpandIT US - ANA - US Sales Tax Shipping - Start -->
                    If row.Table.Columns(i).ColumnName <> "ShipToTaxAreaCode" And row.Table.Columns(i).ColumnName <> "ShipToTaxLiable" And row.Table.Columns(i).ColumnName <> "ShipToShippingAddressCode" Then
                        sb1.Append(row.Table.Columns(i).ColumnName.ToString().Remove(0, format.Length))
                        sb1.Append(", ")
                    End If
                    If AppSettings("EXPANDIT_US_USE_US_SALES_TAX") Then
                        sb2.Append(row.Table.Columns(i).ColumnName.ToString())
                        sb2.Append(", ")
                    Else
                        If row.Table.Columns(i).ColumnName <> "ShipToTaxAreaCode" And row.Table.Columns(i).ColumnName <> "ShipToTaxLiable" Then
                            sb2.Append(row.Table.Columns(i).ColumnName.ToString())
                            sb2.Append(", ")
                        End If
                    End If
                    '<!-- ' ExpandIT US - ANA - US Sales Tax Shipping - End -->
                End If
            Next
        Next

        If sb1.Length > 0 Then sb1.Remove(sb1.Length - 2, 2)
        If sb2.Length > 0 Then sb2.Remove(sb2.Length - 2, 2)

        Dim SqlArray As String() = {sb1.ToString(), sb2.ToString()}
        Return SqlArray

    End Function

    Private Function GetCartAddress(ByVal sqlColumns As String) As ExpDictionary
        Dim dt As DataTable
        Dim returnDict As New ExpDictionary()
        Dim dbNulls As Integer = 0

        dt = getDataTable("SELECT " & sqlColumns & " FROM CartHeader WHERE UserGuid = " & _
        SafeString(HttpContext.Current.Session("UserGuid")) & " AND MultiCartStatus='ACTIVE'")
        If Not dt.Rows.Count = 0 Then
            For Each row As DataRow In dt.Rows
                For i As Integer = 0 To dt.Columns.Count - 1
                    Dim key As String = row.Table.Columns(i).ColumnName.ToString()
                    If row.Item(i).GetType.Name.Equals("DBNull") Then
                        dbNulls += 1
                        returnDict(key) = ""
                    Else
                        returnDict(key) = row.Item(i)
                    End If
                    If row.Table.Columns(i).ColumnName.Contains("UserGuid") Then
                        returnDict(row.Table.Columns(i).ColumnName) = row.Item(i)
                    End If
                Next
            Next

            If dbNulls < returnDict.Count - 1 Then
                Return returnDict
            End If
        End If
        Return Nothing
    End Function

#End Region

End Class
