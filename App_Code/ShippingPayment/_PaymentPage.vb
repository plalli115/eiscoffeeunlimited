Imports System.Configuration.ConfigurationManager
Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports ExpandIT
Imports ExpandIT.ExpandITLib
Imports ExpandIT.EISClass
Imports ExpandIT.ShippingPayment

Namespace ExpandIT.ShippingPayment
    Public Class _PaymentPage
        Inherits ShippingPaymentPage

        ''' <summary>
        ''' Overload used when no fee is calculated
        ''' </summary>
        ''' <param name="paymentType"></param>
        ''' <param name="transact"></param>
        ''' <param name="PaymentTransactionSignature"></param>
        ''' <param name="bRedirect"></param>
        ''' <param name="bMail"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Function processAutomatedTransaction(ByVal paymentType As String, ByVal transact As String, ByVal PaymentTransactionSignature As String, _
                ByVal bRedirect As Boolean, ByRef bMail As Boolean, ByVal verificationStatus As String) As String
            Return processAutomatedTransaction(paymentType, transact, PaymentTransactionSignature, _
                0, bRedirect, bMail, verificationStatus)
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="paymentType"></param>
        ''' <param name="transact"></param>
        ''' <param name="PaymentTransactionSignature"></param>
        ''' <param name="paymentFee"></param>
        ''' <param name="bRedirect"></param>
        ''' <param name="bMail"></param>
        ''' <returns>HeaderGuid in ShopSalesHeader on success, or empty sting on failure</returns>
        ''' <remarks></remarks>
        Protected Function processAutomatedTransaction(ByVal paymentType As String, ByVal transact As String, ByVal PaymentTransactionSignature As String, _
                ByVal paymentFee As Decimal, ByVal bRedirect As Boolean, ByRef bMail As Boolean, ByVal verificationStatus As String) As String
            Dim CartHeaderGuid, HeaderGuid As String
            Dim Shipping As ExpDictionary = Nothing
            Dim Payment As ExpDictionary = Nothing
            Dim BillTo As ExpDictionary = Nothing
            Dim ShipTo As ExpDictionary = Nothing
            Dim aKey As KeyValuePair(Of Object, Object)
            Dim paymentFeeInclTax As Decimal
            Dim bUseShippingAddress As Boolean = False

            ' Get CartHeaderGuid
            CartHeaderGuid = globals.OrderDict("HeaderGuid")
            ' Get shipping address
            If (globals.User("B2C") And AppSettings("USE_SHIPPING_ADDRESS")) Or (globals.User("B2B") And AppSettings("B2B_USE_SHIPPING_ADDRESS")) Or globals.User("Anonymous") Then
                Shipping = CartShippingAddress
                Payment = CartPaymentAddress
                bUseShippingAddress = True
                If Shipping Is Nothing Then
                    If bRedirect Then
                        Response.Redirect(VRoot & "/shipping.aspx?ErrorString=" & Resources.Language.MESSAGE_UNKNOWN_SHIPPING_ADDRESS)
                    Else
                        Payment_Error(Resources.Language.MESSAGE_UNKNOWN_SHIPPING_ADDRESS, CartHeaderGuid, paymentType)
                    End If
                End If
            End If

            ' Prepare the new id for the ShopSalesHeader
            If String.IsNullOrEmpty(ExpandITLib.DBNull2Nothing(globals.OrderDict("HeaderGuid"))) Then
                Try
                    HeaderGuid = GenGuid()
                Catch ex As Exception
                    Return ""
                End Try
            Else
                HeaderGuid = globals.OrderDict("HeaderGuid")
            End If

            ' Copy cart lines to order lines
            ' Set new id's for the order to be saved.
            For Each aKey In globals.OrderDict("Lines")
                globals.OrderDict("Lines")(aKey.Key)("HeaderGuid") = HeaderGuid
                globals.OrderDict("Lines")(aKey.Key)("LineGuid") = GenGuid()
            Next

            ' Store the order lines in the ShopSalesLine table.
            Dicts2Table(globals.OrderDict("Lines"), "ShopSalesLine", "")

            ' Field maps/updates
            globals.OrderDict("HeaderGuid") = HeaderGuid
            globals.OrderDict("globals.UserGuid") = Session("UserGuid")
            globals.OrderDict("HeaderDate") = Now
            globals.OrderDict("PaymentType") = paymentType
            globals.OrderDict("PaymentStatus") = verificationStatus


            'JA2011030701 - PAYMENT TABLE - START
            If paymentType = "COD" Or paymentType = "COP" Or paymentType = "VK" Or paymentType = "" Or paymentType = "INVOICE" Then
                'globals.OrderDict("PaymentTransactionID") = transact
                'globals.OrderDict("PaymentTransactionSignature") = PaymentTransactionSignature
                Dim processAutomatedTransactionDict As New ExpDictionary
                processAutomatedTransactionDict.Add("CustomerReference", globals.OrderDict("CustomerReference"))
                processAutomatedTransactionDict.Add("DocumentGuid", globals.OrderDict("HeaderGuid"))
                processAutomatedTransactionDict.Add("PaymentType", globals.OrderDict("PaymentType"))
                processAutomatedTransactionDict.Add("PaymentGuid", GenGuid())
                processAutomatedTransactionDict.Add("PaymentReference", eis.GenCustRef())
                processAutomatedTransactionDict.Add("PaymentDate", DateTime.Now())
                processAutomatedTransactionDict.Add("UserGuid", globals.User("UserGuid"))
                processAutomatedTransactionDict.Add("LedgerPayment", False)
                processAutomatedTransactionDict.Add("PaymentTransactionID", transact)
                processAutomatedTransactionDict.Add("PaymentTransactionSignature", PaymentTransactionSignature)
                Dict2Table(processAutomatedTransactionDict, "PaymentTable", "")
            Else
                ExpandITLib.excecuteNonQueryDb("UPDATE PaymentTable SET DocumentGuid = " & SafeString(HeaderGuid) & " , PaymentType=" & SafeString(paymentType) & " WHERE CustomerReference = " & SafeString(globals.OrderDict("CustomerReference")))
            End If
            'JA2011030701 - PAYMENT TABLE - END

            globals.OrderDict("CurrencyExchangeRate") = CDblEx(currency.GetExchangeFactor(globals.OrderDict("CurrencyGuid"), AppSettings("MULTICURRENCY_SITE_CURRENCY")))

            ' Set bill to address
            If bUseShippingAddress Then 'If ShippingAddress is used
                If Payment Is Nothing Then
                    If Shipping IsNot Nothing Then
                        BillTo = Shipping
                    Else
                        BillTo = globals.User
                    End If
                Else
                    BillTo = Payment
                End If
            Else
                BillTo = globals.User
            End If

            ' Move billing data from user/shipping to the order head. Only add non-existing fields.
            For Each aKey In BillTo
                If Not globals.OrderDict.Exists(aKey.Key) Then
                    globals.OrderDict(aKey.Key) = BillTo(aKey.Key)
                End If
            Next

            ' Set ship to address
            If bUseShippingAddress Then
                ShipTo = Shipping
            Else
                ShipTo = globals.User
            End If


            If bUseShippingAddress Then
                addWithPrefix("", ShipTo)
            Else
                addWithPrefix("ShipTo", ShipTo)
            End If

            ' Payment fee might be returned from the payment provider. 
            ' This fee is added to the resulting order header as PaymentFee. 

            ' The fee is NOT Tax dutiable (added to Total and TotalInclTax)
            paymentFeeInclTax = paymentFee

            If paymentFee > 0 Then
                globals.OrderDict("PaymentFeeAmount") = paymentFee
                globals.OrderDict("PaymentFeeAmountInclTax") = paymentFeeInclTax
                globals.OrderDict("TaxAmount") = globals.OrderDict("TaxAmount") + (paymentFeeInclTax - paymentFee)
                globals.OrderDict("Total") = globals.OrderDict("Total") + paymentFee
                globals.OrderDict("TotalInclTax") = globals.OrderDict("TotalInclTax") + paymentFeeInclTax
            End If



            'JA2010102801 - PAPERCHECK - Start
            If CStrEx(paymentType) = "PAPERCHECK" Then
                ExpandITLib.excecuteNonQueryDb("UPDATE PaperChecks SET OrderCompleted = '1' WHERE OrderNumber = " & SafeString(globals.OrderDict("CustomerReference")))
            End If
            'JA2010102801 - PAPERCHECK - End




            ' Set ShopID (for telmi)
            globals.OrderDict("WebShopID") = globals.shopID

            ' Store order header data in the ShopSalesHeader table.
            If Dict2Table(globals.OrderDict, "ShopSalesHeader", "") Then 'check save success             
                ' Empty the shopping cart.
                ' Delete cart lines
                excecuteNonQueryDb("DELETE FROM CartLine WHERE HeaderGuid=" & SafeString(CartHeaderGuid))

                ' Delete cart
                excecuteNonQueryDb("DELETE FROM CartHeader WHERE HeaderGuid=" & SafeString(CartHeaderGuid))

                ' Update the cartcounters. Call with Empty aregument to clear all counters.
                eis.UpdateMiniCartInfo(Nothing)

                ' Send Confirmation Email
                'bMail = eis.SendOrderConfirmationEmail(globals.OrderDict, BillTo, ShipTo)
				ExpandIT.FileLogger.log("About to send confirmation email with RequireSSL = "  & RequireSSL)
				bMail = eis.SendOrderConfirmationEmail(globals.OrderDict, BillTo, ShipTo, RequireSSL)
                If bMail Then
                    ExpandITLib.excecuteNonQueryDb("UPDATE ShopSalesHeader SET MailSent = 1 WHERE HeaderGuid = " & SafeString(HeaderGuid))
                Else
                    ExpandITLib.excecuteNonQueryDb("UPDATE ShopSalesHeader SET MailSent = 0 WHERE HeaderGuid = " & SafeString(HeaderGuid))
                End If

                ExpandITLib.excecuteNonQueryDb("UPDATE ShopSalesHeader SET FinalizedOrderDate =" & SafeString(CDateEx(DateTime.Now)) & ", OrderCreated=" & SafeString(CDateEx(globals.OrderDict("CreatedDate"))) & " WHERE HeaderGuid = " & SafeString(HeaderGuid))

                'JA2011030701 - PAYMENT TABLE - START
                ExpandITLib.excecuteNonQueryDb("UPDATE PaymentTable SET OrderCompleted=1 WHERE DocumentGuid = " & SafeString(HeaderGuid))
                'JA2011030701 - PAYMENT TABLE - END

                Return HeaderGuid
            Else
                Return ""
            End If
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="ErrorString"></param>
        ''' <param name="CartHeaderGuid"></param>
        ''' <param name="paymentType"></param>
        ''' <remarks></remarks>
        Protected Sub Payment_Error(ByVal ErrorString As String, ByVal CartHeaderGuid As String, _
          ByVal paymentType As String, Optional ByVal statusCode As System.Net.HttpStatusCode = System.Net.HttpStatusCode.OK)
            Dim sql As String

            If ErrorString <> "" Then
                Application(Now & "_Payment_" & paymentType & "_Error ") = ErrorString

                sql = "UPDATE CartHeader " & _
                    " SET PaymentStatus = " & SafeString(ErrorString) & "," & _
                    " PaymentError = " & SafeString(ErrorString) & _
                    " WHERE HeaderGuid = " & SafeString(CartHeaderGuid)

                excecuteNonQueryDb(sql)

                Response.Write("Error processing payment. [" & ErrorString & "]")
                Response.StatusCode = statusCode
                Response.End()
            End If
        End Sub

        Private Sub addWithPrefix(ByVal prefix As String, ByVal ShipTo As ExpDictionary)
            Dim aKey As KeyValuePair(Of Object, Object)
            Try
                For Each aKey In ShipTo
                    If Not globals.OrderDict.Exists(prefix & aKey.Key.ToString()) Then
                        globals.OrderDict(prefix & aKey.Key.ToString()) = ShipTo(aKey.Key)
                    Else
                        If Not globals.OrderDict(prefix & CStr(aKey.Key)).Equals(DBNull.Value) Then
                            If String.IsNullOrEmpty(CStr(globals.OrderDict(prefix & CStr(aKey.Key)))) Then
                                globals.OrderDict(prefix & CStr(aKey.Key)) = ShipTo(CStr(aKey.Key))
                            End If
                        Else
                            globals.OrderDict(prefix & CStr(aKey.Key)) = ShipTo(CStr(aKey.Key))
                        End If
                    End If
                Next
            Catch ex As Exception

            End Try
        End Sub

    End Class
End Namespace