Imports Microsoft.VisualBasic
Imports ExpandIT
Imports ExpandIT.ExpandITLib
Imports System.Collections.Generic
Imports System.Configuration.ConfigurationManager
Imports System.Security.Principal
Imports System.Threading

Namespace ExpandIT.ShippingPayment

    Public Class _CartPage
        Inherits ShippingPaymentPage
        Implements System.Web.UI.ICallbackEventHandler
        'Inherits ShippingMethodPage

        Protected OrderObject As ExpDictionary
        Protected OldNumberOfLines As Integer
        Protected bLinesHaveBeenRemoved As Boolean
        Protected OrderPadInfo As String
        Protected cbReq As Dictionary(Of String, ArrayList)
        Protected Delegate Sub AsyncTaskDelegate()
        Protected identity As WindowsIdentity

        Protected Sub saveOrderDictAsync()
            identity.Impersonate()
            If globals.OrderDict IsNot Nothing Then
                eis.SaveOrderDictionary(globals.OrderDict)
            End If
        End Sub

        Protected Overloads Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender

            Dim cbReference As String
            Try
                cbReference = Page.ClientScript.GetCallbackEventReference(Me, _
                    "arg", "ReceiveServerData", "context", "callBackErrorHandler", True)

                Dim callbackScript As String = ""
                callbackScript &= "function CallServer(arg, context) { " & _
                    cbReference & ";}"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), _
                    "CallServer", callbackScript, True)
                Dim ReceiveServerData As String = ""

                ReceiveServerData &= "function ReceiveServerData(rValue) {" & _
                    Environment.NewLine & "var tArray = new Array();" & _
                    Environment.NewLine & "tArray = rValue.split(';');" & _
                    Environment.NewLine & "document.getElementById(tArray[1]).innerHTML = tArray[0];" & _
                    Environment.NewLine & "}"

                If Not ClientScript.IsClientScriptBlockRegistered("ReceiveServerData") Then
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), _
                        "ReceiveServerData", ReceiveServerData, True)
                End If
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "ReceiveServerData", ReceiveServerData, True)

                Dim AddToCart As String = ""
                AddToCart &= "function AddToCart(queryStr) {" & _
                    "CallServer(queryStr, ''); }"

                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), _
                    "AddToCart", AddToCart, True)

                Dim callBackErrorHandler As String = ""
                callBackErrorHandler &= "function callBackErrorHandler(exception,context){" & _
                    "alert('An Error Occured!');}"

                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), _
                    "callBackErrorHandler", callBackErrorHandler, True)

            Catch ex As Exception

            End Try

        End Sub

        ''' <summary>
        ''' Helper function: Add the item(s) to the order.
        ''' </summary>
        ''' <param name="isAsyncTask"></param>
        ''' <remarks></remarks>
        ''' 'AM0122201001 - UOM - Start
        Protected Sub Add(Optional ByVal isAsyncTask As Boolean = False, Optional ByVal OnlyProduct As String = "")
            'AM0122201001 - UOM - End

            Dim sku As String
            Dim i As Integer
            Dim quantity As Decimal
            Dim comment, VariantCode, myGuidStr As String
            Dim sqlVariant As String
            Dim dictVariants As ExpDictionary = Nothing
            Dim req As Dictionary(Of String, ArrayList)
            'AM0122201001 - UOM - Start
            Dim UOM As String = ""
            'AM0122201001 - UOM - End

            ' Read parameters
            globals.GroupGuid = CLngEx(Request("GroupGuid"))

            If IsCallback Then
                req = cbReq
            Else
                req = QueryString2Dict()
            End If

            If Not req.ContainsKey("SKU") Then req.Add("SKU", New ArrayList())
            If Not req.ContainsKey("Quantity") Then req.Add("Quantity", New ArrayList())
            If Not req.ContainsKey("VariantCode") Then req.Add("VariantCode", New ArrayList())
            If Not req.ContainsKey("LineComment") Then req.Add("LineComment", New ArrayList())
            'AM0122201001 - UOM - Start
            If AppSettings("EXPANDIT_US_USE_UOM") Then
                If Not req.ContainsKey("UOM") Then req.Add("UOM", New ArrayList())
            End If
            'AM0122201001 - UOM - End

            'JA2010092201 - RECURRING ORDERS - START
            If CStrEx(Request("modifyRecurrency")) <> "" Then
                globals.OrderDict = eis.LoadOrderModifyRecurrency(CStrEx(Request("modifyRecurrency")))
            Else
                globals.OrderDict = eis.LoadOrderDictionary(globals.User)
            End If
            'JA2010092201 - RECURRING ORDERS - END

            OrderObject = globals.OrderDict

            ' Save comment and purchase order number
            If Not Request("HeaderComment") Is Nothing Then globals.OrderDict("HeaderComment") = CStrEx(Request("HeaderComment"))
            If Not Request("CustomerPONumber") Is Nothing Then globals.OrderDict("CustomerPONumber") = CStrEx(Request("CustomerPONumber"))
            If Not Request("YourName") Is Nothing Then globals.OrderDict("YourName") = CStrEx(Request("YourName"))
            'WLB 10/10/2012 - BEGIN - Add "Your E-mail" to Additional Information
            If Not Request("YourEmail") Is Nothing Then globals.OrderDict("YourEmail") = CStrEx(Request("YourEmail"))
            'WLB 10/10/2012 - END - Add "Your E-mail" to Additional Information

            'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
            If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
                Dim csrObj As New USCSR(globals)
                csrObj.setAdjustmentInfo(Request)
            End If
            'AM2011031801 - ENTERPRISE CSR STAND ALONE - End

            ' Get Variant for sku's. This will enable all products to get a "default" VariantCode.
            'AM2010021901 - VARIANTS - Start
            If CBoolEx(AppSettings("EXPANDIT_US_USE_VARIANTS")) Then
                'AM2010021901 - VARIANTS - End
                dictVariants = New ExpDictionary
                myGuidStr = ""

                For i = 0 To req("SKU").Count - 1
                    ' Checking if variant guid is applied.
                    sku = Trim(req("SKU")(i))

                    ' If VariantCode = Null, then check collect Guids.
                    If req("Quantity").Count >= i + 1 Then
                        If CStrEx(sku) <> "" And Trim(req("Quantity")(i)) <> "" Then
                            If myGuidStr <> "" Then myGuidStr = myGuidStr & ","
                            myGuidStr = myGuidStr & SafeString(sku)
                        End If
                    End If
                Next
                If myGuidStr = "" Then myGuidStr = "''"

                ' Get possible variants for the ProductGuid's collected in myGuidStr
                If Len(myGuidStr) > 2 Then
                    sqlVariant = "SELECT * FROM ProductVariant WHERE ProductGuid IN (" & myGuidStr & ") ORDER BY VariantName DESC"
                    dictVariants = SQL2Dicts(sqlVariant, "ProductGuid")
                End If
            End If

            ' Add SKUs to the cart
            'AM0122201001 - UOM - Start
            Dim j As Integer = 0
            Try
                For i = 0 To req("SKU").Count - 1
                    sku = req("SKU")(i)
                    If sku <> "" Then
                        If AppSettings("EXPANDIT_US_USE_UOM") And req("SKU").Count = 1 Then
                            For j = 0 To req("Quantity").Count
                                quantity = String2UFloat("0" & req("Quantity")(j))
                                If AppSettings("EXPANDIT_US_USE_UOM") Then
                                    UOM = CStrEx(req("UOM")(j))
                                End If
                                'AM0122201001 - UOM - End

                                If AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_COMMENT") And req("LineComment").Count >= i + 1 Then
                                    ' Get line comments if they exist in the request object
                                    comment = CStrEx(req("LineComment")(i))
                                Else
                                    comment = Nothing
                                End If

                                ' Insert VariantCode.
                                'AM2010021901 - VARIANTS - Start
                                If CBoolEx(AppSettings("EXPANDIT_US_USE_VARIANTS")) Then
                                    'AM2010021901 - VARIANTS - Start
                                    'If IsDict(dictVariants(sku)) Then
                                    '    VariantCode = dictVariants(sku)("VariantCode")
                                    'Else
                                    '    VariantCode = Nothing
                                    'End If
                                    VariantCode = Nothing
                                    If req("VariantCode").Count >= i + 1 Then
                                        If Trim(req("VariantCode")(i)) <> "" Then
                                            VariantCode = Trim(req("VariantCode")(i))
                                        End If
                                    End If
                                Else
                                    VariantCode = Nothing
                                End If
                                'AM0122201001 - UOM - Start
                                If OnlyProduct = "" Or (OnlyProduct = sku) Then
                                    'JA2010092201 - RECURRING ORDERS - START
                                    If CStrEx(globals.OrderDict("ModifiedRecurrency")) = "" Then
                                        If AppSettings("EXPANDIT_US_USE_UOM") Then
                                            eis.AddItemToOrder2(globals.OrderDict, sku, quantity, comment, VariantCode, UOM)
                                        Else
                                            eis.AddItemToOrder2(globals.OrderDict, sku, quantity, comment, VariantCode)
                                        End If
                                    End If
                                    'JA2010092201 - RECURRING ORDERS - END
                                End If
                                'AM0122201001 - UOM - End
                            Next
                        Else
                            quantity = String2UFloat("0" & req("Quantity")(i))

                            'AM0122201001 - UOM - Start
                            If AppSettings("EXPANDIT_US_USE_UOM") Then
                                UOM = CStrEx(req("UOM")(i))
                            End If
                            'AM0122201001 - UOM - End

                            If AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_COMMENT") And req("LineComment").Count >= i + 1 Then
                                ' Get line comments if they exist in the request object
                                comment = CStrEx(req("LineComment")(i))
                            Else
                                comment = Nothing
                            End If

                            ' Insert VariantCode.
                            'AM2010021901 - VARIANTS - Start
                            If CBoolEx(AppSettings("EXPANDIT_US_USE_VARIANTS")) Then
                                'AM2010021901 - VARIANTS - Start

                                'If IsDict(dictVariants(sku)) Then
                                'VariantCode = dictVariants(sku)("VariantCode")
                                'Else
                                'VariantCode = Nothing
                                'End If

                                VariantCode = Nothing
                                If req("VariantCode").Count >= i + 1 Then
                                    If Trim(req("VariantCode")(i)) <> "" Then
                                        VariantCode = Trim(req("VariantCode")(i))
                                    End If
                                End If

                            Else
                                VariantCode = Nothing
                            End If
                            If OnlyProduct = "" Or (OnlyProduct = sku) Then
                                'AM0122201001 - UOM - Start
                                'JA2010092201 - RECURRING ORDERS - START
                                If CStrEx(globals.OrderDict("ModifiedRecurrency")) = "" Then
                                    If AppSettings("EXPANDIT_US_USE_UOM") Then
                                        eis.AddItemToOrder2(globals.OrderDict, sku, quantity, comment, VariantCode, UOM)
                                    Else
                                        eis.AddItemToOrder2(globals.OrderDict, sku, quantity, comment, VariantCode)
                                    End If
                                End If
                                'JA2010092201 - RECURRING ORDERS - END
                                'AM0122201001 - UOM - End
                            End If
                            End If
                    End If
                Next
            Catch ex As System.ArgumentOutOfRangeException

            End Try

            ' Check if any lines have been deleted due to line set to 0 or -1
            bLinesHaveBeenRemoved = 0
            If Not Request.Form("Update") Is Nothing Then
                If CBool(AppSettings("CART_ENTRY")) Then
                    OldNumberOfLines = req("SKU").Count - 1
                Else
                    OldNumberOfLines = req("SKU").Count
                End If
                If globals.OrderDict("Lines").Count < OldNumberOfLines Then
                    bLinesHaveBeenRemoved = 1
                End If
            End If

            ' For performance reasons.
            globals.OrderDict("IsCalculated") = False
            globals.OrderDict("VersionGuid") = GenGuid()
            '*****shipping*****-start
            If AppSettings("EXPANDIT_US_USE_SHIPPING") Then
                globals.OrderDict("ShippingAmount") = 0
                globals.OrderDict("ShippingAmountInclTax") = 0
            End If
            '*****shipping*****-end
            ' If not async call, save the order.
            If Not isAsyncTask Then eis.SaveOrderDictionary(globals.OrderDict)

        End Sub

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult

            Dim ret As String = EISClass.FindChildControl(Me, "lbMiniCart").ClientID

            If Not String.IsNullOrEmpty(ret) Then
                OrderPadInfo &= ";" & ret
            End If

            Return OrderPadInfo

        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="eventArgument"></param>
        ''' <remarks></remarks>
        Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent

            Dim atd As New AsyncTaskDelegate(AddressOf saveOrderDictAsync)

            cbReq = QueryString2Dict(eventArgument)
            identity = System.Security.Principal.WindowsIdentity.GetCurrent()

            ' add to the order
            Add(True)

            ' save the orderdict
            Dim result As IAsyncResult
            result = atd.BeginInvoke(Nothing, atd)
            atd.EndInvoke(result)

            eis.CalculateOrder()
            ' Update the cartcounters.
            If globals.OrderDict Is Nothing Then
                eis.UpdateMiniCartInfo(Nothing)
            Else
                eis.UpdateMiniCartInfo(globals.OrderDict)
            End If

            If globals.MiniCartInfo("Counter") > 1 Then
                OrderPadInfo = Replace(Resources.Language.LABEL_SEVERAL_ITEMS_IN_CART, "%1", globals.MiniCartInfo("Counter"))
            ElseIf globals.MiniCartInfo("Counter") = 1 Then
                OrderPadInfo = Replace(Resources.Language.LABEL_ONE_ITEM_IN_CART, "%1", globals.MiniCartInfo("Counter"))
            Else
                OrderPadInfo = Resources.Language.LABEL_ORDER_PAD_IS_EMPTY
            End If
            REM -- Must be run from here
            If CBoolEx(AppSettings("MINICART_DISPLAY_TOTAL")) Then
                If globals.MiniCartInfo("Counter") <> 0 Then
                    Dim totalValue As Decimal = IIf(AppSettings("SHOW_TAX_TYPE") = "INCL", globals.MiniCartInfo("TotalInclTax"), globals.MiniCartInfo("Total"))
                    If eis.CheckPageAccess("Prices") Then
                        OrderPadInfo = OrderPadInfo & " /" & CurrencyFormatter.FormatAmount(totalValue, globals.User("CurrencyGuid"))
                    Else
                        OrderPadInfo = OrderPadInfo
                    End If
                End If
            End If
        End Sub

    End Class

End Namespace