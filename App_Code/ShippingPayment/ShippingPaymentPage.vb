﻿Imports System.Configuration.ConfigurationManager
Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports ExpandIT.EisClass
Imports ExpandIT.ExpandITLib

Namespace ExpandIT.ShippingPayment

    Public Class ShippingPaymentPage
        Inherits ExpandIT.Page

        Private SPLogic As ShippingPaymentLogic

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
            SPLogic = New ShippingPaymentLogic(eis)
        End Sub

#Region "ShippingMethodPage"

        ''' <summary>
        ''' Gets the currently selected PaymentMethod value from CartHeader
        ''' Sets the PaymentMethod value in CartHeader         
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property CustomerPaymentMethod() As Object
            Get
                Return SPLogic.CustomerPaymentMethod
            End Get
            Set(ByVal value As Object)
                SPLogic.CustomerPaymentMethod = value
            End Set
        End Property

        ''' <summary>
        ''' Gets the currently selected ShippingHandlingProviderGuid from CartHeader
        ''' Sets the ShippingHandlingProviderGuid value in CartHeader    
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property CustomerShippingProvider() As Object
            Get
                Return SPLogic.CustomerShippingProvider
            End Get
            Set(ByVal value As Object)
                SPLogic.CustomerShippingProvider = value
            End Set
        End Property

        ''' <summary>
        ''' Gets the ProviderName from ShippingHandlingProvider
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property CustomerShippingProviderName() As String
            Get
                Return SPLogic.CustomerShippingProviderName
            End Get
        End Property

        ''' <summary>
        ''' Finds the first possible PaymentType option for the current user
        ''' based on user status combined with valid combinations of ShippingHandlingproviders        
        ''' </summary>
        ''' <param name="addConstraint"></param>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property FirstPossiblePaymentType(ByVal addConstraint As Boolean) As String
            Get
                Return SPLogic.FirstPossiblePaymentType(addConstraint)
            End Get
        End Property

        ''' <summary>
        ''' Finds the first possible ShippingProvider option for the current user
        ''' based on user status and possible payment alternatives, combined
        ''' with valid combinations of shippingProviders and PaymentMethods              
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property FirstPossibleShippingProvider(Optional ByVal addConstraint As Boolean = False) As String
            'Check for default shipping provider guid in web.config and then compare the sanity of that value.
            Get
                Return SPLogic.FirstPossibleShippingProvider(addConstraint)
            End Get
        End Property

        ''' <summary>
        ''' Finds all the possible ShippingProviders for the current user
        ''' based on user status and possible payment alternatives, combined
        ''' with valid combinations of shippingProviders and PaymentMethods              
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property AllPossibleShippingProviders() As ArrayList
            Get
                Return SPLogic.AllPossibleShippingProviders
            End Get
        End Property

        '''' <summary>
        '''' Returns the current userstatus
        '''' </summary>
        '''' <returns></returns>
        '''' <remarks></remarks>
        'Private Function determineUserStatus() As String
        '    ' Check User Status
        '    If userStatus Is Nothing Then
        '        userStatus = globals.eis.determineUserStatus()
        '    End If
        '    Return userStatus
        'End Function

        ''' <summary>
        ''' This property uses the internal class PathFinder to calculate the next url
        ''' based on the current url, combined with the rules concerning valid payment
        ''' and shippingprovider combimations defined in the DB       
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property NextUrl() As String
            Get
                Dim pFinder As PathFinder = New PathFinder(Me)
                Return pFinder.nextUrl
            End Get
        End Property

        'AM2010080301 - CHECK OUT PROCESS MAP - Start
        Public ReadOnly Property CheckOutPaths() As String()
            Get
                Dim pFinder As PathFinder = New PathFinder(Me)
                Return pFinder.PathsArray
            End Get
        End Property

        Public ReadOnly Property getCurrentLocation() As String
            Get
                Dim pFinder As PathFinder = New PathFinder(Me)
                Return pFinder.getLocation()
            End Get
        End Property
        'AM2010080301 - CHECK OUT PROCESS MAP - End

        ''' <summary>
        ''' This function returns a SQL-Statement that will return all
        ''' possible ShippingProviderNames and ShippingProviderGuids for the current user.
        ''' It's based on user status and user laguage, and also all possible payment alternatives 
        ''' for the current user. Finally this is combined
        ''' with valid combinations of shippingProviders and PaymentMethods               
        ''' </summary>
        ''' <param name="isAppendConstraint"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function PossibleShippingProvidersSQLString(ByVal isAppendConstraint As Boolean) As String
            Return SPLogic.PossibleShippingProvidersSQLString(isAppendConstraint)
        End Function


        ''' <summary>
        ''' This function returns a SQL-Statement that will return all possible
        ''' PaymentTypes for the current user.    
        ''' It's based on user status combined with valid combinations of ShippingHandlingproviders            
        ''' </summary>
        ''' <param name="isAppendConstraint"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        ''' JA2010092201 - RECURRING ORDERS - START
        Public Function PossiblePaymentTypesSQLString(Optional ByVal isAppendConstraint As Boolean = False, Optional ByVal recurring As Boolean = False) As String
            Return SPLogic.PossiblePaymentTypesSQLString(isAppendConstraint)
        End Function
        ' JA2010092201 - RECURRING ORDERS - END


        ''' <summary>
        ''' Internal Private Class PathFinder calculates valid paths in the EIS checkout process
        ''' </summary>
        ''' <remarks></remarks>
        Class PathFinder

            Private currentLocation As String
            Private paths() As String
            Private pathList As ArrayList
            Private cartPage As String = "cart.aspx"
            Private shippingPage As String = "shipping.aspx"
            Private providerPage As String = "shippingprovider.aspx"
            Private paymentTypePage As String = "paymentmethod.aspx"
            Private paymentPage As String = "payment.aspx"
            Private parentPage As ShippingPaymentPage

            'AM2010080301 - CHECK OUT PROCESS MAP - Start
            Public ReadOnly Property PathsArray() As String()
                Get
                    Return paths
                End Get
            End Property
            Public ReadOnly Property getLocation() As String
                Get
                    Return Location()
                End Get
            End Property

            'AM2010080301 - CHECK OUT PROCESS MAP - End

            ''' <summary>
            ''' Parameterized Constructor for the internal Class PathFinder
            ''' </summary>
            ''' <param name="parent">reference to the containing class</param>            
            ''' <remarks></remarks>
            Public Sub New(ByVal parent As ShippingPaymentPage)

                parentPage = parent
                '*****shipping*****-start
                If AppSettings("EXPANDIT_US_USE_SHIPPING") Then
                    paths = New String() {cartPage, shippingPage, paymentTypePage, paymentPage}
                Else
                    'AM2010042601 - ENTERPRISE SHIPPING - Start
                    paths = New String() {cartPage, shippingPage, providerPage, paymentTypePage, paymentPage}
                    'AM2010042601 - ENTERPRISE SHIPPING - End
                End If
                '*****shipping*****-end
                pathList = New ArrayList()
                For i As Integer = 0 To paths.Length - 1
                    pathList.Add(paths(i))
                Next

                'JA2010092201 - RECURRING ORDERS - START
                If CBoolEx(AppSettings("RECURRING_ORDERS_SHOW")) AndAlso CBoolEx(AppSettings("RECURRING_ORDERS_REQUIRE_CREDIT_CARD")) Then
                    If parentPage.globals.OrderDict Is Nothing Then
                        parentPage.globals.OrderDict = parentPage.eis.LoadOrderDictionary(parentPage.globals.User)
                    End If
                    If CBoolEx(parentPage.globals.OrderDict("RecurringOrder")) Then
                        deleteFromPathList(SQL2DataTable(parent.PossiblePaymentTypesSQLString(False, parentPage.globals.OrderDict("RecurringOrder"))), SQL2DataTable(parent.PossibleShippingProvidersSQLString(False)))
                    Else
                        deleteFromPathList(SQL2DataTable(parent.PossiblePaymentTypesSQLString()), SQL2DataTable(parent.PossibleShippingProvidersSQLString(False)))
                    End If
                Else
                    deleteFromPathList(SQL2DataTable(parent.PossiblePaymentTypesSQLString()), SQL2DataTable(parent.PossibleShippingProvidersSQLString(False)))
                End If
                'JA2010092201 - RECURRING ORDERS - END

                Dim parts() As String = Split(HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath, "/")
                Dim pathStr As String = parts(parts.Length - 1)
                currentLocation = Me.determineValidLocation(pathStr)

            End Sub

            ''' <summary>
            ''' Removes uneeded paths from the pathlist
            ''' </summary>
            ''' <param name="dtPaymentTypes"></param>
            ''' <param name="dtProvidertypes"></param>            
            ''' <remarks></remarks>
            Private Sub deleteFromPathList(ByVal dtPaymentTypes As DataTable, ByVal dtProvidertypes As DataTable)

                ' Find out if customers provider type is pickup
                Dim isPickUp As Boolean

                '*****shipping*****-start
                If Not CBoolEx(AppSettings("EXPANDIT_US_USE_SHIPPING")) Then
                    Dim isPickUpObject As Object = ExpandITLib.getSingleValueDB("SELECT IsPickUp FROM ShippingHandlingProvider WHERE ShippingHandlingProviderGuid = " & SafeString(parentPage.CustomerShippingProvider))
                    If isPickUpObject IsNot Nothing Then
                        If Not isPickUpObject.Equals(DBNull.Value) Then
                            isPickUp = isPickUpObject
                        End If
                    End If
                End If
                '*****shipping*****-end

                If dtPaymentTypes.Rows.Count = 1 Then
                    ' We have only one paymentType option - no need to visit payment selection page
                    pathList.Remove(Me.paymentTypePage)
                    ' Since we have only one paymentType, we set the customers paying method here
                    Dim pt As String = parentPage.FirstPossiblePaymentType(False)
                    parentPage.CustomerPaymentMethod = pt
                    If parentPage.globals.OrderDict Is Nothing Then
                        parentPage.globals.OrderDict = parentPage.eis.LoadOrderDictionary(parentPage.globals.User)
                    End If
                    parentPage.globals.OrderDict("PaymentMethod") = pt
                End If
                '*****shipping*****-start
                If Not CBoolEx(AppSettings("EXPANDIT_US_USE_SHIPPING")) Then
                    If dtProvidertypes.Rows.Count = 1 Then
                        ' We have only one shipping provider option -
                        ' no need to visit shipping provider selection page
                        pathList.Remove(providerPage)
                    End If
                End If
                '*****shipping*****-end

                If Not CBoolEx(parentPage.globals.User("Anonymous")) Then
                    If isPickUp Then
                        ' If the user has selected to collect and the user isn't anonymous
                        ' there is no reason to visit the shipping address page
                        pathList.Remove(shippingPage)
                    End If
                End If

            End Sub

            ''' <summary>
            ''' Calculates and returns the next valid url string from the paths() array
            ''' </summary>
            ''' <returns>A string representing the next valid url</returns>
            ''' <remarks></remarks>
            Public Function nextUrl() As String
                Dim index As Integer = Array.IndexOf(paths, Location)
                If index >= 0 Then
                    Dim tempIndex As Integer = 0
                    Dim currentIndex As Integer = paths.Length
                    For Each item As String In pathList
                        tempIndex = Array.IndexOf(paths, item)
                        If tempIndex >= 0 Then
                            If tempIndex > index Then
                                If tempIndex < currentIndex Then
                                    currentIndex = tempIndex
                                End If
                            End If
                        End If
                    Next

                    If CBoolEx(parentPage.globals.User("Anonymous")) And Not parentPage.eis.CheckPageAccess("Order") Then
                        Return "~/user_login.aspx?bouncecheckout=true&bouncereturl=~/cart.aspx"
                    End If
                    Return paths(currentIndex)
                End If
                Return currentLocation
            End Function

            ''' <summary>
            ''' Property uses internal function determineValidLocation to check
            ''' that the current url is one of the defined urls in the paths() array.
            ''' Returns the current url as string            
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Protected Property Location() As String
                Get
                    Return currentLocation
                End Get
                Set(ByVal value As String)
                    currentLocation = determineValidLocation(value)
                End Set
            End Property

            ''' <summary>
            ''' Function loops through the paths() array to determine if the current url is valid.
            ''' If the current url is valid, it will be removed from the path list. This is done to insure
            ''' that the current page will not call itself.                        
            ''' </summary>
            ''' <param name="val"></param>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Private Function determineValidLocation(ByVal val As String) As String
                For Each item As String In paths
                    If val.ToLower.Equals(item) Then
                        pathList.Remove(val)
                        Return item
                    End If
                Next
                Return String.Empty
            End Function

        End Class

#End Region

#Region "PaymentMethodPage"

        Public Property CartShippingAddress() As ExpDictionary
            Get
                Return SPLogic.CartShippingAddress
            End Get
            Set(ByVal value As ExpDictionary)
                SPLogic.CartShippingAddress = value
            End Set
        End Property

        Public Property CartPaymentAddress() As ExpDictionary
            Get
                Return SPLogic.CartPaymentAddress
            End Get
            Set(ByVal value As ExpDictionary)
                SPLogic.CartPaymentAddress = value
            End Set
        End Property

#End Region

    End Class

End Namespace