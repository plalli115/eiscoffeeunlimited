﻿using ExpandIT31.ExpanditFramework.BLLBase;
using ExpandIT31.ExpanditFramework.Core;
using ExpandIT31.ExpanditFramework.Infrastructure;
using ExpandIT31.Logic;

namespace ExpandIT31 {
    /// <summary>
    /// ExpanditFactory is the class where the concrete instances of the ExpandITBLL classes are returned
    /// to the caller.
    /// Usage example:
    /// To obtain the default concrete instance of type UserBase, the CreateUser 
    /// method returns: GenericFactory<ExpandITUser>.getInstance(userGuid);
    /// To change the behavior, and obtain another concrete implementation of userBase, change what's inside the <> and
    /// use for example: GenericFactory<SomeImplementorDefinedUser>.getInstance(userGuid);
    /// Now the the CreateUser method returns SomeImplementorDefinedUser.
    /// </summary>
    public class ExpanditFactory {
        static ExpanditFactory() { }
        
        #region "User"

        public static UserBase CreateUser(string userGuid) {            
            return GenericFactory<ExpandITUser>.getInstance(userGuid);            
        }
        
        public static UserValidationBase CreateUserValidator(UserBase bllObject, string[] requiredFields, string[] requiredFieldsLabels){
            return GenericFactory<UserValidator>.getInstance(bllObject, requiredFields, requiredFieldsLabels);
        }

        public static UserActionBase CreateUserMail(UserBase expUser, PageMessages pageMessage) {
            return GenericFactory<UserMail>.getInstance(expUser, pageMessage);           
        }
        
        #endregion
        
        #region "UserB2B"

        public static UserB2BBase CreateUserB2B(string userGuid) {            
            return GenericFactory<ExpandITB2BUser>.getInstance(userGuid);
        }
        
        #endregion
        
    }
}