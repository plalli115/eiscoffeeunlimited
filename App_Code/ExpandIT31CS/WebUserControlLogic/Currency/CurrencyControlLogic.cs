﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Web;
using ExpandIT31.ExpanditFramework.Infrastructure;
using ExpandIT31.ExpanditFramework.Util;

namespace ExpandIT {
    /// <summary>
    /// Abstract base class.
    /// Handles site currencies by applying business rules found in web.config.
    /// Uses Template Pattern to cooperate with its concrete descentents.
    /// The currency values are cached.
    /// Class is adapted to be used by ObjectDataSources.    
    /// </summary>
    [DataObjectAttribute()]
    public abstract class CurrencyControlLogic {

        private const string sql = "SELECT CurrencyCode, CurrencyName FROM CurrencyTable " +
                        " WHERE CurrencyEnabled <> 0 ORDER BY CurrencyName";
        private const string cacheNamePrimary = "SiteCurrencies";
        private const string cacheNameSecondary = "SecondarySiteCurrencies";
        private Dictionary<object, object> _currentUser;
        protected DataTable CurrencyDataTable;
        protected DataTable SecondaryCurrencyDataTable;
        protected string siteCurrency;

        // Error message will never be shown on a Navision backend. In Navision the empty currency is the default currency.
        private const string exceptionText1 = "The shop is set to use currencies from the CustomerTable." +
                        " The currency cannot be null or empty. Please check the CustomerTable.";
        // Error message will be shown on any backend
        private const string exceptionText2 = "Currency {0} is not the site currency and" +
                        " is not enabled in the CurrencyTable. Please Make sure all currencies you plan to use other" +
                        " than the site currency are enabled in the CurrencyTable.";

        /// <summary> 
        /// Entry point for ObjectDataSource 
        /// </summary> 
        /// <returns>Returns the primary currency DataTable</returns> 
        /// <remarks></remarks> 
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public DataTable CurrencyTable() {
            return CurrencyDataTable;
        }

        /// <summary> 
        /// Entry point for ObjectDataSource 
        /// </summary> 
        /// <returns>Returns the secondary currency DataTable</returns> 
        /// <remarks></remarks> 
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public DataTable SecondaryCurrencyTable() {
            return SecondaryCurrencyDataTable;
        }

        //protected CurrencyControlLogic() { }
        public CurrencyControlLogic(){}

        /// <summary> 
        /// Constructor prepares CurrencyDataTables 
        /// on object creation 
        /// </summary> 
        /// <param name="currentUser">Dictionary containing user specific information</param> 
        /// <remarks>
        /// Constructor made public as a Workaround made to ensure taht the program won't fail on servers 
        /// running under medium trust without a ReflectionPermission entry in the web_mediumtrust.config file.
        /// </remarks> 
        public CurrencyControlLogic(Dictionary<object, object> currentUser) {
            _currentUser = currentUser;
            // Saftey precaution. Make sure the currency keys exists in the dictionary
            if (!_currentUser.ContainsKey("CurrencyGuid")) { _currentUser.Add("CurrencyGuid", ""); }
            if (!_currentUser.ContainsKey("SecondaryCurrencyGuid")) { _currentUser.Add("SecondaryCurrencyGuid", ""); }
            // Load the default site currency from web.config
            siteCurrency = ConfigurationManager.AppSettings["MULTICURRENCY_SITE_CURRENCY"];
            // Get primary currency DataTable from cache if it exists
            CurrencyDataTable = getCachedData(cacheNamePrimary);
            if (CurrencyDataTable == null) {
                // Load all availible currencies into the CurrencyDataTable
                CurrencyDataTable = loadCurrencies();
                // Make additional preparations of the DataTable
                preparePrimaryCurrencyTable();
                backEndSpecificPreparation();
                // Cache the currency table
                setCacheData(CurrencyDataTable, cacheNamePrimary);
            }
            // Get secondary currency DataTable from cache if it exists
            SecondaryCurrencyDataTable = getCachedData(cacheNameSecondary);
            if (SecondaryCurrencyDataTable == null) {
                // Make a copy of the already prepared currency DataTable. 
                SecondaryCurrencyDataTable = CurrencyDataTable.Copy();
                // Make additional preparation of the secondary table
                prepareSecondaryCurrencyTable();
                // Cache the secondary currency table
                setCacheData(SecondaryCurrencyDataTable, cacheNameSecondary);
            }
            setUserCurrencySetting();
        }

        /// <summary> 
        /// Returns a valid value for a primary currency controls selected attribute 
        /// </summary> 
        /// <value></value> 
        /// <returns></returns> 
        /// <remarks></remarks> 
        public string SelectedValue {
            get { return (string)(hasData() ? _currentUser["CurrencyGuid"] : null); }
        }

        /// <summary>
        /// Returns a valid value for a secondary currency controls selected attribute 
        /// </summary>
        public string SelectedSecondaryValue {
            get {
                if (hasData()) {
                    if (!(_currentUser["SecondaryCurrencyGuid"].Equals(DBNull.Value)) && ((string)(_currentUser["SecondaryCurrencyGuid"]) != string.Empty)) {
                        return (string)_currentUser["SecondaryCurrencyGuid"];
                    }
                }
                return "";
            }
        }

        /// <summary> 
        /// Provides access to the user dictionary 
        /// </summary> 
        /// <value></value> 
        /// <returns></returns> 
        /// <remarks></remarks> 
        protected Dictionary<object, object> CurrentUser {
            get { return _currentUser; }
        }

        /// <summary> 
        /// Uses business logic to set the users currency 
        /// </summary> 
        /// <remarks></remarks> 
        protected abstract void setUserCurrencySetting();

        /// <summary> 
        /// Overridable method. The default implementation sets the users currency to the default
        /// site currency, if the user's currencyGuid is not in the cached currency table, or if
        /// multicurrency is disabled.        
        /// </summary> 
        /// <remarks></remarks> 
        protected virtual void setUserDefaultCurrencySetting() {
            if (!isCurrencyGuidInDataTable((string)_currentUser["CurrencyGuid"]) || !isMultiCurrencyEnabled()) {
                setUserCurrencyGuid(siteCurrency);
            }
        }

        /// <summary> 
        /// Sets the users currency to the supplied currencyGuid value if the currency exists in
        /// the cached currency table or if the currencyGuid is the same as the default site currency
        /// </summary> 
        /// <param name="currencyGuid"></param> 
        /// <remarks></remarks> 
        protected void setUserCurrencyGuid(string currencyGuid) {
            if (isCurrencyGuidInDataTable(currencyGuid) || currencyGuid == siteCurrency) {
                _currentUser["CurrencyGuid"] = currencyGuid;
                HttpContext.Current.Session["UserCurrencyGuid"] = currencyGuid;
            } else {
                if (string.IsNullOrEmpty(currencyGuid)) {
                    throw new Exception(exceptionText1);
                } else {
                    throw new Exception(String.Format(exceptionText2, currencyGuid));
                }
            }
        }

        /// <summary>
        /// Checks if current user has a valid currencyGuid
        /// </summary>
        /// <returns></returns>
        //protected bool haveUserCurrencyGuid() {
        //    return string.IsNullOrEmpty(Utilities.CStrEx(_currentUser["CurrencyGuid"])) == false;
        //}

        /// <summary>
        /// Finds if the provided currency guid exists in the currency DataTable
        /// </summary>
        /// <returns></returns>
        protected bool isCurrencyGuidInDataTable(string currencyGuid) {
            foreach (DataRow row in CurrencyDataTable.Rows) {
                if (row[0].Equals(currencyGuid)) {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Finds if the users currency guid exists in the currency DataTable
        /// </summary>
        /// <returns></returns>
        //protected bool isUserCurrencyGuidInDataTable() {
        //    foreach (DataRow row in CurrencyDataTable.Rows) {
        //        if (row[0].Equals(_currentUser["CurrencyGuid"])) {
        //            return true;
        //        }
        //    }
        //    return false;
        //}

        /// <summary>
        /// Checks if there is data in currency DataTable
        /// </summary>
        /// <returns></returns>
        protected bool hasData() {
            return CurrencyDataTable.Rows.Count > 0;
        }

        /// <summary>
        /// Checks if MultiCurrency is enabled
        /// </summary>
        /// <returns></returns>
        protected bool isMultiCurrencyEnabled() {
            return Utilities.CBoolEx(ConfigurationManager.AppSettings["MultiCurrencyEnabled"]);
        }

        /// <summary>
        /// Loads currencies from the database
        /// </summary>
        /// <returns></returns>
        private DataTable loadCurrencies() {
            return DataAccess.SQL2DataTable(sql);
        }

        /// <summary>
        /// Return the DataTable form cache
        /// </summary>
        /// <param name="cacheName"></param>
        /// <returns></returns>
        private DataTable getCachedData(string cacheName) {
            return (DataTable)HttpContext.Current.Cache.Get(cacheName);
        }

        /// <summary>
        /// Adds the dataTable to the cahce with an aggregated cache dependency
        /// </summary>
        /// <param name="cachedCurrencies"></param>
        /// <param name="cacheName"></param>
        private void setCacheData(DataTable cachedCurrencies, string cacheName) {
            CacheManager.CacheSetAggregated(cacheName, cachedCurrencies, new string[] { "CurrencyTable", "CurrencyExchange" });
        }

        /// <summary> 
        /// Ensures that the DataTable contains the correct values to be used by the different 
        /// currency dropDownLists around the site. 
        /// </summary> 
        /// <remarks></remarks> 
        private void preparePrimaryCurrencyTable() {
            // If the table has data, make sure all currencyGuids have a CurrencyName (not DBNull)
            if (hasData()) {
                foreach (DataRow row in CurrencyDataTable.Rows) {
                    if (row[1].Equals(DBNull.Value) || string.IsNullOrEmpty(row[1].ToString())) {
                        row[1] = row[0];
                    }
                }
            }
            // If the current user doesn't have a valid currencyGuid, then we are running on the default currency
            if (Utilities.NaV(_currentUser["CurrencyGuid"])) {
                _currentUser["CurrencyGuid"] = siteCurrency;
            }
        }

        /// <summary>
        /// Make additional preparation based on back end type
        /// </summary>
        protected abstract void backEndSpecificPreparation();

        /// <summary>
        /// Adds the 'NONE' Field to the secondaryCurrencyTable
        /// </summary>        
        private void prepareSecondaryCurrencyTable() {
            if (!isAdded(SecondaryCurrencyDataTable)) {
                DataRow dr = SecondaryCurrencyDataTable.NewRow();
                dr[0] = DBNull.Value;
                dr[1] = "[None]";
                SecondaryCurrencyDataTable.Rows.Add(dr);
            }
        }

        /// <summary>
        /// Checks if the 'NONE' field is already added
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private bool isAdded(DataTable dt) {
            foreach (DataRow row in dt.Rows) {
                if (row[1].Equals("[None]")) {
                    return true;
                }
            }
            return false;
        }

    }
}
