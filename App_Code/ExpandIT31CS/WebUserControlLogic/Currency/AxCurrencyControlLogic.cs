﻿using System;
using System.Collections.Generic;
using System.Configuration;
using ExpandIT31.ExpanditFramework.Infrastructure;
using ExpandIT31.ExpanditFramework.Util;

namespace ExpandIT {

    /// <summary>
    /// Handles the Axapta specific currency settings 
    /// </summary>
    public class AxCurrencyControlLogic : CurrencyControlLogic {

        private const string customerTableSql = "SELECT CurrencyGuid FROM CustomerTable WHERE CustomerGuid = '{0}'";

        //protected AxCurrencyControlLogic(Dictionary<object, object> currentUser)
        //    : base(currentUser) {
        //}

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="currentUser"></param>
        /// <remarks>
        /// Workaround made to ensure taht the program won't fail on servers running under medium trust without 
        /// a ReflectionPermission entry in the web_mediumtrust.config file.
        /// </remarks>
        public AxCurrencyControlLogic(Dictionary<object, object> currentUser)
            : base(currentUser) {
        }

        protected override void setUserCurrencySetting() {

            string customerGuid = string.Empty;
            string currencyGuid = string.Empty;

            if (Utilities.CBoolEx(ConfigurationManager.AppSettings["AXAPTA_USE_CURRENCY_FROM_CUSTOMER"])) {

                if (!CurrentUser.ContainsKey("CustomerGuid")) {
                    customerGuid = ConfigurationManager.AppSettings["ANONYMOUS_CUSTOMERGUID"];
                } else if (CurrentUser["CustomerGuid"].Equals(DBNull.Value)) {
                    customerGuid = ConfigurationManager.AppSettings["ANONYMOUS_CUSTOMERGUID"];
                } else {
                    customerGuid = Utilities.CStrEx(CurrentUser["CustomerGuid"]);
                }

                try {
                    currencyGuid = Utilities.CStrEx(DataAccess.getSingleValueDB(string.Format(customerTableSql, customerGuid)));
                } catch (Exception ex) {
                    throw new Exception("Could not load currencyGuid from table", ex);
                }

                if (Utilities.NaV(currencyGuid)) {
                    setUserCurrencyGuid(ConfigurationManager.AppSettings["MULTICURRENCY_SITE_CURRENCY"]);
                } else {
                    setUserCurrencyGuid(currencyGuid);
                }

                return;

            }

            setUserDefaultCurrencySetting();

        }

        protected override void backEndSpecificPreparation() { }

    }
}
