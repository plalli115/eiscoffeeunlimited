﻿using System;
using System.Collections.Generic;
using System.Configuration;
using ExpandIT31.ExpanditFramework.Infrastructure;
using ExpandIT31.ExpanditFramework.Util;
using System.Data;

namespace ExpandIT {

    /// <summary>
    /// Handles the Navision specific currency settings 
    /// </summary>
    public class NavCurrencyControlLogic : CurrencyControlLogic {

        private const string customerTableSql = "SELECT CurrencyGuid FROM CustomerTable WHERE CustomerGuid = '{0}'";

        //protected NavCurrencyControlLogic(Dictionary<object, object> currentUser)
        //    : base(currentUser) {
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentUser"></param>
        /// <remarks>
        /// Constructor made public as a Workaround made to ensure taht the program won't fail on servers 
        /// running under medium trust without a ReflectionPermission entry in the web_mediumtrust.config file.
        /// </remarks> 
        public NavCurrencyControlLogic(Dictionary<object, object> currentUser)
            : base(currentUser) {
        }

        protected override void setUserCurrencySetting() {

            string customerGuid = string.Empty;
            string currencyGuid = string.Empty;

            if (Utilities.CBoolEx(ConfigurationManager.AppSettings["ATTAIN_USE_CURRENCY_FROM_BILLTO_CUSTOMER"])) {

                if (!CurrentUser.ContainsKey("CustomerGuid")) {
                    customerGuid = ConfigurationManager.AppSettings["ANONYMOUS_CUSTOMERGUID"];
                } else if (CurrentUser["CustomerGuid"].Equals(DBNull.Value)) {
                    customerGuid = ConfigurationManager.AppSettings["ANONYMOUS_CUSTOMERGUID"];
                } else {
                    customerGuid = Utilities.CStrEx(CurrentUser["CustomerGuid"]);
                }

                try {
                    currencyGuid = (string)DataAccess.getSingleValueDB(string.Format(customerTableSql, customerGuid));
                } catch (Exception ex) {
                    throw new Exception("Could not load currencyGuid from table", ex);
                }
                if (Utilities.NaV(currencyGuid) || currencyGuid == "") {
                    setUserCurrencyGuid(ConfigurationManager.AppSettings["MULTICURRENCY_SITE_CURRENCY"]);
                } else {
                    setUserCurrencyGuid(currencyGuid);
                }

                return;
            }

            setUserDefaultCurrencySetting();

        }

        /// <summary>
        /// If we are using Navision then the default currency could be the empty currency. 
        /// In that case the currency DataTable has no currencyName nor currencyGuid for the default currency.
        /// Not too cool if you run multi currency and cannot select the default currency... 
        /// Therefore we must add currency guid and currency name to our cached DataTable so the 
        /// currency dropdowns can display all values for selection. 
        /// </summary>
        protected override void backEndSpecificPreparation() {            
            if (hasData() && !isCurrencyGuidInDataTable(siteCurrency)) {
                DataRow dr = CurrencyDataTable.NewRow();
                dr[0] = siteCurrency;
                dr[1] = (string)ConfigurationManager.AppSettings["DEFAULT_CURRENCY_NAME"];
                try {
                    CurrencyDataTable.Rows.Add(dr);
                } catch (Exception ex) {
                    throw new Exception("Failed to add data to CurrencyDataTable", ex);
                }
            }            
        }
    }
}
