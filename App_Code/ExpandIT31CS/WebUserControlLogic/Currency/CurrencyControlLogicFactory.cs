﻿using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.UI;
using ExpandIT31.ExpanditFramework.Core;

namespace ExpandIT {

    /// <summary>
    /// Creates and returns the correct instance of the CurrencyControlLogic classes
    /// </summary>
    public class CurrencyControlLogicFactory {
    
        /// <summary>
        /// Removes CurrencyCollection from HttpContext.Current.Items
        /// on postback. Important!!
        /// </summary>
        private static void reloadItemsOnPostback(){
            Page currentPage = HttpContext.Current.Handler as Page;
            if(currentPage.IsPostBack){                            
                HttpContext.Current.Items.Remove("CurrencyControlLogic");
            }
        }        

        /// <summary>
        /// Creates and returns the correct currency logic instance.
        /// Stores the created instance in the Context.Items collection so it can be reused
        /// throughout the lifetime of the request as a "per request" singleton. 
        /// </summary>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        public static CurrencyControlLogic CreateInstance(Dictionary<object, object> currentUser) {
            // Clear Context.Items collection on postback so "old" data don't get used on an update of the user currency
            reloadItemsOnPostback();
            string backEndType = ConfigurationManager.AppSettings["BackendType"];
            switch (backEndType) {
                case "AX":
                    return (HttpContext.Current.Items["CurrencyControlLogic"] ??
                        (HttpContext.Current.Items["CurrencyControlLogic"] =
                            GenericFactory<AxCurrencyControlLogic>.getInstance(currentUser))) as CurrencyControlLogic;
                case "NAV":
                    return (HttpContext.Current.Items["CurrencyControlLogic"] ??
                        (HttpContext.Current.Items["CurrencyControlLogic"] =
                            GenericFactory<NavCurrencyControlLogic>.getInstance(currentUser))) as CurrencyControlLogic;
                default:
                    return (HttpContext.Current.Items["CurrencyControlLogic"] ??
                        (HttpContext.Current.Items["CurrencyControlLogic"] =
                            GenericFactory<NoneCurrencyControlLogic>.getInstance(currentUser))) as CurrencyControlLogic;
            }
        }
    }
}

