﻿using System.Collections.Generic;

namespace ExpandIT {

    /// <summary>
    /// Handles the NONE backed specific currency settings 
    /// </summary>
    public class NoneCurrencyControlLogic : CurrencyControlLogic {

        //protected NoneCurrencyControlLogic(Dictionary<object, object> currentUser)
        //    : base(currentUser) {
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentUser"></param>
        /// <remarks>
        /// Constructor made public as a Workaround made to ensure taht the program won't fail on servers 
        /// running under medium trust without a ReflectionPermission entry in the web_mediumtrust.config file.
        /// </remarks> 
        public NoneCurrencyControlLogic(Dictionary<object, object> currentUser)
            : base(currentUser) {
        }

        protected override void setUserCurrencySetting() {
            setUserDefaultCurrencySetting();
        }

        protected override void backEndSpecificPreparation() { }
    }
}
