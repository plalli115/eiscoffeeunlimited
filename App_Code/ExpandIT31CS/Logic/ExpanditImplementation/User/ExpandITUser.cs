﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Web;
using ExpandIT31.ExpanditFramework.BLLBase;
using ExpandIT31.ExpanditFramework.Infrastructure;
using ExpandIT31.ExpanditFramework.Util;

namespace ExpandIT31.Logic {
    public sealed class ExpandITUser : UserBase {

        private bool useConfirmedRegistration = Utilities.CBoolEx(ConfigurationManager.AppSettings["USE_MAIL_CONFIRMED_REGISTRATION"]);
        private DataTable myData = null;
        private SqlDataAdapter myDataAdapter = null;

        private Dictionary<int, List<string>> returnDict;

        /// <summary>
        /// Public constructor
        /// </summary>
        /// <param name="userGuid"></param>
        /// <remarks>
        /// Constructor made public as a Workaround made to ensure that the program won't fail on servers 
        /// running under medium trust without a ReflectionPermission entry in the web_mediumtrust.config file.
        /// </remarks> 
        public ExpandITUser(string userGuid) {
            LoadRootdata(this, "SELECT * FROM UserTable WHERE UserGuid = " + Utilities.SafeString(userGuid));
            m_OriginalUserPassword = m_UserPassword;
            m_GlobalUserGuid = userGuid;
            returnDict = new Dictionary<int, List<string>>();
        }

        /// <summary>
        /// Loads the data from the UserTable
        /// </summary>
        /// <param name="C"></param>
        /// <param name="sqlQuery"></param>
        /// <remarks>Using reflection</remarks>
        protected override void LoadRootdata(object C, string sqlQuery) {
            myDataAdapter = new SqlDataAdapter();
            myData = DataAccess.getDataTable(sqlQuery, ref myDataAdapter);
            PropertyInfo[] properties = C.GetType().GetProperties();            
            if (myData.Rows.Count > 0) {
                DataRow currRow = myData.Rows[0];
                foreach (PropertyInfo propInfo in properties) {
                    int ordinal = myData.Columns.IndexOf(propInfo.Name);
                    if (ordinal >= 0) {
                        try {
                            if (propInfo.CanWrite) {
                                propInfo.SetValue(C, Utilities.safeDBNull(currRow[ordinal]), null);
                            }
                        } catch (Exception) {
                        }
                    }
                }
            }
        }

        /// <summary> 
        /// Handles the mapping between the public properties and the DataRow columns, and 
        /// transfers the values of the properties to the DataRow columns. 
        /// </summary> 
        /// <param name="C"></param> 
        /// <param name="row"></param> 
        /// <remarks>Using reflection</remarks> 
        private void SetDataRow(object C, DataRow row) {
            PropertyInfo[] properties = C.GetType().GetProperties();
            DataTable currTable = row.Table;
            foreach (PropertyInfo propInfo in properties) {
                int ordinal = currTable.Columns.IndexOf(propInfo.Name);
                if (ordinal >= 0) {
                    try {
                        row[ordinal] = propInfo.GetValue(C, null);
                    } catch (Exception) {
                    }
                }
            }
        }

        /// <summary> 
        /// Handles SQL Update 
        /// </summary> 
        /// <returns></returns> 
        /// <remarks></remarks> 
        private int SQLUpdate() {
            try {
                SetDataRow(this, myData.Rows[0]);
                return myDataAdapter.Update(myData);
            } catch (Exception) {
                return 0;
            }
        }

        /// <summary> 
        /// Handles SQL Insert 
        /// </summary> 
        /// <returns></returns> 
        /// <remarks></remarks> 
        private int SQLInsert() {
            DataRow dr = myData.NewRow();
            try {
                SetDataRow(this, dr);
                myData.Rows.Add(dr);
                return myDataAdapter.Update(myData);
            } catch (Exception) {
                return 0;
            }
        }

        /// <summary> 
        /// ObjectDataSource default select method 
        /// </summary> 
        /// <param name="userGuid"></param> 
        /// <returns>Returns this</returns> 
        /// <remarks></remarks>         
        public override UserBase GetData(string userGuid) {
            return this;
        }

        /// <summary> 
        /// ObjectDataSource default update method 
        /// </summary> 
        /// <param name="Address1"></param> 
        /// <param name="Address2"></param> 
        /// <param name="CityName"></param> 
        /// <param name="CompanyName"></param>
        /// <param name="ResidentialAddress"></param> 
        /// <param name="ContactName"></param> 
        /// <param name="CountryGuid"></param>
        /// <param name="CurrencyGuid"></param> 
        /// <param name="EmailAddress"></param> 
        /// <param name="LanguageGuid"></param> 
        /// <param name="PhoneNo"></param> 
        /// <param name="SecondaryCurrencyGuid"></param> 
        /// <param name="StateName"></param> 
        /// <param name="UserGuid"></param> 
        /// <param name="UserLogin"></param> 
        /// <param name="UserPassword"></param> 
        /// <param name="ZipCode"></param>        
        /// <remarks></remarks>        
        public override Dictionary<int, List<string>> SetData(string Address1, string Address2, string CityName, string CompanyName, Boolean ResidentialAddress, string ContactName, string CountryGuid, string CurrencyGuid, string EmailAddress, string LanguageGuid,
        string PhoneNo, string SecondaryCurrencyGuid, string StateName, string UserGuid, string UserLogin, string UserPassword, string ZipCode, string ConfirmedUserPassword) {
            m_Address1 = Address1;
            m_Address2 = Address2;
            m_CityName = CityName;
            m_CompanyName = CompanyName;
            m_ResidentialAddress = ResidentialAddress;
            m_ContactName = ContactName;
            m_CountryGuid = CountryGuid;
            m_CurrencyGuid = CurrencyGuid;
            m_EmailAddress = EmailAddress;
            m_LanguageGuid = LanguageGuid;
            m_PhoneNo = PhoneNo;
            m_SecondaryCurrencyGuid = SecondaryCurrencyGuid;
            m_StateName = StateName;
            m_UserGuid = UserGuid;
            m_UserLogin = UserLogin;
            m_UserPassword = UserPassword;
            m_ConfirmedUserPassword = ConfirmedUserPassword;
            m_ZipCode = ZipCode;

            if (m_UserGuid != null) {
                m_isUpdate = true;
                updateExistingUser();
            } else {
                createNewUser();
            }

            if ((returnDict.ContainsKey(0))) {
                // Insert/Update operation failed 
                throw new UserValidationException("Message", returnDict[0]);
            } else if (returnDict.ContainsKey(1)) {
                // Insert/Update operation succeeded                 
                if (this.observers != null) {
                    notify(); // Notify registered observers 
                }
            }
            return returnDict;
        }

        /// <summary>
        /// Handles the update of an existing user
        /// </summary>
        private void updateExistingUser() {
            if (handleValidation()) {
                if (!IsLoginTaken(m_UserGuid, m_UserLogin)) {
                    m_UserModified = DateTime.Now;
                    if (m_Status == UserStates.Confirmed)
                        m_Status = UserStates.Updated;
                    if (!Utilities.CBoolEx(ConfigurationManager.AppSettings["USE_MAIL_CONFIRMED_REGISTRATION"])) {
                        m_Status = UserStates.Updated;
                    }
                    if (SQLUpdate() > 0) {
                        System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(m_LanguageGuid);
                        HttpContext.Current.Session["UserCurrencyGuid"] = m_CurrencyGuid;
                        returnDict[1] = new List<string>(new string[] { Resources.Language.MESSAGE_USER_INFORMATION_UPDATED });
                    }
                }
            } else {
                returnDict[0] = new List<string>(new string[] { Utilities.Replace(Resources.Language.MESSAGE_SELECTED_LOGIN_ALREADY_IN_USE, "%1%", m_UserLogin) });
            }
        }

        /// <summary>
        /// Handles the creation of a new user
        /// </summary>
        private void createNewUser() {
            if (handleValidation()) {
                m_Status = UserStates.New;
                if (m_GlobalUserGuid == null) {
                    m_UserGuid = Guid.NewGuid().ToString();
                } else {
                    m_UserGuid = m_GlobalUserGuid;
                }
                if (!IsLoginTaken(m_UserGuid, m_UserLogin)) {
                    m_UserCreated = DateTime.Now;
                    m_UserModified = DateTime.Now;
                    if (SQLInsert() > 0) {
                        string mess = "";
                        if (!useConfirmedRegistration) {
                            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(m_LanguageGuid);
                            HttpContext.Current.Session["UserCurrencyGuid"] = m_CurrencyGuid;
                            mess = Resources.Language.MESSAGE_USER_INFORMATION_CREATED;
                        } else {
                            mess = Resources.Language.MESSAGE_USER_INFORMATION_CREATED;
                        }
                        returnDict[1] = new List<string>(new string[] { mess });
                    } else {
                        returnDict[0] = new List<string>(new string[] { Utilities.Replace(Resources.Language.MESSAGE_SELECTED_LOGIN_ALREADY_IN_USE, "%1%", m_UserLogin) });
                    }
                } else {
                    returnDict[0] = new List<string>(new string[] { Utilities.Replace(Resources.Language.MESSAGE_SELECTED_LOGIN_ALREADY_IN_USE, "%1%", m_UserLogin) });
                }
            }
        }

        /// <summary> 
        /// Handles the contact with the validator (if one is registered). 
        /// </summary> 
        /// <returns> 
        /// The value from the validator if one is registered and in use. 
        /// If no validator is assigned it returns true 
        /// </returns> 
        /// <remarks></remarks> 
        private bool handleValidation() {
            if (this.m_validator != null) {
                if (!m_validator.validate()) {
                    returnDict[0] = m_validator.ErrorMessage;
                    return false;
                }
            }
            return true;
        }

        /// <summary> 
        /// This function determines if a user (UserGuid) can use a particular login or if its taken by another user. 
        /// </summary> 
        /// <param name="UserGuid">The guid of the user, which you are trying to test a new login on.</param> 
        /// <param name="UserLogin">The login which needs to be tested. To be sure it does not already exist.</param> 
        /// <returns>Boolean</returns> 
        /// <remarks></remarks> 
        private bool IsLoginTaken(string UserGuid, string UserLogin) {
            string sql = null;
            bool exists = false;

            // Check UserTable first 
            sql = "SELECT UserLogin FROM UserTable WHERE UserGuid<>" + Utilities.SafeString(UserGuid) + " AND UPPER(UserLogin)=" + Utilities.SafeString(UserLogin).ToUpper();
            exists = existsInDB(sql);

            if (!exists) {
                // Check UserTableB2B if it was not found in the UserTable 
                sql = "SELECT UserLogin FROM UserTableB2B WHERE UserGuid<>" + Utilities.SafeString(UserGuid) + " AND UPPER(UserLogin)=" + Utilities.SafeString(UserLogin).ToUpper();
                exists = existsInDB(sql);
            }
            return exists;
        }

        private bool existsInDB(string sql) {
            try {
                object o = DataAccess.getSingleValueDB(sql);
                return o != null;
            } catch (Exception) {
                //We don't know if user exists, but it's possible 
                return true;
            }
        }
    }
}