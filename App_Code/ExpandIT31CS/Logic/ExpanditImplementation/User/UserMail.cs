﻿using System.Collections.Generic;
using System.Configuration;
using System.Net.Mail;
using ExpandIT31.ExpanditFramework.Core;
using ExpandIT31.ExpanditFramework.Util;
using ExpandIT31.ExpanditFramework.BLLBase;
using System.Text;
using System;
using ExpandIT31.ExpanditFramework.Infrastructure;

namespace ExpandIT31.Logic {

    public class UserMail : UserActionBase {

        private UserBase m_expUser;
        private Dictionary<string, object> m_propDict;
        private PageMessages m_pageMessage;
        private bool useConfirmedRegistration = Utilities.CBoolEx(ConfigurationManager.AppSettings["USE_MAIL_CONFIRMED_REGISTRATION"]);
        private string updateAcountSubject = string.Empty;
        private string newConfirmedAccountSubject = string.Empty;
        private string confirmReqAccountSubject = string.Empty;

        public UserMail(UserBase expUser, PageMessages pageMessage) {
            m_expUser = expUser;
            m_expUser.register(this);
            m_pageMessage = pageMessage;
        }

        public override void update(IObservable obs, object o) {
            string subject = null;
            UserBase expUser = (UserBase)obs;
            m_propDict = Utilities.getPropDict(expUser);
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(expUser.LanguageGuid);
            updateAcountSubject = Resources.Language.LABEL_MAIL_SUBJECT_UPDATED_CUSTOMER_ACCOUNT;
            newConfirmedAccountSubject = Resources.Language.LABEL_MAIL_SUBJECT_NEW_CUSTOMER_ACCOUNT;
            confirmReqAccountSubject = Resources.Language.LABEL_MAIL_SUBJECT_CONFIRM_REQ_CUSTOMER_ACCOUNT;
            switch (expUser.Status) {
                case UserBase.UserStates.New:
                    if (useConfirmedRegistration) {
                        subject = confirmReqAccountSubject;

                    } else {
                        subject = newConfirmedAccountSubject;
                    }
                    break;
                case UserBase.UserStates.Confirmed:
                    subject = newConfirmedAccountSubject;
                    break;
                case UserBase.UserStates.Updated:
                    subject = updateAcountSubject;
                    break;
                //case UserBase.UserStates.Deactivated:
                default:
                    subject = updateAcountSubject;
                    break;
            }
            bool sended = sendCustomerConfirmationEmail(subject, expUser.Status, expUser.EmailAddress, expUser.ContactName, expUser.LanguageGuid);
            if (useConfirmedRegistration && !sended) {
                throw new ApplicationException(Resources.Language.MESSAGE_MAIL_SEND_FAIL);
            }
        }

        protected bool sendCustomerConfirmationEmail(string subject, UserBase.UserStates status, string emailAddress, string contactName, string languageGuid) {
            string message = null;
            message = prepareHtmlMail(Encoding.UTF8, status, languageGuid);
            if (message != null) {
                Dictionary<object, object> recipient = new Dictionary<object, object>();
                recipient.Add(emailAddress, contactName);
                MailMessage mail = Mailer.CreateMailMessage(Utilities.CStrEx(ConfigurationManager.AppSettings["ORDER_EMAIL_FROM"]), recipient, subject, true, Encoding.UTF8);
                string templDir = string.Format("~/{0}/", ConfigurationManager.AppSettings["USER_UPDATE_MAIL_FOLDER"]);
                MailUtils.EmbedImages(message, templDir, ref mail);
                MailUtils.ConfigMailer();
                return Mailer.SendEmail(ConfigurationManager.AppSettings["MAIL_REMOTE_SERVER"], ConfigurationManager.AppSettings["MAIL_REMOTE_SERVER_PORT"], mail, m_pageMessage);
            } else {
                return false;
            }
        }

        protected string prepareHtmlMail(Encoding reqEncoding, UserBase.UserStates status, string languageGuid) {
            string retStr = null;
            string standardLanguageISO = "en";
            string folder = ConfigurationManager.AppSettings["USER_UPDATE_MAIL_FOLDER"];
            string cssFileFolder = ConfigurationManager.AppSettings["CSS_MAIL_FILE_FOLDER"];
            string cssfile = ConfigurationManager.AppSettings["CSS_MAIL_FILE"];
            string languageISO = Utilities.CStrEx(languageGuid.ToLower());

            string filenameTempl = null;
            string filename = null;
            string standardFilename = null;
            string mailPath = null;
            string standardMailPath = null;
            string cssPath = null;
            Dictionary<string, string> extDict = null;

            switch (status) {
                case UserBase.UserStates.New:
                    if (useConfirmedRegistration) {
                        filenameTempl = ConfigurationManager.AppSettings["USER_REG_CONFIRM_REQ_MAIL_NAME_MAIN_PART"] + "{0}." + ConfigurationManager.AppSettings["USER_REG_CONFIRM_REQ_MAIL_SUFFIX"];
                        extDict = makeConfirmReqExtDict();

                    } else {
                        filenameTempl = ConfigurationManager.AppSettings["USER_NEW_MAIL_NAME_MAIN_PART"] + "{0}." + ConfigurationManager.AppSettings["USER_NEW_MAIL_SUFIX"];
                    }
                    break;
                case UserBase.UserStates.Confirmed:
                    filenameTempl = ConfigurationManager.AppSettings["USER_NEW_MAIL_NAME_MAIN_PART"] + "{0}." + ConfigurationManager.AppSettings["USER_NEW_MAIL_SUFIX"];
                    break;
                case UserBase.UserStates.Updated:
                    filenameTempl = ConfigurationManager.AppSettings["USER_UPDATE_MAIL_NAME_MAIN_PART"] + "{0}." + ConfigurationManager.AppSettings["USER_UPDATE_MAIL_SUFFIX"];
                    break;
                //case UserBase.UserStates.Deactivated:
                default:
                    filenameTempl = ConfigurationManager.AppSettings["USER_UPDATE_MAIL_NAME_MAIN_PART"] + "{0}." + ConfigurationManager.AppSettings["USER_UPDATE_MAIL_SUFFIX"];
                    break;
            }
            if (!string.IsNullOrEmpty(filenameTempl)) {
                filename = string.Format(filenameTempl, languageISO);
                standardFilename = string.Format(filenameTempl, standardLanguageISO);
            }

            mailPath = "~/" + folder + "/" + filename;
            standardMailPath = "~/" + folder + "/" + standardFilename;
            cssPath = "~/" + cssFileFolder + "/" + cssfile;
            string templFilePath = null;
            if (FileReader.fileHasContent(mailPath)) {
                templFilePath = mailPath;
            } else if (FileReader.fileHasContent(standardMailPath)) {
                templFilePath = standardMailPath;
            }
            if (templFilePath != null) {
                retStr = MailUtils.EmbedCss(templFilePath, MailUtils.getInlineStyleSheetFromFile(cssPath, reqEncoding));
                Dictionary<object, object> replaceDict = replaceParametersWithUserDictValues("MAIL_USER_REPLACE_PARAMS");
                //extend the replace dictionary with not user data
                if (extDict != null) {
                    foreach (string key in extDict.Keys) {
                        replaceDict.Add(key, extDict[key]);
                    }
                }
                retStr = replaceHtmlFileContent(retStr, replaceDict);
            }

            return retStr;

        }


        private Dictionary<string, string> makeConfirmReqExtDict() {
            Dictionary<string, string> result = new Dictionary<string, string>();
            result.Add("%SHOP_URL%", AppUtil.GetAppRootUrl());
            result.Add("%SHOP_NAME%", Resources.Language.LABEL_EXPANDIT_INTERNET_SHOP);
            string userStr = System.Web.HttpUtility.UrlEncode(m_expUser.UserGuid);
            result.Add("%CONFIRM_URL%", string.Format("{0}/ConfirmReg.aspx?UserRegKey={1}", AppUtil.GetAppRootUrl(), userStr));
            return result;
        }



        public Dictionary<object, object> replaceParametersWithUserDictValues(string value) {
            string[] keyValArray = Utilities.Split(ConfigurationManager.AppSettings[value], "¤");
            Dictionary<object, object> replaceDict = new Dictionary<object, object>();
            for (int i = 0; i <= keyValArray.Length - 1; i++) {
                string[] temp = Utilities.Split(keyValArray[i], "|");
                replaceDict.Add(Utilities.Trim(temp[0]), Utilities.Trim(Utilities.CStrEx(m_propDict[temp[1]])));
            }
            return replaceDict;
        }

        /// <summary> 
        /// Returns the content of a html file and a css file as a string representation 
        /// of one html file with css styles applied, in the requested encoding. 
        /// </summary> 
        /// <param name="htmlStr"></param> 
        /// <returns></returns> 
        /// <remarks></remarks> 
        public string replaceHtmlFileContent(string htmlStr, Dictionary<object, object> replaceDict) {
            string returnStr = null;
            try {
                foreach (KeyValuePair<object, object> item in replaceDict) {
                    htmlStr = Utilities.Replace(htmlStr, Utilities.CStrEx(item.Key), Utilities.CStrEx(item.Value));
                }
                returnStr = htmlStr;
            } catch (Exception) {

            }
            return returnStr;
        }
    }
}
