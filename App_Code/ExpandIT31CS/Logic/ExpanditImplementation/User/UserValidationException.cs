﻿using System;
using System.Collections.Generic;
using System.Web;

namespace ExpandIT31.Logic {

    /// <summary>
    /// Summary description for UserValidationException
    /// </summary>
    public class UserValidationException : Exception {

        private List<string> _errorList;

        public List<string> ErrorList {
            get { return _errorList; }
        }

        public UserValidationException(string errorMessage, List<string> errorList)
            : base(errorMessage) {
            _errorList = errorList;
        }

    }
}