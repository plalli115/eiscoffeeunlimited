﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using ExpandIT31.ExpanditFramework;
using ExpandIT31.ExpanditFramework.Core;
using ExpandIT31.ExpanditFramework.Util;
using ExpandIT31.ExpanditFramework.BLLBase;

namespace ExpandIT31.Logic {

    /// <summary> 
    /// The Class Validates a UserBase object. 
    /// </summary> 
    /// <remarks>Implements the IValidator Interface.</remarks> 
    public class UserValidator : UserValidationBase {

        //Holds a reference to the UserBase object 
        private UserBase m_expUser;
        //Array of strings with the names of the fields to validate 
        private string[] m_validationParameters;
        private string[] m_validationParametersLabels;
        //Flag 
        private bool m_isValid;
        //Collection of error message strings 
        private List<string> m_errorMessage;

        public UserValidator(UserBase expUser, string[] validationParameters, string[] validationParameterLabels) {
            m_expUser = expUser;
            m_validationParameters = validationParameters;
            m_validationParametersLabels = validationParameterLabels;
            //Set the UserBase object's validator to use this instance 
            expUser.setValidator(this); 
        }

        /// <summary> 
        /// Validate E-mail field 
        /// </summary> 
        /// <returns></returns> 
        /// <remarks></remarks> 
        private bool validateMailField() {
            try {
                return Utilities.RegExpTest(m_expUser.EmailAddress, "^[\\w-]+(\\.[\\w-]+)*@([\\w-]+\\.)+[a-zA-Z]{2,7}$");
            } catch (Exception) {
                return false;
            }
        }

        /// <summary> 
        /// Performs the validation. Receives a call from the UserBase object 
        /// </summary> 
        /// <returns>True if validation passed, False if validation failed</returns> 
        /// <remarks></remarks> 
        public override bool validate() {
            m_isValid = true;

            if (!validateMailField()) {
                ErrorString = Resources.Language.MESSAGE_EMAIL_ADDRESS_NOT_VALID;
                m_isValid = false;
            }

            if (!ValidatePassword()) {
                ErrorString = Resources.Language.MESSAGE_PASSWORD_INCORRECT;
                m_isValid = false;
            }            
            
            Dictionary<string, object> propDict = Utilities.getPropDict(m_expUser);
            
            object currentObject = null;
            for (int i = 0; i < m_validationParameters.Length; i++) {
                currentObject = propDict[m_validationParameters[i]];
                try {
                    if (string.IsNullOrEmpty((string)currentObject)) {
                        ErrorString = m_validationParametersLabels[i] + ": " + Resources.Language.MESSAGE_REQUIRED_FIELD;
                        m_isValid = false;
                    }
                } catch (Exception) {
                    //Invalid Cast 
                }
            }

            return m_isValid;

        }

        /// <summary> 
        /// Validates User Password 
        /// </summary> 
        /// <returns></returns> 
        /// <remarks></remarks> 
        private bool ValidatePassword() {
            string NewPassword1 = null;
            string NewPassword2 = null;
            
            NewPassword1 = Utilities.CStrEx(m_expUser.UserPassword).Trim();
            NewPassword2 = Utilities.CStrEx(m_expUser.ConfirmedUserPassword).Trim();
            //New User 
            if (string.IsNullOrEmpty(m_expUser.UserGuid)) {
                if (NewPassword1 == NewPassword2 && !string.IsNullOrEmpty(NewPassword1)) {
                    //Respect the settings in web.config 
                    if (NewPassword1.Length >= Utilities.CLngEx(ConfigurationManager.AppSettings["PASSWORD_LEN"])) {
                        return (m_expUser.UserPassword = Utilities.EncryptXOrWay(NewPassword1)) != "";                        
                    }
                }
            }
                //Existing User 
            else {
                if (NewPassword1 == NewPassword2 && !string.IsNullOrEmpty(NewPassword1)) {
                    //if (Utilities.EncryptXOrWay(NewPassword1).Equals(m_expUser.OriginalUserPassword)) { JA20101129 - not validate password
                        return (m_expUser.UserPassword = Utilities.EncryptXOrWay(NewPassword1)) != "";
                    //}
                }
            }
            return false;
        }

        public override bool isValid() {
            return m_isValid;
        }

        /// <summary> 
        /// Returns a List of Error Message Strings 
        /// </summary> 
        /// <value></value> 
        /// <returns>List(Of String)</returns> 
        /// <remarks></remarks> 
        public override List<string> ErrorMessage {
            get { return m_errorMessage; }
        }

        /// <summary> 
        /// Private Property used to add Error Strings to the m_errorMessage List(Of String) 
        /// </summary> 
        /// <value></value> 
        /// <remarks></remarks> 
        private string ErrorString {
            set {
                if (m_errorMessage == null) {
                    m_errorMessage = new List<string>();
                }
                m_errorMessage.Add(value);
            }
        }

    }
}