﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;

namespace ExpandIT31.Logic
{
    /// <summary>
    /// Sorts the Product Dictionary
    /// </summary>
    public class ProductSorter : IEnumerable, IComparer
    {
        private string m_sortedFieldName;
        private Dictionary<object, object> m_dataSource;
        private Dictionary<object, object> sorted_dataSource;
        private Type m_FieldType;
        private object[] keys;
        private object[] items;

        public Dictionary<object, object> DataSource
        {
            get
            {
                return m_dataSource;
            }
            set
            {
                if (m_dataSource != value)
                {
                    m_dataSource = value;
                    if (!string.IsNullOrEmpty(m_sortedFieldName))
                    {
                        MakeSort(m_sortedFieldName);
                    }
                }
            }
        }

        public Dictionary<object, object> SortedDataSource
        {
            get
            {
                return sorted_dataSource;
            }
            set
            {
                if (sorted_dataSource != value)
                {
                    sorted_dataSource = value;
                }
            }
        }

        public string sortFieldName
        {
            get
            {
                return m_sortedFieldName;
            }
            set
            {
                if ((string.Compare(m_sortedFieldName, value) != 0) && (m_dataSource != null))
                {
                    MakeSort(value);
                    m_sortedFieldName = value;
                }
            }
        }



        private void MakeSort(string fieldName)
        {
            int size = m_dataSource.Count;
            keys = new object[size];
            items = new object[size];
            m_FieldType = null;
            int index = 0;
            Dictionary<object, object> tempDict=new Dictionary<object,object>(); 
            foreach (KeyValuePair<object, object> item in m_dataSource)
            {
                Dictionary<object, object> product = (Dictionary<object, object>)item.Value;
                object property = null;
                if (product.TryGetValue(fieldName, out property))
                {
                    if (m_FieldType == null)
                    {
                        m_FieldType = property.GetType();
                    }
                    keys[index] = property;
                    items[index] = item.Key;
                    index++;
                }
            }
            Array.Sort(keys, items, this);
            if (items != null)
            {
                foreach (object item in items)
                {
                    tempDict.Add(item, m_dataSource[item]);
                }
                SortedDataSource=tempDict;
            }
        }

        #region IEnumerable Member


        public IEnumerator GetEnumerator()
        {
            if (items != null)
            {
                foreach (object item in items)
                {
                    yield return new KeyValuePair<object, object>(item, m_dataSource[item]);
                }
            }
        }

        #endregion

        #region IComparer Member

        public int Compare(object x, object y)
        {

            if (m_FieldType == typeof(string))
            {
                return string.Compare((string)x, (string)y);
            }
            else
            {
                if (m_FieldType == typeof(int))
                {
                    return Math.Sign((int)x - (int)y);
                }
                else
                {
                    if ((m_FieldType == typeof(Decimal)) || (m_FieldType == typeof(float)))
                    {
                        return Math.Sign((Decimal)x - (Decimal)y);
                    }
                    else
                    {
                        if (m_FieldType == typeof(decimal))
                        {
                            return Math.Sign((decimal)x - (decimal)y);
                        }
                        else
                        {
                            if (m_FieldType == typeof(double))
                            {
                                return Math.Sign((double)x - (double)y);
                            }
                            else
                            {
                                throw new NotImplementedException(string.Format("Can't compare objects of type {0}", m_FieldType.Name));
                            }
                        }
                    }
                }
            }

        #endregion
        }
    }
}
