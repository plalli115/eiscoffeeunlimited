﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using ExpandIT31.ExpanditFramework.BLLBase;
using ExpandIT31.ExpanditFramework.Infrastructure;
using ExpandIT31.ExpanditFramework.Util;

namespace ExpandIT31.Logic {
    /// <summary>
    /// Summary description for ExpandITB2BUser
    /// </summary>
    public sealed class ExpandITB2BUser : UserB2BBase {

        private DataTable myData = null;
        private SqlDataAdapter myDataAdapter = null;
        
        public ExpandITB2BUser(string userGuid) {
            LoadRootdata(this, "SELECT * FROM UserTable WHERE UserGuid = " + Utilities.SafeString(userGuid)); //+ 
            //" AND IsB2B <> 0");
        }

        protected override void LoadRootdata(object C, string sqlQuery) {
            myDataAdapter = new SqlDataAdapter();
            myData = DataAccess.getDataTable(sqlQuery, ref myDataAdapter);

            PropertyInfo[] properties = C.GetType().GetProperties();

            foreach (PropertyInfo propInfo in properties) {
                try {
                    propInfo.SetValue(C, Utilities.safeDBNull(myData.Rows[0][propInfo.Name]), null);
                } catch (Exception) {

                }
            }
        }

        public override UserB2BBase GetData(string userGuid) {
            return this;
        }        
        
    }
}