﻿using System;
using System.Web;
using EISCS.ExpandITFramework.Infrastructure.SqlDependencyCaching.Interfaces;

namespace EISCS.ExpandITFramework.Infrastructure.SqlDependencyCaching {
    public class SqlCacheDependencyContext : ISqlCacheDependencyContext {

        #region ISqlCacheDependencyContext Members

        public string MapPath(string path) {
            return HttpContext.Current.Server.MapPath(path);
        }        

        public void CacheInsert(string key, object value, System.Web.Caching.CacheDependency dependencies) {
            HttpContext.Current.Cache.Insert(key, value, dependencies);
        }

        public void CacheInsert(string key, object value, System.Web.Caching.CacheDependency dependencies, 
            DateTime absoluteExpiration, TimeSpan slidingExpiration) {
            HttpContext.Current.Cache.Insert(key, value, dependencies, absoluteExpiration, slidingExpiration);
        }

        public void CacheInsert(string key, object value, System.Web.Caching.CacheDependency dependencies, 
            DateTime absoluteExpiration, TimeSpan slidingExpiration, System.Web.Caching.CacheItemUpdateCallback onUpdateCallback) {
            HttpContext.Current.Cache.Insert(key, value, dependencies, absoluteExpiration, slidingExpiration, onUpdateCallback);
        }

        public void CacheInsert(string key, object value, System.Web.Caching.CacheDependency dependencies, 
            DateTime absoluteExpiration, TimeSpan slidingExpiration, System.Web.Caching.CacheItemPriority priority, System.Web.Caching.CacheItemRemovedCallback onRemoveCallback) {
            HttpContext.Current.Cache.Insert(key, value, dependencies, absoluteExpiration, slidingExpiration, priority, onRemoveCallback);
        }

        #endregion
    }
}
