﻿namespace EISCS.ExpandITFramework.Infrastructure.SqlDependencyCaching.Interfaces {
    public interface ISqlCacheDependencyPermissionChecker {
        bool CheckDdlTriggerPermission();
        bool CheckRoles();
        bool CheckSqlCacheDependencyPermissions();
    }
}
