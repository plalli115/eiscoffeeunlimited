﻿using System.Data;

namespace EISCS.ExpandITFramework.Infrastructure.SqlDependencyCaching.Interfaces {
    public interface ISqlExecutor {
        bool IsLocalMode { get; }
        object GetSingleValueDb(string sql);
        string ExcecuteNonQueryDb(string cmdText);
        DataTable Sql2DataTable(string sql);
    }
}
