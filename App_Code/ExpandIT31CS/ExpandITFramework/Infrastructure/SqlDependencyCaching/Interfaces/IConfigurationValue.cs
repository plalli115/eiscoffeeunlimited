﻿namespace EISCS.ExpandITFramework.Infrastructure.SqlDependencyCaching.Interfaces {
    public interface IConfigurationValue {
        string AppSettings(string name);
    }
}
