﻿using EISCS.ExpandITFramework.Infrastructure.SqlDependencyCaching.Interfaces;
using ExpandIT31.ExpanditFramework.Infrastructure;

namespace EISCS.ExpandITFramework.Infrastructure.SqlDependencyCaching {
    public class SqlExecutor : ISqlExecutor {
        #region ISqlExecutor Members

        public bool IsLocalMode {
            get { return DataAccess.IsLocalMode; }
        }

        public object GetSingleValueDb(string sql) {
            return DataAccess.getSingleValueDB(sql);
        }

        public string ExcecuteNonQueryDb(string cmdText) {
            return DataAccess.excecuteNonQueryDb(cmdText);
        }

        public System.Data.DataTable Sql2DataTable(string sql) {
            return DataAccess.SQL2DataTable(sql);
        }

        #endregion
    }
}
