﻿using EISCS.ExpandITFramework.Infrastructure.SqlDependencyCaching.Interfaces;

namespace EISCS.ExpandITFramework.Infrastructure.SqlDependencyCaching {
    public class ConfigurationValue : IConfigurationValue {

        #region IAppSettingValue Members

        public string AppSettings(string name) {
            return System.Configuration.ConfigurationManager.AppSettings[name];
        }

        #endregion
    }
}
