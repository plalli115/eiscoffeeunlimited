﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using ExpandIT31.ExpanditFramework.Util;

namespace ExpandIT31.ExpanditFramework.Infrastructure {

    /// <summary>
    /// Summary description for DataAccess
    /// </summary>
    public class DataAccess {
        private static string ConnStr;
        private static string ConnStrName;     
        private static bool isLocalModeVB;
        private static bool isSqlServer2005;

        static DataAccess() {
            try {    
                if(!isRemoteMode()){
                    isLocalModeVB = true;
                }
                ConnStr = System.Configuration.ConfigurationManager.ConnectionStrings["ExpandITConnectionString"].ConnectionString;
                ConnStrName = "ExpandITConnectionString";
            } catch (Exception) {
            }
            findSqlServerVersion();
        }

        /// <summary> 
        /// Finds out if the running DB is SQLServer 2005 Or Newer 
        /// </summary> 
        /// <remarks></remarks> 
        private static void findSqlServerVersion() {
            try {
                object vers = getSingleValueDB("SELECT SERVERPROPERTY('productversion')");
                string str = (string)vers;
                string[] sArr = Utilities.Split(str, ".");
                if (int.Parse(sArr[0]) > 8) {
                    isSqlServer2005 = true;
                }
            } catch (Exception) {
                isSqlServer2005 = false;
            }
        }

        /// <summary> 
        /// Finds out if this is a remote or local site 
        /// </summary> 
        /// <returns>Boolean</returns> 
        /// <remarks></remarks> 
        private static bool isRemoteMode() {
            string str = HttpRuntime.AppDomainAppPath + "expandITRemote.xml";
            return System.IO.File.Exists(str);
        }

        /// <summary> 
        /// Checks if the site is running in local or remote mode 
        /// </summary> 
        /// <value></value> 
        /// <returns>True if the site is running in local mode</returns> 
        /// <remarks></remarks> 
        public static bool IsLocalMode {
            get { return isLocalModeVB; }
        } 

        /// <summary> 
        /// Finds out if the running DB is SQL Server 2005 
        /// </summary> 
        /// <value></value> 
        /// <returns>True if Server is SQL Server 2005</returns> 
        /// <remarks></remarks> 
        public static bool IsSQL2005 {
            get { return isSqlServer2005; }
        }

        /// <summary> 
        /// Get or Set the current ConnectionStringName used by this module 
        /// </summary> 
        /// <value>String</value> 
        /// <returns>The name of the connectionstring</returns> 
        /// <remarks></remarks> 
        public static string CurrentConnectionString {
            get { return ConnStr; }
            set {
                try {
                    ConnStr = value;
                } catch (Exception) {

                }
            }
        } 

        /// <summary> 
        /// Get or Set the current ConnectionStringName used by this module 
        /// </summary> 
        /// <value>String</value> 
        /// <returns>The name of the connectionstring</returns> 
        /// <remarks></remarks> 
        public static string CurrentConnectionStringName {
            get { return ConnStrName; }
            set {
                try {
                    ConnStrName = value;
                } catch (Exception) {

                }
            }
        } 

        #region "Data Access"

        /// <summary> 
        /// Function returns a single value as a result from a database query 
        /// </summary> 
        /// <param name="sql">The SQL-Statement to excecute</param> 
        /// <returns>A single value of type Object</returns> 
        /// <remarks></remarks> 
        public static object getSingleValueDB(string sql) {
            SqlCommand oc = default(SqlCommand);
            object ret = null;
            using (SqlConnection localConn = new SqlConnection(ConnStr)) {
                try {
                    localConn.Open();
                    oc = new SqlCommand(sql, localConn);
                    oc.CommandType = CommandType.Text;
                    ret = oc.ExecuteScalar();
                } catch (Exception) {
                }
            }
            return ret;
        }


        /// <summary> 
        /// Executes Any SQL-statement. Does not return a reultset. 
        /// </summary> 
        /// <param name="cmdText">SQL-Statement</param> 
        /// <remarks> 
        /// Useful for queries that don't require a resultset 
        /// </remarks> 
        public static string excecuteNonQueryDb(string cmdText) {
            SqlCommand oc = default(SqlCommand);
            string message = string.Empty;

            using (SqlConnection localConn = new SqlConnection(ConnStr)) {
                try {
                    localConn.Open();
                    oc = new SqlCommand(cmdText, localConn);
                    oc.CommandType = CommandType.Text;
                    oc.ExecuteNonQuery();
                } catch (Exception ex) {
                    message = ex.Message;
                }
            }
            return message;
        }

        /// <summary> 
        /// This function returns a filled DataTable 
        /// </summary> 
        /// <param name="inSqlCommand">A SQL-Statement</param> 
        /// <param name="da">A SqlDataAdapter</param> 
        /// <returns>DataTable</returns> 
        /// <remarks></remarks> 
        public static DataTable getDataTable(string inSqlCommand, ref SqlDataAdapter da) {
            SqlDataAdapter od = default(SqlDataAdapter);
            DataTable dt = new DataTable();

            if (da != null) {
                da = getDataAdapter(null, inSqlCommand);
                da.Fill(dt);
            } else {
                od = getDataAdapter(null, inSqlCommand);
                od.Fill(dt);
            }
            return dt;

        }

        /// <summary> 
        /// This function returns a DataAdapter tied to a Database table 
        /// </summary> 
        /// <param name="tableName">The name of the db-table</param> 
        /// <param name="sqlSelectCommand">The SQL-Statement</param> 
        /// <returns>SqlDataAdapter</returns> 
        /// <remarks> 
        /// This Function creates commands for delete, insert and update, by using a CommandBuilder. 
        /// It uses the MissingSchemaAction.AddWithKey option, so the db-table must contain at least 
        /// a unique index, to make the CommandBuilder work. 
        /// </remarks> 
        public static SqlDataAdapter getDataAdapter(string tableName, string sqlSelectCommand) {

            SqlDataAdapter da = default(SqlDataAdapter);
            SqlCommandBuilder dcmd = default(SqlCommandBuilder);

            if (sqlSelectCommand == null && tableName != null) {
                da = new SqlDataAdapter("SELECT * FROM " + tableName + " WHERE 1 = 2", ConnStr);
            }
            if (sqlSelectCommand != null && tableName == null) {
                da = new SqlDataAdapter(sqlSelectCommand, ConnStr);
            }
            if (sqlSelectCommand != null && tableName != null) {
                da = new SqlDataAdapter(sqlSelectCommand + tableName, ConnStr);
            }

            try {
                dcmd = new SqlCommandBuilder(da);
                da.DeleteCommand = dcmd.GetDeleteCommand();
                da.InsertCommand = dcmd.GetInsertCommand();
                da.UpdateCommand = dcmd.GetUpdateCommand();
                da.MissingSchemaAction = MissingSchemaAction.AddWithKey;
            } catch (Exception) {
            }

            return da;

        }

        /// <summary> 
        /// Populates a DataTable with the result from the provided SQL-Query 
        /// </summary> 
        /// <param name="sql"></param> 
        /// <returns>DataTable</returns> 
        /// <remarks></remarks> 
        public static DataTable SQL2DataTable(string sql) {
            SqlDataAdapter da = default(SqlDataAdapter);
            SqlConnection conn = GetDBConnection();
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandTimeout = 0;
            //Don't time out 
            DataTable dt = new DataTable();
            da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            da.Dispose();
            conn.Close();
            return dt;
        }

        /// <summary> 
        /// Returns an open SqlConnection Object 
        /// </summary> 
        /// <returns></returns> 
        /// <remarks></remarks> 
        private static SqlConnection GetDBConnection() {
            SqlConnection conn = new SqlConnection(ConnStr);
            conn.Open();
            return conn;
        }

        #endregion
    }
}