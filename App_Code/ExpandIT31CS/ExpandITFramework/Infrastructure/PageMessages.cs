﻿using System.Collections;
using System.Collections.Generic;
using System.Web;
using ExpandIT31.ExpanditFramework.Util;
using System;
using System.Text;

namespace ExpandIT31.ExpanditFramework.Infrastructure {

    public class PageMessages {
        public List<string> Actions;
        public List<string> Errors;
        public List<string> Warnings;
        public List<string> Messages;


        public enum MessageType : byte {
            actions,
            errors,
            warnings,
            messages
        }

        public PageMessages() {

            Actions = GetMsgsFromContext(MessageType.actions);
            Errors = GetMsgsFromContext(MessageType.errors);
            Messages = GetMsgsFromContext(MessageType.messages);
            Warnings = GetMsgsFromContext(MessageType.warnings);

        }

        private List<string> GetMsgsFromContext(MessageType messType) {
            List<string> retv = new List<string>();
            try {
                string mess_type_name = Enum.GetName(typeof(MessageType), messType);
                //1. get from request
                string[] strings = null;
                strings = Utilities.Split(HttpContext.Current.Request[mess_type_name], "\t");
                if (strings != null) {
                    foreach (string s in strings) {
                        if (!string.IsNullOrEmpty(s)) retv.Add(s);
                    }
                }
                //2. get from context, and merge
                strings = Utilities.Split((string)HttpContext.Current.Items[mess_type_name], "\t");
                if (strings != null) {
                    foreach (string s in strings) {
                        if (!string.IsNullOrEmpty(s) && (!retv.Contains(s))) {
                            retv.Add(s);
                        }
                    }
                }
            } catch (Exception) {
            }
            return retv;
        }

        public string ContextItems() {
            return ContextItems(MessageType.messages);
        }

        public string ContextItems(MessageType messType) {
            StringBuilder builder = new StringBuilder();
            List<string> msg_list = null;
            switch (messType) {
                case MessageType.actions:
                    msg_list = Actions;
                    break;
                case MessageType.errors:
                    msg_list = Errors;
                    break;
                case MessageType.messages:
                    msg_list = Messages;
                    break;
                case MessageType.warnings:
                    msg_list = Warnings;
                    break;
                default:
                    break;
            }
            if (msg_list != null) {
                foreach (string itm in msg_list) {
                    builder.AppendFormat("{0}\t", itm);
                }
            }
            return builder.ToString();
        }

        public void SaveToContextItems(IDictionary context_items) {
            foreach (MessageType mt in Enum.GetValues(typeof(MessageType))) {
                string mess_type_name = Enum.GetName(typeof(MessageType), mt);
                string mess_value = ContextItems(mt);
                if (!string.IsNullOrEmpty(mess_value)) {
                    context_items.Add(mess_type_name, mess_value);
                }
            }
        }

        /// <summary> 
        /// Returns the messages in the current object as query string parameters. 
        /// </summary> 
        /// <returns>A part of a query string.</returns> 
        /// <remarks>Use this function when you want to pass the state of the message object to another page.</remarks> 
        public string QueryString() {
            string retv = "";
            string s = null;

            s = "";
            foreach (string itm in Actions) {
                s = s + (!string.IsNullOrEmpty(s) ? "%09" : "") + HttpUtility.UrlEncode(itm);
            }
            if (!string.IsNullOrEmpty(s)) retv = retv + (!string.IsNullOrEmpty(retv) ? "&" : "") + "actions=" + s;

            s = "";
            foreach (string itm in Errors) {
                s = s + (!string.IsNullOrEmpty(s) ? "%09" : "") + HttpUtility.UrlEncode(itm);
            }
            if (!string.IsNullOrEmpty(s)) retv = retv + (!string.IsNullOrEmpty(retv) ? "&" : "") + "errors=" + s;

            s = "";
            foreach (string itm in Warnings) {
                s = s + (!string.IsNullOrEmpty(s) ? "%09" : "") + HttpUtility.UrlEncode(itm);
            }
            if (!string.IsNullOrEmpty(s)) retv = retv + (!string.IsNullOrEmpty(retv) ? "&" : "") + "warnings=" + s;

            s = "";
            foreach (string itm in Messages) {
                s = s + (!string.IsNullOrEmpty(s) ? "%09" : "") + HttpUtility.UrlEncode(itm);
            }
            if (!string.IsNullOrEmpty(s)) retv = retv + (!string.IsNullOrEmpty(retv) ? "&" : "") + "messages=" + s;

            return retv;
        }
    }
}
