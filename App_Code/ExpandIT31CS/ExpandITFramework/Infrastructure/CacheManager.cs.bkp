﻿using System.Data;
using System.Web;
using System.Web.Caching;
using System.Xml;
using ExpandIT31.ExpanditFramework.Util;
using System;
using System.Text;
using System.Collections;
using System.Configuration;

namespace ExpandIT31.ExpanditFramework.Infrastructure {
    /// <summary> 
    /// CacheManager Class is used to setup and maintain ASP.Net Caching with SQL Cache Dependency. 
    /// This class is made to work on Security Level "Medium Trust". Thats why we are setting up all 
    /// infrastructure via SQL statements instead of using the built in SqlCacheDependencyAdmin class, 
    /// which demands "High Trust" to operate. 
    /// </summary> 
    /// <remarks></remarks> 
    public class CacheManager {

        private static string[] cachingTables = { };
        private static string DBName;
        private static string ConnectionStringName;
        private static string ConnectionString;
        private static bool hasDDLPermission = false;

        public static string DataBaseName {
            get { return DBName; }
        }

        static CacheManager() {
            // Set up the SQLCacheDependency Infrastructure 
            DataAccess.excecuteNonQueryDb(createEnableSqlCahceDependencyScript());
            if (DataAccess.IsSQL2005) {
                // If SQL Server version is 2005 or newer, and we have the right permissions, we're setting up a ddl-trigger to 
                // catch recreation of database tables during upload, extract, inject etc. 
                try {
                    if(DataAccess.getSingleValueDB("SELECT permission_name FROM fn_my_permissions(NULL, 'DATABASE') WHERE permission_name = 'ALTER ANY DATABASE DDL TRIGGER'")!= DBNull.Value){
                        hasDDLPermission = true;
                        DataAccess.excecuteNonQueryDb(ddlTriggerScript());
                    }                 
                } catch (Exception) {

                }
                try {
                    DataAccess.excecuteNonQueryDb(spTriggerScript());
                } catch (Exception) {

                }
            }
            // Get the name of the current ConnectionString 
            ConnectionStringName = DataAccess.CurrentConnectionStringName;
            // Get Current Database name 
            readConfigCacheSettings();
            try {
                ConnectionString = DataAccess.CurrentConnectionString;
                checkCachingTables();
            } catch (Exception) {

            }
        }

        /// <summary> 
        /// Read web.config cacheSettings 
        /// </summary> 
        /// <remarks></remarks> 
        private static void readConfigCacheSettings() {
            string path = HttpContext.Current.Server.MapPath("~/web.config");
            XmlDocument configs = new XmlDocument();

            try {
                configs.Load(path);
                XmlNamespaceManager mgr = new XmlNamespaceManager(configs.NameTable);
                string prefix = string.Empty;

                if (!string.IsNullOrEmpty(configs.DocumentElement.NamespaceURI)) {
                    prefix = "x:";
                    mgr.AddNamespace("x", configs.DocumentElement.NamespaceURI);
                }

                XmlNodeList xnl = configs.SelectNodes(prefix + "configuration/" + prefix + "system.web/" + prefix + "caching/" + prefix + "sqlCacheDependency/" + prefix + "databases/" + prefix + "add", mgr);

                foreach (XmlNode xn in xnl) {
                    if (string.Equals(xn.Attributes.GetNamedItem("connectionStringName").Value, ConnectionStringName)) {
                        DBName = xn.Attributes.GetNamedItem("name").Value;
                        break;
                    }
                }
            } catch (Exception) {

            }
        }

        /// <summary> 
        /// Get all tables enabled for cache dependency 
        /// </summary> 
        /// <remarks></remarks> 
        private static void checkCachingTables() {
            string str = "exec AspNet_SqlCacheQueryRegisteredTablesStoredProcedure";
            DataTable dt = DataAccess.SQL2DataTable(str);
            cachingTables = new string[dt.Rows.Count + 1];
            int i = 0;
            foreach (DataRow row in dt.Rows) {
                cachingTables[i] = row[0].ToString();
                i += 1;
            }
        }

        /// <summary> 
        /// Find out if dependency caching is enabled on specified table 
        /// </summary> 
        /// <param name="tableName"></param> 
        /// <value></value> 
        /// <returns></returns> 
        /// <remarks></remarks> 
        public static bool CacheEnabled(string tableName) {
            return Array.IndexOf(cachingTables, tableName) >= 0;             
        }

        /// <summary> 
        /// During a full upload some or all of the tables are dropped and recreated, and our SqlCacheUpdate-triggers 
        /// no longer exist.When this happens we need to invalidate the cache and recreate the triggers in order to 
        /// maintain a cache with the correct SqlDependency. In SQL Server 2005 and later we are using ddl-triggers 
        /// and stored procedures to do this work, but in SQL Server 2000 there are no ddl-triggers. 
        /// In case of running on a SQL Server 2000 we have to do this in code. 
        /// Also if ALTER ANY DATABASE DDL TRIGGER is not granted to the current user then this code will be run.
        /// </summary> 
        /// <param name="TableArray">An ArrayList containing the names of the tables that has been recreated</param> 
        /// <remarks></remarks> 
        public static void TablesRecreated(ArrayList TableArray) {
            if (!hasDDLPermission) {
                if (TableArray != null && TableArray.Count > 0) {
                    for (int i = 0; i <= TableArray.Count - 1; i++) {
                        if (CacheEnabled((string)TableArray[i])) {
                            recreateDependency((string)TableArray[i]);
                        }
                    }
                } else {
                    for (int i = 0; i <= cachingTables.Length - 1; i++) {
                        recreateDependency(cachingTables[i]);
                    }
                }
            }
        }

        /// <summary> 
        /// During a full upload some or all of the tables are dropped and recreated, and our SqlCacheUpdate-triggers 
        /// no longer exist. When this happens we need to invalidate the cache and recreate the triggers in order to 
        /// maintain a cache with the correct SqlDependency 
        /// </summary> 
        /// <param name="tableName"></param> 
        /// <remarks></remarks> 
        private static void recreateDependency(string tableName) {
            string sql = "SELECT TOP 1 name FROM " + DBName + ".sys.triggers WHERE NAME = '" + tableName + "_AspNet_SqlCacheNotification_Trigger'";
            object o = DataAccess.getSingleValueDB(sql);
            if (o == null) {
                // Notify cache that a change has occurred in a dependent table 
                DataAccess.excecuteNonQueryDb("AspNet_SqlCacheUpdateChangeIdStoredProcedure @tableName = " + tableName);
                // Recreate the trigger 
                RecreateTrigger(tableName);
            }
        }

        /// <summary> 
        /// Helper function for recreateDependency. It creates a SqlCacheNotification-trigger on the specified table 
        /// </summary> 
        /// <param name="tableName"></param> 
        /// <remarks></remarks> 
        private static void RecreateTrigger(string tableName) {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("CREATE TRIGGER [" + tableName + "_AspNet_SqlCacheNotification_Trigger] ON [" + tableName + "]");
            sb.AppendLine(" FOR INSERT, UPDATE, DELETE AS BEGIN");
            sb.AppendLine(" SET NOCOUNT ON");
            sb.AppendLine(" EXEC AspNet_SqlCacheUpdateChangeIdStoredProcedure N'" + tableName + "'");
            sb.AppendLine(" END");
            DataAccess.excecuteNonQueryDb(sb.ToString());
        }

        /// <summary> 
        /// Insert object with single dependency into dependency cache 
        /// </summary> 
        /// <param name="tableName"></param> 
        /// <param name="cacheObject"></param> 
        /// <returns></returns> 
        /// <remarks>True on success</remarks> 
        public static bool InsertIntoCache(string tableName, object cacheObject) {
            if (CacheEnabled(tableName)) {
                try {
                    HttpContext.Current.Cache.Insert(tableName, cacheObject, new SqlCacheDependency(DBName, tableName));
                    return true;
                } catch (Exception ex) {
                    throw new Exception("Could not use table " + tableName + " for SQLCacheDependency", ex);
                }
            }else{
                return false;
            }
        }

        /// <summary> 
        /// Overloaded Function inserts object with single dependency into dependency cache 
        /// </summary> 
        /// <param name="key"></param> 
        /// <param name="tableName"></param> 
        /// <param name="cacheObject"></param> 
        /// <returns></returns> 
        /// <remarks></remarks> 
        public static bool InsertIntoCache(string key, string tableName, object cacheObject) {
            if (CacheEnabled(tableName)) {
                try {
                    HttpContext.Current.Cache.Insert(key, cacheObject, new SqlCacheDependency(DBName, tableName));
                    return true;
                } catch (Exception ex) {
                    throw new Exception("Could not use table " + tableName + " for SQLCacheDependency", ex);
                }
            }else{
                return false;
            }
        }

        /// <summary> 
        /// Insert object with aggregated dependency into dependency cache 
        /// </summary> 
        /// <param name="key"></param> 
        /// <param name="cacheObject"></param> 
        /// <param name="aggDependency"></param> 
        /// <returns></returns> 
        /// <remarks></remarks> 
        public static bool InsertIntoCache(string key, object cacheObject, AggregateCacheDependency aggDependency) {
            try {
                HttpContext.Current.Cache.Insert(key, cacheObject, aggDependency);
                return true;
            } catch (Exception ) {
                return false;
            }
        }

        public static bool InsertIntoCache(string key, object cacheObject, CacheDependency dependency, System.DateTime d, TimeSpan t, CacheItemPriority cip, ref CacheItemRemovedCallback circ) {
            try {
                HttpContext.Current.Cache.Insert(key, cacheObject, dependency, d, t, cip, circ);
                return true;
            } catch (Exception) {
                return false;
            }
        }

        /// <summary> 
        /// Insert an object with single dependency into cache. 
        /// </summary> 
        /// <param name="key">The key that will identify the object in cache</param> 
        /// <param name="v">The object to cache</param> 
        /// <param name="tableName">String containing the tablename that the object depends on</param> 
        /// <remarks> 
        /// Adds an object to cache with a dependency on a single table. 
        /// A change in the dependent table will invalidate and empty the cache. 
        /// </remarks> 
        internal static void CacheSet(string key, object v, string tableName) {
            if (!DataAccess.IsLocalMode || Utilities.CBoolEx(ConfigurationManager.AppSettings["LOCALMODE_USE_CACHE"])) {
                try {
                    CacheManager.InsertIntoCache(key, tableName, v);
                } catch (Exception) {
                   
                }
            }
        } 
        
        /// <summary> 
        /// Insert an aggregated dependency into cache 
        /// </summary> 
        /// <param name="key">The key that will identify the object in cache</param> 
        /// <param name="v">The object to cache</param> 
        /// <param name="tableNames">A string array with the tablenames that the object depends on</param> 
        /// <remarks> 
        /// Adds an object to cache with dependencies on multiple tables. 
        /// A change in one of the dependent tables will invalidate and empty the cache. 
        /// </remarks> 
        internal static void CacheSetAggregated(string key, object v, string[] tableNames) {
            if (tableNames != null) {
                if (!DataAccess.IsLocalMode || Utilities.CBoolEx(ConfigurationManager.AppSettings["LOCALMODE_USE_CACHE"])) {
                    AggregateCacheDependency aggDep = new AggregateCacheDependency();
                    string dbName = CacheManager.DataBaseName;
                    for (int i = 0; i <= tableNames.Length - 1; i++) {
                        aggDep.Add(new SqlCacheDependency(dbName, tableNames[i]));
                    }
                    try {
                        CacheManager.InsertIntoCache(key, v, aggDep);
                    } catch (Exception) {
                        
                    }
                }
            }
        } 



        /// <summary> 
        /// Enable dependency caching on specified table 
        /// </summary> 
        /// <returns>True on success</returns> 
        /// <remarks></remarks> 
        public static bool enableCaching(string tableName) {
            try {
                try {
                    string sql = "exec dbo.AspNet_SqlCacheRegisterTableStoredProcedure N'" + tableName + "'";
                    DataAccess.excecuteNonQueryDb(sql);
                    Array.Resize(ref cachingTables, cachingTables.Length + 1);
                    cachingTables[cachingTables.Length - 1] = tableName;
                    return true;
                } catch (Exception) {
                    try {
                        Array.Resize(ref cachingTables, cachingTables.Length + 1);
                        cachingTables[cachingTables.Length - 1] = tableName;
                        return true;
                    } catch (Exception) {
                        return false;
                    }

                }
            } catch (System.Security.SecurityException) {
                return false;
            }
        }

        /// <summary> 
        /// Enable dependency caching on specified tables 
        /// </summary> 
        /// <param name="tableNames"></param> 
        /// <returns></returns> 
        /// <remarks></remarks> 
        public static void enableCaching(string[] tableNames) {
            for (int i = 0; i <= tableNames.Length - 1; i++) {
                enableCaching(tableNames[i]);
            }
        }

        /// <summary> 
        /// Disable dependency caching on specified table 
        /// </summary> 
        /// <param name="tableName"></param> 
        /// <returns></returns> 
        /// <remarks></remarks> 
        public static bool disableCaching(string tableName) {
            try {
                string sql = "exec dbo.AspNet_SqlCacheUnRegisterTableStoredProcedure N'" + tableName + "'";
                DataAccess.excecuteNonQueryDb(sql);
                return true;
            } catch (Exception) {
                return false;
            }
        }

        /// <summary> 
        /// Disable dependency caching on specified tables 
        /// </summary> 
        /// <param name="tableNames"></param> 
        /// <returns></returns> 
        /// <remarks></remarks> 
        public static void disableCaching(string[] tableNames) {
            for (int i = 0; i <= tableNames.Length - 1; i++) {
                disableCaching(tableNames[i]);
            }
        }

        /// <summary> 
        /// Returns the script needed to set up a ddl-trigger on the DataBase defined in web.config 
        /// </summary> 
        /// <returns></returns> 
        /// <remarks></remarks> 
        private static string ddlTriggerScript() {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(" CREATE TRIGGER [TableCreated] ");
            sb.AppendLine(" ON DATABASE ");
            sb.AppendLine(" FOR CREATE_TABLE, ALTER_TABLE ");
            sb.AppendLine(" AS ");
            sb.AppendLine(" SET NOCOUNT ON ");
            sb.AppendLine(" DECLARE @xmlEventData XML, ");
            sb.AppendLine(" @tableName VARCHAR(450) ");
            sb.AppendLine(" SET @xmlEventData = EVENTDATA() ");
            sb.AppendLine(" SET @tableName = CONVERT(VARCHAR(300),@xmlEventData.query('data(/EVENT_INSTANCE/ObjectName)')) ");
            sb.AppendLine(" IF EXISTS (SELECT name FROM sysobjects WITH (NOLOCK) WHERE name = 'RestoreCache' AND type = 'P')");
            sb.AppendLine(" IF EXISTS (SELECT name FROM sysobjects WITH (TABLOCKX) WHERE name = 'RestoreCache' AND type = 'P')");
            sb.AppendLine(" EXEC RestoreCache @tableName ");

            return sb.ToString();
        }

        /// <summary> 
        /// Returns the script needed to set up the "RestoreCache" stored procedure on the DataBase defined in web.config 
        /// </summary> 
        /// <returns></returns> 
        /// <remarks></remarks> 
        private static string spTriggerScript() {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(" CREATE PROCEDURE RestoreCache ");
            sb.AppendLine(" @p1 varchar(450) = '' ");
            sb.AppendLine(" AS ");
            sb.AppendLine(" BEGIN ");
            sb.AppendLine(" SET NOCOUNT ON; ");
            sb.AppendLine(" DECLARE @triggerCreate varchar(5000) ");
            sb.AppendLine(" DECLARE @triggerName varchar(1000) ");

            sb.AppendLine(" DECLARE @tableName VARCHAR(450), @pattern_position INT ");
            sb.AppendLine(" SELECT @pattern_position = CHARINDEX('$', @p1) ");
            sb.AppendLine(" IF @pattern_position > 0 ");
            sb.AppendLine(" SELECT @pattern_position = CHARINDEX('$', RIGHT(@p1, LEN(@p1) -1)) +1 ");
            sb.AppendLine(" SET @tableName = SUBSTRING(@p1, @pattern_position +1, LEN(@p1)) ");


            sb.AppendLine(" SET @triggerName = @tableName+'_AspNet_SqlCacheNotification_Trigger' ");
            sb.AppendLine(" SET @triggerCreate = 'CREATE TRIGGER ['+@triggerName+'] ON ['+@p1+'] ");
            sb.AppendLine(" FOR INSERT, UPDATE, DELETE AS BEGIN ");
            sb.AppendLine(" SET NOCOUNT ON ");
            sb.AppendLine(" EXEC dbo.AspNet_SqlCacheUpdateChangeIdStoredProcedure N'''+@tableName+''' ");
            sb.AppendLine(" END' ");
            sb.AppendLine(" END ");

            sb.AppendLine(" IF EXISTS(SELECT tableName FROM AspNet_SqlCacheTablesForChangeNotification WHERE tableName = @tableName) ");
            sb.AppendLine(" EXEC AspNet_SqlCacheUpdateChangeIdStoredProcedure @tableName ");

            sb.AppendLine(" DECLARE @dropTrigger VARCHAR(500) ");
            sb.AppendLine(" SET @dropTrigger = 'DROP TRIGGER ['+@triggerName+']' ");

            sb.AppendLine(" IF EXISTS (SELECT name FROM sysobjects WITH (NOLOCK) WHERE name = @triggerName AND type = 'TR') ");
            sb.AppendLine(" EXEC(@dropTrigger) ");
            sb.AppendLine(" IF EXISTS (SELECT name FROM sysobjects WITH (TABLOCKX) WHERE name = @triggerName AND type = 'TR') ");
            sb.AppendLine(" EXEC(@dropTrigger) ");
            sb.AppendLine(" IF EXISTS(SELECT tableName FROM AspNet_SqlCacheTablesForChangeNotification WHERE tableName = @tableName) ");
            sb.AppendLine(" EXEC(@triggerCreate) ");

            return sb.ToString();

        }

        /// <summary> 
        /// Returns the script needed to set up SqlCacheDependency on the DataBase defined in web.config 
        /// </summary> 
        /// <returns></returns> 
        /// <remarks></remarks> 
        private static string createEnableSqlCahceDependencyScript() {
            StringBuilder sql = new StringBuilder();

            sql.AppendLine(" IF NOT EXISTS (SELECT name FROM sysobjects WITH (NOLOCK) WHERE name = 'AspNet_SqlCacheTablesForChangeNotification' AND type = 'U')");
            sql.AppendLine(" IF NOT EXISTS (SELECT name FROM sysobjects WITH (TABLOCKX) WHERE name = 'AspNet_SqlCacheTablesForChangeNotification' AND type = 'U')");
            sql.AppendLine(" CREATE TABLE dbo.AspNet_SqlCacheTablesForChangeNotification (");
            sql.AppendLine(" tableName NVARCHAR(450) NOT NULL PRIMARY KEY,");
            sql.AppendLine(" notificationCreated DATETIME NOT NULL DEFAULT(GETDATE()),");
            sql.AppendLine(" changeId INT NOT NULL DEFAULT(0)");
            sql.AppendLine(")");
            sql.AppendLine(" /* Create polling SP */");
            sql.AppendLine(" IF NOT EXISTS (SELECT name FROM sysobjects WITH (NOLOCK) WHERE name = 'AspNet_SqlCachePollingStoredProcedure' AND type = 'P')");
            sql.AppendLine(" IF NOT EXISTS (SELECT name FROM sysobjects WITH (TABLOCKX) WHERE name = 'AspNet_SqlCachePollingStoredProcedure' AND type = 'P')");
            sql.AppendLine(" EXEC('CREATE PROCEDURE dbo.AspNet_SqlCachePollingStoredProcedure AS");
            sql.AppendLine(" SELECT tableName, changeId FROM dbo.AspNet_SqlCacheTablesForChangeNotification");
            sql.AppendLine(" RETURN 0')");
            sql.AppendLine("/* Create SP for registering a table. */");
            sql.AppendLine(" IF NOT EXISTS (SELECT name FROM sysobjects WITH (NOLOCK) WHERE name = 'AspNet_SqlCacheRegisterTableStoredProcedure' AND type = 'P')");
            sql.AppendLine(" IF NOT EXISTS (SELECT name FROM sysobjects WITH (TABLOCKX) WHERE name = 'AspNet_SqlCacheRegisterTableStoredProcedure' AND type = 'P')");
            sql.AppendLine(" EXEC('CREATE PROCEDURE dbo.AspNet_SqlCacheRegisterTableStoredProcedure");
            sql.AppendLine(" @tableName NVARCHAR(450)");
            sql.AppendLine(" AS");
            sql.AppendLine(" BEGIN");
            sql.AppendLine(" DECLARE @triggerName AS NVARCHAR(3000)");
            sql.AppendLine(" DECLARE @fullTriggerName AS NVARCHAR(3000)");
            sql.AppendLine(" DECLARE @canonTableName NVARCHAR(3000)");
            sql.AppendLine(" DECLARE @quotedTableName NVARCHAR(3000)");
            sql.AppendLine(" /* Create the trigger name */");
            sql.AppendLine(" SET @triggerName = REPLACE(@tableName, ''['', ''__o__'')");
            sql.AppendLine(" SET @triggerName = REPLACE(@triggerName, '']'', ''__c__'')");
            sql.AppendLine(" SET @triggerName = @triggerName + ''_AspNet_SqlCacheNotification_Trigger''");
            sql.AppendLine(" SET @fullTriggerName = ''dbo.['' + @triggerName + '']''");
            sql.AppendLine(" /* Create the cannonicalized table name for trigger creation */");
            sql.AppendLine(" /* Do not touch it if the name contains other delimiters */");
            sql.AppendLine(" IF (CHARINDEX(''.'', @tableName) <> 0 OR");
            sql.AppendLine(" CHARINDEX(''['', @tableName) <> 0 OR");
            sql.AppendLine(" CHARINDEX('']'', @tableName) <> 0)");
            sql.AppendLine(" SET @canonTableName = @tableName");
            sql.AppendLine(" ELSE");
            sql.AppendLine(" SET @canonTableName = ''['' + @tableName + '']''");
            sql.AppendLine(" /* First make sure the table exists */");
            sql.AppendLine(" IF (SELECT OBJECT_ID(@tableName, ''U'')) IS NULL");
            sql.AppendLine(" BEGIN");
            sql.AppendLine(" RAISERROR (''00000001'', 16, 1)");
            sql.AppendLine(" RETURN");
            sql.AppendLine(" END");
            sql.AppendLine(" BEGIN TRAN");
            sql.AppendLine(" /* Insert the value into the notification table */");
            sql.AppendLine(" IF NOT EXISTS (SELECT tableName FROM dbo.AspNet_SqlCacheTablesForChangeNotification WITH (NOLOCK) WHERE tableName = @tableName)");
            sql.AppendLine(" IF NOT EXISTS (SELECT tableName FROM dbo.AspNet_SqlCacheTablesForChangeNotification WITH (TABLOCKX) WHERE tableName = @tableName)");
            sql.AppendLine(" INSERT dbo.AspNet_SqlCacheTablesForChangeNotification");
            sql.AppendLine(" VALUES (@tableName, GETDATE(), 0)");
            sql.AppendLine(" /* Create the trigger */");
            sql.AppendLine(" SET @quotedTableName = QUOTENAME(@tableName, '''''''')");
            sql.AppendLine(" IF NOT EXISTS (SELECT name FROM sysobjects WITH (NOLOCK) WHERE name = @triggerName AND type = ''TR'')");
            sql.AppendLine(" IF NOT EXISTS (SELECT name FROM sysobjects WITH (TABLOCKX) WHERE name = @triggerName AND type = ''TR'')");
            sql.AppendLine(" EXEC(''CREATE TRIGGER '' + @fullTriggerName + '' ON '' + @canonTableName +''");
            sql.AppendLine(" FOR INSERT, UPDATE, DELETE AS BEGIN");
            sql.AppendLine(" SET NOCOUNT ON");
            sql.AppendLine(" EXEC dbo.AspNet_SqlCacheUpdateChangeIdStoredProcedure N'' + @quotedTableName + ''");
            sql.AppendLine(" END");
            sql.AppendLine(" '')");
            sql.AppendLine(" COMMIT TRAN");
            sql.AppendLine(" END");
            sql.AppendLine(" ')");
            sql.AppendLine(" /* Create SP for updating the change Id of a table. */");
            sql.AppendLine(" IF NOT EXISTS (SELECT name FROM sysobjects WITH (NOLOCK) WHERE name = 'AspNet_SqlCacheUpdateChangeIdStoredProcedure' AND type = 'P')");
            sql.AppendLine(" IF NOT EXISTS (SELECT name FROM sysobjects WITH (TABLOCKX) WHERE name = 'AspNet_SqlCacheUpdateChangeIdStoredProcedure' AND type = 'P')");
            sql.AppendLine(" EXEC('CREATE PROCEDURE dbo.AspNet_SqlCacheUpdateChangeIdStoredProcedure");
            sql.AppendLine(" @tableName NVARCHAR(450)");
            sql.AppendLine(" AS");
            sql.AppendLine(" BEGIN");
            sql.AppendLine(" UPDATE dbo.AspNet_SqlCacheTablesForChangeNotification WITH (ROWLOCK) SET changeId = changeId + 1");
            sql.AppendLine(" WHERE tableName = @tableName");
            sql.AppendLine(" END");
            sql.AppendLine(" ')");
            sql.AppendLine(" /* Create SP for unregistering a table. */");
            sql.AppendLine(" IF NOT EXISTS (SELECT name FROM sysobjects WITH (NOLOCK) WHERE name = 'AspNet_SqlCacheUnRegisterTableStoredProcedure' AND type = 'P')");
            sql.AppendLine(" IF NOT EXISTS (SELECT name FROM sysobjects WITH (TABLOCKX) WHERE name = 'AspNet_SqlCacheUnRegisterTableStoredProcedure' AND type = 'P')");
            sql.AppendLine(" EXEC('CREATE PROCEDURE dbo.AspNet_SqlCacheUnRegisterTableStoredProcedure");
            sql.AppendLine(" @tableName NVARCHAR(450)");
            sql.AppendLine(" AS");
            sql.AppendLine(" BEGIN");
            sql.AppendLine(" BEGIN TRAN");
            sql.AppendLine(" DECLARE @triggerName AS NVARCHAR(3000)");
            sql.AppendLine(" DECLARE @fullTriggerName AS NVARCHAR(3000)");
            sql.AppendLine(" SET @triggerName = REPLACE(@tableName, ''['', ''__o__'')");
            sql.AppendLine(" SET @triggerName = REPLACE(@triggerName, '']'', ''__c__'')");
            sql.AppendLine(" SET @triggerName = @triggerName + ''_AspNet_SqlCacheNotification_Trigger''");
            sql.AppendLine(" SET @fullTriggerName = ''dbo.['' + @triggerName + '']''");
            sql.AppendLine(" /* Remove the table-row from the notification table */");
            sql.AppendLine(" IF EXISTS (SELECT name FROM sysobjects WITH (NOLOCK) WHERE name = ''AspNet_SqlCacheTablesForChangeNotification'' AND type = ''U'')");
            sql.AppendLine(" IF EXISTS (SELECT name FROM sysobjects WITH (TABLOCKX) WHERE name = ''AspNet_SqlCacheTablesForChangeNotification'' AND type = ''U'')");
            sql.AppendLine(" DELETE FROM dbo.AspNet_SqlCacheTablesForChangeNotification WHERE tableName = @tableName");
            sql.AppendLine(" /* Remove the trigger */");
            sql.AppendLine(" IF EXISTS (SELECT name FROM sysobjects WITH (NOLOCK) WHERE name = @triggerName AND type = ''TR'')");
            sql.AppendLine(" IF EXISTS (SELECT name FROM sysobjects WITH (TABLOCKX) WHERE name = @triggerName AND type = ''TR'')");
            sql.AppendLine(" EXEC(''DROP TRIGGER '' + @fullTriggerName)");
            sql.AppendLine(" COMMIT TRAN");
            sql.AppendLine(" END");
            sql.AppendLine(" ')");
            sql.AppendLine(" /* Create SP for querying all registered table */");
            sql.AppendLine(" IF NOT EXISTS (SELECT name FROM sysobjects WITH (NOLOCK) WHERE name = 'AspNet_SqlCacheQueryRegisteredTablesStoredProcedure' AND type = 'P')");
            sql.AppendLine(" IF NOT EXISTS (SELECT name FROM sysobjects WITH (TABLOCKX) WHERE name = 'AspNet_SqlCacheQueryRegisteredTablesStoredProcedure' AND type = 'P')");
            sql.AppendLine(" EXEC('CREATE PROCEDURE dbo.AspNet_SqlCacheQueryRegisteredTablesStoredProcedure");
            sql.AppendLine(" AS");
            sql.AppendLine(" SELECT tableName FROM dbo.AspNet_SqlCacheTablesForChangeNotification ')");
            sql.AppendLine(" /* Create roles and grant them access to SP */");
            sql.AppendLine(" IF NOT EXISTS (SELECT name FROM sysusers WHERE issqlrole = 1 AND name = N'aspnet_ChangeNotification_ReceiveNotificationsOnlyAccess')");
            sql.AppendLine(" EXEC sp_addrole N'aspnet_ChangeNotification_ReceiveNotificationsOnlyAccess'");
            sql.AppendLine(" GRANT EXECUTE ON dbo.AspNet_SqlCachePollingStoredProcedure to aspnet_ChangeNotification_ReceiveNotificationsOnlyAccess");
            return sql.ToString();
        }

    }
}