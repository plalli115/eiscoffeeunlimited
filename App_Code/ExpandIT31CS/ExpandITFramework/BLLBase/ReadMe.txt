﻿The classes in these subfolders are the abstract base classes that should be inherited from
whenever a "userland" implementor whishes to create a new BLL class. It's not recommended to 
make changes to the base classes directly; Instead create a new class and derive it from one of the
base classes, and put the new class in the UserLandImplementation folder. Some of the base classes
are partial, so it's possible to make additions without touching the original file.