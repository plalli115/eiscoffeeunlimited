﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web;
using ExpandIT31.ExpanditFramework;
using ExpandIT31.ExpanditFramework.Core;
using ExpandIT31.ExpanditFramework.Infrastructure;
using ExpandIT31.ExpanditFramework.Util;

namespace ExpandIT31.ExpanditFramework.BLLBase {
    /// <summary>
    /// Summary description for UserBase
    /// </summary>
    [DataObject(true)]
    public abstract partial class UserBase : ExpandITBLLBase, IObservable {

        public enum UserStates : byte {
            New = 1,
            Confirmed = 2,
            Updated = 3,
            Deactivated = 4
        }

        protected string m_Address1;
        protected string m_Address2;
        protected string m_CityName;
        protected string m_CompanyName;
        //AM: Added Residential
        protected Boolean  m_ResidentialAddress;
        protected string m_ContactName;
        protected string m_CountryGuid;
        protected string m_CurrencyGuid;
        protected string m_CustomerGuid;
        protected string m_EmailAddress;
        protected byte m_IsB2B;
        protected string m_LanguageGuid;
        protected string m_PasswordVersion;
        protected string m_PhoneNo;
        protected string m_SecondaryCurrencyGuid;
        protected string m_StateName;
        protected DateTime m_UserCreated;
        protected string m_UserGuid;
        protected string m_UserLogin;
        protected DateTime m_UserModified;
        protected string m_UserPassword;
        protected string m_ZipCode;
        protected string m_ShippingProvider;
        protected string m_ConfirmedUserPassword;
        protected string m_OriginalUserPassword;
        protected string m_GlobalUserGuid;
        protected bool m_isUpdate;
        protected UserStates m_Status;        
        protected List<IObserver> observers;

        public UserBase() { }
        public UserBase(string userGuid) { }
        
        /// <summary> 
        /// ObjectDataSource default select method 
        /// </summary> 
        /// <param name="userGuid"></param> 
        /// <returns>Returns this</returns> 
        /// <remarks></remarks> 
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public virtual UserBase GetData(string userGuid) {
            throw new NotImplementedException("Method GetData(string userGuid) must be implemented by the sub class");
        }

        /// <summary> 
        /// ObjectDataSource default update method 
        /// </summary> 
        /// <param name="Address1"></param> 
        /// <param name="Address2"></param> 
        /// <param name="CityName"></param> 
        /// <param name="CompanyName"></param> 
        /// <param name="ResidentialAddress"></param> 
        /// <param name="ContactName"></param> 
        /// <param name="CountryGuid"></param> 
        /// <param name="CurrencyGuid"></param> 
        /// <param name="EmailAddress"></param> 
        /// <param name="LanguageGuid"></param> 
        /// <param name="PhoneNo"></param> 
        /// <param name="SecondaryCurrencyGuid"></param> 
        /// <param name="StateName"></param> 
        /// <param name="UserGuid"></param> 
        /// <param name="UserLogin"></param> 
        /// <param name="UserPassword"></param> 
        /// <param name="ZipCode"></param> 
        /// <remarks></remarks> 
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public virtual Dictionary<int, List<string>> SetData(string Address1, string Address2, string CityName, string CompanyName, Boolean ResidentialAddress, string ContactName, string CountryGuid, 
        string CurrencyGuid, string EmailAddress, string LanguageGuid,string PhoneNo, string SecondaryCurrencyGuid, string StateName, string UserGuid, 
        string UserLogin, string UserPassword, string ZipCode, string ConfirmedUserPassword)
        {
            throw new NotImplementedException("Method SetData(string userGuid) must be implemented by the sub class");
        }
        
        
        protected abstract void LoadRootdata(object C, string sqlQuery);

        #region "Internal Get/Set"

        [Browsable(false)]
        public string OriginalUserPassword {
            get { return m_OriginalUserPassword; }
        }

        [Browsable(false)]
        public bool IsUpdate {
            get { return m_isUpdate; }
        }
        #endregion

        #region "Get/Set Properties"
        public string Address1 {
            get { return m_Address1; }
            set { m_Address1 = value; }
        }

        public string Address2 {
            get { return m_Address2; }
            set { m_Address2 = value; }
        }

        public string CityName {
            get { return m_CityName; }
            set { m_CityName = value; }
        }

        public string CompanyName {
            get { return m_CompanyName; }
            set { m_CompanyName = value; }
        }

        public Boolean ResidentialAddress
        {
            get { return m_ResidentialAddress; }
            set { m_ResidentialAddress = value; }
        }

        public string ContactName {
            get { return m_ContactName; }
            set { m_ContactName = value; }
        }

        public string CountryGuid {
            get { return m_CountryGuid; }
            set { m_CountryGuid = value; }
        }

        public string CurrencyGuid {
            get { return m_CurrencyGuid; }
            set { m_CurrencyGuid = value; }
        }

        public string CustomerGuid {
            get { return m_CustomerGuid; }
            set { m_CustomerGuid = value; }
        }

        public string EmailAddress {
            get { return m_EmailAddress; }
            set { m_EmailAddress = value; }
        }

        public byte IsB2B {
            get { return m_IsB2B; }
            set { m_IsB2B = value; }
        }

        public string LanguageGuid {
            get { return m_LanguageGuid; }
            set { m_LanguageGuid = value; }
        }

        public string PasswordVersion {
            get { return m_PasswordVersion; }
            set { m_PasswordVersion = value; }
        }

        public string PhoneNo {
            get { return m_PhoneNo; }
            set { m_PhoneNo = value; }
        }

        public string SecondaryCurrencyGuid {
            get { return m_SecondaryCurrencyGuid; }
            set { m_SecondaryCurrencyGuid = value; }
        }

        public string StateName {
            get { return m_StateName; }
            set { m_StateName = value; }
        }

        public DateTime UserCreated {
            get { return m_UserCreated; }
            set { m_UserCreated = value; }
        }

        public string UserGuid {
            get { return m_UserGuid; }
            set { m_UserGuid = value; }
        }

        public string UserLogin {
            get { return m_UserLogin; }
            set { m_UserLogin = value; }
        }

        public DateTime UserModified {
            get { return m_UserModified; }
            set { m_UserModified = value; }
        }

        public string UserPassword {
            get { return m_UserPassword; }
            set { m_UserPassword = value; }
        }

        public string ConfirmedUserPassword {
            get { return m_ConfirmedUserPassword; }
            set { m_ConfirmedUserPassword = value; }
        }

        public string ZipCode {
            get { return m_ZipCode; }
            set { m_ZipCode = value; }
        }

        public string ShippingProvider {
            get { return m_ShippingProvider; }
            set { m_ShippingProvider = value; }
        }

        public UserStates Status {
            get { return m_Status; }
            set { m_Status = value; }
        }

        #endregion    
        
        #region "IObservable Implementation"

        public virtual void notify() {
            if (observers != null) {
                foreach (IObserver observer in observers) {
                    observer.update(this, null);
                }
            }
        }

        public virtual void register(IObserver o) {
            if (observers == null) observers = new List<IObserver>();
            observers.Add(o);
        }

        public virtual void remove(IObserver o) {
            int i = observers.IndexOf(o);
            observers.RemoveAt(i);
        }

        #endregion
    }
}