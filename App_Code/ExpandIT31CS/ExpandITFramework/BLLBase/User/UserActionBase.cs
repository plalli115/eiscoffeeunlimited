﻿using System;
using System.Collections.Generic;
using System.Web;
using ExpandIT31.ExpanditFramework.Core;

namespace ExpandIT31.ExpanditFramework.BLLBase {
    /// <summary>
    /// Summary description for UserActionBase
    /// </summary>
    public abstract class UserActionBase : IObserver {
        public UserActionBase() {}

        #region IObserver Members

        public abstract void update(IObservable obs, object o);

        #endregion
    }
}