﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web;
using ExpandIT31.ExpanditFramework.Core;

namespace ExpandIT31.ExpanditFramework.BLLBase {

    /// <summary>
    /// Summary description for UserB2BBase
    /// </summary>
    [DataObject(true)]
    public abstract partial class UserB2BBase : ExpandITBLLBase {

        protected string m_Address1;
        protected string m_Address2;
        protected string m_CityName;
        protected string m_CompanyName;
        //AM: Added Residential
        protected Boolean m_ResidentialAddress;
        protected string m_ContactName;
        protected string m_CountryGuid;
        protected string m_CurrencyGuid;
        protected string m_CustomerGuid;
        protected string m_EmailAddress;
        protected byte m_IsB2B;
        protected string m_LanguageGuid;
        protected string m_PasswordVersion;
        protected string m_PhoneNo;
        protected string m_SecondaryCurrencyGuid;
        protected string m_StateName;
        protected DateTime m_UserCreated;
        protected string m_UserGuid;
        protected string m_UserLogin;
        protected DateTime m_UserModified;
        protected string m_UserPassword;
        protected string m_ZipCode;
        protected string m_ShippingProvider;

        public UserB2BBase() { }

        /// <summary> 
        /// ObjectDataSource default select method 
        /// </summary> 
        /// <param name="userGuid"></param> 
        /// <returns>Returns this</returns> 
        /// <remarks></remarks> 
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public virtual UserB2BBase GetData(string userGuid) {
            throw new NotImplementedException("Method GetData(string userGuid) must be implemented by the sub class");
        }


        protected abstract void LoadRootdata(object C, string sqlQuery);

        #region "Get/Set Properties"

        public string Address1 {
            get { return m_Address1; }
            set { m_Address1 = value; }
        }

        public string Address2 {
            get { return m_Address2; }
            set { m_Address2 = value; }
        }

        public string CityName {
            get { return m_CityName; }
            set { m_CityName = value; }
        }

        public string CompanyName {
            get { return m_CompanyName; }
            set { m_CompanyName = value; }
        }

        public Boolean ResidentialAddress
        {
            get { return m_ResidentialAddress; }
            set { m_ResidentialAddress = value; }
        }

        public string ContactName {
            get { return m_ContactName; }
            set { m_ContactName = value; }
        }

        public string CountryGuid {
            get { return m_CountryGuid; }
            set { m_CountryGuid = value; }
        }

        public string CurrencyGuid {
            get { return m_CurrencyGuid; }
            set { m_CurrencyGuid = value; }
        }

        public string CustomerGuid {
            get { return m_CustomerGuid; }
            set { m_CustomerGuid = value; }
        }

        public string EmailAddress {
            get { return m_EmailAddress; }
            set { m_EmailAddress = value; }
        }

        public byte IsB2B {
            get { return m_IsB2B; }
            set { m_IsB2B = value; }
        }

        public string LanguageGuid {
            get { return m_LanguageGuid; }
            set { m_LanguageGuid = value; }
        }

        public string PasswordVersion {
            get { return m_PasswordVersion; }
            set { m_PasswordVersion = value; }
        }

        public string PhoneNo {
            get { return m_PhoneNo; }
            set { m_PhoneNo = value; }
        }

        public string SecondaryCurrencyGuid {
            get { return m_SecondaryCurrencyGuid; }
            set { m_SecondaryCurrencyGuid = value; }
        }

        public string StateName {
            get { return m_StateName; }
            set { m_StateName = value; }
        }

        public DateTime UserCreated {
            get { return m_UserCreated; }
            set { m_UserCreated = value; }
        }

        public string UserGuid {
            get { return m_UserGuid; }
            set { m_UserGuid = value; }
        }

        public string UserLogin {
            get { return m_UserLogin; }
            set { m_UserLogin = value; }
        }

        public DateTime UserModified {
            get { return m_UserModified; }
            set { m_UserModified = value; }
        }

        public string UserPassword {
            get { return m_UserPassword; }
            set { m_UserPassword = value; }
        }

        public string ZipCode {
            get { return m_ZipCode; }
            set { m_ZipCode = value; }
        }

        public string ShippingProvider {
            get { return m_ShippingProvider; }
            set { m_ShippingProvider = value; }
        }


        #endregion
    }
}