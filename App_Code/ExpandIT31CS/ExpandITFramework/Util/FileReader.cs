﻿using System.IO;
using System.Web;
using System;
using System.Text;

/// <summary> 
/// This class is responsible for reading files within the Internet Shop application folder 
/// </summary> 
/// <remarks></remarks> 
namespace ExpandIT31.ExpanditFramework.Util {

    public class FileReader {

        /// <summary>
        /// Finds out if a file exists and is not empty.
        /// </summary>
        /// <param name="path">virtual path of the file</param>
        /// <returns>True if the file exists, is not empty and access to file is granted, false otherwise</returns>
        public static bool fileHasContent(string path) {
            bool result = false;
            try {
                FileInfo info = new FileInfo(HttpContext.Current.Server.MapPath(path));
                result = info.Exists && (info.Length != 0);
            } catch {
            }
            return result;
        }

        /// <summary>
        /// Reads a file and returns the content as a string
        /// </summary>
        /// <param name="path">Relative path of the file</param>
        /// <returns></returns>
        public static string readFile(string path) {
            string retStr = null;
            try {

                using (StreamReader sr = new StreamReader(File.OpenRead(HttpContext.Current.Server.MapPath(path)))) {
                    retStr = sr.ReadToEnd();
                }
            } catch (Exception) {
                return null;
            }
            return retStr;
        }

        /// <summary> 
        /// Reads a file and returns a string in the requested encoding 
        /// </summary> 
        /// <param name="path">Relative path to the file</param> 
        /// <param name="reqEncoding">The requested encoding of the returned string</param> 
        /// <returns>A string representation of a file in the defined encoding</returns> 
        /// <remarks></remarks> 
        public static string readFile(string path, Encoding reqEncoding) {

            byte[] b = null;
            Encoding enc = GetFileEncoding(HttpContext.Current.Server.MapPath(path));

            try {

                using (FileStream fs = new FileStream(HttpContext.Current.Server.MapPath(path), FileMode.Open, FileAccess.Read)) {

                    b = new byte[fs.Length + 1];
                    int bytesToRead = b.Length - 1;
                    int readFrom = 0;
                    int bytesRead = 0;

                    while (bytesToRead > 0) {
                        bytesRead = fs.Read(b, readFrom, bytesToRead);
                        bytesToRead = bytesToRead - bytesRead;
                        readFrom = readFrom + bytesRead;
                    }

                }

                b = Encoding.Convert(enc, reqEncoding, b);

                return reqEncoding.GetString(b);
            } catch (Exception) {
                return null;
            }

        }

        /// <summary> 
        /// Returns the Encoding of a text file. 
        /// </summary> 
        /// <param name="FileName"></param> 
        /// <returns></returns> 
        /// <remarks>Return Encoding.Default if no Unicode BOM (byte order mark) is found.</remarks> 
        public static Encoding GetFileEncoding(string FileName) {

            Encoding Result = null;
            FileInfo FI = new FileInfo(FileName);
            FileStream FS = null;

            try {
                FS = FI.OpenRead();
                Encoding[] UnicodeEncodings = { Encoding.BigEndianUnicode, Encoding.Unicode, Encoding.UTF8 };
                for (int i = 0; i < UnicodeEncodings.Length; i++) {
                    if (Result != null) {
                        break;
                    }
                    FS.Position = 0;
                    byte[] Preamble = UnicodeEncodings[i].GetPreamble();
                    bool PreamblesAreEqual = true;
                    for (int j = 0; j < Preamble.Length; j++) {
                        if (PreamblesAreEqual == true) {
                            break;
                        }
                        PreamblesAreEqual = (Preamble[j] == FS.ReadByte());
                    }
                    if (PreamblesAreEqual) {
                        Result = UnicodeEncodings[i];
                    }
                }
            } catch (System.IO.IOException) {
            } finally {
                if (FS != null) {
                    FS.Close();
                }
                if (Result == null) {
                    Result = Encoding.Default;
                }
            }

            return Result;

        }

    }
}