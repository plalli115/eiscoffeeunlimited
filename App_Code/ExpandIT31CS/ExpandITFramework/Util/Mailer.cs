﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Security;
using System.Text;
using ExpandIT31.ExpanditFramework.Infrastructure;

namespace ExpandIT31.ExpanditFramework.Util
{

    /// <summary> 
    /// This class is responsible for sending emails from the Internet Shop. 
    /// </summary> 
    /// <remarks></remarks> 
    public class Mailer
    {

        // used, when SMTP-server is external   
        private static string smtp_userName = "";
        private static string smtp_passw = "";

        public static string SMTP_UserID
        {
            get { return smtp_userName; }
            set { smtp_userName = value; }
        }

        public static string SMTP_Password
        {
            get { return smtp_passw; }
            set { smtp_passw = value; }
        }

        /// <summary> 
        /// Sends email to one or more recipients. 
        /// </summary> 
        /// <param name="host">String. The name of the mail server</param> 
        /// <param name="port">String. The port to send from</param> 
        /// <param name="fromAddress">String. The sender email address</param> 
        /// <param name="recipient">Dictionary. Key = email address, Value = name to display</param> 
        /// <param name="subject">String. The message shown in the subject field</param> 
        /// <param name="message">String. The body of the email</param> 
        /// <param name="isHtml">Boolean. Set to false to send text mail.</param> 
        /// <param name="selEncoding">Encoding. The encoding to use. Ex. UTF8 etc.</param> 
        /// <param name="bccRecip">Dictionary. Key = email address, Value = name to display. BCC recipients, can be Nothing</param> 
        /// <returns></returns> 
        /// <remarks></remarks> 
        //AM2010060801 - BCC ORDER CONFIRMATION - Start
        public static bool SendEmail(string host, string port, string fromAddress, Dictionary<object, object> recipient, string subject, string message, bool isHtml, Encoding selEncoding, PageMessages messages, Dictionary<object, object> bccRecip)
        {
            MailMessage mail;
            try
            {
                if (bccRecip != null)
                {
                    mail = Mailer.CreateMailMessage(fromAddress, recipient, subject, isHtml, selEncoding,null,bccRecip);
                    mail.Body = message;
                }
                else
                {
                    mail = Mailer.CreateMailMessage(fromAddress, recipient, subject, message, isHtml, selEncoding);
                }
                //AM2010060801 - BCC ORDER CONFIRMATION - End
            }
            catch (Exception ex)
            {
                if (messages != null)
                {
                    messages.Errors.Add(ex.Message);
                }
                return false;
            }
            return SendEmail(host, port, mail, messages);
        }

        /// <summary> 
        /// Send prepared email 
        /// </summary> 
        /// <param name="host">String. The name of the mail server</param> 
        /// <param name="port">String. The port to send from</param> 
        /// <param name="mail">MailMessage. Prepared email</param> 
        /// <returns></returns> 
        /// <remarks></remarks> 
        public static bool SendEmail(string host, string port, MailMessage mail, PageMessages messages)
        {
            bool result = false;
            try
            {
                // Send the mail 
                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
                smtp.Host = host;
                smtp.Port = int.Parse(port);
                try
                {
                    if (string.IsNullOrEmpty(SMTP_UserID) || string.IsNullOrEmpty(SMTP_Password))
                    {
                        smtp.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                    }
                    else
                    {
                        smtp.Credentials = new System.Net.NetworkCredential(SMTP_UserID, SMTP_Password);
                    }
                    smtp.Send(mail);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (messages != null)
                    {
                        messages.Errors.Add(ex.Message);
                    }
                }
            }
            catch (SecurityException ex)
            {
                // Under Medium Trust you can only use port 25 for smtp, or a Security Exception is raised... 
                if (messages != null)
                {
                    messages.Errors.Add(ex.Message);
                }
            }
            catch (Exception ex)
            {
                if (messages != null)
                {
                    messages.Errors.Add(ex.Message);
                }
            }
            return result;
        }


        /// <summary> 
        /// Prepare email to send 
        /// </summary> 
        /// <param name="fromAddress">String. The sender email address</param> 
        /// <param name="recipient">Dictionary. Key = email address, Value = name to display</param> 
        /// <param name="subject">String. The message shown in the subject field</param> 
        /// <param name="message">String. The body of the email</param> 
        /// <param name="isHtml">Boolean. Set to false to send text mail.</param> 
        /// <param name="selEncoding">Encoding. The encoding to use. Ex. UTF8 etc.</param> 
        /// <returns></returns> 
        /// <remarks></remarks> 
        public static MailMessage CreateMailMessage(string fromAddress, Dictionary<object, object> recipient, string subject, string message, bool isHtml, Encoding selEncoding)
        {
            MailMessage mail;
            try
            {
                mail = CreateMailMessage(fromAddress, recipient, subject, isHtml, selEncoding);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            mail.Body = message;
            return mail;
        }

        /// <summary> 
        /// Prepare empty email to send, with empty CC and BCC 
        /// </summary> 
        /// <param name="fromAddress">String. The sender email address</param> 
        /// <param name="recipient">Dictionary. Key = email address, Value = name to display</param> 
        /// <param name="subject">String. The message shown in the subject field</param> 
        /// <param name="isHtml">Boolean. Set to false to send text mail.</param> 
        /// <param name="selEncoding">Encoding. The encoding to use. Ex. UTF8 etc.</param> 
        /// <returns></returns> 
        /// <remarks></remarks> 
        public static MailMessage CreateMailMessage(string fromAddress, Dictionary<object, object> recipient, string subject, bool isHtml, Encoding selEncoding)
        {

            return CreateMailMessage(fromAddress, recipient, subject, isHtml, selEncoding, null, null);
        }

        /// <summary> 
        /// Prepare empty email to send 
        /// </summary> 
        /// <param name="fromAddress">String. The sender email address</param> 
        /// <param name="recipient">Dictionary. Key = email address, Value = name to display</param> 
        /// <param name="subject">String. The message shown in the subject field</param> 
        /// <param name="isHtml">Boolean. Set to false to send text mail.</param> 
        /// <param name="selEncoding">Encoding. The encoding to use. Ex. UTF8 etc.</param> 
        /// <param name="ccRecip">Dictionary. Key = email address, Value = name to display. CC recipients, can be Nothing</param> 
        /// <param name="bccRecip">Dictionary. Key = email address, Value = name to display. BCC recipients, can be Nothing</param> 
        /// <returns></returns> 
        /// <remarks></remarks> 
        public static MailMessage CreateMailMessage(string fromAddress, Dictionary<object, object> recipient, string subject, bool isHtml, Encoding selEncoding, Dictionary<object, object> ccRecip, Dictionary<object, object> bccRecip)
        {

            //#changed 080829 
            //Note about the Net.Mail.MailMessage default constructor: 
            //Quote from the msdn online documentation: "From is set to the value in the network element 
            //for mailSettings<smtp> Element (Network Settings), if it exists". 
            //This means that even if the element or setting is not in your local web.config 
            //it can exist in the machine level web.config, and be read from there. Therefore we must ensure 
            //that no such value will be used when creating an object of the MailMessage class.     

            KeyValuePair<object, object> rcp;
            MailAddress firstRecipient;
            MailMessage mail;

            //Get the first recipient entry from the dictionary 
            try
            {
                rcp = Utilities.GetFirst(recipient);
            }
            catch (Exception ex)
            {
                throw new Exception("GetFirst failed", ex);
            }

            try
            {
                try
                {
                    //Remove that entry from the dictionary 
                    recipient.Remove(rcp.Key);
                }
                catch (ArgumentNullException ane)
                {
                    throw new Exception("Recipient can not be null", ane);
                }
                //Create the first mail address with the first recipients address 
                firstRecipient = new MailAddress((string)rcp.Key, (string)rcp.Value);
            }
            catch (Exception ex)
            {
                throw new Exception("Create mail address failed", ex);
            }

            //Use the parameterized constructor to ensure no faulty data is read from a web.config file 
            try
            {
                mail = new MailMessage(new MailAddress(fromAddress), firstRecipient);
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to create new mail message", ex);
            }

            // Make a HTML representation of the body if this is requested 
            mail.IsBodyHtml = isHtml;

            mail.BodyEncoding = selEncoding;
            mail.SubjectEncoding = selEncoding;

            // Get the recipients for the email 
            foreach (KeyValuePair<object, object> receiver in recipient)
            {
                if (!string.IsNullOrEmpty((string)receiver.Key))
                {
                    mail.To.Add(new MailAddress((string)receiver.Key, (string)receiver.Value, selEncoding));
                }
            }
            // Set Subject property 
            mail.Subject = subject;

            // Get the CC recipients for the email 
            if (ccRecip != null)
            {
                foreach (KeyValuePair<object, object> receiver in ccRecip)
                {
                    if (!string.IsNullOrEmpty((string)receiver.Key))
                    {
                        mail.CC.Add(new MailAddress((string)receiver.Key, (string)receiver.Value, selEncoding));
                    }
                }
            }
            // Get the BCC recipients for the email 
            if (bccRecip != null)
            {
                foreach (KeyValuePair<object, object> receiver in bccRecip)
                {
                    if (!string.IsNullOrEmpty((string)receiver.Key))
                    {
                        mail.Bcc.Add(new MailAddress((string)receiver.Key, (string)receiver.Value, selEncoding));
                    }
                }
            }
            return mail;
        }


        /// <summary> 
        /// Sends an email to one recipient 
        /// </summary> 
        /// <param name="host">String. The name of the mail server</param> 
        /// <param name="port">String. The port to send from</param> 
        /// <param name="fromAddress">String. The sender email address</param> 
        /// <param name="toAddress">String. The receiver email address</param> 
        /// <param name="displayName">String. The name to display</param> 
        /// <param name="subject">String. The message shown in the subject field</param> 
        /// <param name="message">String. The body of the email</param> 
        /// <param name="isHtml">Boolean. Set to false to send text mail.</param> 
        /// <param name="selEncoding">Encoding. The encoding to use. Ex. UTF8 etc.</param> 
        /// <returns></returns> 
        /// <remarks></remarks> 
        public static bool SendEmail(string host, string port, string fromAddress, string toAddress, string displayName, string subject, string message, bool isHtml, Encoding selEncoding, PageMessages messages)
        {

            Dictionary<object, object> recipient = new Dictionary<object, object>();
            recipient.Add(toAddress, displayName);
            //AM2010060801 - BCC ORDER CONFIRMATION - Start
            return SendEmail(host, port, fromAddress, recipient, subject, message, isHtml, selEncoding, messages,null);
            //AM2010060801 - BCC ORDER CONFIRMATION - End
        }
    }
}