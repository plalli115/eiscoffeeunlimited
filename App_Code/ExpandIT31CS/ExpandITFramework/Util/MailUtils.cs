﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;

namespace ExpandIT31.ExpanditFramework.Util {
    //Test version 

    public class MailUtils {

        /// <summary> 
        /// replace of src-attribute for all img-elements in html-document to a form "cid:filename_ext" 
        /// return list of corresponding file paths 
        /// </summary> 
        /// <param name="contentHtml"></param> 
        /// <returns>list of paths of image files</returns> 
        /// <remarks></remarks> 
        public static List<string> AdjustImageLinks(ref string contentHtml) {
            StringBuilder outBuilder = null;
            List<string> result = new List<string>();
            try {
                XmlDocument contentDOM = new XmlDocument();
                contentHtml = Util.Utilities.Replace(contentHtml, "&", "&amp;");
                contentDOM.LoadXml(contentHtml);
                XmlNodeList imgList = contentDOM.SelectNodes("descendant::img");
                foreach (XmlNode node in imgList) {
                    XmlNode attrib = node.Attributes.GetNamedItem("src");
                    string oldSrc = attrib.Value;
                    string newSrc = string.Format("cid:{0}", Path.GetFileName(oldSrc).Replace(".", "_"));
                    attrib.Value = newSrc;
                    result.Add(oldSrc);
                }
                outBuilder = new StringBuilder();
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.OmitXmlDeclaration = true;
                XmlWriter writer = XmlWriter.Create(new StringWriter(outBuilder), settings);
                contentDOM.Save(writer);
            } catch (Exception) {
            }
            if ((outBuilder != null)) {
                contentHtml = outBuilder.ToString();
            }
            return result;
        }
        /// <summary>
        /// convert a srcPath to app-relative form
        /// </summary>
        /// <param name="srcPath"></param>
        /// <param name="dirVirtualPath"></param>
        /// <returns></returns>
        private static string NormalizeVirtPath(string srcPath, string dirVirtualPath) {
            if (!VirtualPathUtility.IsAbsolute(srcPath)) {
                if (VirtualPathUtility.IsAppRelative(srcPath)) {
                    return srcPath;
                } else {
                    return VirtualPathUtility.Combine(dirVirtualPath, srcPath);
                }
            } else {
                return VirtualPathUtility.ToAppRelative(srcPath);
            }
        }


        /// <summary> 
        /// Embed images in email
        /// </summary> 
        /// <param name="contentHtml">text of html-content for email</param> 
        /// <param name="imageList">path-list of image-files</param> 
        /// <param name="templDirVirtualPath"></param> 
        /// <param name="mail">mail-message for embed image</param> 
        /// <remarks></remarks> 
        public static void EmbedImages(string contentHtml, List<string> imageList, string templDirVirtualPath, ref MailMessage mail) {
            if ((imageList != null) && (imageList.Count > 0)) {
                string mediaType = "text/html";
                AlternateView item = AlternateView.CreateAlternateViewFromString(contentHtml, null, mediaType);
                foreach (string imgSrc in imageList) {
                    string virtPath = NormalizeVirtPath(imgSrc, templDirVirtualPath);
                    string imgPath = HttpContext.Current.Server.MapPath(virtPath);
                    LinkedResource resource = null;
                    try {
                        Stream imgStream = null;
                        try {
                            imgStream = new FileStream(imgPath, FileMode.Open, FileAccess.Read);
                            resource = new LinkedResource(imgStream);
                        } catch (Exception) {
                            if ((imgStream != null)) {
                                imgStream.Dispose();
                            }
                            throw;
                        }
                        resource.ContentId = Path.GetFileName(imgSrc).Replace(".", "_");
                        resource.ContentType.Name = Path.GetFileName(imgSrc);
                        item.LinkedResources.Add(resource);
                    } catch (Exception) {
                        if ((resource != null)) {
                            resource.Dispose();
                        }
                    }
                }
                mail.AlternateViews.Add(item);
            } else {
                mail.Body = contentHtml;
            }
        }


        /// <summary> 
        /// 
        /// </summary> 
        /// <param name="contentHtml"></param> 
        /// <param name="templDirVirtualPath"></param> 
        /// <param name="mail"></param> 
        /// <remarks></remarks> 
        public static void EmbedImages(string contentHtml, string templDirVirtualPath, ref MailMessage mail) {
            List<string> imageList = null;
            try {
                imageList = MailUtils.AdjustImageLinks(ref contentHtml);
                EmbedImages(contentHtml, imageList, templDirVirtualPath, ref mail);
            } catch (Exception ex) {
                mail.Body = ex.ToString();
                StringStream contStream = new StringStream(contentHtml);
                mail.Attachments.Add(new Attachment(contStream, "content.htm", System.Net.Mime.MediaTypeNames.Application.Octet));

            }
        }


        /// <summary> 
        /// 
        /// </summary> 
        /// <param name="template"></param> 
        /// <param name="replacements"></param> 
        /// <returns></returns> 
        /// <remarks></remarks> 
        public static string MakeReplacements(string template, Dictionary<string, string> replacements) {
            foreach (string key in replacements.Keys) {
                template = Regex.Replace(template, key, replacements[key], RegexOptions.IgnoreCase);
            }
            return template;
        }

        /// <summary> 
        /// Read the template file, embedd css from linked files (tag "link" with attribute 
        /// rel="stylesheet"), or from param "inlinedCss" in html and return result as string.
        /// Function used content of the param "inlinedCss" if css-link(s) not found         
        /// </summary> 
        /// <param name="templateFilePath">virtual path of the template file</param> 
        /// <param name="inlinedCss">css text to add </param>
        /// <returns>template html with embedded css</returns> 
        /// <remarks></remarks> 
        public static string EmbedCss(string templateFilePath, string inlinedCss) {
            StringBuilder outBuilder = null;
            string errorMsg = "";
            string filePath = HttpContext.Current.Server.MapPath(templateFilePath);
            try {
                XmlDocument contentDOM = new XmlDocument();   
                try{                            
                    contentDOM.Load(filePath);
                }catch{ 
                    // Try system default encoding                   
                    StreamReader str = new StreamReader(filePath, Encoding.Default);
                    string s = str.ReadToEnd();
                    byte[] asciiString = Encoding.Default.GetBytes(s);
                    byte[] utf8String = Encoding.Convert(Encoding.Default, Encoding.UTF8, asciiString);
                    s = Encoding.UTF8.GetString(utf8String);
                    str.Close();                                 
                    contentDOM.LoadXml(s);
                }
                string shortFileName = Path.GetFileNameWithoutExtension(templateFilePath);
                XmlNode headNode = contentDOM.FirstChild.SelectSingleNode("head");
                XmlNodeList linkList = headNode.SelectNodes("link");
                XmlDocumentFragment cssFragm = contentDOM.CreateDocumentFragment();
                XmlReaderSettings cssReaderSetting = new XmlReaderSettings();
                cssReaderSetting.ConformanceLevel = ConformanceLevel.Fragment;
                XmlReader cssReader = null;
                XmlNode child = null;
                Dictionary<string, string> cssDict = new Dictionary<string, string>();
                foreach (XmlNode node in linkList) {
                    XmlNode attrib = node.Attributes.GetNamedItem("rel");
                    if (((attrib != null))) {
                        if (attrib.Value == "stylesheet") {
                            attrib = node.Attributes.GetNamedItem("href");
                            string cssSrc = attrib.Value;
                            string virtPath = NormalizeVirtPath(cssSrc, VirtualPathUtility.GetDirectory(templateFilePath));
                            string inlCss = "";
                            if (!((attrib.Value.IndexOf(shortFileName) >= 0) & ((inlinedCss != null)))) {
                                //not the own stylesheet, oder no data to replace, attempt get inlined CSS on one's own 
                                try {
                                    inlCss = getInlineStyleSheetFromFile(virtPath, Encoding.UTF8);
                                } catch {
                                }
                                cssDict.Add(cssSrc, inlCss);
                            } else {
                                cssDict.Add(cssSrc, inlinedCss);
                            }
                        }
                    }
                }
                if (cssDict.Count > 0) {
                    foreach (XmlNode node in linkList) {
                        XmlNode attrib = node.Attributes.GetNamedItem("rel");
                        if (attrib != null) {
                            if (attrib.Value == "stylesheet") {
                                attrib = node.Attributes.GetNamedItem("href");
                                string inlCss = cssDict[attrib.Value];
                                if (!string.IsNullOrEmpty(inlCss)) {
                                    //Stylesheet link node found 
                                    cssReader = XmlReader.Create(new StringReader(inlCss), cssReaderSetting);
                                    child = contentDOM.ReadNode(cssReader);
                                    //make inlined css node 
                                    while (((child != null))) {
                                        cssFragm.AppendChild(child);
                                        child = contentDOM.ReadNode(cssReader);
                                    }
                                    //replace 
                                    headNode.ReplaceChild(cssFragm, node);
                                }
                            }
                        }
                    }
                } else {
                    if (inlinedCss != null) {
                        //Stylesheet link node not found, insert 
                        cssReader = XmlReader.Create(new StringReader(inlinedCss), cssReaderSetting);
                        child = contentDOM.ReadNode(cssReader);
                        //make inlined css node 
                        while (child != null) {
                            cssFragm.AppendChild(child);
                            child = contentDOM.ReadNode(cssReader);
                        }
                        headNode.InsertAfter(cssFragm, headNode.LastChild);
                    }
                }
                outBuilder = new StringBuilder();
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.OmitXmlDeclaration = true;
                XmlWriter writer = XmlWriter.Create(new StringWriter(outBuilder), settings);
                /* XmlWriter testWriter = XmlWriter.Create(HttpContext.Current.Server.MapPath("~/test.html"), settings);
                 contentDOM.WriteTo(testWriter); 
                 testWriter.Flush(); */
                contentDOM.Save(writer);
            } catch (Exception ex) {
                errorMsg = "ERROR: " + ex.Message;
            }
            if (outBuilder == null) {
                return errorMsg;
            } else {
                return outBuilder.ToString();
            }
        }

        /// <summary> 
        /// old version 
        /// </summary> 
        /// <param name="templateFilePath">virtual path of the template file</param> 
        /// <param name="inlinedCss"></param> 
        /// <returns></returns> 
        /// <remarks></remarks> 
        public static string EmbedCss2(string templateFilePath, string inlinedCss) {
            StringBuilder outBuilder = null;
            string errorMsg = "";
            try {

                XmlDocument contentDOM = new XmlDocument();
                contentDOM.Load(HttpContext.Current.Server.MapPath(templateFilePath));
                string shortFileName = Path.GetFileNameWithoutExtension(templateFilePath);
                XmlNode headNode = contentDOM.FirstChild.SelectSingleNode("head");
                XmlNodeList linkList = headNode.SelectNodes("link");
                XmlDocumentFragment cssFragm = contentDOM.CreateDocumentFragment();
                XmlReaderSettings cssReaderSetting = new XmlReaderSettings();
                cssReaderSetting.ConformanceLevel = ConformanceLevel.Fragment;
                XmlReader cssReader = XmlReader.Create(new StringReader(inlinedCss), cssReaderSetting);
                XmlNode child = default(XmlNode);
                child = contentDOM.ReadNode(cssReader);
                //make inlined css node 
                while (((child != null))) {
                    cssFragm.AppendChild(child);
                    child = contentDOM.ReadNode(cssReader);
                }
                bool OK = false;
                foreach (XmlNode node in linkList) {
                    if (!OK) {
                        XmlNode attrib = node.Attributes.GetNamedItem("rel");
                        if (attrib != null) {
                            if (attrib.Value == "stylesheet") {
                                attrib = node.Attributes.GetNamedItem("href");
                                if (attrib.Value.IndexOf(shortFileName) >= 0) {
                                    //Stylesheet link node found, replace 
                                    headNode.ReplaceChild(cssFragm, node);
                                    OK = true;
                                }
                            }
                        }
                    }
                }
                if (!OK) {
                    //Stylesheet link node not found, insert 
                    headNode.InsertAfter(cssFragm, headNode.LastChild);
                }
                outBuilder = new StringBuilder();
                TextWriter writer = new StringWriter(outBuilder);
                //Dim testWriter as XmlWriter = XmlWriter.Create(HttpContext.Current.Server.MapPath("~/test.html")) 
                //contentDOM.WriteTo(testWriter) 
                //testWriter.Flush() 
                contentDOM.Save(writer);
            } catch (Exception ex) {
                //TODO Error handling 
                errorMsg = ex.Message;
            }
            if (outBuilder == null) {
                return errorMsg;
            } else {
                return outBuilder.ToString();
            }
        }

        public static string getInlineStyleSheetFromFile(string filePath, Encoding reqEncoding) {
            string styleStr = FileReader.readFile(filePath);
            string formatStr = "<style>{0}{1}{0}</style>{0}";
            return (string)(string.IsNullOrEmpty(styleStr) ? string.Empty : string.Format(formatStr, Environment.NewLine, styleStr));
        }

        public static void ConfigMailer() {
            Mailer.SMTP_UserID = Utilities.CStrEx(System.Configuration.ConfigurationManager.AppSettings["MAIL_REMOTE_UID"]);
            Mailer.SMTP_Password = Utilities.CStrEx(System.Configuration.ConfigurationManager.AppSettings["MAIL_REMOTE_PWD"]);
        }
    }
}