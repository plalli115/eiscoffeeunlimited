﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;

namespace ExpandIT31.ExpanditFramework.Util {

    public class Utilities {       
        
        /// <summary>
        /// Retreives the properties of the provided object type 
        /// </summary>
        /// <param name="C"></param>
        /// <returns></returns>
        public static Dictionary<string, object> getPropDict(object C) {

            Dictionary<string, object> propDict = new Dictionary<string, object>();
            PropertyInfo[] properties = C.GetType().GetProperties();

            foreach (PropertyInfo propInfo in properties) {
                propDict.Add(propInfo.Name, propInfo.GetValue(C, null));
            }

            return propDict;

        }       

        /// Determines if a variable contains a value. 
        /// </summary> 
        /// <param name="v">Value to check.</param> 
        /// <returns>Boolean</returns> 
        /// <remarks> 
        /// NaV is short for Not a Value. If v is a valid value this function returns true. Otherwise it returns false. 
        /// Valid values are values that are not empty or null. 
        /// </remarks> 
        public static bool NaV(object v) {
            try{
                return v == null;
            }catch{
                return true;
            }            
        } 

        public static object safeDBNull(object obj) {
            return (obj.Equals(DBNull.Value) ? null : obj);
        }

        /// <summary>
        /// Makes a SQL safe string 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static string SafeString(Object v) {
            string s = string.Empty;
            s = CStrEx(v);
            if (s == "") {
                return "NULL";
            } else {
                return "N'" + Replace(s, "'", "''") + "'";                
            }
        }

        public static string Replace(string s, string p, string p_3) {
            return Regex.Replace(s, p, p_3);
        }

        /// <summary>
        /// Makes a safe conversion to a String. 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static string CStrEx(object v) {
            if (v == DBNull.Value) {
                return "";
            }
            try {
                if (v != null) {
                    return (string)v;
                }else{
                    return "";
                }
            } catch (Exception) {
                return "";
            }
        }

        /// <summary>
        /// Makes a safe conversion to an Integer. 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static Int32 CLngEx(object v) {
            if (v == DBNull.Value) {
                return 0;
            }
            if (v.GetType() == typeof(int)) {
                return (int)v;
            }
            try {
                return int.Parse((string)v);
            } catch (Exception) {
                return 0;
            }
        }

        /// <summary> 
        /// Makes a safe conversion to an Integer. 
        /// </summary> 
        /// <param name="v">Value to convert.</param> 
        /// <returns>Integer</returns> 
        /// <remarks> 
        /// Returns the integer representing v. If v cannot be converted to a integer the function returns 0 (zero). 
        /// </remarks> 
        public static int CIntEx(object v) {
            if (v == DBNull.Value) {
                return 0;
            }
            if (v.GetType() == typeof(int)) {
                return (int)v;
            }
            try {
                return int.Parse((string)v);
            } catch (Exception) {
                return 0;
            }
        }

        /// <summary> 
        /// Makes a safe conversion to a Decimal. 
        /// </summary> 
        /// <param name="v">Value to convert.</param> 
        /// <returns>Decimal</returns> 
        /// <remarks> 
        /// Returns the Decimal representing v. If v cannot be converted to a Decimal the function returns 0 (zero). 
        /// </remarks> 
        public static decimal CDblEx(object v) {
            if (v == DBNull.Value) {
                return 0;
            }
            if (v.GetType() == typeof(decimal)) {
                return (decimal)v;
            }
            try {
                return decimal.Parse((string)v);
            } catch (Exception) {
                return 0;
            }
        }

        /// <summary> 
        /// Makes a safe conversion to a boolean. 
        /// </summary> 
        /// <param name="v">Value to convert.</param> 
        /// <returns>Boolean</returns> 
        /// <remarks> 
        /// Returns the boolean representing v. If v cannot be converted to a boolean the function returns false. 
        /// </remarks> 
        public static bool CBoolEx(object v) {
            if (v == DBNull.Value) {
                return false;
            }
            if (v.GetType() == typeof(bool)) {
                return (bool)v;
            }
            try {                
                string rawString = (string)v;                
                if (Trim(rawString) == "0") {
                    return false;
                }
                else{
                    return true;
                }                             
            } catch (Exception) {
                return false;
            }
        }

        /// <summary> 
        /// Makes a safe conversion to a date. 
        /// </summary> 
        /// <param name="v">Value to convert.</param> 
        /// <returns>Date</returns> 
        /// <remarks> 
        /// Returns the date representing v. If v cannot be converted to a date the function returns CDate(0). 
        /// </remarks> 
        public static System.DateTime CDateEx(object v) {
            try {
                return DateTime.Parse((string)v);                
            } catch (Exception) {
                return System.DateTime.MinValue;
            }
        }

        public static bool RegExpTest(string testStr, string pattern) {
            Regex regex = new Regex(pattern, RegexOptions.IgnoreCase);
            return regex.IsMatch(testStr);
        }

        /// <summary>
        /// Encrypts as string by the Xor algorithm 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string EncryptXOrWay(string s) {
            string r = null;
            int i = 0;
            int ch = 0;

            r = "";
            try{
            for (i = 0; i < s.Length; i++) {

                ch = Convert.ToInt32(Convert.ToChar(Mid(s, i, 1)));                
                ch = ch ^ 111;
                r = r + Right("0" + ch.ToString("X"), 2);
            }
            }catch(Exception ex){
                Console.WriteLine(ex.Message);
            }
            return r;
        }

        /// <summary>
        /// Retreives the first entry in the dictionary
        /// </summary>
        /// <param name="dict"></param>
        /// <returns></returns>
        public static KeyValuePair<object, object> GetFirst(Dictionary<object,object> dict) {
            IEnumerator<KeyValuePair<object, object>> enumerator = default(IEnumerator<KeyValuePair<object, object>>);
            enumerator = dict.GetEnumerator();
            if (enumerator.MoveNext()) {
                return enumerator.Current;
            } else {
                return new KeyValuePair<object, object>();
            }
        } 


        #region "VB Functionality"

        /// <summary>
        /// Retrieves a substring of a given length starting from the beginning of the string
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string Left(string param, int length) {
            //we start at 0 since we want to get the characters starting from the
            //left and with the specified lenght and assign it to a variable
            string result = param.Substring(0, length);
            //return the result of the operation
            return result;
        }
        
        /// <summary>
        /// Retrieves a substring of a given length starting from the end of the string
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string Right(string param, int length) {
            //start at the index based on the lenght of the sting minus
            //the specified lenght and assign it a variable
            string result = param.Substring(param.Length - length, length);
            //return the result of the operation
            return result;
        }

        /// <summary>
        /// Retreives a substring of a given length from a given start position
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string Mid(string param, int startIndex, int length) {
            //start at the specified index in the string ang get N number of
            //characters depending on the lenght and assign it to a variable
            string result = param.Substring(startIndex, length);
            //return the result of the operation
            return result;
        }

        /// <summary>
        /// Retreives a substring from a given start position
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <returns></returns>
        public static string Mid(string param, int startIndex) {
            //start at the specified index and return all characters after it
            //and assign it to a variable
            string result = param.Substring(startIndex);
            //return the result of the operation
            return result;
        }
        
        /// <summary>
        /// Returns a string array that contains the substrings that are delimited
        /// by the provided delimiter string
        /// </summary>
        /// <param name="param"></param>
        /// <param name="delimiter"></param>
        /// <returns></returns>
        public static string[] Split(string param, string delimiter){
            try{
                char[] delim = delimiter.ToCharArray();
                return param.Split(delim);
            }catch(Exception){
                return null;
            }            
        }
        
        /// <summary>
        /// Removes all leading and trailing whitespaces from the current string 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public static string Trim(string param){
            return param.Trim();
        }

        #endregion
    }
}