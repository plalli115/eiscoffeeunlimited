﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Text;

namespace ExpandIT31.ExpanditFramework.Util {

    public class AppUtil {

        public static string GetAppRootUrl() {
            StringBuilder builder = new StringBuilder(HttpContext.Current.Request.Url.Scheme);
            builder.Append("://");
            builder.Append(HttpContext.Current.Request.Url.Host);
            builder.Append(HttpContext.Current.Request.ApplicationPath);
            return builder.ToString();
        }

        public static string GetAppRoot() {
            StringBuilder builder = new StringBuilder(HttpContext.Current.Request.Url.Host);
            builder.Append(HttpContext.Current.Request.ApplicationPath);
            return builder.ToString();
        }

        public static string GetVirtualRoot() {
            return HttpRuntime.AppDomainAppVirtualPath.Equals("/") ? "" : HttpRuntime.AppDomainAppVirtualPath;
        }

        public static string ProductLink(object productguid, object groupguid) {
            //return ShopFullPath() + "/link.aspx?ProductGuid=" + HttpContext.Current.Server.UrlEncode((string)productguid) + (Utilities.CLngEx(groupguid) > 0 ? "&GroupGuid=" + Utilities.CLngEx(groupguid) : "&GetGroup=1");
            return GetVirtualRoot() + "/link.aspx?ProductGuid=" + HttpContext.Current.Server.UrlEncode((string)productguid) + (Utilities.CLngEx(groupguid) > 0 ? "&GroupGuid=" + Utilities.CLngEx(groupguid) : "&GetGroup=1");
        }

        public static string GroupLink(string groupGuid) {
            return GetVirtualRoot() + "/link.aspx?GroupGuid=" + HttpContext.Current.Server.UrlEncode(groupGuid);
        }

        //public static string TemplateGroupLink(Group grp) {
        //    string template = grp.Properties.PropDict["TEMPLATE"];
        //    if (string.IsNullOrEmpty(template)) {
        //        template = AppSettingsReader.Read("DEFAULT_GROUP_TEMPLATE");
        //    }
        //    return GetVirtualRoot() + "/templates/" + template + "?GroupGuid=" + HttpContext.Current.Server.UrlEncode(grp.ID.ToString());
        //}

        public static string ShopFullPath() {
            return ShopFullPathServerOnly() + GetVirtualRoot();
        }

        public static string ShopFullPathServerOnly() {
            string Protocol = null;
            string Port = null;
            string ServerName = null;
            Protocol = "http://";
            Port = HttpContext.Current.Request.ServerVariables["SERVER_PORT"];
            if (Port == "80")
                Port = "";
            else
                Port = ":" + Port;
            if (HttpContext.Current.Request.IsSecureConnection) {
                if (Port == ":443") 
                    Port = "";
                Protocol = "https://";
            }
            ServerName = HttpContext.Current.Request.ServerVariables["HTTP_HOST"];
            int colonIndex = ServerName.IndexOf(":");
            if (colonIndex > 0) {
                ServerName = ServerName.Substring(0, colonIndex - 1);
            }
            return Protocol + ServerName + Port;
        }
    }
}