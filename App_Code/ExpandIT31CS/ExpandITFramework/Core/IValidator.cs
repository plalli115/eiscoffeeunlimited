﻿using System.Collections.Generic;

namespace ExpandIT31.ExpanditFramework.Core {
    public interface IValidator {
        bool validate();
        bool isValid();
        List<string> ErrorMessage {
            get;
        }
    }
}