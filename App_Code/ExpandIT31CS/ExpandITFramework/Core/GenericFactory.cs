﻿using System;
using System.Reflection;

namespace ExpandIT31.ExpanditFramework.Core {

    /// <summary>
    /// GenericFactory creates concrete instances of any type
    /// </summary>
    public class GenericFactory<T> where T : class {

        /// <summary>
        /// Creates and returns a new object of type T
        /// </summary>
        /// <returns></returns>
        //public static T getInstance(params object[] args) {
        //    T instance;
        //    Type[] tArgs;
        //    ConstructorInfo[] publicConstructor = typeof(T).GetConstructors(BindingFlags.Instance | BindingFlags.Public);
        //    if (publicConstructor.Length > 0) {
        //        throw new Exception("No public constructor is allowed for " + typeof(T));
        //    }            
        //    if (args != null) {
        //        tArgs = new Type[args.Length];
        //        for (int i = 0; i < args.Length; i++) {
        //            tArgs[i] = args[i].GetType();
        //        }
        //    } else {
        //        tArgs = new Type[0];
        //    }
        //    ConstructorInfo constructor = typeof(T).GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic, null, tArgs, new ParameterModifier[0]);
        //    if (constructor == null) {
        //        throw new Exception("No private constructor was provided for " + typeof(T));
        //    }
        //    try {
        //        instance = constructor.Invoke(args) as T;
        //    } catch (Exception e) {
        //        throw new Exception("The object of type " + typeof(T) + " could not be created.", e);
        //    }            
        //    return instance;
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        /// <remarks>
        /// Workaround made to ensure taht the program won't fail on servers running under medium trust without 
        /// a ReflectionPermission entry in the web_mediumtrust.config file.
        /// </remarks>
        public static T getInstance(params object[] args) {
            T instance;
            Type[] tArgs;
            
            if (args != null) {
                tArgs = new Type[args.Length];
                for (int i = 0; i < args.Length; i++) {
                    tArgs[i] = args[i].GetType();
                }
            } else {
                tArgs = new Type[0];
            }
            ConstructorInfo constructor = typeof(T).GetConstructor(BindingFlags.Instance | BindingFlags.Public, null, tArgs, new ParameterModifier[0]);            
            try {
                instance = constructor.Invoke(args) as T;
            } catch (Exception e) {
                throw new Exception("The object of type " + typeof(T) + " could not be created.", e);
            }
            return instance;
        }
    }    
}