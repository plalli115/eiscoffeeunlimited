﻿using System;
using System.Collections.Generic;
using System.Web;

namespace ExpandIT31.ExpanditFramework.Core {
    public interface IObservable {
        void register(IObserver o);
        void remove(IObserver o);
        void notify();
    }
}