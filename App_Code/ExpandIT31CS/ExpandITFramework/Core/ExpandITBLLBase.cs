﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;

namespace ExpandIT31.ExpanditFramework.Core {

    [Serializable()]
    public abstract class ExpandITBLLBase : IValidatable {

        protected IValidator m_validator;
        protected IOrderedDictionary messageDict;     
        protected BindingList<ExpandITBLLBase> childCollection;   

        public void setValidator(IValidator validator) {
            m_validator = validator;
        }
        
        public ExpandITBLLBase(params object[] list){
        
        }

    }
}