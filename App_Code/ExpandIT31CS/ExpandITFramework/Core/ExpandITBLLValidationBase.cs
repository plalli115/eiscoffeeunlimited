﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace ExpandIT31.ExpanditFramework.Core {
    /// <summary>
    /// Summary description for ExpandITBLLValidationBase
    /// </summary>
    public abstract class ExpandITBLLValidationBase : IValidator {
        public ExpandITBLLValidationBase() { }
        protected ExpandITBLLValidationBase(ExpandITBLLBase bllObject){
            _propDict = Util.Utilities.getPropDict(bllObject);
        }

        private Dictionary<String, Object> _propDict;
        
        public Dictionary<string, Object> PropDict {
            get { return _propDict; }
        }

        #region IValidator Members

        public abstract bool validate();

        public abstract bool isValid();

        public abstract List<string> ErrorMessage { get; }

        #endregion
    }
}