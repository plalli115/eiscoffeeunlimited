﻿using System;
using System.Collections.Generic;
using System.Web;

namespace ExpandIT31.ExpanditFramework.Core{
    public interface IObserver{
        void update(IObservable obs, object o);
    }
}
