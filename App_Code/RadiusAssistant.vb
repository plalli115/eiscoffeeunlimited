'<!--JA2011021701 - STORE LOCATOR - Start-->
Imports Microsoft.VisualBasic

Public Class RadiusAssistant
    Private PI, EQUATOR_LAT_MILE
    Private maxLat, minLat, maxLong, minLong

    Sub New(ByVal Latitude, ByVal Longitude, ByVal Miles)
        PI = 3.14159265358979
        EQUATOR_LAT_MILE = 69.172
        maxLat = Latitude + Miles / EQUATOR_LAT_MILE
        minLat = Latitude - (maxLat - Latitude)
        maxLong = Longitude + Miles / (Math.Cos(minLat * PI / 180) * EQUATOR_LAT_MILE)
        minLong = Longitude - (maxLong - Longitude)
    End Sub

    Public ReadOnly Property MaxLatitude()
        Get
            Return maxLat
        End Get
    End Property
    Public ReadOnly Property MinLatitude()
        Get
            Return minLat
        End Get
    End Property
    Public ReadOnly Property MaxLongitude()
        Get
            Return maxLong
        End Get
    End Property
    Public ReadOnly Property MinLongitude()
        Get
            Return minLong
        End Get
    End Property

End Class
'<!--JA2011021701 - STORE LOCATOR - End-->
