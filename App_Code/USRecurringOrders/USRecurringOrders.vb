'AM2011032201 - RECURRING ORDERS STAND ALONE - START
Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Collections.Generic
Imports System.Data
Imports System.Net
Imports ExpandIT.GlobalsClass

Namespace ExpandIT

    Public Class USRecurringOrders

        Private globals As GlobalsClass

        Public Sub New(ByVal _globals As GlobalsClass)
            globals = _globals
        End Sub

        Public Function getRecurrencyNextOrderDate(ByVal rNumber As Integer, ByVal rPeriod As String, ByVal rFinalDate As String, ByVal rNextOrderDate As String, ByVal modifiedRecurrency As String, ByVal isActiveOrder As Boolean, ByVal headerDate As DateTime) As String
            Dim recurrentOrderDate As DateTime

            'Check if there is a recurrency for it
            If rNumber > 0 And rPeriod <> "" Then
                'Check if it had a next order date assigned by the customer previously. 
                'That would mean this is at least the second time that the recurrency has been modified.
                If CStrEx(rNextOrderDate) <> "" Then
                    recurrentOrderDate = CDateEx(rNextOrderDate).Date
                    If recurrentOrderDate >= DateTime.Now.Date Then
                        Return recurrentOrderDate.ToString("MM/dd/yyyy")
                    End If
                ElseIf CStrEx(modifiedRecurrency) <> "" Then
                    'This is a modified recurrency(first modification), so get the header date from the original
                    recurrentOrderDate = CDateEx(getSingleValueDB("SELECT HeaderDate FROM ShopSalesHeader WHERE CustomerReference=" & SafeString(modifiedRecurrency))).Date
                Else
                    'It's not a modification
                    If isActiveOrder Then
                        'Use today's date because there is not header date
                        recurrentOrderDate = DateTime.Now.Date
                    Else
                        'This is being called to create an email or load history, use header date
                        recurrentOrderDate = CDateEx(headerDate).Date
                    End If
                End If

                Do
                    If rPeriod = "DAY" Then
                        recurrentOrderDate = recurrentOrderDate.Date.AddDays(rNumber)
                    ElseIf rPeriod = "WEEK" Then
                        recurrentOrderDate = recurrentOrderDate.Date.AddDays(7 * rNumber)
                    ElseIf rPeriod = "MONTH" Then
                        recurrentOrderDate = recurrentOrderDate.Date.AddMonths(rNumber)
                    End If
                Loop While recurrentOrderDate.Date < DateTime.Now.Date

                If CStrEx(rFinalDate) = "" OrElse recurrentOrderDate.Date <= CDateEx(rFinalDate).Date Then
                    Return recurrentOrderDate.ToString("MM/dd/yyyy")
                End If
            End If

            Return ""
        End Function

        Public Function PossiblePaymentTypesSQLString() As String

            Dim sql As New StringBuilder()
            sql.Append("SELECT DISTINCT PaymentType FROM PaymentType WHERE PaymentType='EEPG'")
            Return sql.ToString()

        End Function

        Public Sub applyRecurring(ByRef orderdict As ExpDictionary)
            Dim line As ExpDictionary
            If Not orderdict("Lines") Is Nothing Then
                For Each line In orderdict("Lines").values
                    If CIntEx(HttpContext.Current.Request("txbNumber_" & line("LineGuid"))) = 0 Then
                        line("RNumber") = DBNull.Value
                    Else
                        line("RNumber") = CIntEx(HttpContext.Current.Request("txbNumber_" & line("LineGuid")))
                    End If
                    If CStrEx(HttpContext.Current.Request("Period_" & line("LineGuid"))) = "SELECT" Then
                        line("RPeriod") = DBNull.Value
                    Else
                        line("RPeriod") = CStrEx(HttpContext.Current.Request("Period_" & line("LineGuid")))
                    End If
                    If CIntEx(HttpContext.Current.Request("txbQuantity_" & line("LineGuid"))) = 0 Then
                        line("RQuantity") = DBNull.Value
                    Else
                        line("RQuantity") = CIntEx(HttpContext.Current.Request("txbQuantity_" & line("LineGuid")))
                    End If
                    If HttpContext.Current.Request("txbFinalDate_" & line("LineGuid")) = "" Then
                        line("RFinalDate") = DBNull.Value
                    Else
                        line("RFinalDate") = CDateEx(HttpContext.Current.Request("txbFinalDate_" & line("LineGuid")))
                    End If
                    If CStrEx(orderdict("ModifiedRecurrency")) <> "" Then
                        If HttpContext.Current.Request("txbRNextOrderDate_" & line("LineGuid")) = "" Then
                            line("RNextOrderDate") = DBNull.Value
                        Else
                            line("RNextOrderDate") = CDateEx(HttpContext.Current.Request("txbRNextOrderDate_" & line("LineGuid")))
                        End If
                    End If
                Next
                globals.eis.SaveOrderDictionary(orderdict)
            End If
        End Sub

    End Class

End Namespace
'AM2011032201 - RECURRING ORDERS STAND ALONE - END

