﻿Imports Microsoft.VisualBasic
Imports ExpandIT 
Imports System
Imports System.Collections.Generic 
Imports System.Configuration.ConfigurationManager
Imports System.IO
Imports System.Data 
Imports System.Xml 
Imports System.Xml.Xsl
Imports System.Text 
Imports System.Text.RegularExpressions 
Imports System.Web
Imports ExpandIT31.ExpanditFramework.Util




Public Class NoteMailContentBuilder

  Public Shared Function GetTemplateList(languageISO As String) As List(Of String)
    Dim result As New List(Of String)()    
    Dim templDir As String = AppSettings("NOTE_MAIL_FOLDER") 
    Dim fileNameSuffix As string = String.Format("_{0}.html", languageISO)   
    Dim fileNameMask As string = "*" & fileNameSuffix
    Dim phPath As String = HttpContext.Current.Server.MapPath(String.Format("~/{0}", templDir )) 
    Dim files As String()
    Try 
      files = Directory.GetFiles(phPath , fileNameMask)      
      For Each filename As String In files
        Dim templName As String
        Dim shortFileName As String
        shortFileName = Path.GetFileName (filename)
        templName = shortFileName.Substring(0, shortFileName.IndexOf(fileNameSuffix)) 
        result.Add(templName)
      Next      
    Catch 
    End Try 
    Return result
  End Function
  
  Private Shared function MakeContentTable(contentXml As String) As String
    Dim templDir As String = AppSettings("NOTE_MAIL_FOLDER")
    Dim xsltPath As String = HttpContext.Current.Server.MapPath(String.Format("~/{0}/Note2htm.xslt", templDir))
    Dim outBuilder as StringBuilder = Nothing 
    Dim xsltReader As XmlReader = Nothing   
    Try
      Dim xsltTransf As New XslCompiledTransform()
      xsltReader = XmlReader.Create(New StreamReader(xsltPath)) 
      Dim reader as XmlReader = XmlReader.Create(new StringReader(contentXml))      
      outBuilder = new StringBuilder()       
      Dim writer as TextWriter  = new StringWriter(outBuilder) 
      xsltTransf.Load(xsltReader) 
      xsltTransf.Transform(reader, Nothing, writer)    
    Catch ex As Exception
      'TODO Error handling
    Finally
      If Not xsltReader is Nothing then
        xsltReader.Close()  
      End If
    End Try
    If outBuilder is Nothing then
      Return ""
    Else
      Return outBuilder.ToString()
    End If
  End Function
  
  Public Shared Function Process(userName As String, templateName As String, contentXml As string, languageISO As String, reqEncoding As Encoding) As String    
    Dim result As string = ""
    Try      
      Dim contentHtml As String = MakeContentTable(contentXml)
      Dim csslDir As String = AppSettings("CSS_NOTE_MAIL_FOLDER")
      Dim templDir As String = AppSettings("NOTE_MAIL_FOLDER")
      Dim cssFilePath As String = String.Format("~/{0}/{1}_{2}.css", csslDir, templateName, languageISO)
      Dim templateFilePath As String = String.Format("~/{0}/{1}_{2}.html", templDir, templateName, languageISO)
      'CreateReplacementsList(contentHtml, userName)
      result = MailUtils.MakeReplacements(MailUtils.EmbedCss(templateFilePath, Nothing), CreateReplacementsList(contentHtml, userName)) 
      
    Catch ex As Exception
      'TODO Error handling
    End Try 
    Return result   
  End Function
  
  
  
  Private shared Function  CreateReplacementsList(contentHtml As String, userName As string) As Dictionary(Of String, String)
    Dim replacements As New Dictionary(Of String, String) 
    replacements.Add("%CONTENT%", contentHtml) 
    Dim builder As New StringBuilder(HttpContext.Current.Request.Url.Scheme)
    builder.Append("://")
    builder.Append(HttpContext.Current.Request.Url.Host)
    builder.Append(HttpContext.Current.Request.ApplicationPath)  
    replacements.Add("%ROOT%", builder.ToString())
    replacements.Add("%NAME%", userName)    
    replacements.Add("%ORD_NUMBER%", Resources.Language.LABEL_ORDINAL_NUM) 
    replacements.Add("%PRODUCT_ID%", Resources.Language.LABEL_PRODUCT)
    replacements.Add("%PRODUCT_NAME%", Resources.Language.LABEL_PRODUCT_NAME)
    replacements.Add("%PRICE%", Resources.Language.LABEL_PRICE)
    replacements.Add("%QUANTITY%", Resources.Language.LABEL_QUANTITY) 
    Return replacements 
  End Function 
  
  
    
End Class
