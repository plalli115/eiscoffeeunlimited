Imports System.Configuration.ConfigurationManager
Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

Namespace ExpandIT

    Public Class Closed

        ' Here all the GET elements are picked up and put in a string value.
        Private Shared Function GetAllGetElements() As String
            Dim i As Integer
            Dim retv As String = ""
            Dim stringlist As ArrayList
            Dim varname As String
            Dim querydict As Dictionary(Of String, ArrayList) = QueryString2Dict(HttpContext.Current.Request.QueryString.ToString)

            If querydict.Count > 0 Then
                For Each varname In querydict.Keys
                    stringlist = querydict.Item(varname)
                    For i = 0 To stringlist.Count - 1
                        retv = retv & varname & "=" & HttpUtility.UrlEncode(stringlist(i)) & "&"
                    Next
                Next
                retv = Left(retv, Len(retv) - 1)
            End If
            Return retv
        End Function

        ' Here all the POST elements are picked up written as hidden fields.
        Private Shared Function GetAllPostElements() As String
            Dim i As Integer
            Dim retv As String = ""
            Dim querydict As Dictionary(Of String, ArrayList) = QueryString2Dict(HttpContext.Current.Request.Form.ToString)
            Dim varname As String
            Dim stringlist As ArrayList

            If querydict.Count > 0 Then
                For Each varname In querydict.Keys
                    stringlist = querydict.Item(varname)
                    For i = 0 To querydict.Count - 1
                        retv = retv & "<input type=""hidden"" name=""" & HTMLEncode(varname) & """ value=""" & HTMLEncode(stringlist(i)) & """>"
                    Next
                Next
            End If
            Return retv
        End Function

        Public Shared Function Render() As String
            Dim txtQueryString As String = GetAllGetElements()
            Dim retv As String = ""

            retv = retv & "<html>" & vbCrLf
            retv = retv & "<head>" & vbCrLf
            retv = retv & "	<title>The site is currently being updated</title>" & vbCrLf
            retv = retv & "	<script language=""JavaScript"">" & vbCrLf
            retv = retv & "	<!--" & vbCrLf
            retv = retv & "	function myReLoad()" & vbCrLf
            retv = retv & "	{" & vbCrLf
            retv = retv & "	    // Timeout of 10 seconds before refresh script is activated." & vbCrLf
            retv = retv & "	    setTimeout( ""submitpage()"", 10*1000 );" & vbCrLf
            retv = retv & "	}" & vbCrLf
            retv = retv & "" & vbCrLf
            retv = retv & "	function submitpage()" & vbCrLf
            retv = retv & "	{" & vbCrLf
            retv = retv & "	    // This code will submit the the the form ""formClosed""." & vbCrLf
            retv = retv & "	    formClosed.submit();" & vbCrLf
            retv = retv & "	}" & vbCrLf
            retv = retv & "	//-->" & vbCrLf
            retv = retv & "	</script>" & vbCrLf
            retv = retv & "	<style>" & vbCrLf
            retv = retv & "		<!--" & vbCrLf
            retv = retv & "		TD" & vbCrLf
            retv = retv & "		{" & vbCrLf
            retv = retv & "		    FONT-SIZE: 10pt;" & vbCrLf
            retv = retv & "		    COLOR: #003366;" & vbCrLf
            retv = retv & "		    FONT-FAMILY: Verdana;" & vbCrLf
            retv = retv & "		}" & vbCrLf
            retv = retv & "		H1" & vbCrLf
            retv = retv & "		{" & vbCrLf
            retv = retv & "		    FONT-WEIGHT: bold;" & vbCrLf
            retv = retv & "		    FONT-SIZE: 20;" & vbCrLf
            retv = retv & "		}		" & vbCrLf
            retv = retv & "		.MainWindow" & vbCrLf
            retv = retv & "		{" & vbCrLf
            retv = retv & "		    BORDER-RIGHT: #999999 1px;" & vbCrLf
            retv = retv & "		    BORDER-TOP: #999999 1px solid;" & vbCrLf
            retv = retv & "		    BACKGROUND-IMAGE: url(images/watermark.gif);" & vbCrLf
            retv = retv & "		    BORDER-LEFT: #999999 1px;" & vbCrLf
            retv = retv & "		    BORDER-BOTTOM: #999999 1px solid;" & vbCrLf
            retv = retv & "		    BACKGROUND-REPEAT: repeat" & vbCrLf
            retv = retv & "		}		" & vbCrLf
            retv = retv & "		-->" & vbCrLf
            retv = retv & "	</style>" & vbCrLf
            retv = retv & "	" & vbCrLf
            retv = retv & "</head>" & vbCrLf
            retv = retv & "<body MarginWidth=""0"" MarginHeight=""0"" TopMargin=""0"" BottomMargin=""0"" LeftMargin=""0"" RightMargin=""0"" onload=""myReLoad()"">" & vbCrLf
            retv = retv & "	<table CELLSPACING=""0"" CELLPADDING=""0"" WIDTH=""100%"" HEIGHT=""100%"">" & vbCrLf
            retv = retv & "		<tr>" & vbCrLf
            retv = retv & "			<td CLASS=""MainWindow"" VALIGN=""TOP"" ALIGN=""LEFT"" HEIGHT=""100%"">" & vbCrLf
            retv = retv & "				<h1>The ExpandIT Internet Shop is currently being updated</h1>" & vbCrLf
            retv = retv & "				This Internet Shop is currently being updated. It will be back online in a matter of seconds." & vbCrLf
            retv = retv & "				<br /><br />" & vbCrLf
            retv = retv & "				This page will automatically reload. You will be transferred back to where you came from once the Internet Shop is back online." & vbCrLf
            If txtQueryString <> "" Then
                retv = retv & "					<form ACTION=""" & HttpContext.Current.Request.ServerVariables("SCRIPT_NAME") & "?" & txtQueryString & """ METHOD=""POST"" id=""formClosed"" name=""formClosed"">" & vbCrLf
            Else
                retv = retv & "					<form ACTION=""" & HttpContext.Current.Request.ServerVariables("SCRIPT_NAME") & """ METHOD=""POST"" id=""form1"" name=""formClosed"">" & vbCrLf
            End If
            retv = retv & GetAllPostElements()
            retv = retv & "				" & vbCrLf
            retv = retv & "				If the page are not reloaded automatically, please reload the page manually by pressing this button.<br /><br />" & vbCrLf
            retv = retv & "				<input type=""AddButton"" value=""Reload page"" name=""Reload page""> 				" & vbCrLf
            retv = retv & "				</form>	" & vbCrLf
            retv = retv & "				<br /><br /><br />" & vbCrLf
            retv = retv & "				Kind regards,<br />ExpandIT Solutions a/s" & vbCrLf
            retv = retv & "" & vbCrLf
            retv = retv & "			</td>" & vbCrLf
            retv = retv & "		</tr>" & vbCrLf
            retv = retv & "	</table>" & vbCrLf
            retv = retv & "</body>" & vbCrLf
            retv = retv & "</html>" & vbCrLf

            Return retv
        End Function

    End Class

End Namespace
