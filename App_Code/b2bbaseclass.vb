Imports System.Configuration.ConfigurationManager
Imports Microsoft.VisualBasic
Imports ExpandIT.EISClass
Imports ExpandIT.GlobalsClass

Namespace ExpandIT

    Public Class B2BBaseClass

        Public Const B2BEnabled As Boolean = True

        Protected globals As GlobalsClass
        Protected eis As EISClass

        Public Sub New(ByVal _globals As GlobalsClass)
            globals = _globals
            eis = globals.eis
        End Sub

        Public Overridable Sub DecodeAddress(ByRef dict As Object)
            ' Do nothing
        End Sub

        Public Overridable Function GetCustomerLedgerEntries(ByVal User As ExpDictionary) As ExpDictionary
            Dim retv As New ExpDictionary()

            retv("Lines") = New ExpDictionary()

            Return retv
        End Function

        Public Overridable Function GetDocumentTypeDescription(ByVal lngType As Integer) As String
            Return lngType.ToString
        End Function


    End Class

End Namespace