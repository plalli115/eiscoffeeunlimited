Imports Microsoft.VisualBasic
Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.IO
Imports System.Xml
Imports System.Xml.Serialization

''' <summary>
''' This Class was made to address the serialization issues connected to ASP.net security setting - Medium Trustlevel.
''' In Medium Trustlevel it's not possible to use binary nor SOAP serialization. The only
''' possibility is to use XML serialization. But since XML serializer can't serialize objects that implements
''' IDictionary, we needed to create this class to convert IDictionary objects to generic List objects before serialization.
''' </summary>
''' <remarks></remarks>
Public Class ExpXMLSerializer

    Public Shared Sub Serialize(ByVal writer As MemoryStream, ByVal dictionary As IDictionary)

        Dim entries As List(Of Entry) = New List(Of Entry)(dictionary.Count)

        For Each key As Object In dictionary.Keys
            entries.Add(New Entry(key, dictionary(key)))
        Next

        Dim serializer As XmlSerializer = New XmlSerializer(GetType(List(Of Entry)))
        serializer.Serialize(writer, entries)

    End Sub

    Public Shared Sub Deserialize(ByVal reader As MemoryStream, ByVal dictionary As IDictionary)
        dictionary.Clear()
        Dim serializer As XmlSerializer = New XmlSerializer(GetType(List(Of Entry)))
        Dim list As List(Of Entry) = CType(serializer.Deserialize(reader), List(Of Entry))
        For Each entryObj As Entry In list
            dictionary(entryObj.Key) = entryObj.Value
        Next
    End Sub

    Public Class Entry

        Public Key As Object
        Public Value As Object

        Public Sub New()

        End Sub

        Public Sub New(ByVal _key As Object, ByVal _value As Object)

            Key = _key
            Value = _value

        End Sub

    End Class

End Class