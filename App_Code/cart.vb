Imports System.Configuration.ConfigurationManager
Imports Microsoft.VisualBasic
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Data.SqlClient
Imports System.Data

Namespace ExpandIT

    Public Class CartClass

        Private globals As GlobalsClass

        Public Sub New(ByVal _globals As GlobalsClass)
            globals = _globals
        End Sub

        Public Sub MergeCarts(ByVal UserSource As Object, ByVal UserDest As Object)
            Dim dtCart, dtCartLine As DataTable

            'AM2010021101
            'If UserSource <> UserDest Then
            '    globals.bUserLoaded = False
            '    globals.User = globals.eis.LoadUser(UserDest)
            '    globals.OrderDict = globals.eis.LoadOrderDictionary(globals.User)
            '    dtCart = SQL2DataTable("SELECT * FROM CartHeader WHERE UserGuid=" & SafeString(UserSource))
            '    If dtCart.Rows.Count > 0 Then
            '        dtCartLine = SQL2DataTable("SELECT * FROM CartLine WHERE HeaderGuid=" & SafeString(dtCart.Rows(0)("HeaderGuid")))

            '        For Each row As DataRow In dtCartLine.Rows
            '            'AM0122201001 - UOM - Start
            '            If Appsettings("EXPANDIT_US_USE_UOM") Then 
            '                  globals.eis.AddItemToOrder2(globals.OrderDict, row("ProductGuid"), row("Quantity"), DBNull2Nothing(row("LineComment")), DBNull2Nothing(row("VariantCode")), row("UOM"))
            '            Else
            '                  globals.eis.AddItemToOrder2(globals.OrderDict, row("ProductGuid"), row("Quantity"), DBNull2Nothing(row("LineComment")), DBNull2Nothing(row("VariantCode")))
            '            End If
            '            'AM0122201001 - UOM - End
            '        Next
            '        globals.OrderDict("IsCalculated") = False
            '        globals.eis.SaveOrderDictionary(globals.OrderDict)
            '        excecuteNonQueryDb("DELETE FROM CartLine WHERE HeaderGuid = " & SafeString(dtCart.Rows(0)("HeaderGuid")))
            '        excecuteNonQueryDb("DELETE FROM CartHeader WHERE HeaderGuid = " & SafeString(dtCart.Rows(0)("HeaderGuid")))
            '    End If
            'End If
            If UserSource <> UserDest Then
                globals.bUserLoaded = False
                globals.User = globals.eis.LoadUser(UserSource)
                globals.OrderDict = globals.eis.LoadOrderDictionary(globals.User)
                globals.User = globals.eis.LoadUser(UserDest)
                dtCart = SQL2DataTable("SELECT * FROM CartHeader WHERE MultiCartStatus='ACTIVE' AND UserGuid=" & SafeString(UserDest))
                If dtCart.Rows.Count > 0 Then
                    dtCartLine = SQL2DataTable("SELECT * FROM CartLine WHERE HeaderGuid=" & SafeString(dtCart.Rows(0)("HeaderGuid")))

                    For Each row As DataRow In dtCartLine.Rows
                        'AM0122201001 - UOM - Start
                        If AppSettings("EXPANDIT_US_USE_UOM") Then
                            globals.eis.AddItemToOrder2(globals.OrderDict, row("ProductGuid"), row("Quantity"), DBNull2Nothing(row("LineComment")), DBNull2Nothing(row("VariantCode")), DBNull2Nothing(row("UOM")))
                        Else
                            globals.eis.AddItemToOrder2(globals.OrderDict, row("ProductGuid"), row("Quantity"), DBNull2Nothing(row("LineComment")), DBNull2Nothing(row("VariantCode")))
                        End If
                        'AM0122201001 - UOM - End
                    Next
                    excecuteNonQueryDb("DELETE FROM CartLine WHERE HeaderGuid = " & SafeString(dtCart.Rows(0)("HeaderGuid")))
                    excecuteNonQueryDb("DELETE FROM CartHeader WHERE HeaderGuid = " & SafeString(dtCart.Rows(0)("HeaderGuid")))
                End If
                globals.OrderDict("IsCalculated") = False
                globals.OrderDict("UserGuid") = UserDest
                globals.eis.SaveOrderDictionary(globals.OrderDict)
            End If
        End Sub


    End Class

End Namespace