﻿<!-- AM2010042101 - PICTURES RESIZER - Start -->

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="picturesResizer.aspx.vb"
    Inherits="picturesResizer" CodeFileBaseClass="ExpandIT.Page" %>

<%@ Register Assembly="Radactive.WebControls.ILoad" Namespace="Radactive.WebControls.ILoad"
    TagPrefix="RAWCIL" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <table style="padding-left: 20px; padding-top: 20px;">
            <asp:Repeater ID="RptPictureTypes" runat="server">
                <HeaderTemplate>
                    <tr>
                        <td style="padding-bottom: 10px;">
                            <b>Please select an option below:</b>
                        </td>
                    </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <input id="<%# "cbx_" & Eval("Value")("PictureTypeGuid")%>" name="<%# "cbx_" & Eval("Value")("PictureTypeGuid")%>"
                                type="checkbox" value="<%#  Eval("Value")("PictureTypeGuid")%>" />
                            <label for="<%# "cbx_" & Eval("Value")("PictureTypeGuid")%>">
                                <%#  Eval("Value")("PictureTypeDescription")%>
                            </label>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
            <tr>
                <td style="padding-top: 5px;">
                    <input id="cbx_ALL" type="checkbox" value="ALL" name="cbx_ALL" />
                    <label for="cbx_ALL">
                        ALL Types of Pictures
                    </label>
                </td>
            </tr>
            <tr>
                <td style="padding-top: 10px;">
                    <asp:Button ID="btnProcess" CssClass="AddButton" runat="server" Text="Process Pictures" /></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblError" runat="server" Visible="false" ForeColor="red"></asp:Label></td>
            </tr>
        </table>
    </form>
</body>
</html>
<!-- AM2010042101 - PICTURES RESIZER - End -->
