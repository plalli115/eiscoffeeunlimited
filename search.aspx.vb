Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports System.Web
Imports ExpandIT.ExpandITLib

Partial Class Search
    Inherits ExpandIT.Page

    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	
	Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)

        ' Check the page access.
        eis.PageAccessRedirect("Catalog")
        'AM2010071901 - ENTERPRISE CSC - Start
        Me.CategorySearchResult.CatalogNo = globals.User("CatalogNo")
        'AM2010071901 - ENTERPRISE CSC - End
        'ExpandIT US - USE CSC - Start
        If AppSettings("EXPANDIT_US_USE_CSC") Then
            Dim objCSC As New csc(globals)
            'JA2010030901 - PERFORMANCE -End
            Dim strCSCAvailableItemsSQL As String = objCSC.getListAvailableItems(globals.User)
            Me.CategorySearchResult.ListAvailableItemsSQL = strCSCAvailableItemsSQL
            'JA2010030901 - PERFORMANCE -End
        End If
        'ExpandIT US - USE CSC - End

    End Sub


    Protected Function searchText()
        Dim text As String = "<input type=""text"" name=""mainsearchstring"" id=""mainsearchstring"" value=""" & Server.HtmlEncode(ExpandIT.USSearch.SearchString()) & """"
        text = text & " onfocus = ""this.value = (this.value=='" & Resources.Language.LABEL_SEARCH & "')? '' : this.value;"""
        text = text & " onblur=""this.value = (this.value=='')? '" & Resources.Language.LABEL_SEARCH & "' : this.value;"" class=""borderTextBox"" style=""width: 200px;vertical-align:middle;""/>"
        Return text
    End Function
End Class