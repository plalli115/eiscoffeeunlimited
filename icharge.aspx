<%@ Page Language="vb" AutoEventWireup="false" Inherits="vb.icharge" CodeFile="icharge.aspx.vb" %>

<%@ Register Assembly="nsoftware.IBizPayWeb" Namespace="nsoftware.IBizPay" TagPrefix="cc1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD runat="server">
		<title>IBiz E-Payment Integrator ASP.NET Edition</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="stylesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<h1>IBiz E-Payment Integrator ASP.NET Edition - Demo Pages</h1>
			<h2>Use ICharge To Authorize Credit Cards</h2>
			<p/>
			Illustrates the use of the&nbsp;ICharge Object
			<p></p>
			<p></p>
			<hr size="1">
			<p>
                <cc1:Icharge ID="icharge1" runat="server">
                </cc1:Icharge>
				<table id="Table2" width="90%">
					<tr>
						<td width="304">
							<table id="Table1" cellspacing="2" cellpadding="5">
								<tr>
									<th nowrap colspan="2">
										Gateway Information
									</th>
								<tr>
									<td nowrap align="right">Gateway:</td>
									<td>
										<asp:dropdownlist id="ddlGateway" runat="server" autopostback="True"></asp:dropdownlist></td>
								</tr>
								<tr>
									<td nowrap align="right">Login:
									</td>
									<td>
										<asp:textbox id="txtGatewayLogin" runat="server" width="120px">cnpdev1047</asp:textbox></td>
								</tr>
								<tr>
									<td nowrap align="right">Password:</td>
									<td>
										<asp:textbox id="txtGatewayPassword" runat="server" width="117px"></asp:textbox></td>
								</tr>
								<tr>
									<th nowrap colspan="2">
										Credit Card Information
									</th>
								</tr>
								<tr>
									<td nowrap align="right">Card Number:</td>
									<td>
										<asp:textbox id="txtCardNumber" runat="server" width="144px">4444333322221111</asp:textbox></td>
								</tr>
								<tr>
									<td nowrap align="right">Exp Date (MM/YY):
									</td>
									<td>
										<asp:dropdownlist id="ddlCardExpMonth" runat="server">
											<asp:listitem value="01">01</asp:listitem>
											<asp:listitem value="02">02</asp:listitem>
											<asp:listitem value="03">03</asp:listitem>
											<asp:listitem value="04">04</asp:listitem>
											<asp:listitem value="05">05</asp:listitem>
											<asp:listitem value="06">06</asp:listitem>
											<asp:listitem value="07">07</asp:listitem>
											<asp:listitem value="08">08</asp:listitem>
											<asp:listitem value="09">09</asp:listitem>
											<asp:listitem value="10">10</asp:listitem>
											<asp:listitem value="11">11</asp:listitem>
											<asp:listitem value="12">12</asp:listitem>
										</asp:dropdownlist>&nbsp;/&nbsp;
										<asp:dropdownlist id="ddlCardExpYear" runat="server"></asp:dropdownlist></td>
								</tr>
								<tr>
									<td nowrap align="right" height="31">CVV2 Data:</td>
									<td height="31">
										<asp:textbox id="txtCardCVV2" runat="server" width="117px"></asp:textbox></td>
								</tr>
								<tr>
									<th nowrap colspan="2">
										Transaction Information
									</th>
								<tr>
									<td nowrap align="right">Amount:</td>
									<td>
										<asp:textbox id="txtTransactionAmount" runat="server" width="144px">1.00</asp:textbox></td>
								</tr>
								<tr>
									<td nowrap align="right">Description:
									</td>
									<td>
										<asp:textbox id="txtTransactionDescription" runat="server" width="117px">Fake Sale</asp:textbox></td>
								</tr>
								<tr>
									<td nowrap align="right">Invoice #:</td>
									<td>
										<asp:textbox id="txtTransactionInvoice" runat="server" width="117px">1</asp:textbox></td>
								</tr>
							</table>
						</td>
						<td>
							<table id="Table3" cellspacing="2" cellpadding="5">
								<tr>
									<th nowrap colspan="2">
										Customer Information:
									</th>
								<tr>
									<td nowrap align="right">Name:</td>
									<td>
										<asp:textbox id="txtCustomerFirstName" runat="server" width="62px">John</asp:textbox>
										<asp:textbox id="txtCustomerLastName" runat="server" width="117px">Smith</asp:textbox></td>
								</tr>
								<tr>
									<td nowrap align="right">Address:</td>
									<td>
										<asp:textbox id="txtCustomerAddress" runat="server" width="181px">123 Nowhere Ln</asp:textbox></td>
								</tr>
								<tr>
									<td nowrap align="right">City / State:</td>
									<td>
										<asp:textbox id="txtCustomerCity" runat="server" width="142px">Beverly Hills</asp:textbox>
										<asp:textbox id="txtCustomerState" runat="server" width="39px">CA</asp:textbox></td>
								</tr>
								<tr>
									<td nowrap align="right">Zip:</td>
									<td>
										<asp:textbox id="txtCustomerZip" runat="server" width="182px">90210</asp:textbox></td>
								</tr>
								<tr>
									<td nowrap align="right">Phone:</td>
									<td>
										<asp:textbox id="txtCustomerPhone" runat="server" width="182px">800-555-5555</asp:textbox></td>
								</tr>
								<tr>
									<td nowrap align="right">Email:</td>
									<td>
										<asp:textbox id="txtCustomerEmail" runat="server" width="182px">me@server.com</asp:textbox></td>
								</tr>
								<tr>
									<td align="right">Customer ID:</td>
									<td>
										<asp:textbox id="txtCustomerID" runat="server" width="182px">CUSTOMER1</asp:textbox></td>
								</tr>
							</table>
							<P align="center">
								<asp:button id="bCharge" CssClass="AddButton" runat="server" text="Authorize"></asp:button></P>
							<P align="center">
								<asp:Label id="lblWarning" runat="server" ForeColor="Red" Visible="False" Font-Size="XX-Small">Supply a Login/Password or use Authorize.Net</asp:Label></P>
						</td>
					</tr>
				</table>
				<strong>
					<asp:literal id="litOutput" runat="server" enableviewstate="False"></asp:literal></strong>
				<asp:panel id="pnlAuthResults" runat="server" width="99.46%" enableviewstate="False" visible="False" height="72px">
					<TABLE id="Table4" cellSpacing="0" cellPadding="5" border="0">
						<TR>
							<TH width="259" colSpan="2">
								<STRONG>Transaction Response Info</STRONG></TH></TR>
						<TR>
							<TD noWrap align="right">Response Code :
							</TD>
							<TD width="128">
								<asp:label id="Label1" runat="server" width="100%">
									<%# icharge1.ResponseCode %>
								</asp:label></TD>
						</TR>
						<TR>
							<TD noWrap align="right">Approval Code :
							</TD>
							<TD width="128">
								<asp:label id="Label2" runat="server" width="100%">
									<%# icharge1.ResponseApprovalCode %>
								</asp:label></TD>
						</TR>
						<TR>
							<TD noWrap align="right">Response Text :
							</TD>
							<TD width="128">
								<asp:label id="Label3" runat="server" width="100%">
									<%# icharge1.ResponseText %>
								</asp:label></TD>
						</TR>
						<TR>
							<TD noWrap align="right">Response AVS :
							</TD>
							<TD width="128">
								<asp:label id="Label4" runat="server" width="100%">
									<%# icharge1.ResponseAVS %>
								</asp:label></TD>
						</TR>
						<TR>
							<TD noWrap align="right">Response CVV2 :
							</TD>
							<TD width="128">
								<asp:label id="Label5" runat="server" width="100%">
									<%# icharge1.ResponseCVV2 %>
								</asp:label></TD>
						</TR>
						<TR>
							<TD noWrap align="right">Invoice Num :
							</TD>
							<TD width="128">
								<asp:label id="Label6" runat="server" width="100%">
									<%# icharge1.ResponseInvoiceNumber %>
								</asp:label></TD>
						</TR>
						<TR>
							<TD noWrap align="right">Transaction Id :
							</TD>
							<TD width="128">
								<asp:label id="Label7" runat="server" width="100%">
									<%# icharge1.ResponseTransactionId %>
								</asp:label></TD>
						</TR>
					</TABLE>
				</asp:panel><br>
			</p>
			<p/>
				<hr size="1">
			<p>&nbsp;</p>
			<p></p>
			<p><br>
				<br>
				&nbsp;</p>
			<p>NOTE: These pages are simple demos, and by no means complete applications. They 
				are intended to illustrate the usage of the IBiz E-Payment Integrator .NET objects in a 
				simple, straightforward way. What we are hoping to demonstrate is how simple it 
				is to program with our tools. If you want to know more about them, or if you 
				have questions, please visit <a href="http://www.nsoftware.com/?demopg-BBN5A">www.nsoftware.com</a>
				or email to <a href="mailto:support@nsoftware.com">support@nsoftware.com</a>.
				<br>
				<br>
				Copyright � 2005 /n software inc. All rights reserved</p>
		</form>
	</body>
</HTML>
