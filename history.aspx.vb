Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT.GlobalsClass

Partial Class history
    Inherits ExpandIT.Page

    Protected objPage As New PagedDataSource()
    Protected Group As ExpDictionary

    Protected page1 As String = "0"
    Protected page2 As String = "0"
    Protected page3 As String = "0"
    'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
    Protected csrObj As USCSR
    'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
    'AM2011032201 - RECURRING ORDERS STAND ALONE - START
    Protected recurringObj As USRecurringOrders
    'AM2011032201 - RECURRING ORDERS STAND ALONE - END

    Public Property CurrentPage() As Integer
        Get
            If (Me.ViewState("CurrentPage") Is Nothing) Then
                Return 0
            Else
                Return Integer.Parse(Me.ViewState("CurrentPage").ToString())
            End If
        End Get

        Set(ByVal value As Integer)
            Me.ViewState("CurrentPage") = value
        End Set
    End Property

    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        ' Check the page access.
        eis.PageAccessRedirect("Order")

        ' Check if customer is registered
        If globals.User("Anonymous") Then
            Response.Redirect("user_login.aspx?returl=" & Server.UrlEncode(ToString(Request.ServerVariables("SCRIPT_NAME"))))
        End If


        If CStrEx(page1) = "0" Then
            page1 = CStrEx(AppSettings("Page_1"))
        End If
        If CStrEx(page2) = "0" Then
            page2 = CStrEx(AppSettings("Page_2"))
        End If
        If CStrEx(page3) = "0" Then
            page3 = CStrEx(AppSettings("Page_3"))
        End If
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
        csrObj = New USCSR(globals)
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
        'AM2011032201 - RECURRING ORDERS STAND ALONE - START
        recurringObj = New USRecurringOrders(globals)
        'AM2011032201 - RECURRING ORDERS STAND ALONE - END

    End Sub

    Protected Overloads Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender

        If Not Page.IsPostBack Then
            Dim list1 As New ListItem(page1, page1, True)
            Me.ddlPageSize.Items.Add(list1)
            Dim list2 As New ListItem(page2, page2)
            Me.ddlPageSize.Items.Add(list2)
            Dim list3 As New ListItem(page3, page3)
            Me.ddlPageSize.Items.Add(list3)
            Dim all As New ListItem("All", "All")
            Me.ddlPageSize.Items.Add(all)


            Me.ddlPageSize2.Items.Add(list1)
            Me.ddlPageSize2.Items.Add(list2)
            Me.ddlPageSize2.Items.Add(list3)
            Me.ddlPageSize2.Items.Add(all)

            'AM2010092201 - ENTERPRISE CSR - Start
            If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And csrObj.getSalesPersonGuid() <> "" And CStrEx(Request("CSR")) = "1" And CStrEx(Request("Status")) = "" And CStrEx(Request("park")) = "" Then
                Me.csrOrderStatus.Items.Add(New ListItem(Resources.Language.LABEL_CSR_STATUS_ALL, Resources.Language.LABEL_CSR_STATUS_ALL))
                Me.csrOrderStatus.Items.Add(New ListItem(Resources.Language.LABEL_CSR_STATUS_ORDERED, Resources.Language.LABEL_CSR_STATUS_ORDERED))
                Me.csrOrderStatus.Items.Add(New ListItem(Resources.Language.LABEL_CSR_STATUS_CANCELLED, Resources.Language.LABEL_CSR_STATUS_CANCELLED))
                Me.csrOrderStatus.Items.Add(New ListItem(Resources.Language.LABEL_CSR_STATUS_RETURNED, Resources.Language.LABEL_CSR_STATUS_RETURNED))
                Me.csrOrderStatus.Items.Add(New ListItem(Resources.Language.LABEL_CSR_STATUS_SHIPPED, Resources.Language.LABEL_CSR_STATUS_SHIPPED))
                Me.csrOrderStatus.Items.Add(New ListItem(Resources.Language.LABEL_CSR_STATUS_PARTIALLY_SHIPPED, Resources.Language.LABEL_CSR_STATUS_PARTIALLY_SHIPPED))
            End If
            'AM2010092201 - ENTERPRISE CSR - End

        End If

        BindData()

    End Sub

    Private Sub BindData(Optional ByVal fromEvent As String = "")

        'AM2010092201 - ENTERPRISE CSR - Start
        Dim sql As String
        If CStrEx(Request("Status")) = "cancell" Then
            sql = "SELECT SH.[HeaderGuid], SH.[CurrencyGuid],HeaderDate As HeaderDateOriginal, CONVERT(VARCHAR(10), SH.[HeaderDate],101) As [HeaderDate] , SH.[CustomerReference], SH.[InvoiceDiscount], " & _
                        " SH.[ServiceCharge], SH.[SubTotal], SH.[SubTotalInclTax], SH.[TotalInclTax], SH.[Total], SH.[UserGuid]"
        ElseIf CStrEx(Request("park")) = "park" Then
            sql = "SELECT [HeaderGuid], [CurrencyGuid], CreatedDate As HeaderDateOriginal,CONVERT(VARCHAR(10),[CreatedDate],101) As [CreatedDate], [CustomerReference], [InvoiceDiscount], [ServiceCharge], " & _
                        " [SubTotal], [SubTotalInclTax], [TotalInclTax], [Total], [UserGuid]"
        Else
            sql = "SELECT [HeaderGuid], [CurrencyGuid],HeaderDate As HeaderDateOriginal, CONVERT(VARCHAR(10),[HeaderDate],101) As [HeaderDate] , [CustomerReference], [InvoiceDiscount], " & _
                        "[ServiceCharge], [SubTotal], [SubTotalInclTax], [TotalInclTax], [Total], [UserGuid]"
        End If

        If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And csrObj.getSalesPersonGuid() <> "" Then
            If CStrEx(Request("CSR")) = "1" Then
                If CStrEx(Request("Status")) = "cancell" Then
                    sql &= " , C.[SalesPersonGuid] FROM [ShopSalesHeader] As SH INNER JOIN [CancelledOrders] As C ON SH.[HeaderGuid]=C.[HeaderGuidOriginal] WHERE CancelOrder=1 AND (C.[SalesPersonGuid] = " & SafeString(csrObj.getSalesPersonGuid()) & ") ORDER BY [HeaderDateOriginal] DESC"
                ElseIf CStrEx(Request("Status")) = "recurrency" Then
                    sql &= " FROM [ShopSalesHeader] WHERE ([SalesPersonGuid] = " & SafeString(csrObj.getSalesPersonGuid()) & ") AND (RecurringOrder = 1) AND (ModifiedRecurrency IS NULL OR ISNULL(ModifiedRecurrency, '') = '' ) AND [HeaderGuid] NOT IN ( SELECT HeaderGuidOriginal FROM CancelledOrders WHERE CancelRecurrency=1 ) ORDER BY [HeaderDateOriginal] DESC"
                ElseIf CStrEx(Request("park")) = "park" Then
                    sql &= " FROM [CartHeader] WHERE ([SalesPersonGuid] = " & SafeString(csrObj.getSalesPersonGuid()) & ") AND (MultiCartStatus='HOLD') ORDER BY [HeaderDateOriginal] DESC"
                Else
                    sql &= " FROM [ShopSalesHeader] WHERE ([SalesPersonGuid] = " & SafeString(csrObj.getSalesPersonGuid()) & ") AND (ModifiedRecurrency IS NULL) AND (ISNULL(DocumentType, '') <> 'Return Order' OR ISNULL(DocumentType, '') = 'Return Order' AND OrderReference Is Null) ORDER BY [HeaderDateOriginal] DESC"
                End If
            Else
                If CStrEx(Request("Status")) = "cancell" Then
                    sql &= " , C.[SalesPersonGuid] FROM [ShopSalesHeader] As SH INNER JOIN [CancelledOrders] As C ON SH.[HeaderGuid]=C.[HeaderGuidOriginal] WHERE CancelOrder=1 AND ([UserGuid] = " & SafeString(globals.User("UserGuid")) & ") ORDER BY [HeaderDateOriginal] DESC"
                ElseIf CStrEx(Request("park")) = "park" Then
                    sql &= " , [SalesPersonGuid] FROM [CartHeader] WHERE ([UserGuid] = " & SafeString(globals.User("UserGuid")) & ") AND (MultiCartStatus='HOLD') ORDER BY [HeaderDateOriginal] DESC"
                Else
                    sql &= ", [SalesPersonGuid] FROM [ShopSalesHeader] WHERE ([UserGuid] = " & SafeString(globals.User("UserGuid")) & ") AND (ModifiedRecurrency IS NULL) AND (ISNULL(DocumentType, '') <> 'Return Order' OR ISNULL(DocumentType, '') = 'Return Order' AND OrderReference Is Null) ORDER BY [HeaderDateOriginal] DESC"
                End If
            End If
        Else
            sql &= " FROM [ShopSalesHeader] WHERE ([UserGuid] = " & SafeString(globals.User("UserGuid")) & ") ORDER BY [HeaderDateOriginal] DESC"
        End If
        'AM2010092201 - ENTERPRISE CSR - End

        Group = SQL2Dicts(sql)

        'AM2010092201 - ENTERPRISE CSR - Start
        If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And csrObj.getSalesPersonGuid() <> "" And CStrEx(Request("CSR")) = "1" And CStrEx(Request("Status")) = "" And CStrEx(Request("park")) = "" Then
            Dim filteredGroup As New ExpDictionary
            If CStrEx(Me.csrOrderStatus.SelectedValue) <> CStrEx(Resources.Language.LABEL_CSR_STATUS_ALL) Then
                If Not Group Is Nothing Then
                    For Each item As ExpDictionary In Group.Values
                        If CStrEx(csrObj.getOrderStatusFromHeader(item("HeaderGuid"))) = CStrEx(Me.csrOrderStatus.SelectedValue) Then
                            filteredGroup.Add(item("HeaderGuid"), item)
                        End If
                    Next
                End If
                Group = filteredGroup
            End If
        End If
        'AM2010092201 - ENTERPRISE CSR - End

        objPage.DataSource = Group

        If Not Group Is Nothing Then
            objPage.AllowPaging = True
            If fromEvent = "" Or fromEvent = "selectedIndex" Then
            If CStrEx(ddlPageSize.SelectedValue) = "All" Then
                objPage.PageSize = Group.Count
                ddlPageSize2.SelectedValue = "All"
            Else
                objPage.PageSize = Integer.Parse(ddlPageSize.SelectedValue)
                ddlPageSize2.SelectedValue = Integer.Parse(ddlPageSize.SelectedValue)
            End If
            ElseIf fromEvent = "selectedIndex2" Then
            If CStrEx(ddlPageSize2.SelectedValue) = "All" Then
                objPage.PageSize = Group.Count
                ddlPageSize.SelectedValue = "All"
            Else
                objPage.PageSize = Integer.Parse(ddlPageSize2.SelectedValue)
                ddlPageSize.SelectedValue = Integer.Parse(ddlPageSize2.SelectedValue)
            End If
            End If
            If CurrentPage < 0 Or CurrentPage >= objPage.PageCount Then
                CurrentPage = 0
            End If
            objPage.CurrentPageIndex = CurrentPage
            lnkbtnNext.Visible = Not objPage.IsLastPage
            lnkbtnPrevious.Visible = Not objPage.IsFirstPage
            lnkbtnNext2.Visible = Not objPage.IsLastPage
            lnkbtnPrevious2.Visible = Not objPage.IsFirstPage
            pricelist.DataSource = objPage
            pricelist.DataBind()
            Me.txtPage.Text = CurrentPage + 1
            Me.txtPage2.Text = CurrentPage + 1
            Me.lblNumPages.Text = Resources.Language.LABEL_PAGINATION_OF & " " & objPage.PageCount
            Me.lblNumPages2.Text = Resources.Language.LABEL_PAGINATION_OF & " " & objPage.PageCount
        End If
    End Sub

    Sub dlPaging_ItemCommand(ByVal sender As Object, ByVal e As DataListCommandEventArgs)

        If (e.CommandName.Equals("lnkbtnPaging")) Then
            CurrentPage = Integer.Parse(e.CommandArgument.ToString())
            BindData()
        End If

    End Sub

    Sub dlPaging_ItemDataBound(ByVal sender As Object, ByVal e As DataListItemEventArgs)

        Dim lnkbtnPage = CType(e.Item.FindControl("lnkbtnPaging"), LinkButton)
        If (lnkbtnPage.CommandArgument.ToString() = CurrentPage.ToString()) Then
            lnkbtnPage.Enabled = False
            lnkbtnPage.Font.Bold = True
        End If
    End Sub
    Sub lnkbtnPrevious_Click(ByVal sender As Object, ByVal e As EventArgs)

        CurrentPage -= 1
        BindData()

    End Sub
    Sub lnkbtnPrevious2_Click(ByVal sender As Object, ByVal e As EventArgs)

        CurrentPage -= 1
        BindData()

    End Sub

    Sub txtPage_TextChanged(ByVal sender As Object, ByVal e As EventArgs)

        CurrentPage = CIntEx(txtPage.Text) - 1
        BindData()

    End Sub
    Sub txtPage2_TextChanged(ByVal sender As Object, ByVal e As EventArgs)

        CurrentPage = CIntEx(txtPage2.Text) - 1
        BindData()

    End Sub

    Sub lnkbtnNext_Click(ByVal sender As Object, ByVal e As EventArgs)

        CurrentPage += 1
        BindData()

    End Sub

    Sub lnkbtnNext2_Click(ByVal sender As Object, ByVal e As EventArgs)

        CurrentPage += 1
        BindData()

    End Sub

    Protected Sub ddlPageSize_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
        CurrentPage = 0
        BindData("selectedIndex")
    End Sub

    Protected Sub ddlPageSize2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPageSize2.SelectedIndexChanged
        CurrentPage = 0
        BindData("selectedIndex2")
    End Sub

    Protected Function getUrl(ByVal link As String, ByVal reference As String)
        Dim url As String = ""
        If CStrEx(Request("Status")) = "cancell" Or CStrEx(csrObj.getOrderStatusFromHeader(link)) = CStrEx(Resources.Language.LABEL_CSR_STATUS_CANCELLED) Then
            url = "history_detail.aspx?HeaderGuid=" & link & "&CancelationTrack=1" & "&CustRef=" & reference
        ElseIf CStrEx(Request("park")) = "park" Then
            If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And csrObj.getSalesPersonGuid() <> "" Then
                'Dim sql As String = "SELECT SalesPersonGuid FROM CartHeader WHERE HeaderGuid=" & SafeString(link)
                'Dim salesGuid As String = CStrEx(getSingleValueDB(sql))
                Dim orderUserGuid As String = CStrEx(getSingleValueDB("SELECT UserGuid FROM CartHeader WHERE HeaderGuid=" & SafeString(link)))
                'If salesGuid = csrobj.getSalesPersonGuid() Then
                url = "cart.aspx?HeaderGuidHold=" & link
                'Else
                '    url = "cart.aspx?HeaderGuidHold=" & link & "&parkChangeSalesGuid=1"
                'End If
                If globals.User("UserGuid") <> orderUserGuid Then
                    url = "userSearchSelection.aspx?OrderUserGuid=" & orderUserGuid & "&returl=" & HTMLEncode(url)
                End If
            Else
                url = "cart.aspx?HeaderGuidHold=" & link
            End If
        Else
            url = "history_detail.aspx?HeaderGuid=" & link & "&CustRef=" & reference
        End If
        Return url
    End Function


    Protected Function getImageUserGuid(ByVal userGuidTable As String)
        Dim str As String = ""
        Dim root As String = ""
        If CStrEx(userGuidTable) = CStrEx(globals.User("UserGuid")) Then
            root = VRoot & "/App_Themes/EnterpriseBlue/Messages/action.png"
            str = "<img style=""width:14px;"" src=""" & root & """ />"
        Else
            root = VRoot & "/App_Themes/EnterpriseBlue/p.gif"
            str = "<img src=""" & root & """ />"
        End If

        Return str

    End Function
    'AM2010092201 - ENTERPRISE CSR - Start
    Protected Function getNextShipmentDate(ByVal guid As Object, ByVal headerDate As Object)

        Dim sql As String = ""
        sql = "SELECT ModifiedRecurrency FROM ShopSalesHeader WHERE HeaderGuid=" & SafeString(CStrEx(guid))
        Dim ModifiedRecurrency As String = CStrEx(getSingleValueDB(sql))

        sql = "SELECT * FROM ShopSalesLine WHERE HeaderGuid=" & SafeString(CStrEx(guid))
        Dim retval As ExpDictionary = SQL2Dicts(sql)
        Dim theDate As String = ""
        Dim theDate2 As String = ""
        Dim finalDate As String = ""
        If Not retval Is Nothing AndAlso Not retval Is DBNull.Value AndAlso retval.Count > 0 Then
            For Each line As ExpDictionary In retval.Values
                theDate = recurringObj.getRecurrencyNextOrderDate(CIntEx(line("RNumber")), CStrEx(line("RPeriod")), CStrEx(line("RFinalDate")), CStrEx(line("RNextOrderDate")), CStrEx(ModifiedRecurrency), False, headerDate)
                If CStrEx(theDate) <> "" Then
                    If theDate2 = "" Then
                        theDate2 = theDate
                        finalDate = theDate
                    End If
                    If CDateEx(theDate) < CDateEx(theDate2) Then
                        finalDate = theDate
                        theDate2 = theDate
                    End If
                End If
            Next
        End If
        Return finalDate
    End Function


    Protected Function isRecurrency(ByVal guid As Object)
        Dim sql As String = ""

        ' sql = "SELECT [HeaderGuid] FROM [ShopSalesHeader] WHERE (RecurringOrder=1) AND (ModifiedRecurrency IS NULL OR ISNULL(ModifiedRecurrency, '')= '') AND ([SalesPersonGuid] =" & SafeString(csrobj.getSalesPersonGuid()) & " ) AND [HeaderGuid] NOT IN (SELECT HeaderGuidOriginal FROM CancelledOrders WHERE CancelRecurrency=1) ORDER BY [HeaderDate] DESC"
        sql = "SELECT [HeaderGuid] FROM [ShopSalesHeader] WHERE (RecurringOrder=1) ORDER BY [HeaderDate] DESC"


        Dim retval As ExpDictionary = SQL2Dicts(sql)
        Dim src As String = VRoot & "/images/p.gif"

        If Not retval Is Nothing AndAlso Not retval Is DBNull.Value AndAlso retval.Count > 0 Then
            For Each line As ExpDictionary In retval.Values
                If CStrEx(line("HeaderGuid")) = CStrEx(guid) Then
                    src = VRoot & "/App_Themes/EnterpriseBlue/Messages/action.png"
                    Exit For
                End If
            Next
        End If
        Return src

    End Function

    Protected Function getContactName(ByVal userguid As String)
        Dim sql As String = ""
        Dim contactName As String = ""
        sql = "SELECT [ContactName] FROM [UserTable] WHERE UserGuid=" & SafeString(userguid)
        contactName = CStrEx(getSingleValueDB(sql))

        Return contactName

    End Function
    'AM2010092201 - ENTERPRISE CSR - End

End Class
