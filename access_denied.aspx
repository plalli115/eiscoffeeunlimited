<%@ Import Namespace="System.Configuration.ConfigurationManager" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="access_denied.aspx.vb" Inherits="access_denied"
    CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master"
    RuntimeMasterPageFile="ThreeColumn.master" %>

<%@ Import Namespace="ExpandIT.EISClass" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <div class="AccessDeniedPage">
        <p>
            <% =Resources.Language.LABEL_YOUR_LOGIN_NOT_ACCEPTET_PART_1%>
        </p>
    </div>
</asp:Content>
