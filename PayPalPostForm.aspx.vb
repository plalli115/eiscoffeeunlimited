Option Strict On
Imports System.Configuration.ConfigurationManager
Imports System.IO
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

Partial Class PayPalPostForm
    Inherits System.Web.UI.Page

    Private cmd As String = AppSettings("cmd")
    Private business As String = AppSettings("BusinessEmail")
    Private return_url As String 'success url
    Private notify_url As String 'callback url
    Private shop_url As String 'continue shopping url
    Private cancel_url As String 'return to merchant url
    Private currency_code As String
    Private URL As String 'PayPal url
    Private rm As String
    Private OrderDict As ExpDictionary
    Private Shipping As ExpDictionary

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        return_url = ShopFullPath() & AppSettings("ReturnUrl")
        notify_url = ShopFullPath() & AppSettings("NotifyUrl")
        shop_url = ShopFullPath() & AppSettings("ShopUrl")
        cancel_url = ShopFullPath() & AppSettings("CancelUrl")
        OrderDict = CType(Context.Items("OrderDict"), ExpDictionary)
        Shipping = CType(Context.Items("Shipping"), ExpDictionary)
        currency_code = CStr(OrderDict("CurrencyGuid"))

        ' determining the URL to work with depending on whether sandbox or a real PayPal account should be used
        If AppSettings("UseSandbox").ToString = "true" Then
            URL = "https://www.sandbox.paypal.com/cgi-bin/webscr"
        Else
            URL = "https://www.paypal.com/cgi-bin/webscr"
        End If

        ' This parameter determines the way information about successfull transaction will be passed to the script
        ' specified in the return_url parameter.
        ' "1" - no parameters will be passed.
        ' "2" - the POST method will be used.
        ' "0" - the GET method will be used. 
        ' The parameter is "0" by deault.
        If AppSettings("SendToReturnURL").ToString = "true" Then
            rm = "2"
        Else
            rm = "1"
        End If
    End Sub

    Private Function ShopFullPath() As String
            Dim Protocol, Port, ServerName As String
            Protocol = "http://"
            Port = HttpContext.Current.Request.ServerVariables("SERVER_PORT")
            If Port = "80" Then Port = "" Else Port = ":" & Port
            If CBool(InStr(1, UCase(HttpContext.Current.Request.ServerVariables("SERVER_PROTOCOL")), "HTTPS")) Then
                If Port = "443" Then Port = ""
                Protocol = "https://"
            End If
            ServerName = HttpContext.Current.Request.ServerVariables("HTTP_HOST")
            If InStr(1, ServerName, ":") > 0 Then
                ServerName = Mid(ServerName, 1, InStr(1, ServerName, ":") - 1)
            End If
            ShopFullPath = Protocol & ServerName & Port & CStr(VRoot)
    End Function

    Private Function formatAmountPayPal(ByVal amount As String) As String
        Return Replace(FormatNumber(amount, -1, CType(-1, TriState), 0, 0), ",", ".")
    End Function

    Protected Function writeCartLines() As String
        Dim sb As StringBuilder = New StringBuilder()
        sb.Append("Payment for ordernr: " & CStr(OrderDict("CustomerReference")))
        Return sb.ToString()
    End Function

    Protected Function writeFormCart() As String
        Dim sb As StringBuilder = New StringBuilder()
        sb.Append("<form id=""payForm"" action=""" & URL & """")
        sb.AppendLine(" method=""post"">")
        sb.AppendLine("<input type=""hidden"" name=""charset"" value=""utf-8""/>")
        sb.AppendLine("<input type=""hidden"" name=""cmd"" value=""" & cmd & """/>")
        sb.AppendLine("<input type=""hidden"" name=""upload"" value=""1""/>")
        sb.AppendLine("<input type=""hidden"" name=""business"" value=""" & business & """/>")
        sb.AppendLine("<input type=""hidden"" name=""item_name"" value=""" & writeCartLines() & """/>")
        sb.AppendLine("<input type=""hidden"" name=""amount"" value=""" & formatAmountPayPal(CStr(OrderDict("TotalInclTax"))) & """/>")
        Dim handling As Decimal = (CDec(OrderDict("ServiceChargeInclTax")) + CDec(OrderDict("ShippingAmountInclTax"))) + CDec(OrderDict("HandlingAmountInclTax"))
        sb.AppendLine("<input type=""hidden"" name=""handling_cart"" value=""" & formatAmountPayPal(handling.ToString()) & """/>")
        sb.AppendLine("<input type=""hidden"" name=""return"" value=""" & return_url & """/>")
        sb.AppendLine("<input type=""hidden"" name=""rm"" value=""" & rm & """/>")
        sb.AppendLine("<input type=""hidden"" name=""notify_url"" value=""" & notify_url & """/>")
        sb.AppendLine("<input type=""hidden"" name=""cancel_return"" value=""" & cancel_url & """/>")
        sb.AppendLine("<input type=""hidden"" name=""shopping_url"" value=""" & shop_url & """/>")
        sb.AppendLine("<input type=""hidden"" name=""currency_code"" value=""" & currency_code & """/>")
        sb.AppendLine("<input type=""hidden"" name=""custom"" value=""" & CStr(OrderDict("UserGuid")) & """/>")
        sb.AppendLine("<input type=""hidden"" name=""invoice"" value=""" & CStr(OrderDict("CustomerReference")) & """/>")
        sb.AppendLine("<input type=""hidden"" name=""address_override"" value=""1""/>")
        Dim strs As String() = split(CStr(Shipping("ShipToContactName")))
        If strs IsNot Nothing Then
            Dim strlen As Integer = strs.Length
            If strlen > 1 Then
                sb.Append("<input type=""hidden"" name=""first_name"" value=""")
                For i As Integer = 0 To strlen - 2
                    sb.Append(strs(i))
                    If i < strlen - 2 Then
                        sb.Append(" ")
                    End If
                Next
                sb.AppendLine("""/>")
                sb.AppendLine("<input type=""hidden"" name=""last_name"" value=""" & strs(strs.Length - 1) & """/>")
            ElseIf strs.Length = 1 Then
                sb.AppendLine("<input type=""hidden"" name=""first_name"" value=""" & strs(0) & """/>")
                sb.AppendLine("<input type=""hidden"" name=""last_name"" value=""" & strs(0) & """/>")
            End If
        End If
        sb.AppendLine("<input type=""hidden"" name=""address1"" value=""" & CStrEx(Shipping("ShipToAddress1")) & """/>")
        sb.AppendLine("<input type=""hidden"" name=""address2"" value=""" & CStrEx(Shipping("ShipToAddress2")) & """/>")
        sb.AppendLine("<input type=""hidden"" name=""city"" value=""" & CStrEx(Shipping("ShipToCityName")) & """/>")
        sb.AppendLine("<input type=""hidden"" name=""state"" value=""" & CStrEx(Shipping("ShipToStateName")) & """/>")
        sb.AppendLine("<input type=""hidden"" name=""zip"" value=""" & CStrEx(Shipping("ShipToZipCode")) & """/>")
        sb.AppendLine("<input type=""hidden"" name=""country"" value=""" & CStrEx(Shipping("ShipToCountryGuid")) & """/>")
        sb.AppendLine("<input type=""hidden"" name=""email"" value=""" & CStrEx(Shipping("ShipToEmailAddress")) & """/>")
        sb.AppendLine("</form>")

        Return sb.ToString()
    End Function

    Private Function split(ByVal str As String) As String()
        Try
            Return str.Split(New Char() {CChar(" ")})
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

End Class
