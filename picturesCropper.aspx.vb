﻿'AM2010050401 - PICTURES CROPPER - Start
Imports System.Data
Imports System.Data.OleDb
Imports Radactive.WebControls.ILoad
Imports Radactive.WebControls.ILoad.Configuration
Imports System.IO
Imports System.Drawing
Imports System.Configuration.ConfigurationManager
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT.GlobalsClass
Imports ExpandIT
Imports ExpandIT.ExpandITLib

Partial Public Class picturesCropper
    Inherits ExpandIT.Page

    Public pictureGuid As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack() Then
        If lblError.Text = "" Then
            lblError.Visible = False
        End If
            SQLdsPicturesTypes.DataBind()
            Me.dplPicturesTypes.DataBind()
        configureCropper()
        End If
    End Sub

    Protected Sub configureCropper()

        Dim sourceFolderPath As String = ""
        Dim producedFolderPath As String = ""
        Dim destinationFolderPath As String = ""
        Dim processedFolderPath As String = ""
        Dim prevProcessedFolderPath As String = ""
        Dim pictureWidth As Double = 0
        Dim pictureHeight As Double = 0
        Dim Fname As String
        Dim definition1 As WebImageDefinition
        Dim sql As String = ""
        Dim pictureTypeDict As ExpDictionary
        Dim config As ILoadConfiguration

        config = New ILoadConfiguration()
        pictureGuid = CStrEx(Me.dplPicturesTypes.SelectedValue)

        If pictureGuid = "" Then
            Me.ILoad1.Visible = False
            Me.lblError.Text = "There is not enough information to configure the cropping system."
            Me.lblError.Visible = True
            Exit Sub
        End If

        config.InternalCode = pictureGuid
        sql = "SELECT * FROM ImageProcessor WHERE Crop=1 AND PictureTypeGuid=" & SafeString(pictureGuid)
        pictureTypeDict = Sql2Dictionary(sql)

        If pictureTypeDict Is Nothing Then
            Me.ILoad1.Visible = False
            Me.lblError.Text = "There is not enough information to configure the cropping system."
            Me.lblError.Visible = True
            Exit Sub
        End If

        If CBoolEx(pictureTypeDict("SaveAs")) Then
            Me.pnlNewName.Visible = True
            Me.tbxPictureName.Text = ""
        Else
            Me.pnlNewName.Visible = False
        End If
        pictureWidth = CDblEx(pictureTypeDict("PictureWidth"))
        pictureHeight = CDblEx(pictureTypeDict("PictureHeight"))
        sourceFolderPath = CStrEx(pictureTypeDict("SourcePath"))
        producedFolderPath = CStrEx(pictureTypeDict("ProcessingPath"))
        destinationFolderPath = CStrEx(pictureTypeDict("DestinationPath"))
        processedFolderPath = CStrEx(pictureTypeDict("ProcessedPath"))

        Try
            definition1 = New WebImageDefinition("Default", "Default")
            config.WebImageDefinitions.Add(definition1)

            definition1.SizeConstraint.Mode = WebImageSizeConstraintMode.FixedSize
            definition1.SizeConstraint.FixedSizeData.Size = New System.Drawing.Size(pictureWidth, pictureHeight)
            definition1.OutputOptions.FormatMode = WebImageOutputFormatMode.Custom
            definition1.OutputOptions.Format = New System.Guid("b96b3cae-0728-11d3-9d7b-0000f81ef32e")
            definition1.OutputOptions.JPEGQuality = 92
            definition1.OutputOptions.JPEGForceRecompression = False

            config.TemporaryFolder.Path = producedFolderPath
            config.TemporaryFolder.UrlMode = UrlCalculationMode.Automatic
            config.DestinationFolder.Path = producedFolderPath
            config.DestinationFolder.UrlMode = UrlCalculationMode.Automatic

            config.KeepSourceImage = True
            config.GenerateSelectedImage = True

            Me.ILoad1.Configuration = config

            Me.lblError.Text = ""
            Me.lblError.Visible = False
            Me.ILoad1.Visible = True

        Catch ex As Exception
            lblError.Text = ex.Message.ToString()
            lblError.Visible = True
        End Try

    End Sub

    Protected Sub dplPicturesTypes_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles dplPicturesTypes.DataBound
        If lblError.Text = "" Then
            lblError.Visible = False
        End If
        configureCropper()
    End Sub

    Protected Sub btnAccept_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAccept.Click
        If Not ILoad1.Value Is Nothing Then
            Dim imageId As String = ""
            Dim processingFolder As String = ""
            Dim sql As String = ""
            Dim typeDict As ExpDictionary
            Dim dimensions As String = ""
            Dim destinationPath As String = ""
            Dim originalFileName As String = ""

            If pictureGuid = "" Then
                pictureGuid = CStrEx(Me.dplPicturesTypes.SelectedValue)
            End If
            'Image was processed
            ' Use the original file name as image id
            If pnlNewName.Visible = True And CStrEx(Me.tbxPictureName.Text) <> "" Then
                originalFileName = CStrEx(Me.tbxPictureName.Text)
            Else
            originalFileName = ILoad1.Value.SourceImageClientFileName
            End If
            imageId = CustomStorageManager.GetNewImageId(pictureGuid, originalFileName)

            ' Store files into the file system with the custom file name
            ILoad1.Value.moveToCustomStorageAndChangeId(imageId)

            sql = "SELECT * FROM ImageProcessor WHERE Crop=1 AND PictureTypeGuid=" & SafeString(pictureGuid)
            typeDict = Sql2Dictionary(sql)
            If Not typeDict Is Nothing Then
                processingFolder = CStrEx(typeDict("ProcessingPath"))
                Dim dir As DirectoryInfo = New DirectoryInfo(processingFolder)
                Dim folderfiles As FileInfo() = dir.GetFiles()
                If folderfiles.Length > 0 Then
                    Kill(processingFolder + "\" + "*.*")
                End If
                destinationPath = CStrEx(typeDict("DestinationPath"))
                dimensions = CIntEx(typeDict("PictureWidth")) & "x" & CIntEx(typeDict("PictureHeight"))
                If File.Exists(destinationPath + "\" + imageId + ".jpg") Then
                    sql = "INSERT INTO ImageProcessorLog VALUES (" & SafeString(dimensions) & "," & SafeString(destinationPath) & "," & SafeString(originalFileName) & ",'" & imageId & ".jpg','Cropped',getdate())"
                    excecuteNonQueryDb(sql)
                End If
                Response.Redirect("picturesCropper.aspx")
            End If

        End If
    End Sub

    Protected Sub dplPicturesTypes_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dplPicturesTypes.SelectedIndexChanged
        configureCropper()
    End Sub
	
	Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
End Class
'AM2010050401 - PICTURES CROPPER - End