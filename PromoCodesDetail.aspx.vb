Imports System.Configuration.ConfigurationManager
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT
Imports ExpandIT.ExpandITLib
Imports System.Web

Partial Class PromoCodesDetail
    Inherits ExpandIT.ShippingPayment._CartPage

    Protected groupsPrdFinal As New ExpDictionary

    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	
	Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim promotionCode = CStrEx(Request("promotionCode"))
        Dim promotionGuid = CStrEx(Request("promotionGuid"))
        If promotionCode <> "" And promotionGuid <> "" Then
            Dim sqlPropGrp As String = ""
            Dim groupsProperties As New ExpDictionary

            Dim groupsPrd As New ExpDictionary
            sqlPropGrp = "SELECT PropOwnerRefGuid, PropGuid, PropLangGuid, PropTransText FROM PropVal INNER JOIN PropTrans" & _
                                   " ON PropVal.PropValGuid=PropTrans.PropValGuid" & _
                                   " WHERE PropOwnerTypeGuid='PRM'" & _
                                   " AND (PropLangGuid IS NULL OR PropLangGuid =" & SafeString(HttpContext.Current.Session("LanguageGuid").ToString) & ")" & _
                                   " AND (PropGuid='PROMO_HTML_MAIN' OR PropGuid='PROMO_NAME' OR PropGuid='PROMO_SMALL_IMAGE')" & _
                                   " AND PropOwnerRefGuid=" & SafeString(promotionGuid)
            groupsProperties = SQL2Dicts(sqlPropGrp)
            If Not groupsProperties Is Nothing Then
                If groupsProperties.Count > 0 Then
                    For Each item2 As ExpDictionary In groupsProperties.Values
                        groupsPrd.Add(item2("PropGuid"), item2("PropTransText"))
                    Next
                    groupsPrd.Add("PromotionCode", promotionCode)
                    groupsPrdFinal.Add(promotionGuid, groupsPrd)
                End If
            End If

        End If
    End Sub
End Class
