Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT.GlobalsClass

Partial Class catalog_overview
    Inherits ExpandIT.Page

    Protected dictCatalogOverview As Object
    Protected cachekey, catalogguid, overviewHtml As String
    Protected dictCatalogs As ExpDictionary

    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' ---------------------------------------------------------------
        ' INFORMATION COLLECTION
        ' ---------------------------------------------------------------

        ' Check the page access.
        eis.PageAccessRedirect("Catalog")

        ' Get CatalogGuid if it is present.
        catalogguid = CStrEx(Request("CatalogGuid"))
        'cachekey = "OverViewHtml-" & Session("LanguageGuid").ToString & "-" & catalogguid
        'AM2010071901 - ENTERPRISE CSC - Start
        cachekey = "OverViewHtml-" & Session("LanguageGuid").ToString & "-" & catalogguid & "-" & globals.User("CatalogNo")
        'AM2010071901 - ENTERPRISE CSC - End
        '*****CUSTOMER SPECIFIC CATALOGS***** -START
        'ExpandIT US - USE CSC - Start
        If CBoolEx(AppSettings("EXPANDIT_US_USE_CSC")) Then
            cachekey &= "-" & CStrEx(globals.User("CustomerGuid"))
        End If
        'ExpandIT US - USE CSC - End
        '*****CUSTOMER SPECIFIC CATALOGS***** -END

        dictCatalogOverview = CacheGet(cachekey)
        If dictCatalogOverview Is Nothing Then
            dictCatalogs = eis.CatLoadCatalogs()

            ' Render html
            overviewHtml = GetOverviewHtml(catalogguid)

            ' Cache the result
            dictCatalogOverview = New ExpDictionary()
            dictCatalogOverview("overviewHtml") = overviewHtml
            dictCatalogOverview("dictCatalogs") = dictCatalogs
            dictCatalogOverview("CatalogGuid") = catalogguid
            CacheSetAggregated(cachekey, dictCatalogOverview, Split(AppSettings("GroupProductRelatedTablesForCaching") & "|GroupProduct", "|"))
        Else
            ' Get the result from the cached values
            overviewHtml = dictCatalogOverview("overviewHtml")
            dictCatalogs = dictCatalogOverview("dictCatalogs")
            catalogguid = dictCatalogOverview("CatalogGuid")
        End If

        litOverview.Text = overviewHtml
    End Sub

    Function GetOverviewGroups(ByVal LanguageGuid As String) As ExpDictionary
        Dim group As ExpDictionary
        Dim groups As ExpDictionary
        Dim parentguid As Integer
        Dim cachekey As String
        Dim sql As String
        Dim info As ExpDictionary

        'AM2010071901 - ENTERPRISE CSC - Start
        cachekey = "OverViewGroups-" & LanguageGuid & "-" & globals.User("CatalogNo")
        'AM2010071901 - ENTERPRISE CSC - End
        '*****CUSTOMER SPECIFIC CATALOGS***** -START
        'ExpandIT US - USE CSC - Start
        If CBoolEx(AppSettings("EXPANDIT_US_USE_CSC")) Then
            cachekey &= "-" & CStrEx(globals.User("CustomerGuid"))
        End If
        'cachekey = "OverViewGroups-" & LanguageGuid
        'ExpandIT US - USE CSC - End
        '*****CUSTOMER SPECIFIC CATALOGS***** -END

        groups = CacheGet(cachekey)
        If groups Is Nothing Then
            ' Load Catalog
            'AM2010071901 - ENTERPRISE CSC - Start
            sql = "SELECT * FROM GroupTable WHERE CatalogNo=" & SafeString(globals.User("CatalogNo"))
            'sql = "SELECT * FROM GroupTable  ORDER BY ParentGuid, SortIndex"
            
            'JA2010030901 - PERFORMANCE -Start
            'ExpandIT US - USE CSC - Start
            If CBoolEx(AppSettings("EXPANDIT_US_USE_CSC")) Then
                Dim objCSC As New csc(globals)
                'AM2010071901 - ENTERPRISE CSC - Start
                sql = objCSC.groupsWithProductsSQL("SELECT DISTINCT ParentGuid FROM GroupTable WHERE CatalogNo=" & SafeString(globals.User("CatalogNo")))
                'AM2010071901 - ENTERPRISE CSC - End
            End If
            'ExpandIT US - USE CSC - End
            'JA2010030901 - PERFORMANCE -End
            groups = Sql2Dictionaries(sql, New String() {"GroupGuid"})

            ' Load Group properties
            info = eis.CatGroups2Info(groups)
            info("GroupInfo")("PropertyArray") = New String() {"NAME"}
            eis.CatLoadGroups(info)

            ' Prepare each group to contain sub groups
            For Each group In groups.Values
                group("SubGroups") = New ExpDictionary
            Next

            ' Associate properties with groups and rearrange the groups into sub groups
            For Each group In groups.Values
                ' Set the position in the tree
                parentguid = group("ParentGuid")
                If parentguid <> 0 Then
                    If groups.Exists(SafeString(parentguid)) Then
                        groups(SafeString(parentguid))("SubGroups")(SafeString(group("GroupGuid"))) = group
                    End If
                End If
            Next

            ' Remove unused values
            Dim grouparray(groups.Count - 1) As Object
            groups.Values.CopyTo(grouparray, 0)
            For Each group In grouparray
                parentguid = group("ParentGuid")
                group.Remove("SortIndex")
                group.Remove("ParentGuid")
                If parentguid <> 0 Then groups.Remove(SafeString(group("GroupGuid")))
            Next

            ' Cache the result
            CacheSet(cachekey, groups, "GroupTable")
        End If

        Return groups
    End Function

    Function GetOverviewHtml(ByVal catalogguid As String) As String
        Dim groups As ExpDictionary
        Dim groupcount As Integer
        Dim blocks As ExpDictionary
        Dim blockkey, blockhtml, html As String
        Dim displaycount As Integer
        Dim cataloggroup As ExpDictionary
        Dim maingroup As ExpDictionary
        Dim blockcount As Integer
        Dim block As ExpDictionary

        groups = GetOverviewGroups(Session("LanguageGuid").ToString)

        ' Make sure that the catalog guid is valid
        If catalogguid = "" Or Not groups.Exists(SafeString(catalogguid)) Then
            Try
                catalogguid = GetFirst(groups).Value("GroupGuid")
            Catch ex As Exception
                catalogguid = ""
            End Try
        End If

        ' Bail out if no catalogs exists
        If CStrEx(catalogguid) = "" Then
            GetOverviewHtml = Resources.Language.MESSAGE_NO_CATALOG_AVAILABLE
            Exit Function
        End If

        ' Get the selected catalog
        cataloggroup = groups(SafeString(catalogguid))

        ' Create the html blocks
        groupcount = 0
        blocks = New ExpDictionary
        For Each maingroup In cataloggroup("SubGroups").values
            blockcount = 0
            blockkey = CStr(blocks.Count + 1)
            blockhtml = "<ul><li style=""font-size: 11px;"">" & RenderBlock(maingroup, blockcount) & "</li></ul>" & vbCrLf
            block = New ExpDictionary()
            block("Size") = blockcount
            block("Html") = blockhtml
            blocks.Add(blockkey, block)
            groupcount = groupcount + blockcount
        Next

        html = "<table class=""CatalogTreeOverview"" border=""0""><tr><td valign=""top"" >" & vbCrLf
        displaycount = 0
        If blocks.Count > 0 Then
            For Each block In blocks.Values
                If displaycount + block("Size") / 2 >= (groupcount / 3) Then
                    html = html & "</td><td>&nbsp;&nbsp;&nbsp;</td><td valign=""top"">" & vbCrLf
                    displaycount = 0
                End If
                displaycount = displaycount + block("Size")
                html = html & block("Html")
            Next
        Else
            html = html & Resources.Language.MESSAGE_NO_CATALOG_AVAILABLE
        End If
        html = html & "</td></tr></table>" & vbCrLf

        Return html
    End Function

    Function RenderBlock(ByVal group As ExpDictionary, ByRef blockcount As Integer) As String
        Dim retv As String
        Dim subgroup As ExpDictionary
        Dim name As String

        name = HTMLEncode(group("NAME"))
        If name = "" Then name = "<font color=""red"">[" & Resources.Language.MESSAGE_MISSING_PROPERTY & "]</font>"

        retv = "<a href=""" & GroupLink(group("GroupGuid")) & """>" & name & "</a>"
        If group("SubGroups").Count > 0 Then
            retv = retv & "<ul>"
            For Each subgroup In group("SubGroups").values
                blockcount = blockcount + 1
                retv = retv & "<li>" & RenderBlock(subgroup, blockcount) & "</li>" & vbCrLf
            Next
            retv = retv & "</ul>" & vbCrLf
        End If
        RenderBlock = retv
    End Function

End Class
