<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="user_logout.aspx.vb" Inherits="user_logout"
    CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master" 
    RuntimeMasterPageFile="ThreeColumn.master" CrumbName="<%$ Resources: Language, LABEL_MENU_LOGOUT %>" %>

<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <div class="UserLogoutPage">
    <uc1:PageHeader ID="PageHeader1" runat="server" text="<%$ Resources: Language, LABEL_MENU_LOGOUT %>" EnableTheming="true" />
    <p>
        <asp:Literal ID="Literal1" runat="server" text="<%$ Resources: Language, LABEL_LOGOUT_COMPLETE %>"></asp:Literal>
    </p>
    </div>
</asp:Content>
