Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT.GlobalsClass
Imports ExpandIT.SectionSettingsManager
Imports ExpandIT.ShippingPayment
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic

Partial Class shipping
    Inherits ExpandIT.ShippingPayment.ShippingPaymentPage
    'Inherits ShippingMethodPage

    Delegate Function DelSetPtr(ByVal args As WebControls.DataListCommandEventArgs) As String
    Protected bDisplayShippingAddress, bDisplayShippingHandling As Boolean
    Protected ucx As ExpandIT.ShippingDesignModel
    Protected B2BUser, B2CUser, AnonymousUser As Boolean
    ' Per ExpandIT Case#ISR-897859 BEGIN
    Protected designModel As Integer = 1
    ' Per ExpandIT Case#ISR-897859 END

    ' Array with virtual paths
    Protected designModelPath() As String = _
        { _
             "~/controls/shipping/designmodels/designModel_1.ascx", _
             "~/controls/shipping/designmodels/DesignModel_2.ascx", _
             "~/controls/shipping/designmodels/DesignModel_3_List.ascx", _
             "~/controls/shipping/designmodels/DesignModelB2B1.ascx", _
             "~/controls/shipping/designmodels/DesignModelB2B1List.ascx", _
             "~/controls/shipping/designmodels/DesignModelB2B3.ascx", _
             "~/controls/shipping/designmodels/DesignModelB2B3List.ascx", _
             "~/controls/shipping/designmodels/DesignModel_2.ascx", _
             "~/controls/shipping/designmodels/DesignModel_3_List.ascx", _
             "~/controls/shipping/designmodels/ShippingAnonymous.ascx", _
             "~/controls/shipping/designmodels/B2C1OnlyOne.ascx" _
        }

    ''' <summary>
    ''' Maps Configuration settings for B2C Customers with corresponding DesignModels
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function configMappingB2C() As Integer

        Try

            If Not CBoolEx(AppSettings("B2C_MULTIPLE_ADDRESS")) Then
                Return 1
            End If

            If Not CBoolEx(AppSettings("B2C_DISPLAY_LIST")) Then
                If CIntEx(ExpandITLib.getSingleValueDB("SELECT COUNT(ShippingAddressGuid) FROM ShippingAddress WHERE UserGuid = " & SafeString(Session("UserGuid")))) > 1 Then
                    Return 2
                Else
                    Return 11
                End If
            Else
                Return 3
            End If

        Catch ex As Exception
            Return 0
        End Try

    End Function

    ''' <summary>
    ''' Maps Configuration settings for B2B Customers with corresponding DesignModels
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function configMappingB2B() As Integer
        Dim isMulti, isOnlyB2B, isOnlyB2C, isList As Boolean
        Dim listMode As Integer = 0

        Try

            isMulti = CBoolEx(AppSettings("B2B_MULTIPLE_ADDRESS"))
            isOnlyB2B = CBoolEx(AppSettings("B2B_USE_B2B_ADDRESS_ONLY"))
            isOnlyB2C = CBoolEx(AppSettings("B2B_USE_B2C_ADDRESS_ONLY"))
            isList = CBoolEx(AppSettings("B2B_DISPLAY_LIST"))

            If isList Then listMode = 1

            If isOnlyB2B And isOnlyB2C Then
                isOnlyB2B = False
                isOnlyB2C = True
            End If

            If isOnlyB2C And Not isMulti Then
                Return 1
            ElseIf isOnlyB2C And isMulti Then
                Return 2 + listMode
            End If

            If isOnlyB2B Then
                Return 4 + listMode
            End If

            If Not isMulti Then
                Return 6 + listMode
            Else
                Return 8 + listMode
            End If

        Catch ex As Exception
            Return 0
        End Try

    End Function

    ''' <summary>
    ''' Checks user status for current user
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub determineUserStatus()
        If Not CBoolEx(globals.User("Anonymous")) Then
            If CBoolEx(globals.User("B2B")) Then
                B2BUser = True
            Else
                B2CUser = True
            End If
        Else
            AnonymousUser = True
        End If
    End Sub

    ''' <summary>
    ''' Handles Page Load Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	
	Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        ' Changed to make sure nothinhg gets written if cart is empty
        globals.OrderDict = eis.LoadOrderDictionary(globals.User)

        ' Check if cart is empty. if so redirect to cart.aspx
        If globals.IsCartEmpty Then
            Response.Redirect("cart.aspx")
        End If

        ' Check User Status
        determineUserStatus()

        ' Check the page access.
        eis.PageAccessRedirect("Order")

        ' added this to use web.config file settings "IGNORE_LOCALMODE"

        ' Check for local mode
        If IsLocalMode And Not CBoolEx(AppSettings("IGNORE_LOCALMODE")) Then
            Response.Redirect("localmode.aspx", True)
        End If

        ' Determine if ShippingAddress is to be displayed 
        Select Case True
            Case B2BUser And Not CBoolEx(AppSettings("B2B_USE_SHIPPING_ADDRESS"))
                bDisplayShippingAddress = False
            Case B2CUser And Not CBoolEx(AppSettings("USE_SHIPPING_ADDRESS"))
                bDisplayShippingAddress = False
            Case AnonymousUser And Not eis.CheckPageAccess("Order")
                Response.Redirect("~/user_login.aspx?returl=" & Request.RawUrl, True)
            Case Else
                ' Always use shipping address
                bDisplayShippingAddress = True
        End Select

        If Not bDisplayShippingAddress Then 'Dangerous!!
            eis.CalculateOrder()
            Response.Redirect("payment.aspx", True)
        End If

        '' Make sure an address exists in ShippingAddress Table
        'If ExpandITLib.getSingleValueDB("SELECT TOP 1 ShippingAddressGuid FROM ShippingAddress WHERE UserGuid = " & SafeString(Session("UserGuid"))) Is Nothing Then
        '' If there's no address in ShippingAddress table make one with data from UserTable
        'MakeNewEntry(makeStandardShippingDict())
        'End If

        'Make sure the user's address exists in ShippingAddress Table
        Dim sql As String
        sql = "SELECT TOP 1 ShippingAddressGuid FROM ShippingAddress WHERE UserGuid = " & SafeString(Session("UserGuid"))
        If CStrEx(globals.User("Address1")) = "" Then
            sql &= " AND (Address1='' OR Address1 IS NULL)"
        Else
            sql &= " AND Address1=" & SafeString(globals.User("Address1"))
        End If
        If CStrEx(globals.User("Address2")) = "" Then
            sql &= " AND (Address2='' OR Address2 IS NULL)"
        Else
            sql &= " AND Address2=" & SafeString(globals.User("Address2"))
        End If
        If CStrEx(globals.User("CityName")) = "" Then
            sql &= " AND (CityName='' OR CityName IS NULL)"
        Else
            sql &= " AND CityName=" & SafeString(globals.User("CityName"))
        End If
        If CStrEx(globals.User("ContactName")) = "" Then
            sql &= " AND (ContactName='' OR ContactName IS NULL)"
        Else
            sql &= " AND ContactName=" & SafeString(globals.User("ContactName"))
        End If

        '--DJW 8/10/2012 Eliminated Country From Being Displayed
        'If CStrEx(globals.User("CountryGuid")) = "" Then
        '    sql &= " AND (CountryGuid='' OR CountryGuid IS NULL)"
        'Else
        '    sql &= " AND CountryGuid=" & SafeString(globals.User("CountryGuid"))
        'End If

        If CStrEx(globals.User("EmailAddress")) = "" Then
            sql &= " AND (EmailAddress='' OR EmailAddress IS NULL)"
        Else
            sql &= " AND EmailAddress=" & SafeString(globals.User("EmailAddress"))
        End If
        If CStrEx(globals.User("StateName")) = "" Then
            sql &= " AND (StateName='' OR StateName IS NULL)"
        Else
            sql &= " AND StateName=" & SafeString(globals.User("StateName"))
        End If
        If CStrEx(globals.User("ZipCode")) = "" Then
            sql &= " AND (ZipCode='' OR ZipCode IS NULL)"
        Else
            sql &= " AND ZipCode=" & SafeString(globals.User("ZipCode"))
        End If
        
        If ExpandITLib.getSingleValueDB(sql) Is Nothing Then
            ' If the user's address doesn't exist in ShippingAddress table, add it
            MakeNewEntry(makeStandardShippingDict())
        End If

        ' Per ExpandIT Case#ISR-897859 BEGIN
        ' Dim designModel As Integer = 1
        ' Per ExpandIT Case#ISR-897859 END

        If B2BUser Then
            designModel = configMappingB2B()
        ElseIf B2CUser Then
            designModel = configMappingB2C()
        Else
            designModel = 10
        End If

        ucx = Page.LoadControl(designModelPath(designModel - 1))

        If designModel = 10 Then
            Dim al As ArrayList = AllPossibleShippingProviders()
            If al.Count = 1 AndAlso al.Contains("NONE") Then
                ucx.IsMultiplePaymentShippingOption = "none"
                Me.CrumbName = Resources.Language.LABEL_PAYMENT_ADDRESS
            End If
            ' Added this to make sure only payment address input field is shown when anonymous user
            ' chooses to pick up the merchandise
            If String.Equals(CustomerShippingProvider, "NONE") Then
                ucx.IsMultiplePaymentShippingOption = "none"
                Me.CrumbName = Resources.Language.LABEL_PAYMENT_ADDRESS
            End If
        End If

        Dim delShpAddr As New DelSetPtr(AddressOf Delete_Click_Event)
        Dim testd As ExpandIT.ShippingUserControl
        If designModel = 2 Or designModel = 8 Then
            testd = CType(ucx, DesignModel_2).MySelf.Controls(1)
            testd.SetPtr = delShpAddr
        ElseIf designModel = 11 Then
            testd = CType(ucx, B2C1OnlyOne).MySelf.Controls(1)
            testd.SetPtr = delShpAddr
        End If

        Dim dl As DataList = ucx.GetDataList()
        '<!-- ' ExpandIT US - AM - US Sales Tax - Start -->
        If dl IsNot Nothing Then
            If designModel = 1 Then
                If AppSettings("EXPANDIT_US_USE_US_SALES_TAX") Then
                    dl.DataSource = New ShippingB2CTaxTableAdapters.ShippingAddressTableAdapter().GetDataByIsDefault(Session("UserGuid"))
                Else
                    dl.DataSource = New ShippingB2CTableAdapters.ShippingAddressTableAdapter().GetDataByIsDefault(Session("UserGuid"))
                End If
            ElseIf designModel < 4 Then
                If AppSettings("EXPANDIT_US_USE_US_SALES_TAX") Then
                    dl.DataSource = New ShippingB2CTaxTableAdapters.ShippingAddressTableAdapter().GetData(Session("UserGuid"))
                Else
                    dl.DataSource = New ShippingB2CTableAdapters.ShippingAddressTableAdapter().GetData(Session("UserGuid"))
                End If
            ElseIf designModel < 6 Then
                If AppSettings("EXPANDIT_US_USE_US_SALES_TAX") Then
                    dl.DataSource = New ShippingB2BTaxTableAdapters.DataTable1TableAdapter().GetData(Session("UserGuid"))
                Else
                    dl.DataSource = New ShippingB2BTableAdapters.DataTable1TableAdapter().GetData(Session("UserGuid"))
                End If
            ElseIf designModel = 6 Then
                Dim dl2 As DataList = ucx.GetDataList2()
                If AppSettings("EXPANDIT_US_USE_US_SALES_TAX") Then
                    dl2.DataSource = New ShippingB2CTaxTableAdapters.ShippingAddressTableAdapter().GetDataByIsDefault(Session("UserGuid"))
                Else
                    dl2.DataSource = New ShippingB2CTableAdapters.ShippingAddressTableAdapter().GetDataByIsDefault(Session("UserGuid"))
                End If
                If AppSettings("EXPANDIT_US_USE_US_SALES_TAX") Then
                    dl.DataSource = New ShippingB2BTaxTableAdapters.DataTable1TableAdapter().GetData(Session("UserGuid"))
                Else
                    dl.DataSource = New ShippingB2BTableAdapters.DataTable1TableAdapter().GetData(Session("UserGuid"))
                End If
                dl2.DataBind()
            ElseIf designModel = 7 Then
                If AppSettings("EXPANDIT_US_USE_US_SALES_TAX") Then
                    dl.DataSource = New ShippingB2BB2CTaxTableAdapters.ShippingAddressTableAdapter().GetDataByIsDefault(Session("UserGuid"))
                Else
                    dl.DataSource = New ShippingB2BB2CTableAdapters.ShippingAddressTableAdapter().GetDataByIsDefault(Session("UserGuid"))
                End If
            ElseIf designModel < 10 Then
                If AppSettings("EXPANDIT_US_USE_US_SALES_TAX") Then
                    dl.DataSource = New ShippingB2BB2CTaxTableAdapters.ShippingAddressTableAdapter().GetData(Session("UserGuid"))
                Else
                    dl.DataSource = New ShippingB2BB2CTableAdapters.ShippingAddressTableAdapter().GetData(Session("UserGuid"))
                End If
            ElseIf designModel = 11 Then
                If AppSettings("EXPANDIT_US_USE_US_SALES_TAX") Then
                    dl.DataSource = New ShippingB2CTaxTableAdapters.ShippingAddressTableAdapter().GetData(Session("UserGuid"))
                Else
                    dl.DataSource = New ShippingB2CTableAdapters.ShippingAddressTableAdapter().GetData(Session("UserGuid"))
                End If
            End If

            dl.DataBind()
        End If
        '<!-- ' ExpandIT US - AM - US Sales Tax - End -->

        placeholder1.Controls.Add(ucx)

        'WLB.10032012.BEGIN
        Dim c As New Control()
        Dim jScriptSourceEmailID As String
        Dim jScriptDestinationEmailID As String
        Dim jScriptSourceAddressID As String
        Dim jScriptDestinationAddressID As String
        jScriptSourceEmailID = ucx.findNamedControl(placeholder1, "ssiMakeEmailPermanentVisible", c).ClientID
        jScriptDestinationEmailID = ucx.findNamedControl(placeholder1, "ssiMakeEmailPermanent", c).ClientID
        jScriptSourceAddressID = ucx.findNamedControl(placeholder1, "ssiMakeAddressPermanentVisible", c).ClientID
        jScriptDestinationAddressID = ucx.findNamedControl(placeholder1, "ssiMakeAddressPermanent", c).ClientID
        'WLB.10032012.END

        If AnonymousUser Then
            Me.Purchase.Attributes.Item("onClick") = "copy();"
        End If

        If B2BUser Then
            If Not CBoolEx(AppSettings("B2B_DISPLAY_LIST")) Then
                'WLB.
                Me.Purchase.Attributes.Item("onClick") = "copy(getSelectedRadioButton());ssiCopyCustomFields(""" & jScriptSourceEmailID & """, """ & jScriptDestinationEmailID & """);ssiCopyCustomFields(""" & jScriptSourceAddressID & """, """ & jScriptDestinationAddressID & """);unhideErrorMessage('errorMessageDiv');"
            Else
                Me.Purchase.Attributes.Item("onClick") = "unhideErrorMessage('errorMessageDiv');"
            End If
        End If

        If B2CUser Then
            If Not CBoolEx(AppSettings("B2C_DISPLAY_LIST")) Then
                Me.Purchase.Attributes.Item("onClick") = "copy(getSelectedRadioButton())"
            End If
        End If

        Me.Purchase.Text = Server.HtmlDecode(Resources.Language.ACTION_NEXT_GREATER_THAN)

        'AM2010080301 - CHECK OUT PROCESS MAP - Start
        If CBoolEx(AppSettings("EXPANDIT_US_USE_CHECK_OUT_PROCESS_MAP")) And Not CheckOutPaths() Is Nothing Then
            If CheckOutPaths.Length > 0 Then
                Master.CheckOutProcess.CheckOutProcessDict = CheckOutPaths()
                Master.CheckOutProcess.currentLocation = getCurrentLocation()
            End If
        End If
        'AM2010080301 - CHECK OUT PROCESS MAP - End

    End Sub

    ''' <summary>
    ''' Make a Shipping Dictionary that will update ShippingAddress Table
    ''' in case no data is found in the Table    
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function makeStandardShippingDict(Optional ByVal refDict As ExpDictionary = Nothing) As ExpDictionary

        Dim tmpShipping As New ExpDictionary

        For Each key As KeyValuePair(Of Object, Object) In globals.User
            If refDict IsNot Nothing Then
                If refDict.ContainsKey(key.Key) Then
                    tmpShipping.Add(key.Key, globals.User.Item(key.Key))
                End If
            Else
                tmpShipping = globals.User
                Exit For
            End If
        Next
        tmpShipping.Add("ShippingAddressGuid", GenGuid())
        tmpShipping.Add("IsDefault", True)

        Return tmpShipping

    End Function

    ''' <summary>
    ''' Handles Delete event
    ''' </summary>
    ''' <param name="args"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Delete_Click_Event(ByVal args As WebControls.DataListCommandEventArgs) As String
        If args.CommandName = "delete" Then
            Dim ShipSend As New ExpDictionary
            ShipSend = ucx.shipAddrData() 'Get data from input field
            excecuteNonQueryDb("Delete From ShippingAddress Where ShippingAddressGuid=" & SafeString(ShipSend("ShippingAddressGuid")))
            HttpContext.Current.Response.Redirect("Shipping.aspx", True)
        End If
        Return Nothing
    End Function

    ''' <summary>
    ''' Handles Click event on button Purchase
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Purchase_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Purchase.Click

        Dim ShipSend, PaymentSend As New ExpDictionary

        If Not globals.IsCartEmpty Then

            ''''
            Dim IsInputValid As Boolean = True
            ' Per ExpandIT Case#ISR-897859 BEGIN
            If designModel <> 6 Then
                Dim summary As ValidationSummary = Me.validationSummary1
                Dim validators As ValidatorCollection = summary.Page.GetValidators(summary.ValidationGroup)
                For i As Integer = 0 To validators.Count - 1
                    Dim validator As IValidator = CType(validators(i), IValidator)
                    If Not validator.IsValid Then
                        IsInputValid = False
                        Exit For
                    End If
                Next
            End If
            ' Per ExpandIT Case#ISR-897859 END
            ''''
            If IsInputValid Then

                ShipSend = ucx.shipAddrData()

                If ShipSend("UserGuid") = "" Then ShipSend("UserGuid") = Session("UserGuid")
                ' process data insert update etc.
                If Left(ShipSend("AddressType"), 3) = "B2C" Or ShipSend("AddressType") = "" Then
                    If Not AnonymousUser Then
                        processData(ShipSend)
                    End If
                End If
                CartShippingAddress = ShipSend
                If AnonymousUser Then
                    PaymentSend = CType(ucx, controls_shipping_designmodels_ShippingAnonymous).shipPaymentData
                    If PaymentSend("ContactName") <> "" And PaymentSend("EmailAddress") <> "" Then
                        CartPaymentAddress = CType(ucx, controls_shipping_designmodels_ShippingAnonymous).shipPaymentData
                    End If
                Else
                    ' Set payment address to registered users address - or comment out the next line                
                    CartPaymentAddress = makeStandardShippingDict(ShipSend)
                End If
                UpdateOrderDict()
                Response.Clear()
                Server.ClearError()

                Response.Redirect(NextUrl)
            End If
        Else
            Response.Redirect("cart.aspx")
        End If

    End Sub

    ''' <summary>
    ''' Updates Shipping information on the Cart
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub UpdateOrderDict()

        globals.OrderDict = eis.LoadOrderDictionary(globals.User)
        globals.OrderDict("IsCalculated") = False
        eis.SaveOrderDictionary(globals.OrderDict)

    End Sub

    ''' <summary>
    ''' Adds a shipping address to the database
    ''' </summary>
    ''' <param name="localShipping"></param>
    ''' <remarks></remarks>
    Protected Sub MakeNewEntry(ByRef localShipping As ExpDictionary)

        setShippingAddressGuid(localShipping)
        SetDictUpdateEx(localShipping, "ShippingAddress")
        executeUpdateQuery(localShipping)

    End Sub

    ''' <summary>
    ''' Makes a ShippingAddressGuid and adds it to the Dictionary
    ''' </summary>
    ''' <param name="Shp"></param>
    ''' <remarks></remarks>
    Protected Sub setShippingAddressGuid(ByRef Shp As ExpDictionary)
        If Shp("ShippingAddressGuid") = "" Then
            Shp("ShippingAddressGuid") = GenGuid()
        End If
        If Not Shp.Exists("IsDefault") Then
            Shp.Add("IsDefault", True)
        End If
    End Sub

    ''' <summary>
    ''' Defines what action should be taken with the input
    ''' </summary>
    ''' <param name="contextShipping"></param>
    ''' <remarks></remarks>
    Protected Sub processData(ByRef contextShipping As ExpDictionary)

        Dim dbShipping As New ExpDictionary
        Dim dt As DataTable = SQL2DataTable("SELECT * FROM ShippingAddress WHERE UserGuid = " _
            & SafeString(contextShipping("UserGuid")))

        For Each row As DataRow In dt.Rows
            dbShipping.Add(row("ShippingAddressGuid"), SetDictionary(row))
        Next

        Dim dictCount As Integer = dbShipping.Keys.Count
        Dim mismatched As Integer = 0
        Dim change As Boolean
        For Each dbSKV As KeyValuePair(Of Object, Object) In dbShipping
            For Each csKV As KeyValuePair(Of Object, Object) In contextShipping
                If Not contextShipping("ShippingAddressGuid") = dbShipping(dbSKV.Key)("ShippingAddressGuid") Then
                    mismatched += 1
                    Exit For
                Else
                    Try
                        If IsDBNull(dbShipping(dbSKV.Key)(csKV.Key)) Then
                            dbShipping(dbSKV.Key)(csKV.Key) = ""
                        End If
                        If Not contextShipping(csKV.Key) = dbShipping(dbSKV.Key)(csKV.Key) Then
                            change = True
                            Exit For
                        End If
                    Catch ex As Exception

                    End Try
                End If
            Next
            If change Then Exit For
        Next
        If mismatched = dictCount Then ''newAddress
            MakeNewEntry(contextShipping)
        ElseIf change Then ''changeAddress
            ChangeTableValues(contextShipping)
        Else ''Existing Address
            executeUpdateQuery(contextShipping)
        End If
    End Sub

    ''' <summary>
    ''' Makes changes to the database
    ''' </summary>
    ''' <param name="locShipping"></param>
    ''' <remarks></remarks>
    Protected Sub ChangeTableValues(ByVal locShipping As ExpDictionary)

        Dim sql As New String("SELECT * FROM ShippingAddress WHERE UserGuid= " & SafeString(locShipping("UserGuid")) & " AND ShippingAddressGuid= " & SafeString(locShipping("ShippingAddressGuid")))
        SetDictUpdateEx(locShipping, "ShippingAddress", sql)
        executeUpdateQuery(locShipping)

    End Sub

    ''' <summary>
    ''' Executes the Update Queries
    ''' </summary>
    ''' <param name="localShipping"></param>
    ''' <remarks></remarks>
    Protected Sub executeUpdateQuery(ByVal localShipping As ExpDictionary)
        excecuteNonQueryDb("Update ShippingAddress Set IsDefault = 0 Where UserGuid=" & SafeString(Session("UserGuid")))
        excecuteNonQueryDb("Update ShippingAddress Set IsDefault = 1 Where ShippingAddressGuid=" & SafeString(localShipping("ShippingAddressGuid")))
    End Sub

    ''' <summary>
    ''' Adds a new shipping address
    ''' </summary>
    ''' <param name="dict"></param>
    ''' <param name="tableName"></param>
    ''' <remarks></remarks>
    Private Sub AddShippingAddress(ByVal dict As ExpDictionary, ByVal tableName As String)
        SetDictUpdateEx(dict, tableName)
    End Sub

End Class