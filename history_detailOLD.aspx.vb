Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT.GlobalsClass
Imports System.Web

Partial Class history_detail
    Inherits ExpandIT.ShippingPayment.ShippingPaymentPage
    'Inherits ExpandIT.ShippingPayment.ShippingMethodPage

    Protected CartLine As ExpDictionary
    Protected ColSpanNumber As Integer
    Protected strID As String
    Protected dictSHProvider As ExpDictionary
    Protected OrderAvailable As Boolean
    Protected UsingVariants As Boolean
    Protected OrderDict As ExpDictionary
    Protected IsCommentShown As Boolean
    Protected IsPoNumberShown As Boolean
    Protected changeCurrentCustomer As Boolean = False
    'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
    Protected csrObj As USCSR
    'AM2011031801 - ENTERPRISE CSR STAND ALONE - End

    '<!-- JA2011042101 - EEPG USE EXISTING CREDIT CARDS - Start -->
    Protected objEEPG As New EEPGCreditCards()
    '<!-- JA2011042101 - EEPG USE EXISTING CREDIT CARDS - End -->


    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	
    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        IsCommentShown = CBoolEx(AppSettings("CART_COMMENT"))
        IsPoNumberShown = CBoolEx(AppSettings("CART_PONUMBER"))
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
        csrObj = New USCSR(globals)
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
        'AM2010091001 - ONLINE PAYMENTS - Start
        If CBoolEx(AppSettings("EXPANDIT_US_USE_EEPG_ONLINE_PAYMENTS")) Then
            If pnlOnlinePayments.Visible Then
                Dim eh As EventHandler = New EventHandler(AddressOf BtnAccept_Click)
                eepgform.clickEvent = eh
                eepgform.cbxChangedEvent = New EventHandler(AddressOf cbxChanged)
            End If
            If CStrEx(Request("OnlinePayments")) <> "" Then
                Me.btnOnlinePayment.Visible = True
            End If
        End If
        'AM2010091001 - ONLINE PAYMENTS - End
        If CStrEx(Request("DrillDownType")) <> "" And CStrEx(Request("DrillDownGuid")) <> "" Then
            'It's a Ledger Entry
            Me.CartRender1.DrillDownType = CIntEx(Request("DrillDownType"))
            Me.CartRender1.DrillDownGuid = CStrEx(Request("DrillDownGuid"))
            Me.CartRender1.IsLedger = True
            Me.CartRender1.IsBrowser = False
            Me.btnCancelRecurrency.Visible = False
            Me.btnModifyRecurrency.Visible = False
            Me.btnReorder.Visible = False
            Me.btnCancel.Visible = False
            Me.btnReturn.Visible = False
        ElseIf CStrEx(Request("HeaderGuid")) <> "" Then
            'It's a Previous Order
            Me.CartRender1.HeaderGuid = CStrEx(Request("HeaderGuid"))
            Me.CartRender1.IsBrowser = True
            Me.CartRender1.IsLedger = False
            If csrObj.getOrderStatusFromHeader(Request("HeaderGuid")) = "Returned" Then
                'Me.btnCancelRecurrency.Visible = False
                'Me.btnModifyRecurrency.Visible = False
                Me.btnCancel.Visible = False
                Me.btnReturn.Visible = False
            ElseIf csrObj.getOrderStatusFromHeader(Request("HeaderGuid")) = Resources.Language.LABEL_CSR_STATUS_PARTIALLY_SHIPPED Or csrObj.getOrderStatusFromHeader(Request("HeaderGuid")) = Resources.Language.LABEL_CSR_STATUS_SHIPPED Then
                Me.btnCancel.Visible = False
            End If
        End If
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End

        'AM2010092201 - ENTERPRISE CSR - Start
        If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And csrObj.getSalesPersonGuid() <> "" Then
            'Check if the order is for a CSR user
            Dim orderUserGuid As String = ""
            orderUserGuid = getSingleValueDB("SELECT UserGuid FROM ShopSalesHeader WHERE (HeaderGuid =" & SafeString(CStrEx(Request("HeaderGuid"))) & ")")
            If CStrEx(orderUserGuid) <> CStrEx(globals.User("UserGuid")) Then
                Me.btnModifyRecurrency.Visible = False
                Me.btnCancelRecurrency.Visible = False
                Me.btnOnlinePayment.Visible = False
                Me.btnPrint.Visible = False
                Me.btnReorder.Visible = False
                Me.btnReturn.Visible = False
                Me.btnCancel.Visible = False
                changeCurrentCustomer = True
                If CStrEx(Request("CancelationTrack")) = "" Then
                    Dim redirectPage As String = "history_detail.aspx?HeaderGuid=" & CStrEx(Request("HeaderGuid")) & "&CustRef=" & CStrEx(Request("CustRef"))
                    Me.Message.Text = Resources.Language.LABEL_CSR_CHANGE_USER1 & " " & "<a href=""" & VRoot & "/userSearchSelection.aspx?OrderUserGuid=" & orderUserGuid & "&returl=" & Server.UrlEncode(redirectPage) & """>" & Resources.Language.LABEL_CSR_CHANGE_USER2 & "</a>"
                    Me.Message.Visible = True
                End If
            End If
        End If

        If CStrEx(Request("justReturn")) <> "" And CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
            Me.btnModifyRecurrency.Visible = False
            Me.btnCancelRecurrency.Visible = False
            Me.btnOnlinePayment.Visible = False
            Me.btnPrint.Visible = False
            Me.btnReorder.Visible = False
            Me.btnReturn.Visible = False
            Me.btnCancel.Visible = False
            Me.CartRender1.isReturn = True
        End If
        'AM2010092201 - ENTERPRISE CSR - End


    End Sub

    Protected Overloads Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        Dim dictRequest As ExpDictionary
        Dim User2 As ExpDictionary
        Dim dictOrderCopy As ExpDictionary
        Dim elm As String
        Dim LineGuid As String
        Dim OrderLine As ExpDictionary
        Dim bUseSecondaryCurrency As Boolean
        Dim sql As String

        objEEPG = New EEPGCreditCards(globals)

        ' Check the page access.
        eis.PageAccessRedirect("Order")
        globals.Context = eis.LoadContext(globals.User)

        ' Fetch parameters
        dictRequest = RequestGetDict()
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
        ' Load order
        If CStrEx(dictRequest("DrillDownType")) <> "" And CStrEx(dictRequest("DrillDownGuid")) <> "" Then
            OrderDict = eis.LoadOrder(dictRequest("DrillDownGuid"), CIntEx(dictRequest("DrillDownType")))
        Else
            OrderDict = eis.LoadOrder(dictRequest("HeaderGuid"))
        End If
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End

        ' Ensure that one user can not access another users information.
        If OrderDict("UserGuid") <> globals.User("UserGuid") Then
            ' If the orders UserGuid doesn't match Users UserGuid then make sure
            ' that the UserGuid doesn't exist in UserTable
            User2 = eis.GetUserDict(OrderDict("UserGuid"))
            ' If a User is returned then the order belongs to someone else!
            If User2.Count > 0 Then
                ' Check if the user is able to login
                If eis.ShowLoginLink() Then
                    OrderDict = New ExpDictionary()
                End If
            End If
        End If
        ' Is there an order?
        OrderAvailable = IsDict(OrderDict("Lines"))
        If OrderAvailable Then
            ' Load information for products
            eis.CatDefaultLoadProducts(OrderDict("Lines"), False, False, False, New String() {}, New String() {"PICTURE2"}, Nothing)
            bUseSecondaryCurrency = eis.UseSecondaryCurrency(globals.User)

            ' Make a copy of the order
            dictOrderCopy = OrderDict 'UnmarshallDictionary(MarshallDictionary(OrderDict))            

            '# Call to CalculateOrder() has been removed out because it makes historic orders use new prices

            ' Swap the dictionaries and rename new order dictionary
            Dim dictReCalculatedOrderCopy As Object
            dictReCalculatedOrderCopy = OrderDict
            OrderDict = dictOrderCopy
            ' 	Get product prices
            Dim dictCurrentProductPrices As Object
            dictCurrentProductPrices = New ExpDictionary
            For Each elm In dictReCalculatedOrderCopy("Lines").keys
                dictCurrentProductPrices(dictReCalculatedOrderCopy("Lines")(elm)("ProductGuid")) = dictReCalculatedOrderCopy("Lines")(elm)("ListPriceInclTax")
            Next
            ' Get all products (if available)
            ' Check that all items are still in the catalog

            For Each LineGuid In OrderDict("Lines").keys
                OrderLine = OrderDict("Lines")(LineGuid)
                OrderLine("Available") = Not dictCurrentProductPrices(OrderLine("ProductGuid")) Is Nothing
                If OrderLine("Available") Then
                    ' The just calculated order will make the prices into what ever currency is available now
                    ' The currency should be the currency that was orininally used.
                    OrderLine("NewListPriceInclTax") = currency.ConvertCurrency(dictCurrentProductPrices(OrderLine("ProductGuid")), Session("UserCurrencyGuid").ToString, OrderDict("CurrencyGuid"))
                End If
                If AppSettings("REQUIRE_CATALOG_ENTRY") Then
                    If NaV(OrderLine("ListPrice")) Then
                        OrderLine("Available") = False
                    Else
                        'ExpandIT US - USE CSC - Start
                        If CBoolEx(AppSettings("EXPANDIT_US_USE_CSC")) Then
                            Dim cscObj As New csc(Globals)
                            sql = cscObj.historyAvailableItemsSQL(OrderLine("ProductGuid"))
                        Else
                            'AM2010071901 - ENTERPRISE CSC - Start
                            sql = "SELECT DISTINCT ProductTable.ProductGuid FROM ProductTable, GroupProduct WHERE ProductTable.ProductGuid=GroupProduct.ProductGuid AND ProductTable.ProductGuid=" & SafeString(OrderLine("ProductGuid")) & " AND CatalogNo=" & SafeString(Globals.User("CatalogNo"))
                            'AM2010071901 - ENTERPRISE CSC - End
                        End If
                        'ExpandIT US - USE CSC - End

                        OrderLine("Available") = HasRecords(sql)
                    End If
                End If
            Next
            'AM2010091001 - ONLINE PAYMENTS - Start

            'JA2011030701 - PAYMENT TABLE - START
            'sql = "SELECT SUM(PaymentTransactionAmount) AS PaidAmount FROM EEPGOnlinePayments WHERE DocumentGuid = " & SafeString(CStrEx(Request("DrillDownGuid")))
            sql = "SELECT SUM(PaymentTransactionAmount) AS PaidAmount FROM PaymentTable WHERE DocumentGuid = " & SafeString(CStrEx(Request("DrillDownGuid")))
            'JA2011030701 - PAYMENT TABLE - END

            eepgform.Amount = OrderDict("TotalInclTax") - CDblEx(getSingleValueDB(sql))
            eepgform.isOnlinePayment = True
            'AM2010091001 - ONLINE PAYMENTS - End

            'AM2010031501 - ORDER RENDER - Start
            'Me.CartRender1.IsActiveOrder = False
            'Me.CartRender1.HeaderGuid = OrderDict("HeaderGuid")
            'Me.CartRender1.EditMode = False
            'Me.CartRender1.IsEmail = False
            'Me.CartRender1.DefaultButton = Nothing
            'Me.CartRender1.ShowDate = True
            'Me.CartRender1.ShowPaymentInfo = True
            'AM2010031501 - ORDER RENDER - End
        End If
        '*****shipping*****-start
        If Not CBoolEx(AppSettings("EXPANDIT_US_USE_SHIPPING")) Then
            ' Shipping and Handling information
            Dim dictSHProviders As Object
            dictSHProviders = eis.GetShippingHandlingProviders(OrderDict("ShippingHandlingProviderGuid"), False)
            dictSHProvider = New ExpDictionary

            ' Get the first provider.
            dictSHProvider = GetFirst(dictSHProviders).Value
        End If
        '*****shipping*****-end
        'AM2010021901 - VARIANTS - Start
        UsingVariants = CBoolEx(AppSettings("EXPANDIT_US_USE_VARIANTS"))
        'AM2010021901 - VARIANTS - End
        btnReorder.Text = Resources.Language.ACTION_REORDER

        panelOrderAvailable.Visible = OrderAvailable
        panelOrderNotAvailable.Visible = Not OrderAvailable



        'JA2010092201 - RECURRING ORDERS - START
        If Not OrderDict Is Nothing Then
            If Not CBoolEx(OrderDict("RecurringOrder")) Then
                Me.btnModifyRecurrency.Visible = False
                Me.btnCancelRecurrency.Visible = False
            End If
        End If
        'JA2010092201 - RECURRING ORDERS - END


        ' DEBUG INFORMATION COLLECTION
        If globals.DebugLogic Then
            DebugValue(globals.User, "User")
            DebugValue(OrderDict, "OrderDict")
        End If


        'AM2010092201 - ENTERPRISE CSR - Start
        If CStrEx(OrderDict("PaymentType")) = "PAPERCHECK" AndAlso CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) AndAlso csrObj.getSalesPersonGuid() <> "" Then
            Dim sql2 As String = "SELECT * FROM PaperChecks WHERE OrderNumber=" & SafeString(OrderDict("CustomerReference"))
            Dim checks As ExpDictionary = SQL2Dicts(sql2)
            Dim count As Double = 0
            If Not checks Is Nothing AndAlso Not checks Is DBNull.Value AndAlso checks.Count > 0 Then
                For Each item As ExpDictionary In checks.Values
                    count = count + CDblEx(item("PaperCheckTotals"))
                Next
            End If

            If CDblEx(OrderDict("PaperCheckTotals")) + count < CDblEx(OrderDict("TotalInclTax")) And changeCurrentCustomer = False Then
                Me.btnAddPayperCheck.Visible = True
                Me.btnAddPayperCheck.PostBackUrl = "AddPaperCheck.aspx?HeaderGuid=" & Request("HeaderGuid") & "&OrderNumber=" & getDocumentReference() & "&ActiveOrder=0"
            Else
                Me.btnAddPayperCheck.Visible = False
            End If
        End If
        'AM2010092201 - ENTERPRISE CSR - End


    End Sub

    ' Helper function for displaying up/down/not available images
    Protected Function ShowProductStatus(ByVal OrderLine As Object) As String
        Dim newPriceStr As String
        Dim status As String = ""

        newPriceStr = CurrencyFormatter.FormatAmount(OrderLine("NewListPriceInclTax"), OrderDict("CurrencyGuid"))
        ' remove the <nobr> and </nobr> tag from the string.
        If LCase(Left(newPriceStr, 6)) = "<nobr>" And LCase(Right(newPriceStr, 7)) = "</nobr>" Then
            newPriceStr = Mid(newPriceStr, 7, Len(newPriceStr) - 13)
        End If
        If Not OrderLine("Available") Then
            status = PictureTag("images/not_available.gif", "alt=""" & Resources.Language.LABEL_ITEM_NO_LONGER_AVAILABLE & """")
        ElseIf (OrderLine("ListPriceInclTax") - OrderLine("NewListPriceInclTax")) > 0.00001 Then
            status = PictureTag("images/price_down.gif", "alt=""" & newPriceStr & """")
        ElseIf (OrderLine("NewListPriceInclTax") - OrderLine("ListPriceInclTax")) > 0.00001 Then
            status = PictureTag("images/price_up.gif", "alt=""" & newPriceStr & """")
        End If
        ShowProductStatus = status
    End Function

    'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
    'AM2010080201 - ORDER PRINT OUT - Start
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim script As String
        If CStrEx(Request("HeaderGuid")) <> "" Then
            script = "printOrder('" & VRoot & "/PrintOrder.aspx?HeaderGuid=" & Request("HeaderGuid") & "');"
        ElseIf CStrEx(Request("DrillDownType")) <> "" And CStrEx(Request("DrillDownGuid")) <> "" Then
            script = "printOrder('" & VRoot & "/PrintOrder.aspx?DrillDownGuid=" & CStrEx(Request("DrillDownGuid")) & "&DrillDownType=" & CStrEx(Request("DrillDownType")) & "');"
        End If

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script3", script, True)
    End Sub
    'AM2010080201 - ORDER PRINT OUT - End

    Protected Function getDocumentTypeTittle()
        If CStrEx(Request("DrillDownType")) <> "" Then
            Return CStrEx(b2b.GetDocumentTypeDescription(CStrEx(Request("DrillDownType")))) & " Number"
        Else
            Return Resources.Language.LABEL_ORDER_NUMBER
        End If
    End Function

    Protected Function getDocumentReference()
        If CStrEx(Request("DrillDownType")) <> "" Then
            Return OrderDict("HeaderGuid")
        Else
            Return OrderDict("CustomerReference")
        End If
    End Function
    'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End  

    'AM2010091001 - ONLINE PAYMENTS - Start
    Protected Sub btnOnlinePayment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOnlinePayment.Click
        pnlOnlinePayments.Visible = True
    End Sub

    Public Sub BtnAccept_Click(ByVal sender As Object, ByVal e As EventArgs)
        'prepareContextItems()
        'AM2010041301 - ENTERPRISE PAYMENT GATEWAY - Start
        Dim ccnumber, expdate, ccv2
        Dim FirstName As String
        Dim LastName As String
        Dim address1No As String
        Dim address1St As String
        Dim amount As Double

        'JA2011030701 - PAYMENT TABLE - START
        'Dim sql As String = "SELECT SUM(PaymentTransactionAmount) AS PaidAmount FROM EEPGOnlinePayments WHERE DocumentGuid = " & SafeString(CStrEx(Request("DrillDownGuid")))
        Dim sql As String = "SELECT SUM(PaymentTransactionAmount) AS PaidAmount FROM PaymentTable WHERE DocumentGuid = " & SafeString(CStrEx(Request("DrillDownGuid")))
        'JA2011030701 - PAYMENT TABLE - END

        If OrderDict Is Nothing Then
            If CStrEx(Request("DrillDownType")) <> "" And CStrEx(Request("DrillDownGuid")) <> "" Then
                OrderDict = eis.LoadOrder(Request("DrillDownGuid"), CIntEx(Request("DrillDownType")))
            End If
        End If
        amount = CDblEx(eepgform.Amount)

        'JA2011030701 - PAYMENT TABLE - START
        Dim eepgDict As New ExpDictionary
        eepgDict.Add("PaymentGuid", GenGuid())
        eepgDict.Add("PaymentReference", eis.GenCustRef())
        eepgDict.Add("PaymentDate", DateTime.Now())
        eepgDict.Add("UserGuid", globals.User("UserGuid"))
        eepgDict.Add("LedgerPayment", True)
        'JA2011030701 - PAYMENT TABLE - END

        If amount <= OrderDict("TotalInclTax") - CDblEx(getSingleValueDB(sql)) Then
            'Getting the credit card information from the form.
            ccnumber = eepgform.CCN
            expdate = eepgform.EXPDATE
            ccv2 = CStrEx(eepgform.CCV)
            FirstName = eepgform.FName
            LastName = eepgform.LName

            'JA2011030701 - PAYMENT TABLE - START
            'OrderDict("CCFName") = FirstName
            'OrderDict("CCLName") = LastName
            eepgDict.Add("PaymentFName", FirstName)
            eepgDict.Add("PaymentLName", LastName)
            'JA2011030701 - PAYMENT TABLE - END

            If CBoolEx(AppSettings("PAYMENT_EEPG_REQUIRE_ADDRESS_INFO")) Then
                address1St = CStrEx(eepgform.Address1)
                address1No = Mid(address1St, 1, address1St.IndexOf(" "))
                address1St = Mid(address1St, address1No.Length + 2)

                'JA2011030701 - PAYMENT TABLE - START
                'OrderDict("CCAddress1No") = address1No
                'OrderDict("CCAddress1St") = address1St
                'OrderDict("CCAddress2") = CStrEx(eepgform.Address2)
                'OrderDict("CCCity") = CStrEx(eepgform.City)
                'OrderDict("CCEmail") = CStrEx(eepgform.Email)
                'OrderDict("CCState") = CStrEx(eepgform.State)
                'OrderDict("CCPhone") = CStrEx(eepgform.Phone)
                'OrderDict("CCCountry") = CStrEx(OrderDict("CountryGuid"))
                eepgDict.Add("DocumentGuid", OrderDict("HeaderGuid"))
                eepgDict.Add("PaymentType", OrderDict("PaymentType"))
                eepgDict.Add("PaymentAddress1No", address1No)
                eepgDict.Add("PaymentAddress1St", address1St)
                eepgDict.Add("PaymentAddress2", CStrEx(eepgform.Address2))
                eepgDict.Add("PaymentCity", CStrEx(eepgform.City))
                eepgDict.Add("PaymentEmail", CStrEx(eepgform.Email))
                eepgDict.Add("PaymentState", CStrEx(eepgform.State))
                eepgDict.Add("PaymentPhone", CStrEx(eepgform.Phone))
                eepgDict.Add("PaymentCountry", CStrEx(OrderDict("CountryGuid")))
                'JA2011030701 - PAYMENT TABLE - END
            End If
            If CBoolEx(AppSettings("PAYMENT_EEPG_REQUIRE_ZIP_CODE")) Then
                'JA2011030701 - PAYMENT TABLE - START
                'OrderDict("CCZipCode") = CStrEx(eepgform.ZipCode)
                eepgDict.Add("PaymentZipCode", CStrEx(eepgform.ZipCode))
                'JA2011030701 - PAYMENT TABLE - END
            End If
            'eis.SaveOrderDictionary(globals.OrderDict)

            'Sending the information to the Payment Processor and getting the results from it.
            Dim paymentProcessor As New ExpandIT.EnterprisePaymentGatewayProcessor()
            Dim result As String = paymentProcessor.EEPGProcessPayment(eepgDict, ccnumber, expdate, ccv2, OrderDict, FirstName, LastName, Request, True, amount)
            Dim results As String() = result.Split("*")
            If results(0) = "True" Then 'Validation passed
                'JA2011030701 - PAYMENT TABLE - START
                'OrderDict("PaymentTransactionID") = results(1)
                'OrderDict("PaymentTransactionAmount") = results(2)
                'OrderDict("CCEncryptedData") = results(3)
                eepgDict.Add("PaymentTransactionID", results(1))
                eepgDict.Add("PaymentTransactionAmount", results(2))
                eepgDict.Add("PaymentEncryptedData", results(3))

                Dict2Table(eepgDict, "PaymentTable", "")
                Response.Redirect("onlinePaymentConfirmed.aspx?PaymentReference=" & eepgDict("PaymentReference") & "&DrillDownType=" & CStrEx(Request("DrillDownType")) & "&DrillDownGuid=" & eepgDict("DocumentGuid"))
                'JA2011030701 - PAYMENT TABLE - END

            Else 'Validation failed
                lblOnlinePaymentMessage.Text = result
                lblOnlinePaymentMessage.Visible = True
                lblOnlinePaymentMessage.ForeColor = Drawing.Color.Red
            End If
            'AM2010041301 - ENTERPRISE PAYMENT GATEWAY - End

        Else
            lblOnlinePaymentMessage.Text = Resources.Language.ONLINE_PAYMENT_ALREADY_PAID
            lblOnlinePaymentMessage.Visible = True
            lblOnlinePaymentMessage.ForeColor = Drawing.Color.Red
        End If
    End Sub

    
    'Private Sub prepareContextItems()
    '    If Not isPaymentShippingAddressLoaded Then
    '        loadPaymentAndShippingData()
    '    End If
    '    Context.Items("OrderDict") = IIf(globals.OrderDict Is Nothing, eis.LoadOrderDictionary(globals.User), globals.OrderDict)
    '    Context.Items("Shipping") = Shipping
    '    Context.Items("Payment") = Payment
    'End Sub

    'AM2010041301 - ENTERPRISE PAYMENT GATEWAY - Start
    ''' <summary>
    ''' Handles CheckedChanged on cbxAddress and cbxZipCode         
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub cbxChanged(ByVal sender As Object, ByVal e As EventArgs)

        If OrderDict Is Nothing Then
            If CStrEx(Request("DrillDownType")) <> "" And CStrEx(Request("DrillDownGuid")) <> "" Then
                OrderDict = eis.LoadOrder(Request("DrillDownGuid"), CIntEx(Request("DrillDownType")))
            End If
        End If
        If sender.id = "cbxAddress" And CBoolEx(sender.Checked()) Then
            eepgform.Address1 = CStrEx(OrderDict("Address1"))
            eepgform.Address2 = CStrEx(OrderDict("Address2"))
            eepgform.City = CStrEx(OrderDict("CityName"))
            eepgform.State = CStrEx(OrderDict("StateName"))
            eepgform.Phone = CStrEx(OrderDict("PhoneNo"))
            eepgform.Email = CStrEx(OrderDict("EmailAddress"))
            If AppSettings("PAYMENT_EEPG_REQUIRE_ZIP_CODE") Then
                eepgform.ZipCode = CStrEx(OrderDict("ZipCode"))
            End If
            sender.focus()
        End If
        If sender.id = "cbxZipCode" And CBoolEx(sender.Checked()) Then
            eepgform.ZipCode = CStrEx(OrderDict("ZipCode"))
            sender.focus()
        End If

    End Sub
    'AM2010041301 - ENTERPRISE PAYMENT GATEWAY - End
    'AM2010091001 - ONLINE PAYMENTS - End




    'JA2010092201 - RECURRING ORDERS - START
    Protected Sub btnCancelRecurrency_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelRecurrency.Click
        'Checking if the whole order is cancelled
        Dim ret = getSingleValueDB("Select HeaderGuidOriginal FROM CancelledOrders where HeaderGuidOriginal=" & SafeString(Request("HeaderGuid")) & " AND CancelOrder = 1")
        'Checking if the recurrency for the order is cancelled
        Dim ret1 = getSingleValueDB("Select HeaderGuidOriginal FROM CancelledOrders where HeaderGuidOriginal=" & SafeString(Request("HeaderGuid")) & " AND CancelRecurrency = 1")
        'Checking if it's already Returned
        'Dim ret2 = getSingleValueDB("SELECT HeaderGuid FROM ShopSalesHeader WHERE OrderReference =" & SafeString(Request("CustRef")) & " AND DocumentType='Return Order' OR (CustomerReference=" & SafeString(Request("CustRef")) & " AND DocumentType='Return Order')")
        'If Not ret2 Is Nothing Then
        'Me.lblError.Text = Resources.Language.ERROR_MESSAGE_CANCELL_RETURN_ORDER
        'Me.lblError.Visible = True
        'Else
        If Not ret Is Nothing Then
            Me.lblError.Text = Resources.Language.ERROR_MESSAGE_CANCELLED_ORDER
            Me.lblError.Visible = True
        ElseIf Not ret1 Is Nothing Then
            Me.lblError.Text = Resources.Language.ERROR_MESSAGE_CANCELLED_RECURRENCY
            Me.lblError.Visible = True
        Else
            Me.btnCancelRecurrency.Visible = False
            Me.btnModifyRecurrency.Visible = False
            Me.btnReorder.Visible = False
            Me.btnPrint.Visible = False
            Me.btnOnlinePayment.Visible = False
            Me.btnCancel.Visible = False
            Me.btnReturn.Visible = False

            Me.lblError.Visible = False
            Me.lblCancelReason.Visible = True
            Me.lblModifyCancelReason.Visible = False
            Me.txbCancelReason.Visible = True
            Me.txbCancelReason.Focus()
            Me.SubmitCancelationRecurrency.Visible = True
            Me.SubmitCancelationForModification.Visible = False
        End If
    End Sub

    Protected Sub SubmitCancelationRecurrency_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles SubmitCancelationRecurrency.Click
        Globals.OrderDict = eis.LoadOrder(Request("HeaderGuid"))
        Dim execute, sql
        Dim ret = getSingleValueDB("Select HeaderGuidOriginal FROM CancelledOrders where HeaderGuidOriginal=" & SafeString(Request("HeaderGuid")) & " AND CancelOrder = 1")
        If Not ret Is Nothing Then
            Me.lblError.Text = Resources.Language.ERROR_MESSAGE_CANCELLED_ORDER
            Me.lblError.Visible = True
        Else
            ret = getSingleValueDB("Select HeaderGuidOriginal FROM CancelledOrders where HeaderGuidOriginal=" & SafeString(Request("HeaderGuid")) & " AND CancelRecurrency = 1")
            If Not ret Is Nothing Then
                Me.lblError.Text = Resources.Language.ERROR_MESSAGE_CANCELLED_RECURRENCY
                Me.lblError.Visible = True
            Else
                If Me.txbCancelReason.Text = "" Then
                    Me.lblError.Text = Resources.Language.ERROR_MESSAGE_NO_CANCELLED_REASON
                    Me.lblError.Visible = True
                    Me.txbCancelReason.Focus()
                Else
                    Dim CustomerReference As String = eis.GenCustRef()
                    sql = "INSERT INTO CancelledOrders (CustomerReference,HeaderGuidOriginal,CancelationDate,CancelationReason,CustomerReferenceOriginal,SalesPersonGuid,CancelRecurrency) VALUES (" & SafeString(CustomerReference) & "," & SafeString(globals.OrderDict("HeaderGuid")) & "," & SafeString(Date.Now) & "," & SafeString(Me.txbCancelReason.Text) & "," & SafeString(globals.OrderDict("CustomerReference")) & "," & SafeString(csrObj.getSalesPersonGuid()) & ",1)"
                    execute = getSingleValueDB(sql)
                    Me.lblCancelReason.Visible = False
                    Me.txbCancelReason.Visible = False
                    Me.SubmitCancelationRecurrency.Visible = False
                    Me.lblError.Visible = False
                    Me.Message.Text = Resources.Language.CANCEL_RECURRENCY_UPDATED & CustomerReference
                    Me.Message.Visible = True
                    Me.btnCancelRecurrency.Visible = False
                    Me.btnModifyRecurrency.Visible = False
                    Me.btnReorder.Visible = False
                    Me.btnPrint.Visible = False
                    Me.btnOnlinePayment.Visible = False
                    Me.btnCancel.Visible = False
                    Me.btnReturn.Visible = False
                    'eis.SendCancelledOrderConfirmationEmail(globals.OrderDict, True)
                End If
            End If
        End If
    End Sub

    Public Function IsRecurrencyCancelled(ByVal headerguid As String) As String
        Dim retCancelled As Boolean
        Dim retModified As Boolean

        retCancelled = CBoolEx(getSingleValueDB("Select CancelRecurrency FROM CancelledOrders where HeaderGuidOriginal=" & SafeString(headerguid) & " AND CancelRecurrency=1"))
        retModified = CBoolEx(getSingleValueDB("Select Top 1 ModifiedRecurrency FROM CancelledOrders where HeaderGuidOriginal=" & SafeString(headerguid) & " ORDER BY CancelationDate DESC"))

        If retCancelled Then
            Me.btnCancelRecurrency.Visible = False
            Me.btnModifyRecurrency.Visible = False
            Return Resources.Language.LABEL_RECURRENCY_STATUS_CANCELLED
        ElseIf retModified Then
            Return Resources.Language.LABEL_RECURRENCY_STATUS_MODIFIED
        Else
            Return ""
        End If


    End Function

    Protected Sub btnModifyRecurrency_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModifyRecurrency.Click
        'Checking if the whole order is cancelled
        Dim ret = getSingleValueDB("Select HeaderGuidOriginal FROM CancelledOrders where HeaderGuidOriginal=" & SafeString(Request("HeaderGuid")) & " AND CancelOrder = 1")
        'Checking if the recurrency for the order is cancelled
        Dim ret1 = getSingleValueDB("Select HeaderGuidOriginal FROM CancelledOrders where HeaderGuidOriginal=" & SafeString(Request("HeaderGuid")) & " AND CancelRecurrency = 1")
        'Checking if it's already Returned
        Dim ret2 = getSingleValueDB("SELECT HeaderGuid FROM ShopSalesHeader WHERE OrderReference =" & SafeString(Request("CustRef")) & " AND DocumentType='Return Order' OR (CustomerReference=" & SafeString(Request("CustRef")) & " AND DocumentType='Return Order')")
        If Not ret2 Is Nothing Then
            Me.lblError.Text = Resources.Language.ERROR_MESSAGE_MODIFY_RECURRENCY_RETURN_ORDER
            Me.lblError.Visible = True
        ElseIf Not ret Is Nothing Then
            Me.lblError.Text = Resources.Language.ERROR_MESSAGE_MODIFY_RECURRENCY_CANCELLED_ORDER
            Me.lblError.Visible = True
        ElseIf Not ret1 Is Nothing Then
            Me.lblError.Text = Resources.Language.ERROR_MESSAGE_MODIFY_RECURRENCY_CANCELLED_RECURRENCY
            Me.lblError.Visible = True
        Else
            Me.btnCancelRecurrency.Visible = False
            Me.btnModifyRecurrency.Visible = False
            Me.btnReorder.Visible = False
            Me.btnPrint.Visible = False
            Me.btnOnlinePayment.Visible = False
            Me.btnCancel.Visible = False
            Me.btnReturn.Visible = False

            Me.lblError.Visible = False
            Me.lblCancelReason.Visible = False
            Me.lblModifyCancelReason.Visible = True
            Me.txbCancelReason.Visible = True
            Me.txbCancelReason.Focus()
            Me.SubmitCancelationForModification.Visible = True
            Me.SubmitCancelationRecurrency.Visible = False
        End If
    End Sub

    Protected Sub SubmitCancelationForModification_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles SubmitCancelationForModification.Click
        Globals.OrderDict = eis.LoadOrder(Request("HeaderGuid"))
        Dim execute, sql
        Dim ret = getSingleValueDB("Select HeaderGuidOriginal FROM CancelledOrders where HeaderGuidOriginal=" & SafeString(Globals.OrderDict("HeaderGuid")) & " AND CancelOrder = 1")
        If Not ret Is Nothing Then
            Me.lblError.Text = Resources.Language.ERROR_MESSAGE_MODIFY_RECURRENCY_CANCELLED_ORDER
            Me.lblError.Visible = True
        Else
            ret = getSingleValueDB("Select HeaderGuidOriginal FROM CancelledOrders where HeaderGuidOriginal=" & SafeString(Request("HeaderGuid")) & " AND CancelRecurrency = 1")
            If Not ret Is Nothing Then
                Me.lblError.Text = Resources.Language.ERROR_MESSAGE_MODIFY_RECURRENCY_CANCELLED_RECURRENCY
                Me.lblError.Visible = True
            Else
                If Trim(CStrEx(Me.txbCancelReason.Text)) = "" Then
                    Me.lblError.Text = Resources.Language.ERROR_MESSAGE_NO_CANCELLED_REASON
                    Me.lblError.Visible = True
                    Me.txbCancelReason.Focus()
                Else
                    Dim CustomerReference As String = eis.GenCustRef()
                    Dim Reason As String
                    Reason = Trim(CStrEx(Me.txbCancelReason.Text))
                    sql = "INSERT INTO CancelledOrders (CustomerReference,HeaderGuidOriginal,CancelationDate,CancelationReason,CustomerReferenceOriginal,SalesPersonGuid,ModifiedRecurrency) VALUES (" & SafeString(CustomerReference) & "," & SafeString(globals.OrderDict("HeaderGuid")) & "," & SafeString(Date.Now) & "," & SafeString(Reason) & "," & SafeString(globals.OrderDict("CustomerReference")) & "," & SafeString(csrObj.getSalesPersonGuid()) & ",1)"
                    execute = getSingleValueDB(sql)
                    Me.lblCancelReason.Visible = False
                    Me.txbCancelReason.Visible = False
                    Me.SubmitCancelationRecurrency.Visible = False
                    Me.lblError.Visible = False
                    Me.Message.Text = Resources.Language.CANCEL_RECURRENCY_UPDATED & CustomerReference
                    Me.Message.Visible = True
                    'eis.SendCancelledOrderConfirmationEmail(globals.OrderDict, True)
                End If
            End If
        End If
        'Response.Redirect("cart.aspx?modifyRecurrency=" & Request("HeaderGuid"))
        Response.Redirect("cart.aspx?cartaction=add&modifyRecurrency=" & Request("HeaderGuid"))
    End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        'Checking if it's already Cancelled
        Dim ret = getSingleValueDB("Select HeaderGuidOriginal FROM CancelledOrders where HeaderGuidOriginal=" & SafeString(Request("HeaderGuid")) & " AND CancelOrder = 1")
        'Checking if it's already Returned
        Dim ret2 = getSingleValueDB("SELECT HeaderGuid FROM ShopSalesHeader WHERE OrderReference =" & SafeString(Request("CustRef")) & " AND DocumentType='Return Order' OR (CustomerReference=" & SafeString(Request("CustRef")) & " AND DocumentType='Return Order')")
        If Not ret2 Is Nothing Then
            Me.lblError.Text = Resources.Language.ERROR_MESSAGE_CANCELL_RETURN_ORDER
            Me.lblError.Visible = True
        ElseIf Not ret Is Nothing Then
            Me.lblError.Text = Resources.Language.ERROR_MESSAGE_CANCELLED_ORDER
            Me.lblError.Visible = True
        Else
            Me.lblCancelReason.Visible = True
            Me.txbCancelReason.Visible = True
            Me.txbCancelReason.Focus()
            Me.SubmitCancelation.Visible = True
        End If
    End Sub

    Protected Sub SubmitCancelation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles SubmitCancelation.Click
        Globals.OrderDict = eis.LoadOrder(Request("HeaderGuid"))
        Dim execute, sql
        Dim ret = getSingleValueDB("Select HeaderGuidOriginal FROM CancelledOrders where HeaderGuidOriginal=" & SafeString(Globals.OrderDict("HeaderGuid")) & " AND CancelOrder = 1")
        If Not ret Is Nothing Then
            Me.lblError.Text = Resources.Language.ERROR_MESSAGE_CANCELLED_ORDER
            Me.lblError.Visible = True
        Else
            If Me.txbCancelReason.Text = "" Then
                Me.lblError.Text = Resources.Language.ERROR_MESSAGE_NO_CANCELLED_REASON
                Me.lblError.Visible = True
                Me.txbCancelReason.Focus()
            Else
                Dim CustomerReference As String = eis.GenCustRef()
                sql = "INSERT INTO CancelledOrders (CustomerReference,HeaderGuidOriginal,CancelationDate,CancelationReason,CustomerReferenceOriginal,SalesPersonGuid,CancelRecurrency,CancelOrder) VALUES (" & SafeString(CustomerReference) & "," & SafeString(globals.OrderDict("HeaderGuid")) & "," & SafeString(Date.Now) & "," & SafeString(Me.txbCancelReason.Text) & "," & SafeString(globals.OrderDict("CustomerReference")) & "," & SafeString(csrObj.getSalesPersonGuid()) & ",0,1)"
                execute = getSingleValueDB(sql)
                Me.lblCancelReason.Visible = False
                Me.txbCancelReason.Visible = False
                Me.SubmitCancelation.Visible = False
                Me.lblError.Visible = False
                Me.Message.Text = Resources.Language.CANCELATION_UPDATED & CustomerReference
                Me.Message.Visible = True
                Me.btnReorder.Visible = False
                Me.btnPrint.Visible = False
                Me.btnOnlinePayment.Visible = False
                Me.btnCancel.Visible = False
                Me.btnReturn.Visible = False
                eis.SendCancelledOrderConfirmationEmail(Globals.OrderDict, False)
            End If
        End If
    End Sub
    'AM2010092201 - ENTERPRISE CSR - Start
    Protected Sub btnReturn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReturn.Click

        Dim continueReturn As Boolean = True

        Globals.OrderDict = eis.LoadOrder(Request("HeaderGuid"))
        'Checking if it's already Cancelled
        Dim ret = getSingleValueDB("SELECT HeaderGuidOriginal FROM CancelledOrders WHERE  CancelRecurrency=0 AND HeaderGuidOriginal=" & SafeString(Request("HeaderGuid")))
        If Not ret Is Nothing Then
            Me.lblError.Text = Resources.Language.ERROR_MESSAGE_RETURN_CANCELLED_ORDER
            Me.lblError.Visible = True
            continueReturn = False
        End If
        'Checking if it's already Returned
        Dim sql As String = "SELECT HeaderGuid FROM ShopSalesHeader WHERE (OrderReference =" & SafeString(Request("CustRef")) & ") AND DocumentType='Return Order' OR (CustomerReference=" & SafeString(Request("CustRef")) & " AND DocumentType='Return Order')"
        Dim ret2 = getSingleValueDB(sql)
        If Not ret2 Is Nothing Then
            Me.lblError.Text = Resources.Language.ERROR_MESSAGE_RETURN_ORDER
            Me.lblError.Visible = True
            continueReturn = False
        End If

        If continueReturn Then
            globals.OrderDict("SalesPersonGuid") = csrObj.getSalesPersonGuid()
            Globals.OrderDict("DocumentType") = "Return Order"
            Globals.OrderDict("OrderReference") = Globals.OrderDict("CustomerReference")
            Globals.OrderDict("CustomerReference") = eis.GenCustRef()
            Globals.OrderDict("IsCalculated") = True
            Globals.OrderDict("RecurringOrder") = DBNull.Value
            If Globals.OrderDict("UserGuid") Is Nothing Then
                Globals.OrderDict("UserGuid") = CStrEx(Globals.User("UserGuid"))
            End If
            Dim newGuid As String = GenGuid()
            For Each line As ExpDictionary In Globals.OrderDict("Lines").Values
                line("HeaderGuid") = newGuid
                line("DocumentType") = "Return Order"
                line("LineGuid") = GenGuid()
                line("RFinalDate") = DBNull.Value
                line("RQuantity") = DBNull.Value
                line("RPeriod") = DBNull.Value
                line("RNumber") = DBNull.Value
                line("RNextOrderDate") = DBNull.Value
            Next
            Globals.OrderDict("HeaderGuid") = newGuid
            Globals.OrderDict("MultiCartDescription") = "New Cart"
            Globals.OrderDict("MultiCartStatus") = "ACTIVE"
            excecuteNonQueryDb("DELETE FROM CartLine WHERE HeaderGuid IN(SELECT Headerguid FROM CartHeader WHERE MultiCartStatus='ACTIVE' AND SalesPersonGuid =" & SafeString(csrObj.getSalesPersonGuid()) & ")")
            excecuteNonQueryDb("DELETE FROM CartHeader WHERE MultiCartStatus='ACTIVE' AND SalesPersonGuid =" & SafeString(csrObj.getSalesPersonGuid()))
            Globals.eis.SaveOrderDictionary(Globals.OrderDict)
            Response.Redirect(VRoot & "/cart.aspx")
        End If
    End Sub

    Protected Function getReturnedOrder()
        Dim str As String = ""
        Dim sql As String = "SELECT HeaderGuid,CustomerReference FROM ShopSalesHeader WHERE (OrderReference =" & SafeString(CStrEx(Request("CustRef"))) & " ) AND (DocumentType = 'Return Order')"
        Dim returnedOrder As ExpDictionary = Sql2Dictionary(sql)
        If Not returnedOrder Is Nothing Then
            If returnedOrder.Count > 0 Then
                str = "<a href=""" & VRoot & "/history_detail.aspx?HeaderGuid=" & returnedOrder("HeaderGuid") & "&CustRef=" & returnedOrder("CustomerReference") & "&justReturn=1" & """ >" & Resources.Language.LABEL_CSR_GET_RETURN_ORDER & "</a>"
            End If
        ElseIf CStrEx(Request("justReturn")) = "" Then
            sql = "SELECT COUNT(*) FROM SHOPSALESHEADER WHERE DOCUMENTTYPE='Return Order' AND OrderReference IS NULL AND HEADERGUID=" & SafeString(Request("HeaderGuid"))
            If CIntEx(getSingleValueDB(sql)) > 0 Then
                str = "<a href=""" & VRoot & "/history_detail.aspx?HeaderGuid=" & Request("HeaderGuid") & "&justReturn=1" & """ >" & Resources.Language.LABEL_CSR_GET_RETURN_ORDER & "</a>"
            End If
        End If
        Return str
    End Function

    Protected Function getTrakingLink(ByVal number As String)
        If CStrEx(number) <> "" Then
            Dim firstLetters As String = number.Substring(0, 2)
            If firstLetters.ToUpper() = "1Z" Then
                Return "<a target=""_blank"" href=""http://wwwapps.ups.com/WebTracking/processInputRequest?TypeOfInquiryNumber=T&InquiryNumber1=" & number & """>" & number & "</a>"
            Else
                Return "<a target=""_blank"" href=""http://trkcnfrm1.smi.usps.com/PTSInternetWeb/InterLabelInquiry.do?origTrackNum=" & number & """>" & number & "</a>"
            End If
        Else
            Return Resources.Language.MESSAGE_NOT_AVAILABLE
        End If

    End Function
    'AM2010092201 - ENTERPRISE CSR - End

    'JA2010092201 - RECURRING ORDERS - END

End Class