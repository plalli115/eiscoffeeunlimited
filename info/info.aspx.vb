Imports System.Configuration.ConfigurationManager
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT.GlobalsClass
Imports ExpandIT
Imports System.IO

Partial Class info
    Inherits ExpandIT.Page

    Protected infohtml As String

    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim InfoLanguage As String

        eis.PageAccessRedirect("HomePage")
        InfoLanguage = UCase(CStrEx(globals.User("LanguageGuid")))

        Select Case InfoLanguage
            Case "DA", "EN", "ES", "DE", "PT", "SV"
                infohtml = ReadTextFile(HttpContext.Current.Server.MapPath(".") & "\info_" & LCase(InfoLanguage) & ".htm")
                infohtml = infohtml.Replace("{CompanyInformation}", eis.RenderCompanyInformation("1"))
            Case Else
                infohtml = ReadTextFile(HttpContext.Current.Server.MapPath(".") & "\info_en.htm")
                infohtml = infohtml.Replace("{CompanyInformation}", eis.RenderCompanyInformation("1"))
        End Select

        '<!--JM2010061001 - LINK TO - Start-->
        Dim newHtml As String = ""
        newHtml = eis.findLinkToHtml(infohtml)
        litContent.Text = newHtml
        '<!--JM2010061001 - LINK TO - End-->
    End Sub

End Class
