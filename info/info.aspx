<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="info.aspx.vb" Inherits="info"
    CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master" RuntimeMasterPageFile="ThreeColumn.master" 
    CrumbName="<%$ Resources: Language, LABEL_INFORMATION %>" %>

<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <div class="InfoPage">
        <uc1:PageHeader ID="PageHeader1" runat="server" Text="<%$ Resources: Language, LABEL_INFORMATION %>"
            EnableTheming="true" />
        <asp:Literal ID="litContent" runat="server" Text="Content of information page"></asp:Literal>
    </div>
</asp:Content>
