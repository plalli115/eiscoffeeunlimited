<%--JA2011021701 - STORE LOCATOR - Start--%>
<%@ Import Namespace="System.Configuration.ConfigurationManager" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="mapShow.aspx.vb" Inherits="mapShow"
    CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master"
    RuntimeMasterPageFile="TwoColumn.master" %>

<%@ Register Src="controls/Box.ascx" TagName="Box" TagPrefix="uc2" %>
<%@ Register Src="controls/LoginStatus.ascx" TagName="LoginStatus" TagPrefix="uc3" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Register Src="~/controls/MostPopular.ascx" TagName="MostPopular" TagPrefix="uc1" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<%@ Register Src="~/controls/Message.ascx" TagName="Message" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <asp:Panel ID="Panel1" runat="server">

        <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=true&amp;key=<%= AppSettings("STORE_LOCATOR_GOOGLE_MAPS_KEY") %>"
            type="text/javascript"></script>

        <table style="width: 100%;">
            <tr>
                <td colspan="2" style="padding-bottom:20px;padding-top:20px;">
                    <asp:Label ID="Label1" style="font-size:14px;font-weight:bold;color:Black;" runat="server" Text="<%$ Resources: Language, LABEL_MAPS_LOCATION  %>"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 560px;">
                    <div id="map" style="width: 550px; height: 360px;">
                    </div>
                    <div id="regularMap" style="position: relative; top: -360px; left: 0px;">
                        <img src="<%=Vroot %>/images/map/icons/UsaMap.jpg" width="570" height="372" border="0"
                            usemap="#Map" />
                        <map name="Map" id="Map1">
                            <area shape="rect" coords="431,279,491,330" href="#" onclick="SetLocation('FL');"
                                alt="FL" />
                            <area shape="rect" coords="420,232,451,264" href="#" style="cursor: pointer;" onclick="SetLocation('GA');"
                                alt="GA" />
                            <area shape="rect" coords="451,215,471,231" href="#" style="cursor: pointer;" onclick="SetLocation('SC');"
                                alt="SC" />
                            <area shape="rect" coords="380,225,405,273" href="#" style="cursor: pointer;" onclick="SetLocation('AL');"
                                alt="AL" />
                            <area shape="rect" coords="450,190,493,208" href="#" style="cursor: pointer;" onclick="SetLocation('NC');"
                                alt="NC" />
                            <area shape="circle" coords="526,190,14" href="#" style="cursor: pointer;" onclick="SetLocation('DC');"
                                alt="DC" />
                            <area shape="rect" coords="513,156,545,175" href="#" style="cursor: pointer;" onclick="SetLocation('MD');"
                                alt="MD" />
                            <area shape="rect" coords="520,141,562,154" href="#" style="cursor: pointer;" onclick="SetLocation('DE');"
                                alt="DE" />
                            <area shape="rect" coords="509,125,554,139" href="#" style="cursor: pointer;" onclick="SetLocation('NJ');"
                                alt="NJ" />
                            <area shape="rect" coords="523,108,556,123" href="#" style="cursor: pointer;" onclick="SetLocation('CT');"
                                alt="CT" />
                            <area shape="rect" coords="525,92,580,108" href="#" style="cursor: pointer;" onclick="SetLocation('RI');"
                                alt="RI" />
                            <area shape="rect" coords="537,70,575,88" href="#" style="cursor: pointer;" onclick="SetLocation('MA');"
                                alt="MA" />
                            <area shape="rect" coords="527,18,561,63" href="#" style="cursor: pointer;" onclick="SetLocation('ME');"
                                alt="ME" />
                            <area shape="rect" coords="502,19,524,57" href="#" style="cursor: pointer;" onclick="SetLocation('NH');"
                                alt="NH" />
                            <area shape="rect" coords="474,23,498,54" href="#" style="cursor: pointer;" onclick="SetLocation('VT');"
                                alt="VT" />
                            <area shape="rect" coords="451,70,506,106" href="#" style="cursor: pointer;" onclick="SetLocation('NY');"
                                alt="NY" />
                            <area shape="rect" coords="449,112,499,136" href="#" style="cursor: pointer;" onclick="SetLocation('PA');"
                                alt="PA" />
                            <area shape="rect" coords="463,158,487,183" href="#" style="cursor: pointer;" onclick="SetLocation('VA');"
                                alt="VA" />
                            <area shape="rect" coords="435,152,459,176" href="#" style="cursor: pointer;" onclick="SetLocation('WV');"
                                alt="WV" />
                            <area shape="rect" coords="408,128,444,150" href="#" style="cursor: pointer;" onclick="SetLocation('OH');"
                                alt="OH" />
                            <area shape="rect" coords="387,169,434,189" href="#" style="cursor: pointer;" onclick="SetLocation('KY');"
                                alt="KY" />
                            <area shape="rect" coords="368,199,420,217" href="#" style="cursor: pointer;" onclick="SetLocation('TN');"
                                alt="TN" />
                            <area shape="rect" coords="388,82,415,122" href="#" style="cursor: pointer;" onclick="SetLocation('MI');"
                                alt="MI" />
                            <area shape="rect" coords="382,127,404,167" href="#" style="cursor: pointer;" onclick="SetLocation('IN');"
                                alt="IN" />
                            <area shape="rect" coords="350,226,378,280" href="#" style="cursor: pointer;" onclick="SetLocation('MS');"
                                alt="MS" />
                            <area shape="rect" coords="315,254,344,288" href="#" style="cursor: pointer;" onclick="SetLocation('LA');"
                                alt="LA" />
                            <area shape="rect" coords="349,131,376,179" href="#" style="cursor: pointer;" onclick="SetLocation('IL');"
                                alt="IL" />
                            <area shape="rect" coords="326,68,368,112" href="#" style="cursor: pointer;" onclick="SetLocation('WI');"
                                alt="WI" />
                            <area shape="rect" coords="311,203,346,249" href="#" style="cursor: pointer;" onclick="SetLocation('AR');"
                                alt="AR" />
                            <area shape="rect" coords="304,152,343,197" href="#" style="cursor: pointer;" onclick="SetLocation('MO');"
                                alt="MO" />
                            <area shape="rect" coords="289,113,338,144" href="#" style="cursor: pointer;" onclick="SetLocation('IA');"
                                alt="IA" />
                            <area shape="poly" coords="271,362,190,276,233,220,308,273" href="#" style="cursor: pointer;"
                                onclick="SetLocation('TX');" alt="TX" />
                            <area shape="rect" coords="253,196,302,236" href="#" style="cursor: pointer;" onclick="SetLocation('OK');"
                                alt="OK" />
                            <area shape="rect" coords="235,158,302,192" href="#" style="cursor: pointer;" onclick="SetLocation('KS');"
                                alt="KS" />
                            <area shape="rect" coords="153,325,210,369" href="#" style="cursor: pointer;" onclick="SetLocation('HI');"
                                alt="HI" />
                            <area shape="rect" coords="4,250,89,367" href="#" style="cursor: pointer;" onclick="SetLocation('AK');"
                                alt="AK" />
                            <area shape="rect" coords="155,189,204,257" href="#" style="cursor: pointer;" onclick="SetLocation('NM');"
                                alt="NM" />
                            <area shape="rect" coords="290,40,319,110" href="#" style="cursor: pointer;" onclick="SetLocation('MN');"
                                alt="MN" />
                            <area shape="rect" coords="220,32,279,74" href="#" style="cursor: pointer;" onclick="SetLocation('ND');"
                                alt="ND" />
                            <area shape="rect" coords="218,81,284,113" href="#" style="cursor: pointer;" onclick="SetLocation('SD');"
                                alt="SD" />
                            <area shape="rect" coords="219,116,285,148" href="#" style="cursor: pointer;" onclick="SetLocation('NE');"
                                alt="NE" />
                            <area shape="rect" coords="164,138,214,186" href="#" style="cursor: pointer;" onclick="SetLocation('CO');"
                                alt="CO" />
                            <area shape="rect" coords="123,21,214,78" href="#" style="cursor: pointer;" onclick="SetLocation('MT');"
                                alt="MT" />
                            <area shape="rect" coords="148,82,211,130" href="#" style="cursor: pointer;" onclick="SetLocation('WY');"
                                alt="WY" />
                            <area shape="rect" coords="97,188,146,254" href="#" style="cursor: pointer;" onclick="SetLocation('AZ');"
                                alt="AZ" />
                            <area shape="rect" coords="110,131,157,180" href="#" style="cursor: pointer;" onclick="SetLocation('UT');"
                                alt="UT" />
                            <area shape="rect" coords="32,5,98,48" href="#" style="cursor: pointer;" onclick="SetLocation('WA');"
                                alt="WA" />
                            <area shape="rect" coords="22,49,86,93" href="#" style="cursor: pointer;" onclick="SetLocation('OR');"
                                alt="OR" />
                            <area shape="poly" coords="107,17,88,100,132,107" href="#" style="cursor: pointer;"
                                onclick="SetLocation('ID');" alt="ID" />
                            <area shape="poly" coords="87,189,49,116,110,112" href="#" style="cursor: pointer;"
                                onclick="SetLocation('NV');" alt="NV" />
                            <area shape="poly" coords="4,152,65,237,81,206,21,91" href="#" style="cursor: pointer;"
                                onclick="SetLocation('CA');" alt="CA" />
                        </map>
                    </div>
                </td>
                <td>
                        <table>
                            <tr>
                                <td>
                                    <input id="geocodeInput" onkeypress="stopRKey(event);" style="height: 16px; width: 125px;"
                                        name="geocodeInput" type="text" />
                                    <%--<asp:Button ID="btnSearch" runat="server" Text="Search" /> --%>
                                <input id="Button1" name="Button1" class="ButtonGray" runat="server" type="button" value="Search" onclick="hideRegularMap();" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span id="Radius" />
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-top: 50px;">
                                    <span id="ShowInfo" style="display: none;" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <div id="international" style="position: relative; top: 10px; left: 3px;">
                                        <a style="cursor: pointer;" onclick="SetLocation('europe');">
                                            <img alt="" src="<%=Vroot %>/images/map/icons/IntLocations.PNG" />
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        </table>
                </td>
            </tr>
        </table>
        <input id="vroot" value="<%=Vroot %>" type="hidden" />

        <script type="text/javascript">

        var vroot=document.getElementById('vroot').value;
        
        var map = new GMap2(document.getElementById("map"));
               
        map.setMapType(google.maps.NORMAL_MAP);
        map.addControl(new GSmallMapControl());
        map.addControl(new GMapTypeControl());
        map.setCenter(new GLatLng(45.6648274, -120.7157535), 4);
        map.checkResize(); 


        // Create a base icon for all of our markers that specifies the
        // shadow, icon dimensions, etc.
        var baseIcon = new GIcon(G_DEFAULT_ICON);
        baseIcon.shadow = vroot + '/images/map/icons/shadow50.png';
        baseIcon.iconSize = new GSize(32, 68);
        baseIcon.shadowSize = new GSize(37, 34);
        baseIcon.iconAnchor = new GPoint(9, 34);
        baseIcon.infoWindowAnchor = new GPoint(5, 2);

        // Creates a marker whose info window displays the letter corresponding
        // to the given index.
        function createMarker(point, index, guid) {
          var vroot=document.getElementById('vroot').value;
          // Create a lettered icon for this point using our icon class
          //var letter = String.fromCharCode("A".charCodeAt(0) + index);
          var letteredIcon = new GIcon(baseIcon);
          letteredIcon.image = vroot + '/images/map/icons/user_red.png';

          // Set up our GMarkerOptions object
          markerOptions = { icon:letteredIcon };
          var marker = new GMarker(point, markerOptions);

          GEvent.addListener(marker, "mouseover", function() {
            marker.openInfoWindowHtml(index);
          });
          <% If CboolEx(AppSettings("STORE_LOCATOR_SHOW_FRANCHISE_PAGE")) Then %>
          GEvent.addListener(marker, "click", function() {
            window.location= vroot + '/franchiseStores.aspx?guid=' + guid ;
          });
          <%End If %>
          return marker;
        }

        //Get All the points

        <%For Each item As ExpDictionary In PointsDict.Values %>
            var latTemp='<%=item("Latitude") %>';
            //alert(latTemp);
            var lonTemp='<%=item("Longitude") %>';
            //alert(lonTemp);
            var html='<b><%=item("FranchiseName") & "</b><br />" & item("Address1") & "<br />" & item("City") & "<br />" & item("StateName") & "<br />" & item("ZipCode") & "<br />" & item("Phone") & "<br />" %>';
            //alert(html);
            var point = new GLatLng(latTemp,lonTemp);
            map.addOverlay(createMarker(point, html, '<%=item("FranchiseGuid") %>'));
        <% Next %>

        //var point = new GLatLng(28.489995,-81.412222)
        //map.addOverlay(createMarker(point,1))
        //var point = new GLatLng(28.655952,-81.346288)
        //map.addOverlay(createMarker(point,2))

        // Set up Geocoder
        function setMyMap(){
            window.geocoder = new google.maps.ClientGeocoder();
            
            // If query string was provided, geocode it
//            var bits = window.location.href.split('?');
//            if (bits[1]) {
//                var location = decodeURI(bits[1]);
//                document.getElementById('geocodeInput').value = location;
//                geocode(location);
//            }
            
            // Set up the form
                geocode(document.getElementById('geocodeInput').value);
        }

        var accuracyToZoomLevel = [
            1,  // 0 - Unknown location
            5,  // 1 - Country
            6,  // 2 - Region (state, province, prefecture, etc.)
            8,  // 3 - Sub-region (county, municipality, etc.)
            11, // 4 - Town (city, village)
            13, // 5 - Post code (zip code)
            15, // 6 - Street
            16, // 7 - Intersection
            17, // 8 - Address
            17  // 9 - Premise
        ];

        function geocodeComplete(result) {
            var vroot=document.getElementById('vroot').value;
            
            if (result.Status.code != 200) {
                //alert('Could not geocode "' + result.name + '"');
                alert('Please fill out the text box.');
                return;
            }
            var placemark = result.Placemark[0]; // Only use first result
            var accuracy = placemark.AddressDetails.Accuracy;
            var zoomLevel = accuracyToZoomLevel[accuracy] || 1;
            var lon = placemark.Point.coordinates[0];
            var lat = placemark.Point.coordinates[1];
            var currentLocation= document.getElementById('geocodeInput').value;
            if(currentLocation.toLowerCase() == 'europe'){
                zoomLevel=3;
            }
            map.setCenter(new google.maps.LatLng(lat, lon), zoomLevel);
            
            map.checkResize(); 
             
            
            
            
            var bounds = map.getBounds();
            var southWest = bounds.getSouthWest();
            var northEast = bounds.getNorthEast();
            var rightEdge = new GLatLng((bounds.getNorthEast().lat() + bounds.getSouthWest().lat())/2,
                            bounds.getNorthEast().lng());
            var  radius = rightEdge.distanceFrom(map.getCenter()) / 1609.344; // meters/mi
            // var radius = southWest.distanceFrom(northEast) / (2*1609.344); // meters/mi
            var miles=radius.toFixed(3);
            document.getElementById('Radius').innerHTML= '<b>Radius: </b>' + parseInt(miles) + ' Miles';
            
            
            
            
            
                         
            var center = map.getCenter();
            var xhReq;
            var url;
            if (center.lat() !='' && center.lng() !=''){                
                url= vroot + "/mapAjax.aspx?lon=" + center.lng() + "&lat=" + center.lat() + "&radious=" + miles;
                xhReq = new XMLHttpRequest();
                
                
                xhReq.open("GET", url += (url.match(/\?/) == null ? "?" : "&") + (new Date()).getTime(), false);
                xhReq.send(null);
                
//                if(xhReq.readyState ==4 && xhReq.status == 200){
//                    alert(xhReq.responseText);
//                }
            }
            document.getElementById('ShowInfo').innerHTML=xhReq.responseText;
            document.getElementById('ShowInfo').style.display='block';
            
                       
        }

        function geocode(location) {
            geocoder.getLocations(location, geocodeComplete);
        }
        function SetLocation(location2) {
            document.getElementById('geocodeInput').value=location2;
            hideRegularMap();
            //document.getElementById('regularMap').style.display='none'; 
            //setMyMap();
            //document.getElementById('geocodeInput').value='';
        }
        
        function hideRegularMap(){
            setMyMap();
            setTimeout('settime();' ,1000);
                       
        }
        
        function settime() {
            document.getElementById('regularMap').style.display='none'; 
            document.getElementById('international').style.display='none';
            setMyMap();
        }
        
        function stopRKey(evt) {
           var evt = (evt) ? evt : ((event) ? event : null);
           var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
           if ((evt.keyCode == 13) && (node.type=="text")) {
                hideRegularMap();
                return false;
           }
        }

        document.onkeypress = stopRKey; 
        
        
        </script>

    </asp:Panel>
</asp:Content>
<asp:Content ID="ContentBestSellers" ContentPlaceHolderID="ContentPlaceHolderBestSellers"
    runat="server">
</asp:Content>
<asp:Content ID="contentSpecials" ContentPlaceHolderID="SpecialsHolder" runat="server">
</asp:Content>
<%--JA2011021701 - STORE LOCATOR - End--%>
