<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AddPaperCheck.aspx.vb" Inherits="AddPaperCheck"
    ValidateRequest="false" EnableEventValidation="false" Debug="true" CodeFileBaseClass="ExpandIT.ShippingPayment.ShippingPaymentPage"
    MasterPageFile="~/masters/default/main.master" RuntimeMasterPageFile="ThreeColumn.master"
    CrumbName="<%$ Resources: Language, LABEL_PAYMENT_INFORMATION %>" %>

<%--AM2010080301 - CHECK OUT PROCESS MAP - Start--%>
<%@ MasterType VirtualPath="~/masters/EnterpriseBlue/ThreeColumn.master" %>
<%--AM2010080301 - CHECK OUT PROCESS MAP - End--%>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Register Namespace="ExpandIT.SimpleUIControls" TagPrefix="expui" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <!-- Here begins the HTML code for the page -->
    <div class="PaymentPage">
        <h2>
            <a href="javascript: history.go(-1)">
                <%=Resources.Language.LABEL_CSR_BACK_TO_ORDER%>
            </a>
        </h2>
        <uc1:PageHeader ID="PageHeader1" runat="server" Text="<%$ Resources: Language, LABEL_PAYMENT_INFORMATION %>"
            EnableTheming="true" />
        <%--JA2010102801 - PAPERCHECK - Start--%>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <expui:PaperCheckPaymentControl ID="papercheckform" runat="server" Visible="false" HideComments="False" 
                        TableWidth="450" ButtonText="ACTION_ACCEPT_2" HeaderText="LABEL_PAYMENT_INFORMATION"
                        LabelText="LABEL_PAYMENT_INFORMATION_PAPERCHECK" TextPart2="ACTION_ACCEPT_2" />
                </td>
            </tr>
        </table>
        <%--JA2010102801 - PAPERCHECK - End--%>
    </div>
</asp:Content>
