<%@ Page Language="VB" AutoEventWireup="false" CodeFile="account.aspx.vb" Inherits="account"
    CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master"
    RuntimeMasterPageFile="ThreeColumn.master" CrumbName="<%$ Resources: Language, LABEL_MENU_ACCOUNT %>" %>

<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <div class="AccountPage">
        <uc1:PageHeader ID="PageHeader1" runat="server" Text="<%$ Resources: Language, LABEL_MENU_ACCOUNT %>"
            EnableTheming="true" />
        <ul>
            <li>
                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="user.aspx" ></asp:HyperLink>
            </li>
            <li>
                <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="userpassword.aspx" Text="<%$ Resources: Language, LABEL_USERDATA_UPDATE_USER_PASSWORD %>"></asp:HyperLink>
            </li>
            <li>
                <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="history.aspx?Status=previous" Text="<%$ Resources: Language, LABEL_ORDER_HISTORY %>"></asp:HyperLink>
            </li>
            <!--AM2011031801 - ENTERPRISE CSR STAND ALONE - Start-->
            <%If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then %>
                <asp:PlaceHolder runat="server" ID="phCSRAccountOptions"></asp:PlaceHolder>
            <%End If %>
            <!--AM2011031801 - ENTERPRISE CSR STAND ALONE - End-->
            <li>
                <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="history_lookup.aspx" Text="<%$ Resources: Language, LINK_LOOKUP_ORDER_REFERENCE %>"></asp:HyperLink>
            </li>
            <%  If Not CBoolEx(globals.User("B2B")) Then
                    If CBoolEx(AppSettings("USE_MAIL_CONFIRMED_REGISTRATION")) Then%>
            <li>
                <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="user_deactivate.aspx"
                    Text="<%$ Resources: Language, LABEL_USER_ACCOUNT_DEACTIVATE %>"></asp:HyperLink>
            </li>
            <%  End If
            End If%>
            <li>
                <asp:HyperLink ID="contactLink" runat="server" NavigateUrl="~/contact.aspx" Text="<%$ Resources: Language, LABEL_CONTACT_US %>"></asp:HyperLink>
            </li>
            <asp:Panel ID="ledgerPanel" runat="server">
                <li>
                    <asp:HyperLink ID="HyperLink6" runat="server" NavigateUrl="history_ledgerentry.aspx"
                        Text="<%$ Resources: Language, LABEL_ORDER_LEDGER %>"></asp:HyperLink>
                </li>
            </asp:Panel>
        </ul>
    </div>
</asp:Content>
