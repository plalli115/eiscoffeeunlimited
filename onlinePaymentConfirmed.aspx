<%@ Page Language="VB" AutoEventWireup="false" CodeFile="onlinePaymentConfirmed.aspx.vb" Inherits="onlinePaymentConfirmed"
    CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master"
    RuntimeMasterPageFile="ThreeColumn.master" CrumbName="<%$ Resources: Language, LABEL_CONFIRMATION %>" %>

<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.GlobalsClass" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <div class="ConfirmedFoundPage">
        <!-- Here begins the HTML code for the page -->
        <uc1:PageHeader ID="PageHeader2" runat="server" Text="<%$ Resources: Language, LABEL_CONFIRMATION %>"
            EnableTheming="true" />
        <p>
            <% Response.Write(paymentMessage())%>
            <br />
            <br />
            <% Response.Write(backToDocumentMessage()) %>
            <br />
            <br />
            <% Response.Write(backToLedgerEntries()) %>
        </p>
        <!-- Here ends the HTML code for the page -->
    </div>
</asp:Content>
