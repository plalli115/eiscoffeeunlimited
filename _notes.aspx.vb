﻿Imports System.Configuration.ConfigurationManager
Imports ExpandIT.GlobalsClass
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT
Imports System.Data



Partial Class _notes
    Inherits ExpandIT.Page
    
    Protected UserGuid As String
    Protected ProductGuid As String
    Protected VariantCode As String
    Protected NoteName As String
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Action As String
      
        ' Check the page access.
        eis.PageAccessRedirect("Notes")
        
        'Get UserGuid
        UserGuid = SafeString(globals.User("UserGuid")) 
      
        ' Get Action.
        Action = UCase(Request("action"))        
        
        ' Get ProductGuid.
        If Request("SKU") <> "" Then
            ProductGuid = Request("SKU")
        Else
            ProductGuid = "NO_PRODUCT_GUID"
        End If
        
        ' Get VariantCode
        If Not Request("VariantCode") Is Nothing then
          VariantCode = Request("VariantCode")
        Else
          VariantCode = ""
        End If
        
        
        'Get NoteName 
        If Not Request("NNN") is Nothing  then      
          NoteName = Request("NNN")
        Else
          NoteName = ""
        End If
        
        
        ' Do Action
        Select Case Action
            Case "ADD"
                Add()
            Case "OUTOFNOTES" 
                OutOfNotes()         
            Case ""
                Response.Redirect(VRoot & "/notes.aspx")
            Case "CLEAR"
                ClearNote()
            Case "SAVE"
                SaveNote(Request("SNNN"))
            Case "SHOW"
                Response.Redirect(VRoot & String.Format("/notes.aspx?NNN={0}", HttpUtility.UrlEncode(NoteName)))             
            Case Else
                globals.messages.Errors.Add(Resources.Language.MESSAGE_UNKNOWN_ACTION & " '" & Action & "'")
        End Select
        
        ' Find URL to redirect to.
        If Request("returl") Is Nothing Then
            Response.Redirect(MakeParam(VRoot & "/notes.aspx?", globals.messages.QueryString))
        Else
            Response.Redirect(MakeParam(VRoot & "/" & Request("returl"), globals.messages.QueryString))
        End If        
    End Sub
    
    ' Helper function: Add the item(s) to notes.
    Sub Add()        
        Dim prGuid As String        
        Dim sql As String           
        prGuid = SafeString(ProductGuid)    
        sql = String.Format("SELECT * FROM Notes WHERE UserGuid = {0} AND  ProductGuid = '{1}' AND ListName = '' AND VariantCode = '{2}'", UserGuid, ProductGuid, VariantCode)
        If Not HasRecords (sql)
            sql = String.Format("INSERT INTO Notes (UserGuid, ProductGuid, ListName, VariantCode) VALUES ({0}, '{1}', '', '{2}')", UserGuid, ProductGuid, VariantCode)
            excecuteNonQueryDb(sql)
        Else
            globals.messages.Warnings.Add(Resources.Language.LABEL_THE_PRODUCT_IS_ALREADY_IN_THE_LIST)
        End If
    End Sub
    
    
    ' Helper function: 
    ' "OutOfNotes" will allow a product to be removed from the note list, by clicking the red cross
    ' (delete) on the notes.asp page.
    Sub OutOfNotes()
        ProductGuid = GetRequest("ProductGuid")              
        If ProductGuid <> "" Then
            Dim sql As String  
            Dim strVariantCode As String = SafeString(VariantCode)
            If strVariantCode = "NULL" then 
              strVariantCode = "''"
            End If                        
            sql = String.Format("DELETE FROM Notes WHERE (UserGuid = {0}) AND (ProductGuid = {1}) AND (ListName = '{2}') AND (VariantCode = {3})", _
              UserGuid, SafeString(ProductGuid), NoteName, strVariantCode)
            Dim s as string = excecuteNonQueryDb(sql)
        End If
        Response.Redirect(VRoot & String.Format("/notes.aspx?NNN={0}", HttpUtility.UrlEncode(NoteName))) 
    End Sub
    
    ' Helper function: Adds the error string to the URL
    Function MakeParam(ByVal url As Object, ByVal errorString As Object) As String
        Dim retv As String

        retv = url
        If errorString <> "" Then
            retv = retv & IIf(InStr(1, url, "?") = 0, "?", "&") & errorString
        End If

        Return retv
    End Function
   
    
    Sub ClearNote()        
        Dim sql As string        
        sql = String.Format("DELETE FROM Notes WHERE (UserGuid = {0}) AND (ListName = '{1}')", UserGuid, NoteName)
        excecuteNonQueryDb(sql)  
        globals.messages.Messages.Add(String.Format(Resources.Language.MESSAGE_NOTE_DELETED, NoteName))      
    End Sub
    
    Sub SaveNote(NewNoteName As String)
        Dim sourceSql As string 
        NewNoteName = NewNoteName.Replace("'","")        
        NewNoteName = NewNoteName.Replace("""","")             
        sourceSql = String.Format("SELECT * FROM Notes WHERE (UserGuid = {0}) AND (ListName = '{1}')", UserGuid, NoteName)
        Dim sourceDt As DataTable = SQL2DataTable(sourceSql)
        Dim testSql As String, insertSql As String
        For Each row As DataRow In sourceDt.Rows
            testSql = String.Format("SELECT * FROM Notes WHERE (UserGuid = '{0}') AND (ProductGuid = '{1}') AND (ListName = '{2}') AND (VariantCode = '{3}')", _ 
              row(0).ToString(), row(1).ToString(), NewNoteName, row(3).ToString())
            If Not HasRecords(testSql) then
                insertSql = String.Format("INSERT INTO Notes (UserGuid, ProductGuid, ListName, VariantCode) VALUES ('{0}', '{1}', '{2}', '{3}')", _ 
                  row(0).ToString(), row(1).ToString(), NewNoteName, row(3).ToString())
                excecuteNonQueryDb(insertSql)
            End If
        Next
        globals.messages.Messages.Add(String.Format(Resources.Language.MESSAGE_NOTE_SAVED, NoteName, NewNoteName))      
        Response.Redirect(MakeParam(VRoot & String.Format("/notes.aspx?NNN={0}", HttpUtility.UrlEncode(NewNoteName)), globals.messages.QueryString))             
    End Sub
    

End Class
