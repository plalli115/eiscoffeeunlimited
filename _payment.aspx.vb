Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT.GlobalsClass
Imports ExpandIT.ShippingPayment
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.IO

Partial Class _payment
    Inherits _PaymentPage

    Private Shipping, Payment, BillTo, ShipTo As ExpDictionary
    Private dtShippingAddress As DataTable
    Private aKey As KeyValuePair(Of Object, Object)
    Private HeaderGuid As String
    Dim bMail As Boolean
    Dim paymenttype, transact, PaymentTransactionSignature As String
    Dim clearedamount, paymentFee, paymentFeeInclTax As Decimal

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim CartHeaderGuid As String
        Dim redirectOnError As Boolean

        ' Load user information.
        ' If response from Authorize.net - load the user from request("globals.UserGuid")
        ' AuthorizeNet_Validate is called later to verify authentication of the request.
        '<!--JA2010022601 - ECHECK - Start-->
        If globals.User("Anonymous") Or (Request("PaymentType") = "AUTHNET") Or (Request("PaymentType") = "ECHECK") Then
            '<!--JA2010022601 - ECHECK - End-->
            If CStrEx(Request("globals.UserGuid")) <> "" Then
                Session("UserGuid") = CStrEx(Request("globals.UserGuid"))
                globals.bUserLoaded = False
                globals.User = eis.LoadUser(Session("UserGuid"))
            End If
        End If

        ' Check for page access.
        eis.PageAccessRedirect("Order")

        ' Check for local mode
        If IsLocalMode And Not CBoolEx(AppSettings("IGNORE_LOCALMODE")) Then
            Response.Redirect("localmode.aspx", True)
        End If

        ' prepare the payment order data
        paymentFee = 0
        paymenttype = ""
        clearedamount = 0
        transact = ""
        PaymentTransactionSignature = ""

        ' Get the shopping cart
        ' Sets the global variable globals.OrderDict.
        globals.OrderDict = eis.LoadOrderDictionary(globals.User)
        'Find selected paymenttype from OrderDict
        paymenttype = DBNull2Nothing(globals.OrderDict("PaymentMethod"))

        'If paymenttype is not set check the Request 
        If paymenttype Is Nothing Then
            Select Case Request("PaymentType")
                Case "DIBS"
                    paymenttype = "DIBS"
                Case "AUTHNET"
                    paymenttype = "AUTHNET"
                    '<!--JA2010022601 - ECHECK - Start-->
                Case "ECHECK"
                    paymenttype = "ECHECK"
                    '<!--JA2010022601 - ECHECK - End-->
                    'AM2010041301 - ENTERPRISE PAYMENT GATEWAY - Start
                    'Add a case statement here for every provided needed.
                Case "EEPG"
                    paymenttype = "EEPG"
                    'AM2010041301 - ENTERPRISE PAYMENT GATEWAY - End
                    'JA2010102801 - PAPERCHECK - Start
                Case "PAPERCHECK"
                    paymenttype = "PAPERCHECK"
                    'JA2010102801 - PAPERCHECK - End
                Case Else
                    paymenttype = "INVOICE"
            End Select
        End If

        Select Case paymenttype
            Case "DIBS"
            Case "AUTHNET"
                '<!--JA2010022601 - ECHECK - Start-->
            Case "ECHECK"
                '<!--JA2010022601 - ECHECK - End-->
                'AM2010041301 - ENTERPRISE PAYMENT GATEWAY - Start
                'Add a case statement here for every provided needed.
            Case "EEPG"
                'AM2010041301 - ENTERPRISE PAYMENT GATEWAY - End
            Case Else
                redirectOnError = True
        End Select

        CartHeaderGuid = globals.OrderDict("HeaderGuid")

        If globals.IsCartEmpty Then
            ' Payment Error handler.

            If paymenttype = "DIBS" Then
                Payment_Error(Resources.Language.LABEL_ORDER_PAD_IS_EMPTY, CartHeaderGuid, paymenttype)
            Else
                Response.Redirect(VRoot & "/cart.aspx?ErrorString=" & Resources.Language.LABEL_ORDER_PAD_IS_EMPTY)
            End If
        End If

        ' Handle payments
        If paymenttype = "DIBS" Then
            DIBS_Validate(CartHeaderGuid)
            DIBS_GetData()
        ElseIf paymenttype = "AUTHNET" Then
            AuthorizeNet_Validate()
            AuthorizeNet_GetData()
            '<!--JA2010022601 - ECHECK - Start-->
        ElseIf paymenttype = "ECHECK" Then
            eCheckNet_Validate()
            eCheckNet_GetData()
            '<!--JA2010022601 - ECHECK - End-->
            'AM2010041301 - ENTERPRISE PAYMENT GATEWAY - Start
            'Handles payment
        ElseIf paymenttype = "EEPG" Then
            EEPG_GetData()
            'AM2010041301 - ENTERPRISE PAYMENT GATEWAY - End
        End If

        'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
        If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
            Dim csrObj As New USCSR(globals)
            csrObj.AssignSalesPersonGuid(globals.OrderDict)
        End If
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - End

        ' Continue with automation processing if all prior steps succeeded.
        HeaderGuid = processAutomatedTransaction(paymenttype, transact, PaymentTransactionSignature, paymentFee, redirectOnError, bMail, "OK")

        ' Makes sure confirm page is called for Authorize.Net and DIBS 
        ' (This could redirect the page, if special redirect is needed, as is the case with AUTHNET)
        If paymenttype = "DIBS" Then
            DIBS_Confirm()
        ElseIf paymenttype = "AUTHNET" Then
            AuthorizeNet_Confirm()
            '<!--JA2010022601 - ECHECK - Start-->
        ElseIf paymenttype = "ECHECK" Then
            eCheckNet_Confirm()
            '<!--JA2010022601 - ECHECK - End-->
            'AM2010041301 - ENTERPRISE PAYMENT GATEWAY - Start
            'Confirmation
        ElseIf paymenttype = "EEPG" Then
            EEPG_Confirm()
            'AM2010041301 - ENTERPRISE PAYMENT GATEWAY - End
        End If

        ' Redirect to the confirmation page.
        Response.Redirect(VRoot & "/confirmed.aspx?HeaderGuid=" & Server.UrlEncode(HeaderGuid) & "&bMail=" & Server.UrlEncode(bMail))
    End Sub

    ''' <summary>
    ''' Sets the current page to use SSL transmission only
    ''' </summary>
    ''' <param name="e"></param>
    ''' <remarks>Added 080825 to make sure the page only gets called with https</remarks>
    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
		ExpandIT.FileLogger.log("On _payment.aspx.vb, setting RequireSSL = True" )
        Me.RequireSSL = True
    End Sub

    ' Initial validation
    Private Sub DIBS_Validate(ByVal CartHeaderGuid As String)
        Dim md5test, testauthkey, testtransact, testcurrency As String
        Dim testamount As Decimal

        ' No validation needed for DIBS	
        If paymenttype = "DIBS" Then
            ' Perform MD5 key validering
            testauthkey = CStrEx(Request("authkey"))
            testtransact = CStrEx(Request("transact"))

            If testauthkey <> "" Then
                testamount = RoundEx(CDec(globals.OrderDict("TotalInclTax")) * CDec(100), 0)

                If CStrEx(Request("fee")) <> "" Then
                    testamount = CInt(CStrEx(Request("fee"))) + testamount
                End If

                testcurrency = globals.currency.GetIso4217Code(globals.OrderDict("CurrencyGuid"))

                Dim toKeyStr As String = AppSettings("DIBS_MD5_KEY_1").ToString & "transact=" & testtransact & "&amount=" & testamount & "&currency=" & testcurrency

                Dim md5d As String = ExpandIT.Crypto.coreMD5(toKeyStr)

                md5test = ExpandIT.Crypto.coreMD5(AppSettings("DIBS_MD5_KEY_2").ToString & md5d)

                If md5test <> testauthkey Then
                    Payment_Error("Authentication key does not match calculated key.", CartHeaderGuid, paymenttype)
                End If
            Else
                Payment_Error("No authentication key was received.", CartHeaderGuid, paymenttype)
            End If
        End If
    End Sub

    ' Read the DIBS specific data
    Sub DIBS_GetData()
        If paymenttype = "DIBS" Then
            transact = CStrEx(Request("transact"))
            PaymentTransactionSignature = Request("authkey")
            If CStrEx(Request("fee")) <> "" Then
                paymentFee = CInt(CStrEx(Request("fee"))) / 100
            End If
        End If
    End Sub

    ' Confirm the DIBS payment
    Sub DIBS_Confirm()
        ' As Confirm is called with callback, no need to redirect. 
        If paymenttype = "DIBS" Then
            Response.Write("DIBS Was succesfully validated.")
            Response.End()
        End If
    End Sub

    Sub AuthorizeNet_Redirect(ByVal anUrl As String)
        Response.Clear()

        Response.Write("<HTML>")
        Response.Write("<HEAD>")
        Response.Write("<META http-equiv=REFRESH Content=""0; url=" & anUrl & """>")
        Response.Write("</HEAD>")
        Response.Write("<BODY>")
        Response.Write("</BODY>")
        Response.Write("</HTML>")

        Response.End()
    End Sub

    Sub AuthorizeNet_Error(ByVal ErrorString As String)
        If ErrorString <> "" Then
            AuthorizeNet_Redirect(ShopFullPath() & "/payment.aspx?AuthorizeNet_ErrorString=" & Server.UrlEncode(ErrorString) & "#AuthorizeNet")
        End If
    End Sub


    Sub AuthorizeNet_ValidateHashValue()
        Dim MD5HashValue, LoginID, TransID, Amount As Object
        Dim x_MD5_Hash, x_MD5_Hash_Calc As Object

        MD5HashValue = AppSettings("AUTHORIZENET_MD5_HASH_VALUE")
        LoginID = AppSettings("AUTHORIZENET_LOGIN")
        '<!--JA2010022601 - ECHECK - Start-->
        '<!--JA2010022602 - AUTHORIZENET - Start-->

        'JA2011030701 - PAYMENT TABLE - START
        'TransID = globals.OrderDict("PaymentTransactionID")
        'Amount = globals.OrderDict("PaymentTransactionAmount")
        'Amount = AuthorizeNet_FormatAmount(Amount)
        'x_MD5_Hash = globals.OrderDict("PaymentTransactionSignature")
        TransID = eis.getPaymentTableColumn(globals.OrderDict("HeaderGuid"), "PaymentTransactionID")
        Amount = eis.getPaymentTableColumn(globals.OrderDict("HeaderGuid"), "PaymentTransactionAmount")
        Amount = AuthorizeNet_FormatAmount(Amount)
        x_MD5_Hash = eis.getPaymentTableColumn(globals.OrderDict("HeaderGuid"), "PaymentTransactionSignature")
        'JA2011030701 - PAYMENT TABLE - END
        
        '<!--JA2010022602 - AUTHORIZENET - End-->
        '<!--JA2010022601 - ECHECK - End-->
        x_MD5_Hash_Calc = UCase(ExpandIT.Crypto.coreMD5(MD5HashValue & LoginID & TransID & Amount))
        If x_MD5_Hash <> x_MD5_Hash_Calc Then
            AuthorizeNet_Error(Replace(Replace(Resources.Language.ERROR_MD5HASHVALUE_DO_NOT_MATCH, "%1", x_MD5_Hash), "%2", x_MD5_Hash_Calc))
        End If
    End Sub

    Sub AuthorizeNet_HandleError()
        Dim ErrorString As String = String.Empty
        '<!--JA2010022601 - ECHECK - Start-->
        '<!--JA2010022602 - AUTHORIZENET - Start-->
        If Request("rrcode").Length > 0 Then
            If Request("rrcode") <> "1" Then
                If Request("rrtext").Length > 0 Then
                    ErrorString = Request("rrtext")
                    '<!--JA2010022602 - AUTHORIZENET - End-->
                    '<!--JA2010022601 - ECHECK - End-->
                Else
                    ErrorString = "Authorize.Net internal error. Code = %1, Sub code = %2"
                    '<!--JA2010022601 - ECHECK - Start-->
                    '<!--JA2010022602 - AUTHORIZENET - Start-->
                    ErrorString = Replace(ErrorString, "%1", Request("rcode"))
                    ErrorString = Replace(ErrorString, "%2", Request("rscode"))
                    '<!--JA2010022602 - AUTHORIZENET - End-->
                    '<!--JA2010022601 - ECHECK - End-->
                End If
            End If
        End If
        If ErrorString <> "" Then AuthorizeNet_Error(ErrorString)
    End Sub

    Sub AuthorizeNet_GetUser()
        If Request("PaymentType") = "AUTHNET" Then
            Session("UserGuid") = Request("globals.UserGuid")
        End If
    End Sub

    ' Validate
    Sub AuthorizeNet_Validate()
        If Request("PaymentType") = "AUTHNET" Then
            '<!--JA2010022601 - ECHECK - Start-->
            '<!--JA2010022602 - AUTHORIZENET - Start-->
            Response.Write("<p>Data</p>") ' This is the "funky" response that will keep Authorize.net happy...
            '<!--JA2010022602 - AUTHORIZENET - End-->
            '<!--JA2010022601 - ECHECK - End-->
            AuthorizeNet_ValidateHashValue()
            AuthorizeNet_HandleError()
            AuthorizeNet_GetUser()
        End If
    End Sub

    ' Process
    Sub AuthorizeNet_GetData()
        If Request("PaymentType") = "AUTHNET" Then
            paymenttype = "AUTHNET"
            '<!--JA2010022601 - ECHECK - Start-->
            '<!--JA2010022602 - AUTHORIZENET - Start-->

            'JA2011030701 - PAYMENT TABLE - START
            'transact = globals.OrderDict("PaymentTransactionID")
            'PaymentTransactionSignature = globals.OrderDict("PaymentTransactionSignature")
            transact = eis.getPaymentTableColumn(globals.OrderDict("HeaderGuid"), "PaymentTransactionID")
            PaymentTransactionSignature = eis.getPaymentTableColumn(globals.OrderDict("HeaderGuid"), "PaymentTransactionSignature")
            'JA2011030701 - PAYMENT TABLE - END

            '<!--JA2010022602 - AUTHORIZENET - End-->
            '<!--JA2010022601 - ECHECK - End-->
        End If
    End Sub

    ' Done
    ' Redirect to the full path as the _payment.asp is being relayed by Authorize.Net
    Sub AuthorizeNet_Confirm()
        If Request("PaymentType") = "AUTHNET" Then
            AuthorizeNet_Redirect(ShopFullPath() & "/confirmed.aspx?HeaderGuid=" & HeaderGuid)
        End If
    End Sub
    '<!--JA2010022601 - ECHECK - Start-->
    '<!--JA2010022602 - AUTHORIZENET - Start-->
    Function AuthorizeNet_FormatAmount(ByVal aValue As Double) As String
        Return Replace(FormatNumber(aValue, -1, -1, 0, 0), ",", ".")
    End Function
    '<!--JA2010022602 - AUTHORIZENET - End-->
    '<!--JA2010022601 - ECHECK - End-->

    '<!--JA2010022601 - ECHECK - Start-->
    ' Validate
    Sub eCheckNet_Validate()
        If Request("PaymentType") = "ECHECK" Then
            Response.Write("<p>Data</p>") ' This is the "funky" response that will keep Authorize.net happy...
            eCheckNet_ValidateHashValue()
            eCheckNet_HandleError()
            eCheckNet_GetUser()
        End If
    End Sub

    ' Process
    Sub eCheckNet_GetData()
        If Request("PaymentType") = "ECHECK" Then
            paymenttype = "ECHECK"

            'JA2011030701 - PAYMENT TABLE - START
            'transact = globals.OrderDict("PaymentTransactionID")
            'PaymentTransactionSignature = globals.OrderDict("PaymentTransactionSignature")
            transact = eis.getPaymentTableColumn(globals.OrderDict("HeaderGuid"), "PaymentTransactionID")
            PaymentTransactionSignature = eis.getPaymentTableColumn(globals.OrderDict("HeaderGuid"), "PaymentTransactionSignature")
            'JA2011030701 - PAYMENT TABLE - END

        End If
    End Sub

    ' Done
    ' Redirect to the full path as the _payment.asp is being relayed by Authorize.Net
    Sub eCheckNet_Confirm()
        If Request("PaymentType") = "ECHECK" Then
            AuthorizeNet_Redirect(ShopFullPath() & "/confirmed.aspx?HeaderGuid=" & HeaderGuid)
        End If
    End Sub


    Sub eCheckNet_Redirect(ByVal anUrl As String)
        Response.Clear()

        Response.Write("<HTML>")
        Response.Write("<HEAD>")
        Response.Write("<META http-equiv=REFRESH Content=""0; url=" & anUrl & """>")
        Response.Write("</HEAD>")
        Response.Write("<BODY>")
        Response.Write("</BODY>")
        Response.Write("</HTML>")

        Response.End()
    End Sub

    Sub eCheckNet_Error(ByVal ErrorString As String)
        If ErrorString <> "" Then
            eCheckNet_Redirect(ShopFullPath() & "/payment.aspx?AuthorizeNet_ErrorString=" & Server.UrlEncode(ErrorString) & "#AuthorizeNet")
        End If
    End Sub


    Sub eCheckNet_ValidateHashValue()
        Dim MD5HashValue, LoginID, TransID, Amount As Object
        Dim x_MD5_Hash, x_MD5_Hash_Calc As Object
        MD5HashValue = AppSettings("AUTHORIZENET_MD5_HASH_VALUE")
        LoginID = EISClass.getAuthorizeNetLogin()
        'LoginID = AppSettings("AUTHORIZENET_LOGIN")

        'JA2011030701 - PAYMENT TABLE - START
        'TransID = globals.OrderDict("PaymentTransactionID")
        'Amount = globals.OrderDict("PaymentTransactionAmount")
        'Amount = AuthorizeNet_FormatAmount(Amount)
        'x_MD5_Hash = globals.OrderDict("PaymentTransactionSignature")
        TransID = eis.getPaymentTableColumn(globals.OrderDict("HeaderGuid"), "PaymentTransactionID")
        Amount = eis.getPaymentTableColumn(globals.OrderDict("HeaderGuid"), "PaymentTransactionAmount")
        Amount = AuthorizeNet_FormatAmount(Amount)
        x_MD5_Hash = eis.getPaymentTableColumn(globals.OrderDict("HeaderGuid"), "PaymentTransactionSignature")
        'JA2011030701 - PAYMENT TABLE - END

        x_MD5_Hash_Calc = UCase(ExpandIT.Crypto.coreMD5(MD5HashValue & LoginID & TransID & Amount))
        If x_MD5_Hash <> x_MD5_Hash_Calc Then
            eCheckNet_Error(Replace(Replace(Resources.Language.ERROR_MD5HASHVALUE_DO_NOT_MATCH, "%1", x_MD5_Hash), "%2", x_MD5_Hash_Calc))
        End If
    End Sub

    Sub eCheckNet_HandleError()
        Dim ErrorString As String = String.Empty

        If Request("rrcode").Length > 0 Then
            If Request("rrcode") <> "1" Then
                If Request("rrtext").Length > 0 Then
                    ErrorString = Request("rrtext")
                Else
                    ErrorString = "Authorize.Net internal error. Code = %1, Sub code = %2"
                    ErrorString = Replace(ErrorString, "%1", Request("rcode"))
                    ErrorString = Replace(ErrorString, "%2", Request("rscode"))
                End If
            End If
        End If
        If ErrorString <> "" Then eCheckNet_Error(ErrorString)
    End Sub

    Sub eCheckNet_GetUser()
        If Request("PaymentType") = "ECHECK" Then
            Session("UserGuid") = Request("globals.UserGuid")
        End If
    End Sub

    '<!--JA2010022601 - ECHECK - End-->

    'AM2010041301 - ENTERPRISE PAYMENT GATEWAY - Start
    'GetData and confirm functions.
    ' Process
    Sub EEPG_GetData()
        If CStrEx(Request("PaymentType")) <> "" Then

            paymenttype = CStrEx(Request("PaymentType"))
            transact = Request("PaymentTransactionID")
        End If
    End Sub

    Sub EEPG_Confirm()
        If Request("PaymentType") <> "" Then
            Response.Redirect(VRoot & "/confirmed.aspx?HeaderGuid=" & Server.UrlEncode(HeaderGuid) & "&bMail=" & Server.UrlEncode(bMail))
        End If
    End Sub
    'AM2010041301 - ENTERPRISE PAYMENT GATEWAY - End
End Class
