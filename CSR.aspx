<%--AM2011031801 - ENTERPRISE CSR STAND ALONE - Start--%>
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CSR.aspx.vb" Inherits="CSR"
    CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master"
    RuntimeMasterPageFile="ThreeColumn.master" CrumbName="<%$ Resources: Language, LABEL_MENU_CSR %>" %>

<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <div class="AccountPage">
        <uc1:PageHeader ID="PageHeader1" runat="server" Text="<%$ Resources: Language, LABEL_MENU_CSR %>"
            EnableTheming="true" />
        <ul>
            <li>
                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="CSRNotes.aspx" Text="<%$ Resources: Language, LABEL_CSR_NOTES %>"></asp:HyperLink>
            </li>
            <li>
                <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="history.aspx?CSR=1&Status=previous" Text="<%$ Resources: Language, LABEL_ORDER_HISTORY %>"></asp:HyperLink>
            </li>
            <%--JA2010092201 - RECURRING ORDERS - START--%>
            <li>
                <asp:HyperLink ID="HyperLink6" runat="server" NavigateUrl="history.aspx?CSR=1&Status=recurrency" Text="<%$ Resources: Language, LABEL_ORDER_HISTORY_RECURRING_ORDERS %>"></asp:HyperLink>
            </li> 
            <%--JA2010092201 - RECURRING ORDERS - END--%>           
            <li>
                <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="history_lookup.aspx?CSR=1" Text="<%$ Resources: Language, LINK_LOOKUP_ORDER_REFERENCE %>"></asp:HyperLink>
            </li>
            <li>
                <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="history.aspx?CSR=1&Status=cancell" Text="<%$ Resources: Language, LABEL_PREVIOUS_ORDER_CANCELL %>"></asp:HyperLink>
            </li>
            <li>
                <asp:HyperLink ID="HyperLink8" runat="server" NavigateUrl="history.aspx?CSR=1&park=park" Text="<%$ Resources: Language, LABEL_PARK_ORDERS %>"></asp:HyperLink>
            </li>
            <li>
                <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="PromoCodes.aspx" Text="<%$ Resources: Language, LABEL_PROMO_TITLE %>"></asp:HyperLink>
            </li>
        </ul>
    </div>
</asp:Content>
<%--AM2011031801 - ENTERPRISE CSR STAND ALONE - End--%>