<%@ Import Namespace="System.Configuration.ConfigurationManager" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="user_no_login.aspx.vb" Inherits="user_no_login"
    CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master"
    RuntimeMasterPageFile="ThreeColumn.master" CrumbName="<%$ Resources: Language, LABEL_MENU_LOGIN %>" %>

<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<%@ Register Namespace="ExpandIT.Buttons" TagPrefix="exbtn" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <div class="UserNoLoginPage">
        <uc1:pageheader id="PageHeader1" runat="server" text="<%$ Resources: Language, LABEL_MENU_LOGIN %>"
            enabletheming="true" />
        <p>
            <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources: Language, LABEL_INVALID_PASSWORD_AND_LOGIN %>"></asp:Literal>
        </p>
        <p>
            <exbtn:ExpLinkButton ID="lbLogIn" PostBackUrl="user_login.aspx" style="text-decoration:none;" runat="server" LabelText="LABEL_MENU_LOGIN" />
        </p>
    </div>
</asp:Content>
