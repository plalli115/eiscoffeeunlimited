﻿<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="notes.aspx.vb" Inherits="notes" 
    CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master" 
    RuntimeMasterPageFile="ThreeColumn.master" CrumbName="<%$ Resources: Language, LABEL_NOTES %>" %>
    
  <%@ Register Src="~/controls/NoteList.ascx" TagName="NoteList" TagPrefix="uc1" %>
  <%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
  <%@ Register src="~/controls/Message.ascx" tagname="Message" tagprefix="uc1" %>


  <asp:content id="Content1" contentplaceholderid="cphSubMaster" runat="Server">
      <div class="NotesPage">
          <uc1:Message ID="Message1" runat="server" />
          <uc1:PageHeader ID="PageHeader1" runat="server" text="<%$ Resources: Language, LABEL_NOTES %>" EnableTheming="true" />
          <uc1:NoteList ID="NoteList1" runat="server" EnableTheming="true" />
      </div>
      <p></p>
      <a href="javascript:history.back()"><% =Resources.Language.LABEL_BACK%></a>
  </asp:content>