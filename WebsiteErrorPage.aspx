<%@ Page Language="VB" AutoEventWireup="false" CodeFile="WebsiteErrorPage.aspx.vb" Inherits="WebsiteErrorPage"
    CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master"
    RuntimeMasterPageFile="main.master" %>

<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphRoot" runat="Server">
    <table>
        <tr>
            <td>
                There was an error on the website.
                <br />
                Click <a href="shop.aspx">here</a> to go back to the home page.
            </td>
        </tr>
    </table>
</asp:Content>
