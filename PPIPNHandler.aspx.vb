Imports System.Net
Imports System.IO
Imports System.Configuration.ConfigurationManager
Imports System.Data
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT.GlobalsClass
Imports ExpandIT.ShippingPayment
Imports System.Collections.Generic

''' <summary>
'''1. Notification Validation 
'''   In this step, your job is to post the data that was posted to you back to PayPal. 
'''   This ensures that the original post indeed came from PayPal, and is not sort of fraud. 
'''   In the data that you post back, you must append the "cmd=_notify-validate" value to the POST string. 
'''   Once the data is posted back to PayPal, PayPal will respond with a string of either "VERIFIED" or "INVALID". 
'''   "VERIFIED" means that PayPal sent the original post, and that you should continue your automation process as part of the transaction. 
'''   "INVALID" means that PayPal did not send the original post, and it should probably be logged and investigated for possible fraud. 
'''
'''2. Payment Status Check 
'''   In this step, you should check that the "payment_status" form field is "Completed". This ensures that 
'''   the customer's payment has been processed by PayPal, and it has been added to the seller's account. 
'''
'''3. Transaction Duplication Check 
'''   In this step, you should check that the "txn_id" form field, transaction ID, has not already been processed by your
'''   automation system. A good thing to do is to store the transaction ID in a database or file for duplication checking. 
'''   If the transaction ID posted by PayPal is a duplicate, you should not continue your automation process for this transaction. 
'''   Otherwise, this could result in sending the same product to a customer twice. 
'''
'''4. Seller Email Validation 
'''   In this step, you simply make sure that the transaction is for your account. Your account will have specific email addresses assigned to it. 
'''   You should verify that the "receiver_email" field has a value that corresponds to an email associated with your account. 
'''
'''5. Payment Validation 
'''   As of now, this step is not listed on other sites as a requirement, but it is very important. Because any customer who is 
'''   familiar with query strings can modify the cost of a seller's product, you should verify that the "payment_gross" field 
'''   corresponds with the actual price of the item that the customer is purchasing. It is up to you to determine the exact price 
'''   of the item the customer is purchasing using the form fields. Some common fields you may use to lookup the item being purchased
'''   include "item_name" and "item_number". To see for yourself, follow these steps: 
'''   Click on a button used to purchase one of your products. 
'''   Locate the "payment_gross" field in the query string and change its value. 
'''   Use the changed URL and perform a re-request, typically by hitting [ENTER] in the browser Address Bar. 
'''   Notice the changed price for your product on the PayPal order page.
''' </summary>
''' <remarks>
'''   Please note that this class requires remote HTTP access permission to work.
'''   This is not granted by default in trust level medium. 
'''</remarks>
Partial Class PPIPNHandler
    Inherits _PaymentPage

    Private currency_code As String
    Private business As String = AppSettings("BusinessEmail")

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim fee As Decimal = 0
        Dim bMail As Boolean
        Dim verificationStatus As String = String.Empty

        If processPayPalIPN(verificationStatus) Then
            processAutomatedTransaction("PayPal", Request("txn_id"), Request("payer_id"), False, bMail, verificationStatus)
        End If
    End Sub

    Private Function processPayPalIPN(ByRef paymentStatus As String) As Boolean
        ' Load User with value from PayPal
        Session("UserGuid") = Request("custom")
        globals.User = eis.LoadUser(Session("UserGuid"))
        ' Load OrderDict belonging to the current user
        globals.OrderDict = eis.LoadOrderDictionary(globals.User)
        ' Get the current users currencycode
        currency_code = CStr(globals.OrderDict("CurrencyGuid"))
        ' Get CartHeader
        Dim CartHeaderGuid As String = globals.OrderDict("HeaderGuid")
        ' Get amount from cart
        Dim amount As String = globals.OrderDict("TotalInclTax")
        ' Get CustomerReference from cart
        Dim cref As String = globals.OrderDict("CustomerReference")

        ' get the URL to work with
        Dim URL As String
        If AppSettings("UseSandbox").ToString = "true" Then
            URL = "https://www.sandbox.paypal.com/cgi-bin/webscr"
        Else
            URL = "https://www.paypal.com/cgi-bin/webscr"
        End If

        ' Step 1a: Modify the POST string.
        Dim formPostData As String = Encoding.ASCII.GetString(Request.BinaryRead(Request.ContentLength))
        formPostData += "&cmd=_notify-validate"

        ' Step 1b: POST the data back to PayPal.
        Dim client As WebClient = New WebClient()
        client.Headers.Add("Content-Type", "application/x-www-form-urlencoded")
        Dim postByteArray As Byte() = Encoding.ASCII.GetBytes(formPostData)
        Server.ClearError()
        Dim responseArray As Byte()
        Try
            responseArray = client.UploadData(URL, "POST", postByteArray)
        Catch ex As Exception
            responseArray = Encoding.ASCII.GetBytes("UNVERIFIED")
        End Try

        Dim response As String = Encoding.ASCII.GetString(responseArray)

        ' Step 1c: Process the response.
        Select Case (response)

          Case "VERIFIED", "UNVERIFIED"
            ' If httpRequest was sent to PayPal we end up here if they verify that the original call came from them.
            ' If httpRequest was not sent this is due to security settings in the machine web.config.
            ' Either way, we will continue and process the transaction. 

                ' Check if transaction has been processed before
            If IsDuplicateID(Request("txn_id")) Then
                    ' transaction already processed, return
                Return False
            End If


            If (Request("payment_status") <> "Completed" And Request("payment_status") <> "Pending") Then
                    ' Payment was denied
                Payment_Error("PAYMENT DENIED", CartHeaderGuid, "PayPal")
                Return False
            End If

                ' check customer reference
            If Request("invoice") <> cref Then
                Payment_Error("INVALID CUSTOMER REFERENCE", CartHeaderGuid, "PayPal")
                Return False
            End If

            ' Check email and transaction type
            If Request("receiver_email") <> business Or Request("txn_type") <> CStr(AppSettings("txn_type")) Then
                Payment_Error("WRONG RECEIVER EMAIL ADDRESS OR TXN-TYPE", CartHeaderGuid, "PayPal")
                Return False
            End If

            If Request("mc_gross") <> formatAmountPayPal(amount) Then
                ' amount is incorrect - Possible fraud
                Payment_Error("WRONG AMOUNT", CartHeaderGuid, "PayPal")
                Return False
            End If

            If Request("mc_currency") <> currency_code Then
                ' currency is incorrect - Possible fraud
                Payment_Error("WRONG CURRENCY", CartHeaderGuid, "PayPal")
                Return False
            End If

            ' Continue with automation processing if all prior steps succeeded.               
            paymentStatus = response
            Return True

          Case Else
                ' Possible fraud. Log for investigation.
            Payment_Error("CALL NOT VALIDATED BY PAYPAL", CartHeaderGuid, "PayPal")
            Return False
        End Select
    End Function

    Private Function formatAmountPayPal(ByVal amount As String) As String
        Return Replace(FormatNumber(amount, -1, CType(-1, TriState), 0, 0), ",", ".")
    End Function

    ''' <summary>
    ''' checking whether the current request is duplicated for the unique number of the txn_id transaction
    ''' </summary>
    ''' <param name="txn_id">the unique transaction number</param>
    ''' <returns>true if the current request has already been processed</returns>
    Public Function IsDuplicateID(ByVal txn_id As String) As Boolean

        'JA2011030701 - PAYMENT TABLE - START 
        'Dim sql As String = "SELECT HeaderGuid FROM ShopSalesHeader WHERE PaymentTransactionID = " & SafeString(txn_id)
        Dim sql As String = "SELECT HeaderGuid FROM PaymentTable WHERE PaymentTransactionID = " & SafeString(txn_id)
        'JA2011030701 - PAYMENT TABLE - END

        Dim dt As DataTable = SQL2DataTable(Sql)
        If dt.Rows.Count > 0 Then Return True
    End Function

End Class

