<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>

<h1>Error Information</h1>
<font face="Courier New" size="2">
<%
    Dim n As Integer
    Dim d As String
    Dim c As String
    Dim info As String

    n = CLngEx(Request("number"))
    d = CStrEx(Request("description"))
    c = CStrEx(Request("context"))

    info = ""

    Select Case n
        Case &H80004005
            Select Case UCase(c)
                Case "INITIALIZEDATABASE"
                    If Application("DBMSType") = "JET" Or Application("DBMSType") = "ACCESS" Then
                        info = "This error can be caused by insufficient access rights on the web database. Make sure the the web server has read and write access to the file: " & Application("WebDBFilename") & _
                        " After the permissions are fixed the global.asa file should be touched to restart the web application."
                    Else
                        info = d
                    End If
            End Select
        Case Else
    End Select
    Response.Write("<b>Context:</b> " & c & "<p>")
    Response.Write("<b>Error:</b> " & n & " (0x" & Hex(n) & ")<p>")
    Response.Write("<b>Description:</b> " & d & "<p>")
    Response.Write("<b>Info:</b> " & info & "<p>")
    Response.Write("<P/><A href=""" & VRoot & """>Home</A>")
%>
<a href="shop.aspx">shop.aspx</a>