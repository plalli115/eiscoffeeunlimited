<%@ Import Namespace="System.Configuration.ConfigurationManager" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="history_ledgerentry.aspx.vb"
    Inherits="history_ledgerentry" CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master"
    RuntimeMasterPageFile="ThreeColumn.master" CrumbName="<%$ Resources: Language, LABEL_ORDER_LEDGER %>" %>

<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">

    <script type="text/javascript">
        function PrepareEmail(strBody) {
            // SET MESSAGE VALUES
            var to = '<% =AppSettings("CUSTOMER_LEDGER_REQUEST_EMAIL_ADDRESS") %>';
            var strSubject = '<% =Resources.Language.MESSAGE_CUSTOMER_LEDGER_EMAIL_REQUEST_SUBJECT %>';

            // BUILD MAIL MESSAGE COMPONENTS
            var doc = "mailto:" + to +
			"?subject=" + strSubject +
			"&amp;body=" + strBody +
			"&amp;enctype=text/plain";

            // POP UP EMAIL MESSAGE WINDOW		
            window.location = doc;
        }
    </script>

    <div class="HistoryLedgerEntryPage">
        <uc1:PageHeader ID="PageHeader1" runat="server" Text="<%$ Resources: Language, LABEL_ORDER_LEDGER %>"
            EnableTheming="true" />
        <%
					
            If dictCuLeEn("Lines").Count = 0 Then%>
        <% =Resources.Language.MESSAGE_LEDGER_ENTRIES_NOT_AVAILABLE%>
        <%
        Else%>
        <% =Resources.Language.LABEL_CUSTOMER_LEDGER_EMAIL_INFORMATION%>
        <br />
        <br />
        <table cellspacing="0" cellpadding="1" width="100%">
            <tr class="TR1">
                <td>
                    <table>
                        <tr>
                            <td class="tdb">
                                &nbsp;<% =Resources.Language.LABEL_LEDGER_ENTRY_CREDIT_LIMIT%>:
                            </td>
                            <td class="tdr" style="text-align: right;">
                                <% =CurrencyFormatter.FormatAmount(dictCuLeEn("CreditLimitLCY"), Session("UserCurrencyGuid").ToString)%>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdb">
                                &nbsp;<% =Resources.Language.LABEL_LEDGER_ENTRY_BALANCE%>:
                            </td>
                            <td class="tdr" style="text-align: right;">
                                <% =CurrencyFormatter.FormatAmount(dictCuLeEn("BalanceAmountLCY"), Session("UserCurrencyGuid").ToString)%>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdb">
                                &nbsp;<% =Resources.Language.LABEL_LEDGER_ENTRY_CREDIT_AVAILABLE%>:
                            </td>
                            <td class="tdr" style="text-align: right;">
                                <% =CurrencyFormatter.FormatAmount(dictCuLeEn("CreditLimitLCY") - (dictCuLeEn("BalanceAmountLCY")), Session("UserCurrencyGuid").ToString)%>
                            </td>
                        </tr>
                    </table>
                </td>
                <td align="center">
                    <table border="1" cellspacing="0" cellpadding="2">
                        <tr>
                            <%  Dim i
                                For i = 0 To 3%>
                            <td style="text-align: center; padding: 3px;">
                                <% = AgingTitles(i) %>
                                <br />
                                <% = FormatCurrency(AgingAmounts(i),2) %>
                            </td>
                            <% Next%>
                        </tr>
                    </table>
                    Aging as of
                    <% =FormatDate(Now)%>
                    (showing days overdue)
                </td>
            </tr>
        </table>
        <br />
        <br />
        <div>
            <table cellpadding="0" cellspacing="0" style="width: 100%;" class="PaginationStyle">
                <tr>
                    <td align="left" style="vertical-align: middle; padding-left: 5px; width: 15%;">
                        <asp:LinkButton ID="lnkbtnPrevious" CssClass="PaginationStyle" Style="text-decoration: none;"
                            runat="server" OnClick="lnkbtnPrevious_Click" Text="<%$Resources: Language, LABEL_PAGINATION_PREVIOUS_PAGE %>"></asp:LinkButton></td>
                    <td style="vertical-align: middle; width: 25%;" align="left">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="vertical-align: middle;">
                                    <asp:Label ID="Label1" CssClass="PaginationStyle" runat="server"><%=Resources.Language.LABEL_PAGINATION_PAGE %> </asp:Label>
                                </td>
                                <td style="padding-left: 5px; padding-right: 5px;">
                                    <asp:TextBox runat="server" ID="txtPage" Style="font-family: Tahoma; padding-top: 1px;
                                        padding-bottom: 1px; text-align: center;" Width="20px" Font-Size="XX-Small" Height="10px"
                                        CssClass="PageNumber" AutoPostBack="true" OnTextChanged="txtPage_TextChanged"></asp:TextBox>
                                </td>
                                <td style="vertical-align: middle;">
                                    <asp:Label ID="lblNumPages" CssClass="PaginationStyle" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="vertical-align: middle; width: 25%;" align="center">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="vertical-align: middle;">
                                    <asp:Label ID="Label2" CssClass="PaginationStyle" runat="server"><%=Resources.Language.LABEL_PAGINATION_ITEMS_PAGE  %> </asp:Label>
                                </td>
                                <td style="padding-left: 5px;">
                                    <asp:DropDownList ID="ddlPageSize" Style="height: 16px; font-family: Tahoma; font-size: x-small;"
                                        AutoPostBack="true" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td align="right" style="vertical-align: middle; padding-right: 5px; width: 15%;">
                        <asp:LinkButton ID="lnkbtnNext" runat="server" CssClass="PaginationStyle" Style="text-decoration: none;"
                            OnClick="lnkbtnNext_Click" Text="<%$Resources: Language, LABEL_PAGINATION_NEXT_PAGE %>"></asp:LinkButton></td>
                </tr>
            </table>
        </div>
        <br />
        <asp:DataList ID="dlPaging" runat="server" OnItemCommand="dlPaging_ItemCommand" OnItemDataBound="dlPaging_ItemDataBound">
            <ItemTemplate>
                <asp:LinkButton ID="lnkbtnPaging" runat="server" CommandArgument='<%# Eval("PageIndex") %>'
                    CommandName="lnkbtnPaging" Text='<%# Eval("PageText") %>'></asp:LinkButton>
            </ItemTemplate>
        </asp:DataList>
        <asp:DataList runat="server" ID="pricelist" RepeatColumns="1" RepeatDirection="Vertical"
            CellPadding="0" CellSpacing="0" ExtractTemplateRows="true" Width="100%">
            <HeaderStyle CssClass="pricelistHeading" />
            <ItemStyle />
            <HeaderTemplate>
                <asp:Table runat="server" ID="ht" CssClass="testclass1" CellSpacing="0" CellPadding="0">
                    <asp:TableRow ID="TableRow1" runat="server">
                        <asp:TableCell Style="text-align: left;">
                            <asp:Label ID="lblProduct" runat="server" CssClass="historyHeader" Text='<%$ Resources:Language, LABEL_FIELD_LEDGER_POSTDATE %>' />
                        </asp:TableCell>
                        <asp:TableCell Style="text-align: left;">
                            <asp:Label ID="Label4" runat="server" CssClass="historyHeader" Text='<%$ Resources:Language, LABEL_FIELD_LEDGER_DOCUMENT_NO %>' />
                        </asp:TableCell>
                        <asp:TableCell Style="text-align: left;">
                            <asp:Label ID="Label6" runat="server" CssClass="historyHeader" Text='<%$ Resources:Language, LABEL_FIELD_LEDGER_DOCUMENT_TYPE %>' />
                        </asp:TableCell>
                        <asp:TableCell Style="text-align: left;">
                            <asp:Label ID="Label7" runat="server" CssClass="historyHeader" Text='<%$ Resources:Language, LABEL_FIELD_LEDGER_DOCUMENT_DESCRIPTION %>' />
                        </asp:TableCell>
                        <asp:TableCell Style="text-align: right;">
                            <asp:Label ID="Label8" runat="server" CssClass="historyHeader" Text='<%$ Resources:Language, LABEL_FIELD_LEDGER_AMOUNT %>' />
                        </asp:TableCell>
                        <asp:TableCell Style="text-align: center;">
                            <asp:Label ID="Label9" runat="server" CssClass="historyHeader" Text='<%$ Resources:Language, LABEL_FIELD_LEDGER_DOCUMENT_OPEN %>' />
                        </asp:TableCell>
                        <asp:TableCell Style="text-align: right;">
                            &nbsp;
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Table runat="server" ID="it" CellSpacing="0" CellPadding="0">
                    <asp:TableRow Height="20px">
                        <asp:TableCell Style="vertical-align: middle; text-align: left;">
                            <asp:Label ID="Label14" runat="server" Text='<%# FormatDate(Eval("Value")("PostingDate"))%>' />
                        </asp:TableCell>
                        <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start--%>
                        <%--AM2010091001 - ONLINE PAYMENTS - Start--%>
                        <asp:TableCell Style="vertical-align: middle; text-align: left;" Visible='<%# CBoolEx(AppSettings("EXPANDIT_US_USE_DRILL_DOWN_CUSTOMER_LEDGER")) And IsDrillDown(Eval("Value")("DocumentType")) %>'>
                            <a href="<%# drillDownLink(Eval("Value")("DocumentType"),Eval("Value")("DocumentGuid"),Eval("Value")("IsOpen"), Eval("Value")("Amount")) %>" ><%# HTMLEncode(Eval("Value")("DocumentGuid"))%></a>
                        </asp:TableCell>
                        <%--AM2010091001 - ONLINE PAYMENTS - End--%>
                        <asp:TableCell Style="vertical-align: middle; text-align: left;" Visible='<%# NOT CBoolEx(AppSettings("EXPANDIT_US_USE_DRILL_DOWN_CUSTOMER_LEDGER")) Or not IsDrillDown(Eval("Value")("DocumentType")) %>'>
                            <asp:Label ID="Label15" runat="server" Text='<%# HTMLEncode(Eval("Value")("DocumentGuid"))%>' />
                        </asp:TableCell>
                        <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End--%>
                        <asp:TableCell Style="vertical-align: middle; text-align: left;">
                            <asp:Label ID="Label11" runat="server" Text='<%# HTMLEncode(b2b.GetDocumentTypeDescription(CLngEx(Eval("Value")("DocumentType")))) %>' />
                        </asp:TableCell>
                        <asp:TableCell Style="vertical-align: middle; text-align: left;">
                            <asp:Label ID="Label12" runat="server" Text='<%# HTMLEncode(Eval("Value")("Description")) %>' />
                        </asp:TableCell>
                        <asp:TableCell Style="vertical-align: middle; text-align: right;">
                            <asp:Label ID="Label13" runat="server" Text='<%# CurrencyFormatter.FormatAmount(Eval("Value")("Amount"), Eval("Value")("CurrencyGuid"))%>' />
                        </asp:TableCell>
                        <asp:TableCell Style="vertical-align: middle; text-align: center;">
                             &nbsp;<input type="checkbox" <%# getCheckbox(Eval("Value")("IsOpen")) %> disabled="disabled" />
                        </asp:TableCell>
                        <asp:TableCell Style="vertical-align: middle; text-align: right;">
                            <img style="cursor: hand" onclick="<%# getImage(HTMLEncode(Eval("Value")("Description")), HTMLEncode(Eval("Value")("DocumentGuid")), HTMLEncode(Eval("Value")("DocumentType"))) %>"
                        src="<% = VRoot %>/images/email.gif" border="0" alt="<% = Resources.Language.LABEL_SEND_EMAIL_REQUEST %>" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ItemTemplate>
        </asp:DataList>
        <br />
        <div>
            <table cellpadding="0" cellspacing="0" style="width: 100%;" class="PaginationStyle">
                <tr>
                    <td align="left" style="vertical-align: middle; padding-left: 5px; width: 15%;">
                        <asp:LinkButton ID="lnkbtnPrevious2" CssClass="PaginationStyle" Style="text-decoration: none;"
                            runat="server" OnClick="lnkbtnPrevious_Click" Text="<%$Resources: Language, LABEL_PAGINATION_PREVIOUS_PAGE %>"></asp:LinkButton></td>
                    <td style="vertical-align: middle; width: 25%;" align="left">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="vertical-align: middle;">
                                    <asp:Label ID="Label3" CssClass="PaginationStyle" runat="server"><%=Resources.Language.LABEL_PAGINATION_PAGE %> </asp:Label>
                                </td>
                                <td style="padding-left: 5px; padding-right: 5px;">
                                    <asp:TextBox runat="server" ID="txtPage2" Style="font-family: Tahoma; padding-top: 1px;
                                        padding-bottom: 1px; text-align: center;" Width="20px" Font-Size="XX-Small" Height="10px"
                                        CssClass="PageNumber" AutoPostBack="true" OnTextChanged="txtPage_TextChanged"></asp:TextBox>
                                </td>
                                <td style="vertical-align: middle;">
                                    <asp:Label ID="lblNumPages2" CssClass="PaginationStyle" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="vertical-align: middle; width: 25%;" align="center">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="vertical-align: middle;">
                                    <asp:Label ID="Label5" CssClass="PaginationStyle" runat="server"><%=Resources.Language.LABEL_PAGINATION_ITEMS_PAGE  %> </asp:Label>
                                </td>
                                <td style="padding-left: 5px;">
                                    <asp:DropDownList ID="ddlPageSize2" Style="height: 16px; font-family: Tahoma; font-size: x-small;"
                                        AutoPostBack="true" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td align="right" style="vertical-align: middle; padding-right: 5px; width: 15%;">
                        <asp:LinkButton ID="lnkbtnNext2" runat="server" CssClass="PaginationStyle" Style="text-decoration: none;"
                            OnClick="lnkbtnNext_Click" Text="<%$Resources: Language, LABEL_PAGINATION_NEXT_PAGE %>"></asp:LinkButton></td>
                </tr>
            </table>
        </div>
        <br />
        <br />
        <% End If%>
        <br />
        <!-- Here ends the HTML code for the page -->
    </div>
</asp:Content>
