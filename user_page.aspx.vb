Imports System.Configuration.ConfigurationManager
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT.GlobalsClass
Imports ExpandIT
Imports ExpandIT.ExpandITLib

Partial Class user_page
    Inherits ExpandIT.Page

    Protected bShowLedger As Boolean

    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	
	Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        eis.PageAccessRedirect("HomePage")
        If Not (CBoolEx(globals.User("B2B")) Or CBoolEx(globals.User("B2C"))) Then ShowLoginPage()
        bShowLedger = CBoolEx(globals.User("B2B"))
        DataBind()
    End Sub
End Class
