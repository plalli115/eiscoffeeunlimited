<%@ Import Namespace="System.Configuration.ConfigurationManager" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="shop.aspx.vb" Inherits="shop"
    CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master"
    RuntimeMasterPageFile="ThreeColumn.master" CrumbName="<%$ Resources: Language, LABEL_EXPANDIT_INTERNET_SHOP %>" %>

<%@ Register Src="controls/Box.ascx" TagName="Box" TagPrefix="uc2" %>
<%@ Register Src="controls/LoginStatus.ascx" TagName="LoginStatus" TagPrefix="uc3" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Register Src="~/controls/MostPopular.ascx" TagName="MostPopular" TagPrefix="uc1" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<%@ Register Src="~/controls/Message.ascx" TagName="Message" TagPrefix="uc1" %>
<%--JA2010052401 - ALERTS - Start--%>
<%@ Register Src="~/controls/Alerts/Alerts.ascx" TagName="Alerts" TagPrefix="uc1" %>
<%--JA2010052401 - ALERTS - End--%>
<%--JA2010061701 - SLIDE SHOW - Start--%>
<%@ Register Src="~/controls/slideshow/GenericSlideShow.ascx" TagName="GenericSlideShow"
    TagPrefix="uc1" %>
<%--JA2010061701 - SLIDE SHOW - End--%>
<%@ Register Src="~/controls/boxes/BestSellersBox.ascx" TagName="BestSellersBox"
    TagPrefix="uc1" %>
<%--AM2010051801 - SPECIAL ITEMS - Start--%>
<%@ Register Src="~/controls/Specials.ascx" TagName="Specials" TagPrefix="uc1" %>
<%--AM2010051801 - SPECIAL ITEMS - End--%>
<%@ Register Src="~/controls/CatManSlide/CatManSlide.ascx" TagName="CatManSlide"
    TagPrefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Banner" runat="Server">
    <%If eis.CheckPageAccess("Catalog") Then %>
    <!--JA2010061701 - SLIDE SHOW - Start-->
   <%If CBoolEx(AppSettings("SHOW_SLIDE_SHOP")) Then%>
    <% If ItemsSlideShow Then%>
    <%Me.GenericSlideShow1.ImageDict = SlideDict%>
    <uc1:GenericSlideShow SlideShowWidth="100%" SlideShowHeight="200px" SlideShowPosition="1"
        SlideShowPrice="0" SlideShowName="0" SlideShowArrows="1" SlideImageHeight="175"
        SlideImageWidth="175" SlideShowFooterBar="1" ID="GenericSlideShow1" runat="server" />
    <% End If%>
    <%End If %>
    <!--JA2010061701 - SLIDE SHOW - End-->
    <%End If %>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">

    <script src="script/Geometry.js" type="text/javascript"></script>

    <script src="script/tooltip.js" type="text/javascript"></script>

    <script src="script/addToCart.js" type="text/javascript"></script>

    <div class="ShopPage" style="padding-top: 20px;">
        <uc1:PageHeader ID="PageHeader1" runat="server" Text="<%$ Resources: Language, LABEL_EXPANDIT_INTERNET_SHOP %>"
            EnableTheming="true" />
        <uc1:Message ID="Message1" runat="server" />
        <!--JA2010031101 - CATMAN SLIDE - Start-->
        <%If CBoolEx(AppSettings("SHOW_CATMAN_SLIDE")) Then%>
        <uc1:CatManSlide ID="CatManSlide1" runat="server" />
        <script type="text/javascript" src="script/slider2.js"></script>
        <%End If %>
        <!--JA2010031101 - CATMAN SLIDE - End-->
    </div>

    <script language="javascript" type="text/javascript" src="script/JSFX_ImageZoom.js"></script>

    <!--JA2010031101 - CATMAN SLIDE - Start-->
    <!--ONLOAD more than one Javascript on the same page (SlideShow / CATMAN Slide) - Start-->
    <%
        Dim onloadLink As String = "<script language=""javascript"" type=""text/javascript"" >window.onload=function(){"
        If SlideShowEnabled And str <> "" Then
            onloadLink = onloadLink & "renderBanners();"
        End If
        '<!--JA2010060101 - ALERTS - Start-->
        If AppSettings("SHOW_ALERT_POP_UP") Then
            If CStrEx(Session("LoadFirstTime")) = "" And getAlerts()  Then
                onloadLink = onloadLink & "loadScript();"
                Session("LoadFirstTime") = "True"
            End If
        End If
        '<!--JA2010060101 - ALERTS - End-->  
        onloadLink = onloadLink & "}</script>"
        Response.Write(onloadLink)
    %>
    <!--ONLOAD more than one Javascript on the same page (SlideShow / CATMAN Slide) - End-->
    <!--JA2010031101 - CATMAN SLIDE - End-->
</asp:Content>
<asp:Content ID="ContentBestSellers" ContentPlaceHolderID="ContentPlaceHolderBestSellers"
    runat="server">
    <%--JA2010030901 - PERFORMANCE -Start--%>
    <asp:PlaceHolder ID="phGetMostPopularItems" runat="server" />  
    <%--<uc1:BestSellersBox ID="BestSellersBox1" SetMethod="GetMostPopularItems" runat="server" />--%>
    <%--JA2010030901 - PERFORMANCE -End--%>
</asp:Content>
<%--AM2010051801 - SPECIAL ITEMS - Start--%>
<asp:Content ID="contentSpecials" ContentPlaceHolderID="SpecialsHolder" runat="server">
   <%If AppSettings("SHOW_SPECIALS") Then%>
    <td>
        <uc1:Specials ID="Specials1" runat="server" />
    </td>
    <%End If%>
</asp:Content>
<%--AM2010051801 - SPECIAL ITEMS - End--%>
