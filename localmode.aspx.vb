Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT.GlobalsClass

Partial Class localmode
    Inherits ExpandIT.Page

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim d As ExpDictionary

        ' Find the remote shop url
        Dim sql As String = "SELECT Value As RemoteWebURL FROM Configuration WHERE Configuration=" & SafeString("Global.Remote Web Root - URL")
        d = Sql2Dictionary(sql)
        linkRemoteShop.NavigateUrl = d("RemoteWebURL")
    End Sub

End Class
