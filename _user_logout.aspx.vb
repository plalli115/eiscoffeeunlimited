Imports System.Configuration.ConfigurationManager
Imports ExpandIT.GlobalsClass
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
Imports ExpandIT
'AM2011031801 - ENTERPRISE CSR STAND ALONE - End

Partial Class _user_logout
    Inherits ExpandIT.Page

    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' Delete users cookie
        Response.Cookies("store")("UserGuid") = ""
        Response.Cookies("store").Path = HttpContext.Current.Server.UrlPathEncode(VRoot) & "/"
        eis.UpdateMiniCartInfo(Nothing)
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
        If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
            Dim csrObj As New USCSR(globals)
            csrObj.clearCSRCookieAndSession()
        End If
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
        Response.Redirect("user_logout.aspx")
    End Sub
End Class
