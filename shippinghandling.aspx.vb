Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT.GlobalsClass
Imports System.Web
Imports System.Collections.Generic

Partial Class shippinghandling
    Inherits ExpandIT.Page

    Protected aKey, aKey2 As KeyValuePair(Of Object, Object)
    Protected dictSH As ExpDictionary
    Protected nCurrDiff, prevVal As Object

    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' Check site access
        eis.PageAccessRedirect("HomePage")

        ' Prepare context dictionaries
        If Not IsDict(Context) Then globals.Context = eis.LoadContext(globals.User)
        '*****shipping*****-start
        If Not CBoolEx(AppSettings("EXPANDIT_US_USE_SHIPPING")) Then
            ' Shipping and Handling data.
            dictSH = eis.GetShippingAndHandlingData(-1)
        End If
        '*****shipping*****-end

        ' Interval range difference, based on the actual Currency.
        nCurrDiff = currency.ConvertCurrency(0.01, globals.Context("CurrencyGuid"), globals.Context("DefaultCurrency"))
    End Sub

    ' helper function. 
    Public Function HTMLFormatSHDescription(ByVal aDescription As String) As String
        HTMLFormatSHDescription = Replace(aDescription, vbCrLf, "<br />")
    End Function
End Class
