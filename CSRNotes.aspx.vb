'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT.GlobalsClass
Imports ExpandIT.ExpandITLib

Partial Class CSRNotes
    Inherits ExpandIT.Page

    Protected csrObj As USCSR

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        ' Check the page access.
        eis.PageAccessRedirect("Order")

        ' Check if customer is registered
        If globals.User("Anonymous") Then
            Response.Redirect("user_login.aspx?returl=" & Server.UrlEncode(ToString(Request.ServerVariables("SCRIPT_NAME"))))
        End If
        csrObj = New USCSR(globals)
        If Not globals.User Is Nothing Then
            bindGridView1()
            Me.pnlCSRNotes.Visible = True
        Else
            Me.pnlCSRNotes.Visible = False
        End If

    End Sub

    Protected Sub btnSaveNote_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveNote.Click
        Dim Note As String = ""
        Dim userGuid As String = ""
        Dim guid As String = ""
        Dim sql As String = ""
        Dim Note2Insert As String = ""
        Dim lineNo As Integer = 0
        Dim lineGuid As String = ""
        Dim NoteDate As DateTime = DateTime.Now

        Note = CStrEx(Me.txbNewNote.Text)
        userGuid = CStrEx(globals.User("UserGuid"))
        guid = GenGuid()

        If Note <> "" And userGuid <> "" Then
            Note = Trim(Note)
            Note2Insert = Mid(Note, 1, 80)
            Note = Mid(Note, 81)
            lineNo = 10000
            lineGuid = GenGuid()

            While Note2Insert <> ""
                sql = "INSERT INTO [CSRNotes] VALUES(" & SafeString(lineGuid) & ",1," & SafeString(NoteDate) & "," & SafeString(Note2Insert) & "," & SafeString(guid) & "," & _
                    lineNo & "," & SafeString(csrObj.getSalesPersonGuid()) & "," & SafeString(userGuid) & ")"
                excecuteNonQueryDb(sql)
                Note2Insert = Mid(Note, 1, 80)
                Note = Mid(Note, 81)
                lineNo += 10000
                lineGuid = GenGuid()
            End While
            Me.txbNewNote.Text = ""
        End If
        bindGridView1()
        Me.pnlCSRNotes.Visible = True
    End Sub

    Public Sub bindGridView1()
        Dim NotesDict As ExpDictionary
        Dim sql As String
        Dim note As String = ""
        Dim newNotesDict As New ExpDictionary
        Dim prevNoteGuid As String = ""

        sql = "SELECT [NoteGuid],[LineGuid],[LineNumber],[CreationDate],[CSRNotes].[SalesPersonGuid],[CSRNotes].[UserGuid],[Note],[ContactName] FROM [CSRNotes] INNER JOIN [UserTableB2B] ON [CSRNotes].[SalesPersonGuid]=[UserTableB2B].[SalesPersonGuid] WHERE ([CSRNotes].[UserGuid] = " & SafeString(CStrEx(globals.User("UserGuid"))) & ")  ORDER BY  CreationDate DESC, NoteGuid, LineNumber"
        NotesDict = SQL2Dicts(sql, "LineGuid")

        For Each noteDict As ExpDictionary In NotesDict.Values
            If prevNoteGuid <> noteDict("NoteGuid") Then
                'It's a new note, add it to the dictionary
                newNotesDict(noteDict("NoteGuid")) = noteDict
                'Check if there is anything on notes, that would mean this is not the first note, 
                'so the value for the previous note needs to be added to the dictionary
                If note <> "" Then
                    newNotesDict(prevNoteGuid)("Note") = note
                End If
                'Assign the new note portion to the note variable, to start building a new note
                note = noteDict("Note")
                prevNoteGuid = noteDict("NoteGuid")
            Else
                'It's a note that we started building, add the note portion to the note variable
                note &= noteDict("Note")
            End If
        Next
        If note <> "" And prevNoteGuid <> "" Then
            newNotesDict(prevNoteGuid)("Note") = note
        End If

        If CIntEx(newNotesDict.Count) > 0 Then
            dlNotes.DataSource = newNotesDict
            dlNotes.DataBind()
        End If

    End Sub
End Class
'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
