<%@ Import Namespace="System.Configuration.ConfigurationManager" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="history.aspx.vb" Inherits="history"
    CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master"
    RuntimeMasterPageFile="ThreeColumn.master" CrumbName="<%$ Resources: Language, LABEL_HISTORY_INFORMATION %>" %>

<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <div class="HistoryPage">
        <uc1:PageHeader ID="PageHeader1" runat="server" Text="<%$ Resources: Language, LABEL_HISTORY_INFORMATION %>"
            EnableTheming="true" />
        <br />
        <div>
            <%--AM2010092201 - ENTERPRISE CSR - Start--%>
            <%If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And csrObj.getSalesPersonGuid() <> "" Then%>
        <table>
            <tr>
                    <td style="height: 30px;">
                        <%If CStrEx(Request("CSR")) <> "" Then%>
                        <span class="historyHeader">
                            <%=Resources.Language.LABEL_MENU_CSR %>
                        </span>
                        <%End If%>
                        <span class="historyHeader">
                            <%If CStrEx(Request("Status")) = "recurrency" Then%>
                            <%=Resources.Language.LABEL_ORDER_HISTORY_RECURRING_ORDERS%>
                            <%ElseIf CStrEx(Request("Status")) = "cancell" Then%>
                            <%=Resources.Language.LABEL_PREVIOUS_ORDER_CANCELL%>
                            <%ElseIf CStrEx(Request("park")) = "park" Then%>
                            <%=Resources.Language.LABEL_PARK_ORDERS%>
                            <%ElseIf CStrEx(Request("Status")) = "previous" Then%>
                            <%=Resources.Language.LABEL_ORDER_HISTORY%>
                            <%End If%>
                        </span>
                </td>
            </tr>
            </table>
            <%End If%>
            <%--AM2010092201 - ENTERPRISE CSR - End--%>
            <%If AppSettings("EXPANDIT_US_USE_PAGINATION") Then%>
            <table cellpadding="0" cellspacing="0" style="width: 100%;" class="PaginationStyle">
                <tr>
                    <td align="left" style="vertical-align: middle; padding-left: 5px; width: 15%;">
                        <asp:LinkButton ID="lnkbtnPrevious" CssClass="PaginationStyle" Style="text-decoration: none;"
                            runat="server" OnClick="lnkbtnPrevious_Click" Text="<%$Resources: Language, LABEL_PAGINATION_PREVIOUS_PAGE %>"></asp:LinkButton></td>
                    <td style="vertical-align: middle; width: 25%;" align="left">
                        <table cellpadding="0" cellspacing="0">
            <tr>
                                <td style="vertical-align: middle;">
                                    <asp:Label ID="Label1" CssClass="PaginationStyle" runat="server"><%=Resources.Language.LABEL_PAGINATION_PAGE %> </asp:Label>
                </td>
                                <td style="padding-left: 5px; padding-right: 5px;">
                                    <asp:TextBox runat="server" ID="txtPage" Style="font-family: Tahoma; padding-top: 1px;
                                        padding-bottom: 1px; text-align: center;" Width="20px" Font-Size="XX-Small" Height="10px"
                                        CssClass="PageNumber" AutoPostBack="true" OnTextChanged="txtPage_TextChanged"></asp:TextBox>
                </td>
                                <td style="vertical-align: middle;">
                                    <asp:Label ID="lblNumPages" CssClass="PaginationStyle" runat="server"></asp:Label>
                </td>
            </tr>
                        </table>
                </td>
                    <td style="vertical-align: middle; width: 25%;" align="center">
                        <table cellpadding="0" cellspacing="0">
            <tr>
                                <td style="vertical-align: middle;">
                                    <asp:Label ID="Label2" CssClass="PaginationStyle" runat="server"><%=Resources.Language.LABEL_PAGINATION_ITEMS_PAGE  %> </asp:Label>
                </td>
                                <td style="padding-left: 5px;">
                                    <asp:DropDownList ID="ddlPageSize" Style="height: 16px; font-family: Tahoma; font-size: x-small;"
                                        AutoPostBack="true" runat="server">
                                    </asp:DropDownList>
                </td>
            </tr>
                        </table>
                </td>
                    <td align="right" style="vertical-align: middle; padding-right: 5px; width: 15%;">
                        <asp:LinkButton ID="lnkbtnNext" runat="server" CssClass="PaginationStyle" Style="text-decoration: none;"
                            OnClick="lnkbtnNext_Click" Text="<%$Resources: Language, LABEL_PAGINATION_NEXT_PAGE %>"></asp:LinkButton></td>
            </tr>
        </table>
        <%End If%>
            <%--AM2010092201 - ENTERPRISE CSR - Start--%>
            <%If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And csrObj.getSalesPersonGuid() <> "" And CStrEx(Request("CSR")) = "1" And CStrEx(Request("Status")) = "" And CStrEx(Request("park")) = "" Then%>
                    <table>
                        <tr>
                    <td align="left" style="vertical-align: middle;">
                        <asp:Label ID="Label15" runat="server" CssClass="historyHeader" Text='<%$ Resources:Language, LABEL_ORDER_STATUS %>' />:
                            </td> 
                    <td align="left" style="vertical-align: middle;">
                        <asp:DropDownList ID="csrOrderStatus" AutoPostBack="true" runat="server">
                        </asp:DropDownList>
                            </td>
                        </tr> 
                    </table>                  
                <%End If %>
            <%--AM2010092201 - ENTERPRISE CSR - End--%>
        </div>
        <br />
        <asp:DataList ID="dlPaging" runat="server" OnItemCommand="dlPaging_ItemCommand" OnItemDataBound="dlPaging_ItemDataBound">
            <ItemTemplate>
                <asp:LinkButton ID="lnkbtnPaging" runat="server" CommandArgument='<%# Eval("PageIndex") %>'
                    CommandName="lnkbtnPaging" Text='<%# Eval("PageText") %>'></asp:LinkButton>
            </ItemTemplate>
        </asp:DataList>
        <%If Group.Count <> 0 Then%>
        <asp:DataList runat="server" ID="pricelist" RepeatColumns="1" RepeatDirection="Vertical"
            CellPadding="0" CellSpacing="0" ExtractTemplateRows="true" Width="100%">
            <HeaderStyle CssClass="pricelistHeading" />
            <ItemStyle />
            <HeaderTemplate>
                <asp:Table runat="server" ID="ht" CssClass="testclass1" CellSpacing="0" CellPadding="0">
                    <asp:TableRow ID="TableRow1" runat="server">
                        <asp:TableCell Style="text-align: center;">
                            <asp:Label ID="lblProduct" runat="server" CssClass="historyHeader" Text='<%$ Resources:Language, LABEL_ORDER %>' />
                        </asp:TableCell>
                        <%--AM2010092201 - ENTERPRISE CSR - Start--%>
                        <asp:TableCell Style="text-align: center;" ID="TableCell1">
                            <%If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And csrObj.getSalesPersonGuid() <> "" And CStrEx(Request("CSR")) <> "1" Then%>
                            <asp:Label ID="Label15" runat="server" CssClass="historyHeader" Text='<%$ Resources:Language, LABEL_MENU_CSR %>' />
                            <%End If%>
                        </asp:TableCell>
                        <asp:TableCell Style="text-align: center;" ID="TableCell5">
                            <%If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And csrObj.getSalesPersonGuid() <> "" And CStrEx(Request("CSR")) <> "1" And CStrEx(Request("park")) = "park" Then%>
                            <asp:Label ID="Label22" runat="server" CssClass="historyHeader" Text='<%$ Resources:Language, LABEL_USERDATA_CONTACT %>' />
                            <%End If%>
                        </asp:TableCell>
                        <asp:TableCell Style="text-align: center;" ID="TableCell3">
                            <%If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And csrObj.getSalesPersonGuid() <> "" And CStrEx(Request("CSR")) = "1" Then%>
                            <asp:Label ID="Label18" runat="server" CssClass="historyHeader" Text='<%$ Resources:Language, LABLE_USER %>' />
            <%End If %>
                        </asp:TableCell>
                        <asp:TableCell Style="text-align: center;" ID="tcHeaderOrderStatus">
                            <%If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And csrObj.getSalesPersonGuid() <> "" And Not CStrEx(Request("park")) = "park" And Not CStrEx(Request("Status")) = "recurrency" And Not CStrEx(Request("Status")) = "cancell" Then%>
                            <asp:Label ID="Label9" runat="server" CssClass="historyHeader" Text='<%$ Resources:Language, LABEL_ORDER_STATUS %>' />
                            <%End If%>
                        </asp:TableCell>
                        <asp:TableCell Style="text-align: center;">
                            <%If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And csrObj.getSalesPersonGuid() <> "" And CStrEx(Request("Status")) = "previous" Then%>
                            <asp:Label ID="Label21" runat="server" CssClass="historyHeader" Text='<%$ Resources:Language, LABEL_RO %>' />
                            <%End If%>
                        </asp:TableCell>
                        <asp:TableCell Style="text-align: center;">
                            <%If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And csrObj.getSalesPersonGuid() <> "" And CStrEx(Request("Status")) = "recurrency" Then%>
                            <asp:Label ID="Label19" runat="server" CssClass="historyHeader" Text='<%$ Resources:Language, LABEL_NEXT_SHIPMENT_DATE %>' />
            <%End If%>
                        </asp:TableCell>
            <%--AM2010092201 - ENTERPRISE CSR - End--%>
                        <asp:TableCell Style="text-align: center;">
                            <asp:Label ID="Label4" runat="server" CssClass="historyHeader" Text='<%$ Resources:Language, LABEL_DATE %>' />
                        </asp:TableCell>
                        <asp:TableCell Style="text-align: right;">
                            <asp:Label ID="Label7" runat="server" CssClass="historyHeader" Text='<%$ Resources:Language, LABEL_TOTAL %>' />
                        </asp:TableCell>
                        <asp:TableCell Style="text-align: right;">
                            <asp:Label ID="Label8" runat="server" CssClass="historyHeader" Text='<%$ Resources:Language, LABEL_INCLUSIVE_VAT %>' />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Table runat="server" ID="it" CellSpacing="0" CellPadding="0">
                    <asp:TableRow Height="20px">
                        <asp:TableCell Style="vertical-align: middle; text-align: center;">
                            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# getUrl(HTMLEncode(Eval("Value")("HeaderGuid")), HTMLEncode(Eval("Value")("CustomerReference")))  %>'
                                Text='<%# HTMLEncode(Eval("Value")("CustomerReference"))%>' />
                        </asp:TableCell>
                        <%--AM2010092201 - ENTERPRISE CSR - Start--%>
                        <asp:TableCell Style="vertical-align: middle; text-align: right;" ID="TableCell2">
                            <%If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And csrObj.getSalesPersonGuid() <> "" And CStrEx(Request("CSR")) <> "1" Then%>
                            <asp:Label ID="Label16" runat="server" Text='<%# HTMLEncode(CSTREX(Eval("Value")("SalesPersonGuid"))) %>' />
                            <%End If%>
                        </asp:TableCell>
                        <asp:TableCell Style="vertical-align: middle; text-align: right;" ID="TableCell6">
                            <%If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And csrObj.getSalesPersonGuid() <> "" And CStrEx(Request("CSR")) <> "1" And CStrEx(Request("park")) = "park" Then%>
                            <asp:Label ID="Label23" runat="server" Text='<%# getContactName(CSTREX(Eval("Value")("UserGuid"))) %>' />
            <%End If%>
                        </asp:TableCell>
                        <asp:TableCell Style="vertical-align: middle; text-align: center;" ID="TableCell4">
                            <%If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And csrObj.getSalesPersonGuid() <> "" And CStrEx(Request("CSR")) = "1" Then%>
                            <%#getImageUserGuid(Eval("Value")("UserGuid"))%>
                            <%End If%>
                        </asp:TableCell>
                        <asp:TableCell Style="vertical-align: middle; text-align: center;" ID="tcLineOrderStatus">
                            <%If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And csrObj.getSalesPersonGuid() <> "" And Not CStrEx(Request("park")) = "park" And Not CStrEx(Request("Status")) = "recurrency" And Not CStrEx(Request("Status")) = "cancell" Then%>
                            <asp:Label ID="Label14" runat="server" Text='<%# csrObj.getOrderStatusFromHeader(Eval("Value")("HeaderGuid")) %>' />
                            <%End If%>
                        </asp:TableCell>
                        <asp:TableCell Style="vertical-align: middle; text-align: center;">
                            <%If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And csrObj.getSalesPersonGuid() <> "" And CStrEx(Request("Status")) = "previous" Then%>
                            <asp:Image ID="Image1" Style="width: 14px;" ImageUrl='<%# isRecurrency(Eval("Value")("HeaderGuid")) %>'
                                runat="server" />
            <% End If%>
                        </asp:TableCell>
                        <asp:TableCell Style="vertical-align: middle; text-align: center;">
                            <%If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And csrObj.getSalesPersonGuid() <> "" And CStrEx(Request("Status")) = "recurrency" Then%>
                            <asp:Label ID="Label20" runat="server" Text='<%# getNextShipmentDate(HTMLEncode(Eval("Value")("HeaderGuid")), Eval("Value")("HeaderDate"))%>' />
            <%End If%>
                        </asp:TableCell>
                        <%--AM2010092201 - ENTERPRISE CSR - End--%>
                        <asp:TableCell Style="vertical-align: middle; text-align: center;">
                            <%If CStrEx(Request("park")) = "park" Then%>
                            <asp:Label ID="Label17" runat="server" Text='<%# HTMLEncode(Eval("Value")("CreatedDate"))%>' />
                            <%Else%>
                            <asp:Label ID="Label10" runat="server" Text='<%# HTMLEncode(Eval("Value")("HeaderDate"))%>' />
            <%End If %>
                        </asp:TableCell>
                        <asp:TableCell Style="vertical-align: middle; text-align: right;">
                            <asp:Label ID="Label12" runat="server" Text='<%# CurrencyFormatter.FormatCurrency(Eval("Value")("Total"), Eval("Value")("CurrencyGuid")) %>' />
                        </asp:TableCell>
                        <asp:TableCell Style="vertical-align: middle; text-align: right;">
                            <asp:Label ID="Label13" runat="server" Text='<%# CurrencyFormatter.FormatCurrency(Eval("Value")("TotalInclTax"), Eval("Value")("CurrencyGuid"))%>' />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ItemTemplate>
        </asp:DataList>
        <%Else%>
        <asp:Literal ID="litNoData" runat="server" Text="<%$ Resources:Language, LABEL_NO_HISTORY_AVAILABLE %>"></asp:Literal>
        <br />
            <%End If%>
        <%If AppSettings("EXPANDIT_US_USE_PAGINATION") Then%>
            <br />
        <div>
            <table cellpadding="0" cellspacing="0" style="width: 100%;" class="PaginationStyle">
                <tr>
                    <td align="left" style="vertical-align: middle; padding-left: 5px; width: 15%;">
                        <asp:LinkButton ID="lnkbtnPrevious2" CssClass="PaginationStyle" Style="text-decoration: none;"
                            runat="server" OnClick="lnkbtnPrevious_Click" Text="<%$Resources: Language, LABEL_PAGINATION_PREVIOUS_PAGE %>"></asp:LinkButton></td>
                    <td style="vertical-align: middle; width: 25%;" align="left">
                        <table cellpadding="0" cellspacing="0">
                <tr>
                    <td style="vertical-align:middle;">
                                    <asp:Label ID="Label3" CssClass="PaginationStyle" runat="server"><%=Resources.Language.LABEL_PAGINATION_PAGE %> </asp:Label>
                    </td>
                                <td style="padding-left: 5px; padding-right: 5px;">
                                    <asp:TextBox runat="server" ID="txtPage2" Style="font-family: Tahoma; padding-top: 1px;
                                        padding-bottom: 1px; text-align: center;" Width="20px" Font-Size="XX-Small" Height="10px"
                                        CssClass="PageNumber" AutoPostBack="true" OnTextChanged="txtPage2_TextChanged"></asp:TextBox>
                    </td>
                    <td style="vertical-align:middle;">
                                    <asp:Label ID="lblNumPages2" CssClass="PaginationStyle" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
                    </td>
                    <td style="vertical-align: middle; width: 25%;" align="center">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="vertical-align: middle;">
                                    <asp:Label ID="Label5" CssClass="PaginationStyle" runat="server"><%=Resources.Language.LABEL_PAGINATION_ITEMS_PAGE  %> </asp:Label>
                                </td>
                                <td style="padding-left: 5px;">
                                    <asp:DropDownList ID="ddlPageSize2" Style="height: 16px; font-family: Tahoma; font-size: x-small;"
                                        AutoPostBack="true" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                                </td>
                    <td align="right" style="vertical-align: middle; padding-right: 5px; width: 15%;">
                        <asp:LinkButton ID="lnkbtnNext2" runat="server" CssClass="PaginationStyle" Style="text-decoration: none;"
                            OnClick="lnkbtnNext_Click" Text="<%$Resources: Language, LABEL_PAGINATION_NEXT_PAGE %>"></asp:LinkButton></td>
                            </tr>
                        </table>
        </div>
        <br />
        <br />
                <% End If%>
        
    </div>
</asp:Content>