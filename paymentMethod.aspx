<%@ Page Language="VB" AutoEventWireup="false" CodeFile="paymentMethod.aspx.vb" Inherits="paymentMethod"
    CodeFileBaseClass="ExpandIT.ShippingPayment.ShippingPaymentPage" MasterPageFile="~/masters/default/main.master"
    RuntimeMasterPageFile="ThreeColumn.master" CrumbName="<%$ Resources: Language, LABEL_SELECT_PAYMENT_TYPE %>" %>
    
<%--AM2010080301 - CHECK OUT PROCESS MAP - Start--%>
<%@ MasterType VirtualPath="~/masters/EnterpriseBlue/ThreeColumn.master" %>
<%--AM2010080301 - CHECK OUT PROCESS MAP - End--%>

<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Register Namespace="ExpandIT.SimpleUIControls" TagPrefix="expui" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <asp:Panel CssClass="PaymentMethodPage" runat="server" ID="PaymentMethodPanel" DefaultButton="nextbutton">
        <div class="PaymentMethodPage">
            <uc1:PageHeader ID="PageHeader1" runat="server" Text="<%$ Resources: Language, LABEL_SELECT_PAYMENT_TYPE %>"
                EnableTheming="true" />
<%-- WLB.10042012            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <expui:GenericPaymentControl ID="cod_form" runat="server" Visible="false" TableWidth="450"
                            RadioButtonMode="true" RadioButtonLabelMargin="15" ButtonText="ACTION_NEXT_GREATER_THAN"
                            HeaderText="LABEL_PAYMENT_SELECT_PAYMENT_COD_HEADER" LabelText="LABEL_PAYMENT_SELECT_PAYMENT_COD" />
                    </td>
                </tr>
            </table>--%>
<%-- WLB.10042012            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <expui:GenericPaymentControl ID="cop_form" runat="server" Visible="false" TableWidth="450"
                            RadioButtonMode="true" RadioButtonLabelMargin="15" ButtonText="ACTION_NEXT_GREATER_THAN"
                            HeaderText="LABEL_PAYMENT_SELECT_PAYMENT_COP_HEADER" LabelText="LABEL_PAYMENT_SELECT_PAYMENT_COP" />
                    </td>
                </tr>
            </table>--%>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <expui:GenericPaymentControl ID="_form" runat="server" Visible="false" TableWidth="450"
                            RadioButtonMode="true" RadioButtonLabelMargin="15" ButtonText="ACTION_NEXT_GREATER_THAN"
                            HeaderText="LABEL_PAYMENT_SELECT_PAYMENT_BILL_HEADER" LabelText="LABEL_PAYMENT_SELECT_PAYMENT_BILL" />
                    </td>
                </tr>
            </table>
<%-- WLB.10042012            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <expui:GenericPaymentControl ID="vk_form" runat="server" Visible="false" TableWidth="450"
                            RadioButtonMode="true" RadioButtonLabelMargin="15" ButtonText="ACTION_NEXT_GREATER_THAN"
                            HeaderText="LABEL_PREPAYMENT" LabelText="LABEL_PAYMENT_SELECT_PAYMENT_PREPAYMENT" />
                    </td>
                </tr>
            </table>--%>
<%-- WLB.10042012            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <expui:GenericPaymentControl ID="ls_form" runat="server" Visible="false" TableWidth="450"
                            RadioButtonMode="true" RadioButtonLabelMargin="15" ButtonText="ACTION_NEXT_GREATER_THAN"
                            HeaderText="LABEL_GIRO" LabelText="LABEL_PAYMENT_SELECT_PAYMENT_GIRO" />
                    </td>
                </tr>
            </table>--%>
            <% If AppSettings("PAYMENT_PAYPAL") Then%>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <expui:PayPalPaymentControl ID="paypal_form" runat="server" Visible="false" TableWidth="450"
                            RadioButtonMode="true" RadioButtonLabelMargin="15" ButtonText="ACTION_NEXT_GREATER_THAN"
                            HeaderText="PayPal" LabelText="LABEL_PAYMENT_SELECT_PAYMENT_PAYPAL" />
                    </td>
                </tr>
            </table>
            <% End If%>
            <!-- AM2010041301 - ENTERPRISE PAYMENT GATEWAY - Start -->
            <!-- Adding the control for the EEPG payment type. Modify or add more according to the generic needs.-->
            <% If AppSettings("PAYMENT_EEPG") Then%>
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <expui:EnterprisePaymentGatewayAIMControl ID="eepg_form" runat="server" Visible="false"
                            TableWidth="450" RadioButtonMode="true" RadioButtonLabelMargin="15" ButtonText="ACTION_NEXT_GREATER_THAN"
                            HeaderText="LABEL_EEPG" LabelText="LABEL_PAYMENT_EEPG" />
                    </td>
                </tr>
            </table>
            <% End If%>
            <!-- AM2010041301 - ENTERPRISE PAYMENT GATEWAY - End -->
            <% If AppSettings("PAYMENT_DIBS") Then%>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <expui:DibsPaymentControl ID="dibs_form" runat="server" Visible="false" TableWidth="450"
                            RadioButtonMode="true" RadioButtonLabelMargin="15" ButtonText="ACTION_NEXT_GREATER_THAN"
                            HeaderText="LABEL_ARCHITRADE_DIBS" LabelText="LABEL_PAYMENT_SELECT_PAYMENT_DIBS"
                            RenderScriptBlock="false" />
                    </td>
                </tr>
            </table>
            <% End If%>
            <% If AppSettings("PAYMENT_AUTHORIZENET") Then%>
<%-- WLB.10042012            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <expui:AuthNetPaymentControl ID="authnet_form" runat="server" Visible="false" TableWidth="450"
                            RadioButtonMode="true" RadioButtonLabelMargin="15" ButtonText="ACTION_NEXT_GREATER_THAN"
                            HeaderText="LABEL_AUTHORIZE_NET" LabelText="LABEL_PAYMENT_SELECT_PAYMENT_AUTH_NET" />
                    </td>
                </tr>
            </table>--%>
            <%  End If%>
            <!--JA2010022601 - ECHECK - Start-->
            <% If AppSettings("PAYMENT_ECHECK_NET") Then%>
<%-- WLB.10042012            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <expui:AuthNetPaymentControl ID="echeck_form" runat="server" Visible="false" TableWidth="450"
                            RadioButtonMode="true" RadioButtonLabelMargin="15" ButtonText="ACTION_NEXT_GREATER_THAN"
                            HeaderText="LABEL_ECHECK_NET" LabelText="LABEL_PAYMENT_ECHECK_AUTH_NET" />
                    </td>
                </tr>
            </table>--%>
            <%  End If%>
            <!--JA2010022601 - ECHECK - End-->
            <%--JA2010102801 - PAPERCHECK - Start--%>
            <%If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And csrObj.getSalesPersonGuid() <> "" And AppSettings("PAYMENT_PAPERCHECK_NET") Then%>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <expui:GenericPaymentControl ID="papercheck_form" runat="server" Visible="false"
                            TableWidth="450" RadioButtonMode="true" RadioButtonLabelMargin="15" ButtonText="ACTION_NEXT_GREATER_THAN"
                            HeaderText="LABEL_PAPERCHECK" LabelText="LABEL_PAYMENT_SELECT_PAYMENT_PAPERCHECK" />
                    </td>
                </tr>
            </table>
            <%End If%>
            <%--JA2010102801 - PAPERCHECK - End--%>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <div id="divpaymethbutton" style="padding-top: 20px; padding-left: 10px;">
                            <asp:Button ID="nextbutton" CssClass="AddButton" runat="server" Text="Next" />
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
</asp:Content>
