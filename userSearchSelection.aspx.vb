﻿'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Web.Mail

Partial Class userSearchSelection
    Inherits ExpandIT.Page

    Protected csrObj As USCSR

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim newUserGuid As String = ""
        Dim orderHeader As String = ""

        csrObj = New USCSR(globals)
        If CStrEx(Request("OrderUserGuid")) <> "" Then
            newUserGuid = CStrEx(Request("OrderUserGuid"))
            'Load the order from the previous User
            If globals.OrderDict Is Nothing Then
                globals.OrderDict = eis.LoadOrderDictionary(globals.User)
            End If
            If Not globals.OrderDict Is Nothing Then
                'If globals.User is the CSR keep the actual order, if not delete the order from the previous user
                If CStrEx(globals.User("SalesPersonGuid")) <> csrObj.getSalesPersonGuid() Then
                    excecuteNonQueryDb("DELETE FROM CartHeader WHERE HeaderGuid=" & SafeString(globals.OrderDict("HeaderGuid")))
                    excecuteNonQueryDb("DELETE FROM CartLine WHERE HeaderGuid=" & SafeString(globals.OrderDict("HeaderGuid")))
                    globals.OrderDict = Nothing
                End If
            End If
            'Load new user
            eis.LoadUser(newUserGuid)
            Session("UserGuid") = newUserGuid
            Response.Cookies("store")("UserGuid") = Session("UserGuid")
            'AM2010092201 - ENTERPRISE CSR - Start
            If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
                HttpContext.Current.Response.Cookies("store")("CSRGuid") = HttpContext.Current.Session("SalesPersonGuid")
                eis.SetCookieExpireDate()
                'Delete any active cart from the new user
                orderHeader = CStrEx(getSingleValueDB("SELECT HeaderGuid FROM CartHeader WHERE UserGuid=" & SafeString(newUserGuid) & " AND MultiCartStatus='ACTIVE'"))
                If orderHeader <> "" Then
                  excecuteNonQueryDb("DELETE FROM CartHeader WHERE HeaderGuid=" & SafeString(orderHeader))
                  excecuteNonQueryDb("DELETE FROM CartLine WHERE HeaderGuid=" & SafeString(orderHeader))
                End If            
             End If
            'Assign the actual cart to the new user
            If Not globals.OrderDict Is Nothing Then
                globals.OrderDict("SalesPersonGuid") = csrObj.getSalesPersonGuid()
                globals.OrderDict("UserGuid") = newUserGuid
                eis.SaveOrderDictionary(globals.OrderDict)
            End If
        End If

        Response.Redirect(VRoot & "/" & Request("returl"))


    End Sub
	
	Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub

End Class
'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
