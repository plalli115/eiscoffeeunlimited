Imports System.Configuration.ConfigurationManager
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT.GlobalsClass
Imports ExpandIT
Imports System.Data.SqlClient
Imports System.Data

Partial Class _history_lookup
    Inherits ExpandIT.Page

    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' Check the page access.
        eis.PageAccessRedirect("Order")

        Dim dt As DataTable
        Dim refno As String
        Dim sql As String
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
        Dim csrObj As New USCSR(globals)
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - End

        ' Get the reference number.
        refno = Trim(Context.Items("CustomerReference"))

        ' Lookup header guid based on ref.no.
        If refno = "" Then
            globals.errstr = "<li><span class=""Error"">" & ReplaceParams(Resources.Language.WARNING_ENTER_REFNO, New Object() {refno}) & "</span></li>"
            'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
            If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
                Response.Redirect(csrObj.historyLookupWarningNoRefRedirect(CStrEx(Request("CSR")), Server.UrlEncode(globals.errstr)))
            Else
                Response.Redirect("history_lookup.aspx?errstr=" & Server.UrlEncode(globals.errstr))
            End If
            'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
        End If
        If globals.User("Anonymous") Or refno <> "" Then
            ' Find the order with the entered ref no
            sql = "SELECT HeaderGuid FROM ShopSalesHeader WHERE LOWER(CustomerReference)=" & SafeString(LCase(refno))
            dt = SQL2DataTable(sql)
            ' If the item was found then display it. Otherwise display an error 
            If dt.Rows.Count > 0 Then
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
                If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
                    Response.Redirect(csrObj.historyLookupDetailRedirect(CStrEx(Request("CSR")), Server.UrlEncode(dt.Rows(0)("HeaderGuid"))))
                Else
                    Response.Redirect("history_detail.aspx?HeaderGuid=" & Server.UrlEncode(dt.Rows(0)("HeaderGuid")))
                End If
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
            Else
                globals.messages.Warnings.Add(ReplaceParams(Resources.Language.WARNING_ORDERREF_NOT_FOUND, New Object() {refno}))
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
                If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
                    Response.Redirect(csrObj.historyLookupWarningRefNotFoundRedirect(CStrEx(Request("CSR"))))
                Else
                    Response.Redirect("history_lookup.aspx?" & globals.messages.QueryString())
                End If
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
            End If
        Else
            'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
            If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
                csrObj.historyLookupHistoryRedirect(CStrEx(Request("CSR")))
            Else
                Response.Redirect("history.aspx")
            End If
            'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
        End If
    End Sub
End Class
