'JA2011021701 - STORE LOCATOR - Start
Imports System.Configuration.ConfigurationManager
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT
Imports ExpandIT.ExpandITLib
Imports System.Web

Partial Class mapAjax
    Inherits ExpandIT.ShippingPayment._CartPage

    Protected intRadiusMiles As Double
    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Dim MaxLatitude, MaxLongitude, MinLatitude, MinLongitude

        Dim resultDict As ExpDictionary

        Dim zcdRadius = Nothing


        intRadiusMiles = CDblEx(Request("radious"))


        If CStrEx(Request("lat")) <> "" And CStrEx(Request("lon")) <> "" Then
            zcdRadius = New RadiusAssistant(CDblEx(Request("lat")), CDblEx(Request("lon")), intRadiusMiles)
        End If

        MaxLongitude = CDbl(zcdRadius.MaxLongitude)
        MinLongitude = CDbl(zcdRadius.MinLongitude)
        MaxLatitude = CDbl(zcdRadius.MaxLatitude)
        MinLatitude = CDbl(zcdRadius.MinLatitude)

        Dim strSql As String = "SELECT * FROM FranchiseLocation WHERE"
        strSql = strSql & " Latitude >= " & MinLatitude _
                           & " AND Latitude <=" & MaxLatitude _
                           & " AND Longitude >= " & MinLongitude _
                           & " AND Longitude <= " & MaxLongitude

        resultDict = Sql2Dictionaries(strSql, "FranchiseGuid")



        Dim str As String = ""
        For Each item As ExpDictionary In resultDict.Values
            str = str & "<b>" & item("FranchiseName") & "</b><br />" & item("Address1") & "<br />" & item("City") & "<br />" & item("StateName") & "<br />" & item("ZipCode") & "<br />" & item("Phone") & "<br />"
            str = str & "<br /><br />"
        Next

        Me.Label1.Text = str


    End Sub


End Class
'JA2011021701 - STORE LOCATOR - End
