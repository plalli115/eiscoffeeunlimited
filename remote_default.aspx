<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Language=VBScript %>
<HTML>
<HEAD>
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<STYLE>
BODY
{
    FONT-SIZE: 8pt;
    COLOR: #003366;
    FONT-FAMILY: Verdana
}
TABLE
{
    PADDING-RIGHT: 0px;
    PADDING-LEFT: 0px;
    FONT-SIZE: 8pt;
    PADDING-BOTTOM: 0px;
    MARGIN: 0px;
    PADDING-TOP: 0px
}
H1
{
    FONT-WEIGHT: bold;
    FONT-SIZE: 20pt
}
</STYLE>
<title>ExpandIT Internet Shop Live Web Site</title>
</HEAD>
<BODY>
<H1>ExpandIT Internet Shop Live Web Site</H1>
<TABLE>
	<TR>
		<TD>
			Welcome to the Live web site.
			<br /><br />
			This page is here to tell you that you need to synchronize the local web site with the live web site.
			<br />
			The shop will not be available on the live site until you have uploaded it.
			<br /><br />
			Start the Catalog Manager and select <B>Synchronize-&gt;Web Site-&gt;Upload</B> from the menu.
		</TD>
	</TR>
</TABLE>
</BODY>
</HTML>