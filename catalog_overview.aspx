<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="catalog_overview.aspx.vb"
    Inherits="catalog_overview" CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master" 
    RuntimeMasterPageFile="ThreeColumn.master" CrumbName="<%$ Resources: Language, LABEL_CATALOG_OVERVIEW %>" %>

<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Import Namespace="ExpandIT.GlobalsClass" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <div class="CatalogOverviewPage">
        <uc1:PageHeader ID="PageHeader1" runat="server" text="<%$ Resources: Language, LABEL_CATALOG_OVERVIEW %>" EnableTheming="true" />
        <% 
            If dictCatalogs.Count > 1 Then%>
        <% For Each item As Object In dictCatalogs.Keys%>
        <% =IIf(CStrEx(catalogguid) = CStrEx(dictCatalogs(item)("GroupGuid")), "<b>", "")%>
        <a href="catalog_overview.aspx?CatalogGuid=<% = dictCatalogs(item)("GroupGuid") %>">
            <% = dictCatalogs(item)("NAME") %>
        </a>
        <% = IIf(CStrEx(catalogguid) = CStrEx(dictCatalogs(item)("GroupGuid")), "</b>", "")%>
        &nbsp;&nbsp;
        <% Next%>
        <br />
        <br />
        <% End If%>
        <asp:Literal ID="litOverview" runat="server"></asp:Literal>
    </div>
</asp:Content>
