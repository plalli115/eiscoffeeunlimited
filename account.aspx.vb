Imports System.Configuration.ConfigurationManager
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT.GlobalsClass
Imports ExpandIT
Imports ExpandIT.ExpandITLib
Imports EEPaymentManager.EEPM

Partial Class account
    Inherits ExpandIT.Page


    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        eis.PageAccessRedirect("HomePage")
        If Not (CBoolEx(globals.User("B2B")) Or CBoolEx(globals.User("B2C"))) Then ShowLoginPage()
        Me.ledgerPanel.Visible = CBoolEx(globals.User("B2B")) AndAlso CIntEx(globals.User("UserType")) = 1
        If CBoolEx(globals.User("B2B")) Then
            HyperLink1.text = Resources.Language.LABEL_USERDATA_VIEW_USER_INFORMATION
        Else
            HyperLink1.text = Resources.Language.LABEL_USERDATA_UPDATE_USER_INFORMATION
        End If
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
        If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
            Dim csrObj As New USCSR(globals)
            csrObj.addControl2PlaceHolder(Me, phCSRAccountOptions, "~/controls/USCSR/CSRAccountOptions.ascx")
        End If
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
    End Sub


End Class
