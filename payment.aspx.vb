﻿Imports System.Configuration.ConfigurationManager
Imports ExpandIT.EISClass
Imports ExpandIT.GlobalsClass
Imports ExpandIT
Imports ExpandIT.Debug
Imports ExpandIT.ShippingPayment
Imports ExpandIT.ExpandITLib
Imports System.Collections.Generic
Imports System.Data
Imports System.Data.SqlClient
Imports ExpandIT.AuthorizeNet.Authorizenet_Simlib

Partial Class payment
    Inherits ShippingPaymentPage
    'Inherits PaymentMethodPage

    Protected dictLine As ExpDictionary
    Protected OrderDictionary, Shipping, Payment As ExpDictionary
    Protected bUseSecondaryCurrency As Boolean
    Protected dictSHProviders, dictSHProvider As ExpDictionary
    Protected strAdditionalInformation As String
    Protected txtPaymentProcessError As String = ""

    ' Used on payment.asp
    Protected CartLine As ExpDictionary
    Protected ColSpanNumber As Integer
    Protected StrId As String
    Protected sqlString As String
    Protected ret, sequence As Object

    Protected isPaymentShippingAddressLoaded As Boolean = False


    '<!-- JA2011042101 - EEPG USE EXISTING CREDIT CARDS - Start -->
    Protected objEEPG As New EEPGCreditCards()
    '<!-- JA2011042101 - EEPG USE EXISTING CREDIT CARDS - End -->


    ''' <summary>
    ''' Sets the current page to use SSL transmission only
    ''' </summary>
    ''' <param name="e"></param>
    ''' <remarks>Added 080825 to make sure the page only gets called with https</remarks>
    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub

    Protected Overloads Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender

    '<!-- JA2011042101 - EEPG USE EXISTING CREDIT CARDS - Start -->
        objEEPG = New EEPGCreditCards(globals)
    '<!-- JA2011042101 - EEPG USE EXISTING CREDIT CARDS - End -->

        ' Make sure this page expires.
        Dim pStr As String

        pStr = "private, no-cache, must-revalidate"
        Response.ExpiresAbsolute = New Date(1990 - 1 - 1)
        Response.AddHeader("pragma", "no-cache")
        Response.AddHeader("cache-control", pStr)

        ' Check the page access.
        eis.PageAccessRedirect("Order")

        If globals.User.Count < 1 Then
            If Request("returl") = "" Then
                Response.Redirect("user_login.aspx")
            Else
                Response.Redirect("user_login.aspx?returl=" & Server.UrlEncode(ToString(Request("returl"))))
            End If
        Else
            ' Get dictionary with ShippingAddress info
            bUseSecondaryCurrency = eis.UseSecondaryCurrency(globals.User)

            globals.OrderDict = eis.LoadOrderDictionary(globals.User)
            ' Always calculate the order on Payment page. This is the final step, and the order should be the final order here.
            globals.OrderDict("IsCalculated") = False
            globals.OrderDict("VersionGuid") = GenGuid()
            eis.CalculateOrder()
            ' Load information for products
            eis.CatDefaultLoadProducts(globals.OrderDict("Lines"), False, False, False, New String() {"ProductName"}, New String() {"PICTURE2"}, Nothing)

            If String.IsNullOrEmpty(DBNull2Nothing(globals.OrderDict("CustomerReference"))) Then
                globals.OrderDict("CustomerReference") = eis.GenCustRef()
            End If

            ' Read PaymentProcessStatus and clear the field.
            txtPaymentProcessError = CStrEx(globals.OrderDict("PaymentStatus"))
            globals.OrderDict("PaymentStatus") = ""

            ' Save the order dictionary
            eis.SaveOrderDictionary(globals.OrderDict)
            loadPaymentAndShippingData()
        End If

        ' read paymentfailed
        If CStrEx(Request("paymentfailed")) <> "" Then
            If txtPaymentProcessError = "" Then
                txtPaymentProcessError = Server.HtmlEncode(CStrEx(Request("paymentfailed")))
            Else
                txtPaymentProcessError = Server.HtmlEncode(CStrEx(Request("paymentfailed"))) & "</LI><LI>" & Server.HtmlEncode(txtPaymentProcessError)
            End If
        End If
        '*****shipping*****-start
        If Not CBoolEx(AppSettings("EXPANDIT_US_USE_SHIPPING")) Then
            ' Shipping and Handling information
        dictSHProviders = eis.GetShippingHandlingProviders(globals.OrderDict("ShippingHandlingProviderGuid"), False)
            If dictSHProviders.Count > 0 Then
                dictSHProvider = GetFirst(dictSHProviders).Value
            Else
                dictSHProvider = New ExpDictionary()
            End If
        End If
        '*****shipping*****-end

        ' Additional information required by danish laws
        strAdditionalInformation = Resources.Language.LABEL_PAYMENT_MORE_INFO
        strAdditionalInformation = Replace(strAdditionalInformation, "%LINK_START%", "<a href=""" & VRoot & "/info/info.aspx"">")
        strAdditionalInformation = Replace(strAdditionalInformation, "%LINK_END%", "</a>")

        ' Initialize the shifting system between TR0 and TR1 (the color shifting in the tables).
        eis.InitNextRowAB()

        'AM2010031501 - ORDER RENDER - Start
        ' Check if cart is empty
        If globals.IsCartEmpty Then
            'litOrderRender.Text = Resources.Language.LABEL_ORDER_PAD_IS_EMPTY
            Me.panelCartEmpty.Visible = True
            Me.panelCartNotEmpty.Visible = False
        Else
            'Dim bShowComment As Boolean = AppSettings("CART_COMMENT") And CStrEx(globals.OrderDict("HeaderComment")) <> ""
            'Dim bShowPONumber As Boolean = AppSettings("CART_PONUMBER") And CStrEx(globals.OrderDict("CustomerPONumber")) <> ""
            'Dim bShowShipping As Boolean = globals.User("Anonymous") Or (globals.User("B2C") And AppSettings("USE_SHIPPING_ADDRESS")) Or (globals.User("B2B") And AppSettings("B2B_USE_SHIPPING_ADDRESS"))
            Me.panelCartEmpty.Visible = False
            Me.panelCartNotEmpty.Visible = True
            'change to reflect the setting in web.config            
            'Dim taxType As Boolean = String.Equals(CType(AppSettings("SHOW_TAX_TYPE"), String), "INCL")
            'litOrderRender.Text = OrderRender.Render(globals.OrderDict, eis, taxType, bShowShipping, True, False, True, True, bShowComment, bShowPONumber, False, False, True, False, CustomerShippingProviderName, False)
            'Me.CartRender1.IsActiveOrder = True
            'Me.CartRender1.EditMode = False
            'Me.CartRender1.IsEmail = False
            Me.CartRender1.DefaultButton = Nothing
            'Me.CartRender1.ShowDate = False
            'Me.CartRender1.ShowPaymentInfo = False
            'AM2010031501 - ORDER RENDER - End
        End If
        ' ---------------------------------------------------------------
        ' DEBUG
        ' ---------------------------------------------------------------
        If globals.DebugLogic Then
            DebugValue(globals.User, "User")
            DebugValue(Shipping, "Shipping")
            DebugValue(Payment, "Payment")
            DebugValue(globals.OrderDict, "OrderDict")
        End If

    End Sub


    Protected Sub loadPaymentAndShippingData()

        If CBoolEx(AppSettings("USE_SHIPPING_ADDRESS")) Then
            Try
                Shipping = CartShippingAddress
                Payment = CartPaymentAddress
            Catch ex As Exception
                globals.messages.Errors.Add(ex.Message)
                Response.Redirect("cart.aspx", True)
            End Try
        Else
            CartShippingAddress = globals.User
            Shipping = CartShippingAddress
            Payment = globals.User
        End If

        ' Set Flag to loaded to avoid reload
        isPaymentShippingAddressLoaded = True

    End Sub

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        globals.OrderDict = eis.LoadOrderDictionary(globals.User)
        If Not globals.IsCartEmpty Then
            Dim custPaymentMethod As Object = CustomerPaymentMethod
            If custPaymentMethod IsNot Nothing Then
                Dim eh As EventHandler = New EventHandler(AddressOf BtnAccept_Click)
                Select Case Trim(CStrEx(custPaymentMethod))
                    Case "COD"
                        Me.codform.Visible = True
                        codform.clickEvent = eh
                    Case "" 'This is the PaymentMethod BILL which is an empty string in the db
                        Me.billform.Visible = True
                        billform.clickEvent = eh
                    Case "COP"
                        Me.pickupform.Visible = True
                        pickupform.clickEvent = eh
                        'JA2010022301 - New Payment Methods - Start
                    Case "VK"
                        Me.vkform.Visible = True
                        vkform.clickEvent = eh
                    Case "LS"
                        Me.lsform.Visible = True
                        lsform.clickEvent = eh
                        'JA2010022301 - New Payment Methods - End
                    Case "PAYPAL"
                        If CBoolEx(AppSettings("PAYMENT_PAYPAL")) Then
                            Me.paypalform.Visible = True
                            paypalform.clickEvent = eh
                        End If
                        'AM2010041301 - ENTERPRISE PAYMENT GATEWAY - Start
                        'Adding the payment option validation
                    Case "EEPG"
                        If CBoolEx(AppSettings("PAYMENT_EEPG")) Then
                            Me.eepgform.Visible = True
                            eepgform.clickEvent = eh
                            eepgform.cbxChangedEvent = New EventHandler(AddressOf cbxChanged)
                        End If
                        'AM2010041301 - ENTERPRISE PAYMENT GATEWAY - End
                    Case "DIBS"
                        If AppSettings("PAYMENT_DIBS") Then
                            Me.dibsform.Visible = True
                            Me.dibsform.clickEvent = eh
                        End If
                    Case "AUTHNET"
                        If AppSettings("PAYMENT_AUTHORIZENET") Then
                            Me.authnetform.Visible = True
                            authnetform.clickEvent = eh
                        End If
                        '<!--JA2010022601 - ECHECK - Start-->
                    Case "ECHECK"
                        If AppSettings("PAYMENT_ECHECK_NET") Then
                            Me.echeckform.Visible = True
                            echeckform.clickEvent = eh
                        End If
                        '<!--JA2010022601 - ECHECK - End-->
                        '<!--JA2010102801 - PAPERCHECK - Start-->
                    Case "PAPERCHECK"
                        If AppSettings("PAYMENT_PAPERCHECK_NET") Then
                            'Me.papercheckform.Visible = True
                            'papercheckform.clickEvent = eh
                            Me.btnAddPaperCheck.Visible = True
                            Me.btnAddPaperCheck.PostBackUrl = "AddPaperCheck.aspx?HeaderGuid=" & globals.OrderDict("HeaderGuid") & "&OrderNumber=" & globals.OrderDict("CustomerReference") & "&ActiveOrder=1"
                            If CStrEx(Request("DeletePaperCheck")) <> "" Then
                                excecuteNonQueryDb("DELETE FROM PaperChecks WHERE CheckGuid=" & SafeString(CStrEx(Request("DeletePaperCheck"))))
                            End If
                            Dim sql As String = "SELECT * FROM PaperChecks WHERE OrderNumber=" & SafeString(globals.OrderDict("CustomerReference"))
                            Dim checks As ExpDictionary
                            checks = SQL2Dicts(sql)
                            If Not checks Is Nothing AndAlso checks.Count > 0 Then
                                Me.PaperChecks1.Visible = True
                                Me.btnAcceptPaperChecks.Visible = True
                                Me.PaperChecks1.OrderDict = checks
                                Me.PaperChecks1.IsActiveOrder = True
                            End If
                            'papercheckform.cbxChangedEvent = New EventHandler(AddressOf cbxChanged)
                        End If
                        '<!--JA2010102801 - PAPERCHECK - End-->
                End Select
            End If
        Else 'Avoid crash when user hits the page by pressing the browser backbutton after finishing an order
            Response.Redirect("~/cart.aspx", True)
        End If

        'AM2010080301 - CHECK OUT PROCESS MAP - Start
        If CBoolEx(AppSettings("EXPANDIT_US_USE_CHECK_OUT_PROCESS_MAP")) And Not CheckOutPaths() Is Nothing Then
            If CheckOutPaths.Length > 0 Then
                Master.CheckOutProcess.CheckOutProcessDict = CheckOutPaths()
                Master.CheckOutProcess.currentLocation = getCurrentLocation()
            End If
        End If
        'AM2010080301 - CHECK OUT PROCESS MAP - End


        'JA2010092201 - RECURRING ORDERS - Start
        'hide everything but recurrency control during recurrency modification
        If CStrEx(globals.OrderDict("ModifiedRecurrency")) <> "" Then
            Me.CartRender1.isModifyRecurrency = True
        End If
        'JA2010092201 - RECURRING ORDERS - End

    End Sub

    ''' <summary>
    ''' Handles Click Event on BtnAccept          
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub BtnAccept_Click(ByVal sender As Object, ByVal e As EventArgs)

        Dim ea As ExpandIT.ExpEventArgs = CType(e, ExpandIT.ExpEventArgs)
        'AM2010041301 - ENTERPRISE PAYMENT GATEWAY - Start
        Dim ccnumber, expdate, ccv2
        Dim CCencrypted As String
        Dim dateencrypted As String
        Dim ccv2encrypted As String
        Dim FirstName As String
        Dim LastName As String
        Dim address1No As String = ""
        Dim address1St As String = ""
        'AM2010041301 - ENTERPRISE PAYMENT GATEWAY - End

        '<!--JA2010102801 - PAPERCHECK - Start-->
        'Dim paperCheckNumber As String
        'Dim paperCheckDate As String
        'Dim paperMultipleChecks As Boolean
        'Dim paperCheckTotals As Double
        'Dim paperCheckBoxTrack1 As Integer
        'Dim paperCheckBoxTrack2 As Integer
        'Dim paperCheckTrackDate As Date
        '<!--JA2010102801 - PAPERCHECK - End-->

        Select Case ea.Text
            Case codform.ID, pickupform.ID, billform.ID, vkform.ID 'JA2010022301 - New Payment Methods - Start -> vkform.ID
                Response.Redirect("_payment.aspx", True)
            Case lsform.ID
                Session("OrderDict") = globals.OrderDict
                Response.Redirect("~/DebitNote.aspx", True)
                'JA2010022301 - New Payment Methods - End
            Case paypalform.ID
                prepareContextItems()
                Server.Transfer("~/paypalpostform.aspx")
                '<!--JA2010022601 - ECHECK - Start-->
            Case echeckform.ID
                'Here begins the new code
                Dim errString As String = String.Empty
                ' The url to the authnet test payment transaction gateway
                Dim url As String = EISClass.getAuthorizeNetUrl()
                'Dim url As String = "https://test.authorize.net/gateway/transact.dll"
                'Dim url As String = "https://secure.authorize.net/gateway/transact.dll"
                'Create an instance of the AuthNetPaymentProcessor class that will handle the payment process.
                Dim paymentProcessor As New ExpandIT.EcheckNetPaymentProcessor()
                ' Process and validate the users creditcard information
                Dim result As String = paymentProcessor.EcheckNetProcessPayment(url, echeckform.RoutingNumber(), echeckform.AccountNumber(), echeckform.BankType(), echeckform.BankName(), echeckform.AssociatedName(), errString, globals.OrderDict)
                Dim results As String() = result.Split("*")
                If results(0) = "True" Then 'Validation passed
                    errString = String.Empty 'Clear error string
                    'JA2011030701 - PAYMENT TABLE - START
                    Dim echeckDict As New ExpDictionary
                    echeckDict.Add("CustomerReference", globals.OrderDict("CustomerReference"))
                    echeckDict.Add("DocumentGuid", globals.OrderDict("HeaderGuid"))
                    echeckDict.Add("PaymentType", globals.OrderDict("PaymentType"))
                    echeckDict.Add("PaymentGuid", GenGuid())
                    echeckDict.Add("PaymentReference", eis.GenCustRef())
                    echeckDict.Add("PaymentDate", DateTime.Now())
                    echeckDict.Add("UserGuid", globals.User("UserGuid"))
                    echeckDict.Add("LedgerPayment", False)
                    echeckDict.Add("PaymentTransactionID", results(1))
                    echeckDict.Add("PaymentTransactionAmount", results(2))
                    echeckDict.Add("PaymentTransactionSignature", results(3))
                    Dict2Table(echeckDict, "PaymentTable", "")
                    'Globals.OrderDict("PaymentTransactionID") = results(1)
                    'Globals.OrderDict("PaymentTransactionAmount") = results(2)
                    'Globals.OrderDict("PaymentTransactionSignature") = results(3)
                    eis.SaveOrderDictionary(globals.OrderDict)
                    'JA2011030701 - PAYMENT TABLE - END

                    Response.Redirect("_payment.aspx?globals.UserGuid=" & globals.OrderDict("UserGuid") & "&PaymentType=" & globals.OrderDict("PaymentMethod") & "&rrcode=" & results(4) & "&rrtxt=" & results(5) & "&rcode=" & results(6) & "&rscode=" & results(7), True) 'Redirect to finish order
                Else 'Validation failed
                    Response.Redirect("payment.aspx?AuthorizeNet_ErrorString=" & result) 'Show error message
                End If
                '<!--JA2010022601 - ECHECK - End-->
                '<!--JA2010022602 - AUTHORIZENET - Start-->
                '<!--JA2010102801 - PAPERCHECK - Start-->
                'Case papercheckform.ID
                '    prepareContextItems()
                '    paperCheckNumber = CStrEx(papercheckform.CheckNumber)
                '    paperCheckDate = CStrEx(papercheckform.CheckDate)
                '    paperMultipleChecks = papercheckform.MultipleChecks
                '    paperCheckTotals = papercheckform.CheckTotals
                '    paperCheckBoxTrack1 = papercheckform.BoxTrack1
                '    paperCheckBoxTrack2 = papercheckform.BoxTrack2
                '    paperCheckTrackDate = papercheckform.BoxTrackDate
                '    If (paperMultipleChecks) Then
                '        paperCheckNumber = ""
                '        paperCheckDate = ""
                '    Else
                '        If paperCheckNumber = "" Or paperCheckDate = "" Then
                '            Response.Redirect("payment.aspx?PaperCheck_ErrorString=" & Resources.Language.PAPERCHECK_ERROR) 'Show error message
                '        Else
                '            If Not validPaperCheckDate(paperCheckDate) Then
                '                Response.Redirect("payment.aspx?PaperCheck_ErrorString=" & Resources.Language.PAPERCHECK_DATE_ERROR) 'Show error message
                '            End If
                '        End If
                '    End If

                '    Dim sql As String = ""
                '    Dim salesPersonGuid As String = csrObj.getSalesPersonGuid()
                '    sql = "INSERT INTO PaperChecks VALUES (" & SafeString(GenGuid()) & "," & SafeString(CStrEx(globals.OrderDict("CustomerReference"))) & ",'PAPERCHECK'," & SafeString(paperCheckNumber) & "," & SafeString(paperCheckDate) & "," & SafeString(paperMultipleChecks) & "," & SafeString(paperCheckTotals) & "," & SafeString(paperCheckBoxTrack1) & "," & SafeString(paperCheckBoxTrack2) & "," & SafeString(paperCheckTrackDate) & "," & SafeString("Main Payment") & ",1," & SafeString(salesPersonGuid) & ")"
                '    excecuteNonQueryDb(sql)

                '    Response.Redirect("_payment.aspx", True)
                '<!--JA2010102801 - PAPERCHECK - End-->
            Case authnetform.ID
                prepareContextItems()

                ccnumber = authnetform.CCN
                CCencrypted = eis.Encrypt(ccnumber)
                expdate = authnetform.EXPDATE
                dateencrypted = eis.Encrypt(expdate)
                ccv2 = authnetform.CCV
                ccv2encrypted = eis.Encrypt(ccv2)

                'Context.Items("gUserGuid") = Session("UserGuid")
                'Server.Transfer("~/authorize_net_post_form.aspx")

                'Here begins the new code
                Dim errString As String = String.Empty
                ' The url to the authnet test payment transaction gateway
                Dim url As String = "https://test.authorize.net/gateway/transact.dll"
                'Dim url As String = "https://secure.authorize.net/gateway/transact.dll"
                'Create an instance of the AuthNetPaymentProcessor class that will handle the payment process.
                Dim paymentProcessor As New ExpandIT.AuthNetPaymentProcessor()
                ' Process and validate the users creditcard information
                Dim result As String = paymentProcessor.authNetProcessPayment(url, authnetform.CCN, authnetform.EXPDATE, "", Shipping, Payment, globals.OrderDict, errString)
                Dim results As String() = result.Split("*")
                If results(0) = "True" Then 'Validation passed
                    errString = String.Empty 'Clear error string

                    'JA2011030701 - PAYMENT TABLE - START
                    Dim authnetDict As New ExpDictionary
                    authnetDict.Add("CustomerReference", globals.OrderDict("CustomerReference"))
                    authnetDict.Add("DocumentGuid", globals.OrderDict("HeaderGuid"))
                    authnetDict.Add("PaymentType", globals.OrderDict("PaymentType"))
                    authnetDict.Add("PaymentGuid", GenGuid())
                    authnetDict.Add("PaymentReference", eis.GenCustRef())
                    authnetDict.Add("PaymentDate", DateTime.Now())
                    authnetDict.Add("UserGuid", globals.User("UserGuid"))
                    authnetDict.Add("LedgerPayment", False)
                    authnetDict.Add("PaymentTransactionID", results(1))
                    authnetDict.Add("PaymentTransactionAmount", results(2))
                    authnetDict.Add("PaymentTransactionSignature", results(3))
                    Dict2Table(authnetDict, "PaymentTable", "")
                    'Globals.OrderDict("PaymentTransactionID") = results(1)
                    'Globals.OrderDict("PaymentTransactionAmount") = results(2)
                    'Globals.OrderDict("PaymentTransactionSignature") = results(3)
                    eis.SaveOrderDictionary(globals.OrderDict)
                    'JA2011030701 - PAYMENT TABLE - END

                    Response.Redirect("_payment.aspx?globals.UserGuid=" & globals.OrderDict("UserGuid") & "PaymentType=" & globals.OrderDict("PaymentMethod") & "&rrcode=" & results(4) & "&rrtxt=" & results(5) & "&rcode=" & results(6) & "&rscode=" & results(7), True) 'Redirect to finish order
                Else 'Validation failed
                    Response.Redirect("payment.aspx?AuthorizeNet_ErrorString=" & result) 'Show error message
                End If
                '<!--JA2010022602 - AUTHORIZENET - End-->
                'AM2010041301 - ENTERPRISE PAYMENT GATEWAY - Start
            Case eepgform.ID
                prepareContextItems()

                'JA2011030701 - PAYMENT TABLE - START
                Dim eepgDict As New ExpDictionary
                eepgDict.Add("CustomerReference", globals.OrderDict("CustomerReference"))
                eepgDict.Add("DocumentGuid", globals.OrderDict("HeaderGuid"))
                eepgDict.Add("PaymentType", globals.OrderDict("PaymentType"))
                eepgDict.Add("PaymentGuid", GenGuid())
                eepgDict.Add("PaymentReference", eis.GenCustRef())
                eepgDict.Add("PaymentDate", DateTime.Now())
                eepgDict.Add("UserGuid", globals.User("UserGuid"))
                eepgDict.Add("LedgerPayment", False)
                'JA2011030701 - PAYMENT TABLE - END

                'Getting the credit card information from the form.


                '<!-- JA2011042101 - EEPG USE EXISTING CREDIT CARDS - Start -->
                If CStrEx(eepgform.decryptData) <> "" Then

                    Dim encryptObj As New EECryptography.EECrypto.EECrypto()

                    'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - Start
                    'Dim sql As String = "SELECT CCGatewayCode,CCGatewayUserLive,CCGatewayPasswordLive,CCGatewayUserTest,CCGatewayPasswordTest,CCAuthorizeOverageType,CCAuthorizeOverageAmount,CCGatewayBusinessModel,CCSimulationMode,CCCryptoPassPhrase,CCCryptoInitVector,CCCryptoSaltValue,CCCryptoPasswordIterations FROM EESetup"
                    'Dim dictGatewayConfig As ExpDictionary = Sql2Dictionary(sql)
                    'If Not dictGatewayConfig Is Nothing AndAlso Not dictGatewayConfig Is DBNull.Value AndAlso dictGatewayConfig.Count > 0 Then
                    '    encryptObj.AddNameValue("!#PASSPHRASE", CStrEx(dictGatewayConfig("CCCryptoPassPhrase")))
                    '    encryptObj.AddNameValue("!#INITIALIZATIONVECTOR", CStrEx(dictGatewayConfig("CCCryptoInitVector")))
                    '    encryptObj.AddNameValue("!#SALTVALUE", CStrEx(dictGatewayConfig("CCCryptoSaltValue")))
                    '    encryptObj.AddNameValue("!#PASSWORDITERATIONS", CStrEx(dictGatewayConfig("CCCryptoPasswordIterations")))
                    '    Dim resulDecrypt As String = encryptObj.Decrypt(CStrEx(eepgform.decryptData))

                    '    ccnumber = resulDecrypt.Substring(0, resulDecrypt.Length - 6)
                    '    Dim formatDate As String = resulDecrypt.Substring(resulDecrypt.Length - 6, 6)
                    '    expdate = formatDate.Substring(0, 2) & formatDate.Substring(4, 2)
                    '    ccv2 = CStrEx(eepgform.CCV)
                    '    FirstName = eepgform.FName
                    '    LastName = eepgform.LName
                    'Else 'Validation failed
                    '    Response.Redirect("payment.aspx?EEPG_ErrorString=" & Resources.Language.LABEL_EEPG_ERROR_DECRYPT, False) 'Show error message
                    'End If
                    Try
                        encryptObj.AddNameValue("!#PASSPHRASE", CStrEx(eis.getEnterpriseConfigurationValue("CCCryptoPassPhrase")))
                        encryptObj.AddNameValue("!#INITIALIZATIONVECTOR", CStrEx(eis.getEnterpriseConfigurationValue("CCCryptoInitVector")))
                        encryptObj.AddNameValue("!#SALTVALUE", CStrEx(eis.getEnterpriseConfigurationValue("CCCryptoSaltValue")))
                        encryptObj.AddNameValue("!#PASSWORDITERATIONS", CStrEx(eis.getEnterpriseConfigurationValue("CCCryptoPasswordIterations")))
                        Dim resulDecrypt As String = encryptObj.Decrypt(CStrEx(eepgform.decryptData))

                        ccnumber = resulDecrypt.Substring(0, resulDecrypt.Length - 6)
                        Dim formatDate As String = resulDecrypt.Substring(resulDecrypt.Length - 6, 6)
                        expdate = formatDate.Substring(0, 2) & formatDate.Substring(4, 2)
                        ccv2 = CStrEx(eepgform.CCV)
                        FirstName = eepgform.FName
                        LastName = eepgform.LName
                    Catch ex As Exception
                    End Try
                    'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - End
                Else
                    ccnumber = eepgform.CCN
                    expdate = eepgform.EXPDATE
                    ccv2 = CStrEx(eepgform.CCV)
                    FirstName = eepgform.FName
                    LastName = eepgform.LName
                End If
                '<!-- JA2011042101 - EEPG USE EXISTING CREDIT CARDS - End -->
                
                'JA2011030701 - PAYMENT TABLE - START
                'Globals.OrderDict("CCFName") = FirstName
                'Globals.OrderDict("CCLName") = LastName
                eepgDict.Add("PaymentFName", FirstName)
                eepgDict.Add("PaymentLName", LastName)
                'JA2011030701 - PAYMENT TABLE - END
                If CBoolEx(AppSettings("PAYMENT_EEPG_REQUIRE_ADDRESS_INFO")) Then
                    address1St = CStrEx(eepgform.Address1)
                    If address1St.IndexOf(" ") > 0 Then
                        address1No = Mid(address1St, 1, address1St.IndexOf(" "))
                        address1St = Mid(address1St, address1No.Length + 2)
                    ElseIf CIntEx(address1St) > 0 Then
                        'It's a number
                        address1No = CIntEx(address1St)
                        address1St = ""
                    Else
                        address1No = ""
                    End If
                    'JA2011030701 - PAYMENT TABLE - START
                    'Globals.OrderDict("CCAddress1No") = address1No
                    'Globals.OrderDict("CCAddress1St") = address1St
                    'Globals.OrderDict("CCAddress2") = CStrEx(eepgform.Address2)
                    'Globals.OrderDict("CCCity") = CStrEx(eepgform.City) 
                    'Globals.OrderDict("CCEmail") = CStrEx(eepgform.Email)
                    'Globals.OrderDict("CCState") = CStrEx(eepgform.State)
                    'Globals.OrderDict("CCPhone") = CStrEx(eepgform.Phone)
                    'Globals.OrderDict("CCCountry") = CStrEx(Globals.OrderDict("CountryGuid"))
                    eepgDict.Add("PaymentAddress1No", CStrEx(address1No))
                    eepgDict.Add("PaymentAddress1St", CStrEx(address1St))
                    eepgDict.Add("PaymentAddress2", CStrEx(eepgform.Address2))
                    eepgDict.Add("PaymentCity", CStrEx(eepgform.City))
                    eepgDict.Add("PaymentEmail", CStrEx(eepgform.Email))
                    eepgDict.Add("PaymentState", CStrEx(eepgform.State))
                    eepgDict.Add("PaymentPhone", CStrEx(eepgform.Phone))
                    eepgDict.Add("PaymentCountry", CStrEx(globals.OrderDict("CountryGuid")))
                    'JA2011030701 - PAYMENT TABLE - END
                End If

                'JA2011030701 - PAYMENT TABLE - START
                'If CBoolEx(AppSettings("PAYMENT_EEPG_REQUIRE_ZIP_CODE")) Then
                '    Globals.OrderDict("CCZipCode") = CStrEx(eepgform.ZipCode)
                'End If
                If CBoolEx(AppSettings("PAYMENT_EEPG_REQUIRE_ZIP_CODE")) Then
                    eepgDict.Add("PaymentZipCode", CStrEx(eepgform.ZipCode))
                End If
                'eis.SaveOrderDictionary(Globals.OrderDict)
                'JA2011030701 - PAYMENT TABLE - END


                ''Adding a customerreference to the order in case it doesn't already have one.
                'If String.IsNullOrEmpty(DBNull2Nothing(OrderDict("CustomerReference"))) Then
                '    OrderDict("CustomerReference") = eis.GenCustRef()
                'End If

                'Sending the information to the Payment Processor and getting the results from it.
                Dim paymentProcessor As New ExpandIT.EnterprisePaymentGatewayProcessor()
                Dim result As String = paymentProcessor.EEPGProcessPayment(eepgDict, ccnumber, expdate, ccv2, globals.OrderDict, FirstName, LastName, Request)
                Dim results As String() = result.Split("*")

                'JA2011030701 - PAYMENT TABLE - START
                'If results(0) = "True" Then 'Validation passed 
                '    Globals.OrderDict("PaymentTransactionID") = results(1) 
                '    Globals.OrderDict("PaymentTransactionAmount") = results(2)
                '    Globals.OrderDict("CCEncryptedData") = results(3)
                '    Globals.OrderDict("CCLastNumbers") = "XXXX" & Microsoft.VisualBasic.Right(CStrEx(results(4)), 4)
                '    Globals.OrderDict("CCCardType") = results(5)
                '    eis.SaveOrderDictionary(Globals.OrderDict)
                '    Response.Redirect("_payment.aspx?PaymentType=" & CStrEx(Globals.OrderDict("PaymentMethod")) & "&PaymentTransactionID=" & Globals.OrderDict("PaymentTransactionID"), False) 'Redirect to finish order
                'Else 'Validation failed
                '    Response.Redirect("payment.aspx?EEPG_ErrorString=" & result, False) 'Show error message
                'End If
                'AM2010041301 - ENTERPRISE PAYMENT GATEWAY - End
                'JA2011030701 - PAYMENT TABLE - END


                'JA2011030701 - PAYMENT TABLE - START               
                If results(0) = "True" Then 'Validation passed
                    eepgDict.Add("PaymentTransactionID", results(1))
                    eepgDict.Add("PaymentTransactionAmount", results(2))
                    eepgDict.Add("PaymentEncryptedData", results(3))
                    eepgDict.Add("PaymentLastNumbers", "XXXX" & Microsoft.VisualBasic.Right(CStrEx(results(4)), 4))
                    eepgDict.Add("PaymentCardType", results(5))
                    Dict2Table(eepgDict, "PaymentTable", "")
                    eis.SaveOrderDictionary(globals.OrderDict)
                    Response.Redirect("_payment.aspx?PaymentType=" & CStrEx(globals.OrderDict("PaymentMethod")) & "&PaymentTransactionID=" & eepgDict("PaymentTransactionID"), False) 'Redirect to finish order
                Else 'Validation failed
                    Response.Redirect("payment.aspx?EEPG_ErrorString=" & result, False) 'Show error message
                End If
                'JA2011030701 - PAYMENT TABLE - END



                'AM2010041301 - ENTERPRISE PAYMENT GATEWAY - End









        End Select

    End Sub

    Private Sub prepareContextItems()
        If Not isPaymentShippingAddressLoaded Then
            loadPaymentAndShippingData()
        End If
        Context.Items("OrderDict") = IIf(globals.OrderDict Is Nothing, eis.LoadOrderDictionary(globals.User), globals.OrderDict)
        Context.Items("Shipping") = Shipping
        Context.Items("Payment") = Payment
    End Sub

    'AM2010041301 - ENTERPRISE PAYMENT GATEWAY - Start
    ''' <summary>
    ''' Handles CheckedChanged on cbxAddress and cbxZipCode         
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub cbxChanged(ByVal sender As Object, ByVal e As EventArgs)

        If sender.id = "cbxAddress" And CBoolEx(sender.Checked()) Then
            eepgform.Address1 = CStrEx(globals.OrderDict("Address1"))
            eepgform.Address2 = CStrEx(globals.OrderDict("Address2"))
            eepgform.City = CStrEx(globals.OrderDict("CityName"))
            eepgform.State = CStrEx(globals.OrderDict("StateName"))
            eepgform.Phone = CStrEx(globals.OrderDict("PhoneNo"))
            eepgform.Email = CStrEx(globals.OrderDict("EmailAddress"))
            If AppSettings("PAYMENT_EEPG_REQUIRE_ZIP_CODE") Then
                eepgform.ZipCode = CStrEx(globals.OrderDict("ZipCode"))
            End If
            sender.focus()
        End If
        If sender.id = "cbxZipCode" And CBoolEx(sender.Checked()) Then
            eepgform.ZipCode = CStrEx(globals.OrderDict("ZipCode"))
            sender.focus()
        End If

    End Sub
    'AM2010041301 - ENTERPRISE PAYMENT GATEWAY - End

    '<!--JA2010102801 - PAPERCHECK - Start-->
    'Private Function validPaperCheckDate(ByVal checkDate As String) As Boolean
    '    Dim dateArray As String()

    '    checkDate = Trim(checkDate)
    '    If checkDate.Length = 10 Then
    '        dateArray = checkDate.Split(New Char() {"/"})
    '        If dateArray.Length = 3 Then
    '            If dateArray(0).Length = 2 Then
    '                If dateArray(1).Length = 2 Then
    '                    If dateArray(2).Length = 4 Then
    '                        If IsDate(checkDate) Then
    '                            Return True
    '                        End If
    '                    End If
    '                End If
    '            End If
    '        End If
    '    End If

    '    Return False
    'End Function
    '<!--JA2010102801 - PAPERCHECK - End-->

End Class