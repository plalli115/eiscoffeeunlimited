<%--AM2010050503 - ENTERPRISE ADMIN SECTION SQL QUERIES - Start--%>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="adminSQL.aspx.vb" Inherits="adminSQL"
    CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master"
    RuntimeMasterPageFile="ThreeColumn.master" CrumbName="SQL QUERIES" %>

<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <div class="AccountPage">
        <uc1:PageHeader ID="PageHeader1" runat="server" Text="SQL QUERIES" EnableTheming="true" />
        <div>
            <asp:CheckBox ID="cbxGetResult" runat="server" Text="Expect Result" />
            <br />
            <br />
            <asp:TextBox runat="server" ID="txtSQLQuery" TextMode="MultiLine" Width="480px"></asp:TextBox>
            <br />
            <br />
            <asp:Button CssClass="AddButton" runat="server" ID="btnAccept" Text="ACCEPT" />
            <br />
            <br />
            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
            <br />
            <br />
            <asp:SqlDataSource ID="SQLdsResult" runat="server" ConnectionString="<%$ ConnectionStrings:ExpandITConnectionString %>">
            </asp:SqlDataSource>
            <asp:Panel ID="pnlResult" runat="server" Visible="false">
                <div style="width: 550px; height: 400px; overflow: auto; zoom:1;">
                    <asp:GridView ID="grvResult" runat="server" DataSourceID="SQLdsResult" Width="100%" >
                    </asp:GridView>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<%--AM2010050503 - ENTERPRISE ADMIN SECTION SQL QUERIES - End--%>
