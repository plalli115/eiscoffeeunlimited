Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT.GlobalsClass
Imports System.Web
Imports System.Globalization

Partial Class history_ledgerentry
    Inherits ExpandIT.Page

    Protected strBody, dictItem, CustomerGuid As Object
    Protected dictCuLeEn As ExpDictionary

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            Dim cultureName As String = Threading.Thread.CurrentThread.CurrentUICulture.Name
            Dim LCID As Integer = CultureInfo.GetCultureInfo(cultureName).TextInfo.ANSICodePage
            Session.CodePage = LCID
        Catch ex As Exception
            Session.CodePage = 1252
        End Try

        ' Check the page access.
        eis.PageAccessRedirect("Order")
        If Not globals.User("B2B") Then ShowPage("account.aspx")

        ' Load Ledger entries
        dictCuLeEn = b2b.GetCustomerLedgerEntries(globals.User)

        ' Prepare stylesheet counter
        eis.InitNextRowAB()

        ' DEBUG INFORMATION COLLECTION
        If globals.DebugLogic Then
            DebugValue(globals.User, "User")
            DebugValue(dictCuLeEn, "CustomerLedgerEntries: " & CustomerGuid)
        End If

    End Sub

End Class
