'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
Imports System.Configuration.ConfigurationManager
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT.GlobalsClass
Imports ExpandIT
Imports ExpandIT.ExpandITLib

Partial Class CSR
    Inherits ExpandIT.Page


    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        eis.PageAccessRedirect("Order")
    End Sub


End Class
'AM2011031801 - ENTERPRISE CSR STAND ALONE - End