Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT.GlobalsClass

Partial Class user
    Inherits ExpandIT.Page

    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	
	Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)

        ' Check the page access.
        eis.PageAccessRedirect("OpenSite")

        ' Check for local mode
        If IsLocalMode And Not CBoolEx(AppSettings("IGNORE_LOCALMODE")) Then
            Response.Redirect("localmode.aspx", True)
        End If
        'If CBoolEx(globals.User("B2B")) Then
        ' Me.CrumbName = Resources.Language.LABEL_USERDATA_VIEW_USER_INFORMATION
        ' Else
        Me.CrumbName = Resources.Language.LABEL_USERDATA_UPDATE_USER_INFORMATION
        'End If


    End Sub

    'AM2010102901 - USER NO INFO AFTER SAVE - Start
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If CStrEx(Request("UserUpdated")) = "1" Then
            Me.CrumbName = Resources.Language.LABEL_USERDATA_USER_INFORMATION_UPDATED
        Else
            Me.CrumbName = Resources.Language.LABEL_USERDATA_UPDATE_USER_INFORMATION
        End If
    End Sub

    'AM2010102901 - USER NO INFO AFTER SAVE - End

End Class
