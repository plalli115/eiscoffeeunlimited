﻿Anfrage über das Kontaktformular Ihres Internet Shops


Kontaktinformation:

Firma: %COMPANY%
Name: %CONTACT_NAME%
Adresse: %ADDRESS1%
PLZ, Ort: %ZIPCODE_CITYNAME%
Land: %COUNTRY%
Telefon: %PHONENO%
E-Mail: %EMAIL%

Nachricht:

%CONTENT%

