<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method ="xml"   indent="yes" omit-xml-declaration ="yes"/>
	<xsl:strip-space elements ="*"/>
  <xsl:template match ="/*">
    <xsl:element name="table">
      <xsl:attribute name ="class">content_table</xsl:attribute>
      <xsl:element name ="tr">
        <xsl:element name ="th">
          <xsl:attribute name ="class">ordinal_number_header</xsl:attribute>
          <xsl:text>%ORD_NUMBER%</xsl:text>
        </xsl:element>
        <xsl:element name ="th">
          <xsl:attribute name ="class">product_number_header</xsl:attribute>
          <xsl:text>%PRODUCT_ID%</xsl:text>
        </xsl:element>
        <xsl:element name ="th">
          <xsl:attribute name ="class">product_name_header</xsl:attribute>
          <xsl:text>%PRODUCT_NAME%</xsl:text>
        </xsl:element>
        <xsl:element name ="th">
          <xsl:attribute name ="class">price_header</xsl:attribute>
          <xsl:text>%PRICE%</xsl:text>
        </xsl:element>
        <xsl:element name ="th">
          <xsl:attribute name ="class">quantity_header</xsl:attribute>
          <xsl:text>%QUANTITY%</xsl:text>
        </xsl:element>
      </xsl:element>
      <xsl:apply-templates />
    </xsl:element> 
  </xsl:template>
	<xsl:template match="/*/*">
    <xsl:element name ="tr">
      <xsl:element name ="td">
        <xsl:attribute name ="class">ordinal_number</xsl:attribute>
        <xsl:value-of select ="position()"/>
      </xsl:element>
      <xsl:element name ="td">
        <xsl:attribute name ="class">product_number</xsl:attribute>
        <xsl:element name ="a">
          <xsl:attribute name ="href">
            <xsl:call-template name="make_addr">
              <xsl:with-param name="str" select="ProductGuid" />
            </xsl:call-template>
          </xsl:attribute>
          <xsl:value-of select ="ProductGuid"/>
        </xsl:element>                   
      </xsl:element>
      <xsl:element name="td">
        <xsl:attribute name ="class">product_name</xsl:attribute>
        <xsl:choose>
          <xsl:when test ="./VariantName">
            <xsl:value-of select ="concat(ProductName, '(', VariantName, ')')"/>  
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select ="ProductName" />                        
          </xsl:otherwise> 
        </xsl:choose>        
      </xsl:element>
      <xsl:element name="td">
        <xsl:attribute name="class">price</xsl:attribute>
        <xsl:value-of select="Price"/>
      </xsl:element>
      <xsl:element name="td">
        <xsl:attribute name="class">quantity</xsl:attribute>
        <xsl:value-of select="Quantity"/>
      </xsl:element>
    </xsl:element>         
	</xsl:template >

  <xsl:template name ="make_addr">
    <xsl:param name="str" />
    <xsl:value-of select ="concat('%ROOT%/link.aspx?ProductGuid=', $str)"/>
  </xsl:template>
</xsl:stylesheet>