<%@ Page Language="VB" AutoEventWireup="false" CodeFile="localmode.aspx.vb" Inherits="localmode"
    CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master"
    RuntimeMasterPageFile="ThreeColumn.master" CrumbName="<%$ Resources: Language, LABEL_LOCAL_WEB_SITE %>" %>

<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <div class="LocalModePage">
        <uc1:PageHeader ID="PageHeader1" runat="server" Text="<%$ Resources: Language, LABEL_LOCAL_WEB_SITE %>"
            EnableTheming="true" />
        <p>
            <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources: Language, MESSAGE_LOCALWEB %>"></asp:Literal>
        </p>
        <asp:HyperLink ID="linkRemoteShop" runat="server" Text="<%$ Resources: Language, LABEL_REMOTE_SHOP %>"></asp:HyperLink>
    </div>
</asp:Content>
