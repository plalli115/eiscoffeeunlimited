Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT.Debug
Imports ExpandIT.GlobalsClass
Imports ExpandIT.CartClass
Imports System.Web
Imports System.Collections.Generic

Partial Class quickcart
    Inherits ExpandIT.ShippingPayment._CartPage

    'Public bUseSecondaryCurrency As Object
    'Public Group As Object
    'Public info As Object
    'Public IsGroupAvailable As Object
    'Public MyCalcColSpan As Object
    'Public product As Object
    'Public productinfo As Object
    'Public searchTimePeriod As String
    Protected pStr As Object
    Protected CartLine As ExpDictionary
    Protected bUseSecondaryCurrency As Boolean
    Protected key As String
    Protected UsingVariants As Boolean
    Protected UseLineComment As Boolean
    Protected i As Integer
    Protected ColSpanNumber As Integer
    Protected strID As String
    Protected dictSHProviders As ExpDictionary
    Protected dictSHProvider As ExpDictionary
    Protected dictNotUpdatedLine As Object


    Private Action As String = String.Empty
    Private UpdateMethod As String = String.Empty
    Private InfoString As String = String.Empty
    Dim valid As Boolean


    Protected Function FindUpdateMethod() As String


        If CStrEx(Request("clear")) <> "" Then
            UpdateMethod = "clear"
        ElseIf CStrEx(Request("update")) <> "" Then
            UpdateMethod = "update"
        ElseIf CStrEx(Request("purchase")) <> "" Then
            UpdateMethod = "purchase"
        ElseIf CStrEx(Request("orderpad")) <> "" Then
            UpdateMethod = "orderpad"
        End If

        Return UpdateMethod
    End Function


    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	
	Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load



        If Me.Page.IsPostBack Then


            'Const DoDebug = False

            ' Check the page access.
            eis.PageAccessRedirect("Cart")

            'Dim OldNumberOfLines, bLinesHaveBeenRemoved, x
            'Dim Action, UpdateMethod, InfoString

            ' Read parameters
            globals.GroupGuid = CLngEx(Request("GroupGuid"))


            ' Find update method
            FindUpdateMethod()


            ' Do action
            Select Case UpdateMethod.ToUpper()
                Case "ADD"
                    Add()
                Case "UPDATE"
                    AddorUpdate()
                Case "OUTOFBASKET"
                    OutOfBasket()
                Case "ORDERPAD"
                    AddorUpdate()
                    Response.Redirect("cart.aspx")
                Case Else
                    globals.messages.Errors.Add(Resources.Language.MESSAGE_UNKNOWN_CART_ACTION & " '" & Action & "'")
            End Select



            ' Update the cartcounters.
            If OrderObject Is Nothing Then
                eis.UpdateMiniCartInfo(Nothing)
            Else
                globals.OrderDict = OrderObject
                eis.CalculateOrder()
                eis.UpdateMiniCartInfo(globals.OrderDict)
            End If
        Else
            'START _Weblogic

            Dim bUseSecondaryCurrency, UsingVariants, i
            Dim aProducts(2) As String
            Dim aQty(2) As String

            aProducts(0) = ""
            aQty(0) = 0
            aProducts(1) = ""
            aQty(1) = 0

            globals.OrderDict = eis.InitQuickPad(globals.User)

            eis.CalculateOrder()
            eis.SaveOrderDictionary(globals.OrderDict)
            eis.UpdateMiniCartInfo(globals.OrderDict)

            globals.errstr = globals.errstr & Request("ErrorStr")


            bUseSecondaryCurrency = eis.UseSecondaryCurrency(globals.OrderDict)

            'eis.CatDefaultLoadProduct(globals.OrderDict("Lines"), False, False, False, Array("ProductName"), Array(), Empty)
            eis.CatDefaultLoadProducts(globals.OrderDict("Lines"), False, False, False, New String() {"ProductName", "ProductGuid"}, Nothing, Nothing)

            eis.InitNextRowAB()



            Dim dictSHProviders, dictSHProvider

            ' Shipping and Handling information
            dictSHProviders = eis.GetShippingHandlingProviders(CStrEx(globals.OrderDict("ShippingHandlingProviderGuid")), False)
            dictSHProvider = New ExpDictionary
            If dictSHProviders.Count > 0 Then
                dictSHProvider = GetFirst(dictSHProviders).Value
            End If


            REM -- Check if lines bave been removed from the Order Pad.
            REM -- This only applies to Order Pads where quantity is changed to 0 or -1.
            Dim bLinesRemoved
            bLinesRemoved = False
            If Request("LinesHaveBeenRemoved") <> "" Then
                If Request("LinesHaveBeenRemoved") = "1" Then
                    bLinesRemoved = True
                End If
            End If


            'Get the NotUpdatedLine LineGuids
            Dim dictNotUpdatedLine = New ExpDictionary()

            Dim req As Dictionary(Of String, ArrayList) = QueryString2Dict()
            If req.ContainsKey("NotUpdatedLine") Then
                For i = 0 To req("NotUpdatedLine").Count - 1
                    dictNotUpdatedLine(req("NotUpdatedLine")(i)) = req("NotUpdatedLine")(i)
                Next
            End If

            If dictNotUpdatedLine.Count > 0 Then
                globals.messages.Warnings.Add(Resources.Language.MESSAGE_LINES_UPDATED_BY_ANOTHER_USER)
            End If

            UsingVariants = Application("USE_PRODUCTVARIANTS")
            'END _Weblogic
        End If

    End Sub



    ' Helper function: Adds the error string to the url.
    Private Function MakeParam(ByVal url As String, ByVal errorString As String, ByVal infoString As String) As String
        Dim retv As String

        retv = url
        If errorString <> "" Then
            retv = retv & IIf(InStr(1, retv, "?") = 0, "?", "&") & errorString
        End If
        If infoString <> "" Then
            retv = retv & IIf(InStr(1, retv, "?") = 0, "?", "&") & infoString
        End If

        Return retv
    End Function

    Sub UpdateOrder(ByVal lineguid As String, ByVal sku As String, ByVal qty As String, ByVal qtyprev As String)
        Dim HeaderGuid, sql As String

        HeaderGuid = eis.GetHeaderGuidEx(Session("UserGuid"), False)
        Select Case UpdateMethod
            Case "clear"
                ' Clear the order and update header.
                sql = "DELETE FROM CartLine WHERE HeaderGuid=" & SafeString(HeaderGuid)
                excecuteNonQueryDb(sql)
                ' Update header information to tell that calculations has to be done again.
                sql = "UPDATE CartHeader SET IsCalculated = 0, VersionGuid = " & SafeString(GenGuid()) & " WHERE HeaderGuid = " & SafeString(HeaderGuid)
                excecuteNonQueryDb(sql)
            Case "purchase", "update"
                ' Update the order content and mark edited lines and order.
                CompareAndUpdateOrder(HeaderGuid, lineguid, sku, qty, qtyprev)
        End Select
    End Sub

    Sub Added(ByVal sku As String, ByVal qty As String)

        Dim quantity

        globals.OrderDict = eis.LoadOrderDictionary(globals.User)


        OrderObject = globals.OrderDict

        Dim comment As String = ""
        Dim variantcode As String = ""
        quantity = String2UFloat("0" & qty)

        If sku <> "" Then

            eis.AddItemToOrder2(globals.OrderDict, sku, quantity, comment, variantcode)
        End If

        ' For performance reasons.
        globals.OrderDict("IsCalculated") = False
        globals.OrderDict("VersionGuid") = GenGuid()

        'If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And csrObj.getSalesPersonGuid() <> "" Then
        '    globals.OrderDict("SalesPersonGuid") = CStrEx(csrObj.getSalesPersonGuid())
        'End If



        ' Save the order.
        eis.SaveOrderDictionary(OrderObject)


    End Sub

    Sub AddorUpdate()
        Dim i As Integer
        Dim skufld, qtyfld, sku, qty, lgfld, qtyprevfld, lineguid, qtyprev As String
        'Loop and determine if this is and add or update on LineGuid

        For i = 1 To CIntEx(AppSettings("NUMBER_OF_ITEMS_QUICK_CART"))

            skufld = "SKU" & CStr(i)
            qtyfld = "QUANTITY" & CStr(i)
            lgfld = "LineGuid" & CStr(i)
            qtyprevfld = "Quantity_Prev" & CStr(i)

            sku = Request(skufld)
            'sku = LPad(sku, "0", 5)
            qty = Request(qtyfld)
            lineguid = Request(lgfld)
            qtyprev = Request(qtyprevfld)

            If Trim(Left(lineguid, 1)) = "" Then
                Added(sku, qty)
            Else
                UpdateOrder(lineguid, sku, qty, qtyprev)
            End If

        Next

    End Sub

    Sub CompareAndUpdateOrder(ByVal HeaderGuid As String, ByVal LineGuid As String, ByVal skuq As String, ByVal qtyq As String, ByVal quantity_prev As String)

        Dim bLineUpdated As Boolean = False
        Dim bUpdateHeader As Boolean = False
        Dim bDoUpdateLine As Boolean
        'Dim sqlVariant As String
        'Dim dictVariants As ExpDictionary

        ' Initialazation

        OrderObject = eis.LoadOrderDictionary(globals.User)

        ' Loop the form elements / line updates
        Dim dictForm As ExpDictionary
        Dim dictFormLine As ExpDictionary
        Dim i, j As Integer
        Dim dictOrderLine As ExpDictionary
        'Dim LineGuid As String = ""
        Dim VersionGuid, strUpdatedLineGuids As String
        Dim dictOrderLinesToUpdate As ExpDictionary
        Dim dictOrderLinesDeleted, dictOrderlinesNotUpdated, dictFieldsNotFound As ExpDictionary
        Dim bDBValueChanged, bUserValueChanged As Boolean
        Dim arrFieldMatch, arrTmp As Object()
        Dim strFieldName, strFieldType As String
        Dim dictTestUpdates As ExpDictionary
        Dim aKey As String
        Dim bNewNonExistingLine As Boolean
        Dim strUpdatedProductGuidsMissingVariant As String
        Dim bReCalculate As Boolean
        'Dim sku As String
        'Dim qty As Double
        'Dim var As String
        Dim sql As String
        'Dim item As String
        Dim dict As ExpDictionary

        'Dim req As Dictionary(Of String, ArrayList) = QueryString2Dict()
        'If Not req.ContainsKey("SKU") Then req.Add("SKU", New ArrayList())
        'If Not req.ContainsKey("LineGuid") Then req.Add("LineGuid", New ArrayList())
        'If Not req.ContainsKey("VersionGuid") Then req.Add("VersionGuid", New ArrayList())
        'If Not req.ContainsKey("HeaderComment") Then req.Add("HeaderComment", New ArrayList())
        'If Not req.ContainsKey("CustomerPONumber") Then req.Add("CustomerPONumber", New ArrayList())

        ' The array containing information on which elements to compare/update....
        'If AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_COMMENT") Then
        '    arrFieldMatch = New Object() {New String() {"Quantity", "NUMBER"}, New String() {"LineComment", "STRING"}}
        'Else
        arrFieldMatch = New Object() {New String() {"Quantity", "NUMBER"}}
        'End If

        strUpdatedLineGuids = ""
        strUpdatedProductGuidsMissingVariant = ""
        dictForm = New ExpDictionary
        dictOrderLinesToUpdate = New ExpDictionary
        dictOrderLinesDeleted = New ExpDictionary
        dictOrderlinesNotUpdated = New ExpDictionary
        dictFieldsNotFound = New ExpDictionary
        bReCalculate = False
        ' loop the sku's and look for updates
        'For i = 0 To req("SKU").Count - 1
        '    sku = Trim(req("SKU")(i))
        If skuq <> "" Then
            dictFormLine = New ExpDictionary
            bNewNonExistingLine = False
            bDoUpdateLine = True
            'If req("LineGuid").Count >= i + 1 Then
            '    LineGuid = req("LineGuid")(i)
            'Else
            '    ' The user has selected to add another product via the cart.
            '    ' The last sku comes from CBoolEx(AppSettings("CART_ENTRY")) being true
            '    If i = req("SKU").Count - 1 Then
            '        If strUpdatedProductGuidsMissingVariant <> "" Then strUpdatedProductGuidsMissingVariant = strUpdatedProductGuidsMissingVariant & ","
            '        strUpdatedProductGuidsMissingVariant = strUpdatedProductGuidsMissingVariant & SafeString(sku)
            '        bNewNonExistingLine = True
            '        'Exit For
            '    End If
            'End If
            'VersionGuid = CStrEx(req("VersionGuid")(i))

            ' Does the line exist in the order?
            If Not IsDict(OrderObject("Lines")(LineGuid)) Then
                ' Since the line doesn't exist please ignore the line. 
                ' Tell the user that a line has been deleted by another user.
                bLinesHaveBeenRemoved = 1
                dictOrderLinesDeleted(LineGuid) = LineGuid
            Else
                dictOrderLine = OrderObject("Lines")(LineGuid)
                'If VersionGuid <> CStrEx(dictOrderLine("VersionGuid")) Then
                '    dictOrderlinesNotUpdated(LineGuid) = dictOrderLine
                'Else
                dictFormLine("LineGuid") = LineGuid
                bDBValueChanged = False
                bUserValueChanged = False

                ' Do matching of field(s)
                ' Loop fields, and convert by type.
                For j = 0 To UBound(arrFieldMatch)
                    arrTmp = arrFieldMatch(j)
                    strFieldName = arrTmp(0)
                    strFieldType = arrTmp(1)

                    ' Is field present on the order line? If not then exit the for loop
                    ' Ignore. Could be due to wrong field information, add debugging information
                    If dictOrderLine(strFieldName) Is Nothing Then
                        dictFieldsNotFound(strFieldName) = strFieldName & ";" & strFieldType
                        Exit For
                    End If
                    dictFormLine(strFieldName & "_prev") = quantity_prev
                    dictFormLine(strFieldName) = qtyq

                    ' Convert to numbers if necessary
                    If strFieldType = "NUMBER" Then
                        dictOrderLine(strFieldName) = CDblEx(dictOrderLine(strFieldName))
                        dictFormLine(strFieldName & "_prev") = CDblEx(dictFormLine(strFieldName & "_prev"))
                        If Not IsNumeric(dictFormLine(strFieldName)) Then
                            dictFormLine(strFieldName) = CDblEx(dictFormLine(strFieldName & "_prev"))
                        Else
                            dictFormLine(strFieldName) = CDblEx(dictFormLine(strFieldName))
                        End If
                        If strFieldName = "Quantity" Then
                            If CBool(AppSettings("CART_ALLOW_DECIMAL")) Then
                                dictFormLine(strFieldName) = CDblEx(SafeUFloat(CDblEx(dictFormLine(strFieldName))))
                            Else
                                dictFormLine(strFieldName) = CLngEx(SafeULong(CDblEx(dictFormLine(strFieldName))))
                            End If
                            If dictFormLine(strFieldName) <= 0 Then
                                ' Delete the line and skip the rest of the update.
                                sql = "DELETE FROM CartLine WHERE LineGuid = " & SafeString(LineGuid) & " AND (VersionGuid = " & SafeString(VersionGuid) & " OR VersionGuid IS NULL)"
                                excecuteNonQueryDb(sql)

                                If OrderObject("Lines").Exists(LineGuid) Then
                                    OrderObject("Lines").Remove(LineGuid)
                                End If

                                bDoUpdateLine = False
                                bReCalculate = True
                            End If
                        End If
                    End If

                    If strFieldType = "BOOLEAN" Then
                        dictOrderLine(strFieldName) = CBoolEx(dictOrderLine(strFieldName))
                        dictFormLine(strFieldName) = CBoolEx(dictFormLine(strFieldName))
                        dictFormLine(strFieldName & "_prev") = CBoolEx(dictFormLine(strFieldName & "_prev"))
                    End If

                    If bDoUpdateLine Then
                        ' Compare value to database? 
                        ' If changed inform the user that this line was not edited since another user changed the value
                        If CStrEx(dictOrderLine(strFieldName)) <> dictFormLine(strFieldName & "_prev") Then
                            ' Inform the user of the updated record. 
                            ' E.g. mark line with "red" or a star. Placing a legend saying that some information was not updated.
                            dictOrderLine("DBFieldValueChange") = strFieldName
                            dictOrderlinesNotUpdated(LineGuid) = dictOrderLine
                            bDBValueChanged = True
                        End If

                        ' Compare value to updated value?
                        If Not bDBValueChanged Then
                            If dictFormLine(strFieldName) <> dictFormLine(strFieldName & "_prev") Then
                                bUserValueChanged = True
                                ' Create the WHERE clause for getting information on deleted/not updated records after the update.
                                If strUpdatedLineGuids <> "" Then strUpdatedLineGuids = strUpdatedLineGuids & ","
                                strUpdatedLineGuids = strUpdatedLineGuids & SafeString(LineGuid)
                                ' Is variant guid missing?
                                If CStrEx(dictOrderLine("VariantCode")) = "" Then
                                    If strUpdatedProductGuidsMissingVariant <> "" Then strUpdatedProductGuidsMissingVariant = strUpdatedProductGuidsMissingVariant & ","
                                    strUpdatedProductGuidsMissingVariant = strUpdatedProductGuidsMissingVariant & SafeString(dictOrderLine("ProductGuid"))
                                End If
                                ' Set the value and mark the line for re-calculation and set new version.
                                dictOrderLine(strFieldName) = dictFormLine(strFieldName)
                                dictOrderLine("IsCalculated") = False
                                dictOrderLine("VersionGuid") = GenGuid()
                                dictOrderLinesToUpdate(LineGuid) = dictOrderLine
                            End If
                        End If
                    End If ' // bDoUpdateLine (No else)
                Next
                dictForm.Add(dictFormLine("LineGuid"), dictFormLine)
                'End If ' // VersionGuid <> dictOrderLine("VersionGuid")
            End If ' // Not IsDict(OrderObject("Lines")(LineGuid))
        End If ' // If sku<>"" Then
        'Next ' // Looping sku's from posting page (cart.aspx)

        'dictVariants = New ExpDictionary()

        '' Get variants for the lines that are missing the variant field.
        'If strUpdatedProductGuidsMissingVariant <> "" Then
        '    If Application("USE_PRODUCTVARIANTS") Then
        '        If Len(strUpdatedProductGuidsMissingVariant) > 0 Then
        '            sqlVariant = "SELECT * FROM ProductVariant WHERE ProductGuid IN (" & strUpdatedProductGuidsMissingVariant & ") ORDER BY VariantName DESC"
        '            dictVariants = SQL2Dicts(sqlVariant, "ProductGuid")
        '        End If
        '    End If
        'End If

        ' Run update statement if any lines to update.
        ' Might need an extra check on the line guid	
        If strUpdatedLineGuids <> "" Then
            ' Make sure that a variant guid is assigned to a line if a VariantCode is not present on the line 

            ' Get possible variants for the ProductGuid's collected in myGuidStr
            'If dictVariants.Count > 0 Then
            '    For Each item In dictOrderLinesToUpdate.Keys
            '        If IsDict(dictVariants(dictOrderLinesToUpdate(item)("ProductGuid"))) Then
            '            If CStrEx(dictOrderLinesToUpdate(item)("VariantCode")) = "" Then
            '                dictOrderLinesToUpdate(item)("VariantCode") = dictVariants(dictOrderLinesToUpdate(item)("ProductGuid"))("VariantCode")
            '            End If
            '        End If
            '    Next
            'End If
            ' Delete/Save the order lines.
            ' Due to performance issues it has been chosen to delete all elements and then create them again.
            ' To write a more multi-user safe structure: run through each line and delete lines based on
            ' both VersionGuid and LineGuid. Create the line if a line was deleted. (A "true" update).
            Dicts2Table(dictOrderLinesToUpdate, "CartLine", "LineGuid IN (" & strUpdatedLineGuids & ")")

            ' Run through the updated records and check if the records actually updated.
            sql = "SELECT LineGuid, VersionGuid FROM CartLine WHERE LineGuid IN (" & strUpdatedLineGuids & ")"
            dictTestUpdates = SQL2Dicts(sql, "LineGuid")
            For Each aKey In dictOrderLinesToUpdate.ClonedKeyArray
                dict = dictOrderLinesToUpdate(aKey)
                ' Is the line present in the update?
                If IsDict(dictTestUpdates(aKey)) Then
                    ' Compare versions.
                    'If dict("VersionGuid") <> dictTestUpdates(aKey)("VersionGuid") Then
                    '    dictOrderlinesNotUpdated(aKey) = dictOrderLinesToUpdate(aKey)
                    '    dictOrderLinesToUpdate.Remove(aKey)
                    'End If
                Else
                    bLinesHaveBeenRemoved = 1
                    dictOrderLinesDeleted(aKey) = aKey
                    dictOrderLinesToUpdate.Remove(aKey)
                End If
            Next
        End If

        ' Process the error message handling for the retval post.
        ' Collect information
        If InfoString <> "" Then InfoString = InfoString & "&"
        InfoString = InfoString & "DeletedLinesCount=" & CLngEx(dictOrderLinesDeleted.Count)
        InfoString = InfoString & "&NotUpdatedLinesCount=" & CLngEx(dictOrderlinesNotUpdated.Count)
        InfoString = InfoString & "&UpdatedLinesCount=" & CLngEx(dictOrderLinesToUpdate.Count)
        InfoString = InfoString & "&UpdatedHeaderInformation=" & CBoolEx(bUpdateHeader Or (dictOrderLinesToUpdate.Count > 0))
        InfoString = InfoString & "&AddedElements=" & (CBoolEx(("CART_ENTRY")) And CBoolEx(bNewNonExistingLine))
        For Each aKey In dictOrderlinesNotUpdated.Keys
            InfoString = InfoString & "&NotUpdatedLine=" & Server.UrlEncode(CStrEx(dictOrderlinesNotUpdated(aKey)("LineGuid")))
        Next
        For Each aKey In dictFieldsNotFound.Keys
            InfoString = InfoString & "&FieldNotFound=" & Server.UrlEncode(CStrEx(dictFieldsNotFound(aKey)))
        Next
    End Sub

    Sub OutOfBasket()
        Dim myLineGuid As String

        OrderObject = eis.LoadOrderDictionary(globals.User)


        ' Remove the line if present.
        If OrderObject("Lines").Count > 0 Then
            myLineGuid = CStrEx(Request("LineGuid"))

            If OrderObject("Lines").Exists(myLineGuid) Then
                OrderObject("Lines").Remove(myLineGuid)

                OrderObject("IsCalculated") = False
                OrderObject("VersionGuid") = GenGuid()
                eis.SaveOrderDictionary(OrderObject)
            End If
        End If
    End Sub
End Class
