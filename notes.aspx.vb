﻿Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT.GlobalsClass

Partial Class notes
    Inherits ExpandIT.Page
    
    
    
    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	
	Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    
        ' Check the page access.
        eis.PageAccessRedirect("Notes")

        ' DEBUG INFORMATION COLLECTION
        If globals.DebugLogic Then
            DebugValue(User, "User")
        End If
        
        If Request("NNN") <> "" then
          NoteName = Request("NNN")
        Else
          NoteName = ""
        End If        
    End Sub
    
    Protected Property NoteName As String
      Get
        Return NoteList1.ListName 
      End Get
      Set(ByVal value As String)
        NoteList1.ListName = value
      End Set
    End Property
    
End Class
