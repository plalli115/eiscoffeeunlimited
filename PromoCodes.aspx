<%@ Import Namespace="System.Configuration.ConfigurationManager" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PromoCodes.aspx.vb" Inherits="PromoCodes"
    CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master"
    RuntimeMasterPageFile="ThreeColumn.master" CrumbName="<%$ Resources: Language, LABEL_PROMO_TITLE %>" %>
<%@ Register Src="~/controls/ShowPromoCodes/ShowPromoCodes.ascx" TagName="ShowPromoCodes"
    TagPrefix="uc1" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>    
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <div class="InfoPage">
        <uc1:pageheader id="PageHeader1" runat="server" text="<%$ Resources: Language, LABEL_PROMO_TITLE %>"
            enabletheming="true" />
        <uc1:showpromocodes id="ShowPromoCodes1" runat="server" />
        <asp:Literal ID="litContent" Visible="false" runat="server" Text="<%$ Resources: Language, LABEL_NO_PROMO_CODES %>"></asp:Literal>
    </div>
</asp:Content>
