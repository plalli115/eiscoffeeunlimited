<%@ Import Namespace="System.Configuration.ConfigurationManager" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="history_lookup.aspx.vb"
    EnableViewState="true" Inherits="history_lookup" ValidateRequest="false" CodeFileBaseClass="ExpandIT.Page"
    MasterPageFile="~/masters/default/main.master" RuntimeMasterPageFile="ThreeColumn.master"
    CrumbName="<%$ Resources: Language, LABEL_MENU_HISTORY %>" %>

<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Register Src="~/controls/Message.ascx" TagName="Message" TagPrefix="uc1" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <div class="HistoryLookupPage">
        <uc1:Message ID="Message1" runat="server" />
        <uc1:PageHeader ID="PageHeader1" runat="server" Text="<%$ Resources: Language, LABEL_MENU_HISTORY %>"
            EnableTheming="true" />
        <p>
            <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources: Language, LABEL_PLEASE_ENTER_ORDERREF %>"></asp:Literal>
        </p>
        <p>
            <asp:Panel runat="server" ID="refSearchPanel" style="vertical-align:middle;"  DefaultButton="submit1">
                <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources: Language, LABEL_HISTORY_LOOKUP %>"></asp:Literal>
                <asp:TextBox runat="server" ID="CustomerReference" style="vertical-align:middle;" CssClass="borderTextBox" />
                &nbsp; &nbsp; &nbsp; &nbsp;<asp:Button ID="submit1" runat="server" CssClass="AddButton" Text="<%$ Resources: Language, ACTION_LOOKUP %>" />
            </asp:Panel>
        </p>
    </div>
</asp:Content>
