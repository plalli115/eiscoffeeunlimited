Imports System.Configuration.ConfigurationManager
Imports System.Data
Imports ExpandIT
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Collections.Generic

Partial Class templates_groupDropDownQty
    Inherits ExpandIT.ShippingPayment._CartPage

    Protected GroupsAvailable As Boolean
    Protected Group As ExpDictionary
    Protected intCountSubGroups As Integer
    Protected subgroup As ExpDictionary
    Protected NumberOfProductsOnEachLine As Integer
    Protected product As ExpDictionary
    Protected myCount As Integer
    Protected bUseSecondaryCurrency As Boolean
    Protected NumberOfGroupsOnEachLine As Integer
    Protected objPage As New PagedDataSource()
    Protected guids As String = ""

    Protected page1 As String = "0"
    Protected page2 As String = "0"
    Protected page3 As String = "0"


    Public Property CurrentPage() As Integer
        Get
            If (Me.ViewState("CurrentPage") Is Nothing) Then
                Return 0
            Else
                Return Integer.Parse(Me.ViewState("CurrentPage").ToString())
            End If
        End Get

        Set(ByVal value As Integer)
            Me.ViewState("CurrentPage") = value
        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'JA2010030901 - PERFORMANCE -Start
        If CBoolEx(AppSettings("SHOW_BEST_SELLERS")) Then
            'Me.BestSellersByCategory1.getGroupGuid = globals.GroupGuid
            Dim myControlBox As Control = Me.LoadControl("~/controls/boxes/BestSellersByCategoryBox.ascx")
            myControlBox.ID = "BestSellersByCategoryBox1"

            Dim myControl As Control = Me.LoadControl("~/controls/BestSellersByCategory.ascx")
            myControl.ID = "BestSellersByCategory1"
            eis.SetControlProperties(myControl, "getGroupGuid", globals.GroupGuid)
            Me.phMyMostPopularItems.Controls.Clear()

            Me.phMyMostPopularItems.Controls.Add(myControlBox)
            Me.phMyMostPopularItems.Controls.Add(myControl)
        End If
        'JA2010030901 - PERFORMANCE -End 
        If AppSettings("EXPANDIT_US_USE_PAGINATION") Then
            If Not IsPostBack Then
                Dim builder As New StringBuilder()
                builder.Append("")
                builder.Append("|")
                builder.Append("")
                builder.Append(",")
                builder.Append(Resources.Language.LABEL_PRODUCT)
                builder.Append("|")
                builder.Append("ProductGuid")
                builder.Append(",")
                builder.Append(Resources.Language.LABEL_NAME)
                builder.Append("|")
                builder.Append("ProductName")
                builder.Append(",")
                builder.Append(Resources.Language.LABEL_PRICE)
                builder.Append("|")
                builder.Append("ListPrice")
                SortControl1.FieldsList = builder.ToString()
            End If
        End If


        If CStrEx(page1) = "0" Then
            page1 = CStrEx(AppSettings("Page_1"))
        End If
        If CStrEx(page2) = "0" Then
            page2 = CStrEx(AppSettings("Page_2"))
        End If
        If CStrEx(page3) = "0" Then
            page3 = CStrEx(AppSettings("Page_3"))
        End If


    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete



        ' Load the user and check the page access.
        eis.PageAccessRedirect("Catalog")

        If AppSettings("EXPANDIT_US_USE_PAGINATION") Then
            If Not Page.IsPostBack Then
                Dim list1 As New ListItem(page1, page1, True)
                Me.ddlPageSize.Items.Add(list1)
                Dim list2 As New ListItem(page2, page2)
                Me.ddlPageSize.Items.Add(list2)
                Dim list3 As New ListItem(page3, page3)
                Me.ddlPageSize.Items.Add(list3)
                Dim all As New ListItem("All", "All")
                Me.ddlPageSize.Items.Add(all)


                Me.ddlPageSize2.Items.Add(list1)
                Me.ddlPageSize2.Items.Add(list2)
                Me.ddlPageSize2.Items.Add(list3)
                Me.ddlPageSize2.Items.Add(all)
            End If
        End If

        BindData()


    End Sub

    Private Sub BindData(Optional ByVal fromEvent As String = "")

        Dim info As ExpDictionary
        Dim productinfo As ExpDictionary
        Dim subgroupinfo As ExpDictionary
        'JA2010030901 - PERFORMANCE -Start
        Dim dictPerformance As ExpDictionary = New ExpDictionary()
        'JA2010030901 - PERFORMANCE -End
        Dim selection As String = ""
        If AppSettings("EXPANDIT_US_USE_PAGINATION") Then
            selection = Me.SortControl1.getSelection()
        End If

        ' Check if the user is using secondary currency.
        bUseSecondaryCurrency = eis.UseSecondaryCurrency(User)

        ' Check if it has been cached earlier. If not then load sub groups and products.
        info = eis.CatGroups2Info(New String() {globals.GroupGuid})

        'JA2010030901 - PERFORMANCE -Start
        'AM2010071901 - ENTERPRISE CSC - Start
        'info("General")("CacheKey") = "groupDropDownQty.aspx-" & globals.GroupGuid.ToString & "-" & Session("LanguageGuid").ToString & "-" & globals.User("CatalogNo")
        '*****CUSTOMER SPECIFIC CATALOGS***** -START
        'ExpandIT US - USE CSC - Start
        'If CBoolEx(AppSettings("EXPANDIT_US_USE_CSC")) And CBoolEx(globals.User("B2B")) Then
        'info("General")("CacheKey") &= "-" & globals.User("CustomerGuid")
        'End If

        If AppSettings("EXPANDIT_US_USE_CSC") Then
            If selection <> "" Then
                info("General")("CacheKey") = "groupDropDownQty.aspx-" & globals.GroupGuid.ToString & "-" & selection & "-" & Session("LanguageGuid").ToString & "-" & globals.User("CatalogNo") & "-" & CStrEx(globals.User("CustomerGuid"))
            Else
                info("General")("CacheKey") = "groupDropDownQty.aspx-" & globals.GroupGuid.ToString & "-" & CurrentPage & "-" & ddlPageSize.SelectedValue & "-" & Session("LanguageGuid").ToString & "-" & globals.User("CatalogNo") & "-" & CStrEx(globals.User("CustomerGuid"))
            End If
        Else
            If selection <> "" Then
                info("General")("CacheKey") = "groupDropDownQty.aspx-" & globals.GroupGuid.ToString & "-" & selection & "-" & Session("LanguageGuid").ToString & "-" & globals.User("CatalogNo")
            Else
                info("General")("CacheKey") = "groupDropDownQty.aspx-" & globals.GroupGuid.ToString & "-" & CurrentPage & "-" & ddlPageSize.SelectedValue & "-" & Session("LanguageGuid").ToString & "-" & globals.User("CatalogNo")
            End If
        End If
        'ExpandIT US - USE CSC - End
        '*****CUSTOMER SPECIFIC CATALOGS***** -End
        'AM2010071901 - ENTERPRISE CSC - End
        'JA2010030901 - PERFORMANCE -End


        'JA2010030901 - PERFORMANCE -Start
        If (selection = "") And AppSettings("EXPANDIT_US_USE_PAGINATION") Then
            dictPerformance("PageNumber") = CurrentPage
            dictPerformance("TotalPerPage") = ddlPageSize.SelectedValue
            info("General")("Performance") = dictPerformance
        End If
        'JA2010030901 - PERFORMANCE -End

        info("General")("CalcPrices") = True
        productinfo = New ExpDictionary()
        'JA2010030901 - PERFORMANCE -Start
        productinfo.Add("LoadAllVariants", True)
        'productinfo.Add("LoadAllVariants", False)
        'JA2010030901 - PERFORMANCE -End
        productinfo.Add("CustomerSpecificPrice", True)
        productinfo.Add("ConvertCurrency", True)
        productinfo.Add("ProductFieldArray", New String() {"ProductName", "QtyOnHand", "ListPrice", "NewItem", "DateAdded", "BaseUOM"})
        ' Changed here to use both HTMLDESC and DESCRIPTION
        productinfo.Add("PropertyArray", New String() {"PICTURE2", "DESCRIPTION", "HIDESUBGROUPS"})
        info("GroupInfo")("ProductInfo") = productinfo

        subgroupinfo = New ExpDictionary()
        subgroupinfo("PropertyArray") = New String() {"NAME", "DESCRIPTION", "PICTURE2"}
        info("GroupInfo")("SubgroupInfo") = subgroupinfo

        eis.CatLoadGroups(info)
        Group = info("GroupInfo")("Groups")(globals.GroupGuid.ToString)

        GroupsAvailable = Group.Count > 0



        'JA2010032101 - META TAGS -Start
        If CStrEx(Group("METATAGKEYWORDS")) <> "" Then
            Dim metaKeywords As System.Web.UI.HtmlControls.HtmlMeta = New HtmlMeta
            metaKeywords.Name = "keywords"
            metaKeywords.Content = CStrEx(Group("METATAGKEYWORDS"))
            Page.Header.Controls.Add(metaKeywords)
        End If

        If CStrEx(Group("METATAGDESCRIPTION")) <> "" Then
            Dim metaDescription As System.Web.UI.HtmlControls.HtmlMeta = New HtmlMeta
            metaDescription.Name = "description"
            metaDescription.Content = CStrEx(Group("METATAGDESCRIPTION"))
            Page.Header.Controls.Add(metaDescription)
        End If
        'JA2010032101 - META TAGS -End




        GroupHeader1.Group = Group

        Dim products As ExpDictionary = Group("Products")
        If products Is Nothing Then products = New ExpDictionary


      


        If AppSettings("EXPANDIT_US_USE_PAGINATION") Then
            If selection = "" Then
                objPage.DataSource = Group("Products")
            Else
                SortControl1.DataSource = Group("Products")
            End If


            If Not Group("Products") Is Nothing Then
                If selection <> "" Then
                    objPage.DataSource = SortControl1.SortedDataSource
                End If
                objPage.AllowPaging = True

                If fromEvent = "" Or fromEvent = "selectedIndex" Then
                    If CStrEx(ddlPageSize.SelectedValue) = "All" Then
                        objPage.PageSize = products.Count
                        ddlPageSize2.SelectedValue = "All"
                    Else
                        objPage.PageSize = Integer.Parse(ddlPageSize.SelectedValue)
                        ddlPageSize2.SelectedValue = Integer.Parse(ddlPageSize.SelectedValue)
                    End If
                ElseIf fromEvent = "selectedIndex2" Then
                    If CStrEx(ddlPageSize2.SelectedValue) = "All" Then
                        objPage.PageSize = products.Count
                        ddlPageSize.SelectedValue = "All"
                    Else
                        objPage.PageSize = Integer.Parse(ddlPageSize2.SelectedValue)
                        ddlPageSize.SelectedValue = Integer.Parse(ddlPageSize2.SelectedValue)
                    End If
                End If


                If CurrentPage < 0 Or CurrentPage >= objPage.PageCount Then
                    CurrentPage = 0
                End If
                objPage.CurrentPageIndex = CurrentPage
                lnkbtnNext.Visible = Not objPage.IsLastPage
                lnkbtnPrevious.Visible = Not objPage.IsFirstPage
                lnkbtnNext2.Visible = Not objPage.IsLastPage
                lnkbtnPrevious2.Visible = Not objPage.IsFirstPage
                dlProducts.DataSource = objPage
                dlProducts.DataBind()
                Me.txtPage.Text = CurrentPage + 1
                Me.txtPage2.Text = CurrentPage + 1
                Me.lblNumPages.Text = Resources.Language.LABEL_PAGINATION_OF & " " & objPage.PageCount
                Me.lblNumPages2.Text = Resources.Language.LABEL_PAGINATION_OF & " " & objPage.PageCount
            End If
        Else
            If Not Group("Products") Is Nothing Then
                objPage.DataSource = Group("Products")
                dlProducts.DataSource = objPage
                dlProducts.DataBind()
            End If
        End If
        If Not Group("Products") Is Nothing Then
        Dim from As Integer = CIntEx(dlProducts.DataSource.count) * CIntEx(dlProducts.DataSource.CurrentPageIndex)
        Dim toCount As Integer = (CIntEx(dlProducts.DataSource.count) * CIntEx(dlProducts.DataSource.CurrentPageIndex)) + CIntEx(dlProducts.DataSource.count) - 1
        Dim j As Integer = 0
        guids = ""
        If Not products Is Nothing AndAlso Not products Is DBNull.Value AndAlso products.Count > 0 Then
            For Each item As ExpDictionary In products.Values
                If j >= from And j <= toCount Then
                    guids = guids & item("ProductGuid") & "|"
                End If
                j = j + 1
            Next
            guids = guids.TrimEnd("|")
        End If
        End If

        PageHeader1.Text = Group("NAME")
    End Sub

    Sub dlPaging_ItemCommand(ByVal sender As Object, ByVal e As DataListCommandEventArgs)

        If (e.CommandName.Equals("lnkbtnPaging")) Then
            CurrentPage = Integer.Parse(e.CommandArgument.ToString())
            BindData()
        End If

    End Sub


    Sub dlPaging_ItemDataBound(ByVal sender As Object, ByVal e As DataListItemEventArgs)

        Dim lnkbtnPage = CType(e.Item.FindControl("lnkbtnPaging"), LinkButton)
        If (lnkbtnPage.CommandArgument.ToString() = CurrentPage.ToString()) Then
            lnkbtnPage.Enabled = False
            lnkbtnPage.Font.Bold = True
        End If
    End Sub

    Sub lnkbtnPrevious_Click(ByVal sender As Object, ByVal e As EventArgs)

        CurrentPage -= 1
        BindData()

    End Sub
    Sub lnkbtnPrevious2_Click(ByVal sender As Object, ByVal e As EventArgs)

        CurrentPage -= 1
        BindData()

    End Sub

    Sub txtPage_TextChanged(ByVal sender As Object, ByVal e As EventArgs)

        CurrentPage = txtPage.Text - 1
        BindData()

    End Sub
    Sub txtPage2_TextChanged(ByVal sender As Object, ByVal e As EventArgs)

        CurrentPage = txtPage2.Text - 1
        BindData()

    End Sub

    Sub lnkbtnNext_Click(ByVal sender As Object, ByVal e As EventArgs)

        CurrentPage += 1
        BindData()

    End Sub

    Sub lnkbtnNext2_Click(ByVal sender As Object, ByVal e As EventArgs)

        CurrentPage += 1
        BindData()

    End Sub

    Protected Sub ddlPageSize_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
        CurrentPage = 0
        BindData("selectedIndex")
    End Sub

    Protected Sub ddlPageSize2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPageSize2.SelectedIndexChanged
        CurrentPage = 0
        BindData("selectedIndex2")
    End Sub

End Class
