<%--
Template information used by the Catalog Manager
#NAME=Price List
#DESCRIPTION=Displays a list of products with price information.
#GROUPTEMPLATE=TRUE
#PRODUCTTEMPLATE=FALSE
--%>
<%@ Import Namespace="System.Configuration.ConfigurationManager" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="pricelist.aspx.vb" Inherits="templates_pricelist"
    CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master"
    RuntimeMasterPageFile="ThreeColumn.master" %>

<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.GlobalsClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Register Src="~/controls/AddToCartButton.ascx" TagName="AddToCartButton" TagPrefix="uc1" %>
<%@ Register Namespace="ExpandIT.SimpleUIControls" TagPrefix="expui" %>
<%@ Register Namespace="ExpandIT.Buttons" TagPrefix="exbtn" %>
<%@ Register Src="~/controls/Message.ascx" TagName="Message" TagPrefix="uc1" %>
<%@ Register Src="~/controls/ProductSortControl.ascx" TagName="SortControl" TagPrefix="uc2" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<%@ Register Src="~/controls/boxes/BestSellersByCategoryBox.ascx" TagName="BestSellersByCategoryBox"
    TagPrefix="uc1" %>
<%@ Register Src="~/controls/BestSellersByCategory.ascx" TagName="BestSellersByCategory"
    TagPrefix="uc1" %>
<%--AM0122201001 - UOM - Start--%>
<%@ Register Src="~/controls/USUOM/UOM.ascx" TagName="UOM" TagPrefix="uc1" %>
<%--AM0122201001 - UOM - End--%>
<%--AM2010021901 - VARIANTS - Start--%>
<%@ Register Src="~/controls/USVariants/Variants.ascx" TagName="Variants" TagPrefix="uc1" %>
<%--AM2010021901 - VARIANTS - End--%>
<%--AM2010051902 - PRODUCT AVAILABILITY - Start--%>
<%@ Register Src="~/controls/USProductAvailability/ProductAvailability.ascx" TagName="ProductAvailability"
    TagPrefix="uc1" %>
<%--AM2010051902 - PRODUCT AVAILABILITY - End--%>

<%@ Register Src="~/controls/PriceNoUom.ascx" TagName="PriceNoUom"
    TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <asp:Panel runat="server" ID="pricelistPanel">
        <% If Not Group("Products") Is Nothing Then%>
        <div class="pricelisttemplate">

        
            <script src="<%=Vroot %>/script/Geometry.js" type="text/javascript"></script>

            <script src="<%=Vroot %>/script/tooltip.js" type="text/javascript"></script>

            <script src="<%=Vroot %>/script/addToCart.js" type="text/javascript"></script>

            <uc1:Message ID="Message1" runat="server" />
            <uc1:PageHeader ID="PageHeader1" runat="server" Text="" EnableTheming="true" />
            <% If Not Group("PICTURE1") = "" Then%>
            <table style="width: 100%;">
                <tr>
                    <td style="text-align: right; width: 50%;">
                        <asp:Image ID="Picture1" runat="server" />
                    </td>
                </tr>
            </table>
            <% End If%>
            <%If CStrEx(Group("HTMLDESC")) <> "" Then%>
            <div class="pricelistTemplateHeader">
                <% =IIf(CStrEx(Group("HTMLDESC")) <> "", Group("HTMLDESC"), "")%>
            </div>
            <%End If%>
            <input type="hidden" name="GroupGuid" value="<% = globals.GroupGuid %>" />
            <%If AppSettings("EXPANDIT_US_USE_PAGINATION") Then%>
            <br />
            <div>
                <table cellpadding="0" cellspacing="0" style="width: 100%;" class="PaginationStyle">
                    <tr>
                        <td align="left" style="vertical-align: middle; padding-left: 5px; width: 15%;">
                            <asp:LinkButton ID="lnkbtnPrevious" CssClass="PaginationStyle" Style="text-decoration: none;"
                                runat="server" OnClick="lnkbtnPrevious_Click" Text="<%$Resources: Language, LABEL_PAGINATION_PREVIOUS_PAGE %>"></asp:LinkButton></td>
                        <td style="vertical-align: middle; width: 25%;" align="left">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="vertical-align: middle;">
                                        <asp:Label ID="Label1" CssClass="PaginationStyle" runat="server"><%=Resources.Language.LABEL_PAGINATION_PAGE %> </asp:Label>
                                    </td>
                                    <td style="padding-left: 5px; padding-right: 5px;">
                                        <asp:TextBox runat="server" ID="txtPage" Style="font-family: Tahoma; padding-top: 1px;
                                            padding-bottom: 1px; text-align: center;" Width="20px" Font-Size="XX-Small" Height="10px"
                                            CssClass="PageNumber" AutoPostBack="true" OnTextChanged="txtPage_TextChanged"></asp:TextBox>
                                    </td>
                                    <td style="vertical-align: middle;">
                                        <asp:Label ID="lblNumPages" CssClass="PaginationStyle" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="vertical-align: middle; width: 25%;" align="left">
                            <uc2:SortControl ID="SortControl1" runat="server"></uc2:SortControl>
                        </td>
                        <td style="vertical-align: middle; width: 25%;" align="center">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="vertical-align: middle;">
                                        <asp:Label ID="Label2" CssClass="PaginationStyle" runat="server"><%=Resources.Language.LABEL_PAGINATION_ITEMS_PAGE  %> </asp:Label>
                                    </td>
                                    <td style="padding-left: 5px;">
                                        <asp:DropDownList ID="ddlPageSize" Style="height: 16px; font-family: Tahoma; font-size: x-small;"
                                            AutoPostBack="true" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="right" style="vertical-align: middle; padding-right: 5px; width: 15%;">
                            <asp:LinkButton ID="lnkbtnNext" runat="server" CssClass="PaginationStyle" Style="text-decoration: none;"
                                OnClick="lnkbtnNext_Click" Text="<%$Resources: Language, LABEL_PAGINATION_NEXT_PAGE %>"></asp:LinkButton></td>
                    </tr>
                </table>
            </div>
            <br />            
            <asp:DataList ID="dlPaging" runat="server" OnItemCommand="dlPaging_ItemCommand" OnItemDataBound="dlPaging_ItemDataBound">
                <ItemTemplate>
                    <asp:LinkButton ID="lnkbtnPaging" runat="server" CommandArgument='<%# Eval("PageIndex") %>'
                        CommandName="lnkbtnPaging" Text='<%# Eval("PageText") %>'></asp:LinkButton>
                </ItemTemplate>
            </asp:DataList>
            <%End If %>
            <asp:DataList runat="server" ID="pricelist" RepeatColumns="1" RepeatDirection="Vertical"
                Style="margin-bottom: 20px;" CellPadding="0" CellSpacing="0" ExtractTemplateRows="true"
                Width="100%">
                <HeaderStyle CssClass="pricelistHeading" />
                <ItemStyle />
                <HeaderTemplate>
                    <asp:Table runat="server" ID="ht" CssClass="testclass1" CellSpacing="0" CellPadding="0">
                        <asp:TableRow ID="TableRow1" runat="server">
                            <asp:TableCell>
                            &nbsp;
                            </asp:TableCell>
                            <asp:TableCell CssClass="leftPricelistHeaderColumn">
                                <%--AM0122201001 - UOM - Start--%>
                                <asp:Label ID="lblProduct" runat="server" Text='<%$Resources: Language, LABEL_PRODUCT %>'
                                    Visible='<%# AppSettings("SHOW_TAX_TYPE") = "EXCL" And Not CBoolEx(AppSettings("EXPANDIT_US_USE_UOM"))%>' />
                                <%--AM0122201001 - UOM - End--%>
                            </asp:TableCell>
                            <asp:TableCell Style="text-align: center;" Visible='<%# AppSettings("SHOW_TAX_TYPE") = "EXCL" And Not CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) AND eis.CheckPageAccess("Prices")%>'>
                                <%--AM0122201001 - UOM - Start--%>
                                <asp:Label ID="lblPrice" runat="server" Text='<%$Resources: Language, LABEL_PRICE %>' />
                                <%--AM0122201001 - UOM - End--%>
                            </asp:TableCell>
                            <asp:TableCell Visible='<%# AppSettings("SHOW_TAX_TYPE") = "INCL"  And Not CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) AND eis.CheckPageAccess("Prices") %>'>
                                <%--AM0122201001 - UOM - Start--%>
                                <asp:Label ID="lblPriceInclTax" runat="server" Text='<%$Resources: Language, LABEL_PRICE %>' />
                                <%--AM0122201001 - UOM - End--%>
                            </asp:TableCell>
                            <asp:TableCell ColumnSpan="2" HorizontalAlign="Left">
                                <%--AM0122201001 - UOM - Start--%>
                                <asp:Label ID="lblQuantity" runat="server" Text='<%$Resources: Language, LABEL_QUANTITY %>'
                                    Visible='<%# Not CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) %>' />
                                <%--AM0122201001 - UOM - End--%>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Table runat="server" ID="it" CssClass="testclass2" CellSpacing="0" CellPadding="0">
                        <asp:TableRow Height="20px">
                            <asp:TableCell Style="vertical-align: middle; padding-right: 5px; padding-bottom: 10px;"
                                RowSpan="3">
                                <asp:Panel ID="Panel1" runat="server" Style="text-align: center;" Visible='<%# findThumbnail(Eval("Value")("ProductGuid")) <> "" %>'>
                                    <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl='<%# ProductLink(Eval("Value")("ProductGuid"), globals.GroupGuid)%>'>
                                        <asp:Image ID="Image2" Style='<%# findstyle(Eval("Value")("ProductGuid")) %>' ImageUrl='<%# findThumbnail(Eval("Value")("ProductGuid"))%>'
                                            runat="server" />
                                        
                                        <%--JA0531201001 - STAY IN CATALOG - START--%>
                                        <input id='img_<%# Eval("Value")("ProductGuid")%>' type="hidden" value='<%# findThumbnail(Eval("Value")("ProductGuid")) %>' />
                                        <%--JA0531201001 - STAY IN CATALOG - END--%>
                                            
                                    </asp:HyperLink>
                                </asp:Panel>
                                <%--<asp:Panel ID="Panel2" runat="server" Style="text-align: center;" Visible='<%# findThumbnail(Eval("Value")("ProductGuid")) = "" %>'>
                                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# ProductLink(Eval("Value")("ProductGuid"), globals.GroupGuid)%>'>
                                        <asp:Image ID="Image3" Style='<%# findstyle(Eval("Value")("ProductGuid")) %>' ImageUrl='~/images/nopix75.png'
                                            runat="server" />
                                    </asp:HyperLink>
                                </asp:Panel>--%>
                            </asp:TableCell>
                            <%--AM0122201001 - UOM - Start--%>
                            <asp:TableCell ColumnSpan="1">
                                <%--AM0122201001 - UOM - End--%>
                                <%  If CBoolEx(AppSettings("SHOW_PRODUCT_GUID_TEMPLATES")) Then%>
                                <asp:HyperLink ID="HyperLink2" CssClass="PricelistItemName" runat="server" NavigateUrl='<%# ProductLink(Eval("Value")("ProductGuid"), globals.GroupGuid)%>'
                                    Text='<%# Eval("Value")("ProductGuid")%>' />
                                <asp:HyperLink ID="HyperLink4" CssClass="PricelistItemName" runat="server" NavigateUrl='<%# ProductLink(Eval("Value")("ProductGuid"), globals.GroupGuid)%>'
                                    Text="/" />
                                <%End If%>
                                <asp:HyperLink ID="HyperLink3" CssClass="PricelistItemName" runat="server" NavigateUrl='<%# ProductLink(Eval("Value")("ProductGuid"), globals.GroupGuid)%>'
                                    Text='<%# Eval("Value")("ProductName")%>' />
                                <%--AM2010051801 - SPECIAL ITEMS - Start--%>
                                <%--AM2010051802 - WHAT'S NEW - Start--%>
                                <asp:Label ID="Label12" CssClass="SpecialPrice" runat="server" Style="font-weight: bold;"
                                    Text='<%# "&nbsp;" & Eval("Value")("LastName") %>' />
                                <%--AM2010051802 - WHAT'S NEW - End--%>
                                <%--AM2010051801 - SPECIAL ITEMS - End--%>
                            </asp:TableCell>
                            <%--AM0122201001 - UOM - Start--%>
                            <asp:TableCell Visible='<%# Not CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) %>'>
                            &nbsp;
                            </asp:TableCell>
                            <asp:TableCell ColumnSpan="3" Style="vertical-align: middle;" RowSpan="3" Visible='<%# CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) %>'>
                                <%--AM2010051801 - SPECIAL ITEMS - Start--%>
                                
                                <%--JA2010030901 - PERFORMANCE - Start--%>                                 
                                <%--<uc1:UOM ID="UOM1" ProductGuid='<%# Eval("Value")("ProductGuid") %>' ShowCartBtn="true"
                                    ShowFavoritesBtn="true" runat="server" PostBackURL='<%# addtocartlink(Eval("Value")("ProductGuid"))%>'
                                    DisplayMode="List" DefaultQty="1" IsSpecial='<%# CBoolEx(Eval("Value")("IsSpecial")) %>' />--%>

                                    <asp:Label ID="lblGuid" style="display:none;" runat="server" Text='<%# Eval("Value")("ProductGuid") %>'></asp:Label>                            
                                    <asp:Label ID="lblSpecial" style="display:none;"  runat="server" Text='<%# CBoolEx(Eval("Value")("IsSpecial")) %>'></asp:Label> 
                                    <asp:PlaceHolder ID="phUOM" runat="server" />
                                <%--JA2010030901 - PERFORMANCE - End--%> 
                                
                                <%--AM2010051801 - SPECIAL ITEMS - End--%>
                            </asp:TableCell>
                            <%--AM0122201001 - UOM - End--%>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell ColumnSpan="2">
                                <%--AM2010051902 - PRODUCT AVAILABILITY - Start--%>
                                    <uc1:ProductAvailability ID="ProductAvailability1" runat="server" QtyOnHand='<%# Eval("Value")("QtyOnHand") %>' DisplayMode="Stock" />
                                    <%--AM2010051902 - PRODUCT AVAILABILITY - End--%>
                            </asp:TableCell>
                            <%--AM0122201001 - UOM - Start--%>
                            <asp:TableCell Visible='<%# Not CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) %>'>
                            &nbsp;
                            </asp:TableCell>
                            <asp:TableCell Visible='<%# Not CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) %>'>
                            &nbsp;
                            </asp:TableCell>
                            <%--AM0122201001 - UOM - End--%>
                            <%--<asp:TableCell>
                            &nbsp;
                        </asp:TableCell>--%>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Width="200px">
                                <!-- Changed here to add only description to the template -->
                                <!--AM2010021901 - VARIANTS - Start-->
                                <%  If CBoolEx(AppSettings("EXPANDIT_US_USE_VARIANTS")) Then%>
                                <div style="margin-bottom: 5px;">
                                    <%--JA2010030901 - PERFORMANCE - Start--%>
                                    <%--<uc1:Variants ID="Variants1" runat="server" ProductGuid='<%# Eval("Value")("ProductGuid") %>' />--%>
                                    <asp:Label ID="lblGuidVariants" style="display:none;" runat="server" Text='<%# Eval("Value")("ProductGuid") %>'></asp:Label>
                                    <asp:PlaceHolder ID="phVariants" runat="server" />                            
                                    <%--JA2010030901 - PERFORMANCE - End--%> 
                                </div>
                                <%  End If%>
                                <!--AM2010021901 - VARIANTS - End-->
                                <div class="ProductPriceListUpdated_Description">
                                    <asp:HyperLink ID="hlDescription" runat="server" SkinID="ProductDisplayList_Description"
                                        Text='<%# findDescription(Eval("Value")("ProductGuid")) %>' NavigateUrl='<%# ProductLink(Eval("Value")("ProductGuid"), globals.groupguid) %>'><%#Eval("Value")("Name")%></asp:HyperLink>
                                </div>
                                <%--AM0122201001 - UOM - Start--%>
                                <%If Not CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) Then%>
                                <%--<expui:ExpHiddenField ID="SKU" Value='<%# HTMLEncode(Eval("Value")("ProductGuid")) %>'
                                    runat="server" />--%>
                                <%End If%>
                            </asp:TableCell>
                            <%--AM2010051801 - SPECIAL ITEMS - Start--%>
                            <%--It's not a special item--%>
                            <asp:TableCell Style="text-align: center;" CssClass="priceListPrice" Visible='<%# Not CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) and Not CBoolEx(Eval("Value")("IsSpecial")) AND eis.CheckPageAccess("Prices") AND AppSettings("SHOW_TAX_TYPE") = "EXCL"%>'>
                                
                                <uc1:PriceNoUom ID="PriceNoUom1" runat="server" styleText="text-align:center;" Visible='<%# AppSettings("SHOW_TAX_TYPE") = "EXCL" %>' Guid='<%# Eval("Value")("ProductGuid") %>' CssClassText="RegularPrice" Price='<%# CDblEx(Eval("Value")("ListPrice"))  %>' />
                                <%--<asp:Label ID="lblPrice1" runat="server" CssClass="RegularPrice" Text='<%# CurrencyFormatter.FormatAmount(CDblEx(Eval("Value")("ListPrice")), Session("UserCurrencyGuid").ToString)%>'
                                    Visible='<%# AppSettings("SHOW_TAX_TYPE") = "EXCL" %>' />--%>
                            </asp:TableCell>
                            <%--It's a special item--%>
                            <asp:TableCell Style="text-align: center;" CssClass="priceListPrice" Visible='<%# Not CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) and CBoolEx(Eval("Value")("IsSpecial")) AND eis.CheckPageAccess("Prices") AND AppSettings("SHOW_TAX_TYPE") = "EXCL" %>'>
                                <del class="SpecialPrice">
                                    <uc1:PriceNoUom ID="PriceNoUom2" runat="server" styleText="text-align:center;" Visible='<%# AppSettings("SHOW_TAX_TYPE") = "EXCL" %>' Guid='<%# Eval("Value")("ProductGuid") %>' CssClassText="SpecialPrice" Price='<%# CDblEx(Eval("Value")("originalListPrice"))  %>' />
                                    <%--<asp:Label ID="lblPrice1Original" runat="server" CssClass="SpecialPrice" Text='<%# CurrencyFormatter.FormatAmount(CDblEx(Eval("Value")("originalListPrice")), Session("UserCurrencyGuid").ToString)%>'
                                        Visible='<%# AppSettings("SHOW_TAX_TYPE") = "EXCL" %>' />--%>
                                </del>
                                <br />
                                <uc1:PriceNoUom ID="PriceNoUom3" runat="server" styleText="text-align:center;" Visible='<%# AppSettings("SHOW_TAX_TYPE") = "EXCL" %>' Guid='<%# Eval("Value")("ProductGuid") %>' CssClassText="RegularPrice" Price='<%# CDblEx(Eval("Value")("ListPrice"))  %>' />
                                <%--<asp:Label ID="lblPrice1s" runat="server" CssClass="RegularPrice" Text='<%# CurrencyFormatter.FormatAmount(CDblEx(Eval("Value")("ListPrice")), Session("UserCurrencyGuid").ToString)%>'
                                    Visible='<%# AppSettings("SHOW_TAX_TYPE") = "EXCL" %>' />--%>
                            </asp:TableCell>
                            <%--It's not a special item--%>
                            <asp:TableCell Style="text-align: center;" CssClass="priceListPrice" Visible='<%# Not CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) and Not CBoolEx(Eval("Value")("IsSpecial")) AND eis.CheckPageAccess("Prices")  AND AppSettings("SHOW_TAX_TYPE") = "INCL" %>'>
                                <uc1:PriceNoUom ID="PriceNoUom4" runat="server" styleText="text-align:center;" Visible='<%# AppSettings("SHOW_TAX_TYPE") = "INCL" %>' Guid='<%# Eval("Value")("ProductGuid") %>' CssClassText="RegularPrice" Price='<%# CDblEx(Eval("Value")("ListPriceInclTax"))  %>' />
                                <%--<asp:Label ID="lblPrice2" runat="server" CssClass="RegularPrice" Text='<%# CurrencyFormatter.FormatAmount(CDblEx(Eval("Value")("ListPriceInclTax")), Session("UserCurrencyGuid").ToString) %>'
                                    Visible='<%# AppSettings("SHOW_TAX_TYPE") = "INCL" %>' />--%>
                            </asp:TableCell>
                            <%--It's a special item--%>
                            <asp:TableCell Style="text-align: center;" CssClass="priceListPrice" Visible='<%# Not CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) and CBoolEx(Eval("Value")("IsSpecial")) AND eis.CheckPageAccess("Prices") AND AppSettings("SHOW_TAX_TYPE") = "INCL" %>'>
                                <del class="SpecialPrice">
                                    <uc1:PriceNoUom ID="PriceNoUom5" runat="server" styleText="text-align:center;" Visible='<%# AppSettings("SHOW_TAX_TYPE") = "INCL" %>' Guid='<%# Eval("Value")("ProductGuid") %>' CssClassText="SpecialPrice" Price='<%# CDblEx(Eval("Value")("originalListPrice"))  %>' />
                                    <%--<asp:Label ID="lblPrice2Original" runat="server" CssClass="SpecialPrice" Text='<%# CurrencyFormatter.FormatAmount(CDblEx(Eval("Value")("originalListPrice")), Session("UserCurrencyGuid").ToString)%>'
                                        Visible='<%# AppSettings("SHOW_TAX_TYPE") = "INCL" %>' />--%>
                                </del>
                                <br />
                                <uc1:PriceNoUom ID="PriceNoUom6" runat="server" styleText="text-align:center;" Visible='<%# AppSettings("SHOW_TAX_TYPE") = "INCL" %>' Guid='<%# Eval("Value")("ProductGuid") %>' CssClassText="priceListPrice" Price='<%# CDblEx(Eval("Value")("ListPriceInclTax"))  %>' />
                                <%--<asp:Label ID="lblPrice2s" runat="server" CssClass="priceListPrice" Text='<%# CurrencyFormatter.FormatAmount(CDblEx(Eval("Value")("ListPriceInclTax")), Session("UserCurrencyGuid").ToString) %>'
                                    Visible='<%# AppSettings("SHOW_TAX_TYPE") = "INCL" %>' />--%>
                            </asp:TableCell>
                            <%--AM2010051801 - SPECIAL ITEMS - End--%>
                            <asp:TableCell Style="text-align: left;" Visible='<%# Not CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) %>'>
                            <input type="text" style="margin-top:14px;" id="Quantity_<%# HTMLEncode(Eval("Value")("ProductGuid")) %>" onblur="setAddToCart();getPricesNoUom('<%# HTMLEncode(Eval("Value")("ProductGuid")) %>');" name="Quantity_<%# HTMLEncode(Eval("Value")("ProductGuid")) %>" size="2" tabindex="1" />
                            </asp:TableCell>
                            <%--AM0122201001 - UOM - End--%>
                        </asp:TableRow>
                    </asp:Table>
                </ItemTemplate>
            </asp:DataList>
            <%--AM0122201001 - UOM - Start--%>
            <%If eis.CheckPageAccess("Cart") Then%>
                <%If Not CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) Or CBoolEx(AppSettings("PRICELIST_UOM_SHOW_MAIN_ADD_TO_CART")) Then%>
                <div class="pricelisttemplate" align="right">
                    <table border="0" cellspacing="0" cellpadding="2">
                        <tr>
                            <td>
                                <%--<asp:Button ID="lbAddToCart" runat="server" CssClass="AddButton" Text="<%$Resources: Language,ACTION_ADD_TO_ORDER_PAD %>"
                                    PostBackUrl="~/cart.aspx?cartaction=add" TabIndex="2" />--%>
                                <input id="lbAddToCart" type="button" class="AddButton" value="<%= Resources.Language.ACTION_ADD_TO_ORDER_PAD %>" />
                                
                                <%--JA0531201001 - STAY IN CATALOG - START--%>
                                <a id="lightboxIdAddToCart" style="display:none;"  rel="lightbox" class=""
                                    href="<%=Vroot %>/App_Themes/EnterpriseBlue/p.gif">
                                    <img alt="" src="<%=Vroot %>/App_Themes/EnterpriseBlue/p.gif" />
                                </a>
                                <%--JA0531201001 - STAY IN CATALOG - END--%>                                 
                                
                                <input type="hidden" id="GroupGuidTemp" name="GroupGuidTemp" value="<%= Request("GroupGuid") %>" />
                                <input type="hidden" id="AddtoCartLabel" name="AddtoCartLabel" value="<%=Resources.Language.ACTION_ADDED_TO_CART %>" />
                            </td>
                        </tr>
                    </table>
                </div>
                <%End If%>
            <%End If%>
            <%--AM0122201001 - UOM - End--%>
            <%If AppSettings("EXPANDIT_US_USE_PAGINATION") Then%>
            <div>
                <table cellpadding="0" cellspacing="0" style="width: 100%;" class="PaginationStyle">
                    <tr>
                        <td align="left" style="vertical-align: middle; padding-left: 5px; width: 15%;">
                            <asp:LinkButton ID="lnkbtnPrevious2" CssClass="PaginationStyle" Style="text-decoration: none;"
                                runat="server" OnClick="lnkbtnPrevious_Click" Text="<%$Resources: Language, LABEL_PAGINATION_PREVIOUS_PAGE %>"></asp:LinkButton></td>
                        <td style="vertical-align: middle; width: 25%;" align="left">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="vertical-align: middle;">
                                        <asp:Label ID="Label3" CssClass="PaginationStyle" runat="server"><%=Resources.Language.LABEL_PAGINATION_PAGE %> </asp:Label>
                                    </td>
                                    <td style="padding-left: 5px; padding-right: 5px;">
                                        <asp:TextBox runat="server" ID="txtPage2" Style="font-family: Tahoma; padding-top: 1px;
                                            padding-bottom: 1px; text-align: center;" Width="20px" Font-Size="XX-Small" Height="10px"
                                            CssClass="PageNumber" AutoPostBack="true" OnTextChanged="txtPage2_TextChanged"></asp:TextBox>
                                    </td>
                                    <td style="vertical-align: middle;">
                                        <asp:Label ID="lblNumPages2" CssClass="PaginationStyle" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="vertical-align: middle; width: 25%;" align="left">
                        </td>
                        <td style="vertical-align: middle; width: 25%;" align="center">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="vertical-align: middle;">
                                        <asp:Label ID="Label5" CssClass="PaginationStyle" runat="server"><%=Resources.Language.LABEL_PAGINATION_ITEMS_PAGE  %> </asp:Label>
                                    </td>
                                    <td style="padding-left: 5px;">
                                        <asp:DropDownList ID="ddlPageSize2" Style="height: 16px; font-family: Tahoma; font-size: x-small;"
                                            AutoPostBack="true" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="right" style="vertical-align: middle; padding-right: 5px; width: 15%;">
                            <asp:LinkButton ID="lnkbtnNext2" runat="server" CssClass="PaginationStyle" Style="text-decoration: none;"
                                OnClick="lnkbtnNext_Click" Text="<%$Resources: Language, LABEL_PAGINATION_NEXT_PAGE %>"></asp:LinkButton></td>
                    </tr>
                </table>
            </div>
            <br />
            <br />
            <%End If %>   
            <input id="ProductsGuids" type="hidden" value="<%= test %>" />                    
        </div>
        <%End If%>

        <script type="text/javascript">
            function setAddToCart(notuse){
                var str='';
                var guid;
                var value;
                var variant;
                var guids=document.getElementById('ProductsGuids').value;
                var guidsArr=guids.split('_');
                for(i=0;i<guidsArr.length;i++){
                    guid=guidsArr[i];
                    if (document.getElementById('Quantity_' + guid) != null){
                        value = document.getElementById('Quantity_' + guid).value;        
                        if(value != ''){
                            str= str + '&SKU=' + guid + '&Quantity=' + value;
                            if (document.getElementById('VariantCode_' + guid) != null){
                                variant = document.getElementById('VariantCode_' + guid).value;     
                                if(variant != ''){
                                    str= str + '&VariantCode=' + variant;
                                }else{
                                    str= str + '&VariantCode=';
                                }   
                            }else{
                                str= str + '&VariantCode=';
                            }    
                        }
                    }
                }
                var group=document.getElementById('GroupGuidTemp');
                var eventClick=document.getElementById('lbAddToCart');
                eventClick.onclick =  function() {     
                    setEvent(eventClick,guid,str,group);  
                };
            }   
            function setEvent(myeventClick,myguid,mystr,mygroup){
                var label=document.getElementById('AddtoCartLabel').value;
                var e;
                e=window.event;
                AddToCart('~/cart.aspx?cartaction=add' + mystr + '&GroupGuid=' + mygroup.value);
                new Tooltip().schedule(myeventClick,e,label);
                
                //JA0531201001 - STAY IN CATALOG - START
                <%If CIntEx(AppSettings("STAY_IN_CATALOG")) = 0 Then %>
                    setTimeout('goToCart()',500);
//                <%ElseIf CIntEx(AppSettings("STAY_IN_CATALOG")) = 2 Then %>
//                    setTimeout('showLigthBox(' + myguid + ')',300); 
//                    document.getElementById('bottomNav').style.display='none';
                <%End If %>
                //JA0531201001 - STAY IN CATALOG - END
                
                return false;
            }
            
            //JA0531201001 - STAY IN CATALOG - START        
            function showLigthBox(myguid){
                
                var variant='';
                if (document.getElementById('VariantCode_' + myguid) != null){
                    variant = document.getElementById('VariantCode_' + myguid).value;
                    var index=document.getElementById('VariantCode_' + myguid).selectedIndex;
                    var varianName=document.getElementById('VariantCode_' + myguid).options[index].text;
                }    
                var image=document.getElementById('img_' + myguid).value;
                var url= '<%=Vroot %>' + "/AddToCartAjax.aspx?Guid=" + myguid + "&Image=" + image + "&VariantCode=" + variant + '&VariantName=' + varianName;
                var xhReq = new XMLHttpRequest();  
                xhReq.open("GET", url += (url.match(/\?/) == null ? "?" : "&") + (new Date()).getTime(), false);
                xhReq.send(null);
                var html=xhReq.responseText;
                var start=html.indexOf('<body>') + 6;
                var end=html.indexOf('</body>');
                html=html.substring(start,end);
                
                document.getElementById('lightboxIdAddToCart').className=html;
                
                actuateLink(document.getElementById('lightboxIdAddToCart'));            
                      
            }        
            function stayInPage(){
                var str =window.location.href;
                str=str.replace('#','');
                window.location=str;        
            }
            function goToCart(){
                window.location='<%=Vroot%>/cart.aspx';        
            } 
            
            function actuateLink(link){
               var allowDefaultAction = true;
                  
               if (link.click){
                  link.click();
                  return;
               }else if (document.createEvent){
                  var e = document.createEvent('MouseEvents');
                  e.initEvent(
                     'click'     // event type
                     ,true      // can bubble?
                     ,true      // cancelable?
                  );
                  allowDefaultAction = link.dispatchEvent(e);           
               }
                     
               if (allowDefaultAction){
                  var f = document.createElement('form');
                  f.action = link.href;
                  document.body.appendChild(f);
                  f.submit();
               }
            }
            //JA0531201001 - STAY IN CATALOG - END
            
            
            
            
            <%If eis.CheckPageAccess("Prices") AndAlso Not CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) Then%>
        
        
            window.onload=function(){ setParametersNotUom(); }


            function setParametersNotUom(){        
                var guids=document.getElementById('ProductsGuids').value;
                var arrayGuids=guids.split('_');
                for(i=0;i<arrayGuids.length;i++){
                    if (document.getElementById('VariantCode_' + arrayGuids[i]) != null){
                        var theElemet=document.getElementById('VariantCode_' + arrayGuids[i]);    
                        if("fireEvent" in theElemet){
                            theElemet.fireEvent("onchange");
                        }else{
                            var evt=document.createEvent("HTMLEvents");
                            evt.initEvent("change",false,true);
                            theElemet.dispatchEvent(evt);
                        }
                    }
                }
            }
            <%ElseIf eis.CheckPageAccess("Prices") AndAlso CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) Then%>        
        
            window.onload=function(){ setParameters(); }
            var iUom;
            function setParameters(){
                var guids=document.getElementById('ProductsGuids').value;
                var arrayGuids=guids.split('|');
                for(iUom=0;iUom<arrayGuids.length;iUom++){
                    var guid=arrayGuids[iUom];
                    var uom;
                    var theElemet;
                    var displayModeUoms=document.getElementById('DisplayModeUOM_' + guid).value;
                    if(displayModeUoms ==0){
                        theElemet=document.getElementById('Quantity_' + guid);    
                        if("fireEvent" in theElemet){
                            theElemet.fireEvent("onchange");
                        }else{
                            var evt=document.createEvent("HTMLEvents");
                            evt.initEvent("change",false,true);
                            theElemet.dispatchEvent(evt);
                        }      
                    }else{
                        var uoms= document.getElementById('UOMS_' + guid).value;
                        var uomsArr=uoms.split('_');
                        var i=0;
                        for(i=0;i<uomsArr.length;i++){
                            uom=uomsArr[i];
                            theElemet=document.getElementById('Quantity_' + guid + '_' + uom);  
                            if("fireEvent" in theElemet){
                                theElemet.fireEvent("onchange");
                            }else{
                                var evt=document.createEvent("HTMLEvents");
                                evt.initEvent("change",false,true);
                                theElemet.dispatchEvent(evt);
                            }                    
                        } 
                    }
                } 
            }
            
            <%End If%>
    
    
            
            
            
            
        </script>
    </asp:Panel>
</asp:Content>
<asp:Content ID="ContentBestSellers" ContentPlaceHolderID="ContentPlaceHolderBestSellers"
    runat="server">
    <% If AppSettings("SHOW_BEST_SELLERS") Then%>
        <%--JA2010030901 - PERFORMANCE -Start--%>
        <%--<uc1:BestSellersByCategoryBox ID="BestSellersByCategoryBox1" runat="server" />--%>  
        <asp:PlaceHolder ID="phMyMostPopularItems" runat="server" />        
        <%--<uc1:BestSellersByCategory ID="BestSellersByCategory1" runat="server" />--%>
        <%--JA2010030901 - PERFORMANCE -End--%>
    <%End If%>
</asp:Content>
