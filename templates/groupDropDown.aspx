<%--
Template information used by the Catalog Manager
#NAME=Group
#DESCRIPTION=Displays a group of subgroups and the products in the group.
#GROUPTEMPLATE=TRUE
#PRODUCTTEMPLATE=FALSE
--%>
<%@ Import Namespace="System.Configuration.ConfigurationManager" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="groupDropDown.aspx.vb" CodeFileBaseClass="ExpandIT.Page"
    Inherits="templates_groupDropDown" MasterPageFile="~/masters/default/main.master"
    RuntimeMasterPageFile="ThreeColumn.master" %>

<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Register Src="~/controls/ProductBoxSmall.ascx" TagName="ProductBox" TagPrefix="uc1" %>
<%@ Register Src="~/controls/ProductDisplayListDropDown.ascx" TagName="ProductDisplayList"
    TagPrefix="uc1" %>
<%@ Register Src="~/controls/GroupHeader.ascx" TagName="GroupHeader" TagPrefix="uc1" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<%@ Register Src="~/controls/Message.ascx" TagName="Message" TagPrefix="uc1" %>
<%@ Register Src="~/controls/boxes/BestSellersByCategoryBox.ascx" TagName="BestSellersByCategoryBox"
    TagPrefix="uc1" %>
<%@ Register Src="~/controls/BestSellersByCategory.ascx" TagName="BestSellersByCategory"
    TagPrefix="uc1" %>
<%@ Register Src="~/controls/ProductSortControl.ascx" TagName="SortControl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <div class="GroupPage">
        <script src="../script/Geometry.js" type="text/javascript"></script>
        <script src="../script/tooltip.js" type="text/javascript"></script>
        <script src="../script/addToCart.js" type="text/javascript"></script>
        <uc1:Message ID="Message1" runat="server" />
        <uc1:PageHeader ID="PageHeader1" runat="server" Text="" EnableTheming="true" />
        <uc1:GroupHeader ID="GroupHeader1" runat="server" />
        <input type="hidden" name="GroupGuid" value="<% = htmlencode(globals.GroupGuid) %>" />
        <% If Not Group("Products") Is Nothing Then%>
        <br />
        <%If AppSettings("EXPANDIT_US_USE_PAGINATION") Then%>
        <div>
            <table cellpadding="0" cellspacing="0" style="width: 100%;" class="PaginationStyle">
                <tr>
                    <td align="left" style="vertical-align: middle; padding-left: 5px; width: 15%;">
                        <asp:LinkButton ID="lnkbtnPrevious" CssClass="PaginationStyle" Style="text-decoration: none;"
                            runat="server" OnClick="lnkbtnPrevious_Click" Text="<%$Resources: Language, LABEL_PAGINATION_PREVIOUS_PAGE %>"></asp:LinkButton>
                    </td>
                    <td style="vertical-align: middle; width: 25%;" align="left">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="vertical-align: middle;">
                                    <asp:Label ID="Label1" CssClass="PaginationStyle" runat="server"><%=Resources.Language.LABEL_PAGINATION_PAGE %> </asp:Label>
                                </td>
                                <td style="padding-left: 5px; padding-right: 5px;">
                                    <asp:TextBox runat="server" ID="txtPage" Style="font-family: Tahoma; padding-top: 1px;
                                        padding-bottom: 1px; text-align: center;" Width="20px" Font-Size="XX-Small" Height="10px"
                                        CssClass="PageNumber" AutoPostBack="true" OnTextChanged="txtPage_TextChanged"></asp:TextBox>
                                </td>
                                <td style="vertical-align: middle;">
                                    <asp:Label ID="lblNumPages" CssClass="PaginationStyle" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="vertical-align: middle; width: 25%;" align="left">
                        <uc2:SortControl ID="SortControl1" runat="server"></uc2:SortControl>
                    </td>
                    <td style="vertical-align: middle; width: 25%;" align="center">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="vertical-align: middle;">
                                    <asp:Label ID="Label2" CssClass="PaginationStyle" runat="server"><%=Resources.Language.LABEL_PAGINATION_ITEMS_PAGE  %> </asp:Label>
                                </td>
                                <td style="padding-left: 5px;">
                                    <asp:DropDownList ID="ddlPageSize" Style="height: 16px; font-family: Tahoma; font-size: x-small;"
                                        AutoPostBack="true" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td align="right" style="vertical-align: middle; padding-right: 5px; width: 15%;">
                        <asp:LinkButton ID="lnkbtnNext" runat="server" CssClass="PaginationStyle" Style="text-decoration: none;"
                            OnClick="lnkbtnNext_Click" Text="<%$Resources: Language, LABEL_PAGINATION_NEXT_PAGE %>"></asp:LinkButton>
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <br />
        <asp:DataList ID="dlPaging" runat="server" OnItemCommand="dlPaging_ItemCommand" OnItemDataBound="dlPaging_ItemDataBound">
            <ItemTemplate>
                <asp:LinkButton ID="lnkbtnPaging" runat="server" CommandArgument='<%# Eval("PageIndex") %>'
                    CommandName="lnkbtnPaging" Text='<%# Eval("PageText") %>'></asp:LinkButton>
            </ItemTemplate>
        </asp:DataList>
        <%End If%>
        <asp:DataList ID="dlProducts" Style="width: 100%;" SkinID="GroupTemplate" runat="server"
            RepeatColumns="2" RepeatDirection="Horizontal" CssClass="GroupProductList" ItemStyle-HorizontalAlign="Center"
            ItemStyle-CssClass="GroupProductListItem">
            <ItemTemplate>
                <uc1:ProductDisplayList ID="ProductDisplayList1" runat="server" Product='<%# Eval("Value") %>' />
            </ItemTemplate>
        </asp:DataList>
        <input id="guidsArray" type="hidden" value="<%=guids %>" />
        <br />
    </div>
    <%If AppSettings("EXPANDIT_US_USE_PAGINATION") Then%>
    <br />
    <br />
    <div>
        <table cellpadding="0" cellspacing="0" style="width: 100%;" class="PaginationStyle">
            <tr>
                <td align="left" style="vertical-align: middle; padding-left: 5px; width: 15%;">
                    <asp:LinkButton ID="lnkbtnPrevious2" CssClass="PaginationStyle" Style="text-decoration: none;"
                        runat="server" OnClick="lnkbtnPrevious_Click" Text="<%$Resources: Language, LABEL_PAGINATION_PREVIOUS_PAGE %>"></asp:LinkButton>
                </td>
                <td style="vertical-align: middle; width: 25%;" align="left">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="vertical-align: middle;">
                                <asp:Label ID="Label3" CssClass="PaginationStyle" runat="server"><%=Resources.Language.LABEL_PAGINATION_PAGE %> </asp:Label>
                            </td>
                            <td style="padding-left: 5px; padding-right: 5px;">
                                <asp:TextBox runat="server" ID="txtPage2" Style="font-family: Tahoma; padding-top: 1px;
                                    padding-bottom: 1px; text-align: center;" Width="20px" Font-Size="XX-Small" Height="10px"
                                    CssClass="PageNumber" AutoPostBack="true" OnTextChanged="txtPage2_TextChanged"></asp:TextBox>
                            </td>
                            <td style="vertical-align: middle;">
                                <asp:Label ID="lblNumPages2" CssClass="PaginationStyle" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="vertical-align: middle; width: 25%;" align="left">
                </td>
                <td style="vertical-align: middle; width: 25%;" align="center">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="vertical-align: middle;">
                                <asp:Label ID="Label5" CssClass="PaginationStyle" runat="server"><%=Resources.Language.LABEL_PAGINATION_ITEMS_PAGE  %> </asp:Label>
                            </td>
                            <td style="padding-left: 5px;">
                                <asp:DropDownList ID="ddlPageSize2" Style="height: 16px; font-family: Tahoma; font-size: x-small;"
                                    AutoPostBack="true" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
                <td align="right" style="vertical-align: middle; padding-right: 5px; width: 15%;">
                    <asp:LinkButton ID="lnkbtnNext2" runat="server" CssClass="PaginationStyle" Style="text-decoration: none;"
                        OnClick="lnkbtnNext_Click" Text="<%$Resources: Language, LABEL_PAGINATION_NEXT_PAGE %>"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <br />
    <%End If%>
    <% =GetPriceInformation()%>
    <%End If%>
    
    
    <script type="text/javascript" language="javascript" >
        <%If eis.CheckPageAccess("Prices") AndAlso Not CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) Then%>
    
    
        window.onload=function(){ setParametersNotUom(); }
        var iNoUom;

        function setParametersNotUom(){        
            var guids=document.getElementById('guidsArray').value;
            var arrayGuids=guids.split('|');
            for(iNoUom=0;iNoUom<arrayGuids.length;iNoUom++){
                if (document.getElementById('VariantCode_' + arrayGuids[iNoUom]) != null){
                    var theElemet=document.getElementById('VariantCode_' + arrayGuids[iNoUom]);    
                    if("fireEvent" in theElemet){
                        theElemet.fireEvent("onchange");
                    }else{
                        var evt=document.createEvent("HTMLEvents");
                        evt.initEvent("change",false,true);
                        theElemet.dispatchEvent(evt);
                    }
                }
            }
        }
    
        <%ElseIf eis.CheckPageAccess("Prices") AndAlso CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) Then%>        
    
        window.onload=function(){ setParameters(); }
        var iUom;
        function setParameters(){
            var guids=document.getElementById('guidsArray').value;
            var arrayGuids=guids.split('|');
            for(iUom=0;iUom<arrayGuids.length;iUom++){
                var guid=arrayGuids[iUom];
                var uom;
                var theElemet;
                var displayModeUoms=document.getElementById('DisplayModeUOM_' + guid).value;
                if(displayModeUoms ==0){
                    theElemet=document.getElementById('Quantity_' + guid);    
                    if("fireEvent" in theElemet){
                        theElemet.fireEvent("onchange");
                    }else{
                        var evt=document.createEvent("HTMLEvents");
                        evt.initEvent("change",false,true);
                        theElemet.dispatchEvent(evt);
                    }        
                }else{
                    var uoms= document.getElementById('UOMS_' + guid).value;
                    var uomsArr=uoms.split('_');
                    var i=0;
                    for(i=0;i<uomsArr.length;i++){
                        uom=uomsArr[i];
                        theElemet=document.getElementById('Quantity_' + guid + '_' + uom);  
                        if("fireEvent" in theElemet){
                            theElemet.fireEvent("onchange");
                        }else{
                            var evt=document.createEvent("HTMLEvents");
                            evt.initEvent("change",false,true);
                            theElemet.dispatchEvent(evt);
                        }                    
                    } 
                }               
            } 
        }
        <%End If%>
    </script>
    
</asp:Content>
<asp:Content ID="ContentBestSellers" ContentPlaceHolderID="ContentPlaceHolderBestSellers"
    runat="server">
    <% If AppSettings("SHOW_BEST_SELLERS") Then%>
        <%--JA2010030901 - PERFORMANCE -Start--%>
        <%--<uc1:BestSellersByCategoryBox ID="BestSellersByCategoryBox1" runat="server" />--%>  
        <asp:PlaceHolder ID="phMyMostPopularItems" runat="server" />        
        <%--<uc1:BestSellersByCategory ID="BestSellersByCategory1" runat="server" />--%>
        <%--JA2010030901 - PERFORMANCE -End--%>
    <%End If%>
</asp:Content>
