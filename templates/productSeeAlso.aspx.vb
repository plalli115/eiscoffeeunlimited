Imports System.Configuration.ConfigurationManager
Imports ExpandIT.EISClass
Imports ExpandIT
Imports ExpandIT.ExpandITLib

Partial Class productSeeAlso
    Inherits ExpandIT.ShippingPayment._CartPage

    Protected bShowDocument As Boolean
    Protected bShowLink As Boolean
    Protected bUseSecondaryCurrency As Boolean
    Protected myColSpan As Integer
    Protected ProductDict As ExpDictionary
    Protected product As ProductClass
    Protected ProductGuid As String
    Protected relationtypekey As String
    Protected relations As ExpDictionary
    Protected relation As ExpDictionary
    Protected item As ExpDictionary
    Protected m_productclass As ProductClass
    Protected ZoomImagesBig
    Protected ZoomImagesSmall



    Protected VerticalSlideDict As New ExpDictionary


    'JA2010030901 - PERFORMANCE -Start
    Enum DisplayModes
        DropDown = 0
        List = 1
    End Enum
    'JA2010030901 - PERFORMANCE -End

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        ' Load the user and check the page access.
        eis.PageAccessRedirect("Catalog")

        ' Get ProductGuid and GroupGuid from QueryString and set variables.
        ProductGuid = Request("ProductGuid")


        If ProductGuid <> "" Then
            ' Load the product in to a dictionary.
            ProductDict = eis.CatDefaultLoadProduct(ProductGuid, True, True, True)
            m_productclass = New ProductClass(ProductDict)
            product = New ProductClass(ProductDict)



            'JA2010032101 - META TAGS -Start
            If product.ProductMetaTagKeywords() <> "" Then
                Dim metaKeywords As System.Web.UI.HtmlControls.HtmlMeta = New HtmlMeta
                metaKeywords.Name = "keywords"
                metaKeywords.Content = product.ProductMetaTagKeywords()
                Page.Header.Controls.Add(metaKeywords)
            End If

            If product.ProductMetaTagDescription() <> "" Then
                Dim metaDescription As System.Web.UI.HtmlControls.HtmlMeta = New HtmlMeta
                metaDescription.Name = "description"
                metaDescription.Content = product.ProductMetaTagDescription()
                Page.Header.Controls.Add(metaDescription)
            End If
            'JA2010032101 - META TAGS -End



            ' Check if the user is using secondary currency.
            bUseSecondaryCurrency = eis.UseSecondaryCurrency(User)

            ' Learn if a link is available for the product.
            bShowLink = CStrEx(ProductDict("HYPERLINK")) <> ""

            ' Learn if a document is available for the product.
            bShowDocument = CStrEx(ProductDict("ATTACHMENT")) <> ""

            ' Calculate ColSpan.
            If bUseSecondaryCurrency Then
                myColSpan = 4
            Else
                myColSpan = 3
            End If


            ' Add current location to breadcrumbtrail
            eis.InitCatalogStructure(globals.GroupGuid)
            globals.breadcrumbTrail.AddCurrentLocation(product.Name)

            ' Set page properties
            Me.Title = product.Name
            Me.createVerticalSlideControls()
            'JA2010061701 - SLIDE SHOW - Start
            If AppSettings("SHOW_SLIDE_ON_CART_CODE") Then
            Dim SlideDict As ExpDictionary = Nothing
            Dim products As ExpDictionary = New ExpDictionary()
            Dim slideProductDict As ExpDictionary = New ExpDictionary()
            slideProductDict("ProductGuid") = ProductGuid
            products("products") = slideProductDict

            SlideDict = eis.CreateRelatedItemsDictForSlide(products, 2) 'See Also
            Me.Master.SlideToShow.ImageDict = SlideDict
            End If
            'JA2010061701 - SLIDE SHOW - End

        End If
        fwProductForm.DataBind()

    End Sub


    'JA2010030901 - PERFORMANCE -Start

    Protected Sub fwProductForm_DataBound(ByVal sender As Object, ByVal e As EventArgs) Handles fwProductForm.DataBound

        'AM0122201001 - UOM - Start
        If AppSettings("EXPANDIT_US_USE_UOM") Then

            Dim myControl As Control = Me.LoadControl("~/controls/USUOM/UOM.ascx")

            myControl.ID = "UOM1"

            eis.SetControlProperties(myControl, "ProductGuid", CStrEx(ProductGuid))
            eis.SetControlProperties(myControl, "PostBackURL", addToCartLink(CStrEx(ProductGuid)))
            eis.SetControlProperties(myControl, "DisplayMode", DisplayModes.List)
            eis.SetControlProperties(myControl, "ShowCartBtn", True)
            eis.SetControlProperties(myControl, "ShowFavoritesBtn", True)
            eis.SetControlProperties(myControl, "DefaultQty", 1)
            eis.SetControlProperties(myControl, "IsSpecial", CBoolEx(ProductDict("IsSpecial")))
            eis.SetControlProperties(myControl, "ProductDictProperty", ProductDict)

            If findImages() Then
                Dim phUOM3 As PlaceHolder = CType(fwProductForm.FindControl("phUOM3"), PlaceHolder)
                phUOM3.Controls.Clear()
                phUOM3.Controls.Add(myControl)
            Else
                Dim phUOM1 As PlaceHolder = CType(fwProductForm.FindControl("phUOM1"), PlaceHolder)
                phUOM1.Controls.Clear()
                phUOM1.Controls.Add(myControl)
            End If


        End If
        'AM0122201001 - UOM - End

        'AM2010021901 - VARIANTS - Start
        If CBoolEx(AppSettings("EXPANDIT_US_USE_VARIANTS")) Then

            Dim myControlVariant As Control = Me.LoadControl("~/controls/USVariants/Variants.ascx")

            myControlVariant.ID = "Variants1"

            eis.SetControlProperties(myControlVariant, "ProductGuid", CStrEx(ProductGuid))
            eis.SetControlProperties(myControlVariant, "ProductDictPropertyVariant", ProductDict)
            eis.SetControlProperties(myControlVariant, "UOMDisplayMode", DisplayModes.List)

            Dim phVAR1 As PlaceHolder = CType(fwProductForm.FindControl("phVAR1"), PlaceHolder)
            Dim phVAR2 As PlaceHolder = CType(fwProductForm.FindControl("phVAR2"), PlaceHolder)


            If findImages() Then
                phVAR1.Controls.Clear()
                phVAR1.Controls.Add(myControlVariant)
            Else
                phVAR2.Controls.Clear()
                phVAR2.Controls.Add(myControlVariant)
            End If

        End If
        'AM2010021901 - VARIANTS - End

    End Sub

    'JA2010030901 - PERFORMANCE -End




    'AM2010062201 - VERTICAL SLIDE - START
    Public Sub createVerticalSlideControls()
        Dim controlToAdd As ExpandIT.UserControl
        Dim controlType As Type
        Dim controlProperty As System.Reflection.PropertyInfo

        'JA2010071601 - RATINGS AND REVIEWS - START
        If AppSettings("RATINGS_AND_REVIEWS_SHOW") Then
            controlToAdd = New ExpandIT.UserControl()
            controlToAdd = LoadControl(VRoot & "/controls/Rating/Rating.ascx")
            controlType = controlToAdd.GetType()
            Me.VerticalSlideDict.Add(Resources.Language.LABEL_RATINGS_REVIEWS, controlToAdd)
        End If
        'JA2010071601 - RATINGS AND REVIEWS - END


        'AM2010061801 - KITTING - START
        If CBoolEx(AppSettings("EXPANDIT_US_USE_KITTING")) And ProductDict("KittingInfo") IsNot Nothing Then
            controlToAdd = New ExpandIT.UserControl()
            controlToAdd = LoadControl(VRoot & "/controls/USKitting/KittingList.ascx")
            controlType = controlToAdd.GetType()
            controlProperty = controlType.GetProperty("KittingParts")
            If Not controlProperty Is Nothing Then
                controlProperty.SetValue(controlToAdd, ProductDict("KittingInfo"), Nothing)
            End If
            Me.VerticalSlideDict.Add(Resources.Language.VERTICAL_SLIDE_BUTTON_KITTING, controlToAdd)
        End If
        'AM2010061801 - KITTING - END
        If ProductDict("Related") IsNot Nothing Then
            If ProductDict("Related").Count > 0 Then
                For Each relationKey As String In ProductDict("Related").Keys
                    If relationKey <> SafeString(AppSettings("SHOW_SLIDE_ON_CART_CODE")) Then
                        controlToAdd = New ExpandIT.UserControl()
                        controlToAdd = LoadControl(VRoot & "/controls/ProductListSmall.ascx")
                        controlType = controlToAdd.GetType()
                        controlProperty = controlType.GetProperty("SlideShowId")
                        If Not controlProperty Is Nothing Then
                            controlProperty.SetValue(controlToAdd, relationKey, Nothing)
                        End If
                        controlProperty = controlType.GetProperty("Products")
                        If Not controlProperty Is Nothing Then
                            controlProperty.SetValue(controlToAdd, ProductDict("Related")(relationKey), Nothing)
                        End If
                        Me.VerticalSlideDict.Add(eis.GetRelationTypeName(relationKey), controlToAdd)
                    End If
                Next
            End If
        End If
    End Sub
    'AM2010062201 - VERTICAL SLIDE - END

    Protected Sub fwProductform_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim fw As FormView = sender

        fw.DataSource = New ProductClass() {product}
    End Sub

    Protected Sub dlRelated_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dl As DataList = sender

        dl.DataSource = ProductDict("Related")
    End Sub

    Protected Sub dlVariants_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dl As DataList = sender

        dl.DataSource = ProductDict("Variants")
    End Sub

    Public Function addToCartLink(ByVal productGuid As String)

        Dim postbackurl As String = "~/cart.aspx?cartaction=add"
        'SKU
        postbackurl = postbackurl & "&SKU=" & HTMLEncode(productGuid)
        'Qty
        Dim strQty As String = ""
        postbackurl = postbackurl & "&Quantity=1"
        postbackurl = postbackurl & "&GroupGuid=" & CStrEx(Request("GroupGuid"))
        Dim clientscript As String = "AddToCart('" & postbackurl & "&VariantCode=' + findSelectedValue('VariantCode'));new Tooltip().schedule(this,event,'" & Replace(Resources.Language.ACTION_ADDED_TO_CART, "'", "\'") & "');return false;"

        Return clientscript

    End Function

    Protected Function findImages() As Boolean

        'Big Images
        Dim di As New IO.DirectoryInfo(ApplicationRoot() & "/catalog/images/zoom/Product_Big")
        Dim aryFi As IO.FileInfo() = di.GetFiles()
        Dim fi As IO.FileInfo
        Dim name As String = ""
        Dim nameDict() As String
        Dim ZoomImagesBig2 As New ExpDictionary
        Dim ZoomImagesSmall2 As New ExpDictionary

        For Each fi In aryFi
            name = fi.Name
            nameDict = name.Split("_")
            If nameDict(0).ToString = CStrEx(Request("ProductGuid")) Then
                Dim propertiesBig As New ExpDictionary
                propertiesBig.Add("Index", nameDict(1))
                propertiesBig.Add("AltText", nameDict(2).Substring(0, nameDict(2).IndexOf(".")))
                propertiesBig.Add("Src", VRoot & "/catalog/images/zoom/Product_Big/" & fi.Name)
                ZoomImagesBig2.Add(nameDict(0) & "_" & nameDict(1), propertiesBig)
            End If
        Next
        Dim sortedBig As New System.Collections.Generic.SortedDictionary(Of String, Object)
        Dim item
        For Each item In ZoomImagesBig2.Values
            sortedBig.Add(item("Index"), item)
        Next
        ZoomImagesBig = sortedBig

        'Small Images
        Dim di2 As New IO.DirectoryInfo(ApplicationRoot() & "/catalog/images/zoom/Product_Small")
        Dim aryFi2 As IO.FileInfo() = di2.GetFiles()
        Dim fi2 As IO.FileInfo
        For Each fi2 In aryFi2
            name = fi2.Name
            nameDict = name.Split("_")
            If nameDict(0).ToString = CStrEx(Request("ProductGuid")) Then
                Dim pripertiesSmall As New ExpDictionary
                pripertiesSmall.Add("Index", nameDict(1))
                pripertiesSmall.Add("AltText", nameDict(2).Substring(0, nameDict(2).IndexOf(".")))
                pripertiesSmall.Add("Src", VRoot & "/catalog/images/zoom/Product_Small/" & fi2.Name)
                ZoomImagesSmall2.Add(nameDict(0) & "_" & nameDict(1), pripertiesSmall)
            End If
        Next
        Dim sortedSmall As New System.Collections.Generic.SortedDictionary(Of String, Object)
        Dim item2
        For Each item2 In ZoomImagesSmall2.Values
            sortedSmall.Add(item2("Index"), item2)
        Next
        ZoomImagesSmall = sortedSmall

        If ZoomImagesBig.Count <> 0 And ZoomImagesSmall.Count <> 0 Then
            Return True
        Else
            Return False
        End If

    End Function

    Protected Function getOriginalPrice()
        'ProductGuid = Request("ProductGuid")
        'product = New ProductClass(ProductDict)

        If CStrEx(product.OriginalPrice) = "0" Then
            Return 1
        Else
            Return CDblEx(product.OriginalPrice)
        End If
    End Function


    Protected Function getStyle()
        Dim style As String = m_productclass.ThumbnailURLStyle(100, 100)
        Return style
    End Function




End Class

