<%--
Template information used by the Catalog Manager
#NAME=Product
#DESCRIPTION=Displays a product with variants, relations and customer specific pricing.
#GROUPTEMPLATE=FALSE
#PRODUCTTEMPLATE=TRUE
--%>
<%@ Import Namespace="System.Configuration.ConfigurationManager" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="productQty.aspx.vb" Inherits="TemplateProductQty"
    MasterPageFile="~/masters/default/main.master" RuntimeMasterPageFile="ThreeColumn.master"
    CodeFileBaseClass="ExpandIT.Page" %>

<%--JA2010061701 - SLIDE SHOW - Start--%>
<%@ MasterType VirtualPath="~/masters/EnterpriseBlue/ThreeColumn.master" %>
<%--JA2010061701 - SLIDE SHOW - End--%>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Register Src="~/controls/ProductListSmall.ascx" TagName="ProductListSmall" TagPrefix="uc1" %>
<%--<%@ Register Src="~/controls/AddToCartButton.ascx" TagName="AddToCartButton" TagPrefix="uc1" %>--%>
<%--AM0122201001 - UOM - Start--%>
<%@ Register Src="~/controls/USUOM/UOM.ascx" TagName="UOM" TagPrefix="uc1" %>
<%--AM0122201001 - UOM - End--%>
<%--AM2010021901 - VARIANTS - Start--%>
<%@ Register Src="~/controls/USVariants/Variants.ascx" TagName="Variants" TagPrefix="uc1" %>
<%--AM2010021901 - VARIANTS - End--%>
<%--AM2010051902 - PRODUCT AVAILABILITY - Start--%>
<%@ Register Src="~/controls/USProductAvailability/ProductAvailability.ascx" TagName="ProductAvailability"
    TagPrefix="uc1" %>
<%--AM2010051902 - PRODUCT AVAILABILITY - End--%>
<%@ Register Src="~/controls/AddToFavoritesButton.ascx" TagName="AddToFavoritesButton"
    TagPrefix="uc1" %>
<%@ Register Src="~/controls/Message.ascx" TagName="Message" TagPrefix="uc1" %>
<%@ Register Namespace="ExpandIT.Buttons" TagPrefix="exbtn" %>
<%--AM2010062201 - VERTICAL SLIDE - START--%>
<%@ Register Src="~/controls/USVerticalSlide/VerticalSlide.ascx" TagName="VerticalSlide"
    TagPrefix="uc1" %>
<%--AM2010062201 - VERTICAL SLIDE - END--%>


<%@ Register Src="~/controls/PriceNoUom.ascx" TagName="PriceNoUom"
    TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">

    <script src="../script/Geometry.js" type="text/javascript"></script>

    <script src="../script/tooltip.js" type="text/javascript"></script>

    <script src="../script/addToCart.js" type="text/javascript"></script>

    <uc1:Message ID="Message1" runat="server" />
    <asp:FormView ID="fwProductForm" runat="server" EmptyDataText="No data" OnDataBinding="fwProductform_DataBinding"
        Style="width: 100%;">
        <ItemTemplate>
            <div class="ProductTemplate">
                <table cellpadding="0" cellspacing="0" class="ProductTemplate" style="width: 100%;">
                    <%If CStrEx(m_productclass.ProductPictureBig1()) <> "" Or findImages() Then%>
                    <%If CStrEx(m_productclass.ProductPictureBig1()) <> "" Then%>
                    <%-- ZOOM FUNCTIONALITY ********* Start--%>
                    <tr>
                        <td colspan="2">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td colspan="3" align="center">
                                        <a href="<%= CStrEx(m_productclass.ProductPictureBig1()) %>" id="zoom1" title="<%= CStrEx(m_productclass.ProductPictureAlt1()) %>"
                                            class="MagicZoom MagicThumb">
                                            <img alt="<%= CStrEx(m_productclass.ProductPictureAlt1()) %>" src="<%=CStrEx(m_productclass.ProductPictureSmall1()) %>" /></a>
                                        <input id="img_src" type="hidden" value="<%=CStrEx(m_productclass.ProductPictureSmall1()) %>" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="height: 20px;">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <!-- notice here how REl attribute references zoom ID -->
                                        <table cellpadding="0" cellspacing="0" style="width: 100%;">
                                            <tr>
                                                <%If Not CStrEx(m_productclass.ProductPictureBig1()) = "" Then%>
                                                <td align="center">
                                                    <a href="<%= CStrEx(m_productclass.ProductPictureBig1()) %>" rel="zoom1" rev="<%=CStrEx(m_productclass.ProductPictureSmall1()) %>"
                                                        title="<%= CStrEx(m_productclass.ProductPictureAlt1()) %>">
                                                        <img alt="<%= CStrEx(m_productclass.ProductPictureAlt1()) %>" style="width: 70px;
                                                            height: 51px; border: 0px;" src="<%=CStrEx(m_productclass.ProductPictureSmall1()) %>" />
                                                    </a>
                                                </td>
                                                <%End If%>
                                                <%If Not CStrEx(m_productclass.ProductPictureBig2()) = "" Then%>
                                                <td align="center">
                                                    <a href="<%=CStrEx(m_productclass.ProductPictureBig2()) %>" rel="zoom1" rev="<%=CStrEx(m_productclass.ProductPictureSmall2()) %>"
                                                        title="<%= CStrEx(m_productclass.ProductPictureAlt2()) %>">
                                                        <img alt="<%= CStrEx(m_productclass.ProductPictureAlt2()) %>" style="width: 70px;
                                                            height: 51px; border: 0px;" src="<%=CStrEx(m_productclass.ProductPictureSmall2()) %>" />
                                                    </a>
                                                </td>
                                                <%End If%>
                                                <%If Not CStrEx(m_productclass.ProductPictureBig3()) = "" Then%>
                                                <td align="center">
                                                    <a href="<%=CStrEx(m_productclass.ProductPictureBig3()) %>" rel="zoom1" rev="<%=CStrEx(m_productclass.ProductPictureSmall3()) %>"
                                                        title="<%= CStrEx(m_productclass.ProductPictureAlt3()) %>">
                                                        <img alt="<%= CStrEx(m_productclass.ProductPictureAlt3()) %>" style="width: 70px;
                                                            height: 51px; border: 0px;" src="<%= CStrEx(m_productclass.ProductPictureSmall3()) %>" />
                                                    </a>
                                                </td>
                                                <%End If%>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>

                            <script src="../script/mzp-packed.js" type="text/javascript"></script>

                            <link href="../App_Themes/EnterpriseBlue/styleProduct.css" rel="stylesheet" type="text/css"
                                media="screen" />
                            <%-- ZOOM FUNCTIONALITY ********** End--%>
                        </td>
                        <%ElseIf findImages() Then%>
                        <tr>
                            <td colspan="2">
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td colspan="3" align="center">
                                            <a href="<%= CStrEx(ZoomImagesBig(1)("Src")) %>" id="zoom1" title="<%= CStrEx(ZoomImagesBig(1)("AltText")) %>"
                                                class="MagicZoom MagicThumb">
                                                <img alt="<%= CStrEx(ZoomImagesBig(1)("AltText")) %>" src="<%=CStrEx(ZoomImagesSmall(1)("Src")) %>" /></a>
                                                <input id="img_src" type="hidden" value="<%=CStrEx(ZoomImagesSmall(1)("Src")) %>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" style="height: 20px;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <!-- notice here how REl attribute references zoom ID -->
                                            <table cellpadding="0" cellspacing="0" style="width: 100%;">
                                                <%Dim i As Integer = 1%>
                                                <tr>
                                                    <%For i = 1 To ZoomImagesSmall.count%>
                                                    <%If i = 1 Or i = 4 Or i = 7 Then%>
                                                    <td align="center">
                                                        <%ElseIf i = 2 Or i = 5 Or i = 8 Then%>
                                                        <td align="center">
                                                            <%ElseIf i = 3 Or i = 6 Or i = 9 Then%>
                                                            <td align="center">
                                                                <%End If%>
                                                                <%
                                                                    If CStrEx(ZoomImagesSmall(1)("Src")) <> "" Then%>
                                                                <a href="<%= CStrEx(ZoomImagesBig(i)("Src")) %>" rel="zoom1" rev="<%=CStrEx(ZoomImagesSmall(i)("Src")) %>"
                                                                    title="<%= CStrEx(ZoomImagesBig(i)("AltText")) %>">
                                                                    <img alt="<%= CStrEx(ZoomImagesBig(i)("AltText")) %>" style="width: 70px; height: 51px;
                                                                        border: 0px;" src="<%=CStrEx(ZoomImagesSmall(i)("Src")) %>" />
                                                                </a>
                                                            </td>
                                                            <%Else%>
                                                            <img alt="" style="width: 70px; height: 51px; border: 0px;" src="../images/p.gif" /></td>
                                                        <%End If%>
                                                        <%If i = 3 Or i = 6 Or i = 9 Then%>
                                                </tr>
                                                <%End If%>
                                                <%Next%>
                                            </table>
                                        </td>
                                    </tr>
                                </table>

                                <script src="../script/mzp-packed.js" type="text/javascript"></script>

                                <link href="../App_Themes/EnterpriseBlue/styleProduct.css" rel="stylesheet" type="text/css"
                                    media="screen" />
                                <%-- ZOOM FUNCTIONALITY ********** End--%>
                            </td>
                            <%End If%>
                            <td style="width: 10px;">
                            </td>
                            <td class="ProductTemplate_Right" style="width: 320px;">
                                <div class="ProductTemplate_Heading">
                                    <%  If CBoolEx(AppSettings("SHOW_PRODUCT_GUID_TEMPLATES")) Then%>
                                    <asp:Label ID="Label3" runat="server" Text='<%# HTMLEncode(Eval("Guid"))%>' />
                                    <asp:Label ID="Label6" runat="server" Text="/"></asp:Label>
                                    <%End If%>
                                    <asp:Label ID="Label8" runat="server" Text='<%# HTMLEncode(Eval("Name"))%>' />
                                    <%--AM2010051801 - SPECIAL ITEMS - Start--%>
                                    <%--AM2010051802 - WHAT'S NEW - Start--%>
                                    <asp:Label ID="Label13" runat="server" CssClass="SpecialPrice" Style="font-weight: bold;"
                                        Text='<%# "&nbsp;" & Eval("LastName") %>' />
                                    <%--AM2010051802 - WHAT'S NEW - End--%>
                                    <%--AM2010051801 - SPECIAL ITEMS - End--%>
                                </div>
                                <div class="ProductTemplate_Description">
                                    <!-- Changed here -->
                                    <asp:Literal ID="Literal2" runat="server" Text='<%# Eval("HTMLDescription")%>' />
                                </div>
                                <%--PRICE ******* START--%>
                                <%--<div class="ProductTemplate_Enlarge">
                                            <asp:HyperLink ID="HyperLink7" runat="server" NavigateUrl='<%# Eval("PictureURL") %>'
                                                Text="<%$Resources: Language, LABEL_CLICK_TO_ENLARGE%>"></asp:HyperLink>
                                        </div>--%>
                                <div class="ProductTemplate_Caption_Price">
                                    <asp:Label ID="Label10" runat="server" Text='<%$Resources: Language, LABEL_PRICE %>' />
                                </div>
                                <%--'AM0122201001 - UOM - Start--%>
                                <% 
                                    If Not CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) Then%>
                                <%--AM2010051801 - SPECIAL ITEMS - Start--%>
                                <asp:Panel runat="server" ID="pnlSpecialPricing" CssClass="ProductTemplate_SpecialPrice"
                                    Visible='<%# CBoolEx(Eval("IsSpecial")) AND eis.CheckPageAccess("Prices") %>'
                                    Style="text-align: left; padding-left: 12px;">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="padding-bottom: 7px;">
                                                <uc1:PriceNoUom ID="PriceNoUom2" runat="server" styleText="font-size:18px;text-align:left;" Guid='<%# Eval("Guid") %>' CssClassText="SpecialPrice" Price='<%# CDblEx(Eval("Price"))  %>' />
                                                <%--<asp:Label ID="Label24" runat="server" Style="font-size: 18px;" CssClass="SpecialPrice"
                                                    Text='<%# CurrencyFormatter.FormatCurrency(CDblEx(Eval("Price")), Session("UserCurrencyGuid").ToString)  %>' />--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ProductTemplate_SpecialPrice" style="padding-bottom: 5px;">
                                                <asp:Label ID="lblPrice" CssClass="RegularPrice" runat="server" Text="<%$Resources: Language, LABEL_SPECIAL_PRICE_LIST %>" />
                                                <del class="RegularPrice">
                                                    <uc1:PriceNoUom ID="PriceNoUom3" runat="server" styleText="font-size:12px;text-align:left;" Guid='<%# Eval("Guid") %>' CssClassText="RegularPrice" Price='<%# CDblEx(Eval("Price"))  %>' />
                                                    <%--<asp:Label ID="lblSpecialPrice" Style="font-size: 12px;" CssClass="RegularPrice"
                                                        runat="server" Text='<%# CurrencyFormatter.FormatCurrency(CDblEx(Eval("OriginalPrice")), Session("UserCurrencyGuid").ToString) %>' />--%>
                                                </del>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label15" CssClass="RegularPrice" runat="server" Text="<%$Resources: Language, LABEL_SPECIAL_PRICE_SAVE %>" /><br />
                                                <uc1:PriceNoUom ID="PriceNoUom4" runat="server" styleText="font-size:12px;text-align:left;" Guid='<%# Eval("Guid") %>' CssClassText="RegularPrice" Price='<%# CDblEx(Eval("OriginalPrice")) - CDblEx(Eval("Price"))  %>' />
                                                <%--<asp:Label ID="Label14" CssClass="RegularPrice" runat="server" Text='<%# CurrencyFormatter.FormatCurrency(CDblEx(Eval("OriginalPrice")) - CDblEx(Eval("Price")) , Session("UserCurrencyGuid").ToString) %>' />--%>
                                                <asp:Label ID="Label18" CssClass="RegularPrice" runat="server" Text=" (" />
                                                <asp:Label ID="Label17" CssClass="RegularPrice" runat="server" Text='<%# CInt(((CDblEx(Eval("OriginalPrice")) - CDblEx(Eval("Price"))) * 100) /  CDblEx(getOriginalPrice())) %>' />
                                                <asp:Label ID="Label20" CssClass="RegularPrice" runat="server" Text="%)" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="Panel1" runat="server" CssClass="ProductTemplate_Price" Style="text-align: left;
                                    padding-left: 12px;" Visible='<%# not CBoolEx(Eval("IsSpecial")) AND eis.CheckPageAccess("Prices")  %>'>
                                    <uc1:PriceNoUom ID="PriceNoUom1" runat="server" styleText="font-size:12px;text-align:left;" CssClassText="RegularPrice" Guid='<%# Eval("Guid") %>' Price='<%# CDblEx(Eval("Price"))  %>' />                                    
                                    <%--<asp:Label ID="Label11" CssClass="RegularPrice" runat="server" Text='<%# CurrencyFormatter.FormatCurrency(CDblEx(Eval("Price")), Session("UserCurrencyGuid").ToString)  %>' />--%>
                                </asp:Panel>
                                <%--AM2010051801 - SPECIAL ITEMS - End--%>
                                <%End If%>
                                <%--AM0122201001 - UOM - End--%>
                                <%--<asp:Panel ID="panel1" runat="server" Visible='<%# Eval("ShowSecondaryPrice") %>'>
                                            <div class="ProductTemplate_SecondaryPrice">
                                                <asp:Label ID="Label8" runat="server" Text='<%# CurrencyFormatter.FormatCurrency(CDblEx(Eval("SecondaryPrice")), Session("UserCurrencyGuid").ToString)  %>' />
                                            </div>
                                        </asp:Panel>--%>
                                <%--PRICE ******* END--%>
                                
                                <div class="ProductTemplate_Buttons" style="padding-left: 12px;">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td colspan="2">
                                                <%--AM2010051902 - PRODUCT AVAILABILITY - Start--%>
                                                <uc1:ProductAvailability ID="ProductAvailability2" runat="server" QtyOnHand='<%# Eval("QtyOnHand") %>'
                                                    DisplayMode="Stock" />
                                                <%--AM2010051902 - PRODUCT AVAILABILITY - End--%>
                                            </td>
                                        </tr>
                                        <%--'AM0122201001 - UOM - Start--%>
                                        <% If Not CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) Then%>
                                        <tr>
                                            <td style="height: 10px;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: middle; padding-right: 2px;">
                                                <asp:Label ID="Label16" Style="font-weight: bold;display:none;" runat="server" Text="<%$Resources: Language,LABEL_QUANTITY %>"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <input type="text" id="Quantity_<%# Eval("Guid") %>" name="Quantity" onblur="setAddToCart('<%# Eval("Guid") %>');getPricesNoUom('<%# Eval("Guid") %>');"
                                                    style="height: 15px; width: 20px; text-align: center;display:none;" value="1" />
                                            </td>
                                        </tr>
                                        <%End If%>
                                        <%--'AM0122201001 - UOM - End--%>                                        
                                    </table>
                                </div>
                                <div class="ProductTemplate_Buttons" style="padding-left: 12px;">
                                    <table cellpadding="0" cellspacing="0" class="ProductTemplate_Buttons" style="width: 100%;">
                                            <!--AM2010021901 - VARIANTS - Start-->
                                            <%If CBoolEx(AppSettings("EXPANDIT_US_USE_VARIANTS")) Then%>
                                        <tr>                                            
                                            <td colspan="3" style="text-align: left;">
                                                <%--JA2010030901 - PERFORMANCE - Start--%>
                                                <%-- <uc1:Variants ID="Variants3" runat="server" ProductGuid='<%# Eval("Guid") %>' />--%>
                                                <asp:PlaceHolder ID="phVAR1" runat="server" />
                                                <%--JA2010030901 - PERFORMANCE - End--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 20px;">
                                            </td>
                                        </tr>
                                            <%End If%>
                                            <!--AM2010021901 - VARIANTS - End-->
                                        <tr>                                            
                                            <td>
                                                <%If AppSettings("EXPANDIT_US_USE_UOM") Then%>
                                                    <%--JA2010030901 - PERFORMANCE - Start--%>
                                                    <%--AM0122201001 - UOM - Start--%>
                                                    <asp:PlaceHolder ID="phUOM3" runat="server" />    
                                                    <%--AM0122201001 - UOM - End--%> 
                                                    <%--JA2010030901 - PERFORMANCE - End--%>                                           
                                                <%Else %>
                                                <table cellpadding="0" cellspacing="0" style="width: 100%;">
                                                    <tr style="vertical-align: middle; text-align: left;">
                                                        <td style="text-align: left;">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td style="padding-left: 5px;">
                                                                        <%If (CBoolEx(globals.User("B2B")) Or CBoolEx(globals.User("B2C"))) And eis.CheckPageAccess("Favorites") Then%>
                                                                        <div style="position: static;">
                                                                            <uc1:AddToFavoritesButton ID="AddToFavoritesButton3" DisplayMode="Button" runat="server"
                                                                                ProductGuid='<%# Eval("Guid") %>' />
                                                                        </div>
                                                                        <%Else%>
                                                                        <div class="ProductTemplate_AddToFavoritesButton" style="position: static; background-color: Transparent;">
                                                                        </div>
                                                                        <%End If%>
                                                                    </td>
                                                                    <td style="height: 20px; width: 5px;">
                                                                    </td>
                                                                    <td style="padding-right: 5px;">
                                                                        <%If eis.CheckPageAccess("Cart") Then%>
                                                                        <div style="position: static; text-decoration: none;">
                                                                            <input id="lbAddToCartEnterprise" type="button" class="AddButton" onmouseover="setAddToCart('<%# Eval("Guid") %>');"
                                                                                value="<%= Resources.Language.ACTION_ADD_TO_ORDER_PAD %>"  />
                                                                            
                                                                            <%--JA0531201001 - STAY IN CATALOG - START--%>
                                                                            <a id="lightboxIdAddToCart"  style="display:none;" rel="lightbox" class=""
                                                                                href="<%=Vroot %>/App_Themes/EnterpriseBlue/p.gif">
                                                                                <img alt="" src="<%=Vroot %>/App_Themes/EnterpriseBlue/p.gif" />
                                                                            </a>
                                                                            <%--JA0531201001 - STAY IN CATALOG - END--%>
                                                                            
                                                                            <%--<exbtn:ExpLinkButton style="text-decoration:none;"  ID="ExpLinkButton2" runat="server" LabelText="ACTION_ADD_TO_ORDER_PAD"
                                                                                PostBackUrl='<%# addtocartlink(Eval("Guid"))%>' TabIndex="2" />--%>
                                                                            <input type="hidden" id="Hidden2" name="SKU" value='<%# Eval("Guid") %>' />
                                                                        </div>
                                                                        <%End If %>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <%End If %>                                                
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                
                                <% If HTMLEncode(ProductDict("ATTACHTEXT")) <> "" Or HTMLEncode(ProductDict("LINKTEXT")) <> "" Then%>
                                <div class="ProductTemplate_AdditionalInformation">
                                    <div class="ProductTemplate_AdditionalHeading">
                                        <%=Resources.Language.LABEL_ADDTIONAL_INFO%>
                                    </div>
                                    <div class="ProductTemplate_Attachment">
                                        <% If HTMLEncode(ProductDict("ATTACHTEXT")) <> "" Then%>
                                        <div class="AttachmentSubType<% = replace(ucase(System.IO.Path.GetExtension(cstrex(ProductDict("ATTACHMENT")))), ".", "") %>">
                                            <a target="_blank" href="<% = server.URLPathEncode(VRoot & "/" & ProductDict("ATTACHMENT")) %> ">
                                                <% =HTMLEncode(ProductDict("ATTACHTEXT"))%>
                                            </a>
                                        </div>
                                        <%End If%>
                                    </div>
                                    <% If HTMLEncode(ProductDict("LINKTEXT")) <> "" Then%>
                                    <div class="ProductTemplate_Hyperlink">
                                        <a target="_new" href="<% = CStrEx(ProductDict("HYPERLINK")) %>">
                                            <% =HTMLEncode(ProductDict("LINKTEXT"))%>
                                        </a>
                                    </div>
                                    <%End If%>
                                </div>
                                <%End If%>
                            </td>
                        </tr>
                        <%Else%>
                        <tr>
                            <td class="ProductTemplate_Left">
                                <div class="ProductTemplate_Picture">
                                    <asp:HyperLink ID="HyperLink2" runat="server" SkinID="ProductDisplayList_Thumbnail"
                                        NavigateUrl='<%# ProductLink(Eval("Guid"), globals.groupguid) %>'>
                                        <asp:Image ID="Image1" Style='<%# getStyle() %>' ImageUrl='<%# Eval("ThumbnailURL") %>'
                                            runat="server" />
                                        <input id="img_src" type="hidden" value='<%# Eval("ThumbnailURL") %>' />
                                    </asp:HyperLink>
                                </div>
                                <%--<div class="ProductTemplate_Enlarge">
                                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# Eval("PictureURL") %>'
                                        Text="<%$Resources: Language, LABEL_CLICK_TO_ENLARGE%>"></asp:HyperLink>
                                </div>--%>
                                <div class="ProductTemplate_Caption_Price">
                                    <asp:Label ID="Label5" runat="server" Text='<%$Resources: Language, LABEL_PRICE %>' />
                                </div>
                                <%--AM0122201001 - UOM - Start--%>
                                <% If Not CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) Then%>
                                <%--AM2010051801 - SPECIAL ITEMS - Start--%>
                                <asp:Panel runat="server" ID="Panel2" CssClass="ProductTemplate_SpecialPrice" Visible='<%# CBoolEx(Eval("IsSpecial")) AND eis.CheckPageAccess("Prices")  %>'>
                                    <table cellpadding="0" cellspacing="0" style="text-align: left;">
                                        <tr>
                                            <td style="padding-bottom: 7px;">
                                                <uc1:PriceNoUom ID="PriceNoUom5" runat="server" styleText="font-size:18px;text-align:left;" Guid='<%# Eval("Guid") %>' CssClassText="SpecialPrice" Price='<%# CDblEx(Eval("Price"))  %>' />
                                                <%--<asp:Label ID="Label21" runat="server" Style="font-size: 18px;" CssClass="SpecialPrice"
                                                    Text='<%# CurrencyFormatter.FormatCurrency(CDblEx(Eval("Price")), Session("UserCurrencyGuid").ToString)  %>' />--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ProductTemplate_SpecialPrice" style="padding-bottom: 5px;">
                                                <asp:Label ID="Label22" CssClass="RegularPrice" runat="server" Text="<%$Resources: Language, LABEL_SPECIAL_PRICE_LIST %>" />
                                                <del class="RegularPrice">
                                                    <uc1:PriceNoUom ID="PriceNoUom6" runat="server" styleText="font-size:12px;text-align:left;" Guid='<%# Eval("Guid") %>' CssClassText="RegularPrice" Price='<%# CDblEx(Eval("OriginalPrice"))  %>' />
                                                    <%--<asp:Label ID="Label23" Style="font-size: 12px;" CssClass="RegularPrice" runat="server"
                                                        Text='<%# CurrencyFormatter.FormatCurrency(CDblEx(Eval("OriginalPrice")), Session("UserCurrencyGuid").ToString) %>' />--%>
                                                </del>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label25" CssClass="RegularPrice" runat="server" Text="<%$Resources: Language, LABEL_SPECIAL_PRICE_SAVE %>" /><br />
                                                <uc1:PriceNoUom ID="PriceNoUom7" runat="server" styleText="font-size:12px;text-align:left;" CssClassText="RegularPrice" Guid='<%# Eval("Guid") %>' Price='<%# CDblEx(Eval("OriginalPrice")) - CDblEx(Eval("Price"))  %>' />
                                                <%--<asp:Label ID="Label26" CssClass="RegularPrice" runat="server" Text='<%# CurrencyFormatter.FormatCurrency(CDblEx(Eval("OriginalPrice")) - CDblEx(Eval("Price")) , Session("UserCurrencyGuid").ToString) %>' />--%>
                                                <asp:Label ID="Label27" CssClass="RegularPrice" runat="server" Text=" (" />
                                                <asp:Label ID="Label28" CssClass="RegularPrice" runat="server" Text='<%# CInt(((CDblEx(Eval("OriginalPrice")) - CDblEx(Eval("Price"))) * 100) /  CDblEx(getOriginalPrice())) %>' />
                                                <asp:Label ID="Label29" CssClass="RegularPrice" runat="server" Text="%)" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="Panel3" runat="server" CssClass="ProductTemplate_Price" Visible='<%# Not CBoolEx(Eval("IsSpecial")) AND eis.CheckPageAccess("Prices")  %>'>
                                    <uc1:PriceNoUom ID="PriceNoUom8" runat="server" styleText="font-size:12px;text-align:left;" CssClassText="RegularPrice" Guid='<%# Eval("Guid") %>' Price='<%# CDblEx(Eval("Price"))  %>' />
                                    <%--<asp:Label ID="Label4" CssClass="RegularPrice" runat="server" Text='<%# CurrencyFormatter.FormatCurrency(CDblEx(Eval("Price")), Session("UserCurrencyGuid").ToString)  %>' />--%>
                                </asp:Panel>
                                <%--AM2010051801 - SPECIAL ITEMS - End--%>
                                <%End If%>
                                <%--AM0122201001 - UOM - End--%>
                                <%-- <asp:Panel ID="panelSecondaryPrice" runat="server" Visible='<%# Eval("ShowSecondaryPrice") %>'>
                                        <div class="ProductTemplate_SecondaryPrice">
                                            <asp:Label ID="Label3" runat="server" Text='<%# CurrencyFormatter.FormatCurrency(CDblEx(Eval("SecondaryPrice")), Session("UserCurrencyGuid").ToString)  %>' />
                                        </div>
                                    </asp:Panel>--%>
                            </td>
                            <td class="ProductTemplate_Right">
                                <div class="ProductTemplate_Heading">
                                    <%If CBoolEx(AppSettings("SHOW_PRODUCT_GUID_TEMPLATES")) Then%>
                                    <asp:Label ID="Label7" runat="server" Text='<%# HTMLEncode(Eval("Guid"))%>' />
                                    <asp:Label ID="Label9" runat="server" Text="/"></asp:Label>
                                    <%End If%>
                                    <asp:Label ID="lblHeading" runat="server" Text='<%# HTMLEncode(Eval("Name"))%>' />
                                    <%--AM2010051801 - SPECIAL ITEMS - Start--%>
                                    <%--AM2010051802 - WHAT'S NEW - Start--%>
                                    <asp:Label ID="Label12" runat="server" CssClass="SpecialPrice" Style="font-weight: bold;"
                                        Text='<%# "&nbsp;" & Eval("LastName") %>' />
                                    <%--AM2010051802 - WHAT'S NEW - End--%>
                                    <%--AM2010051801 - SPECIAL ITEMS - End--%>
                                </div>
                                <div class="ProductTemplate_Description">
                                    <!-- Changed here -->
                                    <asp:Literal ID="lblHTMLDescription" runat="server" Text='<%# Eval("HTMLDescription")%>' />
                                </div>
                               
                                
                                <div class="ProductTemplate_Buttons" style="padding-left: 12px;">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td colspan="2">
                                                <%--AM2010051902 - PRODUCT AVAILABILITY - Start--%>
                                                <uc1:ProductAvailability ID="ProductAvailability4" runat="server" QtyOnHand='<%# Eval("QtyOnHand") %>'
                                                    DisplayMode="Stock" />
                                                <%--AM2010051902 - PRODUCT AVAILABILITY - End--%>
                                            </td>
                                        </tr>
                                        
                                        <%--'AM0122201001 - UOM - Start--%>
                                        <% If Not CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) Then%>
                                        <tr>
                                            <td style="height: 10px;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: middle; padding-right: 2px;">
                                                <asp:Label ID="Label19" Style="font-weight: bold;display:none;" runat="server" Text="<%$Resources: Language,LABEL_QUANTITY %>"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <input type="text" id="Quantity_<%# Eval("Guid") %>" name="Quantity" onblur="setAddToCart('<%# Eval("Guid") %>');getPricesNoUom('<%# Eval("Guid") %>');"
                                                    style="height: 15px; width: 20px; text-align: center;display:none;" value="1" />
                                            </td>
                                        </tr>
                                        <%End If%>
                                        <%--'AM0122201001 - UOM - End--%>
                                    </table>
                                </div>
                                <div class="ProductTemplate_Buttons" style="width: 438px;">
                                    <table cellpadding="0" cellspacing="0" class="ProductTemplate_Buttons">
                                        <tr>
                                            <!--AM2010021901 - VARIANTS - Start-->
                                            <%If CBoolEx(AppSettings("EXPANDIT_US_USE_VARIANTS")) Then%>
                                            <td colspan="3" style="text-align: left;">
                                                <%--JA2010030901 - PERFORMANCE - Start--%>
                                                <%--<uc1:Variants ID="Variants1" runat="server" ProductGuid='<%# Eval("Guid") %>' />--%>
                                                <asp:PlaceHolder ID="phVAR2" runat="server" />
                                                <%--JA2010030901 - PERFORMANCE - End--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 20px;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <%End If%>
                                            <!--AM2010021901 - VARIANTS - End-->
                                        </tr>
                                    </table>
                                </div>
                                
                                <%--'AM0122201001 - UOM - Start--%>
                                <% If AppSettings("EXPANDIT_US_USE_UOM") Then%>
                                    <%--JA2010030901 - PERFORMANCE - Start--%>
                                    <asp:PlaceHolder ID="phUOM1" runat="server" />
                                    <%--JA2010030901 - PERFORMANCE - End--%>
                                <%Else %>
                                <table cellpadding="0" cellspacing="0" style="width: 260px;">
                                    <tr style="vertical-align: middle; text-align: center;">
                                        <td style="width: 212px; padding-right: 5px;" class="ProductTemplate_AddToFavoritesButton">
                                            <%If (CBoolEx(globals.User("B2B")) Or CBoolEx(globals.User("B2C"))) And eis.CheckPageAccess("Favorites") Then%>
                                            <div style="position: static;">
                                                <uc1:AddToFavoritesButton ID="AddToFavoritesButton1" DisplayMode="Button" runat="server"
                                                    ProductGuid='<%# Eval("Guid") %>' />
                                            </div>
                                            <%Else%>
                                            <div class="ProductTemplate_AddToFavoritesButton" style="position: static; background-color: Transparent;">
                                            </div>
                                            <%End If%>
                                        </td>
                                        <td style="width: 212px; padding-right: 5px;">
                                            <%If eis.CheckPageAccess("Cart") Then%>
                                            <div style="position: static; text-decoration: none;">
                                                <input id="lbAddToCartEnterprise" type="button" class="AddButton" onmouseover="setAddToCart('<%# Eval("Guid") %>');"
                                                    value="<%= Resources.Language.ACTION_ADD_TO_ORDER_PAD %>" />
                                                    
                                                <%--JA0531201001 - STAY IN CATALOG - START--%>
                                                <a id="lightboxIdAddToCart" style="display:none;"  rel="lightbox" class=""
                                                    href="<%=Vroot %>/App_Themes/EnterpriseBlue/p.gif">
                                                    <img alt="" src="<%=Vroot %>/App_Themes/EnterpriseBlue/p.gif" />
                                                </a>
                                                <%--JA0531201001 - STAY IN CATALOG - END--%>   
                                                 
                                                <%--<exbtn:ExpLinkButton style="text-decoration:none;"  ID="lbAddToCart" runat="server" LabelText="ACTION_ADD_TO_ORDER_PAD"
                                                            PostBackUrl='<%# addtocartlink(Eval("Guid"))%>' TabIndex="2" />--%>
                                                <input type="hidden" id="SKU" name="SKU" value='<%# Eval("Guid") %>' />
                                            </div>
                                            <%End If %>
                                        </td>
                                    </tr>
                                </table>
                                <%End If%>
                                <%--AM0122201001 - UOM - End--%>
                                <% If HTMLEncode(ProductDict("ATTACHTEXT")) <> "" Or HTMLEncode(ProductDict("LINKTEXT")) <> "" Then%>
                                <div class="ProductTemplate_AdditionalInformation">
                                    <div class="ProductTemplate_AdditionalHeading">
                                        <%=Resources.Language.LABEL_ADDTIONAL_INFO%>
                                    </div>
                                    <div class="ProductTemplate_Attachment">
                                        <% If HTMLEncode(ProductDict("ATTACHTEXT")) <> "" Then%>
                                        <div class="AttachmentSubType<% = replace(ucase(System.IO.Path.GetExtension(cstrex(ProductDict("ATTACHMENT")))), ".", "") %>">
                                            <a href="<% = server.URLPathEncode(VRoot & "/" & ProductDict("ATTACHMENT")) %> ">
                                                <% =HTMLEncode(ProductDict("ATTACHTEXT"))%>
                                            </a>
                                        </div>
                                        <%End If%>
                                    </div>
                                    <% If HTMLEncode(ProductDict("LINKTEXT")) <> "" Then%>
                                    <div class="ProductTemplate_Hyperlink">
                                        <a target="_new" href="<% = CStrEx(ProductDict("HYPERLINK")) %>">
                                            <% =HTMLEncode(ProductDict("LINKTEXT"))%>
                                        </a>
                                    </div>
                                    <%End If%>
                                </div>
                                <%End If%>
                            </td>
                        </tr>
                        <%End If%>
                        <input type="hidden" id="GroupGuidTemp" name="GroupGuidTemp" value="<%= Request("GroupGuid") %>" />
                        <input type="hidden" id="AddtoCartLabel" name="AddtoCartLabel" value="<%=Resources.Language.ACTION_ADDED_TO_CART %>" />
                </table>
                <%--<div class="ProductTemplate_Related">
                    <asp:DataList ID="dlRelated" SkinID="ProductTemplate" runat="server" OnDataBinding="dlRelated_DataBinding">
                        <ItemTemplate>
                            <div class="ProductTemplate_RelatedListItem">
                                <div class="ProductTemplate_RelatedHeading">
                                    <asp:Label ID="lblRelationHeading" runat="server" Text='<%# HTMLEncode(eis.GetRelationTypeName(Eval("Key"))) %>'></asp:Label>
                                </div>
                                <uc1:ProductListSmall ID="ProductListSmall1" SlideShowId='<%# Eval("Key") %>' runat="server"
                                    Products='<%# Eval("Value")%>' />
                            </div>
                        </ItemTemplate>
                        <SeparatorTemplate>
                            <div class="ProductTemplate_Separator">
                                &nbsp;
                            </div>
                        </SeparatorTemplate>
                    </asp:DataList>
                </div>--%>
                <%--AM2010062201 - VERTICAL SLIDE - START--%>
                <%If VerticalSlideDict.Count > 0 Then%>
                <div>
                    <uc1:VerticalSlide ID="VerticalSlide1" runat="server" ControlsDict='<%# VerticalSlideDict %>' />
                </div>
                <%End If%>
                <%--AM2010062201 - VERTICAL SLIDE - END--%>
                <div class="ProductTemplate_Caption_SKU">
                    <asp:Label ID="Label1" runat="server" Text='<%$Resources: Language, LABEL_SKU %>' />
                </div>
                <div class="ProductTemplate_SKU">
                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("Guid") %>' />
                </div>
            </div>
        </ItemTemplate>
    </asp:FormView>
    <br />
    <% =GetPriceInformation()%>

    <script type="text/javascript">
        function setAddToCart(guid){
            var str='';
            var value;
            var variant;
            str= str + '&SKU=' + guid + '&Quantity=1';  
            if (document.getElementById('VariantCode_' + guid) != null){
                variant = document.getElementById('VariantCode_' + guid).value;     
                if(variant != ''){
                    str= str + '&VariantCode=' + variant;
                }else{
                    str= str + '&VariantCode=';
                }   
            }else{
                str= str + '&VariantCode=';
            }
            var group=document.getElementById('GroupGuidTemp');
            var eventClick=document.getElementById('lbAddToCartEnterprise');
            eventClick.onclick =  function() {     
                setEvent(eventClick,guid,str,group);  
            };
        }   
        function setEvent(myeventClick,myguid,mystr,mygroup){
            var label=document.getElementById('AddtoCartLabel').value;
            var e;
            e=window.event;
            AddToCart('~/cart.aspx?cartaction=add' + mystr + '&GroupGuid=' + mygroup.value);
            new Tooltip().schedule(myeventClick,e,label);
            
            //JA0531201001 - STAY IN CATALOG - START
            <%If CIntEx(AppSettings("STAY_IN_CATALOG")) = 0 Then %>
                setTimeout('goToCart()',500);
            <%ElseIf CIntEx(AppSettings("STAY_IN_CATALOG")) = 2 Then %>
                setTimeout('showLigthBox(' + myguid + ')',300); 
                document.getElementById('bottomNav').style.display='none';
            <%End If %>
            //JA0531201001 - STAY IN CATALOG - END
            
            
            return false;
            
        }
        //JA0531201001 - STAY IN CATALOG - START        
        function showLigthBox(myguid){
            
            var variant='';
            if (document.getElementById('VariantCode_' + myguid) != null){
                variant = document.getElementById('VariantCode_' + myguid).value;
                var index=document.getElementById('VariantCode_' + myguid).selectedIndex;
                var varianName=document.getElementById('VariantCode_' + myguid).options[index].text;
            }    
            
            var imageSrc=document.getElementById('img_src').value;
            
            var url= '<%=Vroot %>' + "/AddToCartAjax.aspx?Guid=" + myguid + "&Image=" + imageSrc + "&VariantCode=" + variant + '&VariantName=' + varianName;
            var xhReq = new XMLHttpRequest();  
            xhReq.open("GET", url += (url.match(/\?/) == null ? "?" : "&") + (new Date()).getTime(), false);
            xhReq.send(null);
            var html=xhReq.responseText;
            var start=html.indexOf('<body>') + 6;
            var end=html.indexOf('</body>');
            html=html.substring(start,end);
            
            document.getElementById('lightboxIdAddToCart').className=html;
            
            actuateLink(document.getElementById('lightboxIdAddToCart'));            
                  
        }        
        function stayInPage(){
            var str =window.location.href;
            str=str.replace('#','');
            window.location=str;        
        }
        function goToCart(){
            window.location='<%=Vroot%>/cart.aspx';        
        } 
        
        function actuateLink(link){
           var allowDefaultAction = true;
              
           if (link.click){
              link.click();
              return;
           }else if (document.createEvent){
              var e = document.createEvent('MouseEvents');
              e.initEvent(
                 'click'     // event type
                 ,true      // can bubble?
                 ,true      // cancelable?
              );
              allowDefaultAction = link.dispatchEvent(e);           
           }
                 
           if (allowDefaultAction){
              var f = document.createElement('form');
              f.action = link.href;
              document.body.appendChild(f);
              f.submit();
           }
        }
        //JA0531201001 - STAY IN CATALOG - END
        
        
        <%If eis.CheckPageAccess("Prices") AndAlso Not CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) Then%>
    
    
        window.onload=function(){ setParametersNotUom(); }


        function setParametersNotUom(){        
            var guid='<%= Request("ProductGuid") %>';
            if (document.getElementById('VariantCode_' + guid) != null){
                var theElemet=document.getElementById('VariantCode_' + guid);    
                if("fireEvent" in theElemet){
                    theElemet.fireEvent("onchange");
                }else{
                    var evt=document.createEvent("HTMLEvents");
                    evt.initEvent("change",false,true);
                    theElemet.dispatchEvent(evt);
                }
            }
        }
        <%ElseIf eis.CheckPageAccess("Prices") AndAlso CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) Then%>   
    
        window.onload=function(){ setParameters(); }
            
        function setParameters(){
            var guid='<%= Request("ProductGuid") %>';
            var uom;
            var theElemet;
            var displayModeUoms=document.getElementById('DisplayModeUOM_' + guid).value;
            if(displayModeUoms ==0){
                theElemet=document.getElementById('Quantity_' + guid);    
                if("fireEvent" in theElemet){
                    theElemet.fireEvent("onchange");
                }else{
                    var evt=document.createEvent("HTMLEvents");
                    evt.initEvent("change",false,true);
                    theElemet.dispatchEvent(evt);
                }     
            }else{
                var uoms= document.getElementById('UOMS_' + guid).value;
                
                var uomsArr=uoms.split('_');
                var i=0;
                for(i=0;i<uomsArr.length;i++){
                    uom=uomsArr[i];
                    theElemet=document.getElementById('Quantity_' + guid + '_' + uom);  
                    if("fireEvent" in theElemet){
                        theElemet.fireEvent("onchange");
                    }else{
                        var evt=document.createEvent("HTMLEvents");
                        evt.initEvent("change",false,true);
                        theElemet.dispatchEvent(evt);
                    }                    
                } 
            } 
        }
        
        <%End If%>  
    </script>


</asp:Content>
