Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT.Debug
Imports System.Collections.Generic
Imports System.Data

Partial Class templates_pricelistLineQty
    Inherits ExpandIT.ShippingPayment._CartPage

    Public bUseSecondaryCurrency As Object
    Public Group As Object
    Public info As Object
    Public IsGroupAvailable As Object
    Public MyCalcColSpan As Object
    Public product As Object
    Public productinfo As Object
    Protected objPage As New PagedDataSource()
    Protected test As String = ""

    Protected page1 As String = "0"
    Protected page2 As String = "0"
    Protected page3 As String = "0"

    'JA2010030901 - PERFORMANCE -Start
    Enum DisplayModes
        DropDown = 0
        List = 1
    End Enum
    'JA2010030901 - PERFORMANCE -End



    Public Property CurrentPage() As Integer
        Get
            If (Me.ViewState("CurrentPage") Is Nothing) Then
                Return 0
            Else
                Return Integer.Parse(Me.ViewState("CurrentPage").ToString())
            End If
        End Get

        Set(ByVal value As Integer)
            Me.ViewState("CurrentPage") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'JA2010030901 - PERFORMANCE -Start
        If CBoolEx(AppSettings("SHOW_BEST_SELLERS")) Then
            'Me.BestSellersByCategory1.getGroupGuid = globals.GroupGuid
            Dim myControlBox As Control = Me.LoadControl("~/controls/boxes/BestSellersByCategoryBox.ascx")
            myControlBox.ID = "BestSellersByCategoryBox1"

            Dim myControl As Control = Me.LoadControl("~/controls/BestSellersByCategory.ascx")
            myControl.ID = "BestSellersByCategory1"
            eis.SetControlProperties(myControl, "getGroupGuid", globals.GroupGuid)
            Me.phMyMostPopularItems.Controls.Clear()

            Me.phMyMostPopularItems.Controls.Add(myControlBox)
            Me.phMyMostPopularItems.Controls.Add(myControl)
        End If
        'JA2010030901 - PERFORMANCE -End
        If Not IsPostBack Then
            Dim builder As New StringBuilder()
            builder.Append("")
            builder.Append("|")
            builder.Append("")
            builder.Append(",")
            builder.Append(Resources.Language.LABEL_PRODUCT)
            builder.Append("|")
            builder.Append("ProductGuid")
            builder.Append(",")
            builder.Append(Resources.Language.LABEL_NAME)
            builder.Append("|")
            builder.Append("ProductName")
            builder.Append(",")
            builder.Append(Resources.Language.LABEL_PRICE)
            builder.Append("|")
            builder.Append("ListPrice")
            SortControl1.FieldsList = builder.ToString()
        End If


        If CStrEx(page1) = "0" Then
            page1 = CStrEx(AppSettings("Page_1"))
        End If
        If CStrEx(page2) = "0" Then
            page2 = CStrEx(AppSettings("Page_2"))
        End If
        If CStrEx(page3) = "0" Then
            page3 = CStrEx(AppSettings("Page_3"))
        End If


    End Sub

    Protected Overloads Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender

        ' Check the page access.
        eis.PageAccessRedirect("Catalog")

        ' Set GroupGuid to 0 and see if any is specified.
        globals.GroupGuid = CLngEx(Request("GroupGuid"))


        If Not Page.IsPostBack Then
            Dim list1 As New ListItem(page1, page1, True)
            Me.ddlPageSize.Items.Add(list1)
            Dim list2 As New ListItem(page2, page2)
            Me.ddlPageSize.Items.Add(list2)
            Dim list3 As New ListItem(page3, page3)
            Me.ddlPageSize.Items.Add(list3)
            Dim all As New ListItem("All", "All")
            Me.ddlPageSize.Items.Add(all)


            Me.ddlPageSize2.Items.Add(list1)
            Me.ddlPageSize2.Items.Add(list2)
            Me.ddlPageSize2.Items.Add(list3)
            Me.ddlPageSize2.Items.Add(all)
        End If

        BindData()


    End Sub

    Private Sub BindData(Optional ByVal fromEvent As String = "")
        Dim selection As String = Me.SortControl1.getSelection()

        'JA2010030901 - PERFORMANCE -Start
        Dim dictPerformance As ExpDictionary = New ExpDictionary()
        'JA2010030901 - PERFORMANCE -End

        ' Load the products in the group.
        info = eis.CatGroups2Info(New String() {globals.GroupGuid})

        ''JA2010030901 - PERFORMANCE -Start
        ''AM2010071901 - ENTERPRISE CSC - Start
        'info("General")("CacheKey") = "pricelistLineQty.aspx-" & globals.GroupGuid.ToString & "-" & Session("LanguageGuid").ToString & "-" & globals.User("CatalogNo")
        ''*****CUSTOMER SPECIFIC CATALOGS***** -START
        ''ExpandIT US - USE CSC - Start
        'If CBoolEx(AppSettings("EXPANDIT_US_USE_CSC")) And CBoolEx(globals.User("B2B")) Then
        'info("General")("CacheKey") &= "-" & globals.User("CustomerGuid")
        'End If

        If AppSettings("EXPANDIT_US_USE_CSC") Then
            If selection <> "" Then
                info("General")("CacheKey") = "pricelistLineQty.aspx-" & globals.GroupGuid.ToString & "-" & selection & "-" & Session("LanguageGuid").ToString & "-" & globals.User("CatalogNo") & "-" & CStrEx(globals.User("CustomerGuid"))
            Else
                info("General")("CacheKey") = "pricelistLineQty.aspx-" & globals.GroupGuid.ToString & "-" & CurrentPage & "-" & ddlPageSize.SelectedValue & "-" & Session("LanguageGuid").ToString & "-" & globals.User("CatalogNo") & "-" & CStrEx(globals.User("CustomerGuid"))
            End If
        Else
            If selection <> "" Then
                info("General")("CacheKey") = "pricelistLineQty.aspx-" & globals.GroupGuid.ToString & "-" & selection & "-" & Session("LanguageGuid").ToString & "-" & globals.User("CatalogNo")
            Else
                info("General")("CacheKey") = "pricelistLineQty.aspx-" & globals.GroupGuid.ToString & "-" & CurrentPage & "-" & ddlPageSize.SelectedValue & "-" & Session("LanguageGuid").ToString & "-" & globals.User("CatalogNo")
            End If
        End If

        ''ExpandIT US - USE CSC - End
        '*****CUSTOMER SPECIFIC CATALOGS***** -END
        ''AM2010071901 - ENTERPRISE CSC - End
        ''JA2010030901 - PERFORMANCE -End

        'JA2010030901 - PERFORMANCE -Start
        If (selection = "") And AppSettings("EXPANDIT_US_USE_PAGINATION") Then
            dictPerformance("PageNumber") = CurrentPage
            dictPerformance("TotalPerPage") = ddlPageSize.SelectedValue
            info("General")("Performance") = dictPerformance
        End If
        'JA2010030901 - PERFORMANCE -End


        info("General")("CalcPrices") = True
        productinfo = New ExpDictionary()
        'JA2010030901 - PERFORMANCE -Start
        productinfo.Add("LoadAllVariants", True)
        'productinfo.Add("LoadAllVariants", False)
        'JA2010030901 - PERFORMANCE -End
        productinfo.Add("ConvertCurrency", True)
        productinfo.Add("CustomerSpecificPrice", True)
        productinfo.Add("ProductFieldArray", New String() {"ProductName", "QtyOnHand", "ListPrice", "NewItem", "DateAdded", "BaseUOM"})
        ' Changed here to use both HTMLDESC and DESCRIPTION
        productinfo.Add("PropertyArray", New String() {"PICTURE2", "DESCRIPTION"})
        info("GroupInfo")("ProductInfo") = productinfo
        eis.CatLoadGroups(info)
        Group = info("GroupInfo")("Groups")(globals.GroupGuid.ToString)

        IsGroupAvailable = Group.Count > 0



        'JA2010032101 - META TAGS -Start
        If CStrEx(Group("METATAGKEYWORDS")) <> "" Then
            Dim metaKeywords As System.Web.UI.HtmlControls.HtmlMeta = New HtmlMeta
            metaKeywords.Name = "keywords"
            metaKeywords.Content = CStrEx(Group("METATAGKEYWORDS"))
            Page.Header.Controls.Add(metaKeywords)
        End If

        If CStrEx(Group("METATAGDESCRIPTION")) <> "" Then
            Dim metaDescription As System.Web.UI.HtmlControls.HtmlMeta = New HtmlMeta
            metaDescription.Name = "description"
            metaDescription.Content = CStrEx(Group("METATAGDESCRIPTION"))
            Page.Header.Controls.Add(metaDescription)
        End If
        'JA2010032101 - META TAGS -End



        ' Check if the user is using secondary currency.
        bUseSecondaryCurrency = CBoolEx(eis.UseSecondaryCurrency(User))

        ' Calculate ColSpan.
        MyCalcColSpan = IIf(bUseSecondaryCurrency, "7", "5")


        Dim Prod As ExpDictionary = Group("Products")



        If AppSettings("EXPANDIT_US_USE_PAGINATION") Then
        If selection = "" Then
                objPage.DataSource = Group("Products")
        Else
                SortControl1.DataSource = Group("Products")
        End If

        
        If Not Group("Products") Is Nothing Then
            If selection <> "" Then
                objPage.DataSource = SortControl1.SortedDataSource
            End If
            objPage.AllowPaging = True

            If fromEvent = "" Or fromEvent = "selectedIndex" Then
                If CStrEx(ddlPageSize.SelectedValue) = "All" Then
                        objPage.PageSize = Prod.Count
                    ddlPageSize2.SelectedValue = "All"
                Else
                    objPage.PageSize = Integer.Parse(ddlPageSize.SelectedValue)
                    ddlPageSize2.SelectedValue = Integer.Parse(ddlPageSize.SelectedValue)
                End If
            ElseIf fromEvent = "selectedIndex2" Then
                If CStrEx(ddlPageSize2.SelectedValue) = "All" Then
                        objPage.PageSize = Prod.Count
                    ddlPageSize.SelectedValue = "All"
                Else
                    objPage.PageSize = Integer.Parse(ddlPageSize2.SelectedValue)
                    ddlPageSize.SelectedValue = Integer.Parse(ddlPageSize2.SelectedValue)
                End If
            End If


            If CurrentPage < 0 Or CurrentPage >= objPage.PageCount Then
                CurrentPage = 0
            End If
            objPage.CurrentPageIndex = CurrentPage
            lnkbtnNext.Visible = Not objPage.IsLastPage
            lnkbtnPrevious.Visible = Not objPage.IsFirstPage
            lnkbtnNext2.Visible = Not objPage.IsLastPage
            lnkbtnPrevious2.Visible = Not objPage.IsFirstPage
            pricelist.DataSource = objPage
            pricelist.DataBind()
            Me.txtPage.Text = CurrentPage + 1
            Me.txtPage2.Text = CurrentPage + 1
            Me.lblNumPages.Text = Resources.Language.LABEL_PAGINATION_OF & " " & objPage.PageCount
            Me.lblNumPages2.Text = Resources.Language.LABEL_PAGINATION_OF & " " & objPage.PageCount
        End If
        Else
            If Not Group("Products") Is Nothing Then
                objPage.DataSource = Group("Products")
                pricelist.DataSource = objPage
                pricelist.DataBind()
            End If
        End If


        PageHeader1.Text = Group("NAME")


        If Not Group("Products") Is Nothing Then
        Dim from As Integer = CIntEx(pricelist.DataSource.count) * CIntEx(pricelist.DataSource.CurrentPageIndex)
        Dim toCount As Integer = (CIntEx(pricelist.DataSource.count) * CIntEx(pricelist.DataSource.CurrentPageIndex)) + CIntEx(pricelist.DataSource.count) - 1
        Dim j As Integer = 0
        test = ""
        If Not Group("Products") Is Nothing AndAlso Not Group("Products") Is DBNull.Value AndAlso Group("Products").Count > 0 Then
            For Each item As ExpDictionary In Group("Products").Values
                If j >= from And j <= toCount Then
                    test = test & item("ProductGuid") & "|"
                End If
                j = j + 1
            Next
            test = test.TrimEnd("|")
        End If
        End If


        'Load Images
        Me.Picture1.ImageUrl = VRoot & "/" & Group("PICTURE1")

    End Sub

    'JA2010030901 - PERFORMANCE -Start

    Protected Sub pricelist_ItemCommand(ByVal sender As Object, ByVal e As DataListItemEventArgs) Handles pricelist.ItemDataBound

        'AM0122201001 - UOM - Start
        If AppSettings("EXPANDIT_US_USE_UOM") Then
            Dim lblGuid As Label = Nothing
            Dim lblSpecial As Label = Nothing
            Dim ph As PlaceHolder = Nothing
            Dim productUOM As New ExpDictionary


            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                lblGuid = CType(e.Item.FindControl("lblGuid"), Label)
                lblSpecial = CType(e.Item.FindControl("lblSpecial"), Label)
                ph = CType(e.Item.FindControl("phUOM"), PlaceHolder)
                For Each item As ExpDictionary In Group("Products").values
                    If CStrEx(item("ProductGuid")) = CStrEx(lblGuid.Text) Then
                        productUOM = item
                        Exit For
                    End If
                Next
            End If

            If Not lblGuid Is Nothing AndAlso Not lblSpecial Is Nothing And Not ph Is Nothing And Not productUOM Is Nothing Then

                Dim myControl As Control = Me.LoadControl("~/controls/USUOM/UOM.ascx")

                myControl.ID = "UOM" & CStrEx(lblGuid.Text)

                eis.SetControlProperties(myControl, "ProductGuid", CStrEx(lblGuid.Text))
                eis.SetControlProperties(myControl, "PostBackURL", addToCartLink(CStrEx(lblGuid.Text)))
                eis.SetControlProperties(myControl, "DisplayMode", DisplayModes.List)
                eis.SetControlProperties(myControl, "ShowCartBtn", True)
                eis.SetControlProperties(myControl, "ShowFavoritesBtn", True)
                eis.SetControlProperties(myControl, "DefaultQty", 1)
                eis.SetControlProperties(myControl, "IsSpecial", CBoolEx(lblSpecial.Text))
                eis.SetControlProperties(myControl, "ProductDictProperty", productUOM)


                ph.Controls.Clear()
                ph.Controls.Add(myControl)
            End If
        End If
        'AM0122201001 - UOM - End

        'AM2010021901 - VARIANTS - Start
        If CBoolEx(AppSettings("EXPANDIT_US_USE_VARIANTS")) Then
            Dim productVAR As New ExpDictionary
            Dim lblGuidVariants As Label = Nothing
            Dim phV As PlaceHolder = Nothing

            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                lblGuidVariants = CType(e.Item.FindControl("lblGuidVariants"), Label)
                phV = CType(e.Item.FindControl("phVariants"), PlaceHolder)
                For Each item As ExpDictionary In Group("Products").values
                    If CStrEx(item("ProductGuid")) = CStrEx(lblGuidVariants.Text) Then
                        productVAR = item
                        Exit For
                    End If
                Next
            End If



            If Not lblGuidVariants Is Nothing And Not phV Is Nothing And Not productVAR Is Nothing Then
                Dim myControlVariant As Control = Me.LoadControl("~/controls/USVariants/Variants.ascx")

                myControlVariant.ID = "Variants" & CStrEx(lblGuidVariants.Text)

                eis.SetControlProperties(myControlVariant, "ProductGuid", CStrEx(lblGuidVariants.Text))
                eis.SetControlProperties(myControlVariant, "ProductDictPropertyVariant", productVAR)
                eis.SetControlProperties(myControlVariant, "UOMDisplayMode", DisplayModes.List)


                phV.Controls.Clear()
                phV.Controls.Add(myControlVariant)
            End If
        End If
        'AM2010021901 - VARIANTS - End

    End Sub

    'JA2010030901 - PERFORMANCE -End



    Sub dlPaging_ItemCommand(ByVal sender As Object, ByVal e As DataListCommandEventArgs)

        If (e.CommandName.Equals("lnkbtnPaging")) Then
            CurrentPage = Integer.Parse(e.CommandArgument.ToString())
            BindData()
        End If

    End Sub

    Sub dlPaging_ItemDataBound(ByVal sender As Object, ByVal e As DataListItemEventArgs)

        Dim lnkbtnPage = CType(e.Item.FindControl("lnkbtnPaging"), LinkButton)
        If (lnkbtnPage.CommandArgument.ToString() = CurrentPage.ToString()) Then
            lnkbtnPage.Enabled = False
            lnkbtnPage.Font.Bold = True
        End If
    End Sub
    Sub lnkbtnPrevious_Click(ByVal sender As Object, ByVal e As EventArgs)

        CurrentPage -= 1
        BindData()

    End Sub
    Sub lnkbtnPrevious2_Click(ByVal sender As Object, ByVal e As EventArgs)

        CurrentPage -= 1
        BindData()

    End Sub

    Sub txtPage_TextChanged(ByVal sender As Object, ByVal e As EventArgs)

        CurrentPage = txtPage.Text - 1
        BindData()

    End Sub
    Sub txtPage2_TextChanged(ByVal sender As Object, ByVal e As EventArgs)

        CurrentPage = txtPage2.Text - 1
        BindData()

    End Sub

    Sub lnkbtnNext_Click(ByVal sender As Object, ByVal e As EventArgs)

        CurrentPage += 1
        BindData()

    End Sub

    Sub lnkbtnNext2_Click(ByVal sender As Object, ByVal e As EventArgs)

        CurrentPage += 1
        BindData()

    End Sub

    Protected Sub ddlPageSize_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
        CurrentPage = 0
        BindData("selectedIndex")
    End Sub

    Protected Sub ddlPageSize2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPageSize2.SelectedIndexChanged
        CurrentPage = 0
        BindData("selectedIndex2")
    End Sub



    Protected Function PictureUrl(ByVal v As String) As String
        Dim s As String = VRoot & "/"
        Return String.Concat(s, v)
    End Function

    Protected Function isEmptyString(ByVal str As String) As Boolean
        Return Trim(Replace(Replace(Replace(str, Chr(10), ""), Chr(13), ""), Chr(9), "")) = ""
    End Function

    Protected Function isNotEmptyString(ByVal str As String) As Boolean
        Return Trim(Replace(Replace(Replace(str, Chr(10), ""), Chr(13), ""), Chr(9), "")) <> ""
    End Function

    Protected Function findStyle(ByVal guid As String)
        Dim Prod As ExpDictionary = Group("Products")
        Dim product As New ExpDictionary
        Dim line
        For Each line In Prod.Values
            If line("ProductGuid") = guid Then
                product = line
            End If
        Next
        Dim m_productclass As ProductClass = New ProductClass(product)
        Dim style As String = m_productclass.ThumbnailURLStyle(100, 100)
        Return style
    End Function

    Protected Function findDescription(ByVal guid As String)
        Dim Prod As ExpDictionary = Group("Products")
        Dim product As New ExpDictionary
        Dim line
        For Each line In Prod.Values
            If line("ProductGuid") = guid Then
                product = line
            End If
        Next
        Dim m_productclass As ProductClass = New ProductClass(product)
        If Not m_productclass.Description() Is Nothing Then
            If m_productclass.Description().Length > 100 Then
                m_productclass.Description2() = m_productclass.Description().Substring(0, 99) & "<ins class=""moreText"">...more</ins>"

            Else
                m_productclass.Description2() = m_productclass.Description()
            End If
        End If
        Return m_productclass.Description2()
    End Function

    'AM0122201001 - UOM - Start
    Public Function addToCartLink(ByVal ProductGuid As String)
        Dim link As String

        If ProductGuid <> "" Then
            link = "~/cart.aspx?cartaction=add&RetURL=" & Server.UrlEncode(Request.ServerVariables("script_name") & "?GroupGuid=" & globals.GroupGuid) & "&OnlyProduct=" & ProductGuid
        Else
            link = "~/cart.aspx?cartaction=add&RetURL=" & Server.UrlEncode(Request.ServerVariables("script_name") & "?GroupGuid=" & globals.GroupGuid)
        End If
        Return link
    End Function
    'AM0122201001 - UOM - End

End Class
