<%--JA2010051301 - ENTERPRISE ADMIN SECTION PASSWORD DECRYPTION - Start--%>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="adminPasswords.aspx.vb"
    Inherits="adminPasswords" CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master"
    RuntimeMasterPageFile="ThreeColumn.master" CrumbName="PASSWORD DECRYPTION" %>

<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <div class="AccountPage">
        <uc1:PageHeader ID="PageHeader1" runat="server" Text="SQL QUERIES" EnableTheming="true" />
        <div>
            <h2>
                User Login</h2>
            <div style="width: 100%; vertical-align: middle;">
                <asp:TextBox ID="txtPasswordInput" runat="server" Width="150px" Style="height: 16px;"></asp:TextBox>
                <asp:TextBox ID="txtPasswordOutput" Width="425px" ReadOnly="true" style="height: 20px; border:0px; padding-left:10px;" runat="server"></asp:TextBox>
            </div>
            <br />
            <asp:Button ID="btnSearch" CssClass="AddButton" runat="server" Text="DeCrypt" PostBackUrl="~/adminPasswords.aspx" />
            <br />
            <br />
            <br />
            <h2>
                Alter All Users</h2>
            <asp:Button ID="DeCryptAll" CssClass="AddButton" runat="server" Text="DeCrypt All" />
            <asp:TextBox ID="MsgDeCryptAll" Width="425px" ReadOnly="true" style="height: 20px; border:0px; padding-left:10px;" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="RemoveAll" CssClass="AddButton" runat="server" Text="Remove All" />
            <asp:TextBox ID="MsgRemoveAll" Width="425px" ReadOnly="true" style="height: 20px; border:0px; padding-left:10px;" runat="server"></asp:TextBox>
        </div>
    </div>
</asp:Content>
<%--JA2010051301 - ENTERPRISE ADMIN SECTION PASSWORD DECRYPTION - End--%>
