﻿<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="user_deactivate.aspx.vb" Inherits="user_deactivate" 
  CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master" 
  RuntimeMasterPageFile="ThreeColumn.master" CrumbName="<%$ Resources: Language, LABEL_USER_ACCOUNT_DEACTIVATING %>"%>

<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<%@ Register src="~/controls/Message.ascx" tagname="Message" tagprefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
  <uc1:PageHeader ID="PageHeader1" runat="server" text="<%$ Resources: Language, LABEL_USER_ACCOUNT_DEACTIVATING %>" EnableTheming="true" />
  <uc1:Message ID="Message1" runat="server"/>
  
  
  <div id="UserDeactivatingPage" class="UserDeactivatingPage" runat="server"  >      
    <p>
      <asp:CheckBox ID="confirmCheckBox" runat="server" Text="<%$ Resources: Language, LABEL_USER_ACCOUNT_DEACTIVATING_CHECK %>" />
    </p>
    <div style="text-align:right">
          <asp:Button ID="backButton" runat="server" CssClass="AddButton" Text="<%$ Resources: Language, LABEL_BACK %>" PostBackUrl="account.aspx" />
          &nbsp;
          <asp:Button ID="continueButton" runat="server" CssClass="AddButton" Text="<%$ Resources: Language, ACTION_SUBMIT %>" />
     </div>
  </div>
</asp:Content>



