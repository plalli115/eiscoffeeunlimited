<%@ Page Language="VB" AutoEventWireup="false" CodeFile="user_login.aspx.vb" Inherits="user_login"
    CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master"
    RuntimeMasterPageFile="ThreeColumn.master" CrumbName="<%$ Resources: Language, LABEL_MENU_LOGIN %>" %>

<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <div class="UserLoginPage">
        <uc1:pageheader id="PageHeader1" runat="server" text="<%$ Resources: Language, LABEL_MENU_LOGIN %>"
            enabletheming="true" />
        <p>
            <% =Resources.Language.LABEL_PLEASE_LOGIN_IF_ACCOUNT_EXISTS_1%>
        </p>
        <% If Request("returl") <> "" Then%>
        <input type="hidden" name="ACTION" value="LOGIN" />
        <% End If%>
        <input type="hidden" value="<% = Request("returl") %>" name="returl" />
        <asp:Panel ID="loginpanel" runat="server" DefaultButton="SubmitLogin">
            <table border="0" cellpadding="2" cellspacing="0">
                <tr class="TR1">
                    <td class="TDR">
                        <% =Resources.Language.LABEL_USERDATA_LOGIN%>
                    </td>
                    <td class="TDL">
                        <asp:TextBox runat="server" TextMode="SingleLine" CssClass="borderTextBox" ID="login" />
                    </td>
                </tr>
                <tr class="TR1">
                    <td class="TDR">
                        <% =Resources.Language.LABEL_USERDATA_PASSWORD%>
                    </td>
                    <td class="TDL">
                        <asp:TextBox runat="server" TextMode="Password" CssClass="borderTextBox" ID="password" />
                    </td>
                </tr>
                <tr class="TR">
                    <td class="TDR" colspan="2">
                        <asp:Button runat="server" ID="SubmitLogin" CssClass="AddButton" Text="<%$Resources: Language,ACTION_SUBMIT %>" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <br />
        <br />
        <br />
        <% If eis.CheckPageAccess("OpenSite") Then%>
        <% If Request("returl") <> "" Then%>
        <input type="hidden" name="returl" value="<% = Request("returl") %>" />
        <% End If%>
        <table border="0" cellpadding="3">
            <tr>
                <td>
                    <b><u>
                        <% =Resources.Language.MENU_SIGNUP%>
                    </u></b>
                </td>
            </tr>
            <tr>
                <td>
                    <% =Resources.Language.LABEL_IF_YOU_DO_NOT_HAVE_AN_ACCOUNT_THEN_CLICK_HERE%>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button runat="server" ID="NewAccount" CssClass="AddButton" Text="<%$Resources: Language, MENU_SIGNUP %>" />
                </td>
            </tr>
        </table>
        <br />
        <% End If%>
        <table border="0" cellpadding="3">
            <tr>
                <td>
                    <b><u>
                        <% =Resources.Language.LABEL_LOST_PASSWORD_SUBJECT%>
                    </u></b>
                </td>
            </tr>
            <tr>
                <td>
                    <% =Resources.Language.LABEL_HAVE_YOU_FORGOTTEN_YOU_PASSWORD_THEN_CLICK_HERE%>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button runat="server" CssClass="AddButton" ID="Forgot" Text="<%$Resources: Language, LABEL_LOST_PASSWORD_SUBJECT %>"
                        PostBackUrl="~/user_typelost_password.aspx" />
                </td>
            </tr>
        </table>
        <%  SetFocusOnElement_OnLoad("login")%>
    </div>
</asp:Content>
