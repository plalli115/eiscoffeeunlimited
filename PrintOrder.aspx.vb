'AM2010080201 - ORDER PRINT OUT - Start
Imports System.Configuration.ConfigurationManager
Imports ExpandIT.EISClass
Imports ExpandIT.GlobalsClass
Imports ExpandIT
Imports ExpandIT.Debug
Imports ExpandIT.ShippingPayment
Imports ExpandIT.ExpandITLib
Imports System.Collections.Generic
Imports System.Data
Imports System.Data.SqlClient

Partial Class PrintOrder
    Inherits ExpandIT.ShippingPayment._CartPage

    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	
	Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
        If CStrEx(Request("HeaderGuid")) <> "" Then
            'It's a Previous Order
            Me.CartRender1.HeaderGuid = CStrEx(Request("HeaderGuid"))
            Me.CartRender1.IsBrowser = True
            Me.CartRender1.IsLedger = False
        ElseIf CStrEx(Request("DrillDownType")) <> "" And CStrEx(Request("DrillDownGuid")) <> "" Then
            'It's a Ledger Entry
            Me.CartRender1.DrillDownType = CIntEx(Request("DrillDownType"))
            Me.CartRender1.DrillDownGuid = CStrEx(Request("DrillDownGuid"))
            Me.CartRender1.IsLedger = True
            Me.CartRender1.IsBrowser = False
        End If
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
    End Sub

End Class
'AM2010080201 - ORDER PRINT OUT - End