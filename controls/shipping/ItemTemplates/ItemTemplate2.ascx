﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ItemTemplate2.ascx.vb"
    Inherits="controls_shipping_ItemTemplates_ItemTemplate2" %>
    
    <%-- --DJW 8/13/2012 Eliminated Contact Name From Being Displayed--%>
<table class="tmpl2table" cellpadding="0" cellspacing="0" visible="false">
    <tr>
        <td class="tmpl2output" visible="false">
            <asp:Literal ID="litCont" runat="server" Visible="false" Text="<%$Resources: Language, LABEL_USERDATA_CONTACT %>" />
        </td>
        <td>
            <asp:TextBox ReadOnly="true" BorderWidth="0" visible="false" BackColor="transparent" runat="server"
                ID="ContactName" CssClass="tmpl2text" Text='<%#Eval("ContactName")%>' />
        </td>
    </tr>
</table>

<%--<table class="tmpl2table" cellpadding="0" cellspacing="0">
    <tr>
        <td class="tmpl2output">
            <asp:Literal ID="litCont" runat="server" Text="<%$Resources: Language, LABEL_USERDATA_CONTACT %>" />:&nbsp;
        </td>
        <td>
            <asp:TextBox ReadOnly="true" BorderWidth="0" BackColor="transparent" runat="server"
                ID="ContactName" CssClass="tmpl2text" Text='<%#Eval("ContactName")%>' />
        </td>
    </tr>
</table>--%>

<table class="tmpl2table" cellpadding="0" cellspacing="0">
    <tr>
        <td class="tmpl2output">
            <asp:Literal ID="litComp" runat="server" Text="<%$Resources: Language, LABEL_USERDATA_COMPANY %>" />:&nbsp;
        </td>
        <td>
            <asp:TextBox ReadOnly="true" BorderWidth="0" BackColor="transparent" runat="server"
                ID="CompanyName" CssClass="tmpl2text" Text='<%#Eval("CompanyName")%>' />
        </td>
    </tr>
</table>
<table class="tmpl2table" cellpadding="0" cellspacing="0">
    <tr>
        <td class="tmpl2output">
            <asp:Literal ID="litAddress1" runat="server" Text="<%$Resources: Language, LABEL_USERDATA_ADDRESS_1 %>" />:&nbsp;
        </td>
        <td>
            <asp:TextBox ReadOnly="true" BorderWidth="0" BackColor="transparent" runat="server"
                ID="Address1" CssClass="tmpl2text" Text='<%#Eval("Address1")%>' />
        </td>
    </tr>
</table>
<table class="tmpl2table" cellpadding="0" cellspacing="0">
    <tr>
        <td class="tmpl2output">
            <asp:Literal ID="litAddress2" runat="server" Text="<%$Resources: Language, LABEL_USERDATA_ADDRESS_2 %>" />:&nbsp;
        </td>
        <td>
            <asp:TextBox ReadOnly="true" BorderWidth="0" BackColor="transparent" runat="server"
                ID="Address2" CssClass="tmpl2text" Text='<%#Eval("Address2")%>' />
        </td>
    </tr>
</table>
<table class="tmpl2table" cellpadding="0" cellspacing="0">
    <tr>
        <td class="tmpl2output">
            <asp:Literal ID="litCity" runat="server" Text="<%$Resources: Language, LABEL_USERDATA_CITY %>" />:&nbsp;
        </td>
        <td>
            <asp:TextBox ReadOnly="true" BorderWidth="0" BackColor="transparent" runat="server"
                ID="CityName" CssClass="tmpl2text" Text='<%#Eval("CityName")%>' />
        </td>
    </tr>
</table>
<asp:Panel runat="server" ID="StatePanel">
    <table class="tmpl2table" cellpadding="0" cellspacing="0">
        <tr>
            <td class="tmpl2output">
                <asp:Literal ID="litState" runat="server" Text="<%$Resources: Language, LABEL_USERDATA_STATE %>" />:&nbsp;
            </td>
            <td>
                <asp:TextBox ReadOnly="true" BorderWidth="0" BackColor="transparent" runat="server"
                    ID="StateName" CssClass="tmpl2text" Text='<%#Eval("StateName")%>' />
            </td>
        </tr>
    </table>
</asp:Panel>
<table class="tmpl2table" cellpadding="0" cellspacing="0">
    <tr>
        <td class="tmpl2output">
            <asp:Literal ID="litZipCode" runat="server" Text="<%$Resources: Language, LABEL_USERDATA_ZIPCODE %>" />:&nbsp;
        </td>
        <td>
            <asp:TextBox ReadOnly="true" BorderWidth="0" BackColor="transparent" runat="server"
                ID="ZipCode" CssClass="tmpl2text" Text='<%#Eval("ZipCode")%>' />
        </td>
    </tr>
</table>
<%--<table class="tmpl2table" cellpadding="0" cellspacing="0">
    <tr>
        <td class="tmpl2output">
            <asp:Literal ID="litCountry" runat="server" Text="<%$Resources: Language, LABEL_USERDATA_COUNTRY %>" />:&nbsp;
        </td>
        <td>
            <asp:TextBox ReadOnly="true" BorderWidth="0" BackColor="transparent" runat="server"
                ID="CountryDropDownList1" CssClass="tmpl2text" Text='<%#CountryGuidsToText(Eval("CountryGuid"))%>' />
        </td>
    </tr>
</table>--%>
<!-- WLB.BEGIN.10/11/2012: Don't display Ship-to Email, reduces confusion -->
<div style="display:none;">
<!-- WLB.END.10/11/2012: Don't display Ship-to Email, reduces confusion -->
<table class="tmpl2table" cellpadding="0" cellspacing="0">
    <tr>
        <td class="tmpl2output">
            <asp:Literal ID="litEmail" runat="server" Text="<%$Resources: Language, LABEL_USERDATA_EMAIL %>" />:&nbsp;
        </td>
        <td>
            <asp:TextBox ReadOnly="true" BorderWidth="0" BackColor="transparent" runat="server"
                ID="EmailAddress" CssClass="tmpl2text" Text='<%#Eval("EmailAddress")%>' />
        </td>
    </tr>
</table>
<!-- WLB.BEGIN.10/11/2012: Don't display Ship-to Email, reduces confusion -->
</div>
<!-- WLB.END.10/11/2012: Don't display Ship-to Email, reduces confusion -->
<table class="tmpl2table" cellpadding="0" cellspacing="0">
    <tr>
        <td class="tmpl2output">
        </td>
        <td>
            <asp:HiddenField runat="server" ID="UserGuid" Value='<%#Eval("UserGuid")%>' />
        </td>
    </tr>
</table>
<table class="tmpl2table" cellpadding="0" cellspacing="0">
    <tr>
        <td class="tmpl2output">
        </td>
        <td>
            <asp:HiddenField runat="server" ID="ShippingAddressGuid" Value='<%#Eval("ShippingAddressGuid")%>' />
        </td>
    </tr>
</table>
<table class="tmpl2table" cellpadding="0" cellspacing="0">
    <tr>
        <td class="tmpl2output">
        </td>
        <td>
            <asp:HiddenField runat="server" ID="AddressType" Value='<%#Eval("AddressType")%>' />
        </td>
    </tr>
</table>
<!-- ' ExpandIT US - ANA - US Sales Tax Shipping - Start -->
<%--<% if  System.Configuration.ConfigurationManager.AppSettings("EXPANDIT_US_USE_US_SALES_TAX") then %>--%>
<table class="tmpl2table" cellpadding="0" cellspacing="0">
    <tr>
        <td class="tmpl2output">
        </td>
        <td>
            <asp:HiddenField runat="server" ID="ShippingAddressCode" />
        </td>
    </tr>
</table>
<table class="tmpl2table" cellpadding="0" cellspacing="0">
    <tr>
        <td class="tmpl2output">
        </td>
        <td>
            <asp:HiddenField runat="server" ID="TaxAreaCode"  />
        </td>
    </tr>
</table>
<table class="tmpl2table" cellpadding="0" cellspacing="0">
    <tr>
        <td class="tmpl2output">
        </td>
        <td>
            <asp:HiddenField runat="server" ID="TaxLiable" />
        </td>
    </tr>
</table>
<%--<% end if %>--%>
<!-- ' ExpandIT US - ANA - US Sales Tax Shipping - End -->
