Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Collections.Generic
Imports ExpandIT.GlobalsClass
Imports ExpandIT.CartClass

Partial Class controls_shipping_ItemTemplates_ItemTemplate2
    Inherits ExpandIT.ShippingPart

    Protected Sub On_Binding(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DataBinding

        Me.ShippingAddressCode.Value = Eval("ShippingAddressCode")

        If AppSettings("EXPANDIT_US_USE_US_SALES_TAX") Then
            Me.TaxAreaCode.Value = Eval("TaxAreaCode")
            Me.TaxLiable.Value = Eval("TaxLiable")
        End If

    End Sub
End Class
