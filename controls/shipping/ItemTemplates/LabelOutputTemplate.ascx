<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LabelOutputTemplate.ascx.vb"
    Inherits="controls_shipping_ItemTemplates_LabelOutputTemplate" %>
<div class="t">

    <%-- --DJW 8/13/2012 Hiding Contact Name From Being Displayed--%>
    <div class="trs" visible="false">
        <div class="tdl" visible="false">
            <asp:Label ID="lblCnt" runat="server" visible="false" Text="<%$Resources: Language, LABEL_USERDATA_CONTACT %>">:</asp:Label>
        </div>
        <div class="tdr" visible="false">
            <asp:Label ID="ContactNameLabelForOutput" visible="false" runat="server"></asp:Label>
        </div>
    </div>
    <br />
    

    <div class="trs">
        <div class="tdl">
            <asp:Label ID="lblComp" runat="server" Text="<%$Resources: Language, LABEL_USERDATA_COMPANY %>">:</asp:Label>
        </div>
        <div class="tdr">
            <asp:Label ID="CompanyNameLabelForOutput" runat="server"></asp:Label>
        </div>
    </div>
    <br />
    <div class="tr">
        <div class="tdl">
            <asp:Label ID="lblA1" runat="server" Text="<%$Resources: Language, LABEL_USERDATA_ADDRESS_1 %>"></asp:Label>
        </div>
        <div class="tdr">
            <asp:Label ID="Address1LabelForOutput" runat="server"></asp:Label>
        </div>
    </div>
    <div class="tr">
        <div class="tdl">
            <asp:Label ID="lblA2" runat="server" Text="<%$Resources: Language, LABEL_USERDATA_ADDRESS_2 %>">:</asp:Label>
        </div>
        <div class="tdr">
            <asp:Label ID="Address2LabelForOutput" runat="server"></asp:Label>
        </div>
    </div>
    
    <div class="tr">
        <div class="tdl">
            <asp:Label ID="lblTown" runat="server" Text="<%$Resources: Language, LABEL_USERDATA_CITY %>">:</asp:Label>
        </div>
        <div class="tdr">
            <asp:Label ID="CityNameLabelForOutput" runat="server"></asp:Label>
        </div>
    </div>
    <asp:Panel runat="server" CssClass="tr" ID="TableRowState">
        <%--<div class="tr">--%>
        <div class="tdl">
            <asp:Label ID="lblSt" runat="server" Text="<%$Resources: Language, LABEL_USERDATA_STATE %>">:</asp:Label>
        </div>
        <div class="tdr">
            <asp:Label ID="StateNameLabelForOutput" runat="server"></asp:Label>
        </div>
        <%--</div>--%>
    </asp:Panel>
    <div class="tr">
        <div class="tdl">
            <asp:Label ID="lblZ" runat="server" Text="<%$Resources: Language, LABEL_USERDATA_ZIPCODE %>">:</asp:Label>
        </div>
        <div class="tdr">
            <asp:Label ID="ZipCodeLabelForOutput" runat="server"></asp:Label>
        </div>
    </div>
    
   <%-- --DJW 8/10/2012 Hiding Country From Being Displayed--%>
   <div class="tr">
        <div class="tdl">
            <asp:Label ID="lblCtr" runat="server" Visible="false" Text="<%$Resources: Language, LABEL_USERDATA_COUNTRY %>">:</asp:Label>
        </div>
        <div class="tdr">
            <asp:Label ID="CountryDropDownList1LabelForOutput" Visible="false" runat="server"></asp:Label>
        </div>
    </div>
    
    <div class="tr">
        <div class="tdl">
            <asp:Label ID="lblEmal" runat="server" Text="<%$Resources: Language, LABEL_USERDATA_EMAIL %>">:</asp:Label>
        </div>
        <div class="tdr">
            <asp:Label ID="EmailAddressLabelForOutput" runat="server"></asp:Label>
        </div>
    </div>
</div>
