<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ItemTemplate1.ascx.vb"
    Inherits="controls_shipping_ItemTemplates_ItemTemplate1" %>
<asp:Table ID="Table1" runat="server" CellSpacing="0" CssClass="ssiChooseAddress">
    <asp:TableRow ID="TableRow1" runat="server">
        <asp:TableCell ID="TableCell1" runat="server" CssClass="TDDark" ColumnSpan="3" Height="1px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableHeaderRow ID="TableHeaderRow1" runat="server" Height="5px">
        <asp:TableHeaderCell ID="TableHeaderCell1" runat="server" ColumnSpan="3" Height="5px"></asp:TableHeaderCell>
    </asp:TableHeaderRow>
    <asp:TableRow ID="TableRow2" runat="server">
        <asp:TableCell ID="TableCell2" runat="server" CssClass="TDDark" ColumnSpan="3" Height="1px"></asp:TableCell>
    </asp:TableRow>
    
    <%-- --DJW 8/13/2012 Hiding Contact Name From Being Displayed--%>
    <asp:TableRow ID="TableRow3" Visible="false" runat="server"> 
         <asp:TableCell ID="TableCell3"  Visible="false" runat="server" CssClass="TDR">
            <asp:Label ID="contact" runat="server" CssClass="shippingAddressFieldLabel" Visible="false" Text="<%$Resources: Language,LABEL_USERDATA_CONTACT %>"></asp:Label>
        </asp:TableCell>
        <asp:TableCell ID="TableCell4" runat="server" Visible="false" CssClass="TDL">
            <asp:TextBox runat="server" CssClass="shippingAddressTextBox" Visible="false" ID="ContactName" Text='<%#Eval("ContactName")%>' />
        </asp:TableCell>
       <%--<asp:TableCell ID="TableCell5" runat="server" Visible="false" CssClass="TDC">
            <asp:RequiredFieldValidator runat="server" ID="ContactNameValidator" ControlToValidate="ContactName"
                ValidationGroup="purchasegroup" ErrorMessage="<%$Resources: Language, MESSAGE_REQUIRED_FIELD %>" />
        </asp:TableCell>--%>
    </asp:TableRow>
    
<%--    <asp:TableRow ID="TableRow10" Visible="false" runat="server"> 
         <asp:TableCell ID="TableCell28" runat="server" CssClass="TDR">
            <asp:Label ID="Label1" runat="server" CssClass="shippingAddressFieldLabel" Visible="false" Text="<%$Resources: Language,LABEL_USERDATA_CONTACT %>"></asp:Label>
        </asp:TableCell>
        <asp:TableCell ID="TableCell29" runat="server" Visible="false" CssClass="TDL">
            <asp:TextBox runat="server" CssClass="shippingAddressTextBox" Visible="false" ID="TextBox1" Text='<%#Eval("ContactName")%>' />
        </asp:TableCell>
       <asp:TableCell ID="TableCell30" runat="server" Visible="false" CssClass="TDC">
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="ContactName"
                ValidationGroup="purchasegroup" ErrorMessage="<%$Resources: Language, MESSAGE_REQUIRED_FIELD %>" />
        </asp:TableCell>
    </asp:TableRow>--%>
    
   
    <asp:TableRow ID="TableRow4" runat="server">
        <asp:TableCell ID="TableCell6" runat="server" CssClass="TDR">
            <asp:Label ID="company" runat="server" CssClass="shippingAddressFieldLabel" Text="<%$Resources: Language,LABEL_USERDATA_COMPANY %>"></asp:Label>
        </asp:TableCell>
        <asp:TableCell ID="TableCell7" runat="server" CssClass="TDL">
            <asp:TextBox runat="server" CssClass="shippingAddressTextBox" ID="CompanyName" Text='<%#Eval("CompanyName")%>' />
        </asp:TableCell>
        <asp:TableCell ID="TableCell8" runat="server" CssClass="TDC">
				&nbsp;&nbsp;
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow ID="TableRow5" runat="server">
        <asp:TableCell ID="TableCell9" runat="server" CssClass="TDR">
            <asp:Label ID="lblAddress1" runat="server" CssClass="shippingAddressFieldLabel" Text="<%$Resources: Language,LABEL_USERDATA_ADDRESS_1 %>"></asp:Label>
        </asp:TableCell>
        <asp:TableCell ID="TableCell10" runat="server" CssClass="TDL">
            <asp:TextBox runat="server" CssClass="shippingAddressTextBox" ID="Address1" Text='<%#Eval("Address1")%>' />
        </asp:TableCell>
        <asp:TableCell ID="TableCell11" runat="server" CssClass="TDC">
            <asp:RequiredFieldValidator runat="server" ID="Address1Validator" ControlToValidate="Address1"
                ValidationGroup="purchasegroup" ErrorMessage="<%$Resources: Language, MESSAGE_REQUIRED_FIELD %>" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow ID="TableRow6" runat="server">
        <asp:TableCell ID="TableCell12" runat="server" CssClass="TDR">
            <asp:Label ID="lblAddress2" runat="server" CssClass="shippingAddressFieldLabel" Text="<%$Resources: Language,LABEL_USERDATA_ADDRESS_2 %>"></asp:Label>
        </asp:TableCell>
        <asp:TableCell ID="TableCell13" runat="server" CssClass="TDL">
            <asp:TextBox runat="server" CssClass="shippingAddressTextBox" ID="Address2" Text='<%#Eval("Address2")%>' />
        </asp:TableCell>
        <asp:TableCell ID="TableCell14" runat="server" CssClass="TDC">
			&nbsp;&nbsp;
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow ID="TableRow8" runat="server">
        <asp:TableCell ID="TableCell18" runat="server" CssClass="TDR">
            <asp:Label ID="city" runat="server" CssClass="shippingAddressFieldLabel" Text="<%$Resources: Language,LABEL_USERDATA_CITY %>"></asp:Label>
        </asp:TableCell>
        <asp:TableCell ID="TableCell19" runat="server" CssClass="TDL">
            <asp:TextBox runat="server" CssClass="shippingAddressTextBox" ID="CityName" Text='<%#Eval("CityName")%>' />
        </asp:TableCell>
        <asp:TableCell ID="TableCell20" runat="server" CssClass="TDC">
            <asp:RequiredFieldValidator runat="server" ID="CityNameValidator" ControlToValidate="CityName"
                ValidationGroup="purchasegroup" ErrorMessage="<%$Resources: Language, MESSAGE_REQUIRED_FIELD %>" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow ID="TableRowState" runat="server">
        <asp:TableCell ID="TableCellStateOutput" runat="server" CssClass="TDR">
            <asp:Label ID="state" runat="server" CssClass="shippingAddressFieldLabel" Text="<%$Resources: Language,LABEL_USERDATA_STATE %>"></asp:Label>
        </asp:TableCell>
        <asp:TableCell ID="TableCellStateInput" runat="server" CssClass="TDL">
            <asp:TextBox runat="server" CssClass="shippingAddressTextBox" ID="StateName" Text='<%#Eval("StateName")%>' />
        </asp:TableCell>
        <asp:TableCell ID="TableCell21" runat="server" CssClass="TDC">
			&nbsp;&nbsp;
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow ID="TableRow7" runat="server">
        <asp:TableCell ID="TableCell15" runat="server" CssClass="TDR">
            <asp:Label ID="lblZipCode" runat="server" CssClass="shippingAddressFieldLabel" Text="<%$Resources: Language,LABEL_USERDATA_ZIPCODE %>"></asp:Label>
        </asp:TableCell>
        <asp:TableCell ID="TableCell16" runat="server" CssClass="TDL">
            <asp:TextBox runat="server" CssClass="shippingAddressTextBox" ID="ZipCode" Text='<%#Eval("ZipCode")%>' />
        </asp:TableCell>
        <asp:TableCell ID="TableCell17" runat="server" CssClass="TDC">
            <asp:RequiredFieldValidator runat="server" ID="ZipCodeValidator" ControlToValidate="ZipCode"
                ValidationGroup="purchasegroup" ErrorMessage="<%$Resources: Language, MESSAGE_REQUIRED_FIELD %>" />
        </asp:TableCell>
    </asp:TableRow>
    
   <%-- --DJW 8/10/2012 Hiding Country From Being Displayed--%>
    <asp:TableRow ID="TableRowCountry" runat="server">
        <asp:TableCell ID="TableCell22" runat="server" Visible="false" CssClass="TDR">
            <asp:Label ID="LabelCountry" runat="server" Visible="false" CssClass="shippingAddressFieldLabel"
                Text="<%$Resources: Language,LABEL_USERDATA_COUNTRY %>"></asp:Label>
        </asp:TableCell>
        <asp:TableCell ID="TableCell23" runat="server" Visible="false" CssClass="TDL">
            <div class="dpdBorder">
                <asp:DropDownList CssClass="borderDropDownList" Visible="false" Width="140px" ID="CountryDropDownList1"
                    runat="server">
                </asp:DropDownList>
            </div>
        </asp:TableCell>
        <asp:TableCell ID="TableCell24" runat="server" Visible="false" CssClass="TDC">
			&nbsp;&nbsp;
        </asp:TableCell>
    </asp:TableRow>
    <%--WLB.10/11/2012: Don't display Ship-to Email, reduces confusion, Added Visible="false" --%>
    <asp:TableRow ID="TableRow9" runat="server" Visible="false">
        <asp:TableCell ID="TableCell25" runat="server" CssClass="TDR">
            <asp:Label ID="LabelEmail" runat="server" CssClass="shippingAddressFieldLabel" Text="<%$Resources: Language,LABEL_USERDATA_EMAIL %>"></asp:Label>
        </asp:TableCell>
        <asp:TableCell ID="TableCell26" runat="server" CssClass="TDL">
            <asp:TextBox runat="server" CssClass="shippingAddressTextBox" ID="EmailAddress" Text='<%#Eval("EmailAddress")%>' />
        </asp:TableCell>
        <asp:TableCell ID="TableCell27" runat="server" CssClass="TDC">
            <%--<asp:RequiredFieldValidator runat="server" ID="EmailAddressValidator" ControlToValidate="EmailAddress"
                ValidationGroup="purchasegroup" ErrorMessage="<%$Resources: Language, MESSAGE_REQUIRED_FIELD %>" Display="Dynamic" />--%>
            <asp:RegularExpressionValidator ValidationExpression="[\w-]+(?:\.[\w-]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,7}"
                ID="ValidEmailAddressValidator" runat="server" ControlToValidate="EmailAddress"
                ValidationGroup="purchasegroup" ErrorMessage="<%$Resources: Language, MESSAGE_EMAIL_ADDRESS_NOT_VALID %>" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow ID="TableRow11" runat="server">
        <asp:TableCell ID="TableCell30" runat="server" CssClass="TDR">
            <%-- THIS IS THE LABEL FOR THE USER INPUT FIELDS, VISIBLE ON THE PAGE. WLB --%>
            <asp:Label ID="LabelssiMakeAddressPermanent" runat="server" CssClass="shippingAddressFieldLabel" Text="<%$Resources: Language,LABEL_SSI_MAKE_ADDRESS_PERM %>"></asp:Label>            
        </asp:TableCell>
        <asp:TableCell ID="TableCell31" runat="server" CssClass="TDL">
           <%-- <asp:CheckBox runat="server" ID="ssiMakeAddressPermanent" Checked='<%#Eval("ssiMakeAddressPermanent") %>' />--%>  
           <%-- THIS IS THE CHECBOX FOR THE USER INPUT FIELDS, VISIBLE ON THE PAGE. WLB --%>
            <asp:CheckBox runat="server" ID="ssiMakeAddressPermanentVisible" />          
        </asp:TableCell>
        <asp:TableCell ID="TableCell32" runat="server" CssClass="TDC">
			&nbsp;&nbsp;
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow ID="TableRow10" runat="server">
        <asp:TableCell ID="TableCell5" runat="server" CssClass="TDR">
            <%-- THIS IS THE LABEL FOR THE USER INPUT FIELDS, VISIBLE ON THE PAGE. WLB --%>
            <asp:Label ID="lblssiMakeEmailPermanent" style="visibility:hidden;" runat="server" CssClass="shippingAddressFieldLabel" Text="<%$Resources: Language,LABEL_SSI_MAKE_EMAIL_PERM %>"></asp:Label>
        </asp:TableCell>
        <asp:TableCell ID="TableCell28" runat="server" CssClass="TDL">
            <%-- THIS IS THE CHECBOX FOR THE USER INPUT FIELDS, VISIBLE ON THE PAGE. WLB --%>
            <asp:CheckBox runat="server" ID="ssiMakeEmailPermanentVisible" style="visibility:hidden;" checked="true" />            
<%--            <asp:TextBox runat="server" CssClass="shippingAddressTextBox" ID="ssiMakeEmailPermanent" Text='<%#Eval("ssiMakeEmailPermanent") %>' />--%>
        </asp:TableCell>
        <asp:TableCell ID="TableCell29" runat="server" CssClass="TDC">
			&nbsp;&nbsp;
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>    
<div style="display: none">
    <asp:TextBox runat="server" ID="UserGuid" Text='<%#Eval("UserGuid")%>' BorderStyle="None" />
</div>
<div style="display: none">
    <asp:TextBox runat="server" ID="ShippingAddressGuid" Text='<%#Eval("ShippingAddressGuid")%>'
        BorderStyle="None" />
</div>
<div style="display: none">
    <%--<asp:HiddenField runat="server" ID="AddressType" Value='<%#Eval("AddressType")%>' />--%>
    <asp:TextBox runat="server" ID="AddressType" Text='<%#Eval("AddressType")%>' BorderStyle="None" />
</div>
