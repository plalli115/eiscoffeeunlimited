<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ListItemTemplate1.ascx.vb"
    Inherits="controls_shipping_ItemTemplates_ListItemTemplate1" %>
<div class="listTemplate1">
    <asp:Table runat="server">   
        <asp:TableRow runat="server" ID="listTemplate1Row">
        
        <%-- --DJW 8/13/2012 Hiding Contact Name From Being Displayed--%>
          <asp:TableCell Style="white-space: nowrap;" visible="false">
                <asp:TextBox CssClass="listTextBox1" ReadOnly="true" runat="server" ID="ContactName"
                    Wrap="false" Visible="false" Text='<%#Eval("ContactName")%>' />
            </asp:TableCell>
                        
            <asp:TableCell Style="white-space: nowrap;">
                <asp:TextBox CssClass="listTextBox2" ReadOnly="true" runat="server" ID="CompanyName"
                    Wrap="false" Text='<%#Eval("CompanyName")%>' />
            </asp:TableCell>   
            <asp:TableCell Style="white-space: nowrap;">
                <asp:TextBox CssClass="listTextBox3" ReadOnly="true" runat="server" ID="Address1"
                    Wrap="false" Text='<%#Eval("Address1")%>' />
            </asp:TableCell>
            <asp:TableCell Style="white-space: nowrap;">
                <asp:TextBox CssClass="listTextBox4" ReadOnly="true" runat="server" ID="Address2"
                    Wrap="false" Text='<%#Eval("Address2")%>' />
            </asp:TableCell>
            <asp:TableCell Style="width: 50px">
                <asp:TextBox CssClass="listTextBox5" ReadOnly="true" runat="server" ID="CityName"
                    Wrap="false" Text='<%#Eval("CityName")%>' />
            </asp:TableCell>
            <asp:TableCell Style="width: 24px" HorizontalAlign="Right">
                <asp:Image ID="padlock" runat="server" ImageUrl="~/images/padlock.gif" AlternateText="_"
                    Visible="false" /></asp:TableCell>
            <asp:TableCell Style="display: none" Width="0px">
                <asp:HiddenField runat="server" ID="StateName" Value='<%#Eval("StateName")%>' />
            </asp:TableCell>
            <asp:TableCell Style="display: none" Width="0px">
                <asp:HiddenField runat="server" ID="ZipCode" Value='<%#Eval("ZipCode")%>' />
            </asp:TableCell>
            
            <%----DJW 8/10/2012 Hiding Country From Being Displayed--%>
            <asp:TableCell Style="display: none" Width="0px">
                <asp:HiddenField runat="server" Visible="false" ID="CountryDropDownList1" Value='<%#CountryGuidsToText(Eval("CountryGuid"))%>' />
            </asp:TableCell>
            
            <asp:TableCell Style="display: none" Width="0px">
                <asp:HiddenField runat="server" ID="EmailAddress" Value='<%#Eval("EmailAddress")%>' />
            </asp:TableCell>
            <asp:TableCell Style="display: none" Width="0px">
                <asp:HiddenField runat="server" ID="UserGuid" Value='<%#Eval("UserGuid")%>' />
            </asp:TableCell>
            <asp:TableCell Style="display: none" Width="0px">
                <asp:HiddenField runat="server" ID="ShippingAddressGuid" Value='<%#Eval("ShippingAddressGuid")%>' />
            </asp:TableCell>
            <asp:TableCell Style="display: none" Width="0px">
                <asp:HiddenField runat="server" ID="AddressType" Value='<%#Eval("AddressType")%>' />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</div>
