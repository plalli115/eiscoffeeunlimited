<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ShippingAddressDataBoundInput.ascx.vb"
    Inherits="controls_shipping_parts_ShippingAddressDataBoundInput" %>
<%@ Register TagPrefix="exp" TagName="itemtemplate" Src="~/controls/shipping/ItemTemplates/ItemTemplate1.ascx" %>
<asp:DataList ID="ShippingAddressDisplay" runat="server" RepeatColumns="1">
    <ItemTemplate>
        <exp:itemtemplate ID="Itemtemplate1" runat="server" />
    </ItemTemplate>
</asp:DataList>
