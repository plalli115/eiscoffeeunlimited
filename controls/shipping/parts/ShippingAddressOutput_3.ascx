<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ShippingAddressOutput_3.ascx.vb"
    Inherits="controls_shipping_parts_ShippingAddressOutput_3" %>
<%@ Register TagPrefix="grb" Namespace="GroupRadioButton" Assembly="GroupRadioButton" %>
<%@ Register TagPrefix="exp" TagName="itemtemplate" Src="~/controls/shipping/ItemTemplates/ItemTemplate2.ascx" %>
<asp:DataList ID="ShippingAddressDisplay" runat="server" RepeatColumns="2" RepeatDirection="Horizontal">
    <ItemTemplate>
        <div class="radioDiv" style="padding-left: 5px; padding-top: 10px; padding-bottom: 10px">
            <grb:GroupRadioButton ID="selectradiobutton" runat="server" GroupName="UseAddr" Checked='<%#convertToChecked(Eval("IsDefault")) %>'>
            </grb:GroupRadioButton>
            <%=Resources.Language.LABEL_USE_ADDRESS%>
        </div>
        <exp:itemtemplate ID="Itemtemplate2" runat="server" />
    </ItemTemplate>
</asp:DataList>
