Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Collections.Generic

Partial Class controls_shipping_parts_ShippingProviderSelection
    Inherits ExpandIT.ShippingPart

    Private sqlString As String = String.Empty

    Public ReadOnly Property RadioButtonListName() As String
        Get
            Return Me.ShippingHandlingProviderGuid.UniqueID
        End Get
    End Property

    Public WriteOnly Property SqlStatement() As String
        Set(ByVal value As String)
            sqlString = value
        End Set
    End Property

    Public ReadOnly Property SelectedValue() As String
        Get
            Return ShippingHandlingProviderGuid.SelectedValue
        End Get
    End Property

    Protected Overloads Sub Page_Prerender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        If Not Page.IsCallback Then
            Me.registerCallbackScript()
            data1.ConnectionString = ExpandITLib.CurrentConnectionString
            data1.SelectCommand = sqlString
        End If

    End Sub

    Protected Sub radioList_OnDataBound(ByVal sender As Object, ByVal e As EventArgs) Handles ShippingHandlingProviderGuid.DataBound

        Dim rbl As RadioButtonList = CType(sender, RadioButtonList)
        Dim i As Integer = 0
        Dim defaultSelected As Integer = 0
        Dim defaultShippingProvider As String = ""

        If CStrEx(globals.OrderDict("ShippingHandlingProviderGuid")) = "" Or CStrEx(globals.OrderDict("ShippingHandlingProviderGuid")) = "NONE" Then
            defaultShippingProvider = getSingleValueDB("SELECT ShippingHandlingProviderGuid FROM ShippingHandlingProvider WHERE DefaultProvider=1")
        Else
            defaultShippingProvider = CStrEx(globals.OrderDict("ShippingHandlingProviderGuid"))
        End If

        For Each item As ListItem In rbl.Items
            item.Attributes.Add("onclick", setCallScript(i, ControlToUpdateOnCallback) & "toggleRadioButtons(" & i & ", '" & rbl.UniqueID & "');return true;")
            If item.Value = defaultShippingProvider Then
                defaultSelected = i
            End If
            item.Text = GetGlobalResourceObject("Language", item.Text)
            i += 1
        Next

        If rbl.Items.Count > 0 Then
            rbl.Items(defaultSelected).Selected = True
        End If
        '*****shipping*****-start
        If Not CBoolEx(AppSettings("EXPANDIT_US_USE_SHIPPING")) Then
            Me.RaiseCallbackEvent(defaultSelected.ToString())
            CType(Me.ControlToUpdateOnCallback(), Label).Text = GetCallbackResult()
        End If
        '*****shipping*****-END

    End Sub

End Class
