﻿Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Collections.Generic

Partial Class controls_shipping_parts_controls_ShippingAddress
    Inherits ExpandIT.ShippingPart

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Not CBoolEx(AppSettings("SHOW_STATE_ON_SHIPPING_ADDRESS")) Then
            TableRowState.Visible = False
        End If
        '*****shipping*****-start
        If AppSettings("EXPANDIT_US_USE_SHIPPING") Then
            If globals.OrderDict Is Nothing Then eis.LoadOrderDictionary(globals.User)
            If globals.OrderDict("ShipToZipCode") Is DBNull.Value Or globals.OrderDict("ShipToZipCode") Is Nothing Then
                Me.ZipCode.Text = ""
            Else
                Me.ZipCode.Text = globals.OrderDict("ShipToZipCode")
            End If
        End If
        '*****shipping*****-end

        If Not Page.IsPostBack Then


            Try
                setCountries()
            Catch ex As Exception

            End Try
        End If

    End Sub

    Protected Sub setCountries()
        ' Changed to handle unicode properly
        Dim CountryNames As New ExpandIT.ExpDictionary()
        Dim CountryGuids As New ExpandIT.ExpDictionary()
        Dim s As String = ""

        'GetCountries(CountryGuids, CountryNames)

        Dim b As KeyValuePair(Of Object, Object)
        Dim i As Integer = 0

        'DJW 8/10/2012 Eliminated Country From Being Displayed
        'For Each b In CountryNames
        '    s = CountryGuidsToText(Server.HtmlDecode(CStrEx(CountryGuids(b.Value))), CountryGuids, CountryNames)
        '    If s <> "" Then
        '        Me.CountryDropDownList1.Items.Insert(i, s)
        '        i += 1
        '    End If
        'Next
        'Me.CountryDropDownList1.Visible = "false"
    End Sub

    Public Overrides ReadOnly Property shipAddrData() As ExpDictionary
        Get
            Dim CountryNames As New ExpandIT.ExpDictionary()
            Dim CountryGuids As New ExpandIT.ExpDictionary()
            GetCountries(CountryGuids, CountryNames)
            shipAddrData = New ExpDictionary()
            Try
                ' DJW 8/13/2012 Eliminated Contact Name From Being Displayed
                'shipAddrData.Add("ContactName", ContactName.Text)

                shipAddrData.Add("ContactName", ContactName.Text)

                shipAddrData.Add("CompanyName", CompanyName.Text)
                shipAddrData.Add("Address1", Address1.Text)
                shipAddrData.Add("Address2", Address2.Text)
                shipAddrData.Add("ZipCode", ZipCode.Text)
                shipAddrData.Add("CityName", CityName.Text)
                shipAddrData.Add("StateName", StateName.Text)

                'DJW 8/10/2012 Eliminated Country From Being Displayed
                'shipAddrData.Add("CountryGuid", CountryGuids(CountryDropDownList1.Text))
                Dim tempCountry As String
                'WLB 10/10/2012 BEGIN - Needs to be a valid Country/Region Code in NAV. "US"
                'tempCountry = AppSettings("EXPANDIT_US_USE_US_SALES_TAX")
                tempCountry = AppSettings("DEFAULT_COUNTRYGUID")
                'WLB 10/10/2012 END
                shipAddrData.Add("CountryGuid", tempCountry)
                shipAddrData.Add("EmailAddress", EmailAddress.Text)
                shipAddrData.Add("UserGuid", UserGuid.Text)
                shipAddrData.Add("ShippingAddressGuid", ShippingAddressGuid.Text)
                shipAddrData.Add("AddressType", AddressType.Text)
                '<!-- ' ExpandIT US - ANA - US Sales Tax Shipping - Start -->
                shipAddrData.Add("ShippingAddressCode", ShippingAddressCode.Text)
                If AppSettings("EXPANDIT_US_USE_US_SALES_TAX") Then
                    shipAddrData.Add("TaxAreaCode", TaxAreaCode.Text)
                    shipAddrData.Add("TaxLiable", TaxLiable.Text)
                    '<!-- ' ExpandIT US - ANA - US Sales Tax Shipping - End -->
                End If
                'WLB.BEGIN.09272012   
                If ssiMakeAddressPermanent.Checked Then
                    shipAddrData.Add("ssiMakeAddressPermanent", 1)
                Else
                    shipAddrData.Add("ssiMakeAddressPermanent", 0)
                End If
                'WLB.10.11.2012 *** Change *** ALWAYS CHANGE THE E-MAIL TO WHAT IS
                '                              IN "YOUR EMAIL"
                'WLB.10.11.2012 If ssiMakeEmailPermanent.Checked Then
                shipAddrData.Add("ssiMakeEmailPermanent", 1)
                'WLB.10.11.2012 Else
                'WLB.10.11.2012 shipAddrData.Add("ssiMakeEmailPermanent", 0)
                'WLB.10.11.2012 End If
                'WLB.END.09272012
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
    End Property

    'DJW 8/10/2012 Eliminated Country From Being Displayed
    'Public ReadOnly Property DropdownID() As DropDownList
    '    Get
    '        Return CountryDropDownList1
    '    End Get
    'End Property

End Class

