Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass

Partial Class controls_shipping_parts_ShippingAddressOutput_3
    Inherits ExpandIT.ShippingPart

    ''' <summary>
    ''' Gets: A ref to the control's Datalist    
    ''' Sets: The Datalist's DataSource
    ''' </summary>
    ''' <value>A DataSource object</value>
    ''' <returns>Datalist</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property ShippingOutput() As DataList
        Get
            Return ShippingAddressDisplay
        End Get
    End Property

    Public WriteOnly Property DataListSource() As Object
        Set(ByVal value As Object)
            ShippingAddressDisplay.DataSource = value
        End Set
    End Property

End Class
