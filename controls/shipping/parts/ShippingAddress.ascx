﻿<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ShippingAddress.ascx.vb"
    Inherits="controls_shipping_parts_controls_ShippingAddress" EnableViewState="true" %>
<asp:Table ID="Table1" runat="server" CellSpacing="0">
    <asp:TableRow ID="TableRow1" runat="server">
        <asp:TableCell ID="TableCell1" runat="server" CssClass="TDDark" ColumnSpan="3" Height="1px"
            Width="350px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableHeaderRow ID="TableHeaderRow1" runat="server" Height="5px">
        <asp:TableHeaderCell ID="TableHeaderCell1" runat="server" ColumnSpan="3" Height="5px"></asp:TableHeaderCell>
    </asp:TableHeaderRow>
    <asp:TableRow ID="TableRow2" runat="server">
        <asp:TableCell ID="TableCell2" runat="server" CssClass="TDDark" ColumnSpan="3" Height="1px"></asp:TableCell>
    </asp:TableRow>    
    <%-- --DJW 8/13/2012 Eliminated Contact Name From Being Displayed--%>
    <asp:TableRow ID="TableRow3" runat="server" Visible="false">
        <asp:TableCell ID="TableCell3" runat="server" CssClass="TDR" Visible="false">
            <asp:Label ID="contact" runat="server" CssClass="shippingAddressFieldLabel" Visible="false" Text="<%$Resources: Language,LABEL_USERDATA_CONTACT %>">:</asp:Label>
        </asp:TableCell>        
        <asp:TableCell ID="TableCell28" runat="server" CssClass="TDL" Visible="false">
            <asp:TextBox runat="server" CssClass="borderTextBox" ID="ContactName" Visible="false"/>            
        </asp:TableCell>
        <asp:TableCell ID="TableCell29" runat="server" CssClass="TDC" Visible="false">
				&nbsp;&nbsp;
        </asp:TableCell>
    </asp:TableRow>    
   <%-- <asp:TableRow ID="TableRow4" runat="server">
       
        <asp:TableCell ID="TableCell4" runat="server" CssClass="TDL">
            <asp:Label runat="server" Visible="false" ID="ContactName" />            
        </asp:TableCell>
        <asp:TableCell ID="TableCell5" runat="server" CssClass="TDC">
				&nbsp;&nbsp;
        </asp:TableCell>
    </asp:TableRow>--%>    
     <asp:TableRow ID="TableRow5" runat="server">
        <asp:TableCell ID="TableCell6" runat="server" CssClass="TDR">
            <asp:Label ID="company" runat="server" CssClass="shippingAddressFieldLabel" Text="<%$Resources: Language,LABEL_USERDATA_COMPANY %>">:</asp:Label>
        </asp:TableCell>
        <asp:TableCell ID="TableCell7" runat="server" CssClass="TDL">
            <asp:TextBox runat="server" CssClass="borderTextBox" ID="CompanyName" />
        </asp:TableCell>
        <asp:TableCell ID="TableCell8" runat="server" CssClass="TDC">
				&nbsp;&nbsp;
        </asp:TableCell>
    </asp:TableRow>    
    <asp:TableRow ID="TableRow6" runat="server">
        <asp:TableCell ID="TableCell9" runat="server" CssClass="TDR">
            <asp:Label ID="lblAddress1" runat="server" CssClass="shippingAddressFieldLabel" Text="<%$Resources: Language,LABEL_USERDATA_ADDRESS_1 %>">:</asp:Label>
        </asp:TableCell>
        <asp:TableCell ID="TableCell10" runat="server" CssClass="TDL">
            <asp:TextBox runat="server" CssClass="borderTextBox" ID="Address1" />
            &nbsp;*
            <asp:RequiredFieldValidator runat="server" ID="Address1Validator" ControlToValidate="Address1"
                ValidationGroup="purchasegroup" ErrorMessage="<%$Resources: Language, MESSAGE_REQUIRED_FIELD %>" />
        </asp:TableCell>
        <asp:TableCell ID="TableCell11" runat="server" CssClass="TDC">
				&nbsp;&nbsp;
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow ID="TableRow7" runat="server">
        <asp:TableCell ID="TableCell12" runat="server" CssClass="TDR">
            <asp:Label ID="lblAddress2" runat="server" CssClass="shippingAddressFieldLabel" Text="<%$Resources: Language,LABEL_USERDATA_ADDRESS_2 %>">:</asp:Label>
        </asp:TableCell>
        <asp:TableCell ID="TableCell13" runat="server" CssClass="TDL">
            <asp:TextBox runat="server" CssClass="borderTextBox" ID="Address2" />
        </asp:TableCell>
        <asp:TableCell ID="TableCell14" runat="server" CssClass="TDC">
			&nbsp;&nbsp;
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow ID="TableRow8" runat="server">
        <asp:TableCell ID="TableCell18" runat="server" CssClass="TDR">
            <asp:Label ID="city" runat="server" CssClass="shippingAddressFieldLabel" Text="<%$Resources: Language,LABEL_USERDATA_CITY %>">:</asp:Label>
        </asp:TableCell>
        <asp:TableCell ID="TableCell19" runat="server" CssClass="TDL">
            <asp:TextBox runat="server" CssClass="borderTextBox" ID="CityName" />
            &nbsp;*
            <asp:RequiredFieldValidator runat="server" ID="CityNameValidator" ControlToValidate="CityName"
                ValidationGroup="purchasegroup" ErrorMessage="<%$Resources: Language, MESSAGE_REQUIRED_FIELD %>" />
        </asp:TableCell>
        <asp:TableCell ID="TableCell20" runat="server" CssClass="TDC">
			&nbsp;&nbsp;
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow ID="TableRowState" runat="server">
        <asp:TableCell ID="TableCellStateOutput" runat="server" CssClass="TDR">
            <asp:Label ID="state" runat="server" CssClass="shippingAddressFieldLabel" Text="<%$Resources: Language,LABEL_USERDATA_STATE %>">:</asp:Label>
        </asp:TableCell>
        <asp:TableCell ID="TableCellStateInput" runat="server" CssClass="TDL">
            <asp:TextBox runat="server" CssClass="borderTextBox" ID="StateName" />
        </asp:TableCell>
        <asp:TableCell ID="TableCell21" runat="server" CssClass="TDC">
			&nbsp;&nbsp;
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow ID="TableRow9" runat="server">
        <asp:TableCell ID="TableCell15" runat="server" CssClass="TDR">
            <asp:Label ID="lblZipCode" runat="server" CssClass="shippingAddressFieldLabel" Text="<%$Resources: Language,LABEL_USERDATA_ZIPCODE %>">:</asp:Label>
        </asp:TableCell>
        <asp:TableCell ID="TableCell16" runat="server" CssClass="TDL">
            <asp:TextBox runat="server" CssClass="borderTextBox" ID="ZipCode" />
            &nbsp;*
            <asp:RequiredFieldValidator runat="server" ID="ZipCodeValidator" ControlToValidate="ZipCode"
                ValidationGroup="purchasegroup" ErrorMessage="<%$Resources: Language, MESSAGE_REQUIRED_FIELD %>" />
        </asp:TableCell>
        <asp:TableCell ID="TableCell17" runat="server" CssClass="TDC">
			&nbsp;&nbsp;
        </asp:TableCell>
    </asp:TableRow>    
     <%--DJW 8/10/2012 Eliminated Country From Being Displayed--%>
  <%--<asp:TableRow ID="TableRowCountry" runat="server">
        <asp:TableCell ID="TableCell22" runat="server" Visible="false" CssClass="TDR">
            <asp:Label ID="LabelCountry" runat="server" Visible="false" CssClass="shippingAddressFieldLabel"
                Text="<%$Resources: Language,LABEL_USERDATA_COUNTRY %>">:</asp:Label>
        </asp:TableCell>
        <asp:TableCell ID="TableCell23" runat="server" CssClass="TDL">
            <div class="dpdBorder">
                <asp:DropDownList ID="CountryDropDownList1" runat="server" Visible="false" CssClass="borderDropDownList"
                    Width="150px">
                </asp:DropDownList>
            </div>
             &nbsp;&nbsp;*
            <asp:RequiredFieldValidator runat="server" ID="RequiredCountryValidator" ControlToValidate="CountryDropDownList1"
                ValidationGroup="purchasegroup" ErrorMessage="<%$Resources: Language, MESSAGE_REQUIRED_FIELD %>"
                Display="Dynamic" />
        </asp:TableCell>
        <asp:TableCell ID="TableCell24" Visible="false" runat="server" CssClass="TDC">
			&nbsp;&nbsp;
        </asp:TableCell>
    </asp:TableRow>--%>
    <asp:TableRow ID="TableRow11" runat="server">
        <asp:TableCell ID="TableCell23" runat="server" CssClass="TDR">
            <asp:Label ID="LabelssiMakeAddressPermanent" runat="server" CssClass="shippingAddressFieldLabel" Text="<%$Resources: Language,LABEL_SSI_MAKE_ADDRESS_PERM %>">:</asp:Label>
        </asp:TableCell>
        <asp:TableCell ID="TableCell24" runat="server" CssClass="TDL">
            <asp:CheckBox runat="server" ID="ssiMakeAddressPermanent" />
        </asp:TableCell>
        <asp:TableCell ID="TableCell30" runat="server" CssClass="TDC">
			&nbsp;&nbsp;
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow ID="TableRow10" runat="server">
        <asp:TableCell ID="TableCell25" runat="server" CssClass="TDR">
            <asp:Label ID="LabelEmail" runat="server" CssClass="shippingAddressFieldLabel" Text="<%$Resources: Language,LABEL_USERDATA_EMAIL %>">:</asp:Label>
        </asp:TableCell>
        <asp:TableCell ID="TableCell26" runat="server" CssClass="TDL">
            <asp:TextBox runat="server" CssClass="borderTextBox" ID="EmailAddress" />
            &nbsp;*
            <%--<asp:RequiredFieldValidator runat="server" ID="RequiredEmailAddressValidator" ControlToValidate="EmailAddress"
                ValidationGroup="purchasegroup" ErrorMessage="<%$Resources: Language, MESSAGE_REQUIRED_FIELD%>"
                Display="Dynamic" />--%>
            <asp:RegularExpressionValidator ValidationExpression="[\w-]+(?:\.[\w-]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,7}"
                ID="ValidEmailAddressValidator" runat="server" ControlToValidate="EmailAddress"
                ValidationGroup="purchasegroup" ErrorMessage="<%$Resources: Language, MESSAGE_EMAIL_ADDRESS_NOT_VALID %>" />
        </asp:TableCell>
        <asp:TableCell ID="TableCell27" runat="server" CssClass="TDC">
			&nbsp;&nbsp;
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow ID="TableRow4" runat="server">
        <asp:TableCell ID="TableCell4" runat="server" CssClass="TDR">
            <asp:Label ID="lblssiMakeEmailPermanent" runat="server" CssClass="shippingAddressFieldLabel" Text="<%$Resources: Language,LABEL_SSI_MAKE_EMAIL_PERM %>">:</asp:Label>
        </asp:TableCell>
        <asp:TableCell ID="TableCell5" runat="server" CssClass="TDL">
            <asp:CheckBox runat="server" ID="ssiMakeEmailPermanent" />
        </asp:TableCell>
        <asp:TableCell ID="TableCell22" runat="server" CssClass="TDC">
			&nbsp;&nbsp;
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
<div style="display: none" id="dibsas">
    <asp:TextBox runat="server" ID="UserGuid" BackColor="Transparent" ForeColor="Transparent" />
</div>
<div style="display: none" id="dibsat">
    <asp:TextBox runat="server" ID="ShippingAddressGuid" BackColor="Transparent" ForeColor="Transparent"
        BorderStyle="None" />
</div>
<div style="display: none" id="haddrestype">
    <asp:TextBox runat="server" CssClass="borderTextBox" ID="AddressType" BackColor="Transparent"
        ForeColor="Transparent" BorderStyle="None" />
</div>
<!-- ' ExpandIT US - ANA - US Sales Tax Shipping - Start -->
<%--<%  If System.Configuration.ConfigurationManager.AppSettings("EXPANDIT_US_USE_US_SALES_TAX") Then%>--%>
<div style="display: none" id="hShippingAddressCode">
    <asp:TextBox runat="server" CssClass="borderTextBox" ID="ShippingAddressCode" BackColor="Transparent"
        ForeColor="Transparent" BorderStyle="None" />
</div>
<div style="display: none" id="hTaxAreaCode">
    <asp:TextBox runat="server" CssClass="borderTextBox" ID="TaxAreaCode" BackColor="Transparent"
        ForeColor="Transparent" BorderStyle="None" />
</div>
<div style="display: none" id="hTaxLiable">
    <asp:TextBox runat="server" CssClass="borderTextBox" ID="TaxLiable" BackColor="Transparent"
        ForeColor="Transparent" BorderStyle="None" />
</div>
<%--<%  End If%>--%>
<!-- ' ExpandIT US - ANA - US Sales Tax Shipping - End -->

