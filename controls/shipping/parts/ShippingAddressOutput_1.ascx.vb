Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports GroupRadioButton.GroupRadioButton

Partial Class controls_ShippingAddressOutput_1
    Inherits ExpandIT.ShippingPart

    Private _default As Boolean

    ''' <summary>
    ''' Gets: A ref to the control's Datalist
    ''' </summary>
    ''' <value>A DataSource object</value>
    ''' <returns>Datalist</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property ShippingOutput() As DataList
        Get
            Return ShippingAddressDisplay
        End Get
    End Property

    ''' <summary>
    ''' Sets: The Datalist's DataSource
    ''' </summary>
    ''' <value></value>
    ''' <remarks></remarks>
    Public WriteOnly Property DataListSource() As Object
        Set(ByVal value As Object)
            ShippingAddressDisplay.DataSource = value
        End Set
    End Property

    Public Overrides Function convertToChecked(ByVal isDefault As Boolean) As Boolean
        If isDefault Then
            _default = True
            Return True
        End If
    End Function

    Public Overrides Function convertToChecked(ByVal isDefault As DBNull) As Boolean
        
    End Function

    Protected Sub ShippingAddressDisplay_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles ShippingAddressDisplay.ItemCommand
        If e.CommandName = "delete" Then
            Me.SetPtr.DynamicInvoke(e)
        End If
    End Sub

    Protected Sub ShippingAddressDisplay_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ShippingAddressDisplay.SelectedIndexChanged
        Dim x As String = "test"
    End Sub

    Protected Function IsB2B(ByVal key As String) As Boolean
        If Left(key, 3) = "B2C" Then Return True
    End Function

    Protected Function IsB2Bdisbld(ByVal key As String) As Boolean
        If Left(key, 3) = "B2B" Then Return True
    End Function

End Class
