Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Collections.Generic

Partial Class controls_shipping_parts_DataBoundList
    Inherits ExpandIT.ShippingPart

    ''' <summary>
    ''' Gets a reference to the control's Datalist
    ''' </summary>
    ''' <value>A DataSource object</value>
    ''' <returns>Datalist</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property ShippingOutput() As DataList
        Get
            Return ShippingAddressDisplay
        End Get
    End Property

    ''' <summary>
    ''' Sets the Datalist's DataSource
    ''' </summary>
    ''' <value></value>
    ''' <remarks></remarks>
    Public WriteOnly Property DataListSource() As Object
        Set(ByVal value As Object)
            ShippingAddressDisplay.DataSource = value
        End Set
    End Property

End Class