<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ShippingAddressDataBoundInputOutput.ascx.vb"
    Inherits="controls_shipping_parts_ShippingAddressDataBoundInputOutput" %>
<%@ Register TagPrefix="grb" Namespace="GroupRadioButton" Assembly="GroupRadioButton" %>
<%@ Register TagPrefix="exp" TagName="itemtemplate1" Src="~/controls/shipping/ItemTemplates/ItemTemplate1.ascx" %>
<%@ Register TagPrefix="exp" TagName="itemtemplate2" Src="~/controls/shipping/ItemTemplates/ItemTemplate2.ascx" %>
<div class="majorshipdiv">
    <div class="B2B3Div">
        <%-- These are the existing Ship-tos --%>
        <%-- ' Per ExpandIT Case#ISR-897859 Added RepeatDirection="Horizontal" --%>
        <asp:DataList ID="ShippingAddressDisplay1" runat="server" RepeatDirection="Horizontal" RepeatColumns="2">
            <ItemTemplate>
                <div class="RadioDiv">
                    <grb:GroupRadioButton ID="selectradiobutton" runat="server" GroupName="UseAddr" Checked='<%#convertToChecked(Eval("IsDefault")) %>'>
                    </grb:GroupRadioButton>
                    <%=Resources.Language.LABEL_USE_ADDRESS%>
                </div>
                <exp:itemtemplate2 ID="Itemtemplate2" runat="server" />
            </ItemTemplate>
        </asp:DataList>
    </div>
    <%-- ' Per ExpandIT Case#ISR-897859 Added RepeatDirection="Horizontal" --%>
    <asp:DataList ID="ShippingAddressDisplay2" runat="server" RepeatDirection="Horizontal" RepeatColumns="1">
        <ItemTemplate>
            <%-- 12/07/2012 WLB This Extraneous <div> causes the section to be pushed off to the right.
                But, without this <div>, the wrong shipping address gets selected again.
                
                We're going to have to use CSS to get this layout altered.
            --%>
            <div class="ShippingTableOutputDisplayDiv">
                <div class="RadioDiv">
                    <grb:GroupRadioButton ID="selectradiobutton" runat="server" GroupName="UseAddr" Checked='<%#convertToChecked(Eval("IsDefault")) %>'>
                    </grb:GroupRadioButton>
                    <%=Resources.Language.LABEL_USE_ADDRESS%>
                </div>
                <exp:itemtemplate1 runat="server" />
            </div>
        </ItemTemplate>
    </asp:DataList>
</div>
<div class="caddress" style="padding-left: 0px; padding-bottom: 20px">
</div>
<div class="linediv">
</div>
