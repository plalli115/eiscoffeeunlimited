Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports GroupRadioButton.GroupRadioButton
Imports System.Collections.Generic

Partial Class controls_shipping_parts_ShippingAddressDataBoundInputOutput
    Inherits ExpandIT.ShippingPart

    Private _default As Boolean

    ''' <summary>
    ''' Gets: A ref to the control's Datalist  
    ''' </summary>
    ''' <value>A DataSource object</value>
    ''' <returns>Datalist</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property ShippingOutput() As DataList
        Get
            Return ShippingAddressDisplay1
        End Get
    End Property
    ''' <summary>
    ''' Sets: The Datalist's DataSource
    ''' </summary>
    ''' <value>Object</value>
    ''' <remarks></remarks>
    Public WriteOnly Property DataListSource() As Object
        Set(ByVal value As Object)
            ShippingAddressDisplay1.DataSource = value
        End Set
    End Property

    ''' <summary>
    ''' Gets: A ref to the control's Datalist 
    ''' </summary>
    ''' <value>A DataSource object</value>
    ''' <returns>Datalist</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property ShippingOutput2() As DataList
        Get
            Return ShippingAddressDisplay2
        End Get
    End Property

    ''' <summary>
    ''' Sets: The Datalist's DataSource
    ''' </summary>
    ''' <value>Object</value>
    ''' <remarks></remarks>
    Public WriteOnly Property DataListSource2() As Object
        Set(ByVal value As Object)
            ShippingAddressDisplay2.DataSource = value
        End Set
    End Property

    Public Overrides Function convertToChecked(ByVal isDefault As Boolean) As Boolean
        If isDefault Then
            _default = True
            Return True
        End If
    End Function

    Public Overrides Function convertToChecked(ByVal isDefault As DBNull) As Boolean

    End Function

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'WLB.10032012.BEGIN -- changed to make consistent with all other pages.
        'If CBoolEx(AppSettings("SHOW_STATE_ON_SHIPPING_ADDRESS")) Then 'change when correct DB is used
        ' Per ExpandIT Case#ISR-897859 BEGIN
        'If Not CBoolEx(AppSettings("SHOW_STATE_ON_SHIPPING_ADDRESS")) Then 'change when correct DB is used
        'WLB.10032012.END
        'Me.ShippingAddressDisplay2.TemplateControl.FindControl("TableRowState").Visible = False
        'End If
        ' Per ExpandIT Case#ISR-897859 END

        If Not Page.IsPostBack Then
            Try
                setCountries()
                setErrorMessage()
            Catch ex As Exception

            End Try
        End If

    End Sub

    Protected Sub setCountries()
        Dim CountryNames As New ExpandIT.ExpDictionary()
        Dim CountryGuids As New ExpandIT.ExpDictionary()
        Dim s As String = ""
        Dim c As New Control()

        Dim dt As Data.DataTable = ShippingAddressDisplay2.DataSource
        Dim str As String = String.Empty
        Try
           str = dt.Rows(0)("CountryGuid")
        Catch ex As Exception

        End Try
        GetCountries(CountryGuids, CountryNames)
        Dim i As Integer = 0

        '--DJW 8/10/2012 Eliminated Country From Being Displayed
        'For Each b As KeyValuePair(Of Object, Object) In CountryNames
        '    s = Server.UrlEncode(CStrEx(CountryGuids(b.Value)))
        '    If s <> "" Or Not IsDBNull(s) Then
        '        CType(findNamedControl(ShippingAddressDisplay2, "CountryDropDownList1", c), DropDownList).Items.Insert(i, b.Value)
        '        If s = str Then
        '            CType(findNamedControl(ShippingAddressDisplay2, "CountryDropDownList1", c), DropDownList).SelectedIndex = i
        '        End If
        '        i += 1
        '    End If
        'Next

    End Sub

    Public Overrides ReadOnly Property shipAddrData() As ExpDictionary
        Get
            Dim CountryNames As New ExpandIT.ExpDictionary()
            Dim CountryGuids As New ExpandIT.ExpDictionary()
            Dim c As New Control()
            GetCountries(CountryGuids, CountryNames)
            shipAddrData = New ExpDictionary()
            Try
                shipAddrData.Add("ContactName", CType(findNamedControl(ShippingAddressDisplay2, "ContactName", c), TextBox).Text)
                shipAddrData.Add("CompanyName", CType(findNamedControl(ShippingAddressDisplay2, "CompanyName", c), TextBox).Text)
                shipAddrData.Add("Address1", CType(findNamedControl(ShippingAddressDisplay2, "Address1", c), TextBox).Text)
                shipAddrData.Add("Address2", CType(findNamedControl(ShippingAddressDisplay2, "Address2", c), TextBox).Text)
                shipAddrData.Add("ZipCode", CType(findNamedControl(ShippingAddressDisplay2, "ZipCode", c), TextBox).Text)
                shipAddrData.Add("CityName", CType(findNamedControl(ShippingAddressDisplay2, "CityName", c), TextBox).Text)
                shipAddrData.Add("StateName", CType(findNamedControl(ShippingAddressDisplay2, "StateName", c), TextBox).Text)

                '--DJW 8/10/2012 Eliminated Country From Being Displayed
                'shipAddrData.Add("CountryGuid", CountryGuids(CType(findNamedControl(ShippingAddressDisplay2, "CountryDropDownList1", c), DropDownList).SelectedItem.Text))

                shipAddrData.Add("EmailAddress", CType(findNamedControl(ShippingAddressDisplay2, "EmailAddress", c), TextBox).Text)
                shipAddrData.Add("UserGuid", CType(findNamedControl(ShippingAddressDisplay2, "UserGuid", c), TextBox).Text)
                shipAddrData.Add("ShippingAddressGuid", CType(findNamedControl(ShippingAddressDisplay2, "ShippingAddressGuid", c), TextBox).Text)
                shipAddrData.Add("AddressType", CType(findNamedControl(ShippingAddressDisplay2, "AddressType", c), TextBox).Text)

            Catch ex As Exception
            End Try
        End Get
    End Property

    Public WriteOnly Property Text1() As String
        Set(ByVal value As String)
            Dim c As New Control()
            CType(findNamedControl(ShippingAddressDisplay2, "ContactName", c), TextBox).Text = value
        End Set
    End Property

    '--DJW 8/10/2012 Eliminated Country From Being Displayed
    'Public ReadOnly Property DropdownID() As DropDownList
    '    Get
    '        Dim c As New Control()
    '        Return CType(findNamedControl(ShippingAddressDisplay2, "CountryDropDownList1", c), DropDownList)
    '    End Get
    'End Property

    Protected Sub setErrorMessage()
        Dim c As New Control()
        Dim errorMessage As String = " * " & Resources.Language.MESSAGE_THE_VALUE_IN_FIELD_IS_NOT_CORRECT_1 _
            & Resources.Language.MESSAGE_THE_VALUE_IN_FIELD_IS_NOT_CORRECT_2 & " 1 " _
            & Resources.Language.MESSAGE_THE_VALUE_IN_FIELD_IS_NOT_CORRECT_3 & " 30 " _
            & Resources.Language.MESSAGE_THE_VALUE_IN_FIELD_IS_NOT_CORRECT_4

        CType(findNamedControl(ShippingAddressDisplay2, "ContactNameValidator", c), RequiredFieldValidator).ErrorMessage = errorMessage
        CType(findNamedControl(ShippingAddressDisplay2, "Address1Validator", c), RequiredFieldValidator).ErrorMessage = errorMessage
        CType(findNamedControl(ShippingAddressDisplay2, "ZipCodeValidator", c), RequiredFieldValidator).ErrorMessage = errorMessage
        CType(findNamedControl(ShippingAddressDisplay2, "CityNameValidator", c), RequiredFieldValidator).ErrorMessage = errorMessage
        CType(findNamedControl(ShippingAddressDisplay2, "EmailAddressValidator", c), RequiredFieldValidator).ErrorMessage = errorMessage

    End Sub
End Class
