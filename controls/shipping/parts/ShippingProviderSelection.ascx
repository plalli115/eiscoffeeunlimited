<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ShippingProviderSelection.ascx.vb"
    Inherits="controls_shipping_parts_ShippingProviderSelection" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.GlobalsClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<table>
    <tr>
        <td>
            <br />
            <br />
            <!-- Here begins HTML code for the shipping provider selection -->
            <h1>
                <%=Resources.Language.LABEL_SHIPPING_PROVIDER_SELECTION%>
                <a href="<%=VRoot %>/shippinghandling.aspx">
                    <img alt="0" src="<%=VRoot %>/images/searchbutton.gif" border="0" /></a>
            </h1>
            <table>
                <tr>
                    <td>
                        <b><%=Resources.Language.LABEL_SHIPPING_PROVIDER%></b>
                    </td>
                </tr>
                            <tr>
                    <td>
                        <asp:SqlDataSource runat="server" ID="data1" />
                        <asp:RadioButtonList ID="ShippingHandlingProviderGuid" runat="server" DataSourceID="data1"
                            DataTextField="ProviderName" DataValueField="ShippingHandlingProviderGuid" AutoPostBack="false"
                            RepeatDirection="Vertical">
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <div id="shipcostlabeldiv" style="padding-left: 0px;">
                            <div style="padding-left: 0px;">
                                <asp:Label runat="server" ID="shipcostlbl" Text="Test text" />
                            </div>
                        </div>
                    </td>
                </tr>
    </table> </td> </tr>
</table>
<!-- Here ends HTML code for the shipping provider selection -->

<script type="text/javascript">
        // Toggle radiobuttons programmatically
        function toggleRadioButtons(index, obj){
            var i = 0;
            var rbs = getRbs(obj);                    
            for(i = 0; i < rbs.length; i++){
                if(rbs[i].checked){
                    rbs[i].checked = false;
                }
            }
            if (i > 0)
                rbs[index].checked = true;
            else
                rbs.checked = true;
            return true;
        }
        // Get all radiobuttons in group "UseAddr"
        function getRbs(obj){
            var rbs;             
            rbs = document.getElementsByName(obj);         
            return rbs;
        }                  
</script>

