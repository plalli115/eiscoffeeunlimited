﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Output_1.ascx.vb" Inherits="controls_shipping_parts_Output_1" %>
<%@ Register TagPrefix="grb" Namespace="GroupRadioButton" Assembly="GroupRadioButton" %>
<%@ Register TagPrefix="exp" TagName="itemtemplate" Src="~/controls/shipping/ItemTemplates/ItemTemplate1.ascx" %>
<%@ Register Namespace="ExpandIT.Buttons" TagPrefix="expBtn" %>
<div class="test123">
    <div class="majorshipdiv">
        <div class="OnlyOneAddressOuter">
            <asp:DataList ID="ShippingAddressDisplay" runat="server" RepeatDirection="Horizontal"
                RepeatColumns="2">
                <ItemTemplate>
                    <div class="RadioDiv" id="radioDiv">
                        <grb:GroupRadioButton ID="selectradiobutton" runat="server" GroupName="UseAddr" CssClass="Output1GroupBtn"
                            Checked='<%#convertToChecked(Eval("IsDefault")) %>'></grb:GroupRadioButton>
                        <asp:Literal ID="radiotext" runat="server" Text="<%$Resources: Language, LABEL_USE_ADDRESS %>" />
                    </div>
                    <exp:itemtemplate ID="Itemtemplate1" runat="server" />
                    <div class="caddress">
                    </div>
                </ItemTemplate>
            </asp:DataList>
        </div>
    </div>
    <div class="linediv">
    </div>
</div>
