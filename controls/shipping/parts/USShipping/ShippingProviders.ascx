﻿<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ShippingProviders.ascx.vb"
    Inherits="controls_shipping_parts_ShippingProviders" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.GlobalsClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<asp:Panel ID="providerPanel" runat="server">
    <!-- AM2010031501 - ORDER RENDER - Start -->
    <asp:Panel ID="pnlProviderEdit" runat="server" Visible="false">
        <table style="margin: 10px;">
            <tr>
                <td>
                    <asp:SqlDataSource runat="server" ID="data1" />
                    <asp:SqlDataSource runat="server" ID="data2" />
                    <table style="width: 300px;">
                        <tr style="height: 17px;">
                            <td align="left" style="width: 112px; padding-left: 3px;">
                                <asp:Label ID="ShippingProvidersLbl" Text="<%$Resources: Language,LABEL_SHIPPING_PROVIDER %>"
                                    runat="server" Font-Bold="True" Width="112px"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:Label ID="ShippingProviderServiceLbl" Text="<%$Resources: Language,LABEL_PROVIDER_SERVICE %>"
                                    runat="server" Font-Bold="True" Width="132px"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <table style="width: 300px;">
                        <tr>
                            <td style="height: 24px; width: 120px; vertical-align: top;">
                                <asp:DropDownList ID="ShippingHandlingProviderGuid" CssClass="ShippingHandlingProviderGuid"
                                    runat="server" DataSourceID="data1" DataTextField="ShippingHandlingProviderGuid"
                                    AutoPostBack="true" DataValueField="ShippingHandlingProviderGuid" EnableViewState="true"
                                    Width="107px">
                                </asp:DropDownList>&nbsp;
                            </td>
                            <td style="height: 24px; width: 202px; vertical-align: top;">
                                <asp:DropDownList ID="ShippingHandlingService" CssClass="ShippingHandlingService"
                                    runat="server" DataSourceID="data2" DataTextField="Description" DataValueField="ShippingProviderServiceCode"
                                    EnableViewState="true" Width="175px">
                                </asp:DropDownList>&nbsp;
                            </td>
                        </tr>
                    </table>
                    <table style="width: 300px; height: 29px">
                        <tr class="TR1">
                            <td class="TDL" style="width: 85px">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="cbxResidentialAddress" runat="server" AutoPostBack="True" />
                                        </td>
                                        <td style="padding-top: 3px;">
                                            <asp:Label ID="Label1" runat="server" Text="<%$Resources: Language,LABEL_SHIPPING_RESIDENTIAL %>"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="TDL" style="width: 120px">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="cbxSaturdayDelivery" runat="server" AutoPostBack="True" />
                                        </td>
                                        <td style="padding-top: 3px; width: 100px;">
                                            <asp:Label ID="Label2" runat="server" Text="<%$Resources: Language,LABEL_SHIPPING_SATURDAY %>"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="TDL" style="width: 100px; height: 26px;" align="right">
                                <table>
                                    <tr>
                                        <td style="padding-top: 3px;">
                                            <% =Resources.Language.LABEL_INPUT_ZIPCODE%>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="ShippingZipCode" class="ShippingZipCode" runat="server" Width="35px"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <table style="width: 300px;">
                        <tr class="TR1">
                            <td class="TDL" style="width: 65px">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="cbxInsurance" runat="server" AutoPostBack="false" />
                                        </td>
                                        <td style="padding-top: 3px;">
                                            <asp:Label ID="Label3" runat="server" Text="<%$Resources: Language,LABEL_SHIPPING_INSURANCE %>"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width: 120px">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label runat="server" ID="lblInsuredValue" Style="padding-top: 4px;" Text="Insured Value"
                                                Width="66px"></asp:Label>
                                        </td>
                                        <td style="padding-top: 2px;">
                                            <asp:TextBox ID="txtInsuredValue" runat="server" CssClass="txtInsuredValue" Width="68px"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="TDL" align="right" style="height: 26px; width: 98px;">
                                <asp:Button runat="server" CssClass="AddButton" Style="margin-top: 3px;" ID="Apply"
                                    Text="<%$Resources: Language,LABEL_SHIPPING_CALCULATE %>" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlProviderNoEdit" runat="server" Visible="false">
        <table style="margin: 10px;">
            <tr>
                <td align="left" style="width: 112px; padding-left: 3px;">
                    <asp:Label ID="ShippingServiceLbl" Text="<%$Resources: Language,LABEL_SHIPPING_SERVICE %>"
                        runat="server" Font-Bold="True" Width="112px"></asp:Label>
                </td>
                <td style="height: 24px; width: 202px; vertical-align: top;">
                    <asp:Label ID="ShippingServiceValueLbl" runat="server" Width="132px"></asp:Label>
                </td>
            </tr>
            <asp:Panel ID="pnlShippingOptions" runat="server" Visible="false">
                <tr>
                    <td align="left" style="width: 112px; padding-left: 3px;">
                        <asp:Label ID="Label4" Text="Residential" runat="server" Font-Bold="True" Width="112px"></asp:Label>
                    </td>
                    <td style="width: 202px; vertical-align: top;">
                        <asp:Label ID="lblResidential" runat="server" Width="132px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 112px; padding-left: 3px;">
                        <asp:Label ID="Label5" Text="Saturday Delivery" runat="server" Font-Bold="True" Width="112px"></asp:Label>
                    </td>
                    <td style="width: 202px; vertical-align: top;">
                        <asp:Label ID="lblSaturdayDelivery" runat="server" Width="132px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 112px; padding-left: 3px;">
                        <asp:Label ID="Label6" Text="Insurance" runat="server" Font-Bold="True" Width="112px"></asp:Label>
                    </td>
                    <td style="width: 202px; vertical-align: top;">
                        <asp:Label ID="lblInsurance" runat="server" Width="132px"></asp:Label>
                    </td>
                </tr>
                <asp:Panel ID="pnlInsuredValue" runat="server" Visible="false">
                    <tr>
                        <td align="left" style="width: 112px; padding-left: 3px;">
                            <asp:Label ID="Label7" Text="Insured Value" runat="server" Font-Bold="True" Width="112px"></asp:Label>
                        </td>
                        <td style="width: 202px; vertical-align: top;">
                            <asp:Label ID="lblInsValue" runat="server" Width="132px"></asp:Label>
                        </td>
                    </tr>
                </asp:Panel>
            </asp:Panel>
        </table>
    </asp:Panel>
    <!-- AM2010031501 - ORDER RENDER - End -->
</asp:Panel>
