﻿Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Collections.Generic
Imports System.Data
Imports System.Net

Partial Class controls_shipping_parts_ShippingProviders
    Inherits ExpandIT.ShippingPart

    'AM2010031501 - ORDER RENDER - Start
    Private m_EditMode As Boolean
    Private m_OrderDict As ExpDictionary
    'AM2010031501 - ORDER RENDER - End
    Private sqlString As String = String.Empty
    Private agentCalculation As Integer = 0
    Private shippingService As String = ""
    Private errorMessage As String = ""
    Const CalculatedFieldName As String = "TotalInclTax"
    Private shipping As New liveShipping()


#Region "Properties"

    'AM2010031501 - ORDER RENDER - Start
    Public Property EditMode() As Boolean
        Get
            Return CBoolEx(m_EditMode)
        End Get
        Set(ByVal value As Boolean)
            m_EditMode = CBoolEx(value)
        End Set
    End Property

    Public Property OrderDict() As ExpDictionary
        Get
            Return m_OrderDict
        End Get
        Set(ByVal value As ExpDictionary)
            m_OrderDict = value
        End Set
    End Property

    'AM2010031501 - ORDER RENDER - End

    Public WriteOnly Property SqlStatement() As String
        Set(ByVal value As String)
            sqlString = value
        End Set
    End Property

    Public Property SelectedProviderValue() As String
        Get
            Return Me.ShippingHandlingProviderGuid.SelectedValue
        End Get
        Set(ByVal value As String)
            If Not Me.ShippingHandlingProviderGuid.Items.FindByValue(value) Is Nothing Then
                Me.ShippingHandlingProviderGuid.SelectedValue = value
            Else
                If Me.ShippingHandlingProviderGuid.Items.FindByValue("NONE") Is Nothing Then
                    Me.ShippingHandlingProviderGuid.Items.Insert(0, "NONE")
                End If
                Me.ShippingHandlingProviderGuid.SelectedValue = "NONE"
                globals.OrderDict("ShippingHandlingProviderGuid") = "NONE"
            End If
            'populateService()
        End Set
    End Property

    Public Property SelectedServiceValue() As String
        Get
            Return Me.ShippingHandlingService.SelectedValue
        End Get
        Set(ByVal value As String)
            If Not Me.ShippingHandlingService.Items.FindByValue(value) Is Nothing Then
                Me.ShippingHandlingService.SelectedValue = value
            Else
                Me.ShippingHandlingService.SelectedIndex = 0
                globals.OrderDict("ServiceCode") = SelectedServiceValue()
            End If
        End Set
    End Property

    Public Property zipcode() As String
        Get
            Return Me.ShippingZipCode.Text
        End Get
        Set(ByVal value As String)
            Me.ShippingZipCode.Text = value
        End Set
    End Property

    Public Property IsResidential() As Boolean
        Get
            Return Me.cbxResidentialAddress.Checked
        End Get
        Set(ByVal value As Boolean)
            Me.cbxResidentialAddress.Checked = value
        End Set
    End Property

    Public Property IsSaturdayDelivery() As Boolean
        Get
            Return Me.cbxSaturdayDelivery.Checked
        End Get
        Set(ByVal value As Boolean)
            Me.cbxSaturdayDelivery.Checked = value
        End Set
    End Property

    Public Property IsInsurance() As Boolean
        Get
            Return Me.cbxInsurance.Checked
        End Get
        Set(ByVal value As Boolean)
            Me.cbxInsurance.Checked = value
        End Set
    End Property

    Public Property InsuredValue() As String
        Get
            Return Me.txtInsuredValue.Text
        End Get
        Set(ByVal value As String)
            Me.txtInsuredValue.Text = value
        End Set
    End Property

#End Region

    Protected Overloads Sub Page_Prerender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'AM2010031501 - ORDER RENDER - Start
        Me.pnlProviderEdit.Visible = EditMode
        Me.pnlProviderNoEdit.Visible = Not EditMode
        If EditMode Then

        Else
            If Not OrderDict Is Nothing Then
                If CStrEx(OrderDict("ShippingHandlingProviderGuid")) <> "" And CStrEx(OrderDict("ServiceCode")) <> "" Then
                    Dim providerDescription As String = ""
                    Dim sql As String = ""
                    sql = "SELECT ShippingHandlingProviderGuid + ' ' + ProviderDescription FROM " & _
                        "USShippingHandlingProvider WHERE ShippingHandlingProviderGuid=" & _
                        SafeString(CStrEx(OrderDict("ShippingHandlingProviderGuid"))) & " AND " & _
                        "ShippingProviderServiceCode=" & SafeString(CStrEx(OrderDict("ServiceCode")))
                    providerDescription = CStrEx(getSingleValueDB(sql))
                    If providerDescription = "" Then
                        providerDescription = CStrEx(OrderDict("ShippingHandlingProviderGuid")) & "-" & CStrEx(OrderDict("ServiceCode"))
                    End If
                    ShippingServiceValueLbl.Text = providerDescription
                    pnlShippingOptions.Visible = True
                    If CBoolEx(OrderDict("ResidentialShipping")) Then
                        lblResidential.Text = "Yes"
                    Else
                        lblResidential.Text = "No"
                    End If
                    If CBoolEx(OrderDict("SaturdayDelivery")) Then
                        lblSaturdayDelivery.Text = "Yes"
                    Else
                        lblSaturdayDelivery.Text = "No"
                    End If
                    If CBoolEx(OrderDict("Insurance")) Then
                        lblInsurance.Text = "Yes"
                        If CStrEx(OrderDict("ShippingHandlingProviderGuid")) = "FEDEX" Then
                            pnlInsuredValue.Visible = True
                            lblInsValue.Text = CurrencyFormatter.FormatCurrency(CDblEx(OrderDict("InsuredValue")), CStrEx(OrderDict("CurrencyGuid")))
                        End If
                    Else
                        lblInsurance.Text = "No"
                        pnlInsuredValue.Visible = False
                    End If
                Else
                    ShippingServiceValueLbl.Text = Resources.Language.LABEL_NOT_SELECTED
                    pnlShippingOptions.Visible = False
                End If
            End If
        End If
        'AM2010031501 - ORDER RENDER - End

    End Sub
    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If EditMode Then
            globals.OrderDict = eis.LoadOrderDictionary(globals.User)

            If Not Page.IsCallback Then
                If Not globals.OrderDict("ShipToZipCode") Is DBNull.Value Then
                    zipcode = globals.OrderDict("ShipToZipCode")
                End If
                If Not globals.OrderDict("ResidentialShipping") Is DBNull.Value Then
                    IsResidential = CBoolEx(globals.OrderDict("ResidentialShipping"))
                End If
                If Not globals.OrderDict("SaturdayDelivery") Is DBNull.Value Then
                    IsSaturdayDelivery = CBoolEx(globals.OrderDict("SaturdayDelivery"))
                End If
                If Not globals.OrderDict("Insurance") Is DBNull.Value Then
                    IsInsurance = CBoolEx(globals.OrderDict("Insurance"))
                End If
                If Not globals.OrderDict("InsuredValue") Is DBNull.Value Then
                    InsuredValue = globals.OrderDict("InsuredValue")
                End If
                Me.registerCallbackScript()
                data1.ConnectionString = ExpandITLib.CurrentConnectionString
                sqlString = "SELECT DISTINCT ShippingHandlingProviderGuid FROM USShippingHandlingProvider"
                data1.SelectCommand = sqlString
                ShippingHandlingProviderGuid.DataBind()
            End If

            If globals.OrderDict("Lines") Is Nothing Then
                globals.OrderDict("ShippingHandlingProviderGuid") = "NONE"
                SelectedProviderValue = "NONE"
            Else
                If SelectedProviderValue = "" Then
                    If Not globals.OrderDict("ShippingHandlingProviderGuid") Is Nothing And CStrEx(globals.OrderDict("ShippingHandlingProviderGuid")) <> "" Then
                        SelectedProviderValue = globals.OrderDict("ShippingHandlingProviderGuid")
                    Else
                        globals.OrderDict("ShippingHandlingProviderGuid") = "NONE"
                        SelectedProviderValue = "NONE"
                    End If
                Else
                    If Not globals.OrderDict("ShippingHandlingProviderGuid") Is Nothing And CStrEx(globals.OrderDict("ShippingHandlingProviderGuid")) <> "" Then
                        If SelectedProviderValue <> globals.OrderDict("ShippingHandlingProviderGuid") Then
                            'Value has changed, populate services and calculate shipping for the first one
                            globals.OrderDict("ShippingHandlingProviderGuid") = SelectedProviderValue
                        End If
                    Else
                        globals.OrderDict("ShippingHandlingProviderGuid") = SelectedProviderValue
                    End If
                End If
            End If

            populateService()
        End If

    End Sub

    Protected Sub doApply()
        globals.OrderDict = eis.LoadOrderDictionary(globals.User)
        globals.Context = eis.LoadContext(globals.User)
        globals.OrderDict("ShipToZipCode") = zipcode
        globals.OrderDict("ResidentialShipping") = IsResidential
        globals.OrderDict("SaturdayDelivery") = IsSaturdayDelivery
        globals.OrderDict("Insurance") = IsInsurance
        globals.OrderDict("InsuredValue") = InsuredValue
        globals.OrderDict("ShippingHandlingProviderGuid") = SelectedProviderValue
        globals.OrderDict("ServiceCode") = SelectedServiceValue
        eis.SaveOrderDictionary(globals.OrderDict)
        globals.OrderDict("IsCalculated") = False
        eis.CalculateOrder()
    End Sub

    Protected Sub ShippingHandlingProviderGuid_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles ShippingHandlingProviderGuid.DataBound
        SelectedProviderValue = CStrEx(globals.OrderDict("ShippingHandlingProviderGuid"))
        eis.SaveOrderDictionary(globals.OrderDict)
        populateService()
    End Sub

    Protected Sub ShippingHandlingService_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles ShippingHandlingService.DataBound

        Dim foundService As Boolean

        SelectedServiceValue = CStrEx(globals.OrderDict("ServiceCode"))

        If SelectedProviderValue <> "FEDEX" Then
            globals.OrderDict("InsuredValue") = 0
        End If
        eis.SaveOrderDictionary(globals.OrderDict)
        doApply()
    End Sub

    Protected Sub cbxResidentialAddress_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxResidentialAddress.CheckedChanged

        If ShippingHandlingProviderGuid.SelectedValue = "USPS" Then
            If Me.cbxResidentialAddress.Checked Then
                Response.Write("<script language='javascript'>alert(' " & AppSettings("USPS_RESIDENTIAL_MESSAGE") & " ')</script>")
            End If
        End If

        doApply()

    End Sub
    Protected Sub cbxSaturdayDelivery_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxSaturdayDelivery.CheckedChanged


        If ShippingHandlingProviderGuid.SelectedValue = "USPS" Then
            If Me.cbxSaturdayDelivery.Checked Then
                Response.Write("<script language='javascript'>alert(' " & AppSettings("USPS_SATURDAY_MESSAGE") & " ')</script>")
            End If
        End If


        doApply()
    End Sub
    Protected Sub cbxInsurance_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxInsurance.CheckedChanged

        If ShippingHandlingProviderGuid.SelectedValue = "USPS" Then
            If Me.cbxInsurance.Checked Then
                Response.Write("<script language='javascript'>alert(' " & AppSettings("USPS_INSURANCE_MESSAGE") & " ')</script>")
            End If
        End If


        doApply()
    End Sub

    Public Sub populateService()

        If SelectedProviderValue = "FEDEX" Then
            Me.lblInsuredValue.Visible = True
            Me.txtInsuredValue.Visible = True
        Else
            Me.lblInsuredValue.Visible = False
            Me.txtInsuredValue.Visible = False
            Me.txtInsuredValue.Text = 0
        End If
        data2.ConnectionString = ExpandITLib.CurrentConnectionString
        sqlString = "SELECT ProviderDescription As Description, ShippingProviderServiceCode FROM USShippingHandlingProvider WHERE ShippingHandlingProviderGuid = " & SafeString(Me.ShippingHandlingProviderGuid.SelectedValue)
        data2.SelectCommand = sqlString
        data2.DataBind()
        ShippingHandlingService.DataBind()

    End Sub

End Class
