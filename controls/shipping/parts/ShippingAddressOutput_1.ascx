<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ShippingAddressOutput_1.ascx.vb"
    Inherits="controls_ShippingAddressOutput_1" %>
<%@ Register TagPrefix="grb" Namespace="GroupRadioButton" Assembly="GroupRadioButton" %>
<%@ Register TagPrefix="exp" TagName="itemtemplate" Src="~/controls/shipping/ItemTemplates/ItemTemplate2.ascx" %>
<%@ Register Namespace="ExpandIT.Buttons" TagPrefix="expBtn" %>
<div class="test123">
    <div class="majorshipdiv">
        <asp:DataList ID="ShippingAddressDisplay" runat="server" RepeatDirection="Horizontal"
            SeparatorStyle-BorderColor="red" RepeatColumns="2">
            <ItemTemplate>
                <div class="RadioDiv">
                    <grb:GroupRadioButton ID="selectradiobutton" runat="server" GroupName="UseAddr" CssClass="Output1GroupBtn"
                        Checked='<%#convertToChecked(Eval("IsDefault")) %>'></grb:GroupRadioButton>
                    <%=Resources.Language.LABEL_USE_ADDRESS%>
                </div>
                <exp:itemtemplate ID="Itemtemplate2" runat="server" />
                <div class="caddress" style="padding-top: 10px; padding-bottom: 20px">
                    <button runat="server" id="ButtonChangeAddress" Visible='<%#IsB2B(Eval("AddressType")) %>'
                        class="ShippingButtons">
                        <%=Resources.Language.ACTION_CHANGE%>
                    </button>
                    <expBtn:ExpButton ID="ButtonDeleteAddress" CommandName="delete" runat="server" CssClass="ShippingButtons"
                        ButtonText="ACTION_DELETE" Visible='<%#IsB2B(Eval("AddressType")) %>' />
                </div>
            </ItemTemplate>
        </asp:DataList>
    </div>    
    <div class="linediv">
    </div>    
</div>
