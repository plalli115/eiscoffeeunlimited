Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Collections.Generic

Partial Class controls_shipping_parts_ShippingAddressDataBoundInput
    Inherits ExpandIT.ShippingPart

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If CBoolEx(AppSettings("SHOW_STATE_ON_SHIPPING_ADDRESS")) = False Then
            findNamedControl(ShippingAddressDisplay, "TableRowState", New Control()).Visible = False
        End If

        If Not Page.IsPostBack Then
            Try
                setCountries()
                setErrorMessage()
            Catch ex As Exception

            End Try
        End If

    End Sub

    ''' <summary>
    ''' Gets: A ref to the control's Datalist    
    ''' Sets: The Datalist's DataSource
    ''' </summary>
    ''' <value>A DataSource object</value>
    ''' <returns>Datalist</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property ShippingOutput() As DataList
        Get
            Return ShippingAddressDisplay
        End Get
    End Property

    Public WriteOnly Property DataListSource() As Object
        Set(ByVal value As Object)
            ShippingAddressDisplay.DataSource = value
        End Set
    End Property

    Protected Sub setCountries()
        Dim CountryNames As New ExpandIT.ExpDictionary()
        Dim CountryGuids As New ExpandIT.ExpDictionary()
        Dim s As String = ""

        Dim dt As Data.DataTable = ShippingAddressDisplay.DataSource
        Dim str As String = String.Empty
        Try
           str = dt.Rows(0)("CountryGuid")
        Catch ex As Exception

        End Try
        GetCountries(CountryGuids, CountryNames)
        Dim b As KeyValuePair(Of Object, Object)
        Dim i As Integer = 0

        '--DJW 8/10/2012 Eliminated Country From Being Displayed
        'For Each b In CountryNames
        '    s = Server.UrlEncode(CStrEx(CountryGuids(b.Value)))
        '    If s <> "" Then

        '        CType(EISClass.FindChildControl(ShippingAddressDisplay, "CountryDropDownList1"), DropDownList).Items.Insert(i, b.Value)
        '        If s = str Then
        '            CType(EISClass.FindChildControl(ShippingAddressDisplay, "CountryDropDownList1"), DropDownList).SelectedIndex = i
        '        End If
        '        i += 1
        '    End If
        'Next

    End Sub

    Public Overrides ReadOnly Property shipAddrData() As ExpDictionary
        Get
            Dim CountryNames As New ExpandIT.ExpDictionary()
            Dim CountryGuids As New ExpandIT.ExpDictionary()
            GetCountries(CountryGuids, CountryNames)
            shipAddrData = New ExpDictionary()
            Try
                shipAddrData.Add("ContactName", CType(EISClass.FindChildControl(ShippingAddressDisplay, "ContactName"), TextBox).Text)
                shipAddrData.Add("CompanyName", CType(EISClass.FindChildControl(ShippingAddressDisplay, "CompanyName"), TextBox).Text)
                shipAddrData.Add("Address1", CType(EISClass.FindChildControl(ShippingAddressDisplay, "Address1"), TextBox).Text)
                shipAddrData.Add("Address2", CType(EISClass.FindChildControl(ShippingAddressDisplay, "Address2"), TextBox).Text)
                shipAddrData.Add("ZipCode", CType(EISClass.FindChildControl(ShippingAddressDisplay, "ZipCode"), TextBox).Text)
                shipAddrData.Add("CityName", CType(EISClass.FindChildControl(ShippingAddressDisplay, "CityName"), TextBox).Text)
                shipAddrData.Add("StateName", CType(EISClass.FindChildControl(ShippingAddressDisplay, "StateName"), TextBox).Text)

                '--DJW 8/10/2012 Eliminated Country From Being Displayed
                'shipAddrData.Add("CountryGuid", CountryGuids(CType(EISClass.FindChildControl(ShippingAddressDisplay, "CountryDropDownList1"), DropDownList).SelectedItem.Text))

                shipAddrData.Add("EmailAddress", CType(EISClass.FindChildControl(ShippingAddressDisplay, "EmailAddress"), TextBox).Text)
                shipAddrData.Add("UserGuid", CType(EISClass.FindChildControl(ShippingAddressDisplay, "UserGuid"), TextBox).Text)
                shipAddrData.Add("ShippingAddressGuid", CType(EISClass.FindChildControl(ShippingAddressDisplay, "ShippingAddressGuid"), TextBox).Text)
                shipAddrData.Add("AddressType", CType(EISClass.FindChildControl(ShippingAddressDisplay, "AddressType"), TextBox).Text)

            Catch ex As Exception
            End Try
        End Get
    End Property

    Protected Sub setErrorMessage()
        Dim errorMessage As String = " * "

        CType(ShippingAddressDisplay.TemplateControl.Controls.Item(0).Controls(0).FindControl("ContactNameValidator"), RequiredFieldValidator).ErrorMessage = errorMessage
        CType(ShippingAddressDisplay.TemplateControl.Controls.Item(0).Controls(0).FindControl("Address1Validator"), RequiredFieldValidator).ErrorMessage = errorMessage
        CType(ShippingAddressDisplay.TemplateControl.Controls.Item(0).Controls(0).FindControl("ZipCodeValidator"), RequiredFieldValidator).ErrorMessage = errorMessage
        CType(ShippingAddressDisplay.TemplateControl.Controls.Item(0).Controls(0).FindControl("CityNameValidator"), RequiredFieldValidator).ErrorMessage = errorMessage
        CType(ShippingAddressDisplay.TemplateControl.Controls.Item(0).Controls(0).FindControl("EmailAddressValidator"), RequiredFieldValidator).ErrorMessage = errorMessage

    End Sub

End Class
