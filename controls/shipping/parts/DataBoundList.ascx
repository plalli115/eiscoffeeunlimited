<%@ Control Language="VB" AutoEventWireup="false" CodeFile="DataBoundList.ascx.vb"
    Inherits="controls_shipping_parts_DataBoundList" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Register TagPrefix="exp" TagName="dlist" Src="~/controls/shipping/ItemTemplates/ListItemTemplate1.ascx" %>
<%@ Register TagPrefix="grb" Namespace="GroupRadioButton" Assembly="GroupRadioButton" %>
<table cellpadding="0" cellspacing="0" style="margin-left:10px;">
    <tr>
        <td>
            <div class="dataBoundList">
                <table>
                    <tr>
                        <%-- --DJW 8/13/2012 Eliminated Contact Name From Being Displayed--%>
                         <td visible="false">                            
                            <asp:TextBox Width="105px" ReadOnly="true" BorderWidth="0" BackColor="transparent"
                                Font-Bold="true" runat="server" ID="ContactName" visible="false" Wrap="false" style="font-size:11px;"
                                Text="<%$Resources: Language,LABEL_USERDATA_CONTACT %>" />
                        </td>                       
                         
                       <td>
                            <asp:TextBox Width="120px" ReadOnly="true" BorderWidth="0" BackColor="transparent"
                                Font-Bold="true" runat="server" ID="TextBox1" Wrap="false" style="font-size:11px;"
                                Text="<%$Resources: Language,LABEL_USERDATA_COMPANY %>" />
                        </td>
                        
                        <td>
                            <asp:TextBox Width="90px" ReadOnly="true" BorderWidth="0" BackColor="transparent"
                                Font-Bold="true" runat="server" ID="TextBox2" Wrap="false" style="font-size:11px;"
                                Text="<%$Resources: Language,LABEL_USERDATA_ADDRESS_1 %>" />
                        </td>
                        <td>
                            <asp:TextBox Width="90px" ReadOnly="true" BorderWidth="0" BackColor="transparent"
                                Font-Bold="true" runat="server" ID="TextBox3" Wrap="false" style="font-size:11px;"
                                Text="<%$Resources: Language,LABEL_USERDATA_ADDRESS_2 %>" />
                        </td>
                        <td>
                            <asp:TextBox Width="81px" ReadOnly="true" BorderWidth="0" BackColor="transparent"
                                Font-Bold="true" runat="server" ID="TextBox4" Wrap="false" style="font-size:11px;"
                                Text="<%$Resources: Language,LABEL_USERDATA_CITY %>" />
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div id="divShipping1" class="fixedHeader">
                <asp:DataList ID="ShippingAddressDisplay" BorderWidth="1" CellPadding="2" CellSpacing="1" 
                Font-Size="XX-Small" runat="server" RepeatColumns="1" AlternatingItemStyle-BackColor="#BEDEDF" Width="100%">
                    <ItemTemplate>
                        <asp:Panel runat="server" ID="panel1">
                            <div style="display: none">
                                <grb:GroupRadioButton ID="selectradiobutton" runat="server" GroupName="UseAddr" Checked='<%#convertToChecked(Eval("IsDefault")) %>'>
                                </grb:GroupRadioButton>
                            </div>
                            <exp:dlist ID="ListItemtemplate1" runat="server" />
                        </asp:Panel>
                    </ItemTemplate>
                </asp:DataList>
                <asp:Panel runat="server" ID="myPanel" />
            </div>
            <div id="line" class="line">
            </div>
        </td>
    </tr>
</table>
