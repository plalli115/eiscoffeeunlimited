<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="DesignModel_3_List.ascx.vb"
    Inherits="DesignModel_3_List" %>
<%@ Register TagPrefix="expshp" TagName="shippingoutput" Src="~/controls/shipping/parts/DataBoundList.ascx" %>
<%@ Register TagPrefix="expshp" TagName="shippinginput" Src="~/controls/shipping/parts/ShippingAddress.ascx" %>
<%@ Register TagPrefix="expshp" TagName="shippinglabeloutput" Src="~/controls/shipping/ItemTemplates/LabelOutputTemplate.ascx" %>
<%@ Register TagPrefix="grb" Namespace="GroupRadioButton" Assembly="GroupRadioButton" %>
<div>    
    <expshp:shippingoutput runat="server" ID="output1" />
    <div class="buttons" id="buttons" style="padding-top: 5px; padding-bottom: 10px">
        <table cellpadding="0" cellspacing="0" style="width: 380px;">
            <tr>
                <td>
                    <button runat="server" id="ButtonChangeAddress" disabled="disabled" class="AddButton">
                        <%=Resources.Language.ACTION_CHANGE%>
                    </button>
                </td>
                <td>
                    <asp:Button ID="ButtonDeleteAddress" CommandName="delete" runat="server" CssClass="AddButton"
                        Text="<%$Resources: Language, ACTION_DELETE %>" Enabled="false" OnClick="ButtonDeleteAddress_Click" />
                </td>
                <td>
                    <button runat="server" id="ButtonNewAddress" class="AddButton">
                        <%=Resources.Language.ACTION_NEW_ADDRESS%>
                    </button>
                </td>
            </tr>
        </table>
    </div>
    <div class="ShippingLabelOutputDisplayDiv" id="shippingdisplaydiv">
        <asp:Panel runat="server" ID="shippinglabeloutputdiv" Style="display: block; width: 230px;">
            <expshp:shippinglabeloutput ID="output2" runat="server" />
        </asp:Panel>
        <div runat="server" class="ShippingLabelTableOutputDisplayDiv" id="shippinginputdiv"
            style="display: none">
            <expshp:shippinginput ID="input1" runat="server" />
        </div>
    </div>
</div>

<script type="text/javascript">        
        // Get selected radiobutton
        function getSelectedRadioButton(){
            var isChecked = false;
            var index = 0;
            var rbs = getRbs();
            for(var i = 0; i < rbs.length; i++){
                if(rbs[i].checked){
                    isChecked = true;
                    return i;                    
                }
            }
            if(!isChecked){
                if(rbs.length > 1)
                    rbs[0].checked = true;
                else
                    rbs.checked = true;
            }               
            return index;
        }
        // Toggle radiobuttons programmatically
        function toggleRadioButtons(index){
            var i = 0;
            var rbs = getRbs();
            for(i = 0; i < rbs.length; i++){
                if(rbs[i].checked){
                    rbs[i].checked = false;
                }
            }
            if (i > 0)
                rbs[index].checked = true;
            else
                rbs.checked = true;
            return true;
        }
        // Get all radiobuttons in group "UseAddr"
        function getRbs(){
            var rbs;    
            rbs = document.forms[0].UseAddr;
            return rbs;
        }  
        // Enable Buttons
        function buttonEnableDisable(buttonId1, buttonId2, isB2C){
            if(isB2C){
                document.getElementById(buttonId1).disabled = false;
                document.getElementById(buttonId2).disabled = false;
                document.getElementById(buttonId1).visible = true;
                document.getElementById(buttonId2).visible = true;      
            }  
            if(!isB2C && document.getElementById(buttonId1).disabled == false){
                document.getElementById(buttonId1).disabled = true;
                document.getElementById(buttonId2).disabled = true;
                document.getElementById(buttonId1).visible = false;
                document.getElementById(buttonId2).visible = false;     
            }    
        }
        // Toggle between edit/display mode
        function toggleEditMode(editId, showId, editOn){
            if(editOn){
                document.getElementById(showId).style.display = "none";
                document.getElementById(editId).style.display = "block";
            }
            else{                
                document.getElementById(editId).style.display = "none";
                document.getElementById(showId).style.display = "block";
            }            
        }     
        // Clear input fields
        function clearScript(){
            var currentIndex = getSelectedRadioButton();            
            var i;
            for (i = 0; i < dArray[currentIndex].length; i++){
                document.getElementById(dArray[currentIndex][i]).value = "";
            }
        }        
</script>

