﻿Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Collections.Generic
Imports System.Web.UI
Imports ExpandIT.Buttons

Partial Class B2C1OnlyOne
    Inherits ExpandIT.ShippingDesignModel

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim str As String = String.Empty
        Dim CreateString As String = String.Empty
        Dim refInt As Integer = 0

        ' Make a DataList reference
        Dim dataLst As DataList = DirectCast(Me.output1.ShippingOutput, DataList)

        ' If not show state
        If Not CBoolEx(AppSettings("SHOW_STATE_ON_SHIPPING_ADDRESS")) Then
            Dim refDict As Dictionary(Of String, Control) = output1.ControlDict(dataLst, "Panel")
            For Each item As KeyValuePair(Of String, Control) In refDict
                If item.Value.ID.Contains("StatePanel") Then
                    item.Value.Visible = False
                End If
            Next
            refDict.Clear()
            refDict = output1.ControlDict(dataLst, "TextBox")
            For Each item As KeyValuePair(Of String, Control) In refDict
                If item.Value.ID.Contains("StateName") Then
                    item.Value.Visible = False
                End If
            Next
            findNamedControl(input1, "StateName", New Control()).Visible = False
        End If
        '*****shipping*****-start 
        If AppSettings("EXPANDIT_US_USE_SHIPPING") Then
            If globals.OrderDict Is Nothing Then eis.LoadOrderDictionary(globals.User)

            CType(findNamedControl(input1, "ZipCode", New Control()), TextBox).Text = globals.OrderDict("ShipToZipCode")
        End If
        '*****shipping*****-end

        ' Prepare the dictionaries to hold the values we'll get 
        ' from searching the DataList's Items collection
        Dim textBoxDictSrc As New Dictionary(Of String, Control)
        Dim grpdict As New Dictionary(Of String, Control)
        Dim hiddendict As New Dictionary(Of String, Control)
        Dim grpkv As New KeyValuePair(Of String, Control)

        Dim drpDwnListSrc As New Dictionary(Of String, Control)

        Dim textBoxDictDest As Dictionary(Of String, Control) = Me.input1.ControlDict(input1, "TextBox", True)
        Dim drpDwnListDest As Dictionary(Of String, Control) = Me.input1.ControlDict(input1, "DropDownList", True)


        ' Loop thru all the Datalist Items in output1
        For inx As Integer = 0 To dataLst.Items.Count - 1
            Dim hit As Integer = 0
            ' Get all the textboxes from each DataList Item
            textBoxDictSrc = Me.output1.ControlDict(dataLst.Items(inx), "TextBox", True)
            ' Get all the HiddenFields from each DataList Item
            hiddendict = Me.output1.ControlDict(dataLst.Items(inx), "HiddenField")
            ' Get all the DropDownLists from each DataList Item
            drpDwnListSrc = Me.output1.ControlDict(dataLst.Items(inx), "DropDownList")
            ' Get all GroupRadioButtons from each DataList Item
            grpdict = Me.output1.ControlDict(dataLst.Items(inx), "GroupRadioButton")
            For Each grpkv In grpdict
                Dim grpButton As GroupRadioButton.GroupRadioButton = _
                DirectCast(grpdict.Item(grpkv.Key), GroupRadioButton.GroupRadioButton)
                ' Assign a JavaScript function to the button
                grpButton.Attributes.Item("onclick") = _
                    "hideDivCopy('" & hiddenfields.ClientID & "', " & refInt & ", true, false);"
            Next
            ' find all the HtmlButtons from each DataList Item
            grpdict = Me.output1.ControlDict(dataLst.Items(inx), "HtmlButton")
            For Each grpkv In grpdict
                Dim cButton As HtmlButton = DirectCast(grpdict.Item(grpkv.Key), HtmlButton)
                If cButton.ID.Contains("ButtonChangeAddress") Then
                    'Assign a JavaScript function to the button
                    cButton.Attributes.Item("onclick") = _
                        "hideDivCopy('" & hiddenfields.ClientID & "', " & refInt & ", false, true);return false;"
                End If
            Next
            ' find all the "ExpButtons" from each DataList Item
            grpdict = Me.output1.ControlDict(dataLst.Items(inx), "ExpButton")
            For Each grpkv In grpdict
                Dim cButton As ExpButton = DirectCast(grpdict.Item(grpkv.Key), ExpButton)
                If cButton.ID.Contains("ButtonDeleteAddress") Then
                    ' Assign a JavaScript function to the button
                    cButton.OnClientClick = "copy(" & refInt & ");"
                End If
            Next
            controlIdCopy(str, CreateString, dataLst, textBoxDictSrc, drpDwnListDest, textBoxDictDest, drpDwnListSrc, _
                hiddendict, inx, hit)
            refInt += 1
        Next

        ' Assign a JavaScript to  NewButton Control on this page
        Me.ButtonNewAddress.Attributes.Item("onclick") = "revealDiv('" & hiddenfields.ClientID & "');return false;"

        ' Register empty JS Arrays
        Page.ClientScript.RegisterArrayDeclaration("sArray", "")
        Page.ClientScript.RegisterArrayDeclaration("dArray", "")

        Dim t As Type = Me.GetType()

        ' Register Script Blocks
        Page.ClientScript.RegisterStartupScript(t, "SetDestArray", String.Format(fString, "function SetArray(){", CreateString, str, "}"))
        Page.ClientScript.RegisterStartupScript(t, "copy", makeCopyScript())

        ' Register Script Blocks
        Page.ClientScript.RegisterStartupScript(t, "onLoadAction", onLoadActionScript())
        Page.ClientScript.RegisterStartupScript(t, "s", startUpScript())
        Page.ClientScript.RegisterStartupScript(t, "hideDivCopy", hideScript())
        ' Page.ClientScript.RegisterStartupScript(t, "clear", makeClearScript())

    End Sub

    Protected Function hideScript() As String
        Dim sb As New StringBuilder()
        sb.AppendLine("<script type=""text/javascript"">")
        sb.AppendLine("    function hideDivCopy(id, val, unhide, show){")
        sb.AppendLine("        var obj = document.getElementById(id);")
        sb.AppendLine("        if(obj.style.display == 'block' && unhide)")
        sb.AppendLine("            obj.style.display = 'none';")
        sb.AppendLine("        else if(show){")
        sb.AppendLine("            obj.style.display = 'block';")
        sb.AppendLine("            toggleRadioButtons(getRbs().length -1);")
        sb.AppendLine("        }")
        sb.AppendLine("        copy(val);")
        sb.AppendLine("        return false;")
        sb.AppendLine("    }")
        sb.AppendLine("</script>")
        Return sb.ToString()
    End Function

    Protected Function startUpScript() As String
        Dim sb As New StringBuilder()
        sb.AppendLine("<script type=""text/javascript"">")
        sb.AppendLine("    document.body.onLoad=onLoadAction();")
        sb.AppendLine("</script>")
        Return sb.ToString()
    End Function

    'Protected Function makeClearScript() As String
    '    Dim sql As String = "SELECT CountryName FROM CountryTable  WHERE CountryGuid = '" + AppSettings("DEFAULT_COUNTRYGUID") + "'"
    '    Dim country = getSingleValueDB(sql)
    '    Dim sb As New StringBuilder()
    '    If country <> "" Then
    '        sb.AppendLine("<script type=""text/javascript"">")
    '        sb.AppendLine("    function clear(){")
    '        sb.AppendLine("        var i;")
    '        sb.AppendLine("        for (i = 0; i < dArray[0].length; i++){")
    '        sb.AppendLine("            if (dArray[0][i].indexOf(""CountryDropDownList1"")>=0){")
    '        sb.AppendLine("            document.getElementById(dArray[0][i]).value = """ + country + """;")
    '        sb.AppendLine("            }else{")
    '        sb.AppendLine("            document.getElementById(dArray[0][i]).value = """";")
    '        sb.AppendLine("            }")
    '        sb.AppendLine("        }")
    '        sb.AppendLine("    }")
    '        sb.AppendLine("</script>")
    '        Return sb.ToString()
    '    Else
    '    sb.AppendLine("<script type=""text/javascript"">")
    '    sb.AppendLine("    function clear(){")
    '    sb.AppendLine("        var i;")
    '    sb.AppendLine("        for (i = 0; i < dArray[0].length; i++){")
    '    sb.AppendLine("            document.getElementById(dArray[0][i]).value = """";")
    '    sb.AppendLine("        }")
    '    sb.AppendLine("    }")
    '    sb.AppendLine("</script>")
    '    Return sb.ToString()

    '    End If
    ' End Function

    ''' <summary>
    ''' Get reference to nested controls DataList     
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overrides ReadOnly Property GetDataList() As DataList
        Get
            Dim lDataList As DataList
            lDataList = Me.output1.ShippingOutput
            Return lDataList
        End Get
    End Property
    ''' <summary>
    ''' Set nested controls DataSource
    ''' </summary>
    ''' <value></value>
    ''' <remarks></remarks>
    Public WriteOnly Property SetDataSource() As Object
        Set(ByVal value As Object)
            output1.DataListSource = value
        End Set
    End Property


    Public Overrides ReadOnly Property shipAddrData() As ExpDictionary
        Get
            Return Me.input1.shipAddrData()
        End Get
    End Property

End Class
