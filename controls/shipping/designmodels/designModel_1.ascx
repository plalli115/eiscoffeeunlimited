<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="designModel_1.ascx.vb"
    Inherits="DesignModel_1" %>
<%@ Register TagPrefix="expshp" TagName="shippinginput" Src="~/controls/shipping/parts/ShippingAddressDataBoundInput.ascx" %>
<div id="shippingdivmodel1" style="padding-left: 5px; padding-top: 20px; padding-bottom: 5px">    
    <div class="ShippingInputDiv" id="shippinginputdiv">        
        <expshp:shippinginput ID="input1" runat="server" />
    </div>
</div>
