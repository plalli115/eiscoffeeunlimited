Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Collections.Generic
Imports System.Web.UI

Partial Class controls_shipping_designmodels_ShippingAnonymous
    Inherits ExpandIT.ShippingDesignModel

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        ' If not show state
        If Not CBoolEx(AppSettings("SHOW_STATE_ON_SHIPPING_ADDRESS")) Then
            findNamedControl(ucShipping, "StateName", New Control()).Visible = False
            findNamedControl(ucPayment, "StateName", New Control()).Visible = False
        End If
        If AppSettings("EXPANDIT_US_USE_SHIPPING") Then
            '*****shipping*****-start
            If globals.OrderDict Is Nothing Then eis.LoadOrderDictionary(globals.User)

            Dim c As New Control()
            CType(findNamedControl(ucShipping, "ZipCode", c), TextBox).Text = globals.OrderDict("ShipToZipCode")
        End If
        '*****shipping*****-end
        Dim t As Type = Me.GetType()
        Dim js As String = Me.jsCopyString(Me.ucShipping, Me.ucPayment)
        Page.ClientScript.RegisterStartupScript(t, "copy", js)
        Page.ClientScript.RegisterStartupScript(t, "clear", clearScript(ucPayment))

    End Sub

    Protected Function jsCopyString(ByVal ucS As ShippingUserControl, ByVal ucP As ShippingUserControl) As String

        Dim c As New HtmlGenericControl()
        Dim div As String
        Dim sDict As Dictionary(Of String, Control) = ucS.ControlDict(ucS, "TextBox", True)
        Dim pDict As Dictionary(Of String, Control) = ucP.ControlDict(ucP, "TextBox", True)
        Dim sEn, pEn As Dictionary(Of String, Control).KeyCollection.Enumerator
        div = Me.findNamedControl(Me, "hiddenfields", c).ClientID
        sEn = sDict.Keys.GetEnumerator()
        pEn = pDict.Keys.GetEnumerator()

        Dim sb As New StringBuilder()
        sb.AppendLine("<script type=""text/javascript"">")
        sb.AppendLine("    function copy(){")
        sb.AppendLine("if(document.getElementById(""" & div & """).style.display == 'none'){")
        For i As Integer = 0 To sDict.Keys.Count - 1
            sEn.MoveNext()
            pEn.MoveNext()
            sb.Append("            document.getElementById(""" & pDict(pEn.Current()).ClientID & """).value = ")
            sb.AppendLine("            document.getElementById(""" & sDict(sEn.Current()).ClientID & """).value;")
        Next
        '  sb.Append("            document.getElementById(""" & CType(ucP, controls_shipping_parts_controls_ShippingAddress).DropdownID.ClientID & """).value = ")
        ' sb.AppendLine("            document.getElementById(""" & CType(ucS, controls_shipping_parts_controls_ShippingAddress).DropdownID.ClientID & """).value;")
        sb.AppendLine("        }")
        sb.AppendLine("    }")
        sb.AppendLine("</script>")
        Return sb.ToString()
    End Function

    Protected Function clearScript(ByVal ucP As ShippingUserControl) As String
        Dim pDict As Dictionary(Of String, Control) = ucP.ControlDict(ucP, "TextBox", True)
        Dim pEn As Dictionary(Of String, Control).KeyCollection.Enumerator
        pEn = pDict.Keys.GetEnumerator()
        Dim sb As New StringBuilder()
        sb.AppendLine("<script type=""text/javascript"">")
        sb.AppendLine("    function clear(){")
        For i As Integer = 0 To pDict.Keys.Count - 1
            pEn.MoveNext()
            sb.AppendLine("     document.getElementById(""" & pDict(pEn.Current()).ClientID & """).value = """";")
        Next
        '    sb.AppendLine("     document.getElementById(""" & CType(ucP, controls_shipping_parts_controls_ShippingAddress).DropdownID.ClientID & """).value = """";")
        sb.AppendLine(" }")
        sb.AppendLine("</script>")
        Return sb.ToString()
    End Function

    Public Overrides ReadOnly Property shipAddrData() As ExpDictionary
        Get
            Return ucShipping.shipAddrData()
        End Get
    End Property

    Public ReadOnly Property shipPaymentData() As ExpDictionary
        Get
            Return ucPayment.shipAddrData()
        End Get
    End Property

End Class
