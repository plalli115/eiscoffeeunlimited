<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ShippingAnonymous.ascx.vb"
    Inherits="controls_shipping_designmodels_ShippingAnonymous" %>
<%@ Register TagPrefix="xp" TagName="uc_shippingAddress" Src="~/controls/shipping/parts/ShippingAddress.ascx" %>
<%@ Register TagPrefix="xp" TagName="uc_paymentAddress" Src="~/controls/shipping/parts/ShippingAddress.ascx" %>
<xp:uc_shippingAddress ID="ucShipping" runat="server"></xp:uc_shippingAddress>
<div id="differentAddressDiv" style="display: <% = IsMultiplePaymentShippingOption %>">
    <asp:Label runat="server" ID="label1" Text="<%$Resources: Language, LABEL_USE_DIFFERENT_PAYMENT_ADDRESS %>"
        Visible="true"></asp:Label>
    <input type="checkbox" onclick="hideDiv('<% =hiddenfields.ClientID %>');" id="chk1"
        name="chk1" /></div>
<br />
<br />
<div id="hiddenfields" runat="server" style="display: none">
    <h1>
        <asp:Label runat="server" ID="labelPaymentAddress" Text="<%$Resources: Language, LABEL_PAYMENT_ADDRESS %>"
            Visible="true"></asp:Label>
    </h1>
    <xp:uc_paymentAddress ID="ucPayment" runat="server" Visible="true"></xp:uc_paymentAddress>
</div>

<script type="text/javascript">
        function hideDiv(id){
            var obj = document.getElementById(id);
            if(obj.style.display == 'block')
                obj.style.display = 'none';
            else{
                clear();
                obj.style.display = 'block';   
            }         
            return false;
        }
</script>

