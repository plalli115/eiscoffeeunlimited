Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Collections.Generic
Imports System.Web.UI

Partial Class controls_shipping_designmodels_DesignModelB2B3List
    Inherits ExpandIT.ShippingDesignModel

Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim str As String = String.Empty
        Dim CreateString As String = String.Empty
        Dim str2 As String = String.Empty
        Dim CreateString2 As String = String.Empty
        Dim refInt As Integer = 0
        Dim refInt2 As Integer = 0

        ' Make a DataList reference
        Dim dataLst As DataList = DirectCast(Me.output1.ShippingOutput, DataList)

        ' If not show state
        If Not CBoolEx(AppSettings("SHOW_STATE_ON_SHIPPING_ADDRESS")) Then
            Dim refDict As Dictionary(Of String, Control) = output1.ControlDict(dataLst, "Panel")
            For Each item As KeyValuePair(Of String, Control) In refDict
                If item.Value.ID.Contains("StatePanel") Then
                    item.Value.Visible = False
                End If
            Next
            refDict.Clear()
            refDict = output1.ControlDict(dataLst, "TextBox")
            For Each item As KeyValuePair(Of String, Control) In refDict
                If item.Value.ID.Contains("StateName") Then
                    item.Value.Visible = False
                End If
            Next
            findNamedControl(input1, "StateName", New Control()).Visible = False
            findNamedControl(output2, "StateNameLabelForOutput", New Control()).Visible = False
            findNamedControl(output2, "TableRowState", New Control()).Visible = False
        End If

        ' Prepare the dictionaries to hold the values we'll get 
        ' from searching the DataList's Items collection
        Dim textBoxDictSrc As New Dictionary(Of String, Control)
        Dim grpdict As New Dictionary(Of String, Control)
        Dim hiddendict As New Dictionary(Of String, Control)
        Dim grpkv As New KeyValuePair(Of String, Control)
        Dim drpDwnListSrc As New Dictionary(Of String, Control)
        ' Get data from input fields/output label fields
        Dim textBoxDictDest As Dictionary(Of String, Control) = Me.input1.ControlDict(input1, "TextBox", True)
        Dim drpDwnListDest As Dictionary(Of String, Control) = Me.input1.ControlDict(input1, "DropDownList", True)
        Dim lblDictDest As Dictionary(Of String, Control) = Me.output2.ControlDict(output2, "Label", True)

        ' Loop thru all the Datalist Items in output1
        Dim inx2 As Integer = 0
        For inx As Integer = 0 To dataLst.Items.Count - 1
            Dim hit As Integer = 0
            Dim hit2 As Integer = 0
            Dim b2bStr As String = String.Empty
            Dim typ As Type = findNamedControl(dataLst.Items(inx), "AddressType", New Control()).GetType()
            If typ.Name.Equals("HiddenField") Then
                b2bStr = CType(findNamedControl(dataLst.Items(inx), "AddressType", New Control()), HiddenField).Value
            End If
            If typ.Name.Equals("Label") Then
                b2bStr = CType(findNamedControl(dataLst.Items(inx), "AddressType", New Control()), Label).Text
            End If
            If typ.Name.Equals("TextBox") Then
                b2bStr = CType(findNamedControl(dataLst.Items(inx), "AddressType", New Control()), TextBox).Text
            End If
            Dim te As Dictionary(Of String, Control) = Me.output1.ControlDict(dataLst.Items(inx), "Panel")
            For Each kk As KeyValuePair(Of String, Control) In te
                CType(kk.Value, Panel).Attributes.Item("onclick") = "copy(" & refInt & ");copyB(" & refInt2 & ");highLight('" & _
                    CType(kk.Value, Panel).ClientID & "', '#8EC6E4');buttonEnableDisable('" & Me.ButtonChangeAddress.ClientID & _
                    "'," & IsB2B(b2bStr) & ");toggleEditMode('" & shippinginputdiv.ClientID & "','" & _
                    shippinglabeloutputdiv.ClientID & "', false);toggleRadioButtons(" & refInt & ");"
                CType(kk.Value, Panel).Attributes.Item("onmouseover") = "this.style.cursor='hand'"
            Next
            ' Get all the textboxes from each DataList Item
            textBoxDictSrc = Me.output1.ControlDict(dataLst.Items(inx), "TextBox", True)
            ' Get all the HiddenFields from each DataList Item
            hiddendict = Me.output1.ControlDict(dataLst.Items(inx), "HiddenField")
            ' Get all the DropDownLists from each DataList Item
            drpDwnListSrc = Me.output1.ControlDict(dataLst.Items(inx), "DropDownList")
            ' Get all GroupRadioButtons from each DataList Item
            grpdict = Me.output1.ControlDict(dataLst.Items(inx), "GroupRadioButton")
            For Each grpkv In grpdict
                Dim grpButton As GroupRadioButton.GroupRadioButton = _
                DirectCast(grpdict.Item(grpkv.Key), GroupRadioButton.GroupRadioButton)
                ' Assign a JavaScript function to the button
                grpButton.Attributes.Item("onclick") = _
                    "hideErrorMessage();copy(" & refInt & ");"
            Next

            ' get image
            If (IsB2B(b2bStr)) = "false" Then
                CType(findNamedControl(dataLst.Items(inx), "padlock", New Control()), Image).Visible = True
            End If

            controlIdCopy(str, CreateString, dataLst, textBoxDictSrc, drpDwnListDest, textBoxDictDest, drpDwnListSrc, _
                hiddendict, inx, hit, lblDictDest, str2, CreateString2, inx2, hit2)
            refInt += 1
            refInt2 += 1
            inx2 += 1
        Next

        ' assign javascript to buttons
        Me.ButtonChangeAddress.Attributes.Item("onclick") = "toggleEditMode('" & shippinginputdiv.ClientID & "','" & shippinglabeloutputdiv.ClientID & "', true);return false;"

        ' Register empty JS Arrays
        Page.ClientScript.RegisterArrayDeclaration("sArray", "")
        Page.ClientScript.RegisterArrayDeclaration("dArray", "")
        Page.ClientScript.RegisterArrayDeclaration("sArrayB", "")
        Page.ClientScript.RegisterArrayDeclaration("dArrayB", "")

        Dim t As Type = Me.GetType()

        ' Register Script Blocks
        Page.ClientScript.RegisterStartupScript(t, "SetDestArray", String.Format(fString, "function SetArray(){", CreateString, str, "}"))
        Page.ClientScript.RegisterStartupScript(t, "copy", makeCopyScript())

        Page.ClientScript.RegisterStartupScript(t, "SetDestArrayB", String.Format(fString, "function SetArrayB(){", CreateString2, str2, "}"))
        Page.ClientScript.RegisterStartupScript(t, "copyB", makeCopyScript2())

        ' Register Script Blocks
        Page.ClientScript.RegisterStartupScript(t, "onLoadAction", onLoadActionScript(True))
        Page.ClientScript.RegisterStartupScript(t, "s", startUpScript())
        Page.ClientScript.RegisterStartupScript(t, "highLight", setHighLightScript())
    End Sub

    Protected Function getLabelID() As String
        Return findNamedControl(Me.output1, "shippingOutputLabel", New Control()).ClientID
    End Function

    Protected Function startUpScript() As String
        Dim sb As New StringBuilder()
        sb.AppendLine("<script type=""text/javascript"">")
        sb.AppendLine("    document.body.onLoad=onLoadAction();")
        sb.AppendLine("</script>")
        Return sb.ToString()
    End Function

    Public Overrides ReadOnly Property GetDataList() As DataList
        Get
            Dim lDataList As DataList
            lDataList = Me.output1.ShippingOutput
            Return lDataList
        End Get
    End Property
    ''' <summary>
    ''' Set nested controls DataSource
    ''' </summary>
    ''' <value></value>
    ''' <remarks></remarks>
    Public WriteOnly Property SetDataSource() As Object
        Set(ByVal value As Object)
            output1.DataListSource = value
        End Set
    End Property

    ''' <summary>
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overrides ReadOnly Property shipAddrData() As ExpDictionary
        Get
            Return input1.shipAddrData()
        End Get
    End Property

    Protected Function IsB2B(ByVal key As String) As String
        If Left(key, 3) = "B2C" Then
             Return "true"
        Else
            Return "false"
        End If
    End Function
End Class