<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="DesignModel_2.ascx.vb"
    Inherits="DesignModel_2" %>
<%@ Register TagPrefix="expshp" TagName="shippingoutput" Src="~/controls/shipping/parts/ShippingAddressOutput_1.ascx" %>
<%@ Register TagPrefix="expshp" TagName="shippinginput" Src="~/controls/shipping/parts/ShippingAddress.ascx" %>
<%@ Register TagPrefix="grb" Namespace="GroupRadioButton" Assembly="GroupRadioButton" %>
<table>
    <tr>
        <td>
            <div id="shippingdivmodel1">
                <div id="shippingoutputdiv">
                    <expshp:shippingoutput ID="output1" runat="server" />
                </div>
                <div class="newaddress">
                    <div class="naddress">
                        <button runat="server" id="ButtonNewAddress" class="AddButton">
                            <%=Resources.Language.ACTION_NEW_ADDRESS%>
                        </button>
                    </div>
                    <div id="hiddenfields" runat="server" style="display: none;">
                        <div class="ShippingInputDiv" id="shippinginputdiv">
                            <p>
                                <% =Resources.Language.LABEL_STARMARKED_INPUT_IS_REQUIRED%></p>
                            <grb:GroupRadioButton ID="selectradiobutton" runat="server" GroupName="UseAddr" CssClass="ModelB2C1GroupBtn">
                            </grb:GroupRadioButton>
                            <%=Resources.Language.LABEL_USE_ADDRESS%>
                            <expshp:shippinginput ID="input1" runat="server" />
                            <div class="linedivSpacer">                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </td>
    </tr>
</table>

<script type="text/javascript">
        // Get selected radiobutton
        function getSelectedRadioButton(){
            var isChecked = false;
            var index = 0;
            var rbs = getRbs();
            for(var i = 0; i < rbs.length; i++){
                if(rbs[i].checked){
                    isChecked = true;
                    return i;                    
                }
            }
            if(!isChecked){
                rbs[0].checked = true;
            }
            return index;
        }
        // Toggle radiobuttons programmatically
        function toggleRadioButtons(index){
            var i = 0;
            var rbs = getRbs();
            for(i = 0; i < rbs.length; i++){
                if(rbs[i].checked){
                    rbs[i].checked = false;
                }
            }
            if (i > 0)
                rbs[index].checked = true;
            else
                rbs.checked = true;
            return true;
        }
        // Get all radiobuttons in group "UseAddr"
        function getRbs(){
            var rbs;    
            rbs = document.forms[0].UseAddr;
            return rbs;
        }
        // Clear and show hidden input fields    
        function revealDiv(id){
            clear();
            var obj = document.getElementById(id);            
            obj.style.display = 'block';
            toggleRadioButtons(getRbs().length -1);                       
            return false;
        }          
           
</script>

