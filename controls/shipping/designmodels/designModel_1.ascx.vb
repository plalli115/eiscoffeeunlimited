Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports System.Collections.Generic
Imports System.Web.UI

Partial Class DesignModel_1
    Inherits ExpandIT.ShippingDesignModel

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    ''' <summary>
    ''' Get reference to nested controls DataList     
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overrides ReadOnly Property GetDataList() As DataList
        Get
            Dim lDataList As DataList
            lDataList = Me.input1.ShippingOutput
            Return lDataList
        End Get
    End Property
    ''' <summary>
    ''' Set nested controls DataSource
    ''' </summary>
    ''' <value></value>
    ''' <remarks></remarks>
    Public WriteOnly Property SetDataSource() As Object
        Set(ByVal value As Object)
            input1.DataListSource = value
        End Set
    End Property

    Public Overrides ReadOnly Property shipAddrData() As ExpDictionary
        Get
            Return Me.input1.shipAddrData()
        End Get
    End Property

End Class
