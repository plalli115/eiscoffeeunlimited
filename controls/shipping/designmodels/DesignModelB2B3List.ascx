<%@ Control Language="VB" AutoEventWireup="false" CodeFile="DesignModelB2B3List.ascx.vb"
    Inherits="controls_shipping_designmodels_DesignModelB2B3List" %>
<%@ Register TagPrefix="expshp" TagName="shippingoutput" Src="~/controls/shipping/parts/DataBoundList.ascx" %>
<%@ Register TagPrefix="expshp" TagName="shippinginput" Src="~/controls/shipping/parts/ShippingAddress.ascx" %>
<%@ Register TagPrefix="expshp" TagName="shippinglabeloutput" Src="~/controls/shipping/ItemTemplates/LabelOutputTemplate.ascx" %>
<%@ Register TagPrefix="grb" Namespace="GroupRadioButton" Assembly="GroupRadioButton" %>
<div>
    <expshp:shippingoutput runat="server" ID="output1" />
    <div class="ShippingButtons" id="buttons">
        <button runat="server" id="ButtonChangeAddress" disabled="disabled" class="AddButton">
            <%=Resources.Language.ACTION_CHANGE%>
        </button>
    </div>
    <div id="shippingdisplaydiv">
        <asp:Panel runat="server" ID="shippinglabeloutputdiv" Style="display: block">
            <expshp:shippinglabeloutput ID="output2" runat="server" />
        </asp:Panel>
        <div runat="server" class="ShippingInputDiv" id="shippinginputdiv" style="display: none">
            <expshp:shippinginput ID="input1" runat="server" />
        </div>
    </div>
</div>

<script type="text/javascript">        
        // Get selected radiobutton
        function getSelectedRadioButton(){
            var isChecked = false;
            var index = 0;
            var rbs = getRbs();
            for(var i = 0; i < rbs.length; i++){
                if(rbs[i].checked){
                    isChecked = true;
                    return i;                    
                }
            }
            if(!isChecked){
                if(rbs.length > 1)
                    rbs[0].checked = true;
                else
                    rbs.checked = true;
            }               
            return index;
        }
        // Toggle radiobuttons programmatically
        function toggleRadioButtons(index){
            var i = 0;
            var rbs = getRbs();
            for(i = 0; i < rbs.length; i++){
                if(rbs[i].checked){
                    rbs[i].checked = false;
                }
            }
            if (i > 0)
                rbs[index].checked = true;
            else
                rbs.checked = true;
            return true;
        }
        // Get all radiobuttons in group "UseAddr"
        function getRbs(){
            var rbs;    
            rbs = document.forms[0].UseAddr;
            return rbs;
        }  
        // Enable Buttons
        function buttonEnableDisable(buttonId1, isB2C){
            if(isB2C){
                document.getElementById(buttonId1).disabled = false;                   
            }  
            if(!isB2C && document.getElementById(buttonId1).disabled == false){
                document.getElementById(buttonId1).disabled = true;                    
            }    
        }
        // Toggle between edit/display mode
        function toggleEditMode(editId, showId, editOn){
            if(editOn){
                document.getElementById(showId).style.display = "none";
                document.getElementById(editId).style.display = "block";
            }
            else{                
                document.getElementById(editId).style.display = "none";
                document.getElementById(showId).style.display = "block";
            }
        }     
        // Clear input fields
        function clearScript(){
            var currentIndex = getSelectedRadioButton();            
            var i;
            for (i = 0; i < dArray[currentIndex].length; i++){
                document.getElementById(dArray[currentIndex][i]).value = "";
            }
        }    
        function test(){
            document.getElementById("").parentNode.style.backGroundColor
        }    
</script>

