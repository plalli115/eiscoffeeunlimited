<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="DesignModelB2B3.ascx.vb" Inherits="controls_shipping_designmodels_DesignModelB2B3" %>
<%@ Register TagPrefix="expshp" TagName="shippingoutput" Src="~/controls/shipping/parts/ShippingAddressDataBoundInputOutput.ascx" %>
<%@ Register TagPrefix="expshp" TagName="shippinginput" Src="~/controls/shipping/parts/ShippingAddress.ascx" %>
<%@ Register TagPrefix="grb" Namespace="GroupRadioButton" Assembly="GroupRadioButton" %>
<table>
    <tr>
        <td>
            <div id="shippingdivmodel1">
                <div id="shippingoutputdiv">
                    <expshp:shippingoutput ID="output1" runat="server" />
                </div>
                <%--<div id="hiddenfields" runat="server" style="display: none">--%>
                <div id="hiddenfields" runat="server" style="display: none">
                    <div class="ShippingInputDiv" id="shippinginputdiv">                        
                        <expshp:shippinginput ID="input1" runat="server" />                        
                    </div>
                </div>                
            </div>
        </td>
    </tr>
</table>

<script type="text/javascript">
        // Get selected radiobutton
        function getSelectedRadioButton(){
            var isChecked = false;
            var index = 0;
            var rbs = getRbs();
            for(var i = 0; i < rbs.length; i++){
                if(rbs[i].checked){
                    isChecked = true;
                    //WLB alert(i);
                    return i;                    
                }
            }
            if(!isChecked){
                rbs[0].checked = true;
            }
            return index;
        }
        // Toggle radiobuttons programmatically
        function toggleRadioButtons(index){
            var i = 0;
            var rbs = getRbs();
            for(i = 0; i < rbs.length; i++){
                if(rbs[i].checked){
                    rbs[i].checked = false;
                }
            }
            if (i > 0)
                rbs[index].checked = true;
            else
                rbs.checked = true;
            return true;
        }
        // Get all radiobuttons in group "UseAddr"
        function getRbs(){
            var rbs;    
            rbs = document.forms[0].UseAddr;
            return rbs;
        }       
             
</script>

