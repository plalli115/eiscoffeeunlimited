<%@ Control Language="VB" AutoEventWireup="false" CodeFile="DesignModelB2B1List.ascx.vb"
    Inherits="controls_shipping_designmodels_DesignModelB2B1List" %>
<%@ Register TagPrefix="expshp" TagName="shippingoutput" Src="~/controls/shipping/parts/DataBoundList.ascx" %>
<%@ Register TagPrefix="expshp" TagName="shippinginput" Src="~/controls/shipping/parts/ShippingAddress.ascx" %>
<%@ Register TagPrefix="expshp" TagName="shippinglabeloutput" Src="~/controls/shipping/ItemTemplates/LabelOutputTemplate.ascx" %>
<%@ Register TagPrefix="grb" Namespace="GroupRadioButton" Assembly="GroupRadioButton" %>
<div>
    <expshp:shippingoutput runat="server" ID="output1" />
    <div class="ShippingLabelOutputDiv" id="shippingdisplaydiv">
        <asp:Panel runat="server" ID="shippinglabeloutputdiv" Style="display: block;">
            <expshp:shippinglabeloutput ID="output2" runat="server" />
        </asp:Panel>
        <div runat="server" class="ShippingInputDiv" id="shippinginputdiv" style="display: none">
            <expshp:shippinginput ID="input1" runat="server" />
        </div>
    </div>
</div>
<script type="text/javascript" language="javascript">        
        // Get selected radiobutton
        function getSelectedRadioButton(){
            var isChecked = false;
            var index = 0;
            var rbs = getRbs();
            for(var i = 0; i < rbs.length; i++){
                if(rbs[i].checked){
                    isChecked = true;
                    return i;                    
                }
            }
            if(!isChecked){
                if(rbs.length > 1)
                    rbs[0].checked = true;
                else
                    rbs.checked = true;
            }               
            return index;
        }
        // Toggle radiobuttons programmatically
        function toggleRadioButtons(index){
            var i = 0;
            var rbs = getRbs();
            for(i = 0; i < rbs.length; i++){
                if(rbs[i].checked){
                    rbs[i].checked = false;
                }
            }
            if (i > 0)
                rbs[index].checked = true;
            else
                rbs.checked = true;
            return true;
        }
        // Get all radiobuttons in group "UseAddr"
        function getRbs(){
            var rbs;    
            rbs = document.forms[0].UseAddr;
            return rbs;
        }      
</script>