<%--AM2011060201 - PRICES BREAKDOWN - Start--%>
<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PricesBreakDown.ascx.vb"
    Inherits="PricesBreakDown" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%  If (Not finalDictPrice Is Nothing AndAlso finalDictPrice.Count > 0) OrElse (Not finalDictDiscounts Is Nothing AndAlso finalDictDiscounts.Count > 0) Then%>
<%--<%  For Each item As ExpDictionary In finalDictPrice.Values%>
<%  If CIntEx(item("MinQty")) > 1 Then%>
<%  ShowInfo = True%>
<%  Exit For%>
<%  End If%>
<%  Next%>
<%  If ShowInfo Then%>--%>
<table style="width: 100%;">
    <tr>
        <td style="text-align: center;">
            <a onclick="javascript:animatedcollapse.toggle('PricesBreakDownDiv_<%= ProductGuid() %>');" style="cursor: pointer;
                font-size: 14px;">
                <%=Resources.Language.LABEL_PRICES_BREAK_DOWN_DISCOUNT%>
            </a>
        </td>
    </tr>
    <tr>
        <td align="left">
            <div id="PricesBreakDownDiv_<%= ProductGuid() %>" style="display: none; position: relative; background-color: White;
                border: solid 1px gray;">
                <table style="width: 100%;">
                    <tr>
                        <td style="padding: 0 5px 10px;">
                            <%If Not finalDictPrice Is Nothing AndAlso Not finalDictPrice Is DBNull.Value AndAlso finalDictPrice.Count > 0 Then%>
                            <% If CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) Then
                                    colSpanTitle = 3
                                End If
                                For Each item As ExpDictionary In finalDictPrice.Values
                                    If CStrEx(item("VariantCode")) <> "" Then
                                        ShowVariants = True
                                        colSpanTitle += 1
                                        Exit For
                                    End If
                                Next%>
                            <table cellpadding="2" cellspacing="2" style="width: 100%;">
                                <tr>
                                    <td align="center" style="padding-bottom: 5px;" <%="colspan=" & colSpanTitle %>>
                                        <b style="font-size: 14px">
                                            <%=Resources.Language.LABEL_PRICES_BREAK_DOWN_SALES_PRICES%>
                                        </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <b>
                                            <%= Resources.Language.LABEL_PRICE_BREAKDOWN_QTY%>
                                        </b>
                                    </td>
                                    <td align="left">
                                        <b>
                                            <%= Resources.Language.LABEL_PRICE_BREAKDOWN_PRICE %>
                                        </b>
                                    </td>
                                    <%If CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) Then%>
                                    <td align="right">
                                        <b>
                                            <%= Resources.Language.LABEL_UOM%>
                                        </b>
                                    </td>
                                    <%End If%>
                                    <%If ShowVariants Then%>
                                    <td align="right">
                                        <b>
                                            <%= Resources.Language.LABEL_VARIANTS_DISCOUNTED_PRICE %>
                                        </b>
                                    </td>
                                    <%End If %>
                                </tr>
                                <%For Each price As ExpDictionary In finalDictPrice.Values%>
                                <tr>
                                    <td align="center" style="font-weight: lighter; <% = IIF(price("Special"),"color:red;","") %>">
                                        <%If CIntEx(price("MinQty")) = 0 Then%>
                                        <%=1 %>
                                        <%Else%>
                                        <% = price("MinQty")%>
                                        <%End If%>
                                    </td>
                                    <td align="right" style="<% = IIF(price("Special"),"color:red;","") %>">
                                        <b>
                                            <%= CurrencyFormatter.FormatCurrency(price("Price"), Session("UserCurrencyGuid").ToString)   %>
                                        </b>
                                    </td>
                                    <%If CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) Then%>
                                    <td align="right" style="font-weight: lighter; <% = IIF(price("Special"),"color:red;","") %>">
                                        <% = price("UOMGuid")%>
                                    </td>
                                    <%End If%>
                                    <%If showvariants then %>
                                    <td align="right" style="font-weight: lighter; <% = IIF(price("Special"),"color:red;","") %>">
                                        <% If CStrEx(price("VariantName")) = "" Then%>
                                        <%= CStrEx(price("VariantCode"))%>
                                        <%Else%>
                                        <%= CStrEx(price("VariantName"))%>
                                        <%End If%>
                                    </td>
                                    <%End If %>
                                </tr>
                                <%Next%>
                            </table>
                            <%End If%>
                        </td>
                    </tr>
                    <%If Not finalDictPrice Is Nothing AndAlso Not finalDictPrice Is DBNull.Value AndAlso finalDictPrice.Count > 0 AndAlso Not finalDictDiscounts Is Nothing AndAlso Not finalDictDiscounts Is DBNull.Value AndAlso finalDictDiscounts.Count > 0 Then%>
                    <tr>
                        <td style="border-top: solid 2px gray;">
                            &nbsp;
                        </td>
                    </tr>
                    <%End If%>
                    <tr>
                        <td style="padding: 0 5px 5px;">
                            <%If Not finalDictDiscounts Is Nothing AndAlso Not finalDictDiscounts Is DBNull.Value AndAlso finalDictDiscounts.Count > 0 Then%>
                            <% If CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) Then
                                    colSpanTitle = 3
                                Else
                                    colSpanTitle = 2
                                End If
                                ShowVariants = False
                                For Each item As ExpDictionary In finalDictDiscounts.Values
                                    If CStrEx(item("VariantCode")) <> "" Then
                                        ShowVariants = True
                                        colSpanTitle += 1
                                        Exit For
                                    End If
                                Next%>
                            <table cellpadding="2" cellspacing="2" style="width: 100%;">
                                <tr>
                                    <td align="center" style="padding-bottom: 10px;" <%="colspan=" & colSpanTitle %>%>
                                        <b style="font-size: 14px">
                                            <%=Resources.Language.LABEL_PRICES_BREAK_DOWN_LINE_DISCOUNTS%>
                                        </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <b>
                                            <%= Resources.Language.LABEL_PRICE_BREAKDOWN_QTY%>
                                        </b>
                                    </td>
                                    <td align="right">
                                        <b>
                                            <%= Resources.Language.LABEL_PRICE_BREAKDOWN_DISCOUNT_PERC%>
                                        </b>
                                    </td>
                                    <%If CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) Then%>
                                    <td align="right">
                                        <b>
                                            <%= Resources.Language.LABEL_UOM%>
                                        </b>
                                    </td>
                                    <%End If%>
                                    <%If ShowVariants Then%>
                                    <td align="right">
                                        <b>
                                            <%= Resources.Language.LABEL_VARIANTS_DISCOUNTED_PRICE %>
                                        </b>
                                    </td>
                                    <%End If %>
                                </tr>
                                <%For Each discount As ExpDictionary In finalDictDiscounts.Values%>
                                <tr>
                                    <td align="center" style="font-weight: lighter;">
                                        <%If CIntEx(discount("MinQty")) = 0 Then%>
                                        <%=1 %>
                                        <%Else%>
                                        <% = discount("MinQty")%>
                                        <%End If%>
                                    </td>
                                    <td align="right">
                                        <b>
                                            <%=RoundEx(discount("LineDiscountPct"), 2) & "%"%>
                                        </b>
                                    </td>
                                    <%If CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) Then%>
                                    <td align="right" style="font-weight: lighter;">
                                        <% = discount("UOMGuid")%>
                                    </td>
                                    <%End If%>
                                    <%If ShowVariants Then%>
                                    <td align="right" style="font-weight: lighter;">
                                        <% = discount("VariantName")%>
                                    </td>
                                    <%End If %>
                                </tr>
                                <%Next%>
                            </table>
                            <%End If%>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
</table>
<script type="text/javascript">
    animatedcollapse.addDiv('PricesBreakDownDiv_<%= ProductGuid() %>', 'fade=1')
    animatedcollapse.ontoggle = function ($, divobj, state) { }
    animatedcollapse.init()
</script>
<%  Else%>
<table style="width: 145px;">
    <tr>
        <td style="height: 13px;">
            &nbsp;
        </td>
    </tr>
</table>
<%  End If%>
<%--<%  End If%>--%>
<%--AM2011060201 - PRICES BREAKDOWN - End--%>
