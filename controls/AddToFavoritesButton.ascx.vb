Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

<Themeable(True)> _
Partial Class AddToFavoritesButton
    Inherits ExpandIT.UserControl

    Private m_productguid As String
    Private m_displaymode As DisplayModes = DisplayModes.Button

    Enum DisplayModes
        Button = 0
        Link = 1
        Image = 2
    End Enum

    <Themeable(False)> _
    Public Property ProductGuid() As String
        Get
            Return m_productguid
        End Get
        Set(ByVal value As String)
            m_productguid = value
        End Set
    End Property

    Public Property ImageURL() As String
        Get
            Return imgbtnFavorites.ImageUrl
        End Get
        Set(ByVal value As String)
            imgbtnFavorites.ImageUrl = ThemeURL(value)
        End Set
    End Property

    Public Property DisplayMode() As DisplayModes
        Get
            Return m_displaymode
        End Get
        Set(ByVal value As DisplayModes)
            m_displaymode = value
        End Set
    End Property


    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim postbackurl As String = "~/_favorites.aspx?action=add&SKU=" & Server.UrlEncode(ProductGuid)

        btnFavorites.Visible = m_displaymode = DisplayModes.Button
        imgbtnFavorites.Visible = m_displaymode = DisplayModes.Image
        linkbtnFavorites.Visible = m_displaymode = DisplayModes.Link

        Select Case m_displaymode
            Case DisplayModes.Button
                With btnFavorites
                    .PostBackUrl = postbackurl
                    .Text = Resources.Language.LABEL_ADD_TO_FAVORITES
                End With
            Case DisplayModes.Image
                With imgbtnFavorites
                    .PostBackUrl = postbackurl
                    .AlternateText = Resources.Language.LABEL_ADD_TO_FAVORITES
                End With
            Case DisplayModes.Link
                With linkbtnFavorites
                    .PostBackUrl = postbackurl
                    .Text = Resources.Language.LABEL_ADD_TO_FAVORITES
                End With
        End Select

    End Sub

End Class
