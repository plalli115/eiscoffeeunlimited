Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.IO
Imports ExpandIT.Debug
Imports ExpandIT.GlobalsClass

'<%--JA2010061701 - SLIDE SHOW - Start--%>
Partial Class GenericSlideShow
    Inherits ExpandIT.UserControl

    Protected ImageDictionary As ExpDictionary
    Protected slideWidth As String = ""
    Protected slideHeight As String = ""
    Protected slidePosition As String = ""
    Protected slideArrows As String = ""
    Protected slideImgWidth As String = ""
    Protected slideImgHeight As String = ""
    Protected slideId As String = ""
    Protected showName As String = ""
    Protected showPrice As String = ""
    Protected showFooterBar As String = ""

    Public Property SlideShowFooterBar() As String
        Get
            Return showFooterBar
        End Get
        Set(ByVal value As String)
            showFooterBar = value
        End Set
    End Property

    Public Property SlideShowId() As String
        Get
            Return slideId
        End Get
        Set(ByVal value As String)
            slideId = value
        End Set
    End Property

    Public Property ImageDict() As ExpDictionary
        Get
            Return ImageDictionary
        End Get
        Set(ByVal value As ExpDictionary)
            ImageDictionary = value
        End Set
    End Property

    Public Property SlideShowWidth() As String
        Get
            Return slideWidth
        End Get
        Set(ByVal value As String)
            slideWidth = value
        End Set
    End Property

    Public Property SlideShowHeight() As String
        Get
            Return slideHeight
        End Get
        Set(ByVal value As String)
            slideHeight = value
        End Set
    End Property

    Public Property SlideShowPosition() As String
        Get
            Return slidePosition
        End Get
        Set(ByVal value As String)
            slidePosition = value
        End Set
    End Property

    Public Property SlideShowArrows() As String
        Get
            Return slideArrows
        End Get
        Set(ByVal value As String)
            slideArrows = value
        End Set
    End Property

    Public Property SlideImageWidth() As String
        Get
            Return slideImgWidth
        End Get
        Set(ByVal value As String)
            slideImgWidth = value
        End Set
    End Property

    Public Property SlideImageHeight() As String
        Get
            Return slideImgHeight
        End Get
        Set(ByVal value As String)
            slideImgHeight = value
        End Set
    End Property

    Public Property SlideShowName() As String
        Get
            Return showName
        End Get
        Set(ByVal value As String)
            showName = value
        End Set
    End Property

    Public Property SlideShowPrice() As String
        Get
            Return showPrice
        End Get
        Set(ByVal value As String)
            showPrice = value
        End Set
    End Property



    '<%--Not Using this code for the current slideshow--%>
    Function GetText() As String
        Dim testStr As String
        Dim folderpath As String = ApplicationRoot()
        Dim relativePath As String = "/controls/slideshow/text"
        Dim flagtext = False
        Dim flagtitle = False
        Dim flag = True
        Dim cont As Integer = 1
        Dim text, title As String
        Dim contItems As Integer = 0

        Dim html As String = ""

        While flag = True

            For Each fileName As String In System.IO.Directory.GetFiles(folderpath & relativePath)
                testStr = fileName.Substring(folderpath.Length + relativePath.Length + 1)
                If testStr.Substring(0, testStr.IndexOf(".")).ToUpper().EndsWith("TEXT" & cont) Then
                    ViewState("fileName") = VRoot & relativePath & "\" & testStr
                    text = File.ReadAllText(Server.MapPath("/" & VRoot & relativePath & "\" & testStr))
                    flagtext = True
                ElseIf testStr.Substring(0, testStr.IndexOf(".")).ToUpper().EndsWith("TITLE" & cont) Then
                    ViewState("fileName") = VRoot & relativePath & "\" & testStr
                    title = File.ReadAllText(Server.MapPath("/" & VRoot & relativePath & "\" & testStr))
                    flagtitle = True
                End If
                If flagtext And flagtitle Then
                    Exit For
                End If
            Next
            If flagtext Or flagtitle Then
                html &= "<div id=""text" & cont & """  style=""visibility: hidden; position: absolute; width:389px"">"
                contItems = contItems + 1
                If flagtitle Then
                    html &= "<div style= ""text-align:center""><a class=""text"">" & title & "</a><br><br></div>"
                    flagtitle = False
                End If
                If flagtext Then
                    html &= "<div style= ""text-align:justify""><a class=""text"">" & text & "</a></div>"
                    flagtext = False
                End If
                html &= "</div>"
                flag = True
            Else
                flag = False
            End If
            cont = cont + 1
        End While

        html &= "<imput title=""" & contItems.ToString() & """ id=""item"" type=""hidden""></imput>"
        Return html

    End Function
    '<%--Not Using this code for the current slideshow--%>


    Function GetImg()

        'eis.CatDefaultLoadProducts(ImageDictionary, False, False, False, New String() {"ProductName", "ListPrice"}, New String() {"DESCRIPTION", "PICTURE2"}, "")

        Dim item
        Dim link As String = ""
        Dim src As String = ""
        Dim html As String = ""
        Dim PriceStyle As String = ""
        For Each item In ImageDictionary.Values

            Dim newDictProperties As New ExpDictionary
            newDictProperties = eis.CatDefaultLoadProduct(item("ProductGuid"), True, True, True)
            link = ProductLink(item("ProductGuid"), 0)
            If CStrEx(newDictProperties("PICTURE2")) = "" Then
                src = "/images/nopix75.png"
            Else
                src = newDictProperties("PICTURE2")
            End If
            If CBoolEx(newDictProperties("IsSpecial")) Then
                PriceStyle = "SpecialPrice"
            Else
                PriceStyle = "RegularPrice"
            End If
            If slidePosition = "1" Then
                If ImageDictionary.Count = 1 Then
                    html &= "<li style=""float:left;"" ><a><img style="" width:" & CIntEx(slideImgWidth) & "px; height: " & CIntEx(slideImgHeight) & "px;"" src=""" & VRoot & "/images/p.gif"" /></a></li>"
                    html &= "<li style=""float:left; padding-left:10px;"" value=""" & item("ProductGuid") & """><a href=""" & link & """><img style=""" & eis.ThumbnailURLStyleMethod(src, CIntEx(slideImgWidth), CIntEx(slideImgHeight)) & """ src=""" & VRoot & "/" & src & """ /></a>"
                    If CStrEx(showName) = "1" Or CStrEx(showPrice) = "1" Then
                        html &= "<div class=""ProductBoxSmall2"">"
                        If CStrEx(showName) = "1" Then
                            html &= "<div style=""width:" & CIntEx(slideImgWidth) & "px;"" class=""ProductBoxSmall_ProductName""><a href=""" & link & """>" & newDictProperties("ProductName") & "</a></div>"
                        End If
                        If CStrEx(showPrice) = "1" And eis.CheckPageAccess("Prices") Then
                            If AppSettings("SHOW_TAX_TYPE") = "EXCL" Then
                                html &= "<div class=""ProductTemplate_Price""><span  style=""cursor:text;"" class=""" & PriceStyle & """>" & CurrencyFormatter.FormatCurrency(CDblEx(newDictProperties("LineTotal")), Session("UserCurrencyGuid").ToString) & "</span></div>"
                            Else
                                html &= "<div class=""ProductTemplate_Price""><span  style=""cursor:text;"" class=""" & PriceStyle & """>" & CurrencyFormatter.FormatCurrency(CDblEx(newDictProperties("TotalInclTax")), Session("UserCurrencyGuid").ToString) & "</span></div>"
                            End If

                        End If
                        html &= "</div></li>"
                    Else
                        html &= "</li>"
                    End If
                    html &= "<li style=""float:left;"" ><a><img style="" width:" & CIntEx(slideImgWidth) & "px; height: " & CIntEx(slideImgHeight) & "px;"" src=""" & VRoot & "/images/p.gif"" /></a></li>"
                Else
                    html &= "<li style=""float:left; padding-left:10px;"" value=""" & item("ProductGuid") & """><a href=""" & link & """><img style=""" & eis.ThumbnailURLStyleMethod(src, CIntEx(slideImgWidth), CIntEx(slideImgHeight)) & """ src=""" & VRoot & "/" & src & """ /></a>"
                    If CStrEx(showName) = "1" Or CStrEx(showPrice) = "1" Then
                        html &= "<div class=""ProductBoxSmall2"">"
                        If CStrEx(showName) = "1" Then
                            html &= "<div style=""width:" & CIntEx(slideImgWidth) & "px;"" class=""ProductBoxSmall_ProductName""><a href=""" & link & """>" & newDictProperties("ProductName") & "</a></div>"
                        End If
                        If CStrEx(showPrice) = "1" And eis.CheckPageAccess("Prices") Then
                            If AppSettings("SHOW_TAX_TYPE") = "EXCL" Then
                                html &= "<div class=""ProductTemplate_Price""><span  style=""cursor:text;"" class=""" & PriceStyle & """>" & CurrencyFormatter.FormatCurrency(CDblEx(newDictProperties("LineTotal")), Session("UserCurrencyGuid").ToString) & "</span></div>"
                            Else
                                html &= "<div class=""ProductTemplate_Price""><span  style=""cursor:text;"" class=""" & PriceStyle & """>" & CurrencyFormatter.FormatCurrency(CDblEx(newDictProperties("TotalInclTax")), Session("UserCurrencyGuid").ToString) & "</span></div>"
                            End If
                        End If
                        html &= "</div></li>"
                    Else
                        html &= "</li>"
                    End If
                End If

            ElseIf slidePosition = "0" Then
                If ImageDictionary.Count = 1 Then
                    html &= "<li style=""float:right;"" ><a><img style="" width:" & CIntEx(slideImgWidth) & "px; height: " & CIntEx(slideImgHeight) & "px;"" src=""" & VRoot & "/images/p.gif"" /></a></li>"
                    html &= "<li style=""float:right; padding-bottom:10px;"" value=""" & item("ProductGuid") & """><a href=""" & link & """><img style=""" & eis.ThumbnailURLStyleMethod(src, CIntEx(slideImgWidth), CIntEx(slideImgHeight)) & """ src=""" & VRoot & "/" & src & """ /></a>"
                    If CStrEx(showName) = "1" Or CStrEx(showPrice) = "1" Then
                        html &= "<div class=""ProductBoxSmall2"">"
                        If CStrEx(showName) = "1" Then
                            html &= "<div style=""width:" & CIntEx(slideImgWidth) & "px;"" class=""ProductBoxSmall_ProductName""><a href=""" & link & """>" & newDictProperties("ProductName") & "</a></div>"
                        End If
                        If CStrEx(showPrice) = "1" And eis.CheckPageAccess("Prices") Then
                            If AppSettings("SHOW_TAX_TYPE") = "EXCL" Then
                                html &= "<div class=""ProductTemplate_Price""><span  style=""cursor:text;"" class=""" & PriceStyle & """>" & CurrencyFormatter.FormatCurrency(CDblEx(newDictProperties("LineTotal")), Session("UserCurrencyGuid").ToString) & "</span></div>"
                            Else
                                html &= "<div class=""ProductTemplate_Price""><span  style=""cursor:text;"" class=""" & PriceStyle & """>" & CurrencyFormatter.FormatCurrency(CDblEx(newDictProperties("TotalInclTax")), Session("UserCurrencyGuid").ToString) & "</span></div>"
                            End If
                        End If
                        html &= "</div></li>"
                    Else
                        html &= "</li>"
                    End If
                    html &= "<li style=""float:right;"" ><a><img style="" width:" & CIntEx(slideImgWidth) & "px; height: " & CIntEx(slideImgHeight) & "px;"" src=""" & VRoot & "/images/p.gif"" /></a></li>"
                Else
                    html &= "<li style=""float:right; padding-bottom:10px;"" value=""" & newDictProperties("ProductGuid") & """><a href=""" & link & """><img style=""" & eis.ThumbnailURLStyleMethod(src, CIntEx(slideImgWidth), CIntEx(slideImgHeight)) & """ src=""" & VRoot & "/" & src & """ /></a>"
                    If CStrEx(showName) = "1" Or CStrEx(showPrice) = "1" Then
                        html &= "<div class=""ProductBoxSmall2"">"
                        If CStrEx(showName) = "1" Then
                            html &= "<div style=""width:" & CIntEx(slideImgWidth) & "px;"" class=""ProductBoxSmall_ProductName""><a href=""" & link & """>" & newDictProperties("ProductName") & "</a></div>"
                        End If
                        If CStrEx(showPrice) = "1" And eis.CheckPageAccess("Prices") Then
                            If AppSettings("SHOW_TAX_TYPE") = "EXCL" Then
                                html &= "<div class=""ProductTemplate_Price""><span  style=""cursor:text;"" class=""" & PriceStyle & """>" & CurrencyFormatter.FormatCurrency(CDblEx(newDictProperties("LineTotal")), Session("UserCurrencyGuid").ToString) & "</span></div>"
                            Else
                                html &= "<div class=""ProductTemplate_Price""><span  style=""cursor:text;"" class=""" & PriceStyle & """>" & CurrencyFormatter.FormatCurrency(CDblEx(newDictProperties("TotalInclTax")), Session("UserCurrencyGuid").ToString) & "</span></div>"
                            End If
                        End If
                        html &= "</div></li>"
                    Else
                        html &= "</li>"
                    End If
                End If
            End If
        Next

        If ImageDictionary.Count = 2 Then
            If slidePosition = "1" Then
                html &= "<li style=""float:left;"" ><a><img style="" width:" & CIntEx(slideImgWidth) & "px; height: " & CIntEx(slideImgHeight) & "px;"" src=""" & VRoot & "/images/p.gif"" /></a></li>"
            Else
                html &= "<li style=""float:right;"" ><a><img style="" width:" & CIntEx(slideImgWidth) & "px; height: " & CIntEx(slideImgHeight) & "px;"" src=""" & VRoot & "/images/p.gif"" /></a></li>"
            End If
        End If

        If ImageDictionary.Count > 3 Then
            If slidePosition = "1" Then
                html &= "<li style=""float:left;"" ><a><img style="" width:50px; height:50px;"" src=""" & VRoot & "/images/p.gif"" /></a></li>"
            Else
                html &= "<li style=""float:right;"" ><a><img style="" width:50px; height:50px;"" src=""" & VRoot & "/images/p.gif"" /></a></li>"
            End If
        End If

        Return html
    End Function


    Function GetCode()
        Dim html As String = ""
        html = html & "<div id=""thumbwrapper""  style=""border-width: 2px; padding-left: 0px; margin-right: 2px; "
        'If AppSettings("SLIDE_SHOW_ARROWS") = 1 Then
        '    html = html & "padding-right: 40px; padding-left: 40px;"
        'End If
        Dim myid As String = "thumbs" & CStrEx(slideId)
        'myid = "thumbs"
        html = html & "width:" & slideWidth & "; height:" & slideHeight & ";"">"
        html = html & " <div id=""thumbarea"" style= ""text-align:justify;"
        html = html & "width:" & slideWidth & "; height:" & slideHeight & ";"">"
        If slidePosition = "1" Then
            html = html & "<ul id=""" & myid & """ style=""height:" & slideHeight & ";width:10000px; list-style:none outside none; margin-left:0; margin-top:12px; padding-left:30px; position:absolute;"">"
        ElseIf slidePosition = "0" Then
            html = html & "<ul id=""" & myid & """ style=""width:" & slideWidth & ";height:10000px; list-style:none outside none; margin-left:0; margin-top:12px; padding-left:0; padding-top:30px; position:absolute;"">"
        End If

        html = html & GetImg() & "</ul></div></div>"
        Return html
    End Function


End Class
'<%--JA2010061701 - SLIDE SHOW - Start--%>