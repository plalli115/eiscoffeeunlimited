<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="GenericSlideShow.ascx.vb"
    Inherits="GenericSlideShow" %>
<%--JA2010061701 - SLIDE SHOW - Start--%> 
<table style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <div id="gallerys" style="border-width: 0px; padding: 0px; top: 0px; left: 0px; margin-bottom: 0px;
                margin-top: 0px;">
                <%--<a id="previmg" class="imgnav " href="javascript:onclick=slideShow.nav3(2)"></a><a id="nextimg"
                            class="imgnav " href="javascript:onclick=slideShow.nav3(1)"></a>--%>
                            <%Dim event1 As String = "slideShow.scrl(-1,'thumbs" & SlideShowId() & "','" & SlideShowPosition() & "');"%>
                            <%Dim event2 As String = "slideShow.scrl(1,'thumbs" & SlideShowId() & "','" & SlideShowPosition() & "');"%>
                            <%Dim event3 As String = "slideShow.scrl(0,'thumbs" & SlideShowId() & "','" & SlideShowPosition() & "');"%>
                <%If slidePosition = "1" Then%>
                <%--Horizontal--%>
                    <%If slideArrows = "1" Then%>
                        <a id="A7" style="width:60px; background: transparent url('<%= Vroot %>/controls/slideshow/images/leftArrow.gif'); background-repeat: no-repeat; background-position:left center;" onmouseover="<%=event1 %>" onmouseout="<%=event3 %>" class="imgnavLeft"></a>
                        <a id="A8" style="width:60px; background: transparent url('<%= Vroot %>/controls/slideshow/images/rightArrow.gif'); background-repeat: no-repeat; background-position:right center;" onmouseover="<%=event2 %>" onmouseout="<%=event3 %>" class="imgnavRight"></a>
                    <%Else%>
                        <a id="A5" style="width:60px; background: transparent url('<%= Vroot %>/controls/slideshow/images/spacer.gif'); background-repeat: no-repeat; background-position:left center;" onmouseover="<%=event1 %>" onmouseout="<%=event3 %>" class="imgnavLeft"></a>
                        <a id="A6" style="width:60px; background: transparent url('<%= Vroot %>/controls/slideshow/images/spacer.gif'); background-repeat: no-repeat; background-position:right center;" onmouseover="<%=event2 %>" onmouseout="<%=event3 %>" class="imgnavRight "></a>
                    <%End If %>
                <%Else %>
                    <%If slideArrows = "1" Then%>
                        <a id="A9" style="height:40px; background: transparent url('<%= Vroot %>/controls/slideshow/images/topArrow.gif'); background-repeat: no-repeat; background-position:center top; " onmouseover="<%=event1 %>" onmouseout="<%=event3 %>" class="imgnavVerticalTop"></a>
                    <%Else %>
                         <a id="A11" style="height:40px; background: transparent url('<%= Vroot %>/controls/slideshow/images/spacer.gif'); background-repeat: no-repeat; background-position:center top;" onmouseover="<%=event1 %>" onmouseout="<%=event3 %>" class="imgnavVerticalTop"></a>
                    <%End If %>                
                <%End If%>
                <div id="imagearea" style="width: 595px; height: 188px; display: none;">
                    <table>
                        <tr>
                            <td>
                                <div id="box">
                                    <a class="index" id="image"></a>
                                </div>
                            </td>
                            <td>
                                <div id="textimage" style="text-align: justify">
                                    <%--Not Using this code for the current slideshow--%>
                                    <% = GetText() %>
                                    <%--Not Using this code for the current slideshow--%>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <%=GetCode()%>
                
                <%If slidePosition = "0" Then%>
                <%--Vertical--%>
                    <%If slideArrows = "1" Then%>
                        <a id="A2" style=" bottom:0; height:40px; background: transparent url('<%= Vroot %>/controls/slideshow/images/bottomArrow.gif'); background-repeat: no-repeat; background-position:center bottom;" onmouseover="<%=event2 %>" onmouseout="<%=event3 %>" class="imgnavVerticalBottom"></a>
                    <%Else %>
                        <a id="A4" style=" bottom:0; height:40px; background: transparent url('<%= Vroot %>/controls/slideshow/images/spacer.gif'); background-repeat: no-repeat; background-position:center bottom;" onmouseover="<%=event2 %>" onmouseout="<%=event3 %>" class="imgnavVerticalBottom"></a>
                    <%End If %>
                <%End If %>
            </div>
        </td>
    </tr>
    <%If showFooterBar = "1" Then%>
    <tr>
        <td class="SlideShowBar">
            <img id="Img2" runat="server" src="~/images/p.gif" border="0" alt="_" height="7"
                width="1" />
        </td>
    </tr>
    <%End If %>
</table>
<%--<%  slideId = ""%>--%>
<%  If CStrEx(slideId) <> "" Then%>
<% ="<script type=""text/javascript"">var Myposition='" & slidePosition & "'; var textid='textimage';var imgid = 'image'; var imgdir = '" & VRoot & "/controls/slideshow/thumbs';var imgext = '.jpg';var thumbid = 'thumbs" & CStrEx(slideId) & "'; var auto = true;var autodelay = 5;</script>"%>
<%Else %>
<% ="<script type=""text/javascript"">var Myposition='" & slidePosition & "'; var textid='textimage';var imgid = 'image'; var imgdir = '" & VRoot & "/controls/slideshow/thumbs';var imgext = '.jpg';var thumbid = 'thumbs'; var auto = true;var autodelay = 5;</script>"%>
<%End If %>

<script type="text/javascript" src="<% = server.URLPathEncode(VRoot &  "/controls/slideshow/slide.js") %>"></script>


<%--JA2010061701 - SLIDE SHOW - End--%>