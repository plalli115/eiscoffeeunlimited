<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MostPopular.ascx.vb"
    Inherits="MostPopular" %>
<%@ Register Src="~/controls/ProductDisplayList.ascx" TagName="ProductDisplayList"
    TagPrefix="uc1" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>

<div class="MostPopularProducts">
<h2><% =Resources.Language.LABEL_MOST_POPULAR_ITEM_SHOP%></h2>
    <asp:DataList ID="dlProducts" SkinID="GroupTemplate" runat="server" RepeatColumns="3"
        RepeatDirection="Horizontal" CssClass="GroupProductList" ItemStyle-CssClass="GroupProductListItem">
        <ItemTemplate>
            <uc1:ProductDisplayList ID="ProductDisplayList1" runat="server" Product='<%# Eval("Value") %>' />
        </ItemTemplate>
    </asp:DataList>
</div>
