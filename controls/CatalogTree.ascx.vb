﻿Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

Partial Class CatalogTree
    Inherits ExpandIT.UserControl
    Protected dictCatalogOverview As Object
    Protected cachekey, catalogguid, overviewHtml As String
    Protected dictCatalogs As ExpDictionary
    Protected bct As BreadcrumbTrail

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If AppSettings("SHOW_EXPANDED_CATALOG") Then
            ' Check the page access.
            eis.PageAccessRedirect("Catalog")

            ' Get CatalogGuid if it is present.
            catalogguid = CStrEx(Request("CatalogGuid"))
            'AM2010071901 - ENTERPRISE CSC - Start
            cachekey = "OverViewHtml-" & Session("LanguageGuid").ToString & "-" & catalogguid & "-" & globals.User("CatalogNo")
            'AM2010071901 - ENTERPRISE CSC - End
            '*****CUSTOMER SPECIFIC CATALOGS***** -START
            'ExpandIT US - USE CSC - Start
            If CBoolEx(AppSettings("EXPANDIT_US_USE_CSC")) Then
                cachekey &= "-" & CStrEx(globals.User("CustomerGuid"))
            End If
            'cachekey = "OverViewHtml-" & Session("LanguageGuid").ToString & "-" & catalogguid
            'ExpandIT US - USE CSC - End
            '*****CUSTOMER SPECIFIC CATALOGS***** -END


            dictCatalogOverview = CacheGet(cachekey)
            If dictCatalogOverview Is Nothing Then
                dictCatalogs = eis.CatLoadCatalogs()

                ' Render html
                overviewHtml = GetOverviewHtml(catalogguid)

                ' Cache the result
                dictCatalogOverview = New ExpDictionary()
                dictCatalogOverview("overviewHtml") = overviewHtml
                dictCatalogOverview("dictCatalogs") = dictCatalogs
                dictCatalogOverview("CatalogGuid") = catalogguid
                CacheSetAggregated(cachekey, dictCatalogOverview, Split(AppSettings("GroupProductRelatedTablesForCaching") & "|GroupProduct", "|"))
            Else
                ' Get the result from the cached values
                overviewHtml = dictCatalogOverview("overviewHtml")
                dictCatalogs = dictCatalogOverview("dictCatalogs")
                catalogguid = dictCatalogOverview("CatalogGuid")
            End If

            litOverview.Text = overviewHtml

        Else
            eis.InitCatalogStructure(GetRequest("GroupGuid"))
        End If

    End Sub


    Function GetOverviewGroups(ByVal LanguageGuid As String) As ExpDictionary
        Dim group As ExpDictionary
        Dim groups As ExpDictionary
        Dim parentguid As Integer
        Dim cachekey As String
        Dim sql As String
        Dim info As ExpDictionary

        'AM2010071901 - ENTERPRISE CSC - Start
        cachekey = "OverViewGroups-" & LanguageGuid & "-" & globals.User("CatalogNo")
        'AM2010071901 - ENTERPRISE CSC - End
        '*****CUSTOMER SPECIFIC CATALOGS***** -START
        'ExpandIT US - USE CSC - Start
        If CBoolEx(AppSettings("EXPANDIT_US_USE_CSC")) Then
            cachekey &= "-" & CStrEx(globals.User("CustomerGuid"))
        End If
        'cachekey = "OverViewGroups-" & LanguageGuid
        'ExpandIT US - USE CSC - End
        '*****CUSTOMER SPECIFIC CATALOGS***** -END

        groups = CacheGet(cachekey)
        If groups Is Nothing Then
            ' Load Catalog

            'AM2010071901 - ENTERPRISE CSC - Start
            sql = "SELECT * FROM GroupTable WHERE CatalogNo=" & SafeString(globals.User("CatalogNo"))
            'sql = "SELECT * FROM GroupTable"
            'AM2010071901 - ENTERPRISE CSC - End

            'JA2010030901 - PERFORMANCE -Start
            'ExpandIT US - USE CSC - Start
            If CBoolEx(AppSettings("EXPANDIT_US_USE_CSC")) Then
                Dim objCSC As New csc(globals)
                'AM2010071901 - ENTERPRISE CSC - Start
                sql = objCSC.groupsWithProductsSQL("SELECT DISTINCT ParentGuid FROM GroupTable WHERE CatalogNo=" & SafeString(globals.User("CatalogNo")))
                'AM2010071901 - ENTERPRISE CSC - End
            End If
            'ExpandIT US - USE CSC - End
            'JA2010030901 - PERFORMANCE -End


            groups = Sql2Dictionaries(sql, New String() {"GroupGuid"})

            ' Load Group properties
            info = eis.CatGroups2Info(groups)
            info("GroupInfo")("PropertyArray") = New String() {"NAME", "DISABLELINK"}
            eis.CatLoadGroups(info)

            ' Prepare each group to contain sub groups
            For Each group In groups.Values
                group("SubGroups") = New ExpDictionary
            Next

            ' Associate properties with groups and rearrange the groups into sub groups
            For Each group In groups.Values
                ' Set the position in the tree
                parentguid = group("ParentGuid")
                If parentguid <> 0 Then
                    If groups.Exists(SafeString(parentguid)) Then
                        groups(SafeString(parentguid))("SubGroups")(SafeString(group("GroupGuid"))) = group
                    End If
                End If
            Next

            ' Remove unused values
            Dim grouparray(groups.Count - 1) As Object
            groups.Values.CopyTo(grouparray, 0)
            For Each group In grouparray
                parentguid = group("ParentGuid")
                group.Remove("SortIndex")
                group.Remove("ParentGuid")
                If parentguid <> 0 Then groups.Remove(SafeString(group("GroupGuid")))
            Next

            ' Cache the result
            CacheSet(cachekey, groups, "GroupTable")
        End If

        Return groups
    End Function

    Function GetOverviewHtml(ByVal catalogguid As String) As String
        Dim groups As ExpDictionary
        Dim groupcount As Integer
        Dim blocks As ExpDictionary
        Dim blockkey, blockhtml As String
        Dim html As String = ""
        Dim displaycount As Integer
        Dim cataloggroup As ExpDictionary
        Dim maingroup As ExpDictionary
        Dim blockcount As Integer
        Dim block As ExpDictionary

        groups = GetOverviewGroups(Session("LanguageGuid").ToString)

        '' Make sure that the catalog guid is valid
        'If catalogguid = "" Or Not groups.Exists(SafeString(catalogguid)) Then
        '    Try
        '        catalogguid = GetFirst(groups).Value("GroupGuid")
        '    Catch ex As Exception
        '        catalogguid = ""
        '    End Try
        'End If

        '' Bail out if no catalogs exists
        'If CStrEx(catalogguid) = "" Then
        '    GetOverviewHtml = Resources.Language.MESSAGE_NO_CATALOG_AVAILABLE
        '    Exit Function
        'End If

        '' Get the selected catalog
        'cataloggroup = groups(SafeString(catalogguid))


        blockhtml = "<ul class=""CatalogTreeLeft"" >"

        For Each cataloggroup In groups.Values
            ' Create the html blocks
            groupcount = 0
            blocks = New ExpDictionary


            If CStrEx(cataloggroup("DISABLELINK")) = "1" Then
                blockhtml = blockhtml & "<li style=""font-size: 11px; font-weight:bold; padding-left:16px;""><img style=""border:0px;"" alt="""" src=""" & VRoot & "/App_Themes/EnterpriseBlue/bg_list2.gif"" /><a style=""padding-left: 5px;"">" & cataloggroup("NAME") & "</a></li>"
            Else
                blockhtml = blockhtml & "<li style=""font-size: 11px; font-weight:bold; padding-left:16px;""><a href=""" & GroupLink(cataloggroup("GroupGuid")) & """><img style=""border:0px;"" alt="""" src=""" & VRoot & "/App_Themes/EnterpriseBlue/bg_list2.gif"" /></a><a href=""" & GroupLink(cataloggroup("GroupGuid")) & """>" & cataloggroup("NAME") & "</a></li>"
            End If

            For Each maingroup In cataloggroup("SubGroups").values
                blockcount = 0
                blockkey = CStr(blocks.Count + 1)
                blockhtml = blockhtml & "<li style=""font-size: 11px; font-weight:bold; padding-left:30px;"">" & RenderBlock(maingroup, blockcount) & vbCrLf
                block = New ExpDictionary()
                block("Size") = blockcount
                block("Html") = blockhtml
                blocks.Add(blockkey, block)
                groupcount = groupcount + blockcount
            Next
        Next

        blockhtml = blockhtml & "</ul>"
        html = vbCrLf
        displaycount = 0
        html = blockhtml
        html = html & vbCrLf
        

        Return html
    End Function

    Function RenderBlock(ByVal group As ExpDictionary, ByRef blockcount As Integer) As String
        Dim retv As String
        Dim subgroup As ExpDictionary
        Dim name As String
        Dim padding As Integer = 1

        name = HTMLEncode(Group("NAME"))
        If name = "" Then name = "<font color=""red"">[" & Resources.Language.MESSAGE_MISSING_PROPERTY & "]</font>"

        If CStrEx(group("DISABLELINK")) = "1" Then
            retv = "<img style=""border:0px;"" alt="""" src=""" & VRoot & "/App_Themes/EnterpriseBlue/bg_list2.gif"" /><a style=""padding-left: 5px;"">" & name & "</a>"
        Else
            retv = "<a href=""" & GroupLink(group("GroupGuid")) & """><img style=""border:0px;"" alt="""" src=""" & VRoot & "/App_Themes/EnterpriseBlue/bg_list2.gif"" /></a><a href=""" & GroupLink(group("GroupGuid")) & """>" & name & "</a>"
        End If

        If Group("SubGroups").Count > 0 Then
            'retv = retv & "<li style=""margin-left:14px;"" >"
            retv = retv & "</li>"
            For Each subgroup In Group("SubGroups").values
                blockcount = blockcount + 1
                padding = CIntEx(eis.NumberOfParents(group("GroupGuid"))) * 14
                If eis.IsParent(subgroup("GroupGuid")) Then
                    retv = retv & "<li style=""font-weight:bold;padding-left:" & padding + 30 & "px;"">" & RenderBlock(subgroup, blockcount) & vbCrLf
                Else
                    retv = retv & "<li style=""font-weight:normal;padding-left:" & padding + 30 & "px;"">" & RenderBlock(subgroup, blockcount) & vbCrLf
                End If
            Next
            'retv = retv & "</ul>" & vbCrLf
        Else
            retv = retv & "</li>"
        End If
        RenderBlock = retv
    End Function

End Class
