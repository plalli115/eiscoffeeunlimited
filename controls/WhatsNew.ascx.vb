'AM2010051802 - WHAT'S NEW - Start
Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

Partial Class WhatsNew
    Inherits ExpandIT.UserControl
    Protected retv As ExpDictionary
    Protected randomNumber As New Random()
    Protected litId As Integer = 0

    Private Sub Page_PreRender(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.PreRender
        For Each item As DataListItem In Me.dlProducts.Items
            Dim lblToFormat As Label

            lblToFormat = CType(item.FindControl("lblPrice"), Label)
            lblToFormat.Text = CurrencyFormatter.FormatCurrency(CDblEx(lblToFormat.Text), HttpContext.Current.Session("UserCurrencyGuid"))

        Next
    End Sub
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim sql As String
        Dim useDate As Boolean = False
        Dim daysBack As Integer = 0
        'JA2010030901 - PERFORMANCE -Start
        Dim cachekey As String
        Dim tableNames As String() = {"GroupProduct", "ProductTable"}

        cachekey = "WhatsNew-" & CStrEx(globals.User("CustomerGuid")) & "-" & globals.User("CatalogNo") & "-" & DateValue(Now)
        If cachekey <> "" Then retv = EISClass.CacheGet(cachekey) Else retv = Nothing
        If retv Is Nothing Then
            If AppSettings("REQUIRE_CATALOG_ENTRY") Then
                '*****CUSTOMER SPECIFIC CATALOGS***** - START
                If AppSettings("EXPANDIT_US_USE_CSC") Then
                    'JA2010030901 - PERFORMANCE -Start
                    Dim objCSC As New csc(globals)
                    sql = objCSC.loadWhatsNew()
                    'JA2010030901 - PERFORMANCE -End
                    '*****CUSTOMER SPECIFIC CATALOGS***** - END
                    'AM2010071901 - ENTERPRISE CSC - Start
                Else
                    sql = "SELECT ProductTable.ProductGuid FROM ProductTable INNER JOIN (SELECT DISTINCT " & _
                    "GroupProduct.ProductGuid FROM GroupProduct WHERE GroupProduct.CatalogNo=" & SafeString(globals.User("CatalogNo")) & _
                    ") AS GP ON ProductTable.ProductGuid=GP.ProductGuid WHERE "
                End If
            Else
                sql = "SELECT ProductGuid FROM ProductTable WHERE "
            End If

            useDate = CBoolEx(getSingleValueDB("SELECT NewItemUseDate FROM EESetup"))
            'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - Start
            'useDate = CBoolEx(getSingleValueDB("SELECT NewItemUseDate FROM EESetup"))
            Try
                useDate = CBoolEx(eis.getEnterpriseConfigurationValue("NewItemUseDate"))
            Catch ex As Exception
            End Try
            'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - End

            If useDate Then


                'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - Start
                ' daysBack = CIntEx(getSingleValueDB("SELECT NewItemDaysBack FROM EESetup"))
                Try
                    daysBack = CIntEx(eis.getEnterpriseConfigurationValue("NewItemDaysBack"))
                Catch ex As Exception
                End Try
                'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - End


                sql &= "DateAdded IS NOT NULL AND DATEDIFF(day,DateAdded,GETDATE()) <= " & daysBack
            Else
                sql &= "NewItem='True'"
            End If
            'AM2010071901 - ENTERPRISE CSC - End

            retv = Sql2Dictionaries(sql, "ProductGuid")
            ' Set cache
            If cachekey <> "" Then
                EISClass.CacheSetAggregated(cachekey, retv, tableNames)
            End If
        End If
        'JA2010030901 - PERFORMANCE -End
        'JA2010030901 - PERFORMANCE -Start 
        'eis.CatDefaultLoadProducts(retv, False, False, False, New String() {"ProductName", "ListPrice", "BaseUOM"}, New String() {"DESCRIPTION", "PICTURE2"}, "")
        retv = eis.CatDefaultLoadProducts(retv, False, False, False, New String() {"ProductName", "ListPrice", "BaseUOM"}, New String() {"DESCRIPTION", "PICTURE2"}, "NewItems")
        eis.GetCustomerPricesEx(retv)
        'JA2010030901 - PERFORMANCE -End


        Dim item As ExpDictionary
        Dim item2 As ExpDictionary
        If Not retv Is Nothing And Not retv.Count = 0 Then

            Dim num As Integer = randomNumber.Next(1, retv.Count + 1)
            Dim num2 As Integer
            Dim cont As Integer = 1
            Dim retv2 As New ExpDictionary
            For Each item In retv.Values
                If cont = num Then
                    retv2.Add(item("ProductGuid"), item)
                    If retv.Count > 1 Then
                        num2 = randomNumber.Next(1, retv.Count + 1)
                        If num = num2 Then
                            While num = num2
                                num2 = randomNumber.Next(1, retv.Count + 1)
                                If num <> num2 Then
                                    Exit While
                                End If
                            End While
                        End If
                        cont = 1
                        For Each item2 In retv.Values
                            If cont = num2 Then
                                retv2.Add(item2("ProductGuid"), item2)
                                Exit For
                            Else
                                cont = cont + 1
                            End If
                        Next
                    End If
                Else
                    cont = cont + 1
                End If
                If retv2.Count = 2 Then
                    Exit For
                End If
            Next


            dlProducts.DataSource = retv2
            dlProducts.DataBind()
        End If
    End Sub

    Protected Function getStyle(ByVal src As Object)
        Dim str As String = ""
        str = eis.ThumbnailURLStyleMethod(src, CIntEx(AppSettings("WHATS_NEW_WIDTH_IMG")), CIntEx(AppSettings("WHATS_NEW_HEIGHT_IMG")))
        Return str
    End Function

    Protected Function getSrc(ByVal pic As String)
        Dim root As String = ""
        If CStrEx(pic) = "" Then
            root = VRoot & "/images/nopix75.png"
        Else
            root = VRoot & "/" & pic
        End If

        Return root
    End Function

    Protected Function getLink(ByVal link As String)
        Dim root As String = ProductLink(link, 0)
        Return root
    End Function

End Class
'AM2010051802 - WHAT'S NEW - End