Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass

Partial Class CurrencyDropDown
    Inherits ExpandIT.UserControl

    Private selectedVal As String

    Protected Sub OnCreate(ByVal sender As Object, ByVal eventArgs As ObjectDataSourceEventArgs) Handles ObjectDataSource1.ObjectCreating
        Try
            Dim cl As CurrencyControlLogic = CurrencyControlLogicFactory.CreateInstance(globals.User)
            eventArgs.ObjectInstance = cl
            selectedVal = cl.SelectedValue

            If selectedVal Is Nothing Or selectedVal Is DBNull.Value Then
                ddlCurrency.Visible = False
            ElseIf selectedVal.Length < 1 Then
                ddlCurrency.Visible = False
            End If


            If selectedVal IsNot Nothing Then
                eis.updateUserCurrency(selectedVal)
            Else ' if currency table is empty or if no currencies are enabled
                eis.updateUserCurrency(AppSettings("MULTICURRENCY_SITE_CURRENCY"))
            End If

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        
    End Sub

    Protected Sub OnBound(ByVal sender As Object, ByVal e As EventArgs) Handles ddlCurrency.DataBound
        Dim ddl As DropDownList
        ddl = sender
        If selectedVal IsNot Nothing AndAlso ddl.Items.FindByValue(selectedVal) IsNot Nothing Then
            ddl.SelectedValue = selectedVal
        End If
    End Sub

    Protected Sub ddlCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCurrency.SelectedIndexChanged
            Dim ddl As DropDownList
            Try
                ddl = sender
                If ddl.SelectedItem.Value IsNot Nothing Then
                    eis.updateUserCurrency(ddl.SelectedItem.Value)
                End If
            Catch ex As Exception
                'No selected item
            End Try
    End Sub
End Class