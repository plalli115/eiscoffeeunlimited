<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BestSellersByCategory.ascx.vb"
    Inherits="BestSellersByCategory" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<table class="infoBoxHeading_table" cellpadding="0" cellspacing="0" style="width: 200px;">
    <tr>
        <td>
            <table class="boxText" style="margin-left: 12px; max-width:200px;">
                <% If MostPopularItems.Count > 0 Then%>
                <% Dim intItemCounter As Integer = 1%>
                <%Dim PopularItem%>
                <% For Each PopularItem In MostPopularItems.Values%>
                <tr style="height: 16px;">
                    <td class="TDL" style="vertical-align:top;">
                        <a>
                            <%=intItemCounter & ". "%>
                        </a>
                        <% intItemCounter = intItemCounter + 1%>
                    </td>
                    <%--<% If eis.CheckPageAccess("Catalog") Then%>
                    <td class="TDL">
                        <a href="<% = ProductLink(PopularItem("ProductGuid"), 0) %>">
                            <% =HTMLEncode(PopularItem("ProductGuid"))%>
                        </a>&nbsp;&nbsp;
                    </td>
                    <% Else%>
                    <td class="TDL">
                        <% =HTMLEncode(PopularItem("ProductGuid"))%>
                        &nbsp;&nbsp;
                        <% End If%>
                    </td>--%>
                    <td class="TDL" style="text-align: left; width: 100%; white-space:normal;">
                        <% If eis.CheckPageAccess("Catalog") Then%>
                        <a href="<% = ProductLink(PopularItem("ProductGuid"), 0) %>">
                            <% = HTMLEncode(PopularItem("ProductName")) %>
                        </a>
                        <% Else%>
                        <% = HTMLEncode(PopularItem("ProductName")) %>
                        <% End If%>
                    </td>
                </tr>
                <% eis.NextRowAB()%>
                <% Next%>
                <% Else%>
                <tr class="TR0">
                    <td colspan="3">
                        <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources: Language, LABEL_NONE_AVAILABLE %>" />
                    </td>
                </tr>
                <% End If%>
            </table>
        </td>
    </tr>
</table>
