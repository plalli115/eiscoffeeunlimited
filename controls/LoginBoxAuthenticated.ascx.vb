Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

Partial Class LoginBoxAuthenticated
    Inherits ExpandIT.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Visible = Not CBoolEx(globals.User("Anonymous"))
        If Visible Then
            lbLoginStatus.Text = Replace(Resources.Language.LABEL_LOGGED_IN_AS_NAME, "%1", CStrEx(globals.User("ContactName")))
            'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
            If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
                Dim csrobj As New USCSR(globals)
                If csrobj.getSalesPersonGuid() <> "" Then
                csrobj.loginAuthenticated(lbLoginStatus)
            End If
            End If
            'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
            lbLoginStatus.PostBackUrl = "~/user.aspx"
        End If
    End Sub
End Class
