﻿<%@ Import Namespace="System.Configuration.ConfigurationManager" %>

<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ContactForm.ascx.vb" Inherits="ContactFormCtrl" %>

<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<%@ Register src="~/controls/Message.ascx" tagname="Message" tagprefix="uc1" %>

<asp:Panel ID="ContactFormPanel" runat="server" CssClass="userNewPanel">  
  <uc1:PageHeader ID="PageHeader1" runat="server" text="<%$Resources: Language,LABEL_CONTACT_FORM %>" EnableTheming="true" />
  <uc1:Message ID="Message1" runat="server" />
  <div class="userNewText">    
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
        ValidationGroup="AllValidators" />
    <table border="0" cellspacing="0" width="90%" >
      <tr class="TR1">
        <td class="TDR">
          <asp:Label ID="langCompanyName" runat="server" 
            Text="<%$Resources: Language,LABEL_USERDATA_COMPANY %>" />
          :
        </td>
        <td>
          <asp:TextBox ID="CompanyName" runat="server" CssClass="borderTextBox" 
            MaxLength="50" />
        </td>
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </td>
      </tr>
      <tr class="TR1">
        <td class="TDR">
          *<asp:Label ID="langContactName" runat="server" 
            Text="<%$Resources: Language,LABEL_USERDATA_CONTACT %>" />
          :
        </td>
        <td class="TDL">
          <asp:TextBox ID="ContactName" runat="server" CssClass="borderTextBox" 
            MaxLength="50" />
          <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
            ControlToValidate="ContactName" Display="Dynamic" 
            ErrorMessage="<%$Resources: Language,MESSAGE_REQUIRED_FIELD %>" 
            ValidationGroup="AllValidators">*</asp:RequiredFieldValidator>
        </td>
        <td>
        </td>
      </tr>
      <tr class="TR1">
        <td class="TDR">
          <asp:Label ID="langAddress1" runat="server" 
            Text="<%$Resources: Language,LABEL_USERDATA_ADDRESS_1 %>" />
          :
        </td>
        <td class="TDL">
          <asp:TextBox ID="Address1" runat="server" CssClass="borderTextBox" 
            MaxLength="50" />
        </td>
        <td>
        </td>
      </tr>
      <tr class="TR1">
        <td class="TDR">
          <asp:Label ID="langZipName" runat="server" 
            Text="<%$Resources: Language,LABEL_USERDATA_ZIPCODE %>" />
          ,
          <asp:Label ID="langCityName" runat="server" 
            Text="<%$Resources: Language,LABEL_USERDATA_CITY %>" />
          :
        </td>
        <td class="TDL">
          <asp:TextBox ID="Zip_CityName" runat="server" CssClass="borderTextBox" 
            MaxLength="50" />
        </td>
        <td>
        </td>
      </tr>
      <tr class="TR1">
        <td class="TDR">
          <asp:Label ID="Label8" runat="server" 
            Text="<%$Resources: Language,LABEL_USERDATA_COUNTRY %>" />
          :
        </td>
        <td class="TDL">
          <asp:Panel ID="dpdCountry" runat="server" CssClass="dpdBorder">
            <asp:DropDownList ID="CountryGuid" runat="server" 
              CssClass="borderDropDownList" Width="155px" />
          </asp:Panel>
        </td>
        <td>
        </td>
      </tr>
      <tr class="TR1">
        <td class="TDR">
          <asp:Label ID="langPhoneNo" runat="server" 
            Text="<%$Resources: Language,LABEL_USERDATA_PHONE %>" />
          :
        </td>
        <td class="TDL">
          <asp:TextBox ID="PhoneNo" runat="server" CssClass="borderTextBox" 
            MaxLength="30" />
        </td>
        <td>
        </td>
      </tr>
      <tr class="TR1">
        <td class="TDR">
          *<asp:Label ID="langEmailAddress" runat="server" 
            Text="<%$Resources: Language,LABEL_USERDATA_EMAIL %>" />
          :
        </td>
        <td class="TDL">
          <asp:TextBox ID="EmailAddress" runat="server" CssClass="borderTextBox" 
            MaxLength="50" />
          <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
            ControlToValidate="EmailAddress" Display="Dynamic" 
            ErrorMessage="<%$Resources: Language,MESSAGE_REQUIRED_FIELD %>" 
            ValidationGroup="AllValidators">*</asp:RequiredFieldValidator>
          <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
            ControlToValidate="EmailAddress" Display="Dynamic" 
            ErrorMessage="<%$Resources: Language,MESSAGE_EMAIL_ADDRESS_NOT_VALID %>" 
            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
            ValidationGroup="AllValidators">*</asp:RegularExpressionValidator>
        </td>
        <td>
        </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td class="TDR">
          <asp:Label ID="subjLabel" runat="server" Text="<%$Resources: Language,LABEL_MAIL_SUBJ %>"></asp:Label>
        </td>
        <td class="TDL" colspan="2" >
          <asp:TextBox ID="subjTextBox" runat="server" Width="100%"  CssClass="borderTextBox" ReadOnly="true"></asp:TextBox>
        </td>
        
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td  class="TDR">
          <asp:Label ID="langEmailContent" runat="server" Text="<%$Resources: Language,LABEL_MAIL_CONTENT %>"></asp:Label>              
        </td>
        <td colspan="2">
          <asp:TextBox ID="EmailContent" runat="server" Width="100%" Height="88px" 
            TextMode="MultiLine"></asp:TextBox>     
        </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr class="TR1">
        <td  class="TDR">
          <asp:Label ID="langSendTo" runat="server" Text="<%$Resources: Language,LABEL_CONTACT_SELECT_ADDRESS %>"></asp:Label>
        </td>
        <td  class="TDL">
          <asp:Panel ID="dpdSendTo" runat="server" CssClass="dpdBorder">
            <asp:DropDownList ID="SendTo" runat="server" CssClass="borderDropDownList" />
          </asp:Panel>
        </td>
        <td>
        </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td align="right" colspan="3">
          <input ID="BtnReset" runat="server" type="reset" class="AddButton" 
            value="<%$Resources: Language,ACTION_RESET %>" />
          <asp:Button ID="BtnSend" runat="server" CssClass="AddButton" 
            Text="<%$Resources: Language,ACTION_SEND %>"
            ValidationGroup="AllValidators" />
        </td>
      </tr>
    </table>
   
    
  </div>
</asp:Panel> 
