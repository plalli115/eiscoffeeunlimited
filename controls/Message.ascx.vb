﻿Imports ExpandIT
Imports ExpandIT31.ExpanditFramework.Infrastructure

Partial Class controls_Message
    Inherits ExpandIT.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim messages As PageMessages = globals.messages

        PanelOuter.Visible = False
        PanelActions.Visible = False
        PanelErrors.Visible = False
        PanelWarnings.Visible = False
        PanelMessages.Visible = False

        If messages.Actions.Count > 0 Or messages.Errors.Count > 0 Or messages.Warnings.Count > 0 Or messages.Messages.Count > 0 Then
            PanelOuter.Visible = True

            If messages.Actions.Count > 0 Then
                PanelActions.Visible = True
                dlActions.DataSource = messages.Actions
                dlActions.DataBind()
            End If

            If messages.Errors.Count > 0 Then
                PanelErrors.Visible = True
                dlErrors.DataSource = messages.Errors
                dlErrors.DataBind()
            End If

            If messages.Warnings.Count > 0 Then
                PanelWarnings.Visible = True
                dlWarnings.DataSource = messages.Warnings
                dlWarnings.DataBind()
            End If

            If messages.Messages.Count > 0 Then
                PanelMessages.Visible = True
                dlMessages.DataSource = messages.Messages
                dlMessages.DataBind()
            End If

        End If
    End Sub
End Class
