<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ProductBoxSmall.ascx.vb"
    Inherits="ProductBoxSmall" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<div class="ProductBoxSmall">
    <asp:FormView ID="fvProduct" runat="server" RowStyle-CssClass="ProductDisplayListRow"
        CssClass="ProductDisplayListForm">
        <ItemTemplate>
            <div class="ProductBoxSmall1">
                <table class="CenterImage" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:HyperLink ID="HyperLink4" runat="server" SkinID="ProductDisplayList_Thumbnail"
                                NavigateUrl='<%# ProductLink(Eval("Guid"), globals.groupguid) %>'>
                                <asp:Image ID="Image1" Style='<%# getStyle() %>' ImageUrl='<%# Eval("ThumbnailURL") %>'
                                    runat="server" />
                            </asp:HyperLink>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="ProductBoxSmall2">
                <div class="ProductBoxSmall_ProductName">
                    <asp:HyperLink ID="hlProductDisplayList_Name" Style="color: #323233; text-decoration: none;
                        font-weight: bold;" runat="server" SkinID="hlProductDisplayList_Name" Text='<%# Eval("Name") %>'
                        NavigateUrl='<%# ProductLink(Eval("Guid"), globals.groupguid) %>'></asp:HyperLink>
                </div>
                <div class="ProductTemplate_Price">
                    <asp:Panel runat="server" ID="pnlSpecialPrice" Visible='<%# CBoolEx(Eval("IsSpecial")) %>'>
                        <asp:Label ID="hlSpecialPrice" CssClass="SpecialPrice" runat="server" SkinID="ProductDisplayList_Price"
                            Text='<%# CurrencyFormatter.FormatCurrency(CDblEx(Eval("Price")), Session("UserCurrencyGuid").ToString) %>'></asp:Label>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlRegularPrice" Visible='<%# Not CBoolEx(Eval("IsSpecial")) %>'>
                        <asp:Label ID="hlRegularPrice" CssClass="RegularPrice" runat="server" SkinID="ProductDisplayList_Price"
                            Text='<%# CurrencyFormatter.FormatCurrency(CDblEx(Eval("Price")), Session("UserCurrencyGuid").ToString) %>'></asp:Label>
                    </asp:Panel>
                </div>
            </div>
        </ItemTemplate>
    </asp:FormView>
</div>
