Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT31.ExpanditFramework.Infrastructure

Partial Class MostPopular
    Inherits ExpandIT.UserControl

    Protected MostPopularItems As ExpandIT.ExpDictionary
    Protected m_userspecific As Boolean
    Protected m_maxitems As Integer = 4

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim products As ExpDictionary

        ' Load list of popular products
        Dim cachekey As String = "MOSTPOPULAR"
        products = CacheGet(cachekey)
        If products Is Nothing Then
            products = eis.GetMostPopularItems(10)
            eis.CatDefaultLoadProducts(products, False, False, True, New String() {"ProductName"}, New String() {"PICTURE2", "DESCRIPTION"}, Nothing)
            'CacheSetAggregated(cachekey, products, Split(AppSettings("GroupProductRelatedTablesForCaching") & "|ShopSalesLine", "|"))
            If Not CacheManager.CacheEnabled("ShopSalesLine") Then
                CacheManager.enableCaching("ShopSalesLine")
            End If
            CacheSet(cachekey, products, "ShopSalesLine")
        End If

        products = ExpDictionary.FilterDict(products, m_maxitems, ExpDictionary.FilterMethods.Random)
        If products Is Nothing Then products = New ExpDictionary
        dlProducts.DataSource = products
        dlProducts.DataBind()
    End Sub

    Public Property MaxItems() As Integer
        Get
            Return m_maxitems
        End Get
        Set(ByVal value As Integer)
            m_maxitems = value
        End Set
    End Property

    Public Property UserSpecific() As Boolean
        Get
            Return m_userspecific
        End Get
        Set(ByVal value As Boolean)
            m_userspecific = value
        End Set
    End Property

End Class
