'JA2010071601 - RATINGS AND REVIEWS - START
Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT.GlobalsClass

Partial Class Rating
    Inherits ExpandIT.UserControl

    Protected showTitle As String = ""
    Protected showReviewHeader As String = ""
    Protected showReview As String = ""
    Protected setShowName As String = ""
    Protected redirectpage As String = ""

    Protected Overloads Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        Dim thePath As String
        If HttpContext.Current.Request.ServerVariables("QUERY_STRING") <> "" Then
            thePath = HttpContext.Current.Request.ServerVariables("SCRIPT_NAME") & "?" & HttpContext.Current.Request.ServerVariables("QUERY_STRING")
        Else
            thePath = HttpContext.Current.Request.ServerVariables("SCRIPT_NAME")
        End If
        redirectpage = VRoot & "/user_login.aspx?returl=" & HttpContext.Current.Server.UrlEncode(CStrEx(thePath))

        Dim sql As String = "SELECT * FROM ProductReviews WHERE UserGuid=" & SafeString(globals.User("UserGuid")) & " AND ProductGuid=" & SafeString(Request("ProductGuid"))
        Dim dict As ExpDictionary = SQL2Dicts(sql)
        If Not dict Is Nothing And Not dict Is DBNull.Value And dict.Count > 0 Then
            showReviewHeader = Resources.Language.LABEL_RATING_MODIFY_REVIEW
            showTitle = CStrEx(dict.Item("1")("ReviewTitle"))
            showReview = CStrEx(HTMLEncode(dict.Item("1")("Review")))
            showReview = Mid(showReview, 1, 1300)
            If CBoolEx(dict.Item("1")("ShowName")) Then
                setShowName = "checked=""checked"""
            End If
        Else
            showReviewHeader = Resources.Language.LABEL_RATING_WRITE_REVIEW
        End If

        Dim ScriptManager1 As ScriptManager = ScriptManager.GetCurrent(Me.Page)
        ScriptManager1.RegisterAsyncPostBackControl(Me.btnHiddenForUpdate)

    End Sub


    Protected Sub btnFilterForbiddenWords_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim titleToFilter As String = CStrEx(Request("TitleReview")).ToUpper & " "
        Dim comment As String = CStrEx(Request("CommentReview")).ToUpper & " "
        Dim myCheckBox As String = CStrEx(Request("CheckBox1"))
        Dim sql As String = "SELECT WordNo, WordText FROM RatingForbiddenWords ORDER BY WordNo"
        Dim dictWords As ExpDictionary = SQL2Dicts(sql)
        Dim count As Integer = 0

        If Not dictWords Is Nothing And Not dictWords Is DBNull.Value Then
            If dictWords.Count > 0 Then
                For Each item As ExpDictionary In dictWords.Values
                    If comment.Contains(" " & CStrEx(item("WordText")).ToUpper & " ") Then
                        count = count + 1
                        comment = comment.Replace(CStrEx(item("WordText")).ToUpper, "*****")
                    End If
                    If titleToFilter.Contains(" " & CStrEx(item("WordText")).ToUpper & " ") Then
                        count = count + 1
                        titleToFilter = titleToFilter.Replace(CStrEx(item("WordText")).ToUpper, "*****")
                    End If
                Next
            End If
        End If
        comment = comment.Replace(vbCrLf, "\n")
        titleToFilter = titleToFilter.Replace(vbCrLf, "\n")
        comment = comment.Replace("'", " ")
        titleToFilter = titleToFilter.Replace("'", " ")

        If count > 0 Then
            Dim script As String = "setTitleComment('" & titleToFilter.ToLower & "' , '" & comment.ToLower & "', '1','" & myCheckBox & "');"
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script1", script, True)

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script2", "animatedcollapse.init();", True)

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script3", "animatedcollapse.show('RateItem');", True)

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script4", "closeLoading();", True)

        Else

            Dim script As String = "setTitleComment('" & titleToFilter.ToLower & "' , '" & comment.ToLower & "', '0','" & myCheckBox & "');"
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script1", script, True)

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script2", "animatedcollapse.init();", True)

            'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script3", "animatedcollapse.show('RateItem');", True)

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script4", "closeLoading();", True)

        End If





    End Sub

    Protected Sub btnHiddenForUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Session("filterBy") = ""

        Me.UpdatePanel1.Update()

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script3", "animatedcollapse.init();", True)

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script4", "closeLoading();", True)

    End Sub
    Protected Sub btnHiddenForUpdateReview_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Session("filterBy") = ""

        Me.UpdatePanel1.Update()

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script2", "animatedcollapse.init();", True)

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script3", "setTimeout('animatedcollapse.show(\'ReadReviews\')', 1000 );", True)

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script4", "closeLoading();", True)


    End Sub




    Protected Function getStartRate()
        Dim numberStar As Integer = 0
        Dim sql As String = ""
        sql = "SELECT Rating FROM ProductRatings WHERE UserGuid=" & SafeString(globals.User("UserGuid")) & " AND ProductGuid=" & SafeString(Request("ProductGuid"))
        numberStar = CIntEx(getSingleValueDB(sql))

        Return CIntEx(numberStar)
    End Function

    Protected Function getCurrentRate()
        Dim totalRating As Integer = 0
        Dim sql As String = ""
        Dim ratings As New ExpDictionary
        Dim average As Integer = 0
        Dim sum As Integer = 0

        sql = "SELECT Rating FROM ProductRatings WHERE ProductGuid=" & SafeString(Request("ProductGuid"))
        ratings = SQL2Dicts(sql, "Rating")

        If Not ratings Is Nothing Then
            If Not ratings Is DBNull.Value Then
                If ratings.Count > 0 Then
                    For Each item As ExpDictionary In ratings.Values
                        sum = sum + CIntEx(item("Rating"))
                    Next
                    average = sum / ratings.Count
                End If
            End If
        End If

        Dim strImage As String = ""
        strImage = strImage & "<table cellpadding=""0"" cellspacing=""0""><tr><td>"
        strImage = strImage & "<img style=""margin:0px;"" alt="""" src=""" & VRoot & "/script/stars/" & CIntEx(average) & ".PNG"" />"

        sql = "SELECT Count(Rating) FROM ProductRatings WHERE ProductGuid=" & SafeString(Request("ProductGuid"))
        totalRating = CIntEx(getSingleValueDB(sql))

        strImage = strImage & "</td><td style=""vertical-align:middle;"">&nbsp;(" & totalRating & " " & Resources.Language.LABEL_RATINGS & ")</td></tr></table>"

        Return strImage

    End Function


    Protected Function getPercentages()
        Dim percentage5 As Integer = 0
        Dim percentage4 As Integer = 0
        Dim percentage3 As Integer = 0
        Dim percentage2 As Integer = 0
        Dim percentage1 As Integer = 0

        Dim strPercentage As String = ""
        Dim sql As String = ""
        Dim total As Integer = 0

        sql = "SELECT Count(Rating) FROM ProductRatings WHERE ProductGuid=" & SafeString(Request("ProductGuid")) & " AND Rating=5"
        percentage5 = CIntEx(getSingleValueDB(sql))
        sql = "SELECT Count(Rating) FROM ProductRatings WHERE ProductGuid=" & SafeString(Request("ProductGuid")) & " AND Rating=4"
        percentage4 = CIntEx(getSingleValueDB(sql))
        sql = "SELECT Count(Rating) FROM ProductRatings WHERE ProductGuid=" & SafeString(Request("ProductGuid")) & " AND Rating=3"
        percentage3 = CIntEx(getSingleValueDB(sql))
        sql = "SELECT Count(Rating) FROM ProductRatings WHERE ProductGuid=" & SafeString(Request("ProductGuid")) & " AND Rating=2"
        percentage2 = CIntEx(getSingleValueDB(sql))
        sql = "SELECT Count(Rating) FROM ProductRatings WHERE ProductGuid=" & SafeString(Request("ProductGuid")) & " AND Rating=1"
        percentage1 = CIntEx(getSingleValueDB(sql))
        total = percentage1 + percentage2 + percentage3 + percentage4 + percentage5
        If total = 0 Then
            total = 1
        End If

        strPercentage = strPercentage & "<table style=""margin-top:15px;"" cellpadding=""1"" cellspacing=""1"">"
        strPercentage = strPercentage & "<tr><td align=""center"" style=""padding-bottom:5px;"" colspan=""3""><b><a style=""cursor:pointer;text-decoration:none;"" onclick=""ClearSession();"">" & getnumberofReviews() & " " & Resources.Language.LABEL_CUSTOMERS_REVIEWS & "</a></b></td></tr>"
        If getnumberofReviewsByRating(5) <> 0 Then
            strPercentage = strPercentage & "<tr><td><a style=""cursor:pointer;text-decoration:underline;"" onclick=""SetFilterBy('5');"">5 " & Resources.Language.LABEL_STAR & ":</a></td><td align=""left"" bgcolor=""#eeeecc"" title=""" & CInt((percentage5 * 100) / total) & "%"" style=""width:100px;"">"
        Else
            strPercentage = strPercentage & "<tr><td>5 " & Resources.Language.LABEL_STAR & ":</td><td align=""left"" bgcolor=""#eeeecc"" title=""" & CInt((percentage5 * 100) / total) & "%"" style=""width:100px;"">"
        End If
        strPercentage = strPercentage & "<div><img style=""background-color:#FFCC66;margin-left:0px;height:13px;width:" & CInt((percentage5 * 100) / total) & "%;"" src=""" & VRoot & "/images/p.gif"" /></div></td><td align=""right"">&nbsp;(&nbsp;" & percentage5 & "&nbsp;)</td></tr>"
        If getnumberofReviewsByRating(4) <> 0 Then
            strPercentage = strPercentage & "<tr><td><a style=""cursor:pointer;text-decoration:underline;"" onclick=""SetFilterBy('4');"">4 " & Resources.Language.LABEL_STAR & ":</a></td><td align=""left"" bgcolor=""#eeeecc"" title=""" & CInt((percentage4 * 100) / total) & "%"" style=""width:100px;"">"
        Else
            strPercentage = strPercentage & "<tr><td>4 " & Resources.Language.LABEL_STAR & ":</td><td align=""left"" bgcolor=""#eeeecc"" title=""" & CInt((percentage4 * 100) / total) & "%"" style=""width:100px;"">"
        End If
        strPercentage = strPercentage & "<div><img style=""background-color:#FFCC66;margin-left:0px;height:13px;width:" & CInt((percentage4 * 100) / total) & "%;"" src=""" & VRoot & "/images/p.gif"" /></div></td><td align=""right"">&nbsp;(&nbsp;" & percentage4 & "&nbsp;)</td></tr>"
        If getnumberofReviewsByRating(3) <> 0 Then
            strPercentage = strPercentage & "<tr><td><a style=""cursor:pointer;text-decoration:underline;"" onclick=""SetFilterBy('3');"">3 " & Resources.Language.LABEL_STAR & ":</a></td><td align=""left"" bgcolor=""#eeeecc"" title=""" & CInt((percentage3 * 100) / total) & "%"" style=""width:100px;"">"
        Else
            strPercentage = strPercentage & "<tr><td>3 " & Resources.Language.LABEL_STAR & ":</td><td align=""left"" bgcolor=""#eeeecc"" title=""" & CInt((percentage3 * 100) / total) & "%"" style=""width:100px;"">"
        End If
        strPercentage = strPercentage & "<div><img style=""background-color:#FFCC66;margin-left:0px;height:13px;width:" & CInt((percentage3 * 100) / total) & "%;"" src=""" & VRoot & "/images/p.gif"" /></div></td><td align=""right"">&nbsp;(&nbsp;" & percentage3 & "&nbsp;)</td></tr>"
        If getnumberofReviewsByRating(2) <> 0 Then
            strPercentage = strPercentage & "<tr><td><a style=""cursor:pointer;text-decoration:underline;"" onclick=""SetFilterBy('2');"">2 " & Resources.Language.LABEL_STAR & ":</a></td><td align=""left"" bgcolor=""#eeeecc"" title=""" & CInt((percentage2 * 100) / total) & "%"" style=""width:100px;"">"
        Else
            strPercentage = strPercentage & "<tr><td>2 " & Resources.Language.LABEL_STAR & ":</td><td align=""left"" bgcolor=""#eeeecc"" title=""" & CInt((percentage2 * 100) / total) & "%"" style=""width:100px;"">"
        End If
        strPercentage = strPercentage & "<div><img style=""background-color:#FFCC66;margin-left:0px;height:13px;width:" & CInt((percentage2 * 100) / total) & "%;"" src=""" & VRoot & "/images/p.gif"" /></div></td><td align=""right"">&nbsp;(&nbsp;" & percentage2 & "&nbsp;)</td></tr>"
        If getnumberofReviewsByRating(1) <> 0 Then
            strPercentage = strPercentage & "<tr><td><a style=""cursor:pointer;text-decoration:underline;"" onclick=""SetFilterBy('1');"">1 " & Resources.Language.LABEL_STAR & ":</a></td><td align=""left"" bgcolor=""#eeeecc"" title=""" & CInt((percentage1 * 100) / total) & "%"" style=""width:100px;"">"
        Else
            strPercentage = strPercentage & "<tr><td>1 " & Resources.Language.LABEL_STAR & ":</td><td align=""left"" bgcolor=""#eeeecc"" title=""" & CInt((percentage1 * 100) / total) & "%"" style=""width:100px;"">"
        End If
        strPercentage = strPercentage & "<div><img style=""background-color:#FFCC66;margin-left:0px;height:13px;width:" & CInt((percentage1 * 100) / total) & "%;"" src=""" & VRoot & "/images/p.gif"" /></div></td><td align=""right"">&nbsp;(&nbsp;" & percentage1 & "&nbsp;)</td></tr>"
        strPercentage = strPercentage & "</table>"


        Return strPercentage
    End Function

    Protected Function getReviews()
        Dim strReviews As String = ""
        Dim sql As String = ""
        Dim reviewsDict As ExpDictionary
        If CStrEx(Session("filterBy")) <> "" Then
            sql = "SELECT Pg.Rating,Ut.ContactName,Pw.ReviewTitle,Pw.Review,Pw.ShowName,Pw.CreatedOne,Pw.ProductGuid FROM ProductRatings As Pg INNER JOIN ProductReviews As Pw ON Pg.ProductGuid = Pw.ProductGuid INNER JOIN UserTable As Ut ON Pw.UserGuid=Ut.UserGuid WHERE Pg.RatingGuid=Pw.RatingGuid AND Pw.ProductGuid=" & SafeString(Request("ProductGuid")) & " AND Pg.Rating=" & CIntEx(Session("filterBy")) & " ORDER BY Pw.CreatedOne DESC"
        Else
            sql = "SELECT Pg.Rating,Ut.ContactName,Pw.ReviewTitle,Pw.Review,Pw.ShowName,Pw.CreatedOne,Pw.ProductGuid FROM ProductRatings As Pg INNER JOIN ProductReviews As Pw ON Pg.ProductGuid = Pw.ProductGuid INNER JOIN UserTable As Ut ON Pw.UserGuid=Ut.UserGuid WHERE Pg.RatingGuid=Pw.RatingGuid AND Pw.ProductGuid=" & SafeString(Request("ProductGuid")) & " ORDER BY Pw.CreatedOne DESC"
        End If
        reviewsDict = SQL2Dicts(sql)
        If Not reviewsDict Is Nothing Then
            If reviewsDict.Count > 0 Then
                If reviewsDict.Count = 1 Then
                    strReviews = strReviews & "<table style=""height:90px;padding-top:5px;width:100%;"" cellpadding=""1"" cellspacing=""3"">"
                Else
                    strReviews = strReviews & "<table style=""width:100%;"" cellpadding=""1"" cellspacing=""3"">"
                End If
                strReviews = strReviews & "<tr><td colspan=""3""><a style=""cursor:pointer;text-decoration:none;"" onclick=""animatedcollapse.hide('ReadReviews');""><b>" & Resources.Language.LABEL_CLOSE_REVIEWS & "</b></a></td></tr>"
                If CStrEx(Session("filterBy")) <> "" Then
                    strReviews = strReviews & "<tr><td colspan=""3""><br /><a style=""cursor:pointer;text-decoration:none;"" onclick=""SetFilterBy('0');""><b>� </b> " & Resources.Language.LABEL_SEE_ALL_STARS & "</a></td></tr>"
                End If
                For Each item As ExpDictionary In reviewsDict.Values
                    Dim reviewFomatText As String = item("Review").Replace(vbCrLf, "<br />")
                    strReviews = strReviews & "<tr><td colspan=""3""><table style=""border-top:1px dashed gray;width:100%;margin-top:10px;""><tr>"
                    strReviews = strReviews & "<td style=""width:110px;padding-top:10px;""><img style=""margin:0px;"" alt="""" src=""" & VRoot & "/script/stars/" & CStrEx(item("Rating")) & ".PNG"" /></td>"
                    strReviews = strReviews & "<td align=""left"" style=""width:292px;Vertical-align:middle; font-weight:bold; padding-left:5px;padding-top:10px;"">" & CStrEx(item("ReviewTitle")) & "</td></tr>"
                    strReviews = strReviews & "<tr><td style=""width:110px;""><img style=""width:110px;height:10px;"" src=""" & VRoot & "/images/p.gif"" /></td>"
                    If CBoolEx(item("ShowName")) Then
                        strReviews = strReviews & "<td align=""left"" style=""width:292px;Vertical-align:middle;padding-left:5px;padding-bottom:10px;"">" & Resources.Language.LABEL_RATING_BY & " " & CStrEx(item("ContactName")) & " (" & CDateEx(item("CreatedOne")).ToString("MMM dd, yyyy") & ")</td>"
                    Else
                        strReviews = strReviews & "<td align=""left"" style=""width:292px;Vertical-align:middle; padding-left:5px;padding-bottom:10px;"">(" & CDateEx(item("CreatedOne")).ToString("MMM dd, yyyy") & ")</td>"
                    End If
                    strReviews = strReviews & "</tr><tr><td style=""width:412px;padding-left:5px;"" colspan=""2"">" & reviewFomatText & "</td>"
                    strReviews = strReviews & "</tr></table></td></tr>"
                Next
                strReviews = strReviews & "</table>"
            Else
                strReviews = strReviews & "<table style=""width:100%; height:90px;"" cellpadding=""0"" cellspacing=""0"">"
                strReviews = strReviews & "<tr><td><a style=""cursor:pointer;text-decoration:none;"" onclick=""animatedcollapse.hide('ReadReviews');""><b>" & Resources.Language.LABEL_CLOSE_REVIEWS & "</b></a><br /><br /><b>" & Resources.Language.LABEL_NO_REVIEWS & "</b></td></tr></table>"
            End If
        Else
            strReviews = strReviews & "<table style=""width:100%; height:90px;"" cellpadding=""0"" cellspacing=""0"">"
            strReviews = strReviews & "<tr><td><a style=""cursor:pointer;text-decoration:none;"" onclick=""animatedcollapse.hide('ReadReviews');""><b>" & Resources.Language.LABEL_CLOSE_REVIEWS & "</b></a><br /><br /><b>" & Resources.Language.LABEL_NO_REVIEWS & "</b></td></tr></table>"
        End If

        Return strReviews

    End Function

    Protected Function getnumberofReviews()
        Dim sql As String = ""
        Dim reviewsDict As ExpDictionary
        sql = "SELECT * FROM ProductReviews WHERE ProductGuid=" & SafeString(Request("ProductGuid"))
        reviewsDict = SQL2Dicts(sql, "RatingGuid")
        Return CIntEx(reviewsDict.Count)
    End Function

    Protected Function getnumberofReviewsByRating(ByVal number As Integer)
        Dim sql As String = ""
        Dim numberReviews As Integer = 0
        sql = "SELECT COUNT (Pw.ProductGuid) FROM ProductRatings As Pg INNER JOIN ProductReviews As Pw ON Pg.ProductGuid = Pw.ProductGuid WHERE Pg.RatingGuid=Pw.RatingGuid AND Pw.ProductGuid=" & SafeString(Request("ProductGuid")) & " AND Pg.Rating=" & number
        numberReviews = CIntEx(getSingleValueDB(sql))
        Return numberReviews
    End Function


    Protected Sub btnHiddenForFilterByStar_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim filterByStar As Integer = CIntEx(Request("filterByStar"))

        If filterByStar = 0 Then
            Session("filterBy") = ""
        Else
            Session("filterBy") = filterByStar
        End If

        Me.UpdatePanel1.Update()

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script2", "animatedcollapse.init();", True)

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script3", "animatedcollapse.show('ReadReviews');", True)

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script4", "closeLoading();", True)

    End Sub

    Protected Sub btnHiddenForClearSession_Click(ByVal sender As Object, ByVal e As System.EventArgs)


        If CStrEx(Session("filterBy")) <> "" Then
            Session("filterBy") = ""
            Me.UpdatePanel1.Update()
        End If

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script2", "animatedcollapse.init();", True)

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script3", "animatedcollapse.show('ReadReviews');", True)

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script4", "closeLoading();", True)

    End Sub


End Class
'JA2010071601 - RATINGS AND REVIEWS - END