<%--JA2010071601 - RATINGS AND REVIEWS - START--%>
<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Rating.ascx.vb" Inherits="Rating" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%--<asp:ScriptManager ID="ScriptManager1" runat="server" />--%>
<link type="text/css" rel="stylesheet" href="<%=Vroot %>/script/stars/jquery.ratings.css" />
<asp:Panel ID="Panel1" runat="server">
    <table cellpadding="0" cellspacing="0" width="98%" style="text-align: left;">
        <tr>
            <td style="vertical-align: middle; font-size: 14px; font-weight: bold; width: 222px;">
                <asp:Label ID="Literal1" runat="server" Text="<%$ Resources: Language, LABEL_RATING_RATE_ITEM %>" />
            </td>
            <td style="vertical-align: middle; width: 225px;">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <div style="vertical-align: middle;" id="example-2">
                            </div>
                            <asp:Button ID="btnHiddenForUpdate" runat="server" Style="display: none;" OnClick="btnHiddenForUpdate_Click" />
                            <input id="verticalSlideMode" type="hidden" value="<%=CIntEx(AppSettings("VERTICAL_SLIDE_DISPLAY_MODE"))%>" />
                        </td>
                        <td style="vertical-align: middle;">
                            <span>
                                <asp:Label ID="Literal3" runat="server" Text="<%$ Resources: Language, LABEL_RATING_YOUR_RATING %>" />
                            </span><span id="example-rating-2">
                                <%=getStartRate()%>
                            </span>
                            <input id="startRate" value="<%= getStartRate() %>" type="hidden" />
                            <input id="vroot" value="<%= Vroot %>" type="hidden" />
                            <input id="ProductGuid" value="<%= Request("ProductGuid") %>" type="hidden" />
                            <input id="newRate" name="newRate" type="hidden" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="Server">
    <ContentTemplate>
        <table cellpadding="0" cellspacing="0" width="98%" style="text-align: left;">
            <tr>
                <td colspan="3" style="width: 431px; vertical-align: middle; padding-bottom: 10px;
                    padding-top: 10px;">
                    <div style="vertical-align: middle;">
                        <%If (CBoolEx(globals.User("B2B")) Or CBoolEx(globals.User("B2C"))) Then%>
                        <a onmouseover="javascript:CheckRate()" onclick="javascript:animatedcollapse.toggle('RateItem')"
                            style="cursor: pointer;">
                            <%me.WriteReview.Text=showReviewHeader %>
                            <asp:Label ID="WriteReview" runat="server"/>
                        </a>
                        <span id="ShowRateError" style="color: Red; display: none;">
                            <%=Resources.Language.LABEL_RATING_RATE%>
                        </span>
                        <asp:Button ID="btnHiddenForUpdateReview" runat="server" Style="display: none;" OnClick="btnHiddenForUpdateReview_Click" />
                        <%Else%>
                        <a href="<%=redirectpage %>" style="cursor: pointer; text-decoration: none;">
                            <asp:Label ID="Label1" runat="server" Text="<%$ Resources: Language, LABEL_RATINGS_REVIEWS_LOGIN %>" />
                        </a>
                        <%End If%>
                    </div>
                    <div id="RateItem" style="display: none; width: 100%; background-color: white; position: relative;
                        padding-top: 5px; padding-bottom: 5px;">
                        <table cellpadding="0" cellspacing="0" style="width: 100%;">
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                        <tr>
                                            <td colspan="2">
                                                <asp:Label ID="Literal2" runat="server" Text="<%$ Resources: Language, LABEL_RATING_ENTER_TITLE %>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-bottom: 5px; width: 75%;">
                                                <input id="Title" name="TitleReview" value="<%= showTitle %>" style="width: 90%;" type="text" />
                                            </td>
                                            <td style="vertical-align: middle; padding-bottom: 3px;">
                                                <input id="CheckBox1" name="CheckBox1" <%= setShowName %> class="ShowNameRatings" style="margin-top: -3px;"
                                                    type="checkbox" />
                                                <asp:Label ID="Literal4" runat="server" Text="<%$ Resources: Language, LABEL_RATING_SHOW_NAME %>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:Label ID="Literal5" runat="server" Text="<%$ Resources: Language, LABEL_RATING_ENTER_REVIEW %>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="padding-bottom: 5px;">
                                                <textarea onkeydown="remainingText(1300);" onkeyup="remainingText(1300);" name="CommentReview" id="Comment"
                                                    style="width: 98%; height: 100px;" cols="20" rows="2"><%= showReview %></textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="vertical-align: middle; text-align: right; font-weight: bold;">
                                                <% Dim newValue As Integer = 0%>
                                                <%If 1300 - showReview.Length > 0 Then%>
                                                <% newValue = 1300 - showReview.Length%>
                                                <%End If%>
                                                (<input style="width: 30px; border: 0px; text-align: center;" id="DisplayCounterField"
                                                    readonly="readonly" type="text" value="<%=newValue %>" />)
                                                <asp:Label ID="Label2" runat="server" Text="<%$ Resources: Language, LABEL_REVIEWS_LETTERS_LEFT %>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="vertical-align: middle;">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <input id="PostReview" onclick="javascript:forbiddenWordsFilter()" type="button" value="Post" />
                                                            <asp:Button ID="btnFilterForbiddenWords" runat="server" Style="display: none;" OnClick="btnFilterForbiddenWords_Click" />
                                                        </td>
                                                        <td style="vertical-align: middle; padding-left: 5px;">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <span id="rateError" style="color: Red; display: none;">
                                                                            <%=Resources.Language.LABEL_RATING_RATE%>
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <span id="titleError" visible="false" style="color: Red; display: none;">
                                                                            <%=Resources.Language.LABEL_RATING_TITLE%>
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <span id="commentError" visible="false" style="color: Red; display: none;">
                                                                            <%=Resources.Language.LABEL_RATING_COMMENT%>
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <span id="msgNoError" visible="false" style="color: Green; display: none;">
                                                                            <%=Resources.Language.LABEL_RATING_NO_ERROR%>
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <span id="msgError" visible="false" style="color: Red; display: none;">
                                                                            <%=Resources.Language.LABEL_RATING_ERROR%>
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <span id="msgForbiddenWords" visible="false" style="color: Red; display: none;">
                                                                            <%=Resources.Language.LABEL_FORBIDDEN_WORD_FILTER%>
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="vertical-align: middle; font-size: 14px; font-weight: bold; width: 222px;">
                    <asp:Label ID="Literal6" runat="server" Text="<%$ Resources: Language, LABEL_RATING_AVERAGE %>" />
                </td>
                <td colspan="2" style="width: 225px;">
                    <%= getCurrentRate() %>
                </td>
            </tr>
            <tr>
                <%--<td style="vertical-align: top; padding-top: 10px; width: 222px;">
                    <div style="vertical-align: middle;">
                        <a onclick="animatedcollapse.toggle('ReadReviews');" style="cursor: pointer;">
                            <% If CStrEx(Session("filterBy")) <> "" Then%>
                            <%="<b>" & Resources.Language.LABEL_READ_REVIEWS & "</b>"%> (<%=Resources.Language.LABEL_SHOWING & " <b>" & CStrEx(Session("filterBy")) & "</b>" & Resources.Language.LABEL_REVIEWS%>)
                            <%Else %>
                            <%="<b>" & Resources.Language.LABEL_READ_REVIEWS & "</b>"%> (<%= Resources.Language.LABEL_SHOWING & " " & Resources.Language.LABEL_ALL_REVIEWS %>)
                            <%End If %>
                        </a>
                    </div>
                </td>--%>
                <td align="center" colspan="3">
                    <%= getPercentages() %>
                    <asp:Button ID="btnHiddenForFilterByStar" runat="server" Style="display: none;" OnClick="btnHiddenForFilterByStar_Click" />
                    <input id="filterByStar" name="filterByStar" type="hidden" />
                    <asp:Button ID="btnHiddenForClearSession" runat="server" Style="display: none;" OnClick="btnHiddenForClearSession_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div id="ReadReviews" style="display: none; background-color: white; position: relative;
                        width: 98%; top: -90px;">
                        <%= getReviews() %>
                    </div>
                </td>
            </tr>
        </table>

        <script type="text/javascript">
            function setTitleComment(titleText, commentText, status, checkBoxValue){
                
                document.getElementById('Comment').value  = commentText;
                document.getElementById('Title').value  = titleText;
                if(status == '1'){
                    document.getElementById('msgForbiddenWords').style.display='block';                                    
                }else{
                    document.getElementById('msgForbiddenWords').style.display='none'; 
                    validateForm(checkBoxValue); 
                }
            }
        
            function CheckRate(){
                var newRate= document.getElementById('newRate').value;
                var startRate= document.getElementById('startRate').value;
                <%If CIntEx(AppSettings("VERTICAL_SLIDE_DISPLAY_MODE")) = 1 Then %>
                var label=document.getElementById('ctl00_ctl00_cphRoot_cphSubMaster_fwProductForm_VerticalSlide1_ctl04_WriteReview');
                <%ElseIf CIntEx(AppSettings("VERTICAL_SLIDE_DISPLAY_MODE")) = 2 Then %>
                 var label=document.getElementById('ctl00_ctl00_cphRoot_cphSubMaster_fwProductForm_VerticalSlide1_ctl09_WriteReview');
                <%End If %>
                var rateErrorLabel=document.getElementById('ShowRateError');
                if(newRate == '' & startRate == '0'){
                    rateErrorLabel.style.display = 'block';  
                    label.style.display='none'; 
                }else{
                    rateErrorLabel.style.display = 'none'; 
                    label.style.display='block';
                }
            }
            
            function remainingText(maxCount) {
                var FieldName=document.getElementById('Comment');
                var DisplayCounterField=document.getElementById('DisplayCounterField');
                if (FieldName.value.length > maxCount){
                    FieldName.value = FieldName.value.substring(0, maxCount);
                }else{
                    DisplayCounterField.value = maxCount - FieldName.value.length;
                }
            }      
            
            function forbiddenWordsFilter(){
                animatedcollapse.hide('ReadReviews');
                
                <%If CIntEx(AppSettings("VERTICAL_SLIDE_DISPLAY_MODE")) = 1 Then %>
                document.getElementById('ctl00_ctl00_cphRoot_cphSubMaster_fwProductForm_VerticalSlide1_ctl04_btnFilterForbiddenWords').click();
                <%ElseIf CIntEx(AppSettings("VERTICAL_SLIDE_DISPLAY_MODE")) = 2 Then %>
                 document.getElementById('ctl00_ctl00_cphRoot_cphSubMaster_fwProductForm_VerticalSlide1_ctl09_btnFilterForbiddenWords').click();
                <%End If %>
                
                //Loading wait
                var divLoading=document.getElementById('Loading');
                var loadingImg=document.getElementById('LoadingImg');
                loadingImg.style.paddingTop='70px';
                divLoading.style.top='50px';
                divLoading.style.left='5px';
                divLoading.style.width='450px';
                divLoading.style.height='355px';
                divLoading.style.display='block';
                //Loading wait
            }
        
            function validateForm(checkBoxValue){
                var title= document.getElementById('Title').value;
                var CheckBox= checkBoxValue;
                if(CheckBox=='on'){
                    CheckBox=1;
                }else{
                    CheckBox=0;
                }
                var Comment= escape(document.getElementById('Comment').value);
                var newRate= document.getElementById('newRate').value;
                var startRate= document.getElementById('startRate').value;
                var rate;
                var titleError= document.getElementById('titleError'); 
                var rateError= document.getElementById('rateError'); 
                var commentError= document.getElementById('commentError');
                var msgError= document.getElementById('msgError'); 
                var msgNoError= document.getElementById('msgNoError'); 
                    
                    
                if(newRate == ''){
                    if(startRate == '0'){
                        rate='';
                    }else{
                        rate=startRate;
                    }
                }else{
                    rate=newRate;        
                }
                
                if(title == '' || Comment == '' || rate == ''){ 
                    msgError.style.display = 'none';
                    msgNoError.style.display = 'none';         
                    if(rate == ''){
                        rateError.style.display = 'block';   
                    }else{
                        rateError.style.display = 'none'; 
                    }
                    if(Comment == ''){    
                        document.getElementById('Comment').focus();        
                        commentError.style.display = 'block';   
                    }else{
                        commentError.style.display = 'none'; 
                    } 
                    if(title == ''){
                        document.getElementById('Title').focus()
                        titleError.style.display = 'block';   
                    }else{
                        titleError.style.display = 'none'; 
                    }                
                }else{
                    rateError.style.display = 'none'; 
                    commentError.style.display = 'none'; 
                    titleError.style.display = 'none';
                    
                    var ProductGuid= document.getElementById('ProductGuid').value;
                    var vroot= document.getElementById('vroot').value;
                    var url= vroot + "/ratings.aspx?SetForm=1&Title=" + title + "&Comment=" + Comment + "&CheckBox1=" + CheckBox + "&newRate=" + rate + "&ProductGuid=" + ProductGuid;
                    var xhReq = new XMLHttpRequest();
                    //xhReq.open("GET", url , false);
                    //Loading wait
                    var divLoading=document.getElementById('Loading');
                    var loadingImg=document.getElementById('LoadingImg');
                    loadingImg.style.paddingTop='70px';
                    divLoading.style.top='50px';
                    divLoading.style.left='5px';
                    divLoading.style.width='450px';
                    divLoading.style.height='190px';
                    divLoading.style.display='block';
                    //Loading wait
                    xhReq.open("GET", url += (url.match(/\?/) == null ? "?" : "&") + (new Date()).getTime(), false);
                    xhReq.send(null);
                    
                    if(xhReq.status == 200){
                        msgNoError.style.display = 'none';                 
                        msgNoError.style.display = 'block'; 
                    }else{
                        msgNoError.style.display = 'none'; 
                        msgError.style.display = 'block'; 
                    }
                    
                                    
                    <%If CIntEx(AppSettings("VERTICAL_SLIDE_DISPLAY_MODE")) = 1 Then %>
                    setTimeout('document.getElementById(\'ctl00_ctl00_cphRoot_cphSubMaster_fwProductForm_VerticalSlide1_ctl04_btnHiddenForUpdateReview\').click()', 1000);
                    <%ElseIf CIntEx(AppSettings("VERTICAL_SLIDE_DISPLAY_MODE")) = 2 Then %>
                     setTimeout('document.getElementById(\'ctl00_ctl00_cphRoot_cphSubMaster_fwProductForm_VerticalSlide1_ctl09_btnHiddenForUpdateReview\').click()', 1000);
                    <%End If %>
                
                
                    
                    
                    //setTimeout('animatedcollapse.hide(\'RateItem\')', 1000 );  
                    //setTimeout('animatedcollapse.show(\'ReadReviews\')', 2000 );  
                }
            }
            
            function SetFilterBy(star){
                animatedcollapse.hide('RateItem');
                //Loading wait
                var divLoading=document.getElementById('Loading');
                var loadingImg=document.getElementById('LoadingImg');
                loadingImg.style.paddingTop='35px';
                divLoading.style.top='150px';
                divLoading.style.left='5px';
                divLoading.style.width='440px';
                divLoading.style.height='180px';
                divLoading.style.display='block';
                //Loading wait
                document.getElementById('filterByStar').value=star;  
                
                <%If CIntEx(AppSettings("VERTICAL_SLIDE_DISPLAY_MODE")) = 1 Then %>
                document.getElementById('ctl00_ctl00_cphRoot_cphSubMaster_fwProductForm_VerticalSlide1_ctl04_btnHiddenForFilterByStar').click();
                <%ElseIf CIntEx(AppSettings("VERTICAL_SLIDE_DISPLAY_MODE")) = 2 Then %>
                 document.getElementById('ctl00_ctl00_cphRoot_cphSubMaster_fwProductForm_VerticalSlide1_ctl09_btnHiddenForFilterByStar').click();
                <%End If %>
                
            }
            
            function ClearSession(){ 
                //animatedcollapse.hide('RateItem');
                //Loading wait
                var divLoading=document.getElementById('Loading');
                var loadingImg=document.getElementById('LoadingImg');
                loadingImg.style.paddingTop='35px';
                divLoading.style.top='150px';
                divLoading.style.left='135px';
                divLoading.style.width='200px';
                divLoading.style.height='90px';
                divLoading.style.display='block';
                //Loading wait
                <%If CIntEx(AppSettings("VERTICAL_SLIDE_DISPLAY_MODE")) = 1 Then %>
                document.getElementById('ctl00_ctl00_cphRoot_cphSubMaster_fwProductForm_VerticalSlide1_ctl04_btnHiddenForClearSession').click();
                <%ElseIf CIntEx(AppSettings("VERTICAL_SLIDE_DISPLAY_MODE")) = 2 Then %>
                document.getElementById('ctl00_ctl00_cphRoot_cphSubMaster_fwProductForm_VerticalSlide1_ctl09_btnHiddenForClearSession').click();
                <%End If %>
                
            }
            //Loading wait
            function closeLoading(){ 
                var divLoading=document.getElementById('Loading');
                divLoading.style.display='none';
            }
            //Loading wait
            
        </script>

    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnHiddenForUpdate" />
    </Triggers>
</asp:UpdatePanel>
<div id="Loading" style="background-color:White; text-align:center; position:absolute; display:none;">
<img id="LoadingImg" alt="" src="<%=Vroot %>/script/stars/loadRating.gif" />
</div>

<%--JA2010071601 - RATINGS AND REVIEWS - END--%>
