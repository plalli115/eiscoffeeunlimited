<%--AM2010051802 - WHAT'S NEW - Start--%>
<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="WhatsNew.ascx.vb" Inherits="WhatsNew" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<asp:DataList ID="dlProducts" HorizontalAlign="Center" CssClass="WhatsNewBorder" runat="server">
    <HeaderTemplate>
        <table cellspacing="0" cellpadding="0" style="width:90%;">
            <tr>
                <td class="WhatsNewHeader" align="center">
                    <%=Resources.Language.LABEL_WHATS_NEW%>
                </td>
            </tr>
        </table>
    </HeaderTemplate>
    <ItemTemplate>
        <table style="width: 90%; margin-bottom: 10px; margin-left:5px; margin-right:5px;" cellpadding="0" cellspacing="0">
            <tr>
                <td class="WhatsNewItemStyle">
                    <span><a href="<%# getLink(Eval("value")("ProductGuid")) %>">
                        <%#Eval("value")("ProductName")%>
                    </a></span>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table cellspacing="0" cellpadding="0" border="0" style="text-align: center;">
                        <tr>
                            <td>
                                <a style="text-decoration: none;" href="<%# getLink(Eval("value")("ProductGuid"))%>">
                                    <img alt="" style="<%# getStyle(Eval("value")("PICTURE2")) %>" src=" <%# getSrc(Eval("value")("PICTURE2"))  %>" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="WhatsNewPrice">
                    <asp:Label runat="server" CssClass="RegularPrice" Visible='<%# eis.CheckPageAccess("Prices") %>' ID="lblPrice" Text='<%# Eval("value")("ListPrice")%>'></asp:Label>
                </td> 
            </tr>
        </table>
    </ItemTemplate>
</asp:DataList>
<%--AM2010051802 - WHAT'S NEW - End--%>
