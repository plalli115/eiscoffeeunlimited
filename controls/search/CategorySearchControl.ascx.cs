﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ExpandIT;
using System.Data;
using System.Text;
using System.Collections.Specialized;
using ExpandIT31.ExpanditFramework.Util;

namespace ExpandIT
{

    /// <summary>
    /// 1. Get the top group and add the content to the first dropdownlist
    /// 2. On ItemIndexChange add new dropdownlist and fill it with the next level of groups
    /// 3. As long as there are sub groups, add new dropdownlist on ItemIndexChange on parent dropdownlist
    /// 4. Add a textbox to enter search string at the current dropdownlist level.
    /// 5. Perform a search at the current catalog level (category search).
    /// </summary>
    public partial class CategorySearchControl : ExpandIT.UserControl
    {

        private Dictionary<string, DropDownList> dplDict;
        //ExpandIT US - USE CSC - Start
        //JA2010030901 - PERFORMANCE -Start
        private string m_ListAvailableItemsSQL;
        //JA2010030901 - PERFORMANCE -End
        //ExpandIT US - USE CSC - End

        //AM2010071901 - ENTERPRISE CSC - Start
        private string m_CatalogNo;

        public string CatalogNo
        {
            get
            {
                return m_CatalogNo;
            }
            set
            {
                m_CatalogNo=value;
            }
        }
        //AM2010071901 - ENTERPRISE CSC - End

        //ExpandIT US - USE CSC - Start
        //JA2010030901 - PERFORMANCE -Start
        public string ListAvailableItemsSQL
        {
            get
            {
                return m_ListAvailableItemsSQL;
            }
            set
            {
                m_ListAvailableItemsSQL = value;
            }
        }
        //JA2010030901 - PERFORMANCE -End
        //ExpandIT US - USE CSC - End

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (Session["ddlDict"] != null)
            {
                dplDict = (Dictionary<string, DropDownList>)Session["ddlDict"];
                foreach (DropDownList ddl in dplDict.Values)
                {
                    ddl.SelectedIndexChanged += new EventHandler(ddl_SelectedIndexChanged);
                    UpdatePanel2.ContentTemplateContainer.Controls.Add(ddl);
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                string topLevelGroups = buildTopLevelGroupsSql();
                List<string> topGroups = ExpandITLib.SQL2List(topLevelGroups);
                string sql = BuildSql(topGroups);

                int levels = 0;
                string sqlLevels = buildMaxLevelSql();
                object rawLevels = ExpandITLib.getSingleValueDB(sqlLevels);
                if (ExpandITLib.DBNull2Nothing(rawLevels) != null)
                {
                    levels = (int)rawLevels;
                }
                UpdatePanel2.ContentTemplateContainer.Controls.Clear();
                if (levels != 0)
                {
                    Dictionary<string, DropDownList> ddlDict = new Dictionary<string, DropDownList>();
                    for (int i = 0; i < levels; i++)
                    {
                        DropDownList ddlist = new DropDownList();
                        ddlist.Style.Add("margin-right", "10px");
                        ddlist.Style.Add("margin-top", "5px");
                        ddlist.Style.Add("margin-bottom", "5px");
                        ddlist.ID = "ddl_" + i;
                        ddlist.AutoPostBack = true;
                        ddlist.Items.Add(new ListItem(Resources.Language.DROPDOWN_TEXT_SEARCH_ALL, "1"));
                        ddlist.Visible = false;
                        ddlist.SelectedIndexChanged += new EventHandler(ddl_SelectedIndexChanged);
                        UpdatePanel2.ContentTemplateContainer.Controls.Add(ddlist);
                        ddlDict.Add(ddlist.ID, ddlist);
                    }
                    Session["ddlDict"] = ddlDict;
                }

                DataTable dtb = new DataTable();
                dtb = ExpandITLib.SQL2DataTable(sql);
                DropDownList1.Items.Add(new ListItem(Resources.Language.DROPDOWN_TEXT_SEARCH_ALL, "1234567890"));
                foreach (DataRow dr in dtb.Rows)
                {
                    DropDownList1.Items.Add(new ListItem(dr[1].ToString(), dr[0].ToString()));
                }
                DropDownList1.SelectedIndex = 0;
                Session["groupValue"] = null;
            }
        }

        protected void ddl_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList dl = (DropDownList)sender;
            string text = dl.SelectedItem.Text;
            string sql = "";
            //JA2010030901 - PERFORMANCE -Start
            //AM2010071901 - ENTERPRISE CSC - Start
            //ExpandIT US - USE CSC - Start
            if (Utilities.CBoolEx(System.Configuration.ConfigurationManager.AppSettings["EXPANDIT_US_USE_CSC"]))
            {
                sql = "EXEC EISEBuildSearchSubCategoriesSQL @strCatalog=" + ExpandITLib.SafeString(CatalogNo, true) + ", @strCSCAvailableItemsSQL=" + ExpandITLib.SafeString(ListAvailableItemsSQL, true) + ", @intParentGuid=" + dl.SelectedValue;
            }
            else
            {
                sql = "SELECT * FROM GroupTable WHERE ParentGuid = " + ExpandITLib.SafeString(dl.SelectedValue, true);
            }
            //ExpandIT US - USE CSC - End
            //AM2010071901 - ENTERPRISE CSC - End
            //JA2010030901 - PERFORMANCE -End
            
            List<string> theList = ExpandITLib.SQL2List(sql);

            if (theList.Count > 0)
            {
                string sql2 = BuildSql(theList);
                DataTable dt = ExpandITLib.SQL2DataTable(sql2);

                if (dt.Rows.Count > 0)
                {
                    if (dplDict.Count != int.Parse(dl.ID.Substring(dl.ID.Length - 1)) + 1)
                    {
                        Dictionary<string, DropDownList>.Enumerator enm = dplDict.GetEnumerator();
                        enm.MoveNext();
                        while (!enm.Current.Key.Equals(dl.ID))
                        {
                            enm.MoveNext();
                        }
                        try
                        {
                            while (enm.MoveNext())
                            {
                                DropDownList dl2 = enm.Current.Value;
                                dl2.Items.Clear();
                                dl2.Visible = false;
                            }
                        }
                        catch { }
                    }

                    if (text != "Search all ...")
                    {
                        Dictionary<string, DropDownList>.Enumerator enm = dplDict.GetEnumerator();
                        enm.MoveNext();
                        while (!enm.Current.Key.Equals(dl.ID))
                        {
                            enm.MoveNext();
                        }
                        enm.MoveNext();
                        DropDownList dl2 = enm.Current.Value;

                        dl2.Items.Clear();

                        System.Threading.Thread.Sleep(500);

                        dl2.Visible = true;

                        dl2.Items.Add(new ListItem(Resources.Language.DROPDOWN_TEXT_SEARCH_ALL, dl.SelectedValue));
                        foreach (DataRow dr in dt.Rows)
                        {
                            dl2.Items.Add(new ListItem(dr[1].ToString(), dr[0].ToString()));
                        }
                    }
                }

                string groups = buildAllGroupsSql(ExpandITLib.SafeString(dl.SelectedValue, true));
                List<string> tl = ExpandITLib.SQL2List(groups);
                /////
                StringBuilder propRef = new StringBuilder();
                for (int i = 0; i < tl.Count - 1; i++)
                {
                    propRef.Append(ExpandITLib.SafeString(tl[i], true) + ",");
                }
                propRef.Append(ExpandITLib.SafeString(tl[tl.Count - 1], true));
                /////
                Session["groupValue"] = propRef.ToString() + ',' + ExpandITLib.SafeString(dl.SelectedValue, true);
            }
            else
            {
                Dictionary<string, DropDownList>.Enumerator enm = dplDict.GetEnumerator();
                enm.MoveNext();
                while (!enm.Current.Key.Equals(dl.ID))
                {
                    enm.MoveNext();
                }
                try
                {
                    while (enm.MoveNext())
                    {
                        DropDownList dl2 = enm.Current.Value;
                        dl2.Items.Clear();
                        dl2.Visible = false;
                    }
                }
                catch { }
                Session["groupValue"] = ExpandITLib.SafeString(dl.SelectedValue, true);
            }
        }


        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //JA2010030901 - PERFORMANCE -Start
            //AM2010071901 - ENTERPRISE CSC - Start
            string sql = "";
            //ExpandIT US - USE CSC - Start
            if (Utilities.CBoolEx(System.Configuration.ConfigurationManager.AppSettings["EXPANDIT_US_USE_CSC"]))
            {
                sql = "EXEC EISEBuildSearchSubCategoriesSQL @strCatalog=" + ExpandITLib.SafeString(CatalogNo, true) + ", @strCSCAvailableItemsSQL=" + ExpandITLib.SafeString(ListAvailableItemsSQL, true) + ", @intParentGuid=" + DropDownList1.SelectedValue;
            }
            else
            {
                sql = "SELECT * FROM GroupTable WHERE ParentGuid = " + ExpandITLib.SafeString(DropDownList1.SelectedValue, true);
            }
            //ExpandIT US - USE CSC - End
            //AM2010071901 - ENTERPRISE CSC - End
            //JA2010030901 - PERFORMANCE -End
            List<string> theList = ExpandITLib.SQL2List(sql);
            if (dplDict != null)
            {
                foreach (DropDownList dpl in dplDict.Values)
                {
                    dpl.Items.Clear();
                    dpl.Visible = false;
                }
            }
            if (theList.Count > 0)
            {
                if (DropDownList1.SelectedIndex > 0)
                {
                    string sql2 = BuildSql(theList);
                    DataTable dt = ExpandITLib.SQL2DataTable(sql2);
                    System.Threading.Thread.Sleep(200);
                    DropDownList ddl = dplDict["ddl_0"];
                    ddl.Items.Clear();
                    ddl.Visible = true;
                    ddl.Items.Add(new ListItem(Resources.Language.DROPDOWN_TEXT_SEARCH_ALL, "1"));
                    foreach (DataRow dr in dt.Rows)
                    {
                        ddl.Items.Add(new ListItem(dr[1].ToString(), dr[0].ToString()));
                    }
                    string groups = buildAllGroupsSql(ExpandITLib.SafeString(DropDownList1.SelectedValue, true));
                    List<string> tl = ExpandITLib.SQL2List(groups);
                    tl.Add(DropDownList1.SelectedValue);
                    StringBuilder propRef = new StringBuilder();
                    for (int i = 0; i < tl.Count - 1; i++)
                    {
                        propRef.Append(ExpandITLib.SafeString(tl[i], true) + ",");
                    }
                    propRef.Append(ExpandITLib.SafeString(tl[tl.Count - 1], true));

                    Session["groupValue"] = propRef.ToString();
                }
                else
                {
                    Session.Remove("groupValue");
                }
            }
            else
            {
                if (DropDownList1.SelectedIndex > 0)
                {
                    Session["groupValue"] = DropDownList1.SelectedValue;
                }
                else
                {
                    Session.Remove("groupValue");
                }
            }
        }

        #region SQL Queries

        protected string buildMaxLevelSql()
        {
            StringBuilder Sb = new StringBuilder();
            //JA2010030901 - PERFORMANCE -Start
            //ExpandIT US - USE CSC - Start
            if (Utilities.CBoolEx(System.Configuration.ConfigurationManager.AppSettings["EXPANDIT_US_USE_CSC"]))
            {
                return "EXEC EISEBuildSearchCategoriesSQL @strCatalog=" + ExpandITLib.SafeString(CatalogNo, true) + ", @strCSCAvailableItemsSQL=" + ExpandITLib.SafeString(ListAvailableItemsSQL, true) + ", @intParentGuid=0, @boolMaxLevel=1";
            }
            else
            {
                Sb.AppendLine("WITH temp_GroupTable (ParentGuid, GroupGuid, iteration) AS ");
                Sb.AppendLine("(");
                Sb.AppendLine("SELECT ParentGuid, GroupGuid, 0 ");
                Sb.AppendLine("FROM GroupTable WHERE ParentGuid = 0 ");
                //AM2010071901 - ENTERPRISE CSC - Start
                Sb.AppendLine("AND CatalogNo=" + ExpandITLib.SafeString(CatalogNo, true));
                //AM2010071901 - ENTERPRISE CSC - End
                Sb.AppendLine("UNION ALL ");
                Sb.AppendLine("SELECT a.ParentGuid, b.GroupGuid, a.iteration + 1 ");
                Sb.AppendLine("FROM temp_GroupTable AS a, GroupTable AS b ");
                Sb.AppendLine("WHERE a.GroupGuid = b.ParentGuid ");
                Sb.AppendLine(")");
                Sb.AppendLine("SELECT MAX(iteration) ");
                Sb.AppendLine("FROM temp_GroupTable ");
                return Sb.ToString();
            }
            //ExpandIT US - USE CSC - End
            //JA2010030901 - PERFORMANCE -End
        }

        protected string buildAllGroupsSql(string parentGuid)
        {
            StringBuilder Sb = new StringBuilder();
            //JA2010030901 - PERFORMANCE -Start
            //ExpandIT US - USE CSC - Start
            if (Utilities.CBoolEx(System.Configuration.ConfigurationManager.AppSettings["EXPANDIT_US_USE_CSC"]))
            {
                return "EXEC EISEBuildSearchCategoriesSQL @strCatalog=" + ExpandITLib.SafeString(CatalogNo, true) + ", @strCSCAvailableItemsSQL=" + ExpandITLib.SafeString(ListAvailableItemsSQL, true) + ", @intParentGuid=" + parentGuid + ", @boolMaxLevel=0";
            }
            else
            {
                Sb.AppendLine("WITH temp_GroupTable (ParentGuid, GroupGuid, iteration) AS ");
                Sb.AppendLine("(");
                Sb.AppendLine("SELECT ParentGuid, GroupGuid, 0 ");
                Sb.AppendLine("FROM GroupTable WHERE ParentGuid = " + parentGuid);
                Sb.AppendLine(" UNION ALL ");
                Sb.AppendLine("SELECT a.ParentGuid, b.GroupGuid, a.iteration + 1 ");
                Sb.AppendLine("FROM temp_GroupTable AS a, GroupTable AS b ");
                Sb.AppendLine("WHERE a.GroupGuid = b.ParentGuid ");
                Sb.AppendLine(")");
                Sb.AppendLine("SELECT GroupGuid ");
                Sb.AppendLine("FROM temp_GroupTable ");
                return Sb.ToString();
            }
            //ExpandIT US - USE CSC - End
            //JA2010030901 - PERFORMANCE -End
        }

        protected string buildTopLevelGroupsSql()
        {
            StringBuilder Sb = new StringBuilder();
            //JA2010030901 - PERFORMANCE -Start
            //ExpandIT US - USE CSC - Start
            if (Utilities.CBoolEx(System.Configuration.ConfigurationManager.AppSettings["EXPANDIT_US_USE_CSC"]))
            {
                return "EXEC EISEBuildSearchTopLevelGroupsSQL @strCatalog=" + ExpandITLib.SafeString(CatalogNo, true) + ", @strCSCAvailableItemsSQL=" + ExpandITLib.SafeString(ListAvailableItemsSQL, true);
            }
            else
            {
                Sb.AppendLine("SELECT GroupGuid ");
                Sb.AppendLine("FROM GroupTable ");
                Sb.AppendLine("WHERE ParentGuid IN(");
                Sb.AppendLine("SELECT ParentGuid ");
                Sb.AppendLine("FROM GroupTable ");
                Sb.AppendLine("EXCEPT ");
                Sb.AppendLine("SELECT GroupGuid ");
                Sb.AppendLine("FROM GroupTable) ");
                //AM2010071901 - ENTERPRISE CSC - Start
                Sb.AppendLine("AND CatalogNo=" + ExpandITLib.SafeString(CatalogNo,true));
                //AM2010071901 - ENTERPRISE CSC - End
                return Sb.ToString();
            }
            //ExpandIT US - USE CSC - End
            //JA2010030901 - PERFORMANCE -End
        }

        protected string BuildSql(List<string> PropOwnerRefGuid)
        {
            string sql = "SELECT DISTINCT pv.PropOwnerRefGuid, p.PropTransText, p.PropLangGuid " +
                            "FROM (PropVal pv " +
                            "JOIN PropTrans pt " +
                            "ON pv.PropValGuid = pt.PropValGuid " +
                            "AND pv.PropOwnerRefGuid IN({0}) " +
                            "AND pv.PropGuid = 'NAME') " +
                            "LEFT JOIN PropTrans p " +
                            "ON p.PropValGuid = pv.PropValGuid " +
                            "AND p.PropLangGuid = " + ExpandITLib.SafeString(Session["LanguageGuid"], true);

            StringBuilder propRef = new StringBuilder();
            for (int i = 0; i < PropOwnerRefGuid.Count - 1; i++)
            {
                propRef.Append(ExpandITLib.SafeString(PropOwnerRefGuid[i], true) + ",");
            }
            propRef.Append(ExpandITLib.SafeString(PropOwnerRefGuid[PropOwnerRefGuid.Count - 1], true));

            return string.Format(sql, propRef.ToString());
        }

        #endregion
    }
}