﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CategorySearchControl.ascx.cs"
    Inherits="ExpandIT.CategorySearchControl" %>
<%--<asp:ScriptManager ID="MyScriptManager" runat="server" />--%>
<br />
<br />
<table border="0">
    <tr>
        <td>
            <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
            </asp:DropDownList>
        </td>
        <td>
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="5">
                <ProgressTemplate>
                    <span style="color: Red"><%= Resources.Language.ACTION_UPDATING%>.....</span></ProgressTemplate>
            </asp:UpdateProgress>
        </td>
    </tr>
    <tr>
        <td>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="DropDownList1" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>
        </td>
        <td>
        </td>
    </tr>
</table>
