Imports System.Configuration.ConfigurationManager
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

<Themeable(True)> _
Partial Class QuickSearch
    Inherits ExpandIT.UserControl

    Private m_showcaption As Boolean = True

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        ImageButton1.PostBackUrl = VRoot & "/Search.aspx"

    End Sub

    Public Property ShowCaption() As Boolean
        Get
            Return m_showcaption
        End Get
        Set(ByVal value As Boolean)
            m_showcaption = value
        End Set
    End Property

    Public Property ImageURL() As String
        Get
            Return ImageButton1.ImageUrl
        End Get
        Set(ByVal value As String)
            ImageButton1.ImageUrl = ThemeURL(value)
        End Set
    End Property


    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        lblSearch.Visible = m_showcaption
    End Sub

    Protected Function searchText()
        Dim text As String = "<input class=""QuickSearch"" style="""" type=""text"" name=""searchstring"" size=""20"" maxlength=""20"""
        text = text & " value = " & """" & Resources.Language.LABEL_SEARCH & """"
        text = text & " onfocus = ""this.value = (this.value=='" & Resources.Language.LABEL_SEARCH & "')? '' : this.value;"""
        text = text & " onblur=""this.value = (this.value=='')? '" & Resources.Language.LABEL_SEARCH & "' : this.value;""/>"

        Return text

    End Function
End Class
