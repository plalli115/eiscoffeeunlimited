﻿Imports System.Configuration.ConfigurationManager
Imports System.Data
Imports ExpandIT
Imports ExpandIT.ExpandITLib
Imports ExpandIT.EISClass

Partial Class NamedNotesDropDown
    Inherits ExpandIT.UserControl
    
    Private m_SelectedValue As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then          
          Dim sql As String
          sql = String.Format("SELECT DISTINCT ListName FROM Notes WHERE UserGuid = {0}", SafeString(globals.User("UserGuid")))
          Dim dt As DataTable = SQL2DataTable(sql)
          ddlNamedNotes.Items.Add(New ListItem(String.Format("({0})", Resources.Language.LABEL_NO_NAMED), ""))
          For Each nameRow As DataRow In dt.Rows             
              Dim name As String =  nameRow.Item(0).ToString()
              If (name <> "")                
                ddlNamedNotes.Items.Add(New ListItem (name, name))
              End If             
          Next
          ddlNamedNotes.SelectedValue = m_SelectedValue 
        End If
        
    End Sub
    
    Public Property SelectedValue As String
      Get
        Return ddlNamedNotes.SelectedValue.ToString()
      End Get
      Set(ByVal value As String)
        m_SelectedValue = value
      End Set
    End Property
    
    Protected Sub ddlNamedNotes_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlNamedNotes.SelectedIndexChanged
        Dim url As String = String.Format("~/_notes.aspx?action=show&NNN={0}", HTMLEncode(ddlNamedNotes.SelectedValue.ToString()))
        Response.Redirect(url)
    End Sub
    
End Class
