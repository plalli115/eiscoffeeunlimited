Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

Partial Class PriceNoUom
    Inherits ExpandIT.UserControl


    Protected m_price As Double
    Protected m_class As String
    Protected m_style As String
    Protected m_guid As String

    Public Property Price() As Double
        Get
            Return m_price
        End Get
        Set(ByVal value As Double)
            m_price = value
        End Set
    End Property

    Public Property CssClassText() As String
        Get
            Return m_class
        End Get
        Set(ByVal value As String)
            m_class = value
        End Set
    End Property

    Public Property styleText() As String
        Get
            Return m_style
        End Get
        Set(ByVal value As String)
            m_style = value
        End Set
    End Property

    Public Property Guid() As String
        Get
            Return m_guid
        End Get
        Set(ByVal value As String)
            m_guid = value
        End Set
    End Property


    Private Sub Page_PreRender(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'AM2011060201 - PRICES BREAKDOWN - Start
        Me.PricesBreakDown1.ProductGuid = Guid()
        'AM2011060201 - PRICES BREAKDOWN - End
    End Sub


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
       
    End Sub

   

End Class