<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ShowPromoCodes.ascx.vb"
    Inherits="ShowPromoCodes" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%  If Not groupsPrdFinal Is Nothing And Not groupsPrdFinal Is DBNull.Value Then%>
<%  If groupsPrdFinal.Count > 0 Then%>
<table class="PromoStyles" cellpadding="0" cellspacing="0" style="width: 100%; margin-top: 20px;">
    <% 
        For Each promo As ExpDictionary In groupsPrdFinal.Values%>
    <tr>
        <td align="center" style="padding-right: 10px; vertical-align:middle;">
            <a href="<%=Vroot %>/PromoCodesDetail.aspx?promotionCode=<%=promo("PromotionCode")%>&promotionGuid=<%=promo("PromotionGuid")%>">
                <img style="<%= CStrEx(eis.ThumbnailURLStyleMethod(promo("PROMO_SMALL_IMAGE"), 100, 100)) %>"
                    src="<%=promo("PROMO_SMALL_IMAGE") %>" />
            </a>
        </td>
        <td>
            <table cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td style="padding-bottom: 10px;">
                        <a href="<%=Vroot %>/PromoCodesDetail.aspx?promotionCode=<%=promo("PromotionCode")%>&promotionGuid=<%=promo("PromotionGuid")%>">
                            <asp:Label ID="Label1" runat="server" Style="font-weight: bold;" Text="<%$ Resources: Language, LABEL_PROMOTION_CODE %>"></asp:Label>:
                            <%=promo("PromotionCode") %>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td style="padding-bottom: 10px;">
                        <a href="<%=Vroot %>/PromoCodesDetail.aspx?promotionCode=<%=promo("PromotionCode")%>&promotionGuid=<%=promo("PromotionGuid")%>">
                            <asp:Label ID="Label2" runat="server" Style="font-weight: bold;" Text="<%$ Resources: Language, LABEL_PROMOTION_NAME %>"></asp:Label>:
                            <%=promo("PROMO_NAME") %>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td style="padding-bottom: 10px;">
                        <a href="<%=Vroot %>/PromoCodesDetail.aspx?promotionCode=<%=promo("PromotionCode")%>&promotionGuid=<%=promo("PromotionGuid")%>">
                            <%=promo("PROMO_HTML_SUB") %>
                        </a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="height: 15px;">
        </td>
    </tr>
    <% Next%>
</table>
<%  End If%>
<%  End If%>
