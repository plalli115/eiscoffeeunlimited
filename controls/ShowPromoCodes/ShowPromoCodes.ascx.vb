'<!--JA2010060101 - ALERTS - Start-->
Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

Partial Class ShowPromoCodes
    Inherits ExpandIT.UserControl

    Protected dictPromo As ExpDictionary
    Protected groupsPrdFinal As New ExpDictionary


    Public Property PromoDict() As ExpDictionary
        Get
            Return dictPromo
        End Get
        Set(ByVal value As ExpDictionary)
            dictPromo = value
        End Set
    End Property

    Protected Overloads Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        getPromotionCodes()
    End Sub


    Protected Sub getPromotionCodes()
        Dim sqlPropGrp As String = ""
        Dim groupsProperties As New ExpDictionary
        Dim sortedproducts As New System.Collections.Generic.SortedDictionary(Of String, Object)

        If Not dictPromo Is Nothing And Not dictPromo Is DBNull.Value Then
            If dictPromo.Count > 0 Then
                'Load the information about the groups
                For Each item As ExpDictionary In dictPromo.Values
                    Dim groupsPrd As New ExpDictionary
                    sqlPropGrp = "SELECT PropOwnerRefGuid, PropGuid, PropLangGuid, PropTransText FROM PropVal INNER JOIN PropTrans" & _
                                           " ON PropVal.PropValGuid=PropTrans.PropValGuid" & _
                                           " WHERE PropOwnerTypeGuid='PRM'" & _
                                           " AND (PropLangGuid IS NULL OR PropLangGuid =" & SafeString(HttpContext.Current.Session("LanguageGuid").ToString) & ")" & _
                                           " AND (PropGuid='PROMO_HTML_SUB' OR PropGuid='PROMO_NAME' OR PropGuid='PROMO_SMALL_IMAGE')" & _
                                           " AND PropOwnerRefGuid=" & SafeString(item("PromotionGuid"))
                    groupsProperties = SQL2Dicts(sqlPropGrp)
                    If Not groupsProperties Is Nothing Then
                        If groupsProperties.Count > 0 Then
                            For Each item2 As ExpDictionary In groupsProperties.Values
                                groupsPrd.Add(item2("PropGuid"), item2("PropTransText"))
                            Next
                            groupsPrd.Add("PromotionCode", item("PromotionCode"))
                            groupsPrd.Add("PromotionGuid", item("PromotionGuid"))
                            groupsPrdFinal.Add(item("PromotionGuid"), groupsPrd)
                        End If
                    End If
                Next
                ' Sort the list of favorites
                'If Not groupsPrdFinal Is Nothing And Not groupsPrdFinal Is DBNull.Value Then
                '    For Each groups2 As ExpDictionary In groupsPrdFinal.Values
                '        sortedproducts.Add(groups2("AlertNo"), groups2)
                '    Next
                'End If
            End If
        End If

    End Sub

End Class
'<!--JA2010060101 - ALERTS - End-->