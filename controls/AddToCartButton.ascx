<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddToCartButton.ascx.vb" Inherits="AddToCartButton" %>
<asp:Button ID="btnAdd" runat="server" CssClass="AddToCartButton"  Text="Add to Cart" />
<asp:ImageButton ID="imgbtnAdd" CssClass="AddToCartImage" runat="server" />   
<asp:LinkButton ID="linkbtnAdd" CssClass="AddToCartLink" runat="server">Add to Cart</asp:LinkButton>