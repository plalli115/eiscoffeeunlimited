﻿<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MailTemplateDropDown.ascx.vb" Inherits="MailTemplateDropDown" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<div class="MailTemplateDropDown">
    <div class="dpdBorder">
        <asp:DropDownList ID="ddlMailTemplate" runat="server" CssClass="borderDropDownList" AutoPostBack="true">
        </asp:DropDownList>
    </div>
</div>