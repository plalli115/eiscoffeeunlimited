Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Web


Partial Class PromoBox
    Inherits ExpandIT.UserControl

    'AM2010031501 - ORDER RENDER - Start
    Private m_EditMode As Boolean
    Private m_OrderDict As ExpDictionary
    'AM2010031501 - ORDER RENDER - End

    'AM2010031501 - ORDER RENDER - Start
    Public Property EditMode() As Boolean
        Get
            Return CBoolEx(m_EditMode)
        End Get
        Set(ByVal value As Boolean)
            m_EditMode = CBoolEx(value)
        End Set
    End Property

    Public Property OrderDict() As ExpDictionary
        Get
            Return m_OrderDict
        End Get
        Set(ByVal value As ExpDictionary)
            m_OrderDict = value
        End Set
    End Property

    Public ReadOnly Property PromotionCode() As String
        Get
            Return CStrEx(promoCode.Text)
        End Get
    End Property

    'AM2010031501 - ORDER RENDER - End

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If OrderDict Is Nothing Then
            OrderDict = eis.LoadOrderDictionary(globals.User)
        End If
        If EditMode Then
            pnlEdit.Visible = True
            If Not Page.IsPostBack Then
                If NaV(OrderDict("PromotionCode")) Then
                    Me.promoCode.Text = ""
                Else
                    Me.promoCode.Text = OrderDict("PromotionCode")
                End If
            End If
        Else
            pnlNoEdit.Visible = True

            If Not OrderDict("PromotionCode") Is DBNull.Value Then
                If AppSettings("SHOW_PROMOTION_CODE") And (CStrEx(OrderDict("PromotionCode")) <> "") Then
                    pnlPromoCode.Visible = True
                    pnlPromoTitle.Visible = True
                End If
            End If
            If Not OrderDict("PromotionName") Is DBNull.Value Then
                If AppSettings("SHOW_PROMOTION_NAME") And (OrderDict("PromotionName") <> "") Then
                    pnlPromoName.Visible = True
                    pnlPromoTitle.Visible = True
                End If
            End If
            If Not OrderDict("PromotionDescription") Is DBNull.Value Then
                If AppSettings("SHOW_PROMOTION_DESCRIPTION") And (OrderDict("PromotionDescription") <> "") Then
                    pnlPromoLongDescription.Visible = True
                    pnlPromoTitle.Visible = True
                End If
            End If
        End If
    End Sub

    Protected Sub lblApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblApply.Click
        If OrderDict Is Nothing Then OrderDict = eis.LoadOrderDictionary(globals.User)
        Dim sql As String
        Dim dictPromoHeader As ExpDictionary
        Dim promoObj As New promotions(globals)

        If Me.promoCode.Text = "" Then
            OrderDict("PromotionCode") = Me.promoCode.Text
            OrderDict("PromotionName") = ""
            OrderDict("PromotionDescription") = ""
        Else
            'sql = "SELECT * FROM Promotion WHERE PromotionCode = " & SafeString(promoCode.Text) & _
            '      " AND (([PromotionStartDate] <= " & SafeDatetime(DateValue(Now)) & ")" & _
            '      " AND ([PromotionEndDate] >= " & SafeDatetime(DateValue(Now)) & "))" & _
            '      " AND (([NoOfOpportunities] > (SELECT COUNT(*) As Total FROM ShopSalesHeader WHERE " & _
            '      "[PromotionCode] = " & SafeString(promoCode.Text) & _
            '      " AND (([HeaderDate] >= (SELECT [PromotionStartDate] FROM Promotion WHERE " & _
            '      "[PromotionCode] = " & SafeString(promoCode.Text) & _
            '      " AND (([PromotionStartDate] <= " & SafeDatetime(DateValue(Now)) & ")" & _
            '      " AND ([PromotionEndDate] >= " & SafeDatetime(DateValue(Now)) & ")))))" & _
            '      " AND ([HeaderDate] <= (SELECT [PromotionEndDate] FROM Promotion WHERE " & _
            '      "[PromotionCode] = " & SafeString(promoCode.Text) & _
            '      " AND (([PromotionStartDate] <= " & SafeDatetime(DateValue(Now)) & ")" & _
            '      " AND ([PromotionEndDate] >= " & SafeDatetime(DateValue(Now)) & "))))))" & _
            '      " OR ([NoOfOpportunities] = 0))"

            'dictPromoHeader = Sql2Dictionary(sql)
            OrderDict("PromotionCode") = Me.promoCode.Text
            dictPromoHeader = promoObj.NFObtainPromotionHeader(OrderDict, globals.Context)

            If Not (dictPromoHeader Is Nothing) Then
                OrderDict("PromotionName") = dictPromoHeader("PromotionName")
                OrderDict("PromotionDescription") = dictPromoHeader("PromotionDescription")
                globals.messages.Errors.Remove(Resources.Language.LABEL_INVALID_PROMO_CODE)
            Else
                globals.messages.Errors.Add(Resources.Language.LABEL_INVALID_PROMO_CODE)
                Me.promoCode.Text = ""
                OrderDict("PromotionCode") = Me.promoCode.Text
                OrderDict("PromotionName") = DBNull.Value
                OrderDict("PromotionDescription") = DBNull.Value
            End If
        End If
        OrderDict("IsCalculated") = False
        globals.OrderDict = OrderDict
        eis.CalculateOrder(OrderDict)
        eis.SaveOrderDictionary(globals.OrderDict)
    End Sub

End Class