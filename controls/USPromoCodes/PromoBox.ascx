<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PromoBox.ascx.vb" Inherits="PromoBox" %>
<%@ Register Namespace="ExpandIT.Buttons" TagPrefix="exbtn" %>
<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<asp:Panel ID="pnlEdit" runat="server" DefaultButton="lblApply" Width="180" CssClass="providerPanel"
    Visible="false">
    <table style="margin: 10px; text-align: left;">
        <tr>
            <td style="padding-bottom: 5px; padding-left: 3px;">
                <asp:Label ID="Label1" Text='<%$ Resources: Language, LABEL_PROMO_TITLE%>' runat="server"
                    Font-Bold="True" Width="112px" Height="17"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="padding-bottom: 5px;">
                <asp:TextBox Height="15" ID="promoCode" CssClass="borderTextBox" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                <%--WGS 10-14-2013  Your Name/Email; added CausesValidation="False" --%>
                <asp:Button runat="server" CssClass="AddButton" Style="margin-top: 3px;" ID="lblApply" CausesValidation="False"
                    Text="Calculate" />
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel ID="pnlNoEdit" runat="Server" Visible="false">
    <asp:Panel ID="pnlPromoTitle" runat="server" Visible="false">
        <div>
            <asp:Label ID="Label2" Text="Promotion Info" runat="server" Font-Bold="True"></asp:Label>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlPromoCode" runat="server" Visible="false">
        <div>
            Code:
            <% = HTMLEncode(OrderDict("PromotionCode")) %>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlPromoName" runat="server" Visible="false">
        <div>
            Name:
            <% = HTMLEncode(OrderDict("PromotionName")) %>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlPromoLongDescription" runat="server" Visible="false">
        <div>
            Description:
            <% =HTMLEncode(OrderDict("PromotionDescription"))%>
        </div>
    </asp:Panel>
</asp:Panel>
