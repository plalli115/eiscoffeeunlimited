<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PromotionBox.ascx.vb"
    Inherits="PromotionBox" %>
<%@ Register Src="../Box.ascx" TagName="Box" TagPrefix="uc1" %>
<%@ Register Src="PromoBox.ascx" TagName="PromoBox" TagPrefix="uc1" %>
<%@ Register Namespace="ExpandIT.SimpleUIControls" TagPrefix="expui" %>
<uc1:Box ID="Box1" runat="server" IdPrefix="PromotionBox">
    <headertemplate>  
    <asp:HyperLink ID="lblHeader" runat="server"><%=Resources.Language.LABEL_PROMO_TITLE%></asp:HyperLink>   
</headertemplate>
    <itemtemplate>
    <div class="LoginBoxContent">
        <uc1:PromoBox ID="PromoBox1" runat="server" />
    </div>
</itemtemplate>
</uc1:Box>
