﻿Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.ExpandITLib
Imports ExpandIT.EISClass
Imports System.IO
Imports System.Data 

Imports System.Collections.Generic 
Imports System.Collections.Specialized


Partial Class NoteList
    Inherits ExpandIT.UserControl
    
    Private m_listName As String
    Private products As ExpDictionary
    
    Private Const SQL_TMPL_old As String = "SELECT * FROM Notes INNER JOIN ProductTable ON " _
     & "Notes.ProductGuid=ProductTable.ProductGuid WHERE (Notes.UserGuid = {0}) AND (Notes.ListName = '{1}')"
    
    Private Const SQL_TMPL As String = "SELECT N.*, PT.*, PV.VariantName  FROM Notes AS N INNER JOIN ProductTable AS PT ON " _
    & "N.ProductGuid=PT.ProductGuid LEFT JOIN ProductVariant AS PV ON (N.VariantCode = PV.VariantCode AND " _
    & "N.ProductGuid = PV.ProductGuid)  WHERE (N.UserGuid = {0}) AND (N.ListName = '{1}')"
    
       
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim product As ExpDictionary 
        Dim sql As String         
        
        ' Get products in notes.         
        sql = String.Format(SQL_TMPL, SafeString(globals.User("UserGuid")), m_listName)
        products = SQL2Dicts(sql, "ProductGuid")
        clearnote.PostBackUrl = String.Format("~/_notes.aspx?action=clear&NNN={0}", HttpUtility.UrlEncode(m_listName)) 
        selnote.SelectedValue = m_listName
        
        If products.Count > 0 Then
            PanelEmptyList.Visible = False
            ' Load the information about the products
            eis.CatDefaultLoadProducts(products, False, False, True, New String() {"ProductName"}, New String() {"PICTURE2"}, "")
            ' Sort the list 
            Dim sortedproducts As New System.Collections.Generic.SortedDictionary(Of String, Object)
            For Each product In products.Values
                sortedproducts.Add(product("ProductName") & vbTab & product("ProductGuid"), product)
            Next
            '
            ShowMailControls(True)            
            ' Databinding
            dlProducts.DataSource = sortedproducts
            dlProducts.DataBind()
        Else
            PanelList.Visible = False
        End If
    End Sub
    
   
    Public Property ListName As String 
      Get
        Return m_listName
      End Get
      Set(ByVal value As String)
        m_listName = value
      End Set
    End Property   
    
    Private Sub ShowMailControls(show As Boolean)      
        Label4.Visible = show
        sel_mailtempl.Visible = show 
        sendnote.Visible = show
    End Sub



    Protected Sub savenote_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles savenote.Click
        If ListNameTextBox.Text <> "" then
          Dim s As String = String.Format("~/_notes.aspx?action=save&NNN={0}&SNNN={1}", HttpUtility.UrlEncode(m_listName), HttpUtility.UrlEncode(ListNameTextBox.Text))
          Response.Redirect(s)
        End if
    End Sub

    Protected Sub sendnote_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles sendnote.Click
        Dim ds As DataSet = New DataSet("Content")
        
        Dim dtContent As DataTable = ds.Tables.Add("ContentRows")
        
        dtContent.Columns.Add("ProductGuid", GetType(String))
        dtContent.Columns.Add("ProductName", GetType(String))
        dtContent.Columns.Add("VariantName", GetType(String)) ' "LABEL_VARIANTNAME"
        dtContent.Columns.Add("Quantity", GetType(String))
        dtContent.Columns.Add("Price", GetType(String)) 
        'dtContent.Columns.Add("SecondaryPrice", GetType(String))          
        Dim formContents As NameValueCollection  = HttpContext.Current.Request.Form 
        Dim product As ExpDictionary 
        
        If products.Count > 0 Then
          Dim sku_all As String = formContents.Item("SKU")          
          Dim sku() As String = {}
          Dim quantity_all As String = formContents.Item("Quantity")
          Dim quantity() As String = {}
          Dim quantDict As New Dictionary(Of String, String)
          If (Not (sku_all is Nothing)) AND (Not (quantity_all Is Nothing)) then
            sku = sku_all.Split(",") 
            quantity = quantity_all.Split(",") 
            For i As Integer = 0 To sku.Length-1 
              quantDict.Add(sku(i),quantity(i))
            Next
          End If          
          Dim productGuid As String = ""
          Dim productName As string = ""
          Dim variantCode As String = ""
          Dim variantName As String = ""
          Dim productQuantity As String = ""
          Dim productPrice As String = ""
          'Dim secondaryPrice As String = ""
          For Each product In products.Values
            Dim ProductObj As New ProductClass(product)
            productGuid = product.Item("ProductGuid")
            productName = product.Item("ProductName")
            variantCode = product.Item("VariantCode")
            variantName = product.Item("VariantName").ToString() 'Can be DbNull
            productPrice = CurrencyFormatter.FormatCurrency(CDblEx(ProductObj.Price), Session("UserCurrencyGuid").ToString)
            'secondaryPrice = CurrencyFormatter.FormatCurrency(CDblEx(ProductObj.SecondaryPrice), Session("UserCurrencyGuid").ToString) 
            Dim dr as DataRow = dtContent.NewRow()
            dr.Item("ProductGuid") = productGuid  
            dr.Item("ProductName") = productName
            if quantDict.TryGetValue(productGuid, productQuantity) then
              dr.Item("Quantity") = productQuantity
            End If
            If variantName <> "" then
              dr.Item("VariantName") = variantName 
            End If
            dr.Item("Price") = productPrice 
            'dr.Item("SecondaryPrice") = secondaryPrice
            dtContent.Rows.Add(dr)                           
          Next          
          Session.Add("XmlNoteContent", ds.GetXml())          
          'ds.WriteXml(HttpContext.Current.Server.MapPath("~/test_t.xml")) 
          Dim url As String = String.Format("~/NoteMailPreview.aspx?templ_name={0}", HttpUtility.UrlEncode(sel_mailtempl.SelectedValue()))
          Response.Redirect(url)  
        End If        
    End Sub
    
End Class
