'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
Imports System.Configuration.ConfigurationManager
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

Public Class CurrentUser
    Inherits ExpandIT.UserControl

    Protected currentUserName As String = ""

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Not globals.User Is Nothing Then
            If CBoolEx(globals.User("B2B")) Then
                currentUserName = globals.User("ContactName") & " (B2B)"
            ElseIf CBoolEx(globals.User("B2C")) Then
                currentUserName = globals.User("ContactName") & " (B2C)"
            Else
                currentUserName = globals.User("ContactName")
            End If
        Dim myControl As Control = Me.LoadControl("~/controls/user/Controls_ExpandITB2BUser.ascx")
        myControl.ID = "ucB2B1"
        Me.phCurrentCustomerInfo.Controls.Clear()
        Me.phCurrentCustomerInfo.Controls.Add(myControl)
        End If  

    End Sub

End Class
'AM2011031801 - ENTERPRISE CSR STAND ALONE - End