<!--AM2011031801 - ENTERPRISE CSR STAND ALONE - Start-->
<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CurrentUser.ascx.vb"
    Inherits="CurrentUser" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Register Src="~/controls/USCSR/UserSearch.ascx" TagName="UserSearch" TagPrefix="uc1" %>
<%@ Register TagName="ucB2B" TagPrefix="exp" Src="~/controls/user/Controls_ExpandITB2BUser.ascx" %>
<%@ Register TagName="ucNotes" TagPrefix="uc1" Src="~/controls/USCSR/CSRNotesControl.ascx" %>
<table>
    <%--<tr>
        <td>
            <asp:Label ID="lblCurrentUserTitle" runat="server" Style="font-weight: bold;" Text="<%$ Resources: Language,LABEL_CSR_CURRENT_USER %>"></asp:Label>
        </td>
    </tr>--%>
    <tr>
        <td>
            <a href='<% = Vroot & "/user.aspx"%>'>
                <% = currentUserName %>
            </a><a onclick="javascript:animatedcollapse.toggle('divNotes');centerPage('divNotes');" style="cursor: pointer;">
                <img style="border: 0px;" src='<% = Vroot & "/images/Notes.png" %>' /></a>
            <div id="divNotes" class="CSRUserSearchDiv" style="display:none;width:640px;">
                <div style="position:absolute; left: 630px;">
                    <img alt="" style="cursor: pointer;" src="<%= Vroot %>/App_Themes/EnterpriseBlue/graphics/close.PNG"
                        onclick="javascript:animatedcollapse.toggle('divNotes')" />
                </div>
                 <uc1:ucNotes ID="ucNotes1" runat="server" />                 
            </div>
        </td>
    </tr>
    <tr>
        <td style="padding: 0px; padding-top: 4px;">
            <div style="vertical-align: bottom; height: 17px;text-align:left;">
                <%--<%=Resources.Language.LABEL_CSR_CHANGE_CURRENT_USER%>--%>
                <a onclick="javascript:animatedcollapse.toggle('divUserSearch');centerPage('divUserSearch');" style="cursor: pointer;">
                    <%=Resources.Language.LABEL_CSR_CHANGE_CURRENT_USER%>
                </a>
            </div>
            <div style="vertical-align: bottom; text-align:left;">
                <a onclick="javascript:animatedcollapse.toggle('divUserInfo');centerPage('divUserInfo');" style="cursor: pointer;">
                    <%=Resources.Language.LABEL_CSR_VIEW_USER_INFO%>
                </a>
            </div>
            <div id="divUserSearch" class="CSRUserSearchDiv" style="display: none;">
                <%--JA2010092101 - USER SEARCH CONTROL - START--%>
                <uc1:UserSearch ID="UserSearch1" runat="server"></uc1:UserSearch>
                <%--JA2010092101 - USER SEARCH CONTROL - END--%>
            </div>
        </td>
    </tr>
    <%--<tr>
            <td>
                
            </td>
        </tr>--%>
    <tr>
        <td>
            <div id="divUserInfo" class="divUserInfo" style="display: none; width:260px;">
                <div style="position:absolute; left:245px;">
                    <img alt="" style="cursor: pointer;" src="<%= Vroot %>/App_Themes/EnterpriseBlue/graphics/close.PNG"
                        onclick="javascript:animatedcollapse.toggle('divUserInfo')" />
                </div>
                <asp:PlaceHolder ID="phCurrentCustomerInfo" runat="server" />
            </div>
        </td>
    </tr>
</table>

<script type="text/javascript"> 
    animatedcollapse.addDiv('divUserInfo', 'fade=1')
    animatedcollapse.addDiv('divUserSearch', 'fade=1')
    animatedcollapse.addDiv('divNotes', 'fade=0')
    animatedcollapse.ontoggle=function($, divobj, state){}
    animatedcollapse.init()
</script>

<script type="text/javascript"> 
        function centerPage(guid){
            var t=document.getElementById(guid);
            var x='middle';
            var y='middle';
            this.getviewpoint() //Get current viewpoint numbers
            t.style.left=(x=="middle")? this.scroll_left+(this.docwidth-t.offsetWidth)/2+"px" : this.scroll_left+parseInt(x)+"px"
            //t.style.top=(y=="middle")? this.scroll_top+(this.docheight-t.offsetHeight)/2+"px" : this.scroll_top+parseInt(y)+"px"    
        }
        
        function getviewpoint(){ //get window viewpoint numbers
            var ie=document.all && !window.opera
            var domclientWidth=document.documentElement && parseInt(document.documentElement.clientWidth) || 100000 //Preliminary doc width in non IE browsers
            this.standardbody=(document.compatMode=="CSS1Compat")? document.documentElement : document.body //create reference to common "body" across doctypes
            this.scroll_top=(ie)? this.standardbody.scrollTop : window.pageYOffset
            this.scroll_left=(ie)? this.standardbody.scrollLeft : window.pageXOffset
            this.docwidth=(ie)? this.standardbody.clientWidth : (/Safari/i.test(navigator.userAgent))? window.innerWidth : Math.min(domclientWidth, window.innerWidth-16)
            this.docheight=(ie)? this.standardbody.clientHeight: window.innerHeight
        }
    </script>

<!--AM2011031801 - ENTERPRISE CSR STAND ALONE - End-->
