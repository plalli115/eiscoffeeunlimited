<!--AM2011031801 - ENTERPRISE CSR STAND ALONE - Start-->
<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CSRCartReturn.ascx.vb"
    Inherits="CSRCartReturn" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%  
    If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And csrObj.getSalesPersonGuid() <> "" And Not globals.IsCartEmpty Then%>
    <td>
        <asp:Button runat="server" Visible="false" CssClass="AddButton" ID="Returned" Text="<%$Resources: Language,LABEL_RETURN_LINK%>" />
    </td>
<%End If%>
<!--AM2011031801 - ENTERPRISE CSR STAND ALONE - End-->
