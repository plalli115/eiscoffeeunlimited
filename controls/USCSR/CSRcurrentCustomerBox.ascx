<%--AM2011031801 - ENTERPRISE CSR STAND ALONE - Start--%>
<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CSRcurrentCustomerBox.ascx.vb"
    Inherits="CSRcurrentCustomerBox" %>
<%@ Register TagPrefix="expCard" TagName="creditcards" Src="~/controls/CreditCards.ascx" %>
<%@ Register TagPrefix="expCard" TagName="Box" Src="~/controls/Box.ascx" %>
<%@ Register Src="~/controls/USCSR/CurrentUser.ascx" TagName="CurrentUser" TagPrefix="uc1" %>

<expCard:Box ID="credCardBox" runat="server" IdPrefix="CreditCardBox">
    <headertemplate>
        <asp:HyperLink ID="HyperLink1" runat="server"><%= Resources.Language.LABEL_CSR_CURRENT_USER%></asp:HyperLink>
    </headertemplate>
    <itemtemplate>
<div style="padding-left:15px; padding-bottom:10px; padding-top:10px;">
    <uc1:CurrentUser ID="CurrentUser1" runat="server"></uc1:CurrentUser>
</div>
</itemtemplate>
</expCard:Box>
<%--AM2011031801 - ENTERPRISE CSR STAND ALONE - End--%>