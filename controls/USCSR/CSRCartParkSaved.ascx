<!--AM2011031801 - ENTERPRISE CSR STAND ALONE - Start-->
<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CSRCartParkSaved.ascx.vb"
    Inherits="CSRCartParkSaved" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<asp:Label ID="MessageSaved" runat="server" Style="color: #208040;" Visible="false"
    Text="<%$Resources: Language,LABEL_PARK_SAVED_MESSAGE%>"></asp:Label>
<!--AM2011031801 - ENTERPRISE CSR STAND ALONE - End-->
