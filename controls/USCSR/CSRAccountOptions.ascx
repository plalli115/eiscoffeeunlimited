<!--AM2011031801 - ENTERPRISE CSR STAND ALONE - Start-->
<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CSRAccountOptions.ascx.vb"
    Inherits="CSRAccountOptions" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>

<%  If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And csrObj.getSalesPersonGuid() <> "" Then%>
    <li>
        <asp:HyperLink ID="HyperLink7" runat="server" NavigateUrl="~/history.aspx?Status=cancell"
            Text="<%$ Resources: Language, LABEL_PREVIOUS_ORDER_CANCELL %>"></asp:HyperLink>
    </li>
    <li>
        <asp:HyperLink ID="HyperLink8" runat="server" NavigateUrl="~/history.aspx?park=park"
            Text="<%$ Resources: Language, LABEL_PARK_ORDERS %>"></asp:HyperLink>
    </li>
<%End If%>
<!--AM2011031801 - ENTERPRISE CSR STAND ALONE - End-->
