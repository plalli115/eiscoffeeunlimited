'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
Imports System.Configuration.ConfigurationManager
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT

Public Class CSRCartReturn
    Inherits ExpandIT.UserControl

    Protected csrObj As USCSR
    Protected _cartPaymentAddress As Object
    Protected _cartShippingAddress As Object

    Public Property CartPaymentAddress() As Object
        Get
            Return _cartPaymentAddress
        End Get
        Set(ByVal value)
            _cartPaymentAddress = value
        End Set
    End Property

    Public Property CartShippingAddress() As Object
        Get
            Return _cartShippingAddress
        End Get
        Set(ByVal value)
            _cartShippingAddress = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        csrObj = New USCSR(globals)
    End Sub

    Protected Sub returned_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Returned.Click
        csrObj.completeReturn(CartPaymentAddress, CartShippingAddress)
    End Sub

End Class
'AM2011031801 - ENTERPRISE CSR STAND ALONE - End