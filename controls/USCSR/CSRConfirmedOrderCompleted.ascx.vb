'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
Imports System.Configuration.ConfigurationManager
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT

Public Class CSRConfirmedOrderCompleted
    Inherits ExpandIT.UserControl

    Protected csrObj As USCSR
    Private _order As ExpDictionary
    Private _bMailSend As Boolean

    Public Property Order() As ExpDictionary
        Get
            Return _order
        End Get
        Set(ByVal value As ExpDictionary)
            _order = value
        End Set
    End Property

    Public Property bMailSend() As Boolean
        Get
            Return _bMailSend
        End Get
        Set(ByVal value As Boolean)
            _bMailSend = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        csrObj = New USCSR(globals)
    End Sub

End Class
'AM2011031801 - ENTERPRISE CSR STAND ALONE - End