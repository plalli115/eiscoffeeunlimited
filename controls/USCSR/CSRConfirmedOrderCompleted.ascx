<!--AM2011031801 - ENTERPRISE CSR STAND ALONE - Start-->
<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CSRConfirmedOrderCompleted.ascx.vb"
    Inherits="CSRConfirmedOrderCompleted" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%  
    If Not Request("return") Is Nothing And Not Request("return") Is DBNull.Value Then%>
<p>
    <% Response.Write(Resources.Language.MESSAGE_YOUR_ORDER_IS_FINISHED_RETURN)%>
    &nbsp;<%=csrObj.getLinkToHistoryDetailReturnOrders(Order)%>
    <br />
    <br />
</p>
<br />
<p>
    <% Else%>
    <p>
        <% Response.Write(Resources.Language.MESSAGE_YOUR_ORDER_IS_FINISHED)%>
        &nbsp;<% =csrObj.getLinkToHistoryDetail(Order)%>
        <br />
        <br />
        <%--AM2010080201 - ORDER PRINT OUT - Start--%>
        <% Response.Write(Resources.Language.CLICK_TO_PRINT_ORDER)%>
        &nbsp;<a href="PrintOrder.aspx?HeaderGuid=<% Response.Write(Order("HeaderGuid")) %>"
            target="_blank">here</a>.
        <%--AM2010080201 - ORDER PRINT OUT - End--%>
    </p>
    <br />
    <p>
        <%      If bMailSend Then
                Response.Write(Resources.Language.MESSAGE_MAIL_SEND_OK)
            Else
                Response.Write(Resources.Language.MESSAGE_MAIL_SEND_FAIL)
            End If%>
        <%End If%>
        <!--AM2011031801 - ENTERPRISE CSR STAND ALONE - End-->
