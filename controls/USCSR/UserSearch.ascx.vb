'AM2011031801 - ENTERPRISE CSR STAND ALONE - START
'JA2010092101 - USER SEARCH CONTROL - START
Imports System.Configuration.ConfigurationManager
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT


Public Class UserSearch
    Inherits ExpandIT.UserControl

    Private m_showcaption As Boolean = True
    Protected page1 As String
    Protected csrObj As USCSR

    Protected Overloads Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        page1 = CStrEx(HttpContext.Current.Request.Url.PathAndQuery.Replace(VRoot & "/", ""))
        Me.quicksearchpanel.DefaultButton = "SearchBtn"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        csrObj = New USCSR(globals)
        If CBoolEx(AppSettings("USER_SEARCH_FIELDS_FIRST_NAME")) Then
            Me.txbLastName.Focus()
        ElseIf CBoolEx(AppSettings("USER_SEARCH_FIELDS_LAST_NAME")) Then
            Me.txbLastName2.Focus()
        ElseIf CBoolEx(AppSettings("USER_SEARCH_FIELDS_LOGIN")) Then
            Me.txbLogin.Focus()
        ElseIf CBoolEx(AppSettings("USER_SEARCH_FIELDS_PHONE")) Then
            Me.txbPhoneNumber.Focus()
        ElseIf CBoolEx(AppSettings("USER_SEARCH_FIELDS_ADDRESS")) Then
            Me.txbAddress.Focus()
        ElseIf CBoolEx(AppSettings("USER_SEARCH_FIELDS_ZIPCODE")) Then
            Me.txbZipCode.Focus()
        ElseIf CBoolEx(AppSettings("USER_SEARCH_FIELDS_STATE")) Then
            Me.txbState.Focus()
        End If

        If Page.IsPostBack Then
            If CBoolEx(AppSettings("USER_SEARCH_FIELDS_FIRST_NAME")) Then
                Session("Search_Name_" & CStrEx(globals.User("UserGuid"))) = CStrEx(CType(Me.quicksearchpanel.FindControl("txbLastName"), TextBox).Text)
            End If
            If CBoolEx(AppSettings("USER_SEARCH_FIELDS_LAST_NAME")) Then
                Session("Search_Name_2_" & CStrEx(globals.User("UserGuid"))) = CStrEx(CType(Me.quicksearchpanel.FindControl("txbLastName2"), TextBox).Text)
            End If
            If CBoolEx(AppSettings("USER_SEARCH_FIELDS_LOGIN")) Then
                Session("Search_Login_" & CStrEx(globals.User("UserGuid"))) = CStrEx(CType(quicksearchpanel.FindControl("txbLogin"), TextBox).Text)
            End If
            If CBoolEx(AppSettings("USER_SEARCH_FIELDS_PHONE")) Then
                Session("Search_Phone_" & CStrEx(globals.User("UserGuid"))) = CStrEx(CType(quicksearchpanel.FindControl("txbPhoneNumber"), TextBox).Text)
            End If
            If CBoolEx(AppSettings("USER_SEARCH_FIELDS_ADDRESS")) Then
                Session("Search_Address_" & CStrEx(globals.User("UserGuid"))) = CStrEx(CType(quicksearchpanel.FindControl("txbAddress"), TextBox).Text)
            End If
            If CBoolEx(AppSettings("USER_SEARCH_FIELDS_ZIPCODE")) Then
                Session("Search_ZipCode_" & CStrEx(globals.User("UserGuid"))) = CStrEx(CType(quicksearchpanel.FindControl("txbZipCode"), TextBox).Text)
            End If
            If CBoolEx(AppSettings("USER_SEARCH_FIELDS_STATE")) Then
                Session("Search_State_" & CStrEx(globals.User("UserGuid"))) = CStrEx(CType(quicksearchpanel.FindControl("txbState"), TextBox).Text)
            End If
        Else
            If CBoolEx(AppSettings("USER_SEARCH_FIELDS_FIRST_NAME")) Then
                CType(Me.quicksearchpanel.FindControl("txbLastName"), TextBox).Text = CStrEx(Session("Search_Name_" & CStrEx(globals.User("UserGuid"))))
            End If
            If CBoolEx(AppSettings("USER_SEARCH_FIELDS_LAST_NAME")) Then
                CType(Me.quicksearchpanel.FindControl("txbLastName2"), TextBox).Text = CStrEx(Session("Search_Name_2_" & CStrEx(globals.User("UserGuid"))))
            End If
            If CBoolEx(AppSettings("USER_SEARCH_FIELDS_LOGIN")) Then
                CType(quicksearchpanel.FindControl("txbLogin"), TextBox).Text = CStrEx(Session("Search_Login_" & CStrEx(globals.User("UserGuid"))))
            End If
            If CBoolEx(AppSettings("USER_SEARCH_FIELDS_PHONE")) Then
                CType(quicksearchpanel.FindControl("txbPhoneNumber"), TextBox).Text = CStrEx(Session("Search_Phone_" & CStrEx(globals.User("UserGuid"))))
            End If
            If CBoolEx(AppSettings("USER_SEARCH_FIELDS_ADDRESS")) Then
                CType(quicksearchpanel.FindControl("txbAddress"), TextBox).Text = CStrEx(Session("Search_Address_" & CStrEx(globals.User("UserGuid"))))
            End If
            If CBoolEx(AppSettings("USER_SEARCH_FIELDS_ZIPCODE")) Then
                CType(quicksearchpanel.FindControl("txbZipCode"), TextBox).Text = CStrEx(Session("Search_ZipCode_" & CStrEx(globals.User("UserGuid"))))
            End If
            If CBoolEx(AppSettings("USER_SEARCH_FIELDS_STATE")) Then
                CType(quicksearchpanel.FindControl("txbState"), TextBox).Text = CStrEx(Session("Search_State_" & CStrEx(globals.User("UserGuid"))))
            End If
        End If
    End Sub
    Protected Sub focusOnLogin(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.txbLogin.Focus()
    End Sub
    Protected Sub focusOnPhone(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.txbPhoneNumber.Focus()
    End Sub
    Protected Sub focusOnAddress(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.txbAddress.Focus()
    End Sub
    Protected Sub focusOnZipCode(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.txbZipCode.Focus()
    End Sub

    Protected Sub SearchBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles SearchBtn.Click
        If CBoolEx(AppSettings("USER_SEARCH_FIELDS_FIRST_NAME")) Then
            Session("Search_Name_" & CStrEx(globals.User("UserGuid"))) = CStrEx(CType(Me.quicksearchpanel.FindControl("txbLastName"), TextBox).Text)
        End If
        If CBoolEx(AppSettings("USER_SEARCH_FIELDS_LAST_NAME")) Then
            Session("Search_Name_2_" & CStrEx(globals.User("UserGuid"))) = CStrEx(CType(Me.quicksearchpanel.FindControl("txbLastName2"), TextBox).Text)
        End If
        If CBoolEx(AppSettings("USER_SEARCH_FIELDS_LOGIN")) Then
            Session("Search_Login_" & CStrEx(globals.User("UserGuid"))) = CStrEx(CType(quicksearchpanel.FindControl("txbLogin"), TextBox).Text)
        End If
        If CBoolEx(AppSettings("USER_SEARCH_FIELDS_PHONE")) Then
            Session("Search_Phone_" & CStrEx(globals.User("UserGuid"))) = CStrEx(CType(quicksearchpanel.FindControl("txbPhoneNumber"), TextBox).Text)
        End If
        If CBoolEx(AppSettings("USER_SEARCH_FIELDS_ADDRESS")) Then
            Session("Search_Address_" & CStrEx(globals.User("UserGuid"))) = CStrEx(CType(quicksearchpanel.FindControl("txbAddress"), TextBox).Text)
        End If
        If CBoolEx(AppSettings("USER_SEARCH_FIELDS_ZIPCODE")) Then
            Session("Search_ZipCode_" & CStrEx(globals.User("UserGuid"))) = CStrEx(CType(quicksearchpanel.FindControl("txbZipCode"), TextBox).Text)
        End If
        If CBoolEx(AppSettings("USER_SEARCH_FIELDS_STATE")) Then
            Session("Search_State_" & CStrEx(globals.User("UserGuid"))) = CStrEx(CType(quicksearchpanel.FindControl("txbState"), TextBox).Text)
        End If
        Search()
    End Sub

    Public Sub Search()
        Dim Name As String = ""
        Dim Name2 As String = ""
        Dim login As String = ""
        Dim phone As String = ""
        Dim address As String = ""
        Dim zip As String = ""
        Dim state As String = ""

        Name = Replace(Session("Search_Name_" & CStrEx(globals.User("UserGuid"))), "'", "''")
        Name2 = Replace(Session("Search_Name_2_" & CStrEx(globals.User("UserGuid"))), "'", "''")
        login = Replace(Session("Search_Login_" & CStrEx(globals.User("UserGuid"))), "'", "''")
        phone = Replace(Session("Search_Phone_" & CStrEx(globals.User("UserGuid"))), "'", "''")
        address = Replace(Session("Search_Address_" & CStrEx(globals.User("UserGuid"))), "'", "''")
        state = Replace(Session("Search_State_" & CStrEx(globals.User("UserGuid"))), "'", "''")
        zip = Replace(Session("Search_ZipCode_" & CStrEx(globals.User("UserGuid"))), "'", "''")

        Dim whereSQL As String = ""
        Dim whereSQL2 As String = ""

        If (Name <> "") Then
            whereSQL &= " AND ContactName LIKE '" & Name & "%'"
            whereSQL2 &= " AND U.ContactName LIKE '" & Name & "%'"
        End If
        If (Name2 <> "") Then
            whereSQL &= " AND ContactName LIKE '%" & Name2 & "%'"
            whereSQL2 &= " AND U.ContactName LIKE '%" & Name2 & "%'"
        End If
        If (login <> "") Then
            whereSQL &= " AND UserLogin LIKE '%" & login & "%'"
            whereSQL2 &= " AND U.UserLogin LIKE '%" & login & "%'"
        End If
        If (phone <> "") Then
            whereSQL &= " AND PhoneNo LIKE '%" & phone & "%'"
            whereSQL2 &= " AND C.PhoneNo LIKE '%" & phone & "%'"
        End If
        If (address <> "") Then
            whereSQL &= " AND Address1 LIKE '%" & address & "%'"
            whereSQL2 &= " AND C.Address1 LIKE '%" & address & "%'"
        End If
        If (state <> "") Then
            whereSQL &= " AND StateName LIKE '%" & state & "%'"
            whereSQL2 &= " AND C.StateName LIKE '%" & state & "%'"
        End If
        If (zip <> "") Then
            whereSQL &= " AND ZipCode LIKE '%" & zip & "%'"
            whereSQL2 &= " AND C.ZipCode LIKE '%" & zip & "%'"
        End If

        Dim sql As String = ""
        Dim sql2 As String = ""
        Dim sql3 As String = ""
        Dim retval As Integer = 0

        If (whereSQL <> "") Then

            'AM2010092201 - ENTERPRISE CSR - Start
            If Not CBoolEx(AppSettings("CSR_ALLOWED_TO_ORDER")) Then
                whereSQL &= " AND ISNULL(SalesPersonGuid, '') = ''"
                whereSQL2 &= " AND ISNULL(U.SalesPersonGuid, '') = ''"
            Else
                whereSQL &= " AND ISNULL(SalesPersonGuid, '') <> " & SafeString(csrObj.getSalesPersonGuid())
                whereSQL2 &= " AND ISNULL(U.SalesPersonGuid, '') <> " & SafeString(csrObj.getSalesPersonGuid())
            End If
            'AM2010092201 - ENTERPRISE CSR - End

            whereSQL = Mid(whereSQL, 6)
            whereSQL = "WHERE " & whereSQL

            If CStrEx(page1) = "" Then
                page1 = CStrEx(HttpContext.Current.Request.Url.PathAndQuery.Replace(VRoot & "/", ""))
            End If

            If CStrEx(AppSettings("USER_SEARCH_NUMBER_RECORDS")) <> "" Then
                sql = "SELECT TOP " & CStrEx(AppSettings("USER_SEARCH_NUMBER_RECORDS")) & " '" & page1 & "' AS PageURL, UserGuid,ContactName,Address1,Address2,CityName,StateName,LEFT(ZipCode, 5) AS ZipCode,PhoneNo,UserLogin FROM UserTable " & whereSQL & " UNION SELECT '" & page1 & "' AS PageURL, U.UserGuid,U.ContactName,C.Address1,C.Address2,C.CityName,C.StateName, LEFT(C.ZipCode, 5) AS ZipCode,C.PhoneNo,U.UserLogin  FROM UserTableB2B U INNER JOIN CustomerTable C ON U.CustomerGuid=C.CustomerGuid " & whereSQL2 & " AND U.UserGuid NOT IN (SELECT UserGuid FROM UserTable) ORDER BY ContactName"
            Else
                sql = "SELECT '" & page1 & "' AS PageURL, UserGuid,ContactName,Address1,Address2,CityName,StateName,LEFT(ZipCode, 5) AS ZipCode,PhoneNo,UserLogin FROM UserTable " & whereSQL & " UNION SELECT '" & page1 & "' AS PageURL, U.UserGuid,U.ContactName,C.Address1,C.Address2,C.CityName,C.StateName, LEFT(C.ZipCode, 5) AS ZipCode,C.PhoneNo,U.UserLogin  FROM UserTableB2B U INNER JOIN CustomerTable C ON U.CustomerGuid=C.CustomerGuid " & whereSQL2 & " AND U.UserGuid NOT IN (SELECT UserGuid FROM UserTable) ORDER BY ContactName"
            End If


            sql2 = "SELECT COUNT (*) FROM UserTable " & whereSQL
            If whereSQL2 = "" Then
                whereSQL2 = " WHERE "
            Else
                whereSQL2 &= " AND "
            End If
            sql3 = "SELECT COUNT (*)  FROM UserTableB2B U INNER JOIN CustomerTable C ON U.CustomerGuid=C.CustomerGuid " & whereSQL2 & " U.UserGuid NOT IN (SELECT UserGuid FROM UserTable)"

            retval = CIntEx(getSingleValueDB(sql2)) + CIntEx(getSingleValueDB(sql3))
        End If

        GridView1.Columns.Clear()
        Dim boundHyperLinkField As New HyperLinkField
        boundHyperLinkField.HeaderStyle.CssClass = "userSearchCartHeader"
        boundHyperLinkField.ItemStyle.CssClass = "userSearchCart"
        boundHyperLinkField.DataNavigateUrlFields = New String() {"UserGuid", "PageURL"}
        boundHyperLinkField.Text = "Select"
        boundHyperLinkField.DataNavigateUrlFormatString = VRoot & "/userSearchSelection.aspx?OrderUserGuid={0}&returl={1}"
        GridView1.Columns.Add(boundHyperLinkField)

        Dim boundfieldPark As New BoundField
        boundfieldPark.HeaderText = Resources.Language.LABEL_PARK_USER_SEARCH
        boundfieldPark.HeaderStyle.Width = Unit.Pixel(30)
        boundfieldPark.ItemStyle.Width = Unit.Pixel(30)
        boundfieldPark.DataField = "UserGuid"
        boundfieldPark.HeaderStyle.CssClass = "userSearchCartHeader"
        boundfieldPark.ItemStyle.CssClass = "userSearchCart"

        'boundHyperLinkFieldPark.DataNavigateUrlFormatString = VRoot & "/history.aspx?parkUserGuid={0}&park=1"
        GridView1.Columns.Add(boundfieldPark)



        If (whereSQL <> "") Then
            If retval <> 0 Then
                Me.searchUser.SelectCommand = sql
                Me.GridView1.DataBind()
                Me.GridView1.Visible = True
                Me.ErrorMsg.Visible = False
                If CStrEx(AppSettings("USER_SEARCH_NUMBER_RECORDS")) <> "" Then
                    Dim recordsDict As ExpDictionary = SQL2Dicts(sql)
                    If CIntEx(recordsDict.Count) >= 30 Then
                        Me.ErrorMsg.Text = Replace(Resources.Language.LABEL_SEARCH_LIMITED_RECORDS, "%1", CStrEx(AppSettings("USER_SEARCH_NUMBER_RECORDS")))
                        Me.ErrorMsg.Visible = True
                    Else
                        Me.ErrorMsg.Text = ""
                        Me.ErrorMsg.Visible = False
                    End If
                End If
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script1", "setHeight();", True)
            Else
                Me.ErrorMsg.Text = Resources.Language.LABEL_NO_MATCHES
                Me.ErrorMsg.Visible = True
                Me.GridView1.Visible = False
            End If
        Else
            Me.ErrorMsg.Text = Resources.Language.LABEL_SEARCH_CRITERIA_ENTERED
            Me.ErrorMsg.Visible = True
            Me.GridView1.Visible = False
        End If

        'Get array from webconfig key - START
        Dim fields As String = CStrEx(AppSettings("USER_SEARCH_FIELDS_TO_SHOW"))
        Dim fieldsArray() As String = fields.Split(",")
        Dim i As Integer = 0
        For i = 0 To fieldsArray.Length - 1
            Dim boundField As New BoundField
            boundField.DataField = CStrEx(fieldsArray(i))
            Dim label As String = "LABEL_" & CStrEx(fieldsArray(i))
            boundField.HeaderText = HttpContext.GetGlobalResourceObject("Language", label)
            boundField.SortExpression = CStrEx(fieldsArray(i))
            boundField.HeaderStyle.CssClass = "userSearchCartHeader"
            boundField.ItemStyle.CssClass = "userSearchCart"
            If CStrEx(fieldsArray(i)) = "ZipCode" Then
                boundField.HeaderStyle.Width = Unit.Pixel(50)
                boundField.ItemStyle.Width = Unit.Pixel(50)
                boundField.HeaderStyle.HorizontalAlign = HorizontalAlign.Right
                boundField.ItemStyle.CssClass = "ItemStyleRight"
            ElseIf CStrEx(fieldsArray(i)) = "StateName" Then
                boundField.HeaderStyle.Width = Unit.Pixel(25)
                boundField.ItemStyle.Width = Unit.Pixel(25)
                boundField.HeaderStyle.HorizontalAlign = HorizontalAlign.Left
                boundField.ItemStyle.CssClass = "ItemStyleLeft"
            ElseIf CStrEx(fieldsArray(i)) = "PhoneNo" Then
                boundField.HeaderStyle.Width = Unit.Pixel(100)
                boundField.ItemStyle.Width = Unit.Pixel(100)
                boundField.HeaderStyle.HorizontalAlign = HorizontalAlign.Right
                boundField.ItemStyle.CssClass = "ItemStyleRight"
            Else
                boundField.HeaderStyle.Width = Unit.Pixel(100)
                boundField.ItemStyle.Width = Unit.Pixel(100)
                boundField.HeaderStyle.HorizontalAlign = HorizontalAlign.Left
                boundField.ItemStyle.CssClass = "ItemStyleLeft"
            End If
            GridView1.Columns.Add(boundField)
        Next
        'Get array from webconfig key - END

        'If page1 = "csrnotes.aspx" Then
        '    If GridView1.Rows.Count > 0 Then
        '        Try
        '            Me.GridView1.Columns.Remove(Me.GridView1.Columns.Item(8))
        '        Catch ex As Exception

        '        End Try
        '    End If
        'End If

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script1", "closeLoading();", True)

        Me.UpdatePanel2.Update()

        'Name = ""
        'login = ""
        'phone = ""
        'address = ""
        'zip = ""
    End Sub

    Protected Sub GridView1_DataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles GridView1.RowDataBound
        Dim userguid As String
        Dim count As Integer = 0

        Try
            If e.Row.Cells(1).Text <> Resources.Language.LABEL_PARK_USER_SEARCH Then
                userguid = CStrEx(e.Row.Cells(1).Text)
                count = CIntEx(getSingleValueDB("SELECT COUNT(*) FROM [CartHeader] WHERE [UserGuid] = " & SafeString(userguid) & " AND MultiCartStatus='HOLD'"))
                If count > 0 Then
                    e.Row.Cells(1).Text = count
                Else
                    e.Row.Cells(1).Text = ""
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub NewUserBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles NewUserBtn.Click
        Session("NewUserGuid") = GenGuid()
        Response.Redirect(VRoot & "/user_new.aspx")
    End Sub

    Protected Sub ClearBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ClearBtn.Click

        If CBoolEx(AppSettings("USER_SEARCH_FIELDS_FIRST_NAME")) Then
            Session("Search_Name_" & CStrEx(globals.User("UserGuid"))) = ""
            CType(Me.quicksearchpanel.FindControl("txbLastName"), TextBox).Text = ""
        End If
        If CBoolEx(AppSettings("USER_SEARCH_FIELDS_LAST_NAME")) Then
            Session("Search_Name_2_" & CStrEx(globals.User("UserGuid"))) = ""
            CType(Me.quicksearchpanel.FindControl("txbLastName2"), TextBox).Text = ""
        End If
        If CBoolEx(AppSettings("USER_SEARCH_FIELDS_LOGIN")) Then
            Session("Search_Login_" & CStrEx(globals.User("UserGuid"))) = ""
            CType(quicksearchpanel.FindControl("txbLogin"), TextBox).Text = ""
        End If
        If CBoolEx(AppSettings("USER_SEARCH_FIELDS_PHONE")) Then
            Session("Search_Phone_" & CStrEx(globals.User("UserGuid"))) = ""
            CType(quicksearchpanel.FindControl("txbPhoneNumber"), TextBox).Text = ""
        End If
        If CBoolEx(AppSettings("USER_SEARCH_FIELDS_ADDRESS")) Then
            Session("Search_Address_" & CStrEx(globals.User("UserGuid"))) = ""
            CType(quicksearchpanel.FindControl("txbAddress"), TextBox).Text = ""
        End If
        If CBoolEx(AppSettings("USER_SEARCH_FIELDS_ZIPCODE")) Then
            Session("Search_ZipCode_" & CStrEx(globals.User("UserGuid"))) = ""
            CType(quicksearchpanel.FindControl("txbZipCode"), TextBox).Text = ""
        End If
        If CBoolEx(AppSettings("USER_SEARCH_FIELDS_STATE")) Then
            Session("Search_State_" & CStrEx(globals.User("UserGuid"))) = ""
            CType(quicksearchpanel.FindControl("txbState"), TextBox).Text = ""
        End If

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script1", "closeLoading();", True)

        Me.UpdatePanel1.Update()

    End Sub

    'Public Function getParkOrdersText(ByVal userGuid As String)
    '    Dim cont As Integer = 0

    '    cont = CIntEx(getSingleValueDB("SELECT COUNT(*) FROM [CartHeader] WHERE [UserGuid] = " & SafeString(userGuid) & " AND MultiCartStatus='HOLD'"))

    '    If cont > 0 Then
    '        Return Resources.Language.LABEL_PARK_ORDERS_LINK
    '    End If
    '    Return ""
    'End Function

    'Public Function getParkOrdersLink(ByVal userGuid As String)
    '    Return "../holdOrders.aspx?OrderUserGuid=" & userGuid
    'End Function

End Class
'JA2010092101 - USER SEARCH CONTROL - END
'AM2011031801 - ENTERPRISE CSR STAND ALONE - END