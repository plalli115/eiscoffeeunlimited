<%--AM2011031801 - ENTERPRISE CSR STAND ALONE - START--%>
<%--JA2010092101 - USER SEARCH CONTROL - START--%>
<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UserSearch.ascx.vb" Inherits="UserSearch" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<div style="width: 685px; height: 215px;">
    <div style="position: absolute; left: 680px;">
        <img alt="" style="cursor: pointer;" src="<%= Vroot %>/App_Themes/EnterpriseBlue/graphics/close.PNG"
            onclick="javascript:animatedcollapse.toggle('divUserSearch')" />
    </div>
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="Server">
        <ContentTemplate>
            <asp:Panel ID="quicksearchpanel" runat="server">
                <table cellpadding="0" cellspacing="0" style="width: 685px; text-align: center;">
                    <tr>
                        <td align="center">
                            <table cellpadding="0" cellspacing="0" style="height: 205px;">
                                <tr style="height: 22px;">
                                    <td align="left" style="width: 100px;">
                                        <asp:Label ID="Label1" Style="font-weight: bold;" runat="server" Text="Find User"></asp:Label>
                                    </td>
                                </tr>
                                <%If CBoolEx(AppSettings("USER_SEARCH_FIELDS_FIRST_NAME")) Then%>
                                <tr style="height: 22px;">
                                    <td align="left" style="width: 100px;">
                                        <asp:Label ID="lblLastName" runat="server" Text="<%$ Resources: Language,LABEL_FIRST_NAME %>"></asp:Label>:
                                    </td>
                                    <td style="width: 156px">
                                        <asp:TextBox ID="txbLastName" onkeydown="if (event.keyCode == 13) OpenLoadingSearch()"
                                            OnTextChanged="focusOnLogin" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <%End If%>
                                <%If CBoolEx(AppSettings("USER_SEARCH_FIELDS_LAST_NAME")) Then%>
                                <tr style="height: 22px;">
                                    <td align="left" style="width: 100px;">
                                        <asp:Label ID="lblLastName2" runat="server" Text="<%$ Resources: Language,LABEL_LAST_NAME %>"></asp:Label>:
                                    </td>
                                    <td style="width: 156px">
                                        <asp:TextBox ID="txbLastName2" onkeydown="if (event.keyCode == 13) OpenLoadingSearch()"
                                            OnTextChanged="focusOnLogin" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <%End If%>
                                <%If CBoolEx(AppSettings("USER_SEARCH_FIELDS_LOGIN")) Then%>
                                <tr style="height: 22px;">
                                    <td align="left" style="width: 100px;">
                                        <asp:Label ID="lblLogin" runat="server" Text="<%$ Resources: Language,LABEL_LOGIN %>"></asp:Label>:
                                    </td>
                                    <td style="width: 156px">
                                        <asp:TextBox ID="txbLogin" onkeydown="if (event.keyCode == 13) OpenLoadingSearch()"
                                            OnTextChanged="focusOnPhone" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <%End If%>
                                <%If CBoolEx(AppSettings("USER_SEARCH_FIELDS_PHONE")) Then%>
                                <tr style="height: 22px;">
                                    <td align="left" style="width: 100px;">
                                        <asp:Label ID="lblPhoneNumber" runat="server" Text="<%$ Resources: Language,LABEL_PHONE_NUMBER %>"></asp:Label>:
                                    </td>
                                    <td style="width: 156px">
                                        <asp:TextBox ID="txbPhoneNumber" onkeydown="if (event.keyCode == 13) OpenLoadingSearch()"
                                            OnTextChanged="focusOnAddress" runat="server" OnBlur="validatePhone(this);"></asp:TextBox>
                                    </td>
                                </tr>
                                <%End If%>
                                <%If CBoolEx(AppSettings("USER_SEARCH_FIELDS_ADDRESS")) Then%>
                                <tr style="height: 22px;">
                                    <td align="left" style="width: 100px;">
                                        <asp:Label ID="lblAddress" runat="server" Text="<%$ Resources: Language,LABEL_ADDRESS %>"></asp:Label>:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txbAddress" onkeydown="if (event.keyCode == 13) OpenLoadingSearch()"
                                            OnTextChanged="focusOnZipCode" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <%End If%>
                                <%If CBoolEx(AppSettings("USER_SEARCH_FIELDS_STATE")) Then%>
                                <tr style="height: 22px;">
                                    <td align="left" style="width: 100px;">
                                        <asp:Label ID="lblState" runat="server" Text="<%$ Resources: Language,LABEL_STATE %>"></asp:Label>:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txbState" onkeydown="if (event.keyCode == 13) OpenLoadingSearch()"
                                            runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <%End If%>
                                <%If CBoolEx(AppSettings("USER_SEARCH_FIELDS_ZIPCODE")) Then%>
                                <tr style="height: 22px;">
                                    <td align="left" style="width: 100px;">
                                        <asp:Label ID="lblZipCode" runat="server" Text="<%$ Resources: Language,LABEL_USERDATA_ZIPCODE %>"></asp:Label>:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txbZipCode" onkeydown="if (event.keyCode == 13) OpenLoadingSearch()"
                                            runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <%  End If%>
                                <tr>
                                    <td style="vertical-align: bottom;" colspan="2">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="SearchBtn" Style="display: none;" runat="server" />
                                                    <input id="SearchBtnLoading" onclick="OpenLoadingSearch();" type="button" value="<%= Resources.Language.LABEL_MENU_SEARCH %>" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="ClearBtn" Style="display: none;" runat="server" Text="Clear" />
                                                    <input id="ClearBtnLoading" onclick="OpenLoadingClear();" type="button" value="<%= Resources.Language.ACTION_CLEAR %>" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="NewUserBtn" runat="server" Text="<%$ Resources: Language, MENU_SIGNUP %>" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="ErrorMsg" Style="color: Red; margin-left: 0px;" runat="server" Visible="True"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="LoadingCurrentUser" style="background-color: White; background-image: url('<%= Vroot %>/script/stars/loadRating.gif');
                                background-repeat: no-repeat; background-position: center center; text-align: center;
                                position: absolute; display: none;">
                        </td>
                    </tr>
                </table>
                <br />
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ClearBtn" />
        </Triggers>
    </asp:UpdatePanel>

    <script type="text/javascript">
        function OpenLoadingSearch(){ 
            //Loading wait
            var divLoading=document.getElementById('LoadingCurrentUser');
            divLoading.style.top='0px';
            divLoading.style.left='205px';
            divLoading.style.width='300px';
            divLoading.style.height='180px';
            divLoading.style.display='block';
            //Loading wait
            document.getElementById('ctl00_ctl00_CurrentUser1_UserSearch1_SearchBtn').click();
        }
        function OpenLoadingClear(){ 
            //Loading wait
            var divLoading=document.getElementById('LoadingCurrentUser');
            divLoading.style.top='0px';
            divLoading.style.left='205px';
            divLoading.style.width='300px';
            divLoading.style.height='180px';
            divLoading.style.display='block';
            //Loading wait
            document.getElementById('ctl00_ctl00_CurrentUser1_UserSearch1_ClearBtn').click();
        }
        //Loading wait
        function closeLoading(){ 
            var divLoading=document.getElementById('LoadingCurrentUser');
            divLoading.style.display='none';
        }
        //Loading wait
        
        function setHeight(){
            document.getElementById('divUserSearch').style.height='390px';
        }
    </script>

</div>
<asp:UpdatePanel ID="UpdatePanel2" UpdateMode="Conditional" runat="Server">
    <ContentTemplate>
        <table cellpadding="0" cellspacing="0" style="width: 685px;">
            <tr>
                <td style="height:10px;">
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="searchUser"
                        Visible="False">
                        <Columns>
                        </Columns>
                    </asp:GridView>
                    <asp:SqlDataSource ID="searchUser" runat="server" ConnectionString="<%$ ConnectionStrings:ExpandITConnectionString %>">
                    </asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td style="height: 20px;">
                </td>
            </tr>
        </table>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="SearchBtn" />
        <asp:AsyncPostBackTrigger ControlID="ClearBtn" />
    </Triggers>
</asp:UpdatePanel>

<script language="javascript" type="text/javascript">
    function validatePhone(id) {
        
        var phoneField=id.value;
        if(phoneField.length >0) {
            if (!/^\d*$/.test(phoneField)) {
                  phoneField = phoneField.replace(/[^\d]/g,'');
            }
           if(phoneField.length == 10) {
              phoneField = "(" + phoneField.substring(0,3) + ") " + phoneField.substring(3, 6) + "-" + phoneField.substring(6);
              id.value=phoneField;
           }
       }
          
    }
</script>

<%--JA2010092101 - USER SEARCH CONTROL - END--%>
<%--AM2011031801 - ENTERPRISE CSR STAND ALONE - END--%>
