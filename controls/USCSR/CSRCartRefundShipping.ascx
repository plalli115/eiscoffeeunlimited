<!--AM2011031801 - ENTERPRISE CSR STAND ALONE - Start-->
<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CSRCartRefundShipping.ascx.vb"
    Inherits="CSRCartRefundShipping" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>

<%  
If CStrEx(Globals.OrderDict("DocumentType")) = "Return Order" And Not Globals.IsCartEmpty Then%>
    <br />
    <table>
        <tr>
            <td>
                <asp:CheckBox ID="RefundShipping" CssClass="refundShipping" AutoPostBack="true" runat="server"
                    Text="<%$Resources: Language,LABEL_REFOUND_SHIPPING %>" />
            </td>
        </tr>
    </table>
    <br />
<%End If%>
<!--AM2011031801 - ENTERPRISE CSR STAND ALONE - End-->
