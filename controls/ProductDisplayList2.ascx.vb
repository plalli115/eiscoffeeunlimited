Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass

Partial Class ProductDisplayList2
    Inherits ExpandIT.UserControl

    Private m_product As ExpDictionary
    Protected m_productclass As ProductClass

    <Themeable(False)> _
    Public Property Product() As ExpDictionary
        Get
            Return m_product
        End Get
        Set(ByVal value As ExpDictionary)
            m_product = value
        End Set
    End Property

    Protected Overloads Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender

        m_productclass = New ProductClass(m_product)
        m_productclass.GroupGuid = globals.GroupGuid
        fvProduct.DataSource = New ProductClass() {m_productclass}
        fvProduct.DataBind()
    End Sub

End Class
