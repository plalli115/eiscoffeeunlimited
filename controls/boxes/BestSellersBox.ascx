<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BestSellersBox.ascx.vb"
    Inherits="BestSellersBox" %>
<%@ Register TagPrefix="uc1" TagName="Box" Src="~/controls/Box.ascx" %>
<%@ Register Src="~/controls/BestSellers.ascx" TagName="BestSellers" TagPrefix="uc1" %>
<uc1:Box ID="BoxBestSellers" runat="server" IdPrefix="BestSellersBox">
    <headertemplate>        
        <asp:HyperLink ID="HyperLink1" runat="server">
        <%If method.ToUpper.Contains("MY") Then%>
        <%= Resources.Language.LABEL_MY_BESTSELLERS%>
        <%Else%>
        <%= Resources.Language.LABEL_BESTSELLERS%>
        <%End If%>
        </asp:HyperLink>
    </headertemplate>
    <itemtemplate>
    <%If method.ToUpper.Contains("MY") Then%>
            <uc1:BestSellers ID="BestSellers2" SetMethod="GetMyMostPopularItems" runat="server" />
    <%Else%>
            <uc1:BestSellers ID="BestSellers3" SetMethod="GetMostPopularItems" runat="server" />
    <%End If%>
        
    </itemtemplate>
</uc1:Box>
