<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CreditCardBox.ascx.vb"
    Inherits="controls_boxes_CreditCardBox" %>
<%@ Register TagPrefix="expCard" TagName="creditcards" Src="~/controls/CreditCards.ascx" %>
<%@ Register TagPrefix="expCard" TagName="Box" Src="~/controls/Box.ascx" %>
<expCard:Box ID="credCardBox" runat="server" IdPrefix="CreditCardBox">
    <headertemplate>
    <asp:Label ID="lblHeading" runat="server" Text="<%$Resources: Language, LABEL_SUPPORTED_CREDIT_CARDS %>"></asp:Label>
    </headertemplate>
    <itemtemplate>
<div class="creditCardBoxContent">
    <expCard:creditcards runat="server" ID="creditcards1" />
</div>
</itemtemplate>
</expCard:Box>
