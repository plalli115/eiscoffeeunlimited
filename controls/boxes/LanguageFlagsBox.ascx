<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LanguageFlagsBox.ascx.vb"
    Inherits="LanguageFlagsBox" %>
<%@ Register Src="~/controls/Box.ascx" TagName="Box" TagPrefix="uc1" %>
<%@ Register Src="~/controls/LanguageFlags.ascx" TagName="LanguageFlags" TagPrefix="uc1" %>
<%--<uc1:Box ID="LanguageFlagBox" runat="server">
    <headertemplate>
    <asp:HyperLink ID="HyperLink1" runat="server"><%=Resources.Language.LABEL_LANGUAGES%></asp:HyperLink>
    </headertemplate>
    <itemtemplate>
            <uc1:LanguageFlags ID="LanguageFlags1" runat="server" /> 
    </itemtemplate>
</uc1:Box>--%>
<%@ Register Src="~/controls/LanguageDropDown.ascx" TagName="LanguageDropDown" TagPrefix="uc1" %>
<uc1:Box ID="LanguageFlagBox" runat="server">
    <headertemplate>
    <asp:Label ID="lblHeading" runat="server" Text="<%$Resources: Language, LABEL_LANGUAGES %>"></asp:Label>
    </headertemplate>
    <itemtemplate>
        <div class="LanguageFlagsBoxContent">      
            <% If AppSettings("LanguageDisplay").Equals("Flags") Then%>      
                <uc1:LanguageFlags ID="LanguageFlags1" runat="server" />     
            <% Else%> 
                <uc1:LanguageDropDown runat="server" ID="test" />    
            <% End If%>   
        </div>
    </itemtemplate>
</uc1:Box>
<br class="BoxSeperator" />