<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BestSellersByCategoryBox.ascx.vb"
    Inherits="BestSellersBox" %>
<%@ Register TagPrefix="uc1" TagName="Box" Src="~/controls/Box.ascx" %>
<uc1:Box ID="BoxBestSellersByCategory" runat="server" IdPrefix="BestSellersByCategoryBox">
    <headertemplate>        
        <asp:HyperLink ID="HyperLink1" runat="server"><%= Resources.Language.LABEL_BESTSELLERS%></asp:HyperLink>
    </headertemplate>
    <itemtemplate>
            
    </itemtemplate>
</uc1:Box>
