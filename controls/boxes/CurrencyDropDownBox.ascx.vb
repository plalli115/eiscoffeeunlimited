Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.ExpandITLib

Partial Class CurrencyDropDownBox
    Inherits ExpandIT.UserControl

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)
        Me.Visible = CurrencyBoxLogicFactory.CreateInstance().isVisible(globals.User)
    End Sub

End Class
