<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CatalogTreeBox.ascx.vb"
    Inherits="CatalogTreeBox" %>
<%@ Register TagPrefix="uc1" TagName="Box" Src="~/controls/Box.ascx" %>
<%@ Register Src="~/controls/CatalogTree.ascx" TagName="CatalogTree" TagPrefix="uc1" %>
<uc1:Box id="BoxCatalogTree" runat="server" IdPrefix="CatalogTreeBox">
    <headertemplate>        
        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/catalog_overview.aspx"><%=Resources.Language.LABEL_CATALOG_OVERVIEW%></asp:HyperLink>
    </headertemplate>
    <itemtemplate>
        <uc1:CatalogTree ID="CatalogTree1" runat="server" />
    </itemtemplate>
</uc1:Box>
