<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CurrencyDropDownBox.ascx.vb"
    Inherits="CurrencyDropDownBox" %>
<%@ Register TagPrefix="uc1" TagName="Box" Src="~/controls/Box.ascx" %>
<%@ Register Src="~/controls/CurrencyDropDown.ascx" TagName="CurrencyDropDown" TagPrefix="uc1" %>
<%--<uc1:Box id="BoxCurrencyDropDown" runat="server" IdPrefix="CurrencyDropDownBox">
    <headertemplate>        
        <asp:HyperLink ID="HyperLink1" runat="server"><%=Resources.Language.LABEL_USERDATA_CURRENCY_1%></asp:HyperLink>
    </headertemplate>
    <itemtemplate>
       <uc1:CurrencyDropDown ID="CurrencyDropDown1" runat="server" />
    </itemtemplate>
</uc1:Box>--%>
<%@ Register Namespace="ExpandIT.SimpleUIControls" TagPrefix="expui" %>
<uc1:Box ID="CurrencyDropDown" runat="server" IdPrefix="CurrencyDropDownBox">
    <headertemplate>
    <expui:ExpLabel ID="lblHeading" runat="server" LabelText="LABEL_USERDATA_CURRENCY_1"></expui:ExpLabel>    
    </headertemplate>
    <itemtemplate>
        <div class="CurrencyDropDown">
            <uc1:CurrencyDropDown ID="CurrencyDropDown1" runat="server" />
        </div>
    </itemtemplate>
</uc1:Box>
<br class="BoxSeperator" />
