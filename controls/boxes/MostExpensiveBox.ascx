<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MostExpensiveBox.ascx.vb" Inherits="MostExpensiveBox" %>
<%@ Register Src="../ProductListVertical.ascx" TagName="ProductListVertical" TagPrefix="uc1" %>
  <uc1:ProductListVertical ID="ProductListVertical1" runat="server" maxitems="4" Heading="Most Expensive" 
                CacheKey="BoxMostExpensive" FilterMethod="Top"
                SelectStatement="SELECT ProductTable.ProductGuid FROM ProductTable INNER JOIN (SELECT DISTINCT TOP 100 PERCENT PropVal.PropOwnerRefGuid FROM PropVal INNER JOIN PropTrans ON PropVal.PropValGuid = PropTrans.PropValGuid WHERE (PropVal.PropGuid <> 'PICTURE2') AND (PropVal.PropOwnerTypeGuid = 'PRD') AND (NOT (PropTrans.PropTransText IS NULL)) ORDER BY PropVal.PropOwnerRefGuid) AS T1 ON ProductTable.ProductGuid = T1.PropOwnerRefGuid ORDER BY ProductTable.ListPrice DESC" />