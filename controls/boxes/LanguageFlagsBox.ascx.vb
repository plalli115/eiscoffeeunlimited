Imports System.Configuration.ConfigurationManager
Imports ExpandIT

Partial Class LanguageFlagsBox
    Inherits ExpandIT.UserControl

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)
        Me.Visible = New LanguageBoxLogic().isVisible(globals.User)
    End Sub

End Class
