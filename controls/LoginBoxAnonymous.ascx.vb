Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

Partial Class LoginBoxAnonymous
    Inherits ExpandIT.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Visible = CBoolEx(globals.User("Anonymous"))
    End Sub

    Protected Sub lbLogIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbLogIn.Click
        '<!--JA2010060101 - ALERTS - Start-->
        Session("LoadFirstTime") = ""
        '<!--JA2010060101 - ALERTS - End-->
        Context.Items.Add("password", CStrEx(Request("Password")))
        Context.Items.Add("login", CStrEx(Request("login")))
        If Not String.IsNullOrEmpty(Context.Request("bouncecheckout")) Then
            ViewState.Clear()
            Server.Transfer("~/_user_login.aspx")
        Else
            Server.Transfer("~/_user_login.aspx?returl=" & Context.Request.RawUrl)
        End If
    End Sub

    Protected Function userIdText()
        Dim text As String = "<input class=""borderTextBox"" style=""height: 13px;"" type=""text"" name=""login"" size=""20"" maxlength=""20"""
        text = text & " value = " & """" & Resources.Language.LABEL_USERDATA_LOGIN & """"
        text = text & " onfocus = ""this.value = (this.value=='" & Resources.Language.LABEL_USERDATA_LOGIN & "')? '' : this.value;"""
        text = text & " onblur=""this.value = (this.value=='')? '" & Resources.Language.LABEL_USERDATA_LOGIN & "' : this.value;""/>"
        Return text
    End Function

    Protected Function PasswordText()
        Dim text As String = "<input class=""borderTextBox"" style=""height: 13px;"" type=""password"" name=""Password"" size=""20"" maxlength=""20"""
        text = text & " value = " & """" & Resources.Language.LABEL_USERDATA_PASSWORD & """"
        text = text & " onfocus = ""this.value = (this.value=='" & Resources.Language.LABEL_USERDATA_PASSWORD & "')? '' : this.value;"""
        text = text & " onblur=""this.value = (this.value=='')? '" & Resources.Language.LABEL_USERDATA_PASSWORD & "' : this.value;""/>"
        Return text
    End Function


End Class
