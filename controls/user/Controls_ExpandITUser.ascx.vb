﻿Option Strict On
Imports ExpandIT.ExpandITLib
Imports System.Collections.Generic
Imports System.Configuration.ConfigurationManager
Imports ExpandIT31
Imports ExpandIT31.Logic
Imports ExpandIT31.ExpanditFramework
Imports ExpandIT31.ExpanditFramework.BLLBase
Imports ExpandIT31.ExpanditFramework.Util
Imports ExpandIT31.ExpanditFramework.Infrastructure

Namespace ExpandIT.WebUserControls

    Partial Class Controls_ExpandITUser
        Inherits ExpandIT.ShippingPaymentUserControl

        Private bllObject As UserBase 'The User object as Abstract Base Class
        Private validator As Core.IValidator 'The Validator object as Abstract Base Class
        Private confirmationMail As UserActionBase 'The confirmationMail object as Interface
        Private messageReturnString As String 'String used as QueryString parameter
        Private useConfirmedRegistration As Boolean = False
        'AM2010100101 - ADDRESS VALIDATION - Start
        Private AddressValidator As New AddressValidator()
        Protected responseData As New ExpDictionary
        Protected responseAddress As ExpDictionary
        'AM2010100101 - ADDRESS VALIDATION - End
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
        Protected csrObj As USCSR
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - End


        'The required fields as array. (Used by the validation control) Strings must match the property names of the User Class (Case Sensitive!)
        '--DJW 8/13/2012 Eliminated Contact Name From Being Displayed--%>
        'Private requiredFields As String() = {"ContactName", "Address1", "CityName", "ZipCode", "EmailAddress", "UserLogin", "UserPassword", "ConfirmedUserPassword", "StateName"}

        Private requiredFields As String() = {"Address1", "CityName", "ZipCode", "EmailAddress", "UserLogin", "UserPassword", "ConfirmedUserPassword", "StateName"}

        'The required fields label text as array. (Used by the validation control to add proper error messages in the correct language)
        '--DJW 8/13/2012 Eliminated Contact Name From Being Displayed--%>
        'Private requiredFieldsLabels As String() = {Resources.Language.LABEL_USERDATA_CONTACT, Resources.Language.LABEL_USERDATA_ADDRESS_1, _
        'Resources.Language.LABEL_USERDATA_CITY, Resources.Language.LABEL_USERDATA_ZIPCODE, Resources.Language.LABEL_USERDATA_EMAIL, _
        'Resources.Language.LABEL_USERDATA_LOGIN, Resources.Language.LABEL_USERDATA_PASSWORD, Resources.Language.LABEL_USERDATA_NEW_PASSWORD_CONFIRM}

        Private requiredFieldsLabels As String() = {Resources.Language.LABEL_USERDATA_ADDRESS_1, _
        Resources.Language.LABEL_USERDATA_CITY, Resources.Language.LABEL_USERDATA_ZIPCODE, Resources.Language.LABEL_USERDATA_EMAIL, _
        Resources.Language.LABEL_USERDATA_LOGIN, Resources.Language.LABEL_USERDATA_PASSWORD, Resources.Language.LABEL_USERDATA_NEW_PASSWORD_CONFIRM}

        ''' <summary>
        ''' Handles OnLoad Event
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
            'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
            csrObj = New USCSR(globals)
            'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            ' Disable Message1 ViewState
            Me.Message1.EnableViewState = False
            ' Set QueryString initial value to the empty string
            messageReturnString = String.Empty

            useConfirmedRegistration = Utilities.CBoolEx(ConfigurationManager.AppSettings("USE_MAIL_CONFIRMED_REGISTRATION"))

            ' Set visibility of currencies, languages and state controls.
            Try
                Dim isVisible As Boolean = CurrencyBoxLogicFactory.CreateInstance().isVisible(globals.User)
                CType(FormView1.FindControl("CurrencyPanel1"), HtmlTableRow).Visible = isVisible
                CType(FormView1.FindControl("CurrencyPanel2"), HtmlTableRow).Visible = isVisible
                CType(FormView1.FindControl("LanguagePanel1"), HtmlTableRow).Visible = New LanguageBoxLogic().isVisible(globals.User)
                CType(FormView1.FindControl("StateNamePanel"), HtmlTableRow).Visible = CBoolEx(AppSettings("SHOW_STATE_ON_SHIPPING_ADDRESS"))
            Catch ex As Exception

            End Try

            setRequiredFieldText()

            If Not Me.Page.IsPostBack Then
                requiredFieldInfo.Text = Resources.Language.LABEL_STARMARKED_INPUT_IS_REQUIRED
                If Not CBoolEx(globals.User("Anonymous")) Then
                    Me.PageHeader1.Text = Resources.Language.LABEL_USERDATA_UPDATE_USER_INFORMATION
                Else
                    Me.PageHeader1.Text = Resources.Language.LABEL_REGISTER_AS_NEW_USER
                    userRegisterInformation.Text = Resources.Language.LABEL_FILL_IN_FIELDS_TO_REGISTER
                End If
                Dim page1 As String = System.IO.Path.GetFileName(HttpContext.Current.Request.Url.AbsolutePath).ToLower
                If page1 = "user_new.aspx" Then
                    Dim ResidentialAddressCheckBox As CheckBox = CType(FormView1.FindControl("ResidentialAddressCheckBox"), CheckBox)
                    ResidentialAddressCheckBox.Checked = True
                End If
                'AM2010100101 - ADDRESS VALIDATION - Start
                If (CBoolEx(AppSettings("EXPANDIT_US_CSR_USE_ADDRESS_VALIDATION")) AndAlso csrObj.getSalesPersonGuid() <> "") OrElse eis.CheckPageAccess("AddressValidation") Then
                    Dim btnValidateAddress As Button = CType(FormView1.FindControl("btnValidateAddress"), Button)
                    btnValidateAddress.Visible = True
                    btnValidateAddress.Enabled = True
                Else
                    Dim updateButton As Button = CType(FormView1.FindControl("UpdateButton"), Button)
                    updateButton.Enabled = True
                    updateButton.Visible = True
                    'Set password fields to visible true
                    Dim langPassword1 As Label = CType(FormView1.FindControl("langPassword1"), Label)
                    langPassword1.Visible = True
                    Dim UserPasswordTextBox As TextBox = CType(FormView1.FindControl("UserPasswordTextBox"), TextBox)
                    UserPasswordTextBox.Visible = True
                    Dim lblUserPassword As Label = CType(FormView1.FindControl("lblUserPassword"), Label)
                    lblUserPassword.Visible = True
                    Dim langPassword2 As Label = CType(FormView1.FindControl("langPassword2"), Label)
                    langPassword2.Visible = True
                    Dim UserPassword2TextBox As TextBox = CType(FormView1.FindControl("UserPassword2TextBox"), TextBox)
                    UserPassword2TextBox.Visible = True
                    Dim lblConfirmedUserPassword As Label = CType(FormView1.FindControl("lblConfirmedUserPassword"), Label)
                    lblConfirmedUserPassword.Visible = True
                End If
                'AM2010100101 - ADDRESS VALIDATION - End
            End If
        End Sub

#Region "Object Creation"

        ''' <summary>
        ''' Handles ViewDataSource.ObjectCreating event
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks>The User, Validator and confirmationMail objects must be created by the PublicFactory</remarks>
        Protected Sub OnDataObjectCreating(ByVal sender As Object, ByVal e As ObjectDataSourceEventArgs) Handles ViewDataSource.ObjectCreating
            'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
            If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
                csrObj.userOnDataObjectCreating(bllObject)
            Else
                bllObject = ExpanditFactory.CreateUser(CStr(Session("UserGuid"))) 'Retreive the User object
            End If
            'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
            validator = ExpanditFactory.CreateUserValidator(bllObject, requiredFields, requiredFieldsLabels) 'Create a Validator object
            confirmationMail = ExpanditFactory.CreateUserMail(bllObject, globals.messages) 'Create a mail object to send confirmation email on successful update
            e.ObjectInstance = bllObject 'Register the User object with the ViewDataSource
        End Sub

#End Region

        ''' <summary>
        ''' Handles the ViewDataSource.Updated Event
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Public Sub ViewDataSource_Inserted(ByVal sender As Object, ByVal e As ObjectDataSourceStatusEventArgs) Handles ViewDataSource.Updated
            Dim retval As Dictionary(Of Integer, List(Of String)) = _
                CType(e.ReturnValue, Global.System.Collections.Generic.Dictionary(Of Integer, Global.System.Collections.Generic.List(Of String)))
            If retval IsNot Nothing Then
                If retval.ContainsKey(1) Then
                    globals.messages.Messages.Add(retval(1)(0))
                    messageReturnString = globals.messages.QueryString()
                End If
            End If
        End Sub

#Region "CurrencyDropDownLists Create Events"

        ''' <summary>
        ''' Handles CurrencyDropDownDataSource.ObjectCreating event
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="eventArgs"></param>
        ''' <remarks></remarks>
        Protected Sub OnCreate(ByVal sender As Object, ByVal eventArgs As ObjectDataSourceEventArgs) Handles CurrencyDropDownDataSource.ObjectCreating
            Try
                Dim cl As CurrencyControlLogic = CurrencyControlLogicFactory.CreateInstance(globals.User)
                eventArgs.ObjectInstance = cl
                If Context.Items("CurrencyGuid") Is Nothing Then
                    Context.Items("CurrencyGuid") = cl.SelectedValue
                End If
            Catch ex As Exception
                'Response.Write(ex.Message)
            End Try

        End Sub

        ''' <summary>
        ''' Handles CurrencyDropDownDataSource2.ObjectCreating event
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="eventArgs"></param>
        ''' <remarks></remarks>
        Protected Sub OnCreateSecondary(ByVal sender As Object, ByVal eventArgs As ObjectDataSourceEventArgs) Handles CurrencyDropDownDataSource2.ObjectCreating
            Try
                Dim cl As CurrencyControlLogic = CurrencyControlLogicFactory.CreateInstance(globals.User)
                eventArgs.ObjectInstance = cl
                If Context.Items("SecondaryCurrencyGuid") Is Nothing Then
                    Context.Items("SecondaryCurrencyGuid") = cl.SelectedSecondaryValue
                End If
            Catch ex As Exception
                'Response.Write(ex.Message)
            End Try
        End Sub

#End Region

#Region "DropDownLists Data Bound Events"

        ''' <summary>
        ''' Handles CurrencyDropDownList.DataBound event
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Protected Sub OnBound(ByVal sender As Object, ByVal e As EventArgs)
            Dim ddl As DropDownList
            ddl = CType(FormView1.FindControl("CurrencyDropDownList"), DropDownList)

            If Context.Items("CurrencyGuid") IsNot Nothing AndAlso ddl.Items.FindByValue(CStrEx(Context.Items("CurrencyGuid"))) IsNot Nothing Then
                ddl.SelectedValue = CStrEx(Context.Items("CurrencyGuid"))
            End If

        End Sub

        ''' <summary>
        ''' Handles SecondaryCurrencyDropDownList.DataBound event
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Protected Sub OnBoundSecondary(ByVal sender As Object, ByVal e As EventArgs)
            Dim ddl As DropDownList
            ddl = CType(FormView1.FindControl("SecondaryCurrencyDropDownList"), DropDownList)

            If Context.Items("SecondaryCurrencyGuid") IsNot Nothing AndAlso ddl.Items.FindByValue(CStrEx(Context.Items("SecondaryCurrencyGuid"))) IsNot Nothing Then
                ddl.SelectedValue = CStrEx(Context.Items("SecondaryCurrencyGuid"))
            End If

        End Sub

        ''' <summary>
        ''' Handles CountryDropDownList.DataBound event
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>

        '--DJW 8/10/2012 Eliminated Country From Being Displayed
        'Protected Sub OnCountryBound(ByVal sender As Object, ByVal e As EventArgs)
        '    Dim ddl As DropDownList
        '    ddl = CType(FormView1.FindControl("CountryDropDownList"), DropDownList)
        '    If String.IsNullOrEmpty(CStrEx(Context.Items("CountryGuid"))) Then
        '        If Not String.IsNullOrEmpty(CStrEx(globals.User("CountryGuid"))) Then
        '            ddl.SelectedValue = UCase(CStrEx(globals.User("CountryGuid")))
        '            ddl.Visible = False
        '        Else
        '            ddl.SelectedValue = UCase(AppSettings("DEFAULT_COUNTRYGUID"))
        '            ddl.Visible = False
        '        End If
        '    Else
        '        ddl.SelectedValue = UCase(CStrEx(Context.Items("CountryGuid")))
        '        ddl.Visible = False
        '    End If
        'End Sub

        ''' <summary>
        ''' Handles LanguageDropDownList.DataBound event
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Protected Sub OnLanguageBound(ByVal sender As Object, ByVal e As EventArgs)
            Dim ddl As DropDownList
            ddl = CType(FormView1.FindControl("LanguageDropDownList"), DropDownList)
            If String.IsNullOrEmpty(CStrEx(Session("LanguageGuid"))) Then
                If Not String.IsNullOrEmpty(CStrEx(globals.User("LanguageGuid"))) Then
                    ddl.SelectedValue = LCase(CStrEx(globals.User("LanguageGuid")))
                Else
                    ddl.SelectedValue = LCase(AppSettings("LANGUAGE_DEFAULT"))
                End If
            Else
                ddl.SelectedValue = LCase(CStrEx(Session("LanguageGuid")))
            End If
        End Sub

#End Region

#Region "FormView Events"

        ''' <summary>
        ''' Handles the FormView ItemUpdating Event. Collects the selected values from the DropDownLists
        ''' and adds them to the NewValues and OldValues collection before update. 
        ''' Also handles the StateName value since it's inside a serverside tr tag   
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Protected Sub OnItemUpdating(ByVal sender As Object, ByVal e As FormViewUpdateEventArgs) Handles FormView1.ItemUpdating
            Dim fView As FormView = CType(sender, FormView)
            Dim strCurrencyGuid As String = CType(fView.FindControl("CurrencyDropDownList"), DropDownList).SelectedValue
            Dim strSecondaryCurrencyGuid As String = CType(fView.FindControl("SecondaryCurrencyDropDownList"), DropDownList).SelectedValue

            '--DJW 8/10/2012 Eliminated Country From Being Displayed
            'Dim strCountryGuid As String = CType(fView.FindControl("CountryDropDownList"), DropDownList).SelectedValue

            Dim strLanguageGuid As String = CType(fView.FindControl("LanguageDropDownList"), DropDownList).SelectedValue
            Dim strStateName As String = CType(fView.FindControl("StateNameTextBox"), TextBox).Text

            'Collect the selected values from the dropdown lists and add them to the NewValues collection
            'so that they can be used in the insert/update operation
            e.NewValues("CurrencyGuid") = strCurrencyGuid
            e.NewValues("SecondaryCurrencyGuid") = strSecondaryCurrencyGuid

            '--DJW 8/10/2012 Eliminated Country From Being Displayed
            'e.NewValues("CountryGuid") = strCountryGuid

            e.NewValues("LanguageGuid") = strLanguageGuid
            e.NewValues("StateName") = strStateName

            'And add them also to the OldValues collection so they can be used again in case of an insert/update failure
            e.OldValues("CurrencyGuid") = strCurrencyGuid
            e.OldValues("SecondaryCurrencyGuid") = strSecondaryCurrencyGuid

            '--DJW 8/10/2012 Eliminated Country From Being Displayed
            'e.OldValues("CountryGuid") = strCountryGuid

            e.OldValues("LanguageGuid") = strLanguageGuid
            e.OldValues("StateName") = strStateName

        End Sub

        ''' <summary>
        ''' Handles the FormView ItemUpdated event
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Sub OnItemUpdated(ByVal sender As Object, ByVal e As FormViewUpdatedEventArgs)
            'If insert/update operation failed
            If e.Exception IsNot Nothing Then
                e.ExceptionHandled = True
                Try
                    If (e.Exception.InnerException IsNot Nothing) AndAlso (TypeOf e.Exception.InnerException Is UserValidationException) Then
                        'Add error messages from the UserValidationException to the PageMessages collection
                        Dim errList As List(Of String) = CType(e.Exception.InnerException, UserValidationException).ErrorList
                        globals.messages.Warnings.AddRange(errList)
                    Else
                        globals.messages.Errors.Add(e.Exception.Message)
                    End If
                Catch ex As Exception
                    globals.messages.Errors.Add(ex.Message)
                End Try
                'Insert/update failed so the dropdown lists will have to display the old values as selected
                'Here we put the values in Context.Item so we can pull them out on the dropdown lists data bound events

                '--DJW 8/10/2012 Eliminated Country From Being Displayed
                'Context.Items("CountryGuid") = e.OldValues("CountryGuid")

                Context.Items("SecondaryCurrencyGuid") = e.OldValues("SecondaryCurrencyGuid")
                Context.Items("CurrencyGuid") = e.OldValues("CurrencyGuid")
                Context.Items("StateName") = e.OldValues("StateName")
            Else
                'Insert/update succeeded so the dropdown lists will have to display the new values as selected
                'Here we put the values in Context.Item so we can pull them out on the dropdown lists data bound events

                '--DJW 8/10/2012 Eliminated Country From Being Displayed
                'Context.Items("CountryGuid") = e.NewValues("CountryGuid")

                Context.Items("SecondaryCurrencyGuid") = e.NewValues("SecondaryCurrencyGuid")
                Context.Items("CurrencyGuid") = e.NewValues("CurrencyGuid")
                Context.Items("StateName") = e.NewValues("StateName")
                'The LanguageGuid goes into the session object
                Session("LanguageGuid") = e.NewValues("LanguageGuid")
                'Redirect after insert/update
                'Changed behaviour, if configured, for new users reset auto login and redirect 
                Dim newValue As UserBase.UserStates
                'newValue = CType([Enum].Parse(GetType(UserBase.UserStates),Utilities.CStrEx(e.NewValues("Status"))), UserBase.UserStates) 
                newValue = bllObject.Status
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
                If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
                    csrObj.userOnItemUpdated(useConfirmedRegistration, newValue)
                Else
                    If useConfirmedRegistration AndAlso (newValue = UserBase.UserStates.New) Then
                        ResetAutoLogin()
                        Response.Redirect("~/user-confirm_reg.aspx")
                    Else
                        If Request.QueryString("bouncecheckout") = "true" Then
                            'Transfer to the url contained in the query string
                            Server.Transfer(Request.QueryString("bouncereturl"))
                        Else
                            'A little trick to reset the formview after a successful update/insert 
                            'The PageMessages are added to the Context.Items so they can be shown after redirection
                            Context.Items.Add("messages", globals.messages.ContextItems)
                            'AM2010102901 - USER NO INFO AFTER SAVE - Start
                            'Server.Transfer(Request.UrlReferrer.AbsolutePath & "?UserUpdated=1")
                            Server.Transfer("~/user.aspx?UserUpdated=1")
                            'AM2010102901 - USER NO INFO AFTER SAVE - End
                        End If
                    End If
                End If
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
                'Always keep in edit mode
                e.KeepInEditMode = True
            End If
        End Sub

#End Region

#Region "Set Required Fields"

        ''' <summary>
        ''' Sets the *, and enables the asp:requiredFieldValidators.
        ''' </summary>
        ''' <remarks></remarks>
        Sub setRequiredFieldText()
            Dim controls As Dictionary(Of String, Control) = ControlDict(Me, "Label")
            For Each item As Label In controls.Values
                If Array.IndexOf(requiredFields, Right(item.ID, item.ID.Length - 3)) >= 0 Then
                    item.Text = "*"
                End If
            Next
            controls.Clear()
            controls = ControlDict(Me, "RequiredFieldValidator")
            For Each item As RequiredFieldValidator In controls.Values
                If Array.IndexOf(requiredFields, Left(item.ID, item.ID.Length - 9)) >= 0 Then
                    item.Enabled = True
                End If
            Next
        End Sub

#End Region

#Region "Utility Functions"

        Private Sub ResetAutoLogin()
            ' Delete users cookie
            Session("UserGuid") = ""
            Response.Cookies("store")("UserGuid") = ""
            'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
            If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
                csrObj.setCSR2Cookie()
            End If
            'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
            Response.Cookies("store").Path = HttpContext.Current.Server.UrlPathEncode(VRoot) & "/"
            eis.UpdateMiniCartInfo(Nothing)
        End Sub

#End Region
        'AM2010100101 - ADDRESS VALIDATION - Start
        Public Sub btnValidateAddress_Clicked(ByVal sender As Object, ByVal e As EventArgs)
            'Validate Address

            '--DJW 8/13/2012 Eliminated Contact Name From Being Displayed--%>
            Dim ContactNameTextBox As TextBox = CType(FormView1.FindControl("ContactNameTextBox"), TextBox)

            Dim CompanyNameTextBox As TextBox = CType(FormView1.FindControl("CompanyNameTextBox"), TextBox)
            Dim Address1TextBox As TextBox = CType(FormView1.FindControl("Address1TextBox"), TextBox)
            Dim Address2TextBox As TextBox = CType(FormView1.FindControl("Address2TextBox"), TextBox)
            Dim CityNameTextBox As TextBox = CType(FormView1.FindControl("CityNameTextBox"), TextBox)
            Dim StateNameTextBox As TextBox = CType(FormView1.FindControl("StateNameTextBox"), TextBox)
            Dim ZipCodeTextBox As TextBox = CType(FormView1.FindControl("ZipCodeTextBox"), TextBox)

            '--DJW 8/10/2012 Eliminated Country From Being Displayed
            'Dim CountryDropDownList As DropDownList = CType(FormView1.FindControl("CountryDropDownList"), DropDownList)
            'CountryDropDownList.Visible = False

            Dim PhoneNoTextBox As TextBox = CType(FormView1.FindControl("PhoneNoTextBox"), TextBox)
            Dim EmailAddressTextBox As TextBox = CType(FormView1.FindControl("EmailAddressTextBox"), TextBox)
            Dim UserLoginTextBox As TextBox = CType(FormView1.FindControl("UserLoginTextBox"), TextBox)
            Dim ResidentialAddressCheckBox As CheckBox = CType(FormView1.FindControl("ResidentialAddressCheckBox"), CheckBox)

            '--DJW 8/13/2012 Eliminated Contact Name From Being Displayed--%>
            Dim name As String = ContactNameTextBox.Text

            Dim company As String = CompanyNameTextBox.Text
            Dim address1 As String = Address1TextBox.Text
            Dim address2 As String = Address2TextBox.Text
            Dim city As String = CityNameTextBox.Text
            Dim state As String = StateNameTextBox.Text
            Dim zipCode As String = ZipCodeTextBox.Text

            '--DJW 8/10/2012 Eliminated Country From Being Displayed
            Dim Country As String '= CountryDropDownList.SelectedValue
            Country = "US"

            Dim phone As String = PhoneNoTextBox.Text
            Dim userId As String = UserLoginTextBox.Text
            Dim residentialAddress As Boolean = ResidentialAddressCheckBox.Checked

            '--DJW 8/10/2012 Eliminated Country From Being Displayed
            'If Country = AppSettings("VALIDATE_ADDRESS_COUNTRY") Then
            If AppSettings("VALIDATE_ADDRESS_COUNTRY") = "US" Then
                responseAddress = CType(AddressValidator.AddressValidation(company, address1, address2, city, state, zipCode, Country, residentialAddress), ExpDictionary)
                If Not responseAddress Is Nothing Then
                    If responseAddress.Exists("ErrorResponse") Then
                        'There was an error with the address, don't show suggestion, display error message
                        globals.messages.Errors.Add(responseAddress("ErrorResponse").ToString)
                        If responseAddress("ErrorResponse").ToString <> Resources.Language.MESSAGE_ERROR_VALIDATING_ADDRESS Then
                            Me.AddressValidated.Visible = False
                            'Allow them to save the address without validation
                            Dim updateButton As Button = CType(FormView1.FindControl("UpdateButton"), Button)
                            updateButton.Enabled = True
                            updateButton.Visible = True
                            updateButton.Text = Resources.Language.LABEL_REGISTER_WITHOUT_VALIDATING_ADDRESS
                            'Allow them to revalidate the address, in case they modified it
                            Dim btnValidateAddress As Button = CType(FormView1.FindControl("btnValidateAddress"), Button)
                            btnValidateAddress.Visible = True
                            btnValidateAddress.Text = Resources.Language.LABEL_REVALIDATE_ADDRESS
                            'Set password fields to visible true
                            Dim langPassword1 As Label = CType(FormView1.FindControl("langPassword1"), Label)
                            langPassword1.Visible = True
                            Dim UserPasswordTextBox As TextBox = CType(FormView1.FindControl("UserPasswordTextBox"), TextBox)
                            UserPasswordTextBox.Visible = True
                            Dim lblUserPassword As Label = CType(FormView1.FindControl("lblUserPassword"), Label)
                            lblUserPassword.Visible = True
                            Dim langPassword2 As Label = CType(FormView1.FindControl("langPassword2"), Label)
                            langPassword2.Visible = True
                            Dim UserPassword2TextBox As TextBox = CType(FormView1.FindControl("UserPassword2TextBox"), TextBox)
                            UserPassword2TextBox.Visible = True
                            Dim lblConfirmedUserPassword As Label = CType(FormView1.FindControl("lblConfirmedUserPassword"), Label)
                            lblConfirmedUserPassword.Visible = True
                        End If
                    ElseIf responseAddress.Exists("MsgResponse") Then
                        'There was an issue with the address, and there is a suggestion, display message and suggestion
                        Dim autoModifyAddress As Boolean
                        autoModifyAddress = CBoolEx(AppSettings("AUTO_MODIFY_ADDRESS_ON_WARNING"))
                        If autoModifyAddress AndAlso eis.CheckPageAccess("AutomodifyWarning") Then
                            globals.messages.Messages.Add(Resources.Language.MESSAGE_ADDRESS_NORMALIZED_AFTER_WARNING)
                            Address1TextBox.Text = CStrEx(responseAddress("Address"))
                            CityNameTextBox.Text = CStrEx(responseAddress("City"))
                            StateNameTextBox.Text = CStrEx(responseAddress("State"))
                            ZipCodeTextBox.Text = CStrEx(responseAddress("Zip"))
                            'Allow the user to be saved
                            Me.AddressValidated.Visible = False
                            Dim updateButton As Button = CType(FormView1.FindControl("UpdateButton"), Button)
                            updateButton.Enabled = True
                            updateButton.Visible = True
                            updateButton.Text = Resources.Language.ACTION_SAVE
                            'Set password fields to visible true
                            Dim langPassword1 As Label = CType(FormView1.FindControl("langPassword1"), Label)
                            langPassword1.Visible = True
                            Dim UserPasswordTextBox As TextBox = CType(FormView1.FindControl("UserPasswordTextBox"), TextBox)
                            UserPasswordTextBox.Visible = True
                            Dim lblUserPassword As Label = CType(FormView1.FindControl("lblUserPassword"), Label)
                            lblUserPassword.Visible = True
                            Dim langPassword2 As Label = CType(FormView1.FindControl("langPassword2"), Label)
                            langPassword2.Visible = True
                            Dim UserPassword2TextBox As TextBox = CType(FormView1.FindControl("UserPassword2TextBox"), TextBox)
                            UserPassword2TextBox.Visible = True
                            Dim lblConfirmedUserPassword As Label = CType(FormView1.FindControl("lblConfirmedUserPassword"), Label)
                            lblConfirmedUserPassword.Visible = True
                            If csrObj.getSalesPersonGuid() <> "" And CStrEx(Session("NewUserGuid")) <> "" Then
                                findUserSameAddress()
                            End If
                        Else
                            globals.messages.Messages.Add(responseAddress("MsgResponse").ToString)
                            Me.AddressValidated.Visible = True
                            'Allow them to revalidate the address, in case they modified it
                            Dim btnValidateAddress As Button = CType(FormView1.FindControl("btnValidateAddress"), Button)
                            btnValidateAddress.Visible = True
                            btnValidateAddress.Text = Resources.Language.LABEL_REVALIDATE_ADDRESS
                        End If
                    ElseIf responseAddress.Exists("AddressVerified") Then
                        'Everything was fine with the address, and there was no suggestion or there was a suggestion that matched exactly the address
                        'Just show a successfull message and allow the user to be saved
                        globals.messages.Messages.Add(Resources.Language.MESSAGE_ADDRESS_VERIFIED_SUCCESSFULLY)
                        Me.AddressValidated.Visible = False
                        Dim updateButton As Button = CType(FormView1.FindControl("UpdateButton"), Button)
                        updateButton.Enabled = True
                        updateButton.Visible = True
                        updateButton.Text = Resources.Language.ACTION_SAVE
                        Dim btnValidateAddress As Button = CType(FormView1.FindControl("btnValidateAddress"), Button)
                        btnValidateAddress.Visible = False
                        'Set password fields to visible true
                        Dim langPassword1 As Label = CType(FormView1.FindControl("langPassword1"), Label)
                        langPassword1.Visible = True
                        Dim UserPasswordTextBox As TextBox = CType(FormView1.FindControl("UserPasswordTextBox"), TextBox)
                        UserPasswordTextBox.Visible = True
                        Dim lblUserPassword As Label = CType(FormView1.FindControl("lblUserPassword"), Label)
                        lblUserPassword.Visible = True
                        Dim langPassword2 As Label = CType(FormView1.FindControl("langPassword2"), Label)
                        langPassword2.Visible = True
                        Dim UserPassword2TextBox As TextBox = CType(FormView1.FindControl("UserPassword2TextBox"), TextBox)
                        UserPassword2TextBox.Visible = True
                        Dim lblConfirmedUserPassword As Label = CType(FormView1.FindControl("lblConfirmedUserPassword"), Label)
                        lblConfirmedUserPassword.Visible = True
                        If csrObj.getSalesPersonGuid() <> "" And CStrEx(Session("NewUserGuid")) <> "" Then
                            findUserSameAddress()
                        End If
                    End If
                End If
            Else
                globals.messages.Messages.Add(Resources.Language.MESSAGE_VALIDATE_OTHER_COUNTRY)
                Me.AddressValidated.Visible = False
                Dim updateButton As Button = CType(FormView1.FindControl("UpdateButton"), Button)
                updateButton.Enabled = True
                updateButton.Visible = True
                updateButton.Text = Resources.Language.ACTION_SAVE
                'Set password fields to visible true
                Dim langPassword1 As Label = CType(FormView1.FindControl("langPassword1"), Label)
                langPassword1.Visible = True
                Dim UserPasswordTextBox As TextBox = CType(FormView1.FindControl("UserPasswordTextBox"), TextBox)
                UserPasswordTextBox.Visible = True
                Dim lblUserPassword As Label = CType(FormView1.FindControl("lblUserPassword"), Label)
                lblUserPassword.Visible = True
                Dim langPassword2 As Label = CType(FormView1.FindControl("langPassword2"), Label)
                langPassword2.Visible = True
                Dim UserPassword2TextBox As TextBox = CType(FormView1.FindControl("UserPassword2TextBox"), TextBox)
                UserPassword2TextBox.Visible = True
                Dim lblConfirmedUserPassword As Label = CType(FormView1.FindControl("lblConfirmedUserPassword"), Label)
                lblConfirmedUserPassword.Visible = True
                If csrObj.getSalesPersonGuid() <> "" And CStrEx(Session("NewUserGuid")) <> "" Then
                    findUserSameAddress()
                End If
            End If

        End Sub

        Public Sub findUserSameAddress()

            Dim address1 As String = CType(FormView1.FindControl("Address1TextBox"), TextBox).Text
            Dim state As String = CType(FormView1.FindControl("StateNameTextBox"), TextBox).Text
            Dim zipCode As String = CType(FormView1.FindControl("ZipCodeTextBox"), TextBox).Text

            '--DJW 8/13/2012 Eliminated Contact Name From Being Displayed--%>
            Dim sql As String = "SELECT UserGuid,ContactName,UserLogin,EmailAddress FROM UserTable WHERE Address1 LIKE " & SafeString(address1) & " AND StateName LIKE " & SafeString(state) & " AND ZipCode LIKE " & SafeString(zipCode) & "UNION SELECT U.UserGuid,U.ContactName,U.UserLogin,U.EmailAddress FROM UserTableB2B U INNER JOIN CustomerTable C ON U.CustomerGuid=C.CustomerGuid WHERE C.Address1 LIKE " & SafeString(address1) & " AND C.StateName LIKE " & SafeString(state) & " AND C.ZipCode LIKE " & SafeString(zipCode) & " AND U.UserGuid NOT IN (SELECT UserGuid FROM UserTable)"

            'Dim sql As String = "SELECT UserGuid,UserLogin,EmailAddress FROM UserTable WHERE Address1 LIKE " & SafeString(address1) & " AND StateName LIKE " & SafeString(state) & " AND ZipCode LIKE " & SafeString(zipCode) & "UNION SELECT U.UserGuid,U.UserLogin,U.EmailAddress FROM UserTableB2B U INNER JOIN CustomerTable C ON U.CustomerGuid=C.CustomerGuid WHERE C.Address1 LIKE " & SafeString(address1) & " AND C.StateName LIKE " & SafeString(state) & " AND C.ZipCode LIKE " & SafeString(zipCode) & " AND U.UserGuid NOT IN (SELECT UserGuid FROM UserTable)"
            Dim retval As ExpDictionary = SQL2Dicts(sql)
            If Not retval Is Nothing Then
                If retval.Count > 0 Then
                    Me.findUser.SelectCommand = sql
                    Me.GridView1.DataBind()
                    Me.GridView1.Visible = True
                    globals.messages.Errors.Add(AppSettings("SAME_ADDRESS_VALIDATOR"))
                End If
            End If

            'Update Button Enabled
            Dim updateButton As Button = CType(FormView1.FindControl("UpdateButton"), Button)
            updateButton.Enabled = True
            updateButton.Visible = True

        End Sub

        Protected Sub UpdateAddress_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles UpdateAddress.Click

            '--DJW 8/13/2012 Eliminated Contact Name From Being Displayed--%>
            Dim ContactNameTextBox As TextBox = CType(FormView1.FindControl("ContactNameTextBox"), TextBox)

            Dim CompanyNameTextBox As TextBox = CType(FormView1.FindControl("CompanyNameTextBox"), TextBox)
            Dim Address1TextBox As TextBox = CType(FormView1.FindControl("Address1TextBox"), TextBox)
            Dim Address2TextBox As TextBox = CType(FormView1.FindControl("Address2TextBox"), TextBox)
            Dim CityNameTextBox As TextBox = CType(FormView1.FindControl("CityNameTextBox"), TextBox)
            Dim StateNameTextBox As TextBox = CType(FormView1.FindControl("StateNameTextBox"), TextBox)
            Dim ZipCodeTextBox As TextBox = CType(FormView1.FindControl("ZipCodeTextBox"), TextBox)

            '--DJW 8/10/2012 Eliminated Country From Being Displayed
            ' Dim CountryDropDownList As DropDownList = CType(FormView1.FindControl("CountryDropDownList"), DropDownList)

            Dim PhoneNoTextBox As TextBox = CType(FormView1.FindControl("PhoneNoTextBox"), TextBox)
            Dim EmailAddressTextBox As TextBox = CType(FormView1.FindControl("EmailAddressTextBox"), TextBox)
            Dim UserLoginTextBox As TextBox = CType(FormView1.FindControl("UserLoginTextBox"), TextBox)
            Dim ResidentialAddressCheckBox As CheckBox = CType(FormView1.FindControl("ResidentialAddressCheckBox"), CheckBox)

            '--DJW 8/13/2012 Eliminated Contact Name From Being Displayed--%>
            Dim name As String = ContactNameTextBox.Text

            Dim company As String = CompanyNameTextBox.Text
            Dim address1 As String = Address1TextBox.Text
            Dim address2 As String = Address2TextBox.Text
            Dim city As String = CityNameTextBox.Text
            Dim state As String = StateNameTextBox.Text
            Dim zipCode As String = ZipCodeTextBox.Text

            ''--DJW 8/10/2012 Eliminated Country From Being Displayed
            Dim Country As String '= CountryDropDownList.SelectedValue
            Country = "US"

            Dim phone As String = PhoneNoTextBox.Text
            Dim userId As String = UserLoginTextBox.Text
            Dim residentialAddress As Boolean = ResidentialAddressCheckBox.Checked

            'Password visible true
            Dim langPassword1 As Label = CType(FormView1.FindControl("langPassword1"), Label)
            langPassword1.Visible = True
            Dim UserPasswordTextBox As TextBox = CType(FormView1.FindControl("UserPasswordTextBox"), TextBox)
            UserPasswordTextBox.Visible = True
            Dim lblUserPassword As Label = CType(FormView1.FindControl("lblUserPassword"), Label)
            lblUserPassword.Visible = True
            Dim langPassword2 As Label = CType(FormView1.FindControl("langPassword2"), Label)
            langPassword2.Visible = True
            Dim UserPassword2TextBox As TextBox = CType(FormView1.FindControl("UserPassword2TextBox"), TextBox)
            UserPassword2TextBox.Visible = True
            Dim lblConfirmedUserPassword As Label = CType(FormView1.FindControl("lblConfirmedUserPassword"), Label)
            lblConfirmedUserPassword.Visible = True

            'Validate Address Button visible false
            Dim btnValidateAddress As Button = CType(FormView1.FindControl("btnValidateAddress"), Button)
            btnValidateAddress.Visible = False

            Me.AddressValidated.Visible = False
            responseAddress = CType(AddressValidator.AddressValidation(company, address1, address2, city, state, zipCode, Country, residentialAddress), ExpDictionary)
            If Not responseAddress Is Nothing Then

                '--DJW 8/13/2012 Eliminated Contact Name From Being Displayed--%>
                ContactNameTextBox.Text = name

                CompanyNameTextBox.Text = company
                Address1TextBox.Text = responseAddress("Address").ToString
                Address2TextBox.Text = ""
                CityNameTextBox.Text = responseAddress("City").ToString
                StateNameTextBox.Text = responseAddress("State").ToString
                ZipCodeTextBox.Text = responseAddress("Zip").ToString

                ''--DJW 8/10/2012 Eliminated Country From Being Displayed
                'CountryDropDownList.SelectedValue = Country

                PhoneNoTextBox.Text = phone
                UserLoginTextBox.Text = userId
                ResidentialAddressCheckBox.Checked = residentialAddress

            End If

            If csrObj.getSalesPersonGuid() <> "" And CStrEx(Session("NewUserGuid")) <> "" Then
                findUserSameAddress()
            Else
                'Update Button Enabled
                Dim updateButton As Button = CType(FormView1.FindControl("UpdateButton"), Button)
                updateButton.Enabled = True
                updateButton.Visible = True
                updateButton.Text = Resources.Language.ACTION_SAVE
            End If
        End Sub

        Protected Sub Keep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnKeep.Click

            'Password visible true
            Dim langPassword1 As Label = CType(FormView1.FindControl("langPassword1"), Label)
            langPassword1.Visible = True
            Dim UserPasswordTextBox As TextBox = CType(FormView1.FindControl("UserPasswordTextBox"), TextBox)
            UserPasswordTextBox.Visible = True
            Dim lblUserPassword As Label = CType(FormView1.FindControl("lblUserPassword"), Label)
            lblUserPassword.Visible = True
            Dim langPassword2 As Label = CType(FormView1.FindControl("langPassword2"), Label)
            langPassword2.Visible = True
            Dim UserPassword2TextBox As TextBox = CType(FormView1.FindControl("UserPassword2TextBox"), TextBox)
            UserPassword2TextBox.Visible = True
            Dim lblConfirmedUserPassword As Label = CType(FormView1.FindControl("lblConfirmedUserPassword"), Label)
            lblConfirmedUserPassword.Visible = True

            'Validate Address Button visible false
            Dim btnValidateAddress As Button = CType(FormView1.FindControl("btnValidateAddress"), Button)
            btnValidateAddress.Visible = False

            Me.AddressValidated.Visible = False
            Me.UpdateAddress.Enabled = True
            'Find similar users
            If csrObj.getSalesPersonGuid() <> "" And CStrEx(Session("NewUserGuid")) <> "" Then
                findUserSameAddress()
            Else
                'Update Button Enabled
                Dim updateButton As Button = CType(FormView1.FindControl("UpdateButton"), Button)
                updateButton.Enabled = True
                updateButton.Visible = True
                updateButton.Text = Resources.Language.ACTION_SAVE
            End If
        End Sub
        'AM2010100101 - ADDRESS VALIDATION - End
    End Class

End Namespace