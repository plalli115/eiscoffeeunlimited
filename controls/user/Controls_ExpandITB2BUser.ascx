﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Controls_ExpandITB2BUser.ascx.vb"
    Inherits="controls_Controls_ExpandITB2BUser" ClassName="Controls_ExpandITB2BUser" %>
<asp:FormView ID="FormView1" runat="server" DataSourceID="ViewDataSource">
    <ItemTemplate>
        <div class="userNewText">
            <p>
            </p>
            <table border="0" cellspacing="0">
            
                 <%-- --DJW 8/13/2012 Hiding Contact Name From Being Displayed--%>
                <tr class="TR1" visible="false">
                    <td class="TDR" visible="false">
                        <asp:Label ID="langContactName" runat="server" visible="false" Text='<%$Resources: Language,LABEL_USERDATA_CONTACT %>' />:
                    </td>
                    <td class="TDL" visible="false">
                        <asp:Label ID="ContactNameLabel" runat="server" Visible="false" Text='<%#Bind("ContactName") %>' />
                    </td>
                    <td visible="false">
                    </td>
                </tr>   
                
                <tr class="TR1">
                    <td class="TDR">
                        <asp:Label ID="langCompanyName" runat="server" Text='<%$Resources: Language,LABEL_USERDATA_COMPANY %>' />:
                    </td>
                    <td class="TDL">
                        <asp:Label ID="CompanyNameLabel" runat="server" Text='<%#Bind("CompanyName") %>' />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr class="TR1">
                    <td class="TDR">
                        <asp:Label ID="langAddress1" runat="server" Text='<%$Resources: Language,LABEL_USERDATA_ADDRESS_1 %>' />:
                    </td>
                    <td class="TDL">
                        <asp:Label ID="Address1" runat="server" Text='<%# Bind("Address1") %>' />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr class="TR1">
                    <td class="TDR">
                        <asp:Label ID="langAddress2" runat="server" Text='<%$Resources: Language,LABEL_USERDATA_ADDRESS_2 %>' />:
                    </td>
                    <td class="TDL">
                        <asp:Label ID="Address2Label" runat="server" Text='<%# Bind("Address2") %>' />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr class="TR1">
                    <td class="TDR">
                        <asp:Label ID="langCityName" runat="server" Text='<%$Resources: Language,LABEL_USERDATA_CITY %>' />:
                    </td>
                    <td class="TDL">
                        <asp:Label ID="CityNameLabel" runat="server" Text='<%# Bind("CityName") %>' />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr runat="server" id="StateNamePanel" class="TR1">
                    <td class="TDR">
                        <asp:Label ID="langStateName" runat="server" Text='<%$Resources: Language,LABEL_USERDATA_STATE %>' />:
                    </td>
                    <td class="TDL">
                        <asp:Label ID="StateNameLabel" runat="server" Text='<%# Bind("StateName") %>' />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr class="TR1">
                    <td class="TDR">
                        <asp:Label ID="langZipCode" runat="server" Text='<%$Resources: Language,LABEL_USERDATA_ZIPCODE %>' />:
                    </td>
                    <td class="TDL">
                        <asp:Label ID="ZipCodeLabel" runat="server" Text='<%# Bind("ZipCode") %>' />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr class="TR1">
                    <td class="TDR">
                        <asp:Label ID="Label8" runat="server" Text='<%$Resources: Language,LABEL_USERDATA_COUNTRY %>' />:
                    </td>
                    <td class="TDL">
                        <asp:Label ID="CountryGuidLabel" runat="server" Text='<%# CountryGuid2Text(Eval("CountryGuid")) %>' />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr runat="server" id="CurrencyPanel1" class="TR1">
                    <td class="TDR">
                        <asp:Label ID="Label9" runat="server" Text='<%$Resources: Language,LABEL_USERDATA_CURRENCY_1 %>' />:
                    </td>
                    <td class="TDL">
                        <asp:Label ID="CurrencyGuidLabel" runat="server" Text='<%# CurrencyGuid2Text(Eval("CurrencyGuid")) %>' />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr runat="server" id="CurrencyPanel2" class="TR1">
                    <td class="TDR">
                        <asp:Label ID="Label10" runat="server" Text='<%$Resources: Language,LABEL_USERDATA_CURRENCY_2 %>' />:
                    </td>
                    <td class="TDL">
                        <asp:Label ID="SecondaryCurrencyGuidLabel" runat="server" Text='<%# CurrencyGuid2Text(Eval("SecondaryCurrencyGuid")) %>' />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr runat="server" id="LanguagePanel1" class="TR1">
                    <td class="TDR">
                        <asp:Label ID="Label11" runat="server" Text='<%$Resources: Language,LABEL_SITE_LANGUAGE %>' />:
                    </td>
                    <td class="TDL">
                        <asp:Label ID="LanguageGuidLabel" runat="server" Text='<%# languageGuid2Text(Eval("LanguageGuid")) %>' />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr class="TR1">
                    <td class="TDR">
                        <asp:Label ID="langPhoneNo" runat="server" Text='<%$Resources: Language,LABEL_USERDATA_PHONE %>' />:
                    </td>
                    <td class="TDL">
                        <asp:Label ID="PhoneNoLabel" runat="server" Text='<%# Bind("PhoneNo") %>' />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr class="TR1">
                    <td class="TDR">
                        <asp:Label ID="langEmailAddress" runat="server" Text='<%$Resources: Language,LABEL_USERDATA_EMAIL %>' />:
                    </td>
                    <td class="TDL">
                        <asp:Label ID="EmailAddressLabel" runat="server" Text='<%# Bind("EmailAddress") %>' />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr class="TR1">
                    <td class="TDR">
                        <asp:Label ID="langUserLogin" runat="server" Text='<%$Resources: Language,LABEL_USERDATA_LOGIN %>' /><asp:Literal
                            ID="litLogIn" runat="server" Text=":" />
                    </td>
                    <td class="TDL">
                        <asp:Label ID="UserLoginLabel" runat="server" Text='<%# Bind("UserLogin") %>' />
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
    </ItemTemplate>
</asp:FormView>
<asp:ObjectDataSource ID="ViewDataSource" runat="server" OldValuesParameterFormatString="original_{0}"
    SelectMethod="GetData" OnObjectCreating="OnDataObjectCreating" TypeName="ExpandIT31.ExpanditFramework.BLLBase.UserB2BBase">
    <SelectParameters>
        <asp:SessionParameter Name="userGuid" SessionField="UserGuid" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
