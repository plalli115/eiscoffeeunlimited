﻿Option Strict On
Imports ExpandIT.ExpandITLib
Imports System.Collections.Generic
Imports System.Configuration.ConfigurationManager
Imports ExpandIT31
Imports ExpandIT31.Logic
Imports ExpandIT31.ExpanditFramework
Imports ExpandIT31.ExpanditFramework.BLLBase
Imports ExpandIT31.ExpanditFramework.Util
Imports ExpandIT31.ExpanditFramework.Infrastructure

Namespace ExpandIT.WebUserControls

    Partial Class bkControls_ExpandITUser
        Inherits ExpandIT.ShippingPaymentUserControl

        Private bllObject As UserBase 'The User object as Abstract Base Class
        Private validator As Core.IValidator 'The Validator object as Abstract Base Class
        Private confirmationMail As UserActionBase 'The confirmationMail object as Interface
        Private messageReturnString As String 'String used as QueryString parameter
        Private useConfirmedRegistration As Boolean = False

        'The required fields as array. (Used by the validation control) Strings must match the property names of the User Class (Case Sensitive!)
        Private requiredFields As String() = {"Address1", "CityName", "ZipCode", "EmailAddress", "UserLogin", "UserPassword", "ConfirmedUserPassword"}

        '--DJW 8/13/2012 Eliminated Contact Name From Being Displayed--%>
        'Private requiredFields As String() = {"ContactName", "Address1", "CityName", "ZipCode", "EmailAddress", "UserLogin", "UserPassword", "ConfirmedUserPassword"}

        'The required fields label text as array. (Used by the validation control to add proper error messages in the correct language)
        Private requiredFieldsLabels As String() = {Resources.Language.LABEL_USERDATA_ADDRESS_1, _
       Resources.Language.LABEL_USERDATA_CITY, Resources.Language.LABEL_USERDATA_ZIPCODE, Resources.Language.LABEL_USERDATA_EMAIL, _
       Resources.Language.LABEL_USERDATA_LOGIN, Resources.Language.LABEL_USERDATA_PASSWORD, Resources.Language.LABEL_USERDATA_NEW_PASSWORD_CONFIRM}

        '--DJW 8/13/2012 Eliminated Contact Name From Being Displayed--%>
        'Private requiredFieldsLabels As String() = {Resources.Language.LABEL_USERDATA_CONTACT, Resources.Language.LABEL_USERDATA_ADDRESS_1, _
        'Resources.Language.LABEL_USERDATA_CITY, Resources.Language.LABEL_USERDATA_ZIPCODE, Resources.Language.LABEL_USERDATA_EMAIL, _
        'Resources.Language.LABEL_USERDATA_LOGIN, Resources.Language.LABEL_USERDATA_PASSWORD, Resources.Language.LABEL_USERDATA_NEW_PASSWORD_CONFIRM}

        ''' <summary>
        ''' Handles OnLoad Event
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            ' Disable Message1 ViewState
            Me.Message1.EnableViewState = False
            ' Set QueryString initial value to the empty string
            messageReturnString = String.Empty

            useConfirmedRegistration = Utilities.CBoolEx(ConfigurationManager.AppSettings("USE_MAIL_CONFIRMED_REGISTRATION"))

            ' Set visibility of currencies, languages and state controls.
            Try
                Dim isVisible As Boolean = CurrencyBoxLogicFactory.CreateInstance().isVisible(globals.User)
                CType(FormView1.FindControl("CurrencyPanel1"), HtmlTableRow).Visible = isVisible
                CType(FormView1.FindControl("CurrencyPanel2"), HtmlTableRow).Visible = isVisible
                CType(FormView1.FindControl("LanguagePanel1"), HtmlTableRow).Visible = New LanguageBoxLogic().isVisible(globals.User)
                CType(FormView1.FindControl("StateNamePanel"), HtmlTableRow).Visible = CBoolEx(AppSettings("SHOW_STATE_ON_SHIPPING_ADDRESS"))
            Catch ex As Exception

            End Try

            setRequiredFieldText()

            If Not Me.Page.IsPostBack Then
                requiredFieldInfo.Text = Resources.Language.LABEL_STARMARKED_INPUT_IS_REQUIRED
                If Not CBoolEx(globals.User("Anonymous")) Then
                    Me.PageHeader1.Text = Resources.Language.LABEL_USERDATA_UPDATE_USER_INFORMATION
                Else
                    Me.PageHeader1.Text = Resources.Language.LABEL_REGISTER_AS_NEW_USER
                    userRegisterInformation.Text = Resources.Language.LABEL_FILL_IN_FIELDS_TO_REGISTER
                End If
            End If
        End Sub

#Region "Object Creation"

        ''' <summary>
        ''' Handles ViewDataSource.ObjectCreating event
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks>The User, Validator and confirmationMail objects must be created by the PublicFactory</remarks>
        Protected Sub OnDataObjectCreating(ByVal sender As Object, ByVal e As ObjectDataSourceEventArgs) Handles ViewDataSource.ObjectCreating

            bllObject = ExpanditFactory.CreateUser(CStr(Session("UserGuid"))) 'Retreive the User object
            validator = ExpanditFactory.CreateUserValidator(bllObject, requiredFields, requiredFieldsLabels) 'Create a Validator object
            confirmationMail = ExpanditFactory.CreateUserMail(bllObject, globals.messages) 'Create a mail object to send confirmation email on successful update
            e.ObjectInstance = bllObject 'Register the User object with the ViewDataSource

        End Sub

#End Region

        ''' <summary>
        ''' Handles the ViewDataSource.Updated Event
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Public Sub ViewDataSource_Inserted(ByVal sender As Object, ByVal e As ObjectDataSourceStatusEventArgs) Handles ViewDataSource.Updated
            Dim retval As Dictionary(Of Integer, List(Of String)) = _
                CType(e.ReturnValue, Global.System.Collections.Generic.Dictionary(Of Integer, Global.System.Collections.Generic.List(Of String)))
            If retval IsNot Nothing Then
                If retval.ContainsKey(1) Then
                    globals.messages.Messages.Add(retval(1)(0))
                    messageReturnString = globals.messages.QueryString()
                End If
            End If
        End Sub

#Region "CurrencyDropDownLists Create Events"

        ''' <summary>
        ''' Handles CurrencyDropDownDataSource.ObjectCreating event
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="eventArgs"></param>
        ''' <remarks></remarks>
        Protected Sub OnCreate(ByVal sender As Object, ByVal eventArgs As ObjectDataSourceEventArgs) Handles CurrencyDropDownDataSource.ObjectCreating
            Try
                Dim cl As CurrencyControlLogic = CurrencyControlLogicFactory.CreateInstance(globals.User)
                eventArgs.ObjectInstance = cl
                If Context.Items("CurrencyGuid") Is Nothing Then
                    Context.Items("CurrencyGuid") = cl.SelectedValue
                End If
            Catch ex As Exception
                'Response.Write(ex.Message)
            End Try

        End Sub

        ''' <summary>
        ''' Handles CurrencyDropDownDataSource2.ObjectCreating event
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="eventArgs"></param>
        ''' <remarks></remarks>
        Protected Sub OnCreateSecondary(ByVal sender As Object, ByVal eventArgs As ObjectDataSourceEventArgs) Handles CurrencyDropDownDataSource2.ObjectCreating
            Try
                Dim cl As CurrencyControlLogic = CurrencyControlLogicFactory.CreateInstance(globals.User)
                eventArgs.ObjectInstance = cl
                If Context.Items("SecondaryCurrencyGuid") Is Nothing Then
                    Context.Items("SecondaryCurrencyGuid") = cl.SelectedSecondaryValue
                End If
            Catch ex As Exception
                'Response.Write(ex.Message)
            End Try
        End Sub

#End Region

#Region "DropDownLists Data Bound Events"

        ''' <summary>
        ''' Handles CurrencyDropDownList.DataBound event
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Protected Sub OnBound(ByVal sender As Object, ByVal e As EventArgs)
            Dim ddl As DropDownList
            ddl = CType(FormView1.FindControl("CurrencyDropDownList"), DropDownList)

            If Context.Items("CurrencyGuid") IsNot Nothing AndAlso ddl.Items.FindByValue(CStrEx(Context.Items("CurrencyGuid"))) IsNot Nothing Then
                ddl.SelectedValue = CStrEx(Context.Items("CurrencyGuid"))
            End If

        End Sub

        ''' <summary>
        ''' Handles SecondaryCurrencyDropDownList.DataBound event
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Protected Sub OnBoundSecondary(ByVal sender As Object, ByVal e As EventArgs)
            Dim ddl As DropDownList
            ddl = CType(FormView1.FindControl("SecondaryCurrencyDropDownList"), DropDownList)

            If Context.Items("SecondaryCurrencyGuid") IsNot Nothing AndAlso ddl.Items.FindByValue(CStrEx(Context.Items("SecondaryCurrencyGuid"))) IsNot Nothing Then
                ddl.SelectedValue = CStrEx(Context.Items("SecondaryCurrencyGuid"))
            End If

        End Sub

        ''' <summary>
        ''' Handles CountryDropDownList.DataBound event
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>

        'DJW 8/10/2012 Eliminated Country From Being Displayed
        'Protected Sub OnCountryBound(ByVal sender As Object, ByVal e As EventArgs)
        '    Dim ddl As DropDownList
        '    ddl = CType(FormView1.FindControl("CountryDropDownList"), DropDownList)
        '    If String.IsNullOrEmpty(CStrEx(Context.Items("CountryGuid"))) Then
        '        If Not String.IsNullOrEmpty(CStrEx(globals.User("CountryGuid"))) Then
        '            ddl.SelectedValue = UCase(CStrEx(globals.User("CountryGuid")))
        '            ddl.Visible = False
        '        Else
        '            ddl.SelectedValue = UCase(AppSettings("DEFAULT_COUNTRYGUID"))
        '            ddl.Visible = False
        '        End If
        '    Else
        '        ddl.SelectedValue = UCase(CStrEx(Context.Items("CountryGuid")))
        '        ddl.Visible = False
        '    End If
        'End Sub

        ''' <summary>
        ''' Handles LanguageDropDownList.DataBound event
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Protected Sub OnLanguageBound(ByVal sender As Object, ByVal e As EventArgs)
            Dim ddl As DropDownList
            ddl = CType(FormView1.FindControl("LanguageDropDownList"), DropDownList)
            If String.IsNullOrEmpty(CStrEx(Session("LanguageGuid"))) Then
                If Not String.IsNullOrEmpty(CStrEx(globals.User("LanguageGuid"))) Then
                    ddl.SelectedValue = LCase(CStrEx(globals.User("LanguageGuid")))
                Else
                    ddl.SelectedValue = LCase(AppSettings("LANGUAGE_DEFAULT"))
                End If
            Else
                ddl.SelectedValue = LCase(CStrEx(Session("LanguageGuid")))
            End If
        End Sub

#End Region

#Region "FormView Events"

        ''' <summary>
        ''' Handles the FormView ItemUpdating Event. Collects the selected values from the DropDownLists
        ''' and adds them to the NewValues and OldValues collection before update. 
        ''' Also handles the StateName value since it's inside a serverside tr tag   
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Protected Sub OnItemUpdating(ByVal sender As Object, ByVal e As FormViewUpdateEventArgs) Handles FormView1.ItemUpdating
            Dim fView As FormView = CType(sender, FormView)
            Dim strCurrencyGuid As String = CType(fView.FindControl("CurrencyDropDownList"), DropDownList).SelectedValue
            Dim strSecondaryCurrencyGuid As String = CType(fView.FindControl("SecondaryCurrencyDropDownList"), DropDownList).SelectedValue

            'DJW 8/10/2012 Eliminated Country From Being Displayed
            'Dim strCountryGuid As String = CType(fView.FindControl("CountryDropDownList"), DropDownList).SelectedValue

            Dim strLanguageGuid As String = CType(fView.FindControl("LanguageDropDownList"), DropDownList).SelectedValue
            Dim strStateName As String = CType(fView.FindControl("StateNameTextBox"), TextBox).Text

            'Collect the selected values from the dropdown lists and add them to the NewValues collection
            'so that they can be used in the insert/update operation
            e.NewValues("CurrencyGuid") = strCurrencyGuid
            e.NewValues("SecondaryCurrencyGuid") = strSecondaryCurrencyGuid

            '--DJW 8/10/2012 Eliminated Country From Being Displayed
            'e.NewValues("CountryGuid") = strCountryGuid

            e.NewValues("LanguageGuid") = strLanguageGuid
            e.NewValues("StateName") = strStateName

            'And add them also to the OldValues collection so they can be used again in case of an insert/update failure
            e.OldValues("CurrencyGuid") = strCurrencyGuid
            e.OldValues("SecondaryCurrencyGuid") = strSecondaryCurrencyGuid

            '--DJW 8/10/2012 Eliminated Country From Being Displayed
            'e.OldValues("CountryGuid") = strCountryGuid

            e.OldValues("LanguageGuid") = strLanguageGuid
            e.OldValues("StateName") = strStateName

        End Sub

        ''' <summary>
        ''' Handles the FormView ItemUpdated event
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Sub OnItemUpdated(ByVal sender As Object, ByVal e As FormViewUpdatedEventArgs)
            'If insert/update operation failed
            If e.Exception IsNot Nothing Then
                e.ExceptionHandled = True
                Try
                    If (e.Exception.InnerException IsNot Nothing) AndAlso (TypeOf e.Exception.InnerException Is UserValidationException) Then
                        'Add error messages from the UserValidationException to the PageMessages collection
                        Dim errList As List(Of String) = CType(e.Exception.InnerException, UserValidationException).ErrorList
                        globals.messages.Warnings.AddRange(errList)
                    Else
                        globals.messages.Errors.Add(e.Exception.Message)
                    End If
                Catch ex As Exception
                    globals.messages.Errors.Add(ex.Message)
                End Try
                'Insert/update failed so the dropdown lists will have to display the old values as selected
                'Here we put the values in Context.Item so we can pull them out on the dropdown lists data bound events

                '--DJW 8/10/2012 Eliminated Country From Being Displayed
                'Context.Items("CountryGuid") = e.OldValues("CountryGuid")

                Context.Items("SecondaryCurrencyGuid") = e.OldValues("SecondaryCurrencyGuid")
                Context.Items("CurrencyGuid") = e.OldValues("CurrencyGuid")
                Context.Items("StateName") = e.OldValues("StateName")
            Else
                'Insert/update succeeded so the dropdown lists will have to display the new values as selected
                'Here we put the values in Context.Item so we can pull them out on the dropdown lists data bound events

                '--DJW 8/10/2012 Eliminated Country From Being Displayed
                'Context.Items("CountryGuid") = e.NewValues("CountryGuid")

                Context.Items("SecondaryCurrencyGuid") = e.NewValues("SecondaryCurrencyGuid")
                Context.Items("CurrencyGuid") = e.NewValues("CurrencyGuid")
                Context.Items("StateName") = e.NewValues("StateName")
                'The LanguageGuid goes into the session object
                Session("LanguageGuid") = e.NewValues("LanguageGuid")
                'Redirect after insert/update
                'Changed behaviour, if configured, for new users reset auto login and redirect 
                Dim newValue As UserBase.UserStates
                'newValue = CType([Enum].Parse(GetType(UserBase.UserStates),Utilities.CStrEx(e.NewValues("Status"))), UserBase.UserStates) 
                newValue = bllObject.Status
                If useConfirmedRegistration AndAlso (newValue = UserBase.UserStates.New) Then
                    ResetAutoLogin()
                    Response.Redirect("~/user-confirm_reg.aspx")
                Else
                    If Request.QueryString("bouncecheckout") = "true" Then
                        'Transfer to the url contained in the query string
                        Server.Transfer(Request.QueryString("bouncereturl"))
                    Else
                        'A little trick to reset the formview after a successful update/insert 
                        'The PageMessages are added to the Context.Items so they can be shown after redirection
                        Context.Items.Add("messages", globals.messages.ContextItems)
                        Server.Transfer(Request.UrlReferrer.AbsolutePath)
                    End If
                End If
            End If
            'Always keep in edit mode
            e.KeepInEditMode = True
        End Sub

#End Region

#Region "Set Required Fields"

        ''' <summary>
        ''' Sets the *, and enables the asp:requiredFieldValidators.
        ''' </summary>
        ''' <remarks></remarks>
        Sub setRequiredFieldText()
            Dim controls As Dictionary(Of String, Control) = ControlDict(Me, "Label")
            For Each item As Label In controls.Values
                If Array.IndexOf(requiredFields, Right(item.ID, item.ID.Length - 3)) >= 0 Then
                    item.Text = "*"
                End If
            Next
            controls.Clear()
            controls = ControlDict(Me, "RequiredFieldValidator")
            For Each item As RequiredFieldValidator In controls.Values
                If Array.IndexOf(requiredFields, Left(item.ID, item.ID.Length - 9)) >= 0 Then
                    item.Enabled = True
                End If
            Next
        End Sub

#End Region

#Region "Utility Functions"

        Private Sub ResetAutoLogin()
            ' Delete users cookie
            Session("UserGuid") = ""
            Response.Cookies("store")("UserGuid") = ""
            Response.Cookies("store").Path = HttpContext.Current.Server.UrlPathEncode(VRoot) & "/"
            eis.UpdateMiniCartInfo(Nothing)
        End Sub

#End Region

    End Class

End Namespace