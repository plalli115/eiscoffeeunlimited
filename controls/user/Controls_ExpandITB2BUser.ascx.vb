﻿Option Strict On
Imports ExpandIT
Imports ExpandIT.ExpandITLib
'Imports ExpandIT31.Logic
Imports ExpandIT31.ExpanditFramework.BLLBase
Imports ExpandIT31
Imports System.Configuration.ConfigurationManager
Imports System.Data

Partial Class controls_Controls_ExpandITB2BUser
    Inherits ExpandIT.UserControl

    Private bllObject As UserB2BBase
    Private currencies As DataTable

#Region "Object Creation"

        ''' <summary>
        ''' Handles ViewDataSource.ObjectCreating event
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks>The User, Validator and confirmationMail objects must be created by the PublicFactory</remarks>
        Protected Sub OnDataObjectCreating(ByVal sender As Object, ByVal e As ObjectDataSourceEventArgs) Handles ViewDataSource.ObjectCreating

            bllObject = ExpanditFactory.CreateUserB2B(CStr(Session("UserGuid"))) 'Retreive the B2BUser object            
            e.ObjectInstance = bllObject 'Register the User object with the ViewDataSource

        End Sub

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try
            currencies = ExpandIT.CurrencyControlLogicFactory.CreateInstance(globals.User).CurrencyTable
            Dim isVisible As Boolean = CurrencyBoxLogicFactory.CreateInstance().isVisible(globals.User)
            CType(FormView1.FindControl("CurrencyPanel1"), HtmlTableRow).Visible = isVisible
            CType(FormView1.FindControl("CurrencyPanel2"), HtmlTableRow).Visible = isVisible
            CType(FormView1.FindControl("LanguagePanel1"), HtmlTableRow).Visible = New LanguageBoxLogic().isVisible(globals.User)
            CType(FormView1.FindControl("StateNamePanel"), HtmlTableRow).Visible = CBoolEx(AppSettings("SHOW_STATE_ON_SHIPPING_ADDRESS"))
        Catch ex As Exception

        End Try
    End Sub

    Protected Function CountryGuid2Text(ByVal guid As String) As String
        Return EISClass.GetCountryName(guid)
    End Function

    Protected Function CurrencyGuid2Text(ByVal guid As String) As String
        If currencies IsNot Nothing Then
            Try
                For Each row As DataRow In currencies.Rows
                    If row(0).Equals(guid) Then
                        Return CStrEx(row(1))
                    End If
                Next
            Catch ex As Exception
                Return "ERROR"
            End Try
        End If
        Return String.Empty
    End Function

    Protected Function languageGuid2Text(ByVal guid As String) As String
        Dim m_languages As ExpDictionary = eis.LoadLanguages()

        For Each item As ExpDictionary In m_languages.Values
            If Not item("LanguageGuid").Equals(DBNull.Value) AndAlso item("LanguageGuid").Equals(guid) Then
                Return CStrEx(item("LanguageName"))
            End If
        Next
        Return String.Empty
    End Function

End Class
