﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Controls_ExpandITUser.ascx.vb"
    Inherits="ExpandIT.WebUserControls.Controls_ExpandITUser" ClassName="Controls_ExpandITUser" %>
<%@ Register Src="~/controls/Message.ascx" TagName="Message" TagPrefix="uc1" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<%@ Import Namespace="ExpandIT31.ExpanditFramework.Infrastructure" %>
<%@ Import Namespace="ExpandIT31.ExpanditFramework.BLLBase" %>
<asp:Panel ID="userNewPanel" runat="server" CssClass="userNewPanel">
    <uc1:PageHeader ID="PageHeader1" runat="server" Text="<%$ Resources: Language, LABEL_REGISTER_AS_NEW_USER %>"
        EnableTheming="true" />
    <uc1:Message ID="Message1" runat="server" />
    <div class="userNewText">
        <%--AM2010100101 - ADDRESS VALIDATION - Start--%>
        <%--Address Validation Response--%>
        <asp:Panel runat="server" ID="AddressValidated" Visible="false">
            <table>
                <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server" Style="font-weight: bold;" Text="Address :"></asp:Label></td>
                    <td style="color: Black;">
                        <%=responseAddress("Address") %>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server" Style="font-weight: bold;" Text="City :"></asp:Label>
                    </td>
                    <td style="color: Black;">
                        <%=responseAddress("City") %>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label3" runat="server" Style="font-weight: bold;" Text="State :"></asp:Label>
                    </td>
                    <td style="color: Black;">
                        <%=responseAddress("State") %>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label4" runat="server" Style="font-weight: bold;" Text="Zip Code :"></asp:Label>
                    </td>
                    <td style="color: Black;">
                        <%=responseAddress("Zip") %>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td>
                        <asp:Button ID="UpdateAddress" runat="server" Text="Modify" />
                    </td>
                    <td>
                        <asp:Button ID="btnKeep" runat="server" Text="Keep" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <%--AM2010100101 - ADDRESS VALIDATION - End--%>
        <p>
            <asp:Label runat="server" ID="userRegisterInformation" />
            <br />
            <asp:Label runat="server" ID="requiredFieldInfo" />
        </p>
        <asp:FormView ID="FormView1" runat="server" DefaultMode="Edit" OnItemUpdated="OnItemUpdated"
            DataSourceID="ViewDataSource">
            <EditItemTemplate>
                <asp:Panel runat="server" ID="editpnl" DefaultButton="UpdateButton">
                    <table border="0" cellspacing="0">
                        <tr class="TR1">
                            <td class="TDR">
                                <asp:Label ID="langResidentialAddress" runat="server" Text='<%$Resources: Language,LABEL_USERDATA_RESIDENTIAL %>' />
                            </td>
                            <td class="TDL">
                                <asp:CheckBox ID="ResidentialAddressCheckBox" runat="server" Checked='<%# Bind("ResidentialAddress") %>' />
                            </td>
                        </tr>
                        
                        <%-- --DJW 8/13/2012 Hiding  Contact Name From Being Displayed--%>
                        <tr class="TR1" visible="false">
                            <td class="TDR" visible="false">
                                <asp:Label ID="langContactName" runat="server" visible="false" Text='<%$Resources: Language,LABEL_USERDATA_CONTACT %>' />
                            </td>
                            <td class="TDL" visible="false">
                                <asp:TextBox ID="ContactNameTextBox" runat="server" visible="false" MaxLength="50" CssClass="borderTextBox"
                                    Text='<%# Bind("ContactName") %>' />
                            </td>
                            <td>
                                <asp:Label runat="server" ID="lblContactName" visible="false" />
                                <%--<asp:RequiredFieldValidator runat="server" ID="ContactNameValidator" ControlToValidate="ContactNameTextBox"
                                    ValidationGroup="updateUser" ErrorMessage="<%$Resources: Language, MESSAGE_REQUIRED_FIELD %>"
                                    Enabled="false" />--%>
                            </td>
                        </tr> 
                        
                        <tr class="TR1">
                            <td class="TDR">
                                <asp:Label ID="langCompanyName" runat="server" Text='<%$Resources: Language,LABEL_USERDATA_COMPANY %>' />
                            </td>
                            <td class="TDL">
                                <asp:TextBox ID="CompanyNameTextBox" runat="server" MaxLength="50" CssClass="borderTextBox"
                                    Text='<%# Bind("CompanyName") %>' />
                            </td>
                            <td>
                                <asp:Label runat="server" ID="lblCompanyName" />
                                <asp:RequiredFieldValidator runat="server" ID="CompanyNameValidator" ControlToValidate="CompanyNameTextBox"
                                    ValidationGroup="updateUser" ErrorMessage="<%$Resources: Language, MESSAGE_REQUIRED_FIELD %>"
                                    Enabled="false" />
                            </td>
                        </tr>
                        <tr class="TR1">
                            <td class="TDR">
                                <asp:Label ID="langAddress1" runat="server" Text='<%$Resources: Language,LABEL_USERDATA_ADDRESS_1 %>' />
                            </td>
                            <td class="TDL">
                                <asp:TextBox ID="Address1TextBox" runat="server" MaxLength="50" CssClass="borderTextBox"
                                    Text='<%# Bind("Address1") %>' />
                            </td>
                            <td>
                                <asp:Label ID="lblAddress1" runat="server" />
                                <asp:RequiredFieldValidator runat="server" ID="Address1Validator" ControlToValidate="Address1TextBox"
                                    ValidationGroup="updateUser" ErrorMessage="<%$Resources: Language, MESSAGE_REQUIRED_FIELD %>"
                                    Enabled="false" />
                            </td>
                        </tr>
                        <tr class="TR1">
                            <td class="TDR">
                                <asp:Label ID="langAddress2" runat="server" Text='<%$Resources: Language,LABEL_USERDATA_ADDRESS_2 %>' />
                            </td>
                            <td class="TDL">
                                <asp:TextBox ID="Address2TextBox" runat="server" MaxLength="50" CssClass="borderTextBox"
                                    Text='<%# Bind("Address2") %>' />
                            </td>
                            <td>
                                <asp:Label ID="lblAddress2" runat="server" />
                                <asp:RequiredFieldValidator runat="server" ID="Address2Validator" ControlToValidate="Address2TextBox"
                                    ValidationGroup="updateUser" ErrorMessage="<%$Resources: Language, MESSAGE_REQUIRED_FIELD %>"
                                    Enabled="false" />
                            </td>
                        </tr>
                        <tr class="TR1">
                            <td class="TDR">
                                <asp:Label ID="langCityName" runat="server" Text='<%$Resources: Language,LABEL_USERDATA_CITY %>' />
                            </td>
                            <td class="TDL">
                                <asp:TextBox ID="CityNameTextBox" runat="server" MaxLength="50" CssClass="borderTextBox"
                                    Text='<%# Bind("CityName") %>' />
                            </td>
                            <td>
                                <asp:Label runat="server" ID="lblCityName" />
                                <asp:RequiredFieldValidator runat="server" ID="CityNameValidator" ControlToValidate="CityNameTextBox"
                                    ValidationGroup="updateUser" ErrorMessage="<%$Resources: Language, MESSAGE_REQUIRED_FIELD %>"
                                    Enabled="false" />
                            </td>
                        </tr>
                        <tr runat="server" id="StateNamePanel" class="TR1">
                            <td class="TDR">
                                <asp:Label ID="langStateName" runat="server" Text='<%$Resources: Language,LABEL_USERDATA_STATE %>' />
                            </td>
                            <td class="TDL">
                                <asp:TextBox ID="StateNameTextBox" runat="server" MaxLength="10" CssClass="userNewTextBoxState"
                                    Text='<%# Bind("StateName") %>' />
                            </td>
                            <td>
                                <asp:Label runat="server" ID="lblStateName" />
                                <asp:RequiredFieldValidator runat="server" ID="StateNameValidator" ControlToValidate="StateNameTextBox"
                                    ValidationGroup="updateUser" ErrorMessage="<%$Resources: Language, MESSAGE_REQUIRED_FIELD %>"
                                    Enabled="false" />
                            </td>
                        </tr>
                        <tr class="TR1">
                            <td class="TDR">
                                <asp:Label ID="langZipCode" runat="server" Text='<%$Resources: Language,LABEL_USERDATA_ZIPCODE %>' />
                            </td>
                            <td class="TDL">
                                <asp:TextBox ID="ZipCodeTextBox" runat="server" MaxLength="30" CssClass="userNewTextBoxZipCode"
                                    Text='<%# Bind("ZipCode") %>' />
                            </td>
                            <td>
                                <asp:Label runat="server" ID="lblZipCode" />
                                <asp:RequiredFieldValidator runat="server" ID="ZipCodeValidator" ControlToValidate="ZipCodeTextBox"
                                    ValidationGroup="updateUser" ErrorMessage="<%$Resources: Language, MESSAGE_REQUIRED_FIELD %>"
                                    Enabled="false" />
                            </td>
                        </tr>
                        
                        <%----DJW 8/10/2012 Hiding Country From Being Displayed--%>
                        <%--<tr class="TR1">
                            <td class="TDR">
                                <asp:Label ID="Label8" runat="server" Visible="false" Text='<%$Resources: Language,LABEL_USERDATA_COUNTRY %>' />
                            </td>
                            <td class="TDL">
                                <div class="dpdBorder">
                                    <asp:DropDownList ID="CountryDropDownList" runat="server" CssClass="borderDropDownList"
                                        Width="150px" DataValueField="CountryGuid" DataTextField="CountryName" DataSourceID="ObjectDataSource5"
                                        OnDataBound="OnCountryBound" Visible="false">
                                    </asp:DropDownList>
                                </div>
                                <asp:ObjectDataSource ID="ObjectDataSource5" Visible="false" runat="server" SelectMethod="LoadCountries"
                                    TypeName="ExpandIT.CountryControlLogic"></asp:ObjectDataSource>
                            </td>
                            <td>
                            </td>
                        </tr>--%>
                        
                        <tr runat="server" id="CurrencyPanel1" class="TR1">
                            <td class="TDR">
                                <asp:Label ID="Label9" runat="server" Text='<%$Resources: Language,LABEL_USERDATA_CURRENCY_1 %>' />
                            </td>
                            <td class="TDL">
                                <div class="dpdBorder">
                                    <asp:DropDownList ID="CurrencyDropDownList" runat="server" CssClass="borderDropDownList"
                                        Width="150px" DataSourceID="CurrencyDropDownDataSource" DataValueField="CurrencyCode"
                                        DataTextField="CurrencyName" OnDataBound="OnBound">
                                    </asp:DropDownList>
                                </div>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr runat="server" id="CurrencyPanel2" class="TR1">
                            <td class="TDR">
                                <asp:Label ID="Label10" runat="server" Text='<%$Resources: Language,LABEL_USERDATA_CURRENCY_2 %>' />
                            </td>
                            <td class="TDL">
                                <div class="dpdBorder">
                                    <asp:DropDownList ID="SecondaryCurrencyDropDownList" runat="server" CssClass="borderDropDownList"
                                        Width="150px" DataValueField="CurrencyCode" DataTextField="CurrencyName" OnDataBound="OnBoundSecondary"
                                        DataSourceID="CurrencyDropDownDataSource2">
                                    </asp:DropDownList>
                                </div>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr runat="server" id="LanguagePanel1" class="TR1">
                            <td class="TDR">
                                <asp:Label ID="Label11" runat="server" Text='<%$Resources: Language,LABEL_SITE_LANGUAGE %>' />
                            </td>
                            <td class="TDL">
                                <div class="dpdBorder">
                                    <asp:DropDownList ID="LanguageDropDownList" runat="server" CssClass="borderDropDownList"
                                        Width="150px" DataValueField="LanguageGuid" DataTextField="LanguageName" DataSourceID="LanguageDataSource"
                                        OnDataBound="OnLanguageBound">
                                    </asp:DropDownList>
                                </div>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr class="TR1">
                            <td class="TDR">
                                <asp:Label ID="langPhoneNo" runat="server" Text='<%$Resources: Language,LABEL_USERDATA_PHONE %>' />
                            </td>
                            <td class="TDL">
                                <asp:TextBox ID="PhoneNoTextBox" runat="server" MaxLength="30" CssClass="borderTextBox"
                                    Text='<%# Bind("PhoneNo") %>' OnBlur="validatePhone(this)" />
                            </td>
                            <td>
                                <asp:Label runat="server" ID="lblPhoneNo" />
                                <asp:RequiredFieldValidator runat="server" ID="PhoneNoValidator" ControlToValidate="PhoneNoTextBox"
                                    ValidationGroup="updateUser" ErrorMessage="<%$Resources: Language, MESSAGE_REQUIRED_FIELD %>"
                                    Enabled="false" />
                            </td>
                        </tr>
                        <tr class="TR1">
                            <td class="TDR">
                                <asp:Label ID="langEmailAddress" runat="server" Text='<%$Resources: Language,LABEL_USERDATA_EMAIL %>' />
                            </td>
                            <td class="TDL">
                                <asp:TextBox ID="EmailAddressTextBox" runat="server" MaxLength="50" CssClass="borderTextBox"
                                    Text='<%# Bind("EmailAddress") %>' />
                            </td>
                            <td>
                                <asp:Label runat="server" ID="lblEmailAddress" />
                                <asp:RequiredFieldValidator runat="server" ID="EmailAddressValidator" ControlToValidate="EmailAddressTextBox"
                                    ValidationGroup="updateUser" ErrorMessage="<%$Resources: Language, MESSAGE_REQUIRED_FIELD %>"
                                    Enabled="false" />
                            </td>
                        </tr>
                        <tr class="TR1">
                            <td class="TDR">
                                <asp:Label ID="langUserLogin" runat="server" Text='<%$Resources: Language,LABEL_USERDATA_LOGIN %>' />
                            </td>
                            <td class="TDL">
                                <asp:TextBox ID="UserLoginTextBox" runat="server" MaxLength="20" CssClass="borderTextBox"
                                    Text='<%# Bind("UserLogin") %>' />
                            </td>
                            <td>
                                <asp:Label runat="server" ID="lblUserLogin" />
                                <asp:RequiredFieldValidator runat="server" ID="UserLoginValidator" ControlToValidate="UserLoginTextBox"
                                    ValidationGroup="updateUser" ErrorMessage="<%$Resources: Language, MESSAGE_REQUIRED_FIELD %>" />
                            </td>
                        </tr>
                        <tr class="TR1">
                            <td class="TDR">
                                <asp:Label ID="langPassword1" Visible="false" runat="server" Text='<%$Resources: Language,LABEL_USERDATA_PASSWORD %>' />
                            </td>
                            <td class="TDL">
                                <asp:TextBox ID="UserPasswordTextBox" Visible="false" runat="server" TextMode="Password"
                                    MaxLength="40" CssClass="borderTextBox" Text='<%# Bind("UserPassword") %>' />
                            </td>
                            <td>
                                <asp:Label ID="lblUserPassword" Visible="false" runat="server" />
                                <asp:RequiredFieldValidator runat="server" ID="UserPasswordValidator" ControlToValidate="UserPasswordTextBox"
                                    ValidationGroup="updateUser" ErrorMessage="<%$Resources: Language, MESSAGE_REQUIRED_FIELD %>" />
                            </td>
                        </tr>
                        <tr class="TR1">
                            <td class="TDR">
                                <asp:Label ID="langPassword2" Visible="false" runat="server" Text='<%$Resources: Language,LABEL_USERDATA_CONFIRM_PASSWORD %>' />
                            </td>
                            <td class="TDL">
                                <asp:TextBox ID="UserPassword2TextBox" Visible="false" runat="server" TextMode="Password"
                                    MaxLength="40" CssClass="borderTextBox" Text='<%# Bind("ConfirmedUserPassword") %>' />
                            </td>
                            <td>
                                <asp:Label ID="lblConfirmedUserPassword" Visible="false" runat="server" Text="*" />
                                <asp:RequiredFieldValidator runat="server" ID="UserPassword2Validator" ControlToValidate="UserPassword2TextBox"
                                    ValidationGroup="updateUser" ErrorMessage="<%$Resources: Language, MESSAGE_REQUIRED_FIELD %>" />
                            </td>
                        </tr>
                        <tr class="TR1">
                            <td class="TDR">
                                &nbsp;
                            </td>
                            <td class="TDL">
                                <asp:HiddenField ID="UserGuidHiddenField" runat="server" Value='<%# Bind("UserGuid") %>' />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" colspan="2">
                                <%--AM2010100101 - ADDRESS VALIDATION - Start--%>
                                <asp:Button ID="btnValidateAddress" runat="server" CausesValidation="false" CommandName="ValidateAddress"
                                    Text='<%$Resources: Language,LABEL_VALIDATE_ADDRESS%>' OnClick="btnValidateAddress_Clicked" 
                                    Enabled="false" visible="false" CssClass="AddButton" />
                                <%--AM2010100101 - ADDRESS VALIDATION - End--%>
                                <asp:Button ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" CssClass="AddButton"
                                    Text='<%$Resources: Language,ACTION_SAVE %>' ValidationGroup="updateUser" Enabled="false" visible="false"/>
                                &nbsp;
                                <input type="reset" id="BtnReset" class="AddButton" runat="server" value='<%$Resources: Language,ACTION_RESET %>' />
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </EditItemTemplate>
            <ItemTemplate>
                Address1:
                <asp:TextBox ID="Address1TextBox" runat="server" Text='<%# Bind("Address1") %>' />
                <br />
                Address2:
                <asp:TextBox ID="Address2TextBox" runat="server" Text='<%# Bind("Address2") %>' />
                <br />
                CityName:
                <asp:TextBox ID="CityNameTextBox" runat="server" Text='<%# Bind("CityName") %>' />
                <br />
                CompanyName:
                <asp:TextBox ID="CompanyNameTextBox" runat="server" Text='<%# Bind("CompanyName") %>' />
                <br />
                Residential:
                <asp:CheckBox ID="ResidentialAddressCheckBox" runat="server" Checked='<%# Bind("ResidentialAddress") %>' />
                <br />
                
                <%-- '--DJW 8/10/2012 Eliminated Contact Name From Being Displayed--%>
                <%--ContactName:--%>
                <asp:TextBox ID="ContactNameTextBox" Visible="false" runat="server" Text='<%# Bind("ContactName") %>' />
                <%--<br />--%>
                
               <%-- '--DJW 8/10/2012 Eliminated Country From Being Displayed--%>
                <%--CountryGuid:
                <asp:TextBox ID="CountryGuidTextBox" runat="server" Visible="false" Text='<%# Bind("CountryGuid") %>' />
                <br /> --%>
                
                CurrencyGuid:
                <asp:TextBox ID="CurrencyGuidTextBox" runat="server" Text='<%# Bind("CurrencyGuid") %>' />
                <br />
                EmailAddress:
                <asp:TextBox ID="EmailAddressTextBox" runat="server" Text='<%# Bind("EmailAddress") %>' />
                <br />
                IsB2B:
                <asp:TextBox ID="IsB2BTextBox" runat="server" Text='<%# Bind("IsB2B") %>' />
                <br />
                LanguageGuid:
                <asp:TextBox ID="LanguageGuidTextBox" runat="server" Text='<%# Bind("LanguageGuid") %>' />
                <br />
                PhoneNo:
                <asp:TextBox ID="PhoneNoTextBox" runat="server" Text='<%# Bind("PhoneNo") %>' />
                <br />
                SecondaryCurrencyGuid:
                <asp:TextBox ID="SecondaryCurrencyGuidTextBox" runat="server" Text='<%# Bind("SecondaryCurrencyGuid") %>' />
                <br />
                StateName:
                <asp:TextBox ID="StateNameTextBox" runat="server" Text='<%# Bind("StateName") %>' />
                <br />
                UserGuid:
                <asp:TextBox ID="UserGuidTextBox" runat="server" Text='<%# Bind("UserGuid") %>' />
                <br />
                UserLogin:
                <asp:TextBox ID="UserLoginTextBox" runat="server" Text='<%# Bind("UserLogin") %>' />
                <br />
                UserPassword:
                <asp:TextBox ID="UserPasswordTextBox" runat="server" Text='<%# Bind("UserPassword") %>' />
                <br />
                ZipCode:
                <asp:TextBox ID="ZipCodeTextBox" runat="server" Text='<%# Bind("ZipCode") %>' />
                <br />
                <asp:Button ID="EditButton" CssClass="AddButton" runat="server" CausesValidation="False"
                    CommandName="Edit" Text="Edit" />
                &nbsp;<asp:Button ID="NewButton" CssClass="AddButton" runat="server" CausesValidation="False"
                    CommandName="New" Text="New" />
            </ItemTemplate>
        </asp:FormView>
    </div>
</asp:Panel>
<%--AM2010100101 - ADDRESS VALIDATION - Start--%>
<br />
<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="findUser"
    Visible="False" Width="500px">
    <Columns>
    
        <%-- '--DJW 8/13/2012 Eliminated Contact Name From Being Displayed--%>
       <asp:HyperLinkField DataNavigateUrlFields="UserGuid" DataTextField="ContactName"
            HeaderText="ContactName" DataNavigateUrlFormatString="~/userSearchSelection.aspx?OrderUserGuid={0}&amp;returl=shop.aspx"
            SortExpression="ContactName" />
            
        <asp:BoundField DataField="UserLogin" HeaderText="UserLogin" ReadOnly="True" SortExpression="UserLogin" />
        <asp:BoundField DataField="EmailAddress" HeaderText="EmailAddress" ReadOnly="True"
            SortExpression="EmailAddress" />
    </Columns>
</asp:GridView>
<asp:SqlDataSource ID="findUser" runat="server" ConnectionString="<%$ ConnectionStrings:ExpandITConnectionString %>">
</asp:SqlDataSource>
<%--<asp:SqlDataSource ID="findUser" runat="server" ConnectionString="<%$ ConnectionStrings:ExpandITConnectionString %>"
    SelectCommand="SELECT UserGuid,ContactName,UserLogin,EmailAddress&#13;&#10;FROM UserTable&#13;&#10;UNION&#13;&#10;SELECT U.UserGuid,U.ContactName,U.UserLogin,U.EmailAddress&#13;&#10;FROM UserTableB2B U INNER JOIN CustomerTable C ON U.CustomerGuid=C.CustomerGuid&#13;&#10;WHERE U.UserGuid NOT IN (SELECT UserGuid FROM UserTable)">
</asp:SqlDataSource>--%>
<%--AM2010100101 - ADDRESS VALIDATION - End--%>
<asp:ObjectDataSource ID="ViewDataSource" runat="server" SelectMethod="GetData" TypeName="ExpandIT31.ExpanditFramework.BLLBase.UserBase"
    UpdateMethod="SetData" InsertMethod="SetData" OnObjectCreating="OnDataObjectCreating"
    OldValuesParameterFormatString="original_{0}">
    <UpdateParameters>
        <asp:Parameter Name="Address1" Type="String" />
        <asp:Parameter Name="Address2" Type="String" />
        <asp:Parameter Name="CityName" Type="String" />
        <asp:Parameter Name="CompanyName" Type="String" />
        <asp:Parameter Name="ResidentialAddress" Type="Boolean" />
        
        <%-- '--DJW 8/13/2012 Eliminated Contact Name From Being Displayed--%>
        <asp:Parameter Name="ContactName" Type="String" />
        
        <%--'--DJW 8/10/2012 Eliminated Country From Being Displayed--%>
        <asp:Parameter Name="CountryGuid" Type="String" />        
        
        <asp:Parameter Name="CurrencyGuid" Type="String" />        
        <asp:Parameter Name="EmailAddress" Type="String" />
        <asp:Parameter Name="LanguageGuid" Type="String" />
        <asp:Parameter Name="PhoneNo" Type="String" />
        <asp:Parameter Name="SecondaryCurrencyGuid" Type="String" />
        <asp:Parameter Name="StateName" Type="String" />
        <asp:Parameter Name="UserGuid" Type="String" />
        <asp:Parameter Name="UserLogin" Type="String" />
        <asp:Parameter Name="UserPassword" Type="String" />
        <asp:Parameter Name="ConfirmedUserPassword" Type="String" />
        <asp:Parameter Name="ZipCode" Type="String" />
    </UpdateParameters>
    <SelectParameters>
        <%--<asp:SessionParameter Name="userGuid" SessionField="UserGuid" Type="String" />--%>
        <asp:SessionParameter Name="userGuid" SessionField="NewUserGuid" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="CurrencyDropDownDataSource" runat="server" OldValuesParameterFormatString="original_{0}"
    SelectMethod="CurrencyTable" TypeName="ExpandIT.CurrencyControlLogic" OnObjectCreating="OnCreate">
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="CurrencyDropDownDataSource2" runat="server" OldValuesParameterFormatString="original_{0}"
    SelectMethod="SecondaryCurrencyTable" TypeName="ExpandIT.CurrencyControlLogic"
    OnObjectCreating="OnCreateSecondary"></asp:ObjectDataSource>
<asp:ObjectDataSource ID="languageDataSource" runat="server" OldValuesParameterFormatString="original_{0}"
    SelectMethod="LoadLanguages" TypeName="ExpandIT.LanguageControlLogic">
    <SelectParameters>
        <asp:Parameter DefaultValue="True" Name="load" Type="Boolean" />
    </SelectParameters>
</asp:ObjectDataSource>

<%--AM2010100101 - ADDRESS VALIDATION - Start--%>
<script language="javascript" type="text/javascript">
    function validatePhone(id) {
        
        var phoneField=id.value;
        if(phoneField.length >0) {
            if (!/^\d*$/.test(phoneField)) {
                  phoneField = phoneField.replace(/[^\d]/g,'');
            }
           if(phoneField.length == 10) {
              phoneField = "(" + phoneField.substring(0,3) + ")-" + phoneField.substring(3, 6) + "-" + phoneField.substring(6);
              id.value=phoneField;
           }
       }
          
    }
</script>
<%--AM2010100101 - ADDRESS VALIDATION - End --%>