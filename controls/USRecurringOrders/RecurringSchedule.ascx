<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="RecurringSchedule.ascx.vb"
    Inherits="RecurringSchedule" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.GlobalsClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%  
    If Not DictOrder Is Nothing Then%>
<%  
    If Not DictOrder("Lines") Is Nothing Then%>
<table class="RecurringStyles">
    <tr>
        <td>
            <asp:Panel ID="providerPanel" Style="margin-left: 7px;" runat="server">
                <asp:Panel ID="RecurrencyEditable" Visible="false" runat="server">

                    <script type="text/javascript" src="<%=Vroot %>/script/calendar/calendar_us.js" language="JavaScript"></script>

                    <link href="<%=Vroot %>/script/calendar/calendar.css" rel="stylesheet" />
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="font-weight: bold;">
                                <% =Resources.Language.LABEL_RECURRING_SCHEDULE%>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 10px;">
                            </td>
                        </tr>
                    </table>
                    <table cellpadding="0" cellspacing="0" style="width: 100%;">
                        <tr>
                            <td style="font-weight: bold; vertical-align: bottom;" align="left">
                                <% =Resources.Language.LABEL_NAME%>
                            </td>
                            <td style="font-weight: bold; width: 85px; vertical-align: bottom;" valign="bottom"
                                align="left">
                                <% =Resources.Language.LABEL_RECUR_EVERY%>
                            </td>
                            <td style="font-weight: bold; width: 120px; vertical-align: bottom;" valign="bottom"
                                align="center">
                                <% =Resources.Language.LABEL_PERIOD%>
                            </td>
                            <td style="font-weight: bold; width: 10px; vertical-align: bottom;" valign="bottom"
                                align="center">
                                <% =Resources.Language.LABEL_QUANTITY%>
                            </td>
                            <td style="font-weight: bold; vertical-align: bottom;" valign="bottom" align="center">
                                <% =Resources.Language.LABEL_FINAL_DATE%>
                            </td>
                            <td style="height: 3px;">
                            </td>
                            <%If isModifyRecurrency Then%>
                            <td style="font-weight: bold; vertical-align: bottom;" valign="bottom" align="center">
                                <% =Resources.Language.LABEL_NEXT_SHIPMENT_DATE%>
                            </td>
                <td style="height: 3px;">
                </td>
                            <%End If%>
                        </tr>
                        <%Dim line As ExpDictionary%>
                        <%
                            For Each line In DictOrder("Lines").values%>
                        <tr class="CartLineFieldsAlt">
                            <td align="left" style="padding-bottom: 5px; border: 0px;">
                                <%=line("ProductName") & "<br />" & line("DESCRIPTION2")%>
                            </td>
                            <td style="width: 60px; vertical-align: middle; padding-bottom: 5px; border: 0px;"
                                align="center">
                                <input id="txt" name="txbNumber_<%=line("LineGuid")%>" style="width: 50px; text-align: center;"
                                    value="<% = IIf(CBoolEx(DictOrder("RecurringOrder")),line("RNumber"),"")%>" type="text" />
                            </td>
                            <td align="center" style="vertical-align: middle; padding-bottom: 5px; border: 0px;">
                                <select style="height: 17px; font-family: Tahoma; font-size: 11px; color: #323233;
                                    text-align: center;" name="Period_<%=line("LineGuid")%>">
                                    <option value="SELECT" <%=IIf(CBoolEx(DictOrder("RecurringOrder")) And line("RPeriod") Is DBNull.Value, "selected=selected", "")%>
                                        selected="selected">
                                        <%= Resources.Language.LABEL_RECURRING_SELECT_PERIOD %>
                                    </option>
                                    <option value="DAY" <%=IIf(CBoolEx(DictOrder("RecurringOrder")) And CStrEx(line("RPeriod")) = "DAY", "selected=selected", "")%>>
                                        <%= Resources.Language.LABEL_RECURRING_DAY_PERIOD %>
                                    </option>
                                    <option value="WEEK" <%=IIf(CBoolEx(DictOrder("RecurringOrder")) And CStrEx(line("RPeriod")) = "WEEK", "selected=selected", "")%>>
                                        <%=Resources.Language.LABEL_RECURRING_WEEK_PERIOD%>
                                    </option>
                                    <option value="MONTH" <%=IIf(CBoolEx(DictOrder("RecurringOrder")) And CStrEx(line("RPeriod")) = "MONTH", "selected=selected", "")%>>
                                        <%= Resources.Language.LABEL_RECURRING_MONTH_PERIOD %>
                                    </option>
                                </select>
                            </td>
                            <td style="width: 50px; vertical-align: middle; padding-bottom: 5px; border: 0px;"
                                align="center">
                                <input id="Text2" name="txbQuantity_<%=line("LineGuid")%>" style="width: 50px; text-align: center;"
                                    value="<%=IIf(CBoolEx(DictOrder("RecurringOrder")),line("RQuantity"),"")%>" type="text" />
                            </td>
                            <td align="center" style="vertical-align: middle; padding-bottom: 5px; border: 0px;">
                                <asp:TextBox ID="TextBox1" runat="server" Visible="false" Width="90px"></asp:TextBox>
                                <%If (CBoolEx(DictOrder("RecurringOrder"))) Then%>
                                <%If Not line("RFinalDate") Is Nothing And Not line("RFinalDate") Is DBNull.Value Then%>
                                <input name="txbFinalDate_<%=line("LineGuid")%>" id="txbFinalDate_<%=line("LineGuid")%>"
                                    value="<%=FormatDate(line("RFinalDate")) %>" type="text" style="text-align: center;
                                    width: 90px;" />
                                <%Else%>
                                <%Dim theDate As String = Me.TextBox1.Text%>
                                <input name="txbFinalDate_<%=line("LineGuid")%>" id="txbFinalDate_<%=line("LineGuid")%>"
                                    value="<%= theDate%>" type="text" style="text-align: center; width: 90px;" />
                                <%End If%>
                                <%End If%>
                            </td>
                            <td style="vertical-align: middle; padding-bottom: 5px; border: 0px;">
                                <input id="vroot" type="hidden" value="<%=Vroot %>" />
                                <%=GetImageCalendar(count)%>
                                <%=GetScriptCalendar(line("LineGuid"))%>
                            </td>
                            <%
                                If isModifyRecurrency Then%>
                            <td align="center" style="vertical-align: middle; padding-bottom: 5px; border: 0px;">
                                <%
                                If Not Page.IsPostBack Then%>
                                <input name="txbRNextOrderDate_<%=line("LineGuid")%>" id="txbRNextOrderDate_<%=line("LineGuid")%>"
                                    value="<%= recurringObj.getRecurrencyNextOrderDate(cintex(line("RNumber")),CStrEx(line("RPeriod")),cStrEx(line("RFinalDate")),cstrex(line("RNextOrderDate")),cstrex(DictOrder("ModifiedRecurrency")), IsActiveOrder, DictOrder("HeaderDate")) %>"
                                    type="text" style="text-align: center; width: 90px;" />
                                <%End If%>
                            </td>
                            <td style="vertical-align: middle; padding-bottom: 5px; border: 0px;">
                                <%count = count + 1%>
                                <%=GetImageCalendar(count)%>
                                <%=GetScriptCalendarNextDate(line("LineGuid"))%>
                            </td>
                            <%End If%>
                        </tr>
                        <%count=count + 1 %>
                        <%Next%>
                    </table>
                </asp:Panel>
                <asp:Panel ID="RecurrencyNoEditable" Visible="false" runat="server">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="font-weight: bold;">
                                <% =Resources.Language.LABEL_RECURRING_SCHEDULE%>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 10px;">
                            </td>
                        </tr>
                    </table>
                    <table cellpadding="0" cellspacing="0" style="width: 100%;">
                        <tr>
                            <td style="font-weight: bold; vertical-align: bottom;" align="left">
                                <% =Resources.Language.LABEL_NAME%>
                            </td>
                            <td style="font-weight: bold; width: 85px; vertical-align: bottom;" valign="bottom"
                                align="left">
                                <% =Resources.Language.LABEL_RECUR_EVERY%>
                            </td>
                            <td style="font-weight: bold; width: 120px; vertical-align: bottom;" valign="bottom"
                                align="center">
                                <% =Resources.Language.LABEL_PERIOD%>
                            </td>
                            <td style="font-weight: bold; width: 10px; vertical-align: bottom;" valign="bottom"
                                align="center">
                                <% =Resources.Language.LABEL_QUANTITY%>
                            </td>
                            <td style="font-weight: bold; vertical-align: bottom;" valign="bottom" align="center">
                                <% =Resources.Language.LABEL_FINAL_DATE%>
                            </td>
                            <%'If isModifyRecurrency Then%>
                            <td style="font-weight: bold; vertical-align: bottom;" valign="bottom" align="center">
                                <% =Resources.Language.LABEL_NEXT_SHIPMENT_DATE%>
                            </td>
                            <%'End If%>
                        </tr>
                        <%Dim line As ExpDictionary%>
                        <%For Each line In DictOrder("Lines").values%>
                        <tr class="CartLineFieldsAlt">
                            <td align="left" style="padding-bottom: 5px; border: 0px;">
                                <%=line("ProductName") & "<br />" & line("DESCRIPTION2")%>
                            </td>
                            <td style="width: 60px; vertical-align: middle; padding-bottom: 5px; border: 0px;"
                                align="center">
                                <% = IIf(CBoolEx(DictOrder("RecurringOrder")),line("RNumber"),"")%>
                            </td>
                            <td align="center" style="vertical-align: middle; padding-bottom: 5px; border: 0px;">
                                <%=CStrEx(line("RPeriod"))%>
                            </td>
                            <td style="width: 50px; vertical-align: middle; padding-bottom: 5px; border: 0px;"
                                align="center">
                                <%=IIf(CBoolEx(DictOrder("RecurringOrder")),line("RQuantity"),"")%>
                            </td>
                            <td align="center" style="vertical-align: middle; padding-bottom: 5px; border: 0px;">
                                <%If not line("RFinalDate") is dbnull.value Then %>
                                <%=FormatDate(line("RFinalDate")) %>
                                <%End If %>
                            </td>
                            <td align="center" style="vertical-align: middle; padding-bottom: 5px; border: 0px;">
                            <%
                            dim nextOrderDateVal as string
                            If isModifyRecurrency Or CBoolEx(isOrderModified(line("HeaderGuid"))) Then%>
                                <%If not line("RNextOrderDate") is dbnull.value Then 
                                    If CDateEx(line("RNextOrderDate")).Date < DateTime.Now.Date Then
                                        nextOrderDateVal=recurringObj.getRecurrencyNextOrderDate(cintex(line("RNumber")),CStrEx(line("RPeriod")),cStrEx(line("RFinalDate")),cstrex(line("RNextOrderDate")),cstrex(DictOrder("ModifiedRecurrency")), IsActiveOrder, DictOrder("HeaderDate"))
                                    Else
                                        nextOrderDateVal=FormatDate(line("RNextOrderDate"))%>
                                    <%End If%>
                                    <% = nextOrderDateVal%>
                                <%End If %>
                            <%Else%>
                                <%
                                nextOrderDateVal=recurringObj.getRecurrencyNextOrderDate(cintex(line("RNumber")),CStrEx(line("RPeriod")),cStrEx(line("RFinalDate")),cstrex(line("RNextOrderDate")),cstrex(DictOrder("ModifiedRecurrency")), IsActiveOrder, DictOrder("HeaderDate"))
                                If IsActiveOrder() Then
                                    line("RNextOrderDate")=nextOrderDateVal
                                End If %>
                            <%
                                = nextOrderDateVal  %>                              
                            <%End If%>
                            </td>
                        </tr>
                        <%Next%>
                    </table>
                </asp:Panel>
            </asp:Panel>
        </td>
    </tr>
</table>
<%  End If%>
<%  End If%>
