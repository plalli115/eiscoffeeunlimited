﻿Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Collections.Generic
Imports System.Data
Imports System.Net

Partial Class RecurringSchedule
    Inherits ExpandIT.UserControl
    Private calendar
    Protected orderDictionary As ExpDictionary
    Protected editable As Boolean = True
    Protected email As Boolean = False
    Protected count As Integer = 0
    Private m_isModifyRecurrency As Boolean = False
    Private m_IsActiveOrder As Boolean

    'AM2011032201 - RECURRING ORDERS STAND ALONE - START
    Protected recurringObj As USRecurringOrders
    'AM2011032201 - RECURRING ORDERS STAND ALONE - END

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If IsEditable Then
            Me.RecurrencyEditable.Visible = True
        Else
            Me.RecurrencyNoEditable.Visible = True
        End If
        'AM2011032201 - RECURRING ORDERS STAND ALONE - START
        recurringObj = New USRecurringOrders(globals)
        'AM2011032201 - RECURRING ORDERS STAND ALONE - END

    End Sub

    Public Property isModifyRecurrency() As Boolean
        Get
            Return m_isModifyRecurrency
        End Get
        Set(ByVal value As Boolean)
            m_isModifyRecurrency = value
        End Set
    End Property

    Public Property DictOrder() As ExpDictionary
        Get
            Return orderDictionary
        End Get
        Set(ByVal value As ExpDictionary)
            orderDictionary = value
        End Set
    End Property

    Public Property IsEditable() As Boolean
        Get
            Return editable
        End Get
        Set(ByVal value As Boolean)
            editable = value
        End Set
    End Property

    Public Property IsEmail() As Boolean
        Get
            Return email
        End Get
        Set(ByVal value As Boolean)
            email = value
        End Set
    End Property

    Public Property IsActiveOrder() As Boolean
        Get
            Return m_IsActiveOrder
        End Get
        Set(ByVal value As Boolean)
            m_IsActiveOrder = value
        End Set
    End Property

    Protected Function GetImageCalendar(ByVal index As Integer)
        Dim str As String = ""
        'index = index + 1
        If CBoolEx(IsEditable()) Then
            str = "<img alt=""Open Calendar"" class=""tcalIcon"" onclick=""A_TCALS['" & index & "'].f_toggle()"" id=""tcalico_" & index & """"
        Else
            str = "<img alt=""Open Calendar"" style=""display:none;"" class=""tcalIcon"" onclick=""A_TCALS['" & index & "'].f_toggle()"" id=""tcalico_" & index & """"
        End If
        str = str & " src=""" & VRoot & "/script/calendar/img/cal.gif"" title=""Open Calendar"" />"

        Return str
    End Function

    Protected Function GetScriptCalendar(ByVal guid As String)
        Dim str As String = ""
        str = "<script type=""text/javascript"" language=""javascript"">" & Environment.NewLine
        str = str & "var path=document.getElementById('vroot').value" & Environment.NewLine
        str = str & " var A_CALTPL = {" & Environment.NewLine
        str = str & " 'months' : ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']," & Environment.NewLine
        str = str & " 'weekdays' : ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa']," & Environment.NewLine
        str = str & " 'yearscroll': true," & Environment.NewLine
        str = str & " 'weekstart': 0," & Environment.NewLine
        str = str & " 'centyear' : 70," & Environment.NewLine
        str = str & " 'imgpath' : path + '/script/calendar/img/'" & Environment.NewLine
        str = str & " }" & Environment.NewLine & Environment.NewLine
        str = str & " new tcal ({" & Environment.NewLine
        str = str & " 'controlname': 'txbFinalDate_" & guid & "'" & Environment.NewLine
        str = str & " }, A_CALTPL);" & Environment.NewLine
        str = str & " </script>" & Environment.NewLine

        'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script1", " tcal ({ 'controlname': 'txbFinalDate_" & guid & "'}, A_CALTPL);", True)

        Return str
    End Function

    Protected Function GetScriptCalendarNextDate(ByVal guid As String)
        Dim str As String = ""
        str = "<script type=""text/javascript"" language=""javascript"">" & Environment.NewLine
        str = str & "var path=document.getElementById('vroot').value" & Environment.NewLine
        str = str & " var A_CALTPL = {" & Environment.NewLine
        str = str & " 'months' : ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']," & Environment.NewLine
        str = str & " 'weekdays' : ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa']," & Environment.NewLine
        str = str & " 'yearscroll': true," & Environment.NewLine
        str = str & " 'weekstart': 0," & Environment.NewLine
        str = str & " 'centyear' : 70," & Environment.NewLine
        str = str & " 'imgpath' : path + '/script/calendar/img/'" & Environment.NewLine
        str = str & " }" & Environment.NewLine & Environment.NewLine
        str = str & " new tcal ({" & Environment.NewLine
        str = str & " 'controlname': 'txbRNextOrderDate_" & guid & "'" & Environment.NewLine
        str = str & " }, A_CALTPL);" & Environment.NewLine
        str = str & " </script>" & Environment.NewLine

        'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script1", " tcal ({ 'controlname': 'txbFinalDate_" & guid & "'}, A_CALTPL);", True)

        Return str
    End Function

    Protected Function isOrderModified(ByVal lineHeader As String)
        Dim sql As String = "SELECT ModifiedRecurrency FROM ShopSalesHeader WHERE HeaderGuid=" & SafeString(lineHeader)
        Dim modified As String = CStrEx(getSingleValueDB(sql))
        If CStrEx(modified) <> "" Then
            Return True
        Else
            Return False
        End If
    End Function



End Class
