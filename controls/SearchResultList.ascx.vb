﻿Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass

Partial Class controls_SearchResultList
    Inherits ExpandIT.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim searchstring As String = ExpandIT.USSearch.SearchString
        'AM2010070201 - ENTERPRISE SEARCH - Start
        'If Not IsPostBack Then
        'GridView1.Sort("Name", SortDirection.Ascending)
        'End If
        'AM2010070201 - ENTERPRISE SEARCH - End
        If searchstring = "" Then
            GridView1.Visible = False
        Else
            GridView1.Visible = True
        End If

        If Not eis.CheckPageAccess("Prices") Then
            GridView1.Columns(4).Visible = False
        End If

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        GridView1.DataBind()
    End Sub

End Class