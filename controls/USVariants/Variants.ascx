<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Variants.ascx.vb" Inherits="Variants" %>
<table cellpadding="0" cellspacing="0">
    <tr>
        <asp:DataList ID="dlVariants" runat="server" RepeatLayout="Flow">
            <HeaderTemplate>
                <td style="padding: 0px;">
                    <div>
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="vertical-align: middle; padding: 0px; padding-right: 2px;">
                                    <%If Not CBoolEx(HideTitle()) Then%>
                                    <% =Resources.Language.LABEL_VARIANTS%>
                                    <%End If%>
                                </td>
                                <td style="vertical-align: middle; padding: 0px;">
                                    <%
                                        If CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) Then%>
                                        <%If UOMDisplayMode = UOMDisplayModes.List Then%>
                                            <select class="dpdBorderUOM" onchange="addToCartList('<%= ProductGuid()%>');calculateListMode('getUoms','<%= ProductGuid()%>');" name="VariantCode_<%= ProductGuid()%>" id="VariantCode_<%= ProductGuid()%>">
                                        <%Else%>
                                            <select class="dpdBorderUOM" onchange="showPriceLineUOM('<%= ProductGuid()%>');" name="VariantCode_<%= ProductGuid()%>" id="VariantCode_<%= ProductGuid()%>">
                                        <%End If%>
                                    <%Else%>
                                        <select class="dpdBorderUOM" onchange="setAddToCart('<%= ProductGuid()%>');getPricesNoUom('<%= ProductGuid()%>');" name="VariantCode_<%= ProductGuid()%>" id="VariantCode_<%= ProductGuid()%>">
                                    <%End If%>
            </HeaderTemplate>
            <ItemTemplate>
                <option style="<%# IIF(CBoolEx(Eval("Value")("IsSpecial")),"color:red;","") %>" value="<%# HTMLEncode(Eval("Value")("VariantCode")) %>">
                    <%# HTMLEncode(Eval("Value")("VariantName"))%>
                </option>
            </ItemTemplate>
            <FooterTemplate>
                </select> 
                </td> </tr> </table> </div></td>
            </FooterTemplate>
        </asp:DataList></tr>
</table>
<%  If CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) And UOMDisplayMode = UOMDisplayModes.List Then%>
    <%  Dim cont As Integer = 0%>
    <%  For cont = 1 To UOMCount()%>
    <input type="hidden" id="VariantCode_<%= ProductGuid() & "_" & cont %>" name="VariantCode"
        value="<% Response.Write(HTMLEncode(getFirstVariantCode())) %>" />
    <%  Next%>
<%--JA2010030901 - PERFORMANCE - Start--%>
<%  ElseIf not ProductDict is nothing AndAlso ProductDict("Variants") Is Nothing Then%>
<%--JA2010030901 - PERFORMANCE - End--%>
    <input type="hidden" id="VariantCode" name="VariantCode" value="" />
    <table cellpadding="0" cellspacing="0" style="height: 20px;">
    <tr>
        <td style="vertical-align: middle; padding: 0px; padding-right: 2px;">
            &nbsp;</td>
        <td style="vertical-align: middle; padding: 0px;">
            &nbsp;</td>
    </tr>
</table>
<%  End If%>

<script type="text/javascript" language="javascript" >

    function getPricesNoUom(guid){
        var Price=''; 
        if (document.getElementById('HiddenPriceNoUom_' + guid) != null){
            Price=document.getElementById('HiddenPriceNoUom_' + guid).value;
        } 
        
        var qty = document.getElementById('Quantity_' + guid).value;
        var variant;
        var valueVariant;
        if (document.getElementById('VariantCode_' + guid) != null){
            variant = document.getElementById('VariantCode_' + guid).value;     
            if(variant != ''){
                valueVariant= variant;
            }else{
                valueVariant= '';
            }   
        }else{
            valueVariant='';
        }    
        //Ajax
        if(Price != ''){
            var url= '<%=Vroot %>' + "/getPricingAjax.aspx?Price=" + Price + "&Guid=" + guid + "&minQty=" + qty + '&Currency=' + '<%=Session("UserCurrencyGuid").ToString %>' + '&Variant=' + valueVariant + '&NoUom=1';
            var xhReq = new XMLHttpRequest();  
            xhReq.open("GET", url += (url.match(/\?/) == null ? "?" : "&") + (new Date()).getTime(), false);
            xhReq.send(null);
            var html=xhReq.responseText;
            var start=html.indexOf('<body>') + 6;
            var end=html.indexOf('</body>');
            html=html.substring(start,end);
            document.getElementById('PriceNoUom_' + guid).value= html; 
        }
    }
    
    
    
    
</script>