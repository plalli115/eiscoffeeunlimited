Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Web
Imports System.Collections
Imports System.Collections.Generic


Partial Class Variants
    Inherits ExpandIT.UserControl


    Protected dictUOM As ExpDictionary
    Protected dictPriceLines As New ExpDictionary
    Protected dictPrices As New ExpDictionary
    Protected ProductDict As ExpDictionary
    Protected product As ProductClass

    Private m_productguid As String
    Private m_UOMDisplaymode As UOMDisplayModes = UOMDisplayModes.DropDown
    Private m_UOMCount As Integer
    Private m_HideTitle As Boolean = False

    Enum UOMDisplayModes
        DropDown = 0
        List = 1
    End Enum

    'JA2010030901 - PERFORMANCE - Start
    Public Property ProductDictPropertyVariant() As ExpDictionary
        Get
            Return ProductDict
        End Get
        Set(ByVal value As ExpDictionary)
            ProductDict = value
        End Set
    End Property
    'JA2010030901 - PERFORMANCE - End


    Public Property UOMDisplayMode() As UOMDisplayModes
        Get
            Return m_UOMDisplaymode
        End Get
        Set(ByVal value As UOMDisplayModes)
            m_UOMDisplaymode = value
        End Set
    End Property

    Public Property ProductGuid() As String
        Get
            Return CStrEx(m_productguid)
        End Get
        Set(ByVal value As String)
            m_productguid = CStrEx(value)
        End Set
    End Property

    Public Property UOMCount() As Integer
        Get
            Return m_UOMCount
        End Get
        Set(ByVal value As Integer)
            m_UOMCount = value
        End Set
    End Property
    Public Property HideTitle() As Boolean
        Get
            Return m_HideTitle
        End Get
        Set(ByVal value As Boolean)
            m_HideTitle = value
        End Set
    End Property

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If CStrEx(ProductGuid) <> "" Then
            'JA2010030901 - PERFORMANCE - Start
            'ProductDict = eis.CatDefaultLoadProduct(ProductGuid, True, True, True)
            If Not ProductDict Is Nothing Then
                Me.dlVariants.DataSource = ProductDict("Variants")
                Me.dlVariants.DataBind()
            End If
            'JA2010030901 - PERFORMANCE - End
        End If
    End Sub

    Public Function getFirstVariantCode()
        Dim VariantCode As String = ""
        'JA2010030901 - PERFORMANCE - Start
        If Not ProductDict Is Nothing AndAlso Not ProductDict("Variants") Is Nothing Then
            'JA2010030901 - PERFORMANCE - End
            For Each VariantDict As ExpDictionary In ProductDict("Variants").Values
                VariantCode = VariantDict("VariantCode")
                Exit For
            Next
        End If
        Return VariantCode
    End Function

End Class
