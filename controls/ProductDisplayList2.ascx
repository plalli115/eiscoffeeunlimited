<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ProductDisplayList2.ascx.vb"
    Inherits="ProductDisplayList2" %>
<%@ Register Src="AddToCartButton.ascx" TagName="AddToCartButton" TagPrefix="uc1" %>
    <%@ Import Namespace="ExpandIT" %>
    <%@ Import Namespace="ExpandIT.EISClass" %>
    <%@ Import Namespace="ExpandIT.ExpandITLib" %>
<div class="ProductDisplayList2">
    <asp:FormView ID="fvProduct" runat="server" RowStyle-CssClass="ProductDisplayListRow" CssClass="ProductDisplayListForm">
        <ItemTemplate> 
            <div class="ProductDisplayList_Guid">
                <asp:HyperLink ID="hlProductDisplayList_Guid" runat="server" SkinID="hlProductDisplayList_Name"
                    Text='<%# Eval("Guid") %>' NavigateUrl='<%# ProductLink(Eval("Guid"), globals.groupguid) %>'></asp:HyperLink>
            </div>
            <div class="ProductDisplayList_Name2">
                <asp:HyperLink ID="hlProductDisplayList_Name" runat="server" SkinID="hlProductDisplayList_Name"
                    Text='<%# Eval("Name") %>' NavigateUrl='<%# ProductLink(Eval("Guid"), globals.groupguid) %>'></asp:HyperLink>
            </div>
            <div class="ProductDisplayList_Thumbnail2">
                <asp:HyperLink ID="hlThumbnail" runat="server" SkinID="ProductDisplayList_Thumbnail"
                    ImageUrl='<%# Eval("ThumbnailURL") %>' Text='<%# Eval("Name") %>' NavigateUrl='<%# ProductLink(Eval("Guid"), globals.groupguid) %>'><%# Eval("Name") %></asp:HyperLink>
            </div>
            <div class="ProductDisplayList_Description2">
                <!-- Changed here to replace <br/> with "" and to only use Description -->
                <asp:HyperLink ID="hlDescription" runat="server" SkinID="ProductDisplayList_Description"
                    Text='<%# Replace(Eval("Description"),"<br/>"," ") %>' NavigateUrl='<%# ProductLink(Eval("Guid"), globals.groupguid) %>'><%# Eval("Name") %></asp:HyperLink>
            </div>
            <div class="ProductDisplayList_AddButton2">
               <uc1:AddToCartButton ID="AddButton1" runat="server" ProductGuid='<%# Eval("Guid") %>' />
            </div>
            <div class="ProductDisplayList_DetailsLink2">
                <asp:HyperLink ID="hlDetailsLink" SkinID="ProductDisplayList_DetailsLink" runat="server"
                    NavigateUrl='<%# ProductLink(Eval("Guid"), globals.groupguid) %>' Text="<%$Resources: Language, LABEL_DETAILS %>"></asp:HyperLink>
            </div>
            <div class="ProductDisplayList_Price2">
                <asp:HyperLink ID="hlPrice" runat="server" SkinID="ProductDisplayList_Price"
                    Text='<%# CurrencyFormatter.FormatCurrency(CDblEx(Eval("Price")), Session("UserCurrencyGuid").ToString) %>' NavigateUrl='<%# ProductLink(Eval("Guid"), globals.groupguid) %>'></asp:HyperLink>
            </div>
            <asp:Panel ID="panelSecondaryPrice" runat="server" Visible='<%# Eval("ShowSecondaryPrice") %>'>
                <div class="ProductDisplayList_SecondaryPrice2">
                    <asp:HyperLink ID="hlSecondaryPrice" runat="server" SkinID="ProductDisplayList_SecondaryPrice"
                        Text='<%# CurrencyFormatter.FormatCurrency(CDblEx(Eval("SecondaryPrice")), Session("UserCurrencyGuid").ToString) %>' NavigateUrl='<%# ProductLink(Eval("Guid"), globals.groupguid) %>'></asp:HyperLink>
                </div>
            </asp:Panel>

        </ItemTemplate>
    </asp:FormView>
</div>
