<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddToFavoritesButton.ascx.vb" Inherits="AddToFavoritesButton" %>
<asp:Button ID="btnFavorites" CssClass="AddToFavoritesButton" runat="server" Text="Add to Favorites" />
<asp:ImageButton ID="imgbtnFavorites" runat="server" />
<asp:LinkButton ID="linkbtnFavorites" runat="server">Add to Favorites</asp:LinkButton>
