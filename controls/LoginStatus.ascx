<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LoginStatus.ascx.vb" Inherits="LoginStatus" %>
<div class="LoginStatus">
    <asp:LinkButton ID="lbLoginStatus" runat="server" CssClass="LoginStatus" SkinID="LoginStatus">You are not logged in</asp:LinkButton>
</div>