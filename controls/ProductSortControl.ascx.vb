﻿Imports System.Collections 
Imports System.Collections.Generic
Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT31.Logic
Imports ExpandIT31.ExpanditFramework.Util


Partial Class ProductSortControl    
    Inherits ExpandIT.UserControl 
    Implements IEnumerable 
     
  Private m_FieldsList As String
  
  Private m_ProductSorter As ProductSorter 
  
  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    m_ProductSorter = New ProductSorter()
  End Sub
  
    Public Property DataSource() As Dictionary(Of Object, Object)
        Set(ByVal value As Dictionary(Of Object, Object))
            m_ProductSorter.DataSource = value
            If Not String.IsNullOrEmpty(SortedFieldsDropDownList.SelectedValue) Then
                m_ProductSorter.sortFieldName = SortedFieldsDropDownList.SelectedValue
            End If
        End Set
        Get
            Return m_ProductSorter.DataSource
        End Get
    End Property

    Public ReadOnly Property SortedDataSource() As Dictionary(Of Object, Object)
        Get
            Return m_ProductSorter.SortedDataSource
        End Get
    End Property

    Public ReadOnly Property getSelection() As String
        Get
            Return SortedFieldsDropDownList.SelectedValue
        End Get
    End Property

    Public Property FieldsList() As String
        Get
            Return m_FieldsList
        End Get
        Set(ByVal value As String)
            SortedFieldsDropDownList.Items.Clear()
            Dim itemBuff As String() = Utilities.Split(value, ",")
            For Each item As String In itemBuff
                Dim buff As String() = Utilities.Split(item, "|")
                SortedFieldsDropDownList.Items.Add(New ListItem(buff(0), buff(1)))
            Next
        End Set
    End Property

    Public Function GetEnumerator() As IEnumerator Implements IEnumerable.GetEnumerator
        If String.IsNullOrEmpty(m_ProductSorter.sortFieldName) Then
            m_ProductSorter.sortFieldName = SortedFieldsDropDownList.SelectedValue
        End If
        Return m_ProductSorter.GetEnumerator()
    End Function

    Protected Sub SortedFieldsDropDownList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SortedFieldsDropDownList.SelectedIndexChanged
        m_ProductSorter.sortFieldName = SortedFieldsDropDownList.SelectedValue
    End Sub

End Class
