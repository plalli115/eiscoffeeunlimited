Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.ExpandITLib
Imports ExpandIT.EISClass
Imports system.Drawing
Imports System.Drawing.Drawing2D
Imports System.Drawing.Imaging

Partial Class ProductDisplayList
    Inherits ExpandIT.UserControl

    Private m_product As ExpDictionary
    Protected m_productclass As ProductClass

    'JA2010030901 - PERFORMANCE -Start
    Enum DisplayModes
        DropDown = 0
        List = 1
    End Enum
    'JA2010030901 - PERFORMANCE -End


    <Themeable(False)> _
    Public Property Product() As ExpDictionary
        Get
            Return m_product
        End Get
        Set(ByVal value As ExpDictionary)
            m_product = value
        End Set
    End Property

    Protected Overloads Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender


        m_productclass = New ProductClass(m_product)
        m_productclass.GroupGuid = globals.GroupGuid
        Dim newdescription = ""

        If Not m_productclass.Description() Is Nothing Then
            If m_productclass.Description().Length > 150 Then
                m_productclass.Description2() = m_productclass.Description().Substring(0, 150) & "<ins class=""moreText"">...more</ins>"
            Else
                m_productclass.Description2() = m_productclass.Description()
            End If
        End If

        fvProduct.DataSource = New ProductClass() {m_productclass}


        fvProduct.DataBind()


    End Sub
    'AM0122201001 - UOM - Start

    'JA2010030901 - PERFORMANCE -Start
    Protected Sub fvProduct_DataBound(ByVal sender As Object, ByVal e As EventArgs) Handles fvProduct.DataBound

        'AM0122201001 - UOM - Start
        If AppSettings("EXPANDIT_US_USE_UOM") Then
            Dim lblGuid As Label = CType(fvProduct.FindControl("lblGuid"), Label)
            Dim lblSpecial As Label = CType(fvProduct.FindControl("lblSpecial"), Label)

            If Not lblGuid Is Nothing AndAlso Not lblSpecial Is Nothing Then

                Dim myControl As Control = Me.LoadControl("~/controls/USUOM/UOM.ascx")

                myControl.ID = "UOM" & CStrEx(lblGuid.Text)

                eis.SetControlProperties(myControl, "ProductGuid", CStrEx(lblGuid.Text))
                eis.SetControlProperties(myControl, "PostBackURL", addToCartLink(CStrEx(lblGuid.Text)))
                eis.SetControlProperties(myControl, "DisplayMode", DisplayModes.List)
                eis.SetControlProperties(myControl, "ShowCartBtn", True)
                eis.SetControlProperties(myControl, "ShowFavoritesBtn", True)
                eis.SetControlProperties(myControl, "DefaultQty", 1)
                eis.SetControlProperties(myControl, "IsSpecial", CBoolEx(lblSpecial.Text))
                eis.SetControlProperties(myControl, "ProductDictProperty", m_product)

                Dim ph As PlaceHolder = CType(fvProduct.FindControl("phUOM"), PlaceHolder)
                ph.Controls.Clear()
                ph.Controls.Add(myControl)
            End If
        End If
        'AM0122201001 - UOM - End

        'AM2010021901 - VARIANTS - Start
        If CBoolEx(AppSettings("EXPANDIT_US_USE_VARIANTS")) Then
            Dim lblGuidVariants As Label = CType(fvProduct.FindControl("lblGuidVariants"), Label)

            If Not lblGuidVariants Is Nothing Then
                Dim myControlVariant As Control = Me.LoadControl("~/controls/USVariants/Variants.ascx")

                myControlVariant.ID = "Variants" & CStrEx(lblGuidVariants.Text)

                eis.SetControlProperties(myControlVariant, "ProductGuid", CStrEx(lblGuidVariants.Text))
                eis.SetControlProperties(myControlVariant, "ProductDictPropertyVariant", m_product)
                eis.SetControlProperties(myControlVariant, "UOMDisplayMode", DisplayModes.List)

                Dim phV As PlaceHolder = CType(fvProduct.FindControl("phVariants"), PlaceHolder)
                phV.Controls.Clear()
                phV.Controls.Add(myControlVariant)
            End If
        End If
        'AM2010021901 - VARIANTS - End

    End Sub

    'JA2010030901 - PERFORMANCE -End



    Public Function addToCartLink(Optional ByVal ProductGuid As String = "")
        Dim link As String

        If ProductGuid <> "" Then
            link = "~/cart.aspx?cartaction=add&RetURL=" & Server.UrlEncode(Request.ServerVariables("script_name") & "?GroupGuid=" & globals.GroupGuid) & "&OnlyProduct=" & ProductGuid
        Else
            link = "~/cart.aspx?cartaction=add&RetURL=" & Server.UrlEncode(Request.ServerVariables("script_name") & "?GroupGuid=" & globals.GroupGuid)
        End If
        'AM0122201001 - UOM - End

        Return link
    End Function

    Protected Function isNotEmptyString(ByVal str As String) As Boolean
        Return Trim(Replace(Replace(Replace(str, Chr(10), ""), Chr(13), ""), Chr(9), "")) <> ""
    End Function

    Protected Function isEmptyString(ByVal str As String) As Boolean
        Return Trim(Replace(Replace(Replace(str, Chr(10), ""), Chr(13), ""), Chr(9), "")) = ""
    End Function

    Protected Function findDescription(ByVal description As String)

        If Not description Is Nothing Then
            If description.Length > 200 Then
                description = description.Substring(0, 200) & "<ins class=""moreText"">...more</ins>"
            End If
        End If
        Return description
    End Function


    Protected Function isNotDescription(ByVal strHtml As String, ByVal str As String) As Boolean
        If isNotEmptyString(strHtml) Or isNotEmptyString(str) Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Function getStyle()
        Dim style As String = m_productclass.ThumbnailURLStyle(100, 100)
        Return style
    End Function

End Class
