Imports System.Configuration.ConfigurationManager
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

Partial Class LoginStatus
    Inherits ExpandIT.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If globals.User("Anonymous") Then
            lbLoginStatus.Text = Resources.Language.LABEL_YOU_ARE_NOT_LOGGED_IN & "."
            lbLoginStatus.PostBackUrl = "~/user_login.aspx"
        Else
            lbLoginStatus.Text = Replace(Resources.Language.LABEL_LOGGED_IN_AS_NAME, "%1", CStrEx(globals.User("ContactName"))) & "."
            lbLoginStatus.PostBackUrl = "~/user.aspx"
        End If

    End Sub
End Class
