Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

Partial Class BestSellers
    Inherits ExpandIT.UserControl
    Protected MostPopularItems As ExpDictionary
    Protected method As String = ""
    
    Public Property SetMethod() As String
        Get
            Return method
        End Get
        Set(ByVal value As String)
            method = value
        End Set
    End Property
    

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If method.ToUpper.Contains("MY") Then
            MostPopularItems = eis.GetMyMostPopularItems(10)
        Else
            MostPopularItems = eis.GetMostPopularItems(10)
        End If

    End Sub
    

End Class
