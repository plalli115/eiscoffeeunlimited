<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LanguageFlags.ascx.vb"
    Inherits="LanguageFlags" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<asp:DataList ID="dlLanguages" SkinID="LanguageFlags" HorizontalAlign="Center" runat="server" RepeatColumns="6"
    RepeatDirection="horizontal"  >
    <HeaderTemplate>
        <input type="hidden" name="language_returl" value="<% = Request.ServerVariables("SCRIPT_NAME") %>" />
        <input type="hidden" name="ExpQueryString" value="<% = HTMLEncode(Request.QueryString()) %>" />
    </HeaderTemplate>
    <ItemTemplate>
        <asp:HyperLink ToolTip='<%#Eval("Value")("LanguageName")%>' ID="hlLanguageFlag" NavigateUrl='<%# "~/_language.aspx?LanguageGuid=" & Server.URLEncode(Eval("Value")("LanguageGuid")) & "&language_returl=" & Server.URLEncode(httpcontext.current.request.rawurl) %>'
            ImageUrl='<%# "~/images/languages/" & Eval("Value")("LanguageGuid") & ".png" %>'
            runat="server" Text='<%# htmlencode(Eval("Value")("LanguageGuid")) %>'><%#HTMLEncode(Eval("Value")("LanguageName"))%></asp:HyperLink>
    </ItemTemplate>
</asp:DataList>
