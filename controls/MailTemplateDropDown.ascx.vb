﻿Imports System.Configuration.ConfigurationManager
Imports System.Data
Imports System.Collections.Generic 
Imports ExpandIT
Imports ExpandIT.ExpandITLib
Imports ExpandIT.EISClass

Partial Class MailTemplateDropDown
    Inherits ExpandIT.UserControl

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    If Not IsPostBack Then
      ' Fill template list
      Dim languageISO As String = CStrEx(HttpContext.Current.Session("LanguageGuid")).ToLower()
      Dim templList As List(of String) = NoteMailContentBuilder.GetTemplateList(languageISO)
      For Each templName As String In templList
        ddlMailTemplate.Items.Add(templName)
      Next
    End If
  End Sub

  Protected Sub ddlMailTemplate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMailTemplate.SelectedIndexChanged

  End Sub
  
  Public ReadOnly Property SelectedValue As String
    Get
      Return ddlMailTemplate.SelectedValue()  
    End Get
  End Property
  
End Class
