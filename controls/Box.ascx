<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Box.ascx.vb" Inherits="Box"
    EnableTheming="true" %>
<table id="<% = IdPrefix %>BoxTable" class="BoxTable" cellpadding="0" cellspacing="0">
    <tr id="<% = IdPrefix %>BoxOverHeaderRowOuter" class="BoxOverHeaderRowOuter">
        <td id="<% = IdPrefix %>BoxOverHeaderCellOuter" class="BoxOverHeaderCellOuter">
            <table id="<% = IdPrefix %>BoxOverHeaderTableOuter" class="BoxOverHeaderTableOuter"
                cellpadding="0" cellspacing="0">
                <tr id="<% = IdPrefix %>BoxOverHeaderRow" class="BoxOverHeaderRow">
                    <td id="<% = IdPrefix %>BoxOverHeaderLeft" class="BoxOverHeaderLeft">
                        <div class="BoxCellFill">
                            <img class="BoxCellFill" src="<% = VRoot %>/images/p.gif" alt=""/></div>
                    </td>
                    <td id="<% = IdPrefix %>BoxOverHeaderCenter" class="BoxOverHeaderCenter">
                        <div class="BoxCellFill">
                            <img class="BoxCellFill" src="<% = VRoot %>/images/p.gif" alt=""/></div>
                    </td>
                    <td id="<% = IdPrefix %>BoxOverHeaderRight" class="BoxOverHeaderRight">
                        <div class="BoxCellFill">
                            <img class="BoxCellFill" src="<% = VRoot %>/images/p.gif" alt=""/></div> 
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr id="<% = IdPrefix %>BoxHeaderRowOuter" class="BoxHeaderRowOuter">
        <td id="<% = IdPrefix %>BoxHeaderCellOuter" class="BoxHeaderCellOuter">
            <table id="<% = IdPrefix %>BoxHeaderTableOuter" class="BoxHeaderTableOuter" cellpadding="0"
                cellspacing="0">
                <tr id="<% = IdPrefix %>BoxHeaderRow" class="BoxHeaderRow">
                    <td id="<% = IdPrefix %>BoxHeaderLeft" class="BoxHeaderLeft">
                        <div class="BoxCellFill">
                            <img class="BoxCellFill" src="<% = VRoot %>/images/p.gif" alt="" /></div>
                    </td>
                    <td id="<% = IdPrefix %>BoxHeaderCenter" class="BoxHeaderCenter">
                        <asp:PlaceHolder ID="phHeader" runat="server"></asp:PlaceHolder>
                    </td>
                    <td id="<% = IdPrefix %>BoxHeaderRight" class="BoxHeaderRight">
                        <div class="BoxCellFill">
                            <img class="BoxCellFill" src="<% = VRoot %>/images/p.gif" alt="" /></div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr id="<% = IdPrefix %>BoxUnderHeaderRowOuter" class="BoxUnderHeaderRowOuter">
        <td id="<% = IdPrefix %>BoxUnderHeaderCellOuter" class="BoxUnderHeaderCellOuter">
            <table id="<% = IdPrefix %>BoxUnderHeaderTableOuter" class="BoxUnderHeaderTableOuter"
                cellpadding="0" cellspacing="0">
                <tr id="<% = IdPrefix %>BoxUnderHeaderRow" class="BoxUnderHeaderRow">
                    <td id="<% = IdPrefix %>BoxUnderHeaderLeft" class="BoxUnderHeaderLeft">
                        <div class="BoxCellFill">
                            <img class="BoxCellFill" src="<% = VRoot %>/images/p.gif" alt="" /></div>
                    </td>
                    <td id="<% = IdPrefix %>BoxUnderHeaderCenter" class="BoxUnderHeaderCenter">
                        <div class="BoxCellFill">
                            <img class="BoxCellFill" src="<% = VRoot %>/images/p.gif" alt="" /></div>
                    </td>
                    <td id="<% = IdPrefix %>BoxUnderHeaderRight" class="BoxUnderHeaderRight">
                        <div class="BoxCellFill">
                            <img class="BoxCellFill" src="<% = VRoot %>/images/p.gif" alt="" /></div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr id="<% = IdPrefix %>BoxContentRowOuter" class="BoxContentRowOuter">
        <td id="<% = IdPrefix %>BoxContentCellOuter" class="BoxContentCellOuter">
            <table id="<% = IdPrefix %>BoxContentTableOuter" class="BoxContentTableOuter" cellpadding="0"
                cellspacing="0">
                <tr id="<% = IdPrefix %>BoxContentRow" class="BoxContentRow">
                    <td id="<% = IdPrefix %>BoxContentLeft" class="BoxContentLeft">
                        <div class="BoxCellFill">
                            <img class="BoxCellFill" src="<% = VRoot %>/images/p.gif" alt="" /></div>
                    </td>
                    <td id="<% = IdPrefix %>BoxContentCenter" class="BoxContentCenter">
                        <asp:PlaceHolder ID="phContent" runat="server"></asp:PlaceHolder>
                    </td>
                    <td id="<% = IdPrefix %>BoxContentRight" class="BoxContentRight">
                        <div class="BoxCellFill">
                            <img class="BoxCellFill" src="<% = VRoot %>/images/p.gif" alt="" /></div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr id="<% = IdPrefix %>BoxFooterRowOuter" class="BoxFooterRowOuter">
        <td id="<% = IdPrefix %>BoxFooterCellOuter" class="BoxFooterCellOuter">
            <table id="<% = IdPrefix %>BoxFooterTableOuter" class="BoxFooterTableOuter" cellpadding="0"
                cellspacing="0">
                <tr id="<% = IdPrefix %>BoxFooterRow" class="BoxFooterRow">
                    <td id="<% = IdPrefix %>BoxFooterLeft" class="BoxFooterLeft">
                        <div class="BoxCellFill">
                            <img class="BoxCellFill" src="<% = VRoot %>/images/p.gif" alt="" /></div>
                    </td>
                    <td id="<% = IdPrefix %>BoxFooterCenter" class="BoxFooterCenter" style="height: 2px;">
                        <div class="BoxCellFill">
                            <div class="BoxCellFill">
                                <img class="BoxCellFill" src="<% = VRoot %>/images/p.gif" alt="" /></div>
                        </div>
                    </td>
                    <td id="<% = IdPrefix %>BoxFooterRight" class="BoxFooterRight">
                        <div class="BoxCellFill">
                            <img class="BoxCellFill" src="<% = VRoot %>/images/p.gif" alt="" /></div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
