Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.ExpandITLib

Partial Class MiniCartText
    Inherits ExpandIT.UserControl

    Protected OrderPadInfo As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        globals.OrderDict = eis.LoadOrderDictionary(globals.User)

        If Not globals.MiniCartInfo("CurrencyGuid") = globals.User("CurrencyGuid") OrElse Not DBNull2Nothing(globals.OrderDict("Total")) = DBNull2Nothing(globals.MiniCartInfo("Total")) Then
            EISClass.SetRecalcUserCart(Session("UserGuid"))
            globals.OrderDict = eis.CalculateOrder()
            eis.UpdateMiniCartInfo(globals.OrderDict)
        End If

        OrderPadInfo = OrderRender.RenderMiniCart(globals.MiniCartInfo, eis.CheckPageAccess("Prices"))
        lbMiniCart.Text = OrderPadInfo

        If CStrEx(AppSettings("MasterPageFolder")).Contains("Enterprise") Then
            Me.lbMiniCart.Style.Add(HtmlTextWriterStyle.Color, "White")
        End If

    End Sub

End Class
