<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="FavoriteLine.ascx.vb"
    Inherits="FavoriteLine" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%--<asp:Panel ID="Panel1" runat="server" SkinID="FavoriteLine" CssClass="FavoriteLinePanel">--%>
<%--AM0122201001 - UOM - Start--%>
<%@ Register Src="~/controls/USUOM/UOM.ascx" TagName="UOM" TagPrefix="uc1" %>
<%--AM0122201001 - UOM - End--%>
<%--AM2010021901 - VARIANTS - Start--%>
<%@ Register Src="~/controls/USVariants/Variants.ascx" TagName="Variants" TagPrefix="uc1" %>
<%--AM2010021901 - VARIANTS - End--%>
<asp:FormView ID="fvProduct" runat="server" SkinID="FavoriteLine" Width="100%" Style="margin-bottom: 10px;"
    CssClass="FavoriteLineFormView" CellPadding="0">
    <ItemTemplate>
        <div class="FavoriteLine">
            <div class="FavoriteLineDelete">
                <div class="List_Item">
                    <asp:ImageButton ID="DeleteBtn" runat="server" ImageUrl="~/images/p.gif" CssClass="FavoriteLineDelete" />
                    <%--JA0531201001 - STAY IN CATALOG - START--%>
                    <input id='img_<%# Eval("Guid")%>' type="hidden" value='<%# Eval("ThumbnailURL") %>' />
                    <%--JA0531201001 - STAY IN CATALOG - END--%>
                </div>
            </div>
            <%--WLB Add Pictures to Favorites 11/17/15 - BEGIN--%>
            <div class="FavoriteLineProductName">
                <div class="List_Item">
                        <asp:Image ID="Image1" CssClass="FavoriteLineProductName" ImageUrl='<%# Eval("ThumbnailURL") %>'
                            width="125" Height="125" runat="server" />
                </div>
            </div>
            <%--WLB Add Pictures to Favorites 11/17/15 - END--%>
            <asp:Panel ID="panelProductGuid" runat="server" Visible='<%# AppSettings("HIDE_PRODUCTGUID") <> "1"  %>'>
                <div class="FavoriteLineProductGuid">
                    <div class="List_Item">
                        <asp:HyperLink ID="HyperLink3" runat="server" CssClass="FavoriteLineProductName"
                            NavigateUrl='<%# Eval("ProductLinkURL") %>'><%#Eval("Guid")%></asp:HyperLink>
                    </div>
                </div>
            </asp:Panel>
            <div class="FavoriteLineProductName">
                <div class="List_Item">
                    <asp:HyperLink ID="HyperLink1" runat="server" CssClass="FavoriteLineProductName"
                        NavigateUrl='<%# Eval("ProductLinkURL") %>'><%# Eval("Name") %></asp:HyperLink>
                </div>
            </div>
            <!--AM2010021901 - VARIANTS - Start-->
            <%  If CBoolEx(AppSettings("EXPANDIT_US_USE_VARIANTS")) Then%>
            <div class="FavoriteLineVariants">
                <div class="List_Item">
                    <%--<uc1:Variants ID="Variants1" runat="server" ProductGuid='<%# Eval("Guid") %>' HideTitle="true" />--%>
                    <asp:Label ID="lblGuidVariants" Style="display: none;" runat="server" Text='<%# Eval("Guid") %>'></asp:Label>
                    <asp:PlaceHolder ID="phVariants" runat="server" />
                </div>
            </div>
            <%  End If%>
            <!--AM2010021901 - VARIANTS - End-->
            <%--AM0122201001 - UOM - Start--%>
            <%If Not CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) Then%>
            <div class="FavoriteLineQuantity">
                <div class="List_Item">
                    <%--<input type="hidden" name="SKU" value="<%#Eval("Guid")%>" />
                    <input class="FavoriteLineQuantity favoriteQuantity" type="text" name="Quantity"
                        size="4" value="" />--%>
                    <input type="text" class="FavoriteLineQuantity favoriteQuantity" id="Quantity_<%# Eval("Guid") %>"
                        onblur="setAddToCart();" name="Quantity_<%# Eval("Guid") %>" size="4" tabindex="1" />
                </div>
            </div>
            <%Else%>
            <div class="FavoriteLineUOMs">
                <%--<uc1:UOM ID="UOM1" ProductGuid='<%# Eval("Guid") %>' runat="server" DisplayMode="DropDown"
                    PostBackURL='<%# addtocartlink(Eval("Guid"))%>' ShowFavoritesBtn="false" ShowCartBtn="true"
                    DefaultQty="1" />--%>
                <asp:Label ID="lblGuid" Style="display: none;" runat="server" Text='<%# Eval("Guid") %>'></asp:Label>
                <asp:Label ID="lblSpecial" Style="display: none;" runat="server" Text='<%# CBoolEx(Eval("IsSpecial")) %>'></asp:Label>
                <asp:PlaceHolder ID="phUOM" runat="server" />
            </div>
            <%End If%>
            <%--AM0122201001 - UOM - End--%>
        </div>
    </ItemTemplate>
</asp:FormView>

<script type="text/javascript" language="javascript">

    //JA0531201001 - STAY IN CATALOG - START
    function showLigthBox(myguid){
                
        var variant='';
        if (document.getElementById('VariantCode_' + myguid) != null){
            variant = document.getElementById('VariantCode_' + myguid).value;
            var index=document.getElementById('VariantCode_' + myguid).selectedIndex;
            var varianName=document.getElementById('VariantCode_' + myguid).options[index].text;
        }    
        var image=document.getElementById('img_' + myguid).value;
        var url= '<%=Vroot %>' + "/AddToCartAjax.aspx?Guid=" + myguid + "&Image=" + image + "&VariantCode=" + variant + '&VariantName=' + varianName;
        var xhReq = new XMLHttpRequest();  
        xhReq.open("GET", url += (url.match(/\?/) == null ? "?" : "&") + (new Date()).getTime(), false);
        xhReq.send(null);
        var html=xhReq.responseText;
        var start=html.indexOf('<body>') + 6;
        var end=html.indexOf('</body>');
        html=html.substring(start,end);
        
        document.getElementById('lightboxIdAddToCart').className=html;
        
        actuateLink(document.getElementById('lightboxIdAddToCart'));            
              
    }        
    function stayInPage(){
        var str =window.location.href;
        str=str.replace('#','');
        window.location=str;        
    }
    function goToCart(){
        window.location='<%=Vroot%>/cart.aspx';        
    } 
    
    function actuateLink(link){
       var allowDefaultAction = true;
          
       if (link.click){
          link.click();
          return;
       }else if (document.createEvent){
          var e = document.createEvent('MouseEvents');
          e.initEvent(
             'click'     // event type
             ,true      // can bubble?
             ,true      // cancelable?
          );
          allowDefaultAction = link.dispatchEvent(e);           
       }
             
       if (allowDefaultAction){
          var f = document.createElement('form');
          f.action = link.href;
          document.body.appendChild(f);
          f.submit();
       }
    }
    //JA0531201001 - STAY IN CATALOG - END

</script>

<%--</asp:Panel>--%>
