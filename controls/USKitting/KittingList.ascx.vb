'AM2010061801 - KITTING - START
Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Web
Imports ExpandIT.GlobalsClass
Imports System.Collections.Generic

Partial Class KittingList
    Inherits ExpandIT.UserControl

    Private m_KittingParts As ExpDictionary
    

    Public Property KittingParts() As ExpDictionary
        Get
            Return m_KittingParts
        End Get
        Set(ByVal value As ExpDictionary)
            m_KittingParts = value
        End Set
    End Property

    'Protected Sub dlKitting_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim dl As DataList = sender

    '    dl.DataSource = KittingParts
    'End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        dlKitting.DataSource = KittingParts
        dlKitting.DataBind()
    End Sub
End Class
'AM2010061801 - KITTING - END