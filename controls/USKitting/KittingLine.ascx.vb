'AM2010061801 - KITTING - START
Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Web
Imports ExpandIT.GlobalsClass
Imports System.Collections.Generic

Partial Class KittingLine
    Inherits ExpandIT.UserControl

    Private m_KittingPart As ExpDictionary
    Protected m_productclass As ProductClass


    Public Property KittingPart() As ExpDictionary
        Get
            Return m_KittingPart
        End Get
        Set(ByVal value As ExpDictionary)
            m_KittingPart = value
        End Set
    End Property

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        m_productclass = New ProductClass(KittingPart)
        fvKitting.DataSource = New ProductClass() {m_productclass}
        fvKitting.DataBind()
    End Sub

    Public Function setThumbnailStyle() As String
        Return m_productclass.ThumbnailURLStyle(50, 50)
    End Function

End Class
'AM2010061801 - KITTING - END