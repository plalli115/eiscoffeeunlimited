<%--AM2010061801 - KITTING - START--%>
<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="KittingLine.ascx.vb"
    Inherits="KittingLine" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Import Namespace="ExpandIT.GlobalsClass" %>
<asp:FormView ID="fvKitting" runat="server" Width="100%">
    <ItemTemplate>
        <table width="100%">
            <tr style="width: 100%;">
                <% If AppSettings("KITTING_BROWSER_SHOWCOLUMN_THUMBNAIL") Then%>
                <td style="width: 10%; vertical-align:middle;">
                    <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl='<%# Eval("ProductLinkURL")%>'>
                        <asp:Image ID="imgThumbnail" Style='<%# setThumbnailStyle() %>' ImageUrl='<%# Eval("ThumbnailURL")%>'
                            runat="server" />
                    </asp:HyperLink>
                </td>
                <%End If%>
                <% If AppSettings("KITTING_BROWSER_SHOWCOLUMN_GUID") Then%>
                <td style="width: 10%; vertical-align:middle;" align="right">
                    <asp:HyperLink ID="HyperLink1" runat="server" Text='<%# Eval("Guid") %>' NavigateUrl='<%# Eval("ProductLinkURL")%>'>
                    </asp:HyperLink>
                </td>
                <%End If%>
                <%--AM2010021901 - VARIANTS - Start--%>
                <% If AppSettings("KITTING_BROWSER_SHOWCOLUMN_NAME") And (CBoolEx(AppSettings("EXPANDIT_US_USE_VARIANTS")) And AppSettings("KITTING_BROWSER_SHOWCOLUMN_VARIANT")) Then%>
                    <td style="width: 65%; vertical-align:middle;" align="left">
                        <asp:HyperLink ID="HyperLink2" runat="server" Visible='<%# CStrEx(Eval("Data")("VariantDescription"))<>"" %>' Text='<%# Eval("Name") & " (" & Eval("Data")("VariantDescription") & ")" %>'
                            NavigateUrl='<%# Eval("ProductLinkURL")%>'>
                        </asp:HyperLink>
                        <asp:HyperLink ID="HyperLink3" runat="server" Visible='<%# CStrEx(Eval("Data")("VariantDescription"))="" %>' Text='<%# Eval("Name") %>'
                            NavigateUrl='<%# Eval("ProductLinkURL")%>'>
                        </asp:HyperLink>
                    </td>
                <%Else%>
                    <% If AppSettings("KITTING_BROWSER_SHOWCOLUMN_NAME") Then%>
                        <td style="width: 65%; vertical-align:middle;" align="left">
                            <asp:HyperLink ID="HyperLink6" runat="server" Text='<%# Eval("Name") %>' NavigateUrl='<%# Eval("ProductLinkURL")%>'>
                            </asp:HyperLink>
                        </td>
                    <%Else%>
                        <%--AM2010021901 - VARIANTS - Start--%>
                        <%If CBoolEx(AppSettings("EXPANDIT_US_USE_VARIANTS")) And AppSettings("KITTING_BROWSER_SHOWCOLUMN_VARIANT") Then%>
                            <td style="width: 65%; vertical-align:middle;" align="left">
                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("Data")("VariantDescription") %>'>
                                </asp:Label>
                            </td>
                        <%End If%>
                        <%--AM2010021901 - VARIANTS - End--%>
                    <%End If%>
                <%End If%>
                <%--AM2010021901 - VARIANTS - End--%>
                <% If AppSettings("KITTING_BROWSER_SHOWCOLUMN_QUANTITY") Or (AppSettings("EXPANDIT_US_USE_UOM") And AppSettings("KITTING_BROWSER_SHOWCOLUMN_UOM")) Then%>
                <td style="width: 15%; vertical-align:middle;" align="left">
                    <% If AppSettings("KITTING_BROWSER_SHOWCOLUMN_QUANTITY") Then%>
                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("Data")("Quantity") %>'></asp:Label>
                    <%End If%>
                    <%--AM0122201001 - UOM - Start--%>
                    <% If AppSettings("EXPANDIT_US_USE_UOM") And AppSettings("KITTING_BROWSER_SHOWCOLUMN_UOM") Then%>
                    <asp:Label ID="Label2" runat="server" Text='<%# "(" & Eval("Data")("UOMDescription") & ")" %>'>
                    </asp:Label>
                    <%End If%>
                    <%--AM0122201001 - UOM - End--%>
                </td>
                <%End If%>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
<%--AM2010061801 - KITTING - END--%>
