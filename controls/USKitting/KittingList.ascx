<%--AM2010061801 - KITTING - START--%>
<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="KittingList.ascx.vb"
    Inherits="KittingList" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Import Namespace="ExpandIT.GlobalsClass" %>
<%--AM2010061801 - KITTING - START--%>
<%@ Register Src="~/controls/USKitting/KittingLine.ascx" TagName="KittingLine" TagPrefix="uc1" %>
<%--AM2010061801 - KITTING - END--%>
<asp:DataList ID="dlKitting" SkinID="ProductTemplate" runat="server" Width="100%">
    <ItemTemplate>
        <uc1:KittingLine ID="KittingLine1" runat="server" KittingPart='<%# Eval("Value") %>' />
    </ItemTemplate>
</asp:DataList>
<%--AM2010061801 - KITTING - END--%>
