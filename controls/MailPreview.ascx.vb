﻿Imports System.Xml 
Imports System.IO
Imports System.Web
Imports System.Collections.Generic 
Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.ExpandITLib
Imports System.Net.Mail
Imports ExpandIT31.ExpanditFramework.Util


Partial Class MailPreview
    Inherits ExpandIT.UserControl
  
  Dim m_contentHtml As string
  Dim m_mail As MailMessage
  

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load    
    m_contentHtml = Session("NoteMailHtml")
    Dim contentDOM As New XmlDocument()
    Dim titleText As String = ""
    Try      
      contentDOM.LoadXml(m_contentHtml)
      Dim titleNode As XmlNode = contentDOM.SelectSingleNode("/html/head/title")
      If Not (titleNode is Nothing) then
        titleText = titleNode.FirstChild.Value
      End If         
    Catch ex As Exception   
    End Try
    If Not IsPostBack then
      subjTextBox.Text = titleText 
    End if
  End Sub

  Protected Sub sendnote_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles sendnote.Click
    'CreateMessage -> Embed Images -> Send
    Dim fromAddress As String = CStrEx(globals.User("EmailAddress"))
    Dim recipient As New ExpDictionary()
    Dim split As String() = toTextBox.Text.Split(New [Char]() {","c, ";"c})
    for Each addr As String In split
      recipient.Add(addr,  addr) 
    Next     
    PrepareMail(fromAddress, recipient)
    MailUtils.ConfigMailer() 
    if Mailer.SendEmail(AppSettings("MAIL_REMOTE_SERVER"), AppSettings("MAIL_REMOTE_SERVER_PORT"), m_mail, globals.messages)
      globals.messages.Messages.Add(Resources.Language.MESSAGE_MAIL_SEND_OK)
    Else 
      globals.messages.Errors.Add(Resources.Language.MESSAGE_MAIL_SEND_FAIL)  
    End If
  End Sub
  
  Private Sub PrepareMail(fromAddress As String, recipients As ExpDictionary)
    m_mail = Mailer.CreateMailMessage(fromAddress, recipients, subjTextBox.Text, true, Encoding.UTF8)
    Dim templDirVirtualPath As String = String.Format("~/{0}/",AppSettings("NOTE_MAIL_FOLDER"))
    MailUtils.EmbedImages(m_contentHtml, templDirVirtualPath, m_mail)    
  End Sub  
  
  
End Class
