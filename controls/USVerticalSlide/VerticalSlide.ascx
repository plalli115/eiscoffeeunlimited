<%--AM2010062201 - VERTICAL SLIDE - START--%>
<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="VerticalSlide.ascx.vb"
    Inherits="VerticalSlide" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Import Namespace="ExpandIT.GlobalsClass" %>
<%@ Register Src="~/controls/CreditCards.ascx" TagName="CreditCards" TagPrefix="uc1" %>

<script type="text/javascript">
    jQuery(document).ready(function() {
	jQuery("#side-nav a").vertSlider(
								{
								
								});
	  
							});
</script>

<%--JA2010071601 - RATINGS AND REVIEWS - START--%>
<script type="text/javascript">
            animatedcollapse.addDiv('ReadReviews', 'fade=1')
            animatedcollapse.addDiv('RateItem', 'fade=1')
            animatedcollapse.ontoggle=function(jQuery, divobj, state){}
            animatedcollapse.init()            
    </script>
<%--JA2010071601 - RATINGS AND REVIEWS - END--%>

<%  If CIntEx(AppSettings("VERTICAL_SLIDE_DISPLAY_MODE")) = 1 Then%>
<div id="wrapper-content">
    <div id="side-nav" class="mobile-remove">
        <asp:Panel ID="pnlButtonsDivs" runat="server">
        </asp:Panel>
    </div>
    <div id="main-container">
        <div id="main-container-background">
            <asp:Panel runat="server" ID="pnlControlsDivs">
            </asp:Panel>
        </div>
    </div>
</div>
<%  ElseIf CIntEx(AppSettings("VERTICAL_SLIDE_DISPLAY_MODE")) = 2 Then%>
<div id="Div1">
    <div id="Div2" class="mobile-remove">
        <asp:Panel ID="Panel1" runat="server">
        </asp:Panel>
    </div>
    <div id="Div3">
        <div id="Div4">
            <asp:Panel runat="server" ID="pnlControlsDivsList">
            </asp:Panel>
        </div>
    </div>
</div>
<%End If %>
<%--AM2010062201 - VERTICAL SLIDE - START--%>
