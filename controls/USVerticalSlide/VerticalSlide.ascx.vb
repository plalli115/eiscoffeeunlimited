'AM2010062201 - VERTICAL SLIDE - START
Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Web
Imports ExpandIT.GlobalsClass
Imports System.Collections.Generic

Partial Class VerticalSlide
    Inherits ExpandIT.UserControl

    Protected m_ControlsDict As New ExpDictionary

    Property ControlsDict() As ExpDictionary
        Get
            Return m_ControlsDict
        End Get
        Set(ByVal value As ExpDictionary)
            m_ControlsDict = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        buildPanelsDivs()
    End Sub

    Public Sub buildPanelsDivs()
        Dim divControlObj As HtmlGenericControl
        Dim divControlObjTitles As HtmlGenericControl
        Dim divButtonObj As HtmlGenericControl
        Dim spanButtonObj As HtmlGenericControl
        Dim aButtonObj As HtmlAnchor
        Dim counter As Integer = 0
        Dim controlObj As ExpandIT.UserControl
        Dim tableObj As HtmlTable
        Dim tdObj As HtmlTableCell
        Dim trObj As HtmlTableRow

        For Each controlKey As String In ControlsDict.Keys



            If CIntEx(AppSettings("VERTICAL_SLIDE_DISPLAY_MODE")) = 2 Then
                divControlObjTitles = New HtmlGenericControl("DIV")
                tableObj = New HtmlTable()
                trObj = New HtmlTableRow()
                tdObj = New HtmlTableCell()
                tdObj.Style.Add("vertical-align", "middle")
                tdObj.Align = "right"
                spanButtonObj = New HtmlGenericControl("SPAN")
                spanButtonObj.InnerText = controlKey
                spanButtonObj.Style.Add("font-weight", "bold")
                tdObj.Controls.Add(spanButtonObj)
                trObj.Cells.Add(tdObj)
                tableObj.Rows.Add(trObj)
                divControlObjTitles.Controls.Add(tableObj)
                Me.pnlControlsDivsList.Controls.Add(divControlObjTitles)

            End If



            controlObj = ControlsDict(controlKey)
            counter += 1
            divControlObj = New HtmlGenericControl("DIV")
            divControlObj.Attributes.Add("class", "row-273 mobile-public")
            'JA2010071601 - RATINGS AND REVIEWS - START
            If controlKey = Resources.Language.LABEL_RATINGS_REVIEWS Then
                divControlObj.Attributes.Add("align", "left")
            Else
                divControlObj.Attributes.Add("align", "center")
            End If
            'JA2010071601 - RATINGS AND REVIEWS - END

            If CIntEx(AppSettings("VERTICAL_SLIDE_DISPLAY_MODE")) = 2 Then
                divControlObj.Style.Add("border-Width", "1px")
                divControlObj.Style.Add("border-Color", "gray")
                divControlObj.Style.Add("border-style", "solid")
                divControlObj.Style.Add("margin-bottom", "10px")
                divControlObj.Style.Add("width", "480px")
            End If

            tableObj = New HtmlTable()
            tableObj.Height = "100%"
            tableObj.Width = "96%"
            trObj = New HtmlTableRow()
            tdObj = New HtmlTableCell()
            tdObj.Style.Add("vertical-align", "middle")
            tdObj.Align = "right"
            tdObj.Controls.Add(controlObj)
            trObj.Cells.Add(tdObj)
            tableObj.Rows.Add(trObj)
            divControlObj.Controls.Add(tableObj)

            If CIntEx(AppSettings("VERTICAL_SLIDE_DISPLAY_MODE")) = 1 Then
                Me.pnlControlsDivs.Controls.Add(divControlObj)
            ElseIf CIntEx(AppSettings("VERTICAL_SLIDE_DISPLAY_MODE")) = 2 Then
                Me.pnlControlsDivsList.Controls.Add(divControlObj)
            End If

            divButtonObj = New HtmlGenericControl("DIV")
            If counter > 1 Then
                divButtonObj.Attributes.Add("class", "row-24 section-1 nav remove-right-border")
            Else
                divButtonObj.Attributes.Add("class", "row-24 section-1 nav")
            End If
            aButtonObj = New HtmlAnchor()
            aButtonObj.ID = "section-" & counter
            aButtonObj.Attributes.Add("class", "section")
            spanButtonObj = New HtmlGenericControl("SPAN")
            spanButtonObj.InnerText = controlKey
            aButtonObj.Controls.Add(spanButtonObj)
            divButtonObj.Controls.Add(aButtonObj)
            Me.pnlButtonsDivs.Controls.Add(divButtonObj)




        Next
    End Sub

End Class
'AM2010062201 - VERTICAL SLIDE - END