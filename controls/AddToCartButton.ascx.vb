Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

<Themeable(True)> _
Partial Class AddToCartButton
    Inherits ExpandIT.UserControl

    Private m_groupguid As String
    Private m_productguid As String
    Private m_displaymode As DisplayModes = DisplayModes.Button
    Private m_inputMode As Boolean

    Protected m_mode As String
    Protected m_uom As String
    Protected m_quantity As String

    Enum DisplayModes
        Button = 0
        Link = 1
        Image = 2
    End Enum

    <Themeable(False)> _
    Public Property ProductGuid() As String
        Get
            Return m_productguid
        End Get
        Set(ByVal value As String)
            m_productguid = value
        End Set
    End Property

    <Themeable(False)> _
    Public Property GroupGuid() As String
        Get
            Return m_groupguid
        End Get
        Set(ByVal value As String)
            m_groupguid = value
        End Set
    End Property

    Public Property ImageURL() As String
        Get
            Return imgbtnAdd.ImageUrl
        End Get
        Set(ByVal value As String)
            imgbtnAdd.ImageUrl = ThemeURL(value)
        End Set
    End Property

    Public Property DisplayMode() As DisplayModes
        Get
            Return m_displaymode
        End Get
        Set(ByVal value As DisplayModes)
            m_displaymode = value
        End Set
    End Property

    Public Property InputMode() As Boolean
        Get
            Return m_inputMode
        End Get
        Set(ByVal value As Boolean)
            m_inputMode = value
        End Set
    End Property

    Public Property SetMode() As String
        Get
            Return m_mode
        End Get
        Set(ByVal value As String)
            m_mode = value
        End Set
    End Property

    'Public Property SetQuantity() As String
    '    Get
    '        Return m_quantity
    '    End Get
    '    Set(ByVal value As String)
    '        m_quantity = value
    '    End Set
    'End Property

    Public Property GetUOM() As String
        Get
            Return m_uom
        End Get
        Set(ByVal value As String)
            m_uom = value
        End Set
    End Property

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim buttonText As String = Resources.Language.ACTION_ADD_TO_ORDER_PAD
        Dim postbackurl As String = "~/cart.aspx?cartaction=add"

        If CStrEx(m_productguid) <> "" Then
            postbackurl = postbackurl & "&SKU=" & HTMLEncode(m_productguid)
        End If

        'If CStrEx(m_quantity) <> "" Then
        '    postbackurl = postbackurl & "&Quantity=" & m_quantity
        'Else
        '    postbackurl = postbackurl & "&Quantity=1"
        'End If

        postbackurl = postbackurl & "&Quantity="

        'If CStrEx(m_uom) <> "" Then
        '    postbackurl = postbackurl & "&UOM=" & m_uom
        'Else
        '    postbackurl = postbackurl & "&UOM=PCS"
        'End If

        postbackurl = postbackurl & "&UOM="

        If CStrEx(m_mode) <> "" Then
            postbackurl = postbackurl & "&MODE=" & m_mode
        Else
            postbackurl = postbackurl & "&MODE="
        End If


        postbackurl = postbackurl & "&GroupGuid=" & IIf(GroupGuid Is Nothing, globals.GroupGuid, GroupGuid)

        'changed the script string to handle apostrophs in javascript
        Dim clientscript As String = "AddToCart('" & postbackurl & "&VariantCode=' + findSelectedValue('VariantCode'));new Tooltip().schedule(this,event,'" & Replace(Resources.Language.ACTION_ADDED_TO_CART, "'", "\'") & "');return false;"

        If eis.CheckPageAccess("Cart") Then

            btnAdd.Visible = m_displaymode = DisplayModes.Button
            imgbtnAdd.Visible = m_displaymode = DisplayModes.Image
            linkbtnAdd.Visible = m_displaymode = DisplayModes.Link

            Select Case m_displaymode
                Case DisplayModes.Button
                    With btnAdd
                        If eis.CheckPageAccess("Cart") Then
                            If UseClientScriptPost() Then
                                .OnClientClick = clientscript
                            Else
                                .PostBackUrl = postbackurl
                            End If
                            .Text = buttonText
                        Else
                            .Visible = False
                        End If
                    End With
                Case DisplayModes.Image
                    With imgbtnAdd
                        If eis.CheckPageAccess("Cart") Then
                            If UseClientScriptPost() Then
                                .OnClientClick = clientscript
                            Else
                                .PostBackUrl = postbackurl
                            End If
                            .AlternateText = buttonText
                        Else
                            .Visible = False
                        End If
                    End With
                Case DisplayModes.Link
                    With linkbtnAdd
                        If eis.CheckPageAccess("Cart") Then
                            If UseClientScriptPost() Then
                                .OnClientClick = clientscript
                            Else
                                .PostBackUrl = postbackurl
                            End If
                            .Text = buttonText
                        Else
                            .Visible = False
                        End If
                    End With
            End Select
        Else
            btnAdd.Visible = False
            imgbtnAdd.Visible = False
            linkbtnAdd.Visible = False
        End If
    End Sub

    Private Function UseClientScriptPost() As Boolean
        Return CBoolEx(AppSettings("STAY_IN_CATALOG")) And Not InputMode
    End Function

End Class
