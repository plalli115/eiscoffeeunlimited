﻿<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ProductSortControl.ascx.vb"
    Inherits="ProductSortControl" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<asp:Panel ID="ProductSorterPanel" runat="server" CssClass="ProductSortPanel">
    <table>
        <tr>
            <td style="vertical-align: middle;">
                <asp:Label ID="Label1" runat="server" Style="color: White; font-weight: bold;" Text="<%$Resources: Language,LABEL_SORT_ON_FIELD %>"></asp:Label>
            </td>
            <td style="padding-left: 5px;">
                <%--<div class="dpdBorder">--%>
                    <asp:DropDownList ID="SortedFieldsDropDownList" runat="server" Style="height: 16px;
                        font-family: Tahoma; font-size: x-small;" AutoPostBack="true">
                    </asp:DropDownList>
                <%--</div>--%>
            </td>
        </tr>
    </table>
</asp:Panel>
