Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.ExpandITLib
Imports ExpandIT.EISClass

Partial Class GroupHeaderSubGroups
    Inherits System.Web.UI.UserControl

    Protected m_subgroup As ExpDictionary
    Protected pageobj As ExpandIT.Page = CType(System.Web.HttpContext.Current.Handler, ExpandIT.Page)

    Public Property SubGroup() As ExpDictionary
        Get
            Return m_subgroup
        End Get
        Set(ByVal value As ExpDictionary)
            m_subgroup = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        litHTMLDescription.Text = IIf(SubGroup("DESCRIPTION") <> "", SubGroup("DESCRIPTION"), "")
        'Me.Picture1.ImageUrl = VRoot & "/" & SubGroup("PICTURE2")
        Me.litName.Text = SubGroup("NAME")
    End Sub


    Protected Function getStyle()
        Dim style As String = ""
        If Not SubGroup Is Nothing AndAlso Not SubGroup Is DBNull.Value AndAlso SubGroup.Count > 0 Then
            style = pageobj.eis.ThumbnailURLStyleMethod(SubGroup("PICTURE2"), "250", "250")
        End If

        Return style
    End Function


End Class
