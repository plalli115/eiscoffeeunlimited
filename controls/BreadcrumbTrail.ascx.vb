Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

<Themeable(True)> _
Partial Class BreadcrumbTrailControl
    Inherits ExpandIT.UserControl

    Protected m_pathseparator As String = "/"

    Public Property PathSeparator() As String
        Get
            Return m_pathseparator
        End Get
        Set(ByVal value As String)
            m_pathseparator = value
        End Set
    End Property

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim path As New StringBuilder

        eis.InitCatalogStructure(globals.GroupGuid)

        Dim p As ExpandIT.Page = Page
        If p.CrumbName <> "" Then globals.breadcrumbTrail.AddCurrentLocation(p.CrumbName)

        For Each bc As Breadcrumb In globals.breadcrumbTrail
            If path.Length > 0 Then path.Append("<div class=""BreadcrumbSeparator"">" & m_pathseparator & "</div>")
            path.Append("<div class=""Breadcrumb""><a class=""Breadcrumb"" href=""" & HTMLEncode(bc.URL) & """>" & HTMLEncode(bc.Caption) & "</a></div>")
        Next
        Literal1.Text = path.ToString

    End Sub
End Class
