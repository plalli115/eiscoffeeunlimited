﻿Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

<Themeable(True)> _
Partial Class NoteLine
    Inherits ExpandIT.UserControl

    Private m_product As ExpDictionary
    Protected m_productclass As ProductClass
    
    
    <Themeable(False)> _
    Public Property Product() As ExpDictionary
        Get
            Return m_product
        End Get
        Set(ByVal value As ExpDictionary)
            m_product = value
        End Set
    End Property
    
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        m_productclass = New ProductClass(m_product)
        
        m_productclass.GroupGuid = globals.GroupGuid
        fvProduct.DataSource = New ProductClass() {m_productclass}
        fvProduct.DataBind()
        
        Dim DeleteBtn As ImageButton = FindChildControl(Me, "DeleteBtn")
        DeleteBtn.AlternateText = Resources.Language.LABEL_REMOVE_PRODUCT_FROM_NOTES
        Dim url as String 
        url = String.Format("/_notes.aspx?action=OutOfNotes&ProductGuid={0}&VariantCode={1}&NNN={2}", _
          HttpUtility.UrlEncode(m_productclass.Guid), HTMLEncode(m_product("VariantCode")), (m_product("ListName"))) 
        DeleteBtn.PostBackUrl = VRoot &  url 
    End Sub
    
End Class
