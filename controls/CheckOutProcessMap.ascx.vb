'AM2010080301 - CHECK OUT PROCESS MAP - Start
Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

Partial Class CheckOutProcessMap
    Inherits ExpandIT.UserControl

    Private m_CheckOutProcessDict As String()
    Private m_currentLocation As String

    Public Property CheckOutProcessDict() As String()
        Get
            Return m_CheckOutProcessDict
        End Get
        Set(ByVal value As String())
            m_CheckOutProcessDict = value
        End Set
    End Property

    Public Property currentLocation() As String
        Get
            Return m_currentLocation
        End Get
        Set(ByVal value As String)
            m_currentLocation = value
        End Set
    End Property

    Public Function getPathTitle(ByVal pathAddress As String) As String
        Dim title As String = ""
        If pathAddress <> "" Then
            pathAddress = Mid(pathAddress, 1, pathAddress.IndexOf("."))
            pathAddress = "CHECK_OUT_PROCESS_MAP_" & pathAddress.ToUpper()
            title = HttpContext.GetGlobalResourceObject("Language", pathAddress)
        End If
        Return title
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not CheckOutProcessDict Is Nothing Then
            Dim i As Integer = 0
            Dim j As Integer = 0
            Dim tempCheckOutProcessDict() As String
            Dim cartPosi As Integer = Array.IndexOf(CheckOutProcessDict, "cart.aspx")

            If cartPosi >= 0 Then
                ReDim tempCheckOutProcessDict(CheckOutProcessDict.Length - 1)
            Else
                ReDim tempCheckOutProcessDict(CheckOutProcessDict.Length)
            End If
            For i = 0 To CheckOutProcessDict.Length - 1
                If i <> cartPosi Then
                    tempCheckOutProcessDict(j) = CheckOutProcessDict(i)
                    j += 1
                End If
            Next
            tempCheckOutProcessDict(j) = "confirmed.aspx"
            CheckOutProcessDict = tempCheckOutProcessDict
        End If
    End Sub
End Class
'AM2010080301 - CHECK OUT PROCESS MAP - End