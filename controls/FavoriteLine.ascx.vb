Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

<Themeable(True)> _
Partial Class FavoriteLine
    Inherits ExpandIT.UserControl

    Private m_product As ExpDictionary
    Protected m_productclass As ProductClass
    Dim pageobj As ExpandIT.Page = CType(System.Web.HttpContext.Current.Handler, ExpandIT.Page)


    'JA2010030901 - PERFORMANCE -Start
    Enum DisplayModes
        DropDown = 0
        List = 1
    End Enum
    'JA2010030901 - PERFORMANCE -End




    <Themeable(False)> _
    Public Property Product() As ExpDictionary
        Get
            Return m_product
        End Get
        Set(ByVal value As ExpDictionary)
            m_product = value
        End Set
    End Property

    'JA2010030901 - PERFORMANCE -Start

    Protected Overloads Sub Page_Prerender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender

        m_productclass = New ProductClass(m_product)
        m_productclass.GroupGuid = globals.GroupGuid
        fvProduct.DataSource = New ProductClass() {m_productclass}
        fvProduct.DataBind()

        Dim DeleteBtn As ImageButton = FindChildControl(Me, "DeleteBtn")
        DeleteBtn.AlternateText = Resources.Language.LABEL_REMOVE_PRODUCT_FROM_FAVORITES
        DeleteBtn.PostBackUrl = VRoot & "/_favorites.aspx?action=OutOfFavorite&ProductGuid=" & HttpUtility.UrlEncode(m_productclass.Guid)


        'AM0122201001 - UOM - Start
        If AppSettings("EXPANDIT_US_USE_UOM") Then

            Dim myControl As Control = Me.LoadControl("~/controls/USUOM/UOM.ascx")

            myControl.ID = "UOM" & CStrEx(m_product("ProductGuid"))

            pageobj.eis.SetControlProperties(myControl, "ProductGuid", CStrEx(m_product("ProductGuid")))
            pageobj.eis.SetControlProperties(myControl, "PostBackURL", addToCartLink(CStrEx(m_product("ProductGuid"))))
            pageobj.eis.SetControlProperties(myControl, "DisplayMode", DisplayModes.DropDown)
            pageobj.eis.SetControlProperties(myControl, "ShowCartBtn", True)
            pageobj.eis.SetControlProperties(myControl, "ShowFavoritesBtn", True)
            pageobj.eis.SetControlProperties(myControl, "DefaultQty", 1)
            pageobj.eis.SetControlProperties(myControl, "IsSpecial", CBoolEx(m_product("IsSpecial")))
            pageobj.eis.SetControlProperties(myControl, "ProductDictProperty", m_product)

            Dim phUOM As PlaceHolder = CType(fvProduct.FindControl("phUOM"), PlaceHolder)
            If Not phUOM Is Nothing Then
                phUOM.Controls.Clear()
                phUOM.Controls.Add(myControl)
            End If

        End If
        'AM0122201001 - UOM - End

        'AM2010021901 - VARIANTS - Start
        If CBoolEx(AppSettings("EXPANDIT_US_USE_VARIANTS")) Then

            Dim myControlVariant As Control = Me.LoadControl("~/controls/USVariants/Variants.ascx")

            myControlVariant.ID = "Variants" & CStrEx(m_product("ProductGuid"))

            pageobj.eis.SetControlProperties(myControlVariant, "ProductGuid", CStrEx(m_product("ProductGuid")))
            pageobj.eis.SetControlProperties(myControlVariant, "ProductDictPropertyVariant", m_product)
            pageobj.eis.SetControlProperties(myControlVariant, "HideTitle", False)


            Dim phVAR As PlaceHolder = CType(fvProduct.FindControl("phVariants"), PlaceHolder)
            If Not phVAR Is Nothing Then
                phVAR.Controls.Clear()
                phVAR.Controls.Add(myControlVariant)
            End If

        End If
        'AM2010021901 - VARIANTS - End
    End Sub


    'JA2010030901 - PERFORMANCE -End



    'AM0122201001 - UOM - Start
    Public Function addToCartLink(ByVal ProductGuid As String)
        Dim link As String

        If ProductGuid <> "" Then
            link = "~/cart.aspx?cartaction=add&OnlyProduct=" & ProductGuid
        Else
            link = "~/cart.aspx?cartaction=add"
        End If
        Return link
    End Function
    'AM0122201001 - UOM - End

End Class
