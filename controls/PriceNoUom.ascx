
<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PriceNoUom.ascx.vb" Inherits="PriceNoUom" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>

<%--AM2011060201 - PRICES BREAKDOWN - Start--%>
<%@ Register Src="~/controls/PricesBreakDown.ascx" TagName="PricesBreakDown" TagPrefix="uc1" %>
<%--AM2011060201 - PRICES BREAKDOWN - End--%>


<uc1:PricesBreakDown ID="PricesBreakDown1" ProductGuid="<%=Guid() %>" runat="server" />


<%  Dim myStyle As String = ""%>
<%  If styleText() <> "" Then%>
    <%myStyle = styleText()%>
<%End If %>
<input id="HiddenPriceNoUom_<%=Guid() %>" value="<%= CDblEx(Price()) %>" type="hidden" />
<input id="PriceNoUom_<%=Guid() %>" type="text" class="<%=CssClassText %>" style="<%= myStyle  %>border-color:transparent; font-size:14px;" readonly="readonly" value="<%= CurrencyFormatter.FormatCurrency(CDblEx(Price()), HttpContext.Current.Session("UserCurrencyGuid")) %>" />