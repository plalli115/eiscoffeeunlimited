'AM2010051801 - SPECIAL ITEMS - Start
Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

Partial Class Specials
    Inherits ExpandIT.UserControl
    Protected retv As ExpDictionary
    Protected randomNumber As New Random()

    Private Sub Page_PreRender(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.PreRender
        For Each item As DataListItem In Me.dlProducts.Items
            Dim lblToFormatRegular As Label
            Dim lblToFormatSpecial As Label

            lblToFormatRegular = CType(item.FindControl("lblRegularPrice"), Label)
            lblToFormatRegular.Text = CurrencyFormatter.FormatCurrency(CDblEx(lblToFormatRegular.Text), HttpContext.Current.Session("UserCurrencyGuid"))

            lblToFormatSpecial = CType(item.FindControl("lblSpecialPrice"), Label)
            lblToFormatSpecial.Text = CurrencyFormatter.FormatCurrency(CDblEx(lblToFormatSpecial.Text), HttpContext.Current.Session("UserCurrencyGuid"))

        Next
    End Sub
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim sql As String = ""
        Dim specialsCampaign As String = ""

        If globals.Context Is Nothing Then
            globals.Context = eis.LoadContext(globals.User)
        End If
        If globals.OrderDict Is Nothing Then
            globals.OrderDict = eis.LoadOrderDictionary(globals.User)
        End If


        'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - Start
        'specialsCampaign = CStrEx(getSingleValueDB("SELECT SpecialsCampaign FROM EESetup"))
        Try
            specialsCampaign = CStrEx(eis.getEnterpriseConfigurationValue("SpecialsCampaign"))
        Catch ex As Exception
        End Try
        'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - End

        If AppSettings("REQUIRE_CATALOG_ENTRY") Then
            '*****CUSTOMER SPECIFIC CATALOGS***** - START
            If AppSettings("EXPANDIT_US_USE_CSC") Then
                'JA2010030901 - PERFORMANCE -Start
                Dim objCSC As New csc(globals)
                sql = objCSC.loadSpecials()
                'JA2010030901 - PERFORMANCE -End
                '*****CUSTOMER SPECIFIC CATALOGS***** - END
                'AM2010071901 - ENTERPRISE CSC - Start
            Else
                sql = "SELECT A.ProductGuid FROM Attain_ProductPrice A INNER JOIN (SELECT DISTINCT " & _
                    "GroupProduct.ProductGuid FROM GroupProduct WHERE GroupProduct.CatalogNo=" & SafeString(globals.User("CatalogNo")) & _
                    ") AS GP ON A.ProductGuid=GP.ProductGuid WHERE "
                'sql = "SELECT A.ProductGuid FROM Attain_ProductPrice A INNER JOIN (SELECT DISTINCT " & _
                '"GroupProduct.ProductGuid FROM GroupProduct) AS GP ON A.ProductGuid=GP.ProductGuid "
            End If
        Else
            sql = "SELECT ProductGuid FROM Attain_ProductPrice A WHERE "
        End If

        sql &= "CustomerRelType = 3 AND CustomerRelGuid = " & SafeString(specialsCampaign) & " AND " & _
            "(StartingDate <= " & SafeDatetime(DateValue(Now)) & " OR StartingDate IS NULL) AND " & _
            "(EndingDate >= " & SafeDatetime(DateValue(Now)) & " OR EndingDate IS NULL) AND "
        'AM2010071901 - ENTERPRISE CSC - End
        If Not globals.OrderDict Is Nothing And CStrEx(globals.OrderDict("CurrencyGuid")) <> "" Then
            sql &= "(CurrencyGuid = " & SafeString(globals.OrderDict("CurrencyGuid")) & " OR "
        Else
            sql &= "("
        End If
        sql &= "CurrencyGuid IS NULL OR CurrencyGuid = '' OR CurrencyGuid = " & _
            SafeString(globals.Context("DefaultCurrency")) & ") ORDER BY A.ProductGuid"

        retv = Sql2Dictionaries(sql, "ProductGuid")
        'JA2010030901 - PERFORMANCE -Start
        'eis.CatDefaultLoadProducts(retv, False, False, False, New String() {"ProductName", "ListPrice", "BaseUOM"}, New String() {"DESCRIPTION", "PICTURE2"}, "")
        retv = eis.CatDefaultLoadProducts(retv, False, False, False, New String() {"ProductName", "ListPrice", "BaseUOM"}, New String() {"DESCRIPTION", "PICTURE2"}, "SpecialItems")
        'JA2010030901 - PERFORMANCE -End

        eis.GetCustomerPricesEx(retv)

        Dim item As ExpDictionary
        Dim item2 As ExpDictionary
        If Not retv Is Nothing And Not retv.Count = 0 Then

            Dim num As Integer = randomNumber.Next(1, retv.Count + 1)
            Dim num2 As Integer
            Dim cont As Integer = 1
            Dim retv2 As New ExpDictionary
            For Each item In retv.Values
                If cont = num Then
                    retv2.Add(item("ProductGuid"), item)
                    If retv.Count > 1 Then
                        num2 = randomNumber.Next(1, retv.Count + 1)
                        If num = num2 Then
                            While num = num2
                                num2 = randomNumber.Next(1, retv.Count + 1)
                                If num <> num2 Then
                                    Exit While
                                End If
                            End While
                        End If
                        cont = 1
                        For Each item2 In retv.Values
                            If cont = num2 Then
                                retv2.Add(item2("ProductGuid"), item2)
                                Exit For
                            Else
                                cont = cont + 1
                            End If
                        Next
                    End If
                Else
                    cont = cont + 1
                End If
                If retv2.Count = 2 Then
                    Exit For
                End If
            Next


            dlProducts.DataSource = retv2
            dlProducts.DataBind()

        End If

    End Sub

    Protected Function getStyle(ByVal src As Object)
        Dim str As String = ""
        str = eis.ThumbnailURLStyleMethod(src, CIntEx(AppSettings("SPECIALS_WIDTH_IMG")), CIntEx(AppSettings("SPECIALS_HEIGHT_IMG")))
        Return str
    End Function

    Protected Function getSrc(ByVal pic As String)
        Dim root As String = ""
        If CStrEx(pic) = "" Then
            root = VRoot & "/images/nopix75.png"
        Else
            root = VRoot & "/" & pic
        End If

        Return root
    End Function

    Protected Function getLink(ByVal link As String)
        Dim root As String = ProductLink(link, 0)
        Return root
    End Function

End Class
'AM2010051801 - SPECIAL ITEMS - End