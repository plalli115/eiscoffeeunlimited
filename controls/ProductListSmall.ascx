<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ProductListSmall.ascx.vb"
    Inherits="ProductListSmall" %>
<%@ Register Src="~/controls/ProductBoxSmall.ascx" TagName="ProductBoxSmall" TagPrefix="uc1" %>
<%--JA2010061701 - SLIDE SHOW - Start--%>
<%@ Register Src="~/controls/slideshow/GenericSlideShow.ascx" TagName="GenericSlideShow"
    TagPrefix="uc1" %>
<%  If AppSettings("SHOW_SLIDE_PRODUCT") And Not m_products Is Nothing Then%>
    <uc1:GenericSlideShow SlideShowName="1" SlideShowPrice="1" SlideShowWidth="100%" SlideShowHeight="220px" SlideShowPosition="1"
    SlideShowArrows="1" SlideImageHeight="150" SlideShowFooterBar="0"  SlideImageWidth="150" ID="GenericSlideShow2"
    runat="server" />
<%--JA2010061701 - SLIDE SHOW - End--%>
<%  Else%>
<asp:DataList ID="dlProducts" runat="server" SkinID="ProductListSmall" RepeatColumns="4"
    RepeatDirection="Horizontal" ItemStyle-Width="25%" ItemStyle-VerticalAlign="top">
    <ItemTemplate>
        <uc1:ProductBoxSmall ID="ProductBoxSmall1" runat="server" Product='<%# Eval("Value") %>' />
        <br />
    </ItemTemplate>
</asp:DataList>
<%  End If%>
