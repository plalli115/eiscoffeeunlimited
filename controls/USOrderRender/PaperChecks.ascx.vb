'JA2010102801 - PAPERCHECK - Start
Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Web
Imports ExpandIT.GlobalsClass
Imports System.Collections.Generic

Partial Class PaperChecks
    Inherits ExpandIT.UserControl

    Protected checks As ExpDictionary
    Protected ActiveOrder As Boolean


    Public Property OrderDict() As ExpDictionary
        Get
            Return checks
        End Get
        Set(ByVal value As ExpDictionary)
            checks = value
        End Set
    End Property

    Public Property IsActiveOrder() As Boolean
        Get
            Return ActiveOrder
        End Get
        Set(ByVal value As Boolean)
            ActiveOrder = value
        End Set
    End Property

    Protected Overloads Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        
    End Sub

    Public Function formatAmount(ByVal amount As Double)
        If CStrEx(OrderDict("CurrencyGuid")) <> "" Then
            Return CurrencyFormatter.FormatCurrency(amount, OrderDict("CurrencyGuid"))
        Else
            Return CurrencyFormatter.FormatCurrency(amount, CStrEx(AppSettings("MULTICURRENCY_SITE_CURRENCY")))
        End If
    End Function

    Public Function deleteLink(ByVal checkGuid As String)
        If checkGuid <> "" Then
            Return "payment.aspx?DeletePaperCheck=" & checkGuid
        Else
            Return ""
        End If
    End Function

End Class
'JA2010102801 - PAPERCHECK - End