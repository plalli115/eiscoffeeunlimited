<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CartRenderLine.ascx.vb"
    Inherits="CartRenderLine" %>
<%@ Register Src="~/controls/AddToCartButton.ascx" TagName="AddToCartButton" TagPrefix="uc1" %>
<%@ Register Src="~/controls/USKitting/KittingList.ascx" TagName="KittingList" TagPrefix="uc1" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>

<%--AM2010092201 - ENTERPRISE CSR - Start --%>
<tr>
    <asp:Panel ID="pnlReturnReason" runat="server" Visible="False">
        <td></td>
        <td align="left" style="vertical-align:middle;" colspan="<%= colspan-1 %>">
            <%If Not reasonDict Is Nothing Then%>
                <%=Resources.Language.LABEL_CSR_RETURN_REASON%>:
                <select name="<%= HTMLEncode(OrderLine("ProductGuid")) %>"  id="<%=HTMLEncode(OrderLine("ProductGuid")) %>">
                <%For Each d As ExpDictionary In reasonDict.Values %>
                    <option value="<%= d("Code") %>"><%= d("Description") %></option>
                <%Next %>
                </select> 
            <%End If%>
        </td>
    </asp:Panel>
    <asp:Panel ID="ShowReturnReasons" runat="server" Visible="False">
        <td></td>
        <td align="left" colspan="<%= colspan -1 %>" style="vertical-align:middle; color:#6c6c6c;font-weight:bold;">
            <%If Not OrderLine() Is Nothing Then%>
            <%=Resources.Language.LABEL_CSR_RETURN_REASON%>:
                <% Dim description As String = getSingleValueDB("SELECT Description FROM ReturnReason WHERE Code=" & SafeString(CStrEx(OrderLine("ReturnReason"))))%>
                <%=CStrEx(description)%>
            <%End If%>
        </td>
    </asp:Panel>
</tr>
<%--AM2010092201 - ENTERPRISE CSR - End --%>
<tr class="CartLineFields">
    <%--Thumbnail--%>
    <asp:Panel ID="pnlThumbnail" runat="server" Visible="false">
        <td class="CartLineField_Thumbnail">
            <a href="<% = product.ProductLinkURL %>">
                <img style="<%= product.ThumbnailURLStyle(100, 100)  %>" src="<% = product.ThumbnailURL %>"
                    alt="<% = product.ThumbnailText%>" border="0" />
            </a>
        </td>
    </asp:Panel>
    <%--ProductGuid--%>
    <asp:Panel ID="pnlProductGuidLine" runat="server" Visible="false">
        <%If Not IsEmail Then%>
        <td class="CartLineCaption_ProductGuid">
            <% = HTMLEncode(Resources.Language.LABEL_PRODUCT) %>
        </td>
        <%End If%>
        <td class="CartLineField_ProductGuid">
            <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start --%>
            <%If Not IsLedger OrElse CBoolEx(OrderLine("Available")) Then%>
            <a href="<% = ProductLink(orderline("ProductGuid"), 0)%>">
            <%End If %>
                <% = HTMLEncode(orderline("ProductGuid")) %>
            <%If Not IsLedger OrElse CBoolEx(OrderLine("Available")) Then%>
            </a>
            <%End If %>
            <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End --%>
        </td>
    </asp:Panel>
    <%--ProductName--%>
    <asp:Panel ID="pnlNameLine" runat="server" Visible="false">
        <td class="CartLineCaption_Name">
            <% = HTMLEncode(Resources.Language.LABEL_NAME)%>
        </td>
        <td class="CartLineField_Name">
            <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start --%>
            <%If Not IsLedger OrElse CBoolEx(OrderLine("Available")) Then%>
            <a href="<% = ProductLink(orderline("ProductGuid"), 0)%>">
            <%End If %>
                <% = HTMLEncode(orderline("ProductName"))%>
            <%If Not IsLedger OrElse CBoolEx(OrderLine("Available")) Then%>
            </a>
            <%End If %>
            <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End --%>
        </td>
        <%--VariantName--%>
        <asp:Panel ID="pnlVariantNameLine" runat="server" Visible="false">
            <td class="CartLineCaption_VariantName">
                <% = HTMLEncode(HttpContext.GetGlobalResourceObject("Language", "LABEL_VARIANTNAME"))%>
            </td>
            <td class="CartLineField_VariantName">
                <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start --%>
                <%If Not IsLedger OrElse CBoolEx(OrderLine("Available")) Then%>
                <a href="<% = ProductLink(orderline("ProductGuid"), 0)%>">
                <%End If %>
                    <% =HTMLEncode(OrderLine("VariantName"))%>
                <%If Not IsLedger OrElse CBoolEx(OrderLine("Available")) Then%>
                </a>
                <%End If %>
                <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End --%>
            </td>
        </asp:Panel>
    </asp:Panel>
    <%--ProductNameAndVariant--%>
    <asp:Panel ID="pnlNameAndVariant" runat="server" Visible="false">
        <td class="CartLineField_NameAndVariant">
            <div class="CartLineField_NameAndVariant">
            <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start --%>
            <%If Not IsLedger OrElse CBoolEx(OrderLine("Available")) Then%>
                <a href="<% = ProductLink(orderline("ProductGuid"), 0)%>">
            <%End If %>
                    <% = HTMLEncode(orderline("ProductName"))%>
                    <%If CStrEx(OrderLine("VariantCode")) <> "" Then%>
                    <% = " (" & HTMLEncode(orderline("VariantName")) & ")" %>
                    <%End If%>
                <%If Not IsLedger OrElse CBoolEx(OrderLine("Available")) Then%>
                </a>
                <%End If %>
                <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End --%>
            </div>
        </td>
    </asp:Panel>
    <%--ProductQuantity--%>
    <asp:Panel ID="pnlQuantityEdit" runat="server" Visible="false">
        <td class="CartLineCaption_EditQuantity">
            <% =  HTMLEncode(Resources.Language.LABEL_QUANTITY)%>
        </td>
        <td class="CartLineField_EditQuantity">
            <input type="hidden" name="LineGuid" value="<% = HTMLEncode(orderline("LineGuid"))%>" />
            <input type="hidden" name="VersionGuid" value="<% = HTMLEncode(orderline("VersionGuid"))%>" />
            <input type="hidden" name="SKU" value="<% = HTMLEncode(orderline("ProductGuid"))%>" />
            <input type="hidden" name="VariantCode" value="<% = HTMLEncode(orderline("VariantCode"))%>" />
            <input type="hidden" name="Quantity_prev" value="<% = HTMLEncode(orderline("Quantity"))%>" />
            <%If DefaultButton IsNot Nothing Then%>
            <input size="5" maxlength="5" class="CartLineField_EditQuantity cartQuantity" type="text"
                name="Quantity" value="<% = orderline("Quantity")%>" onkeydown="<% = eis.addOnKeyDownAttributeValue(defaultButton.ClientID)%>" />
            <%Else%>
            <input size="5" maxlength="5" class="CartLineField_EditQuantity cartQuantity" type="text"
                name="Quantity" value="<% = orderline("Quantity")%>" />
            <%End If%>
        </td>
    </asp:Panel>
    <asp:Panel ID="pnlQuantityNoEdit" runat="server" Visible="false">
        <%If Not IsEmail() Then%>
        <td class="CartLineCaption_Quantity">
            <% =HTMLEncode(Resources.Language.LABEL_QUANTITY)%>
        </td>
        <%End If%>
        <td class="CartLineField_Quantity">
            <% = HTMLEncode(orderline("Quantity"))%>
        </td>
    </asp:Panel>
    <%--AM0122201001 - UOM - Start--%>
    <asp:Panel ID="pnlUOMLine" runat="server" Visible="false">
        <td class="CartLineField_Price" style="white-space: nowrap;">
            <% = HTMLEncode(uom) %>
        </td>
    </asp:Panel>
    <%--AM0122201001 - UOM - End--%>
    <%--Unit Price--%>
    <asp:Panel ID="pnlPriceLine" runat="server" Visible="false">
        <%If Not IsEmail() Then%>
        <td class="CartLineCaption_Price">
            <% =HTMLEncode(Resources.Language.LABEL_PRICE)%>
        </td>
        <%End If%>
        <td class="CartLineField_Price" style="white-space: nowrap;">
            <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start--%>
            <%If eis.CheckPageAccess("Prices") Then %>
            <% =HTMLEncode(formatAmount(unitprice, OrderLine("CurrencyGuid")))%>
            <%End If %>
            <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End--%>
        </td>
    </asp:Panel>
    <%--Discounts--%>
    <asp:Panel ID="pnlDiscountAmountLine" runat="server" Visible="false">
        <%If Not IsEmail() Then%>
        <td class="CartLineCaption_DiscountAmount">
            <%If eis.CheckPageAccess("Prices") Then %>
            <% =HTMLEncode(Resources.Language.LABEL_DISC_AMOUNT)%>
            <%End If %>
        </td>
        <%End If%>
        <td class="CartLineField_DiscountAmount">
            <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start--%>
            <%If eis.CheckPageAccess("Prices") Then %>
            <% =HTMLEncode(formatAmount(discountamount, OrderLine("CurrencyGuid")))%>
            <%End If %>
            <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End--%>
        </td>
    </asp:Panel>
    <asp:Panel ID="pnlDiscountPctLine" runat="server" Visible="false">
        <%If Not IsEmail() Then%>
        <td class="CartLineCaption_DiscountPct">
            <%If eis.CheckPageAccess("Prices") Then %>
            <% =HTMLEncode(Resources.Language.LABEL_DISC_PROCENT)%>
            <%End If %>
        </td>
        <%End If%>
        <td class="CartLineField_DiscountPct">
            <%If eis.CheckPageAccess("Prices") Then %>
            <% =HTMLEncode(RoundEx(CDblEx(OrderLine("LineDiscount")), 4)) & "%"%>
            <%End If %>
        </td>
    </asp:Panel>
    <%--Line Total--%>
    <asp:Panel ID="pnlTotalLine" runat="server" Visible="false">
        <%If Not IsEmail() Then%>
        <td class="CartLineCaption_Total">
            <%If eis.CheckPageAccess("Prices") Then %>
            <% =HTMLEncode(Resources.Language.LABEL_LINE_TOTAL)%>
            <%End If %>
        </td>
        <%End If%>
        <td class="CartLineField_Total" style="white-space: nowrap;">
            <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start--%>
            <%If eis.CheckPageAccess("Prices") Then %>
            <% =HTMLEncode(formatAmount(linetotal, OrderLine("CurrencyGuid")))%>
            <%End If %>
            <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End--%>
        </td>
    </asp:Panel>
    <%--Line Comments--%>
    <asp:Panel ID="pnlCommentLineEdit" runat="server" Visible="false">
        <%If Not IsEmail() Then%>
        <td class="CartLineCaption_Comment">
            <% =HTMLEncode(Resources.Language.LABEL_LINE_COMMENT)%>
        </td>
        <%End If%>
        <td class="CartLineField_EditComment">
            <div class="CartLineField_EditComment">
                <input class="CartLineField_EditComment" type="text" name="LineComment" size="10"
                    maxlength="50" value="<% = HTMLEncode(orderline("LineComment"))%>" />
                <input type="hidden" name="LineComment_prev" value="<% = HTMLEncode(orderline("LineComment"))%>" />
            </div>
        </td>
    </asp:Panel>
    <asp:Panel ID="pnlCommentLineNoEdit" runat="server" Visible="false">
        <%If Not IsEmail() Then%>
        <td class="CartLineCaption_Comment">
            <% =HTMLEncode(Resources.Language.LABEL_LINE_COMMENT)%>
        </td>
        <%End If%>
        <td class="CartLineField_Comment">
            <div class="CartLineField_Comment">
                <% =HTMLEncode(orderline("LineComment")) %>
            </div>
        </td>
    </asp:Panel>
    <%--Delete mark--%>
    <asp:Panel ID="pnlDeleteMark" runat="server" Visible="false">
        <%If Not IsEmail() Then%>
        <td class="CartLineCaption_Delete">
            <% =HTMLEncode(Resources.Language.LABEL_DELETE_ITEM_FROM_ORDER_PAD)%>
        </td>
        <%End If%>
        <td class="CartLineField_Delete" style="padding-left: 10px; vertical-align: top;">
            <div class="CartLineField_DeleteImage">
                <a href="<% = deletelink%>">
                    <img class="CartLineField_DeleteImage" border="0" src="<% = VRoot & "/images/p.gif"%>"
                        alt="<% = Resources.Language.LABEL_DELETE_ITEM_FROM_ORDER_PAD%>" />
                </a>
            </div>
            <div class="CartLineField_DeleteText">
                <a href="<% = deletelink%>">
                    <% =Resources.Language.LABEL_DELETE_ITEM_FROM_ORDER_PAD%>
                </a>
            </div>
        </td>
    </asp:Panel>
</tr>
<%--AM2010061801 - KITTING - START--%>
<asp:Panel ID="pnlKitting" runat="server" Visible="False">
    <tr>
        <td>
        </td>
        <td>
        </td>
        <td colspan="<%= colspan - 2 %>" style="padding: 0px;">
            <%Dim myevent As String = "javascript:animatedcollapse.toggle('" & CStrEx(OrderLine("KitBOMNo")) & "')"%>
            <div class="CartLineField_KittingText" style="vertical-align: middle;">
                <a onclick="<%= myevent %>" style="cursor: pointer; padding-left: 8px;">
                    <%=Resources.Language.SHOW_KIT_COMPONENTS%>
                </a>
            </div>
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td colspan="<%= colspan - 1 %>">
            <div id="<%= CStrEx(OrderLine("KitBOMNo")) %>" class="CartLineField_KittingText_Inside"
                style="display: none;">
                <uc1:KittingList ID="KittingList1" runat="server" />
            </div>
        </td>
    </tr>
    <%--AM2010092201 - ENTERPRISE CSR - Start--%>
    <%--Removing extra scripts--%>
    <%--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
    <script type="text/javascript" src="<%= Vroot %>/script/Kitting/animatedcollapse.js">
    </script>
    <script type="text/javascript">
      $.noConflict();
      // Code that uses other library's $ can follow here.
    </script>--%>
    <%--AM2010092201 - ENTERPRISE CSR - End--%>
</asp:Panel>
<%--AM2010061801 - KITTING - END--%>
