<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CartRenderAddresses.ascx.vb"
    Inherits="CartRenderAddresses" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Import Namespace="System.Collections.Generic" %>
<table cellpadding="0" cellspacing="0" border="0" class='<% = "Order_" & prefix & "Address"%>'>
    <tr class='<% = "Order_" & prefix & "AddressHeader"%>'>
        <td class='<% = "Order_" & prefix & "AddressHeader"%>'>
            <% = header%>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Panel ID="pnlCompany" runat="server" Visible="false">
                <% = CStrEx(OrderDict(prefix & "CompanyName")) %>
                <br />
            </asp:Panel>
            <asp:Panel ID="pnlContactName" runat="server" Visible="false">
                <% = CStrEx(OrderDict(prefix & "ContactName")) %>
                <br />
            </asp:Panel>
            <asp:Panel ID="pnlAddress1" runat="server" Visible="false">
                <% =CStrEx(OrderDict(Prefix & "Address1"))%>
                <br />
            </asp:Panel>
            <asp:Panel ID="pnlAddress2" runat="server" Visible="false">
                <% =CStrEx(OrderDict(Prefix & "Address2"))%>
                <br />
            </asp:Panel>
            <asp:Panel ID="pnlCityStateZip" runat="server" Visible="false">
                <% =CStrEx(OrderDict(Prefix & "CityName")) & ", " & CStrEx(OrderDict(Prefix & "StateName")) & " " & CStrEx(OrderDict(Prefix & "ZipCode"))%>
                <br />
            </asp:Panel>
            <asp:Panel ID="pnlCityZip" runat="server" Visible="false">
                <% =CStrEx(OrderDict(Prefix & "CityName")) & " " & CStrEx(OrderDict(Prefix & "ZipCode"))%>
                <br />
            </asp:Panel>
            <asp:Panel ID="pnlStateZip" runat="server" Visible="false">
                <% =CStrEx(OrderDict(Prefix & "StateName")) & " " & CStrEx(OrderDict(Prefix & "ZipCode"))%>
                <br />
            </asp:Panel>
            <asp:Panel ID="pnlCityState" runat="server" Visible="false">
                <% =CStrEx(OrderDict(Prefix & "CityName")) & ", " & CStrEx(OrderDict(Prefix & "StateName"))%>
                <br />
            </asp:Panel>
            <asp:Panel ID="pnlZip" runat="server" Visible="false">
                <% =CStrEx(OrderDict(Prefix & "ZipCode")) & " " %>
                <br />
            </asp:Panel>
            <asp:Panel ID="pnlCity" runat="Server" Visible="false">
                <% =CStrEx(OrderDict(Prefix & "CityName"))%>
                <br />
            </asp:Panel>
            <asp:Panel ID="pnlState" runat="server" Visible="false">
                <% =CStrEx(OrderDict(Prefix & "StateName"))%>
                <br />
            </asp:Panel>
            <asp:Panel ID="pnlZipCity" runat="server" Visible="false">
                <% =CStrEx(OrderDict(Prefix & "ZipCode")) & " " & CStrEx(OrderDict(Prefix & "CityName"))%>
                <br />
            </asp:Panel>
            <asp:Panel ID="pnlCountryName" runat="server" Visible="false">
                <% =GetCountryName(CStrEx(OrderDict(Prefix & "CountryGuid")))%>
                <br />
            </asp:Panel>
            <asp:Panel ID="pnlEmailAddress" runat="server" Visible="false">
                <% =CStrEx(OrderDict(Prefix & "EmailAddress"))%>
                <br />
            </asp:Panel>
        </td>
    </tr>
</table>
