<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CartRenderQuickCart.ascx.vb"
    Inherits="CartRenderQuickCart" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Import Namespace="System.Collections.Generic" %>
<p>
    <asp:Label ID="Label1" runat="server" Style="font-weight: bold;" Text="<%$ Resources: Language, QUICK_CART %>"></asp:Label>:
    <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources: Language, MESSAGE_USE_THE_INPUT_TO_ENTER_PRODUCTS %>"></asp:Literal>
</p>
<table class="CartProductEntry" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <table cellpadding="0" cellspacing="0" style="margin: 10px;">
                <tr>
                    <th class="THL">
                        <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources: Language, LABEL_PRODUCT %>"></asp:Literal>
                    </th>
                    <th class="THL">
                        <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources: Language, LABEL_QUANTITY %>"></asp:Literal>
                    </th>
                </tr>
                <tr class="TR1">
                    <td class="TDL">
                        <input class="cartProduct" type="text" name="SKU" id="InpSku" size="10" value="" />
                    </td>
                    <td class="TDR">
                        <input class="cartQuantity" type="text" name="Quantity" size="4" value="1" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
