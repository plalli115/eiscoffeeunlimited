<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CartRenderAdditionalInfo.ascx.vb"
    Inherits="CartRenderAdditionalInfo" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%--JA2010102801 - PAPERCHECK - Start--%>
<%@ Register Src="~/controls/USOrderRender/PaperChecks.ascx" TagName="PaperChecks"
    TagPrefix="uc1" %>
<%--JA2010102801 - PAPERCHECK - End--%>
<asp:Panel ID="pnlAdditionalInfoEdit" runat="server" Visible="false">

    <%--WGS 10-14-2013 Your Name and Your Email Validation - BEGIN --%>
    <asp:CustomValidator runat="server" id="vld" EnableClientScript="true" ValidateEmptyText="true" ClientValidationFunction="customValidate"/>
    <%--WGS 10-14-2013 Your Name and Your Email Validation - END --%>   
    
    <%--AM2010092201 - ENTERPRISE CSR - Start--%>

    <script src="<%=Vroot %>/script/calendar/calendar_us.js" language="JavaScript"></script>

    <link href="<%=Vroot %>/script/calendar/calendar.css" rel="stylesheet" />



    <%--AM2010092201 - ENTERPRISE CSR - End--%>
    <table class="CartAdditionalInformation" cellpadding="0" cellspacing="0">
        <tr>
            <td class="AdicionalInfoStyle" style="vertical-align: middle;">
                <table style="margin-left: 10px;">
                    <tr>
                        <th class="THC" colspan="2">
                            <% =Resources.Language.LABEL_ADDTIONAL_INFO%>
                        </th>
                    </tr>
                </table>
                <table style="margin: 10px;">
                
                
                    <asp:Panel ID="pnlYourNameEdit" runat="server" Visible="True">
                        <tr class="TR1">
                            <td class="TDL">
                                <% =Resources.Language.LABEL_INPUT_YOUR_NAME%>
                            </td>
                            <td class="TDL">
                                 <%--WGS 10-14-2013 Your Name, added data-required=true for validation with parsley--%>
                                <input class="cartOptional" data-required="true" type="text" name="YourName" id="YourName" maxlength="30" size="30" style="width:240px;"
                                    value="<% = HTMLEncode(globals.OrderDict("YourName")) %>"/>
                                <input type="hidden" name="YourName_prev" value="<% = HTMLEncode(globals.OrderDict("YourName")) %>" />
                            </td>
                        </tr>
                    </asp:Panel>
                
                    <%-- WLB 10/10/2012 - BEGIN - Add "Your E-mail" to Additional Information --%>
                    <asp:Panel ID="pnlYourEmailEdit" runat="server" Visible="True">
                        <tr class="TR1">
                            <td class="TDL">
                                Your E-mail:
                                <%--<% =Resources.Language.LABEL_INPUT_YOUR_NAME%>--%>
                            </td>
                            <td class="TDL">      
                                <%--WGS 10-14-2013 Your Email, added data-required=true for validation with parsley--%>                     
                                 <%-- WGS - 10/17/2013 - Maxlength=30 -> 80 --%>
                                <input class="cartOptional" data-type="email" data-required="true" type="text" name="YourEmail" maxlength="80" size="30" style="width:240px;"
                                    value="<% = HTMLEncode(globals.OrderDict("YourEmail")) %>" />
                                <input type="hidden" name="YourEmail_prev" value="<% = HTMLEncode(globals.OrderDict("YourEmail")) %>" />
                            </td>
                        </tr>
                    </asp:Panel>
                    
                    <asp:Panel ID="pnlPONumberEdit" runat="server" Visible="false">
                        <tr class="TR1">
                            <td class="TDL">
                                <% =Resources.Language.LABEL_INPUT_PONUMBER%>
                            </td>
                            <td class="TDL">
                                <input class="cartOptional" type="text" name="CustomerPONumber" maxlength="30" size="30" style="width:240px;"
                                    value="<% = HTMLEncode(globals.OrderDict("CustomerPONumber")) %>" />
                                <input type="hidden" name="CustomerPONumber_prev" value="<% = HTMLEncode(globals.OrderDict("CustomerPONumber")) %>" />
                            </td>
                        </tr>
                    </asp:Panel>
                    
                    
                    
                    <asp:Panel ID="pnlCommentEdit" runat="server" Visible="false">
                        <tr class="TR1">
                            <td class="TDL" style="vertical-align:top;">
                                <% =Resources.Language.LABEL_INPUT_COMMENT%>
                            </td>
                            <td class="TDL">
                                <%--<input class="cartOptional" type="text" name="HeaderComment" size="30" maxlength="250"
                                    value="<% = HTMLEncode(globals.OrderDict("HeaderComment")) %>" />--%>
                                <textarea id="HeaderComment" name="HeaderComment" maxlength="250" class="cartOptional" style="width:240px;" cols="20" rows="7"><% = HTMLEncode(globals.OrderDict("HeaderComment")) %></textarea>   
                                <input type="hidden" name="HeaderComment_prev" value="<% = HTMLEncode(globals.OrderDict("HeaderComment")) %>" />
                            </td>
                        </tr>
                    </asp:Panel>
                    
                    
                    
                    
                    
                    <%--AM2010092201 - ENTERPRISE CSR - Start--%>
                    <asp:Panel ID="pnlHoldOrderDateEdit" runat="server" Visible="false">
                        <tr class="TR1">
                            <td class="TDL">
                                <% =
                                    Resources.Language.LABEL_INPUT_HOLD_ORDER_DATE%>
                            </td>
                            <td class="TDL">
                                <%Dim holdDate As String%>
                                <%If CStrEx(globals.OrderDict("HoldOrderDate")) = "" Then%>
                                <%holdDate = ""%>
                                <%Else%>
                                <%holdDate = CDateEx(globals.OrderDict("HoldOrderDate")).ToString("MM/dd/yyyy")%>
                                <%End If%>
                                <input class="cartOptional" type="text" name="HoldOrderDate" id="HoldOrderDate" maxlength="30"
                                    size="30" value="<% = holdDate %>" />
                                <input type="hidden" name="HoldOrderDate_prev" value="<% = holdDate %>" />
                                <%If CBoolEx(globals.OrderDict("RecurringOrder")) And CStrEx(globals.OrderDict("ModifiedRecurrency")) = "" Then%>
                                <img alt="Open Calendar" class="tcalIcon" onclick="A_TCALS['<%= CIntEx(globals.OrderDict("Lines").count) %>'].f_toggle()"
                                    id="tcalico_<%= CIntEx(globals.OrderDict("Lines").count) %>" src="<%=Vroot %>/script/calendar/img/cal.gif"
                                    title="Open Calendar" />
                                <%ElseIf CBoolEx(globals.OrderDict("RecurringOrder")) And CStrEx(globals.OrderDict("ModifiedRecurrency")) <> "" Then%>
                                <img alt="Open Calendar" class="tcalIcon" onclick="A_TCALS['<%= CIntEx(globals.OrderDict("Lines").count) * 2 %>'].f_toggle()"
                                    id="tcalico_<%= CIntEx(globals.OrderDict("Lines").count) * 2 %>" src="<%=Vroot %>/script/calendar/img/cal.gif"
                                    title="Open Calendar" />
                                <%Else%>
                                <img alt="Open Calendar" class="tcalIcon" onclick="A_TCALS['0'].f_toggle()" id="tcalico_0"
                                    src="<%=Vroot %>/script/calendar/img/cal.gif" title="Open Calendar" />
                                <%End If%>
                                <input id="vroot" type="hidden" value="<%=Vroot %>" />

                                <script type="text/javascript" language="javascript">
                                    // whole calendar template can be redefined per individual calendar
                                    
                                    var path=document.getElementById('vroot').value
                                     var A_CALTPL = {
                                     'months' : ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                                     'weekdays' : ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                                     'yearscroll': true,
                                     'weekstart': 0,
                                     'centyear' : 70,
                                     'imgpath' : path + '/script/calendar/img/'
                                     }
                                                                                   
                                    new tcal ({
                                     // if referenced by ID then form name is not required
                                     'controlname': 'HoldOrderDate'
                                     }, A_CALTPL);  
                                </script>

                            </td>
                        </tr>
                    </asp:Panel>
                    <%--AM2010092201 - ENTERPRISE CSR - End--%>
                </table>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel ID="pnlAdditionalInfoNoEdit" runat="server" Visible="false">
    <asp:Panel runat="server" ID="pnlOrderNumber" Visible="false">
        <table cellpadding="0" cellspacing="0" border="0" class="Order_PaymentInfo">
            <tr class="Order_PONumber">
                <td class="Order_PaymentTypeCaption">
                    <asp:Label ID="Label1" runat="server" Text="<%$Resources: Language,LABEL_ORDER_NUMBER %>">
                    </asp:Label>
                </td>
                <td class="Order_PaymentTypeValue">
                    <asp:Label ID="lblCustomerReference" runat="server">
                    </asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <%--AM2010092201 - ENTERPRISE CSR - Start--%>
    <asp:Panel runat="server" ID="pnlHoldOrderDateNoEdit" Visible="false">
        <table cellpadding="0" cellspacing="0" border="0" class="Order_PaymentInfo">
            <tr class="Order_PONumber">
                <td class="Order_PaymentTypeCaption">
                    <asp:Label ID="Label9" runat="server" Text="<%$Resources: Language,LABEL_INPUT_HOLD_ORDER_DATE %>">
                    </asp:Label>
                </td>
                <td class="Order_PaymentTypeValue">
                    <asp:Label ID="lblHoldOrderDate" runat="server">
                    </asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <%--AM2010092201 - ENTERPRISE CSR - End--%>
    <asp:Panel ID="pnlDate" runat="server" Visible="false">
        <table cellpadding="0" cellspacing="0" border="0" class="Order_Date">
            <tr class="Order_Date">
                <td class="Order_DateCaption">
                    <asp:Label ID="Label2" runat="server" Text="<%$Resources: Language,LABEL_DATE %>">
                    </asp:Label>
                </td>
                <td class="Order_DateValue">
                    <asp:Label ID="lblOrderDate" runat="server">
                    </asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlPaymentInfo" runat="server" Visible="false">
        <table cellpadding="0" cellspacing="0" border="0" class="Order_PaymentInfo">
            <asp:Panel ID="pnlPaymentType" runat="server" Visible="false">
                <tr>
                    <td class="Order_PaymentTypeCaption">
                        <asp:Label ID="Label3" runat="server" Text="<%$Resources: Language,LABEL_PAYMENT_INFORMATION %>">
                        </asp:Label>
                    </td>
                    <td class="Order_PaymentTypeValue">
                        <asp:Label ID="lblPaymentTypeValue" runat="server">
                        </asp:Label>
                    </td>
                </tr>
            </asp:Panel>
            <%--AM2010092201 - ENTERPRISE CSR - Start--%>
            <%--AM2010041301 - ENTERPRISE PAYMENT GATEWAY - Start--%>
            <asp:Panel ID="pnlPaymentTypeCreditCard" runat="server" Visible="false">
                <tr>
                    <td class="Order_PaymentTypeCaption">
                        <asp:Label ID="Label12" runat="server" Text="<%$Resources: Language,LABEL_EEPG_LAST_DIGITS %>">
                        </asp:Label>
                    </td>
                    <td class="Order_PaymentTypeValue">
                        <asp:Label ID="lblLastDigits" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="Order_PaymentTypeCaption">
                        <asp:Label ID="Label13" runat="server" Text="<%$Resources: Language,LABEL_EEPG_CARD_TYPE %>">
                        </asp:Label>
                    </td>
                    <td class="Order_PaymentTypeValue">
                        <asp:Label ID="lblCardType" runat="server">
                        </asp:Label>
                    </td>
                </tr>
            </asp:Panel>
            <%--AM2010041301 - ENTERPRISE PAYMENT GATEWAY - End--%>
            <%--JA2010102801 - PAPERCHECK - Start--%>
            <asp:Panel ID="pnlPaperChecks" runat="server" Visible="false">
                <tr>
                    <td colspan="2" style="width:100%;">
                        <uc1:PaperChecks ID="PaperChecks1" runat="server" />
                    </td>
                </tr>
            </asp:Panel>
            <%--JA2010102801 - PAPERCHECK - End--%>
            <%--AM2010092201 - ENTERPRISE CSR - End--%>
            <asp:Panel ID="pnlPaymentTransactionID" runat="server" Visible="false">
                <tr>
                    <td class="Order_PaymentTransactionCaption">
                        <asp:Label ID="Label4" runat="server" Text="<%$Resources: Language,LABEL_TRANSACTION_ID %>">
                        </asp:Label>
                    </td>
                    <td class="Order_PaymentTransactionValue">
                        <asp:Label ID="lblPaymentTransactionValue" runat="server">
                        </asp:Label>
                    </td>
                </tr>
            </asp:Panel>
            <%--AM2010091001 - ONLINE PAYMENTS - Start--%>
            <asp:Panel ID="pnlOnlinePayments" runat="server" Visible="false">
                <tr>
                    <td class="Order_PaymentTransactionCaption">
                        <asp:Label ID="Label7" runat="server" Text="<%$Resources: Language,ONLINE_PAYMENT_ADDITIONAL_INFO %>">
                        </asp:Label>
                    </td>
                    <%For Each onlinePayment As ExpDictionary In dictOnlinePayments.Values%>
                    <%If firstOnlinePayment Then%>
                    <%firstOnlinePayment = False%>
                    <td class="Order_PaymentTransactionValue">
                        <%Else%>
                </tr>
                <tr>
                    <td class="Order_PaymentTransactionCaption">
                    </td>
                    <td class="Order_PaymentTransactionValue">
                        <%End If%>
                        <% = onlinePaymentLine(onlinePayment)%>
                    </td>
                    <%Next%>
                </tr>
                <tr>
                    <td class="Order_PaymentTransactionCaption">
                        <asp:Label ID="Label8" runat="server" Text="<%$Resources: Language,ONLINE_PAYMENT_ADDITIONAL_INFO_TOTAL %>">
                        </asp:Label>
                    </td>
                    <td class="Order_PaymentTransactionValue">
                        <span>
                            <% =formatAmount(onlinePaymentsTotal)%>
                        </span>
                    </td>
                </tr>
            </asp:Panel>
            <%--AM2010091001 - ONLINE PAYMENTS - End--%>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlCommentPONumber" runat="server" Visible="false">
        <table cellpadding="0" cellspacing="0" border="0" class="Order_AdditionalInfo">
        
            <asp:Panel ID="pnlYourName" runat="server" Visible="false">
                <tr class="Order_PONumber">
                    <td class="Order_PONumberCaption">
                        <asp:Label ID="Label11" runat="server" Text="<%$Resources: Language,LABEL_INPUT_YOUR_NAME %>">
                        </asp:Label>
                    </td>
                    <td class="Order_PONumberValue">
                        <asp:Label ID="lblYourName" runat="server">
                        </asp:Label>
                    </td>
                </tr>
            </asp:Panel>
            <%-- WLB 10/12/2012 - BEGIN - Add "Your E-mail" to Additional Information --%>
            <asp:Panel ID="pnlYourEmail" runat="server" Visible="false">
                <tr class="Order_PONumber">
                    <td class="Order_PONumberCaption">
                        <asp:Label ID="Label15" runat="server" Text="Your Email:">
                        </asp:Label>
                    </td>
                    <td class="Order_PONumberValue">
                        <asp:Label ID="lblYourEmail" runat="server">
                        </asp:Label>
                    </td>
                </tr>
            </asp:Panel>
            <%-- WLB 10/12/2012 - BEGIN - Add "Your E-mail" to Additional Information --%>
            <asp:Panel ID="pnlPONumber" runat="server" Visible="false">
                <tr class="Order_PONumber">
                    <td class="Order_PONumberCaption">
                        <asp:Label ID="Label6" runat="server" Text="<%$Resources: Language,LABEL_INPUT_PONUMBER %>">
                        </asp:Label>
                    </td>
                    <td class="Order_PONumberValue">
                        <asp:Label ID="lblPONumber" runat="server">
                        </asp:Label>
                    </td>
                </tr>
            </asp:Panel>
            
            <asp:Panel ID="pnlComment" runat="server" Visible="false">
                <tr class="Order_Comment">
                    <td class="Order_CommentCaption">
                        <asp:Label ID="Label5" runat="server" Text="<%$Resources: Language,LABEL_INPUT_COMMENT %>">
                        </asp:Label>
                    </td>
                    <td class="Order_CommentValue">
                        <asp:Label ID="lblComment" runat="server">
                        </asp:Label>
                    </td>
                </tr>
            </asp:Panel>
            
        </table>
    </asp:Panel>
    <%--AM2010092201 - ENTERPRISE CSR - Start--%>
    <asp:Panel ID="pnlAdjustmentNoEdit" runat="server" Visible="false">
        <table cellpadding="0" cellspacing="0" border="0" class="Order_PaymentInfo">
            <tr class="Order_PONumber">
                <td class="Order_PaymentTypeCaption">
                    <asp:Label ID="Label10" runat="server" Text="<%$Resources: Language,LABEL_INPUT_ADJUSTMENT_NOTES %>">
                    </asp:Label>
                </td>
                <td class="Order_PaymentTypeValue">
                    <asp:Label ID="lblAdjustmentNote" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <%--AM2010092201 - ENTERPRISE CSR - End--%>
</asp:Panel>
