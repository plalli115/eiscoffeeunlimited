Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Web
Imports ExpandIT.GlobalsClass
Imports System.Collections.Generic

Partial Class CartRenderAddresses
    Inherits ExpandIT.UserControl

    Private m_OrderDict As ExpDictionary
    Private m_Prefix As String
    Private m_Header As String
    Private m_IsEmail As Boolean
    'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
    Private m_IsBrowser As Boolean
    Private m_IsLedger As Boolean
    'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End

    Public Property OrderDict() As ExpDictionary
        Get
            Return m_OrderDict
        End Get
        Set(ByVal value As ExpDictionary)
            m_OrderDict = value
        End Set
    End Property

    Public Property Prefix() As String
        Get
            Return m_Prefix
        End Get
        Set(ByVal value As String)
            m_Prefix = value
        End Set
    End Property

    Public Property Header() As String
        Get
            Return m_Header
        End Get
        Set(ByVal value As String)
            m_Header = value
        End Set
    End Property

    Public Property IsEmail() As Boolean
        Get
            Return CBoolEx(m_IsEmail)
        End Get
        Set(ByVal value As Boolean)
            m_IsEmail = CBoolEx(value)
        End Set
    End Property

    'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
    Public Property IsLedger() As Boolean
        Get
            Return CBoolEx(m_IsLedger)
        End Get
        Set(ByVal value As Boolean)
            m_IsLedger = CBoolEx(value)
        End Set
    End Property

    Public Property IsBrowser() As Boolean
        Get
            Return CBoolEx(m_IsBrowser)
        End Get
        Set(ByVal value As Boolean)
            m_IsBrowser = CBoolEx(value)
        End Set
    End Property
    'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End

    Protected Overloads Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        Dim cityOK As Boolean = False
        Dim stateOK As Boolean = False
        Dim zipOK As Boolean = False

        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
        cityOK = CStrEx(OrderDict(Prefix & "CityName")) <> "" And (AppSettings("ORDERRENDER_BROWSER_SHOW_CITY") And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOW_CITY") And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOW_CITY") And IsLedger)
        stateOK = CStrEx(OrderDict(Prefix & "StateName")) <> "" And (AppSettings("ORDERRENDER_BROWSER_SHOW_STATE") And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOW_STATE") And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOW_STATE") And IsLedger)
        zipOK = CStrEx(OrderDict(Prefix & "ZipCode")) <> "" And (AppSettings("ORDERRENDER_BROWSER_SHOW_ZIPCODE") And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOW_ZIPCODE") And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOW_ZIPCODE") And IsLedger)

        If CStrEx(OrderDict(Prefix & "CompanyName")) <> "" And (AppSettings("ORDERRENDER_BROWSER_SHOW_COMPANY") And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOW_COMPANY") And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOW_COMPANY") And IsLedger) Then
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
            pnlCompany.Visible = True
        End If
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
        If CStrEx(OrderDict(Prefix & "ContactName")) <> "" And (AppSettings("ORDERRENDER_BROWSER_SHOW_CONTACTNAME") And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOW_CONTACTNAME") And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOW_CONTACTNAME") And IsLedger) Then
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
            pnlContactName.Visible = True
        End If
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
        If CStrEx(OrderDict(Prefix & "Address1")) <> "" And (AppSettings("ORDERRENDER_BROWSER_SHOW_ADDRESS1") And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOW_ADDRESS1") And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOW_ADDRESS1") And IsLedger) Then
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
            pnlAddress1.Visible = True
        End If
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
        If CStrEx(OrderDict(Prefix & "Address2")) <> "" And (AppSettings("ORDERRENDER_BROWSER_SHOW_ADDRESS2") And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOW_ADDRESS2") And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOW_ADDRESS2") And IsLedger) Then
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
            pnlAddress2.Visible = True
        End If
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
        If AppSettings("ORDERRENDER_BROWSER_SHOW_LOCATIONUSFORMAT") And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOW_LOCATIONUSFORMAT") And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOW_LOCATIONUSFORMAT") And IsLedger Then
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
            If cityOK Then
                If stateOK Then
                    If zipOK Then
                        pnlCityStateZip.Visible = True
                    Else
                        pnlCityState.Visible = True
                    End If
                Else
                    If zipOK Then
                        pnlCityZip.Visible = True
                    Else
                        pnlCity.Visible = True
                    End If
                End If
            ElseIf stateOK Then
                If zipOK Then
                    pnlStateZip.Visible = True
                Else
                    pnlState.Visible = True
                End If
            ElseIf zipOK Then
                pnlZip.Visible = True
            End If
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
        ElseIf Not CBoolEx(AppSettings("ORDERRENDER_BROWSER_SHOW_LOCATIONUSFORMAT")) And IsBrowser Or Not CBoolEx(AppSettings("ORDERRENDER_EMAIL_SHOW_LOCATIONUSFORMAT")) And IsEmail Or Not CBoolEx(AppSettings("ORDERRENDER_LEDGER_SHOW_LOCATIONUSFORMAT")) And IsLedger Then
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
            If zipOK Then
                If cityOK Then
                    pnlZipCity.Visible = True
                Else
                    pnlZip.Visible = True
                End If
            ElseIf cityOK Then
                pnlCity.Visible = True
            End If
        End If
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
        If CStrEx(OrderDict(Prefix & "CountryGuid")) <> "" And (AppSettings("ORDERRENDER_BROWSER_SHOW_COUNTRY") And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOW_COUNTRY") And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOW_COUNTRY") And IsLedger) Then
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
            pnlCountryName.Visible = True
        End If
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
        If CStrEx(OrderDict(Prefix & "EmailAddress")) <> "" And (AppSettings("ORDERRENDER_BROWSER_SHOW_EMAIL") And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOW_EMAIL") And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOW_EMAIL") And IsLedger) Then
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
            pnlEmailAddress.Visible = True
        End If
    End Sub

    ''' <summary>
    ''' Get the name of a country by specifying the country code.
    ''' </summary>
    ''' <param name="CountryGuid">A code representing a country.</param>
    ''' <returns></returns>
    ''' <remarks>This function is the reverse of GetCountryGuid.</remarks>
    Public Shared Function GetCountryName(ByVal CountryGuid As String) As String
        Dim names As ExpDictionary = Nothing
        Dim guids As ExpDictionary = Nothing

        GetCountries(guids, names)
        Return CStrEx(names(CountryGuid))
    End Function

End Class
