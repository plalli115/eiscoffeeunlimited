<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CartRender.ascx.vb" Inherits="CartRender" %>
<%@ Register Src="~/controls/AddToCartButton.ascx" TagName="AddToCartButton" TagPrefix="uc1" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%--AM2010031501 - ORDER RENDER - Start--%>
<%@ Register Src="~/controls/USOrderRender/CartRenderAddresses.ascx" TagName="CartRenderAddresses"
    TagPrefix="uc1" %>
<%@ Register Src="~/controls/USOrderRender/CartRenderLine.ascx" TagName="CartRenderLine"
    TagPrefix="uc1" %>
<%@ Register Src="~/controls/USOrderRender/CartRenderTotals.ascx" TagName="CartRenderTotals"
    TagPrefix="uc1" %>
<%@ Register Src="~/controls/USOrderRender/CartRenderAdditionalInfo.ascx" TagName="CartRenderAdditionalInfo"
    TagPrefix="uc1" %>
<%@ Register Src="~/controls/USOrderRender/CartRenderQuickCart.ascx" TagName="CartRenderQuickCart"
    TagPrefix="uc1" %>
<%--AM2010031501 - ORDER RENDER - End--%>
<%--*****shipping*****-start --%>
<%@ Register Src="~/controls/shipping/parts/USShipping/ShippingProviders.ascx" TagName="ShippingProviders"
    TagPrefix="uc1" %>
<%--*****shipping*****-end --%>
<%--*****promotions*****-start--%>
<%@ Register Src="~/controls/USPromoCodes/PromoBox.ascx" TagName="PromoBox" TagPrefix="uc1" %>
<%--*****promotions*****-end--%>
<%--JA2010092201 - RECURRING ORDERS - START--%>
<%@ Register Src="~/controls/USRecurringOrders/RecurringSchedule.ascx" TagName="RecurringSchedule"
    TagPrefix="uc1" %>
<%--JA2010092201 - RECURRING ORDERS - END--%>

<div class='<% = IIF(IsEmail,"Email","Browser") %>'>
    <table cellpadding="0" cellspacing="0" border="0" class="Order">
        <tr class="Order">
            <td class="Order">
                <asp:Panel ID="pnlAddresses" runat="server" Visible="false">
                    <table cellpadding="0" cellspacing="0" border="0" class="Order_Header">
                        <tr class="Order_Header">
                            <asp:Panel ID="pnlBillingAddress" runat="server" Visible="false">
                                <td class="OrderHeader_Address">
                                    <uc1:CartRenderAddresses ID="CartRenderAddressesBillTo" runat="server" />
                                </td>
                            </asp:Panel>
                            <asp:Panel ID="pnlShippingAddress" runat="server" Visible="false">
                                <td class="OrderHeader_ShipToAddress">
                                    <uc1:CartRenderAddresses ID="CartRenderAddressesShipTo" runat="server" />
                                </td>
                            </asp:Panel>
                        </tr>
                        <tr class="Order_Header" style="height: 20px;">
                            <td>
                                &nbsp;</td>
                        </tr>
                    </table>
                </asp:Panel>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <asp:Panel ID="pnlAdditionalInfoNoEditMode" runat="server" Visible="false">
                            <td style="width: 50%;">
                                <uc1:CartRenderAdditionalInfo ID="CartRenderAdditionalInfoNoEdit" runat="server"
                                    EditMode="false" />
                            </td>
                        </asp:Panel>
                        <%--*****promotions*****-start--%>
                        <asp:Panel ID="pnlPromotionsNoEditMode" runat="server" Visible="false">
                            <td style="padding-left: 20px; width: 50%;">
                                <uc1:PromoBox ID="PromoBoxNoEdit" runat="server" EditMode="false" />
                            </td>
                        </asp:Panel>
                        <%--*****promotions*****-end--%>
                    </tr>
                </table>
                <%--LINES--%>
                <asp:Panel ID="pnlCartRender" runat="server">
                    <br />
                    <br />
                    <asp:DataList ID="dlCartRender" runat="server" RepeatColumns="1" RepeatDirection="Horizontal"
                        CssClass="OrderLines">
                        <HeaderTemplate>
                            <tr class="CartLineHeaders">
                                <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start--%>
                                <asp:Panel ID="pnlThumbnailHeader" runat="server" Visible='<%# AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_THUMBNAIL") = "1" And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_THUMBNAIL") = "1" And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOWCOLUMN_THUMBNAIL") = "1" And IsLedger %>'>
                                    <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End--%>
                                    <th class="CartLineHeader_Thumbnail">
                                    </th>
                                </asp:Panel>
                                <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start--%>
                                <asp:Panel ID="pnlProductGuidHeader" runat="server" Visible='<%# AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_PRODUCTGUID") = "1" And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_PRODUCTGUID") = "1" And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOWCOLUMN_PRODUCTGUID") = "1" And IsLedger %>'>
                                    <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End--%>
                                    <th class="CartLineHeader_ProductGuid">
                                        <% = Resources.Language.LABEL_PRODUCT %>
                                    </th>
                                </asp:Panel>
                                <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start--%>
                                <asp:Panel ID="pnlNameHeader" runat="server" Visible='<%# (AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_NAME") = "1" Or AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_NAMEANDVARIANT") = "1") And isBrowser Or (AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_NAME") = "1" Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_NAMEANDVARIANT") = "1") And isEmail Or (AppSettings("ORDERRENDER_LEDGER_SHOWCOLUMN_NAME") = "1" Or AppSettings("ORDERRENDER_LEDGER_SHOWCOLUMN_NAMEANDVARIANT") = "1") And isLedger %>'>
                                    <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End--%>
                                    <th class="CartLineHeader_Name">
                                        <% = Resources.Language.LABEL_NAME %>
                                    </th>
                                </asp:Panel>
                                <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start--%>
                                <asp:Panel ID="pnlQuantityHeader" runat="server" Visible='<%# AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_QUANTITY") = "1" And isBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_QUANTITY") = "1" And isEmail Or AppSettings("ORDERRENDER_LEDGER_SHOWCOLUMN_QUANTITY") = "1" And isLedger %>'>
                                    <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End--%>
                                    <th class="CartLineHeader_Quantity">
                                        <% = Resources.Language.LABEL_QUANTITY %>
                                    </th>
                                </asp:Panel>
                                <%--AM0122201001 - UOM - Start--%>
                                <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start--%>
                                <asp:Panel ID="pnlUOM" runat="server" Visible='<%# AppSettings("EXPANDIT_US_USE_UOM") = "1" And (AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_UOM") = "1" And isBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_UOM") = "1" And isEmail Or AppSettings("ORDERRENDER_LEDGER_SHOWCOLUMN_UOM") = "1" And isLedger) %>'>
                                    <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End--%>
                                    <th class="CartLineHeader_Price">
                                        <% =Resources.Language.LABEL_UOM%>
                                    </th>
                                </asp:Panel>
                                <%--AM0122201001 - UOM - End--%>
                                <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start--%>
                                <asp:Panel ID="pnlPriceHeader" runat="server" Visible='<%# AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_PRICE") = "1" And isBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_PRICE") = "1" And isEmail Or AppSettings("ORDERRENDER_LEDGER_SHOWCOLUMN_PRICE") = "1" And isLedger %>'>
                                    <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End--%>
                                    <th class="CartLineHeader_Price">
                                        <%If eis.CheckPageAccess("Prices") Then %>
                                        <% =Resources.Language.LABEL_PRICE%>
                                        <%End If %>
                                    </th>
                                </asp:Panel>
                                <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start--%>
                                <asp:Panel ID="pnlDiscountAmountHeader" runat="server" Visible='<%# AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_DISCOUNTAMOUNT") = "1" And isBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_DISCOUNTAMOUNT") = "1" And isEmail Or AppSettings("ORDERRENDER_LEDGER_SHOWCOLUMN_DISCOUNTAMOUNT") = "1" And isLedger %>'>
                                    <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End--%>
                                    <th class="CartLineHeader_DiscountAmount">
                                        <%If eis.CheckPageAccess("Prices") Then %>
                                        <% =Resources.Language.LABEL_DISC_AMOUNT%>                                        
                                        <%End If %>
                                    </th>
                                </asp:Panel>
                                <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start--%>
                                <asp:Panel ID="pnlDiscountPctHeader" runat="server" Visible='<%# AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_DISCOUNTPCT") = "1" And isBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_DISCOUNTPCT") = "1" And isEmail Or AppSettings("ORDERRENDER_LEDGER_SHOWCOLUMN_DISCOUNTPCT") = "1" And isLedger %>'>
                                    <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End--%>
                                    <th class="CartLineHeader_DiscountPct">
                                        <%If eis.CheckPageAccess("Prices") Then %>
                                        <% =Resources.Language.LABEL_DISC_PROCENT%>
                                        <%End If %>
                                    </th>
                                </asp:Panel>
                                <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start--%>
                                <asp:Panel ID="pnlTotalHeader" runat="server" Visible='<%# AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_TOTAL") = "1" And isBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_TOTAL") = "1" And isEmail Or AppSettings("ORDERRENDER_LEDGER_SHOWCOLUMN_TOTAL") = "1" And isLedger %>'>
                                    <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End--%>
                                    <th class="CartLineHeader_Total">
                                        <%If eis.CheckPageAccess("Prices") Then %>
                                        <% =Resources.Language.LABEL_LINE_TOTAL%>
                                        <%End If%>
                                    </th>
                                </asp:Panel>
                                <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start--%>
                                <asp:Panel ID="pnlCommentHeader" runat="server" Visible='<%# AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_COMMENT") = "1" And isBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_COMMENT") = "1" And isEmail Or AppSettings("ORDERRENDER_LEDGER_SHOWCOLUMN_COMMENT") = "1" And isLedger %>'>
                                    <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End--%>
                                    <th class="CartLineHeader_Comment">
                                        <% =Resources.Language.LABEL_LINE_COMMENT %>
                                    </th>
                                </asp:Panel>
                            </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <!-- AM2010031501 - ORDER RENDER - Start -->
                            <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start--%>
                            <uc1:CartRenderLine ID="CartRenderLine1" runat="server" OrderLine='<%# Eval("Value") %>'
                                EditMode="<%# EditMode() %>" IsEmail="<%# IsEmail() %>" IsBrowser="<%# IsBrowser() %>"
                                IsActiveOrder="<%# IsActiveOrder() %>" IsLedger="<%# IsLedger() %>" isReturn="<%# isReturn() %>"
                                DefaultButton="<%# DefaultButton() %>" />
                            <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End--%>
                            <!-- AM2010031501 - ORDER RENDER - End -->
                        </ItemTemplate>
                    </asp:DataList>
                </asp:Panel>
                <asp:Panel ID="pnlOrderTotals" runat="server">
                    <table cellpadding="0" cellspacing="0" border="0" class="OrderTotals">
                        <tr>
                            <td align="left">
                                <%--*****shipping*****-start --%>
                                <asp:Panel ID="pnlShippingProviderSelector" runat="server" Visible="false">
                                    <uc1:ShippingProviders ID="ShippingProviders1" runat="server" />
                                </asp:Panel>
                                <%--*****shipping*****-end --%>
                            </td>
                            <td align="right">
                                <uc1:CartRenderTotals ID="CartRenderTotals1" runat="server" />
                            </td>
                            <%If EditMode Then%>
                            <td style='<% = IIF(CStrEx(AppSettings("MasterPageFolder")).Contains("Enterprise"),"width: 20px;","width: 39px;") %>'>
                            </td>
                            <%End If%>
                        </tr>
                    </table>
                    <br />
                </asp:Panel>
                <%--AM2010092201 - ENTERPRISE CSR - Start--%>
                <asp:Panel ID="pnlAdjustmentEdit" runat="server" Visible="false">
                    <table>
                        <tr class="TR1">
                            <td class="TDL" colspan="2">
                                <% =Resources.Language.LABEL_INPUT_ADJUSTMENT_AMOUNT%>
                            </td>
                            <td class="TDL">
                                <input class="cartOptional" type="text" name="AdjustmentAmount" maxlength="30" size="5"
                                    value="<% = HTMLEncode(CDblEx(OrderDict("AdjustmentAmount"))) %>" />
                                <input type="hidden" name="AdjustmentAmount_prev" value="<% = HTMLEncode(CDblEx(OrderDict("AdjustmentAmount"))) %>" />
                            </td>
                        </tr>
                        <tr class="TR1">
                            <td class="TDL" colspan="2">
                                <% =Resources.Language.LABEL_INPUT_ADJUSTMENT_NOTES%>
                            </td>
                            <td class="TDL">
                                <input class="cartOptional" type="text" name="AdjustmentNote" size="30" value="<% = HTMLEncode(CStrEx(OrderDict("AdjustmentNote"))) %>" />
                                <input type="hidden" name="AdjustmentNote_prev" value="<% = HTMLEncode(CStrEx(OrderDict("AdjustmentNote"))) %>" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <%--AM2010092201 - ENTERPRISE CSR - End--%>
                <%--JA2010092201 - RECURRING ORDERS - START--%>
                <%If CBoolEx(AppSettings("RECURRING_ORDERS_SHOW")) And CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And csrObj.getSalesPersonGuid() <> "" Then%>
               
                <table style="width: 100%;">
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="vertical-align: middle;">
                                        <asp:CheckBox ID="CbxRecurring" onclick="OpenLoading();" AutoPostBack="true" runat="server" />
                                    </td>
                                    <td style="vertical-align: middle; height: 68px;">
                                        <asp:Label ID="lblRecurring" runat="server" Text="<%$ Resources: Language, LABEL_RECURRING_ORDER %>">
                                        </asp:Label>
                                                <div id="LoadingRecurrency" style="background-color: White; background-image: url('<%= Vroot %>/script/stars/loadRating.gif');
                                            background-repeat: no-repeat; background-position: center bottom; text-align: center;
                                            position: relative; display: none; top: -11; width: 500px; height: 55px;" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                                    <uc1:RecurringSchedule IsEmail="<%# IsEmail() %>" ID="RecurringSchedule1" runat="server" />
                        </td>
                    </tr>
                </table>
                    

                <script type="text/javascript">
                    function OpenLoading(){ 
                        //Loading wait
                        var divLoading=document.getElementById('LoadingRecurrency');
                        divLoading.style.top='-15px';
                        //divLoading.style.left='22px';
                        divLoading.style.width='500px';
                        divLoading.style.height='55px';
                        divLoading.style.display='block';
                        //Loading wait
                    }
                    //Loading wait
                    function closeLoading(){ 
                        var divLoading=document.getElementById('LoadingRecurrency');
                        divLoading.style.display='none';
                    }
                    //Loading wait
                </script>

                <br />
                <br />
                <%End If%>
                <%--<%End If%>--%>
                <%--JA2010092201 - RECURRING ORDERS - END--%>
                <asp:Panel ID="pnlAdditonalInformation" runat="server">
                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <asp:Panel ID="pnlAdditionalInfoEditMode" runat="server" Visible="false">
                                <td>
                                    <uc1:CartRenderAdditionalInfo ID="CartRenderAdditionalInfoEdit" runat="server" EditMode="true" />
                                </td>
                            </asp:Panel>
                            <%--*****promotions*****-start--%>
                            <asp:Panel ID="pnlPromotionsEditMode" runat="server" Visible="false">
                                <td align="right">
                                    <uc1:PromoBox ID="PromoBoxEdit" runat="server" EditMode="true" />
                                </td>
                                <td style='<% = IIF(CStrEx(AppSettings("MasterPageFolder")).Contains("Enterprise"),"width: 20px;","width: 39px;") %>'>
                                </td>
                            </asp:Panel>
                            <%--*****promotions*****-end--%>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnlQuickCart" runat="server" Visible="false">
                    <br />
                    <br />
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td>
                                <uc1:CartRenderQuickCart ID="CartRenderQuickCart1" runat="server" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <br />
                <br />
            </td>
        </tr>
    </table>
</div>
<%--AM2010061801 - KITTING - START--%>
<%  If kits Then%>
<%  Response.Write(myscript)%>
<%  End If%>
<%--AM2010061801 - KITTING - END--%>
