<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PaperChecks.ascx.vb"
    Inherits="PaperChecks" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%--JA2010102801 - PAPERCHECK - Start--%>
<asp:Panel ID="pnlPaperChecks" runat="server">
    <table class="Order_PaymentInfo" cellpadding="5" cellspacing="5" style="width: 100%; border:1px solid gray;">
        <tr>
            <%If IsActiveOrder Then %>
                <td></td>
            <%End IF %>
            <td align="center">
                <asp:Label ID="Label21" style="font-weight:bold;" runat="server" Text="<%$Resources: Language,PAPERCHECK_CHECK_DATE %>">
                </asp:Label>
            </td>
            <td align="right">
                <asp:Label ID="Label19" style="font-weight:bold;" runat="server" Text="<%$Resources: Language,PAPERCHECK_CHECK_NUMBER %>">
                </asp:Label>
            </td>
            <td align="right">
                <asp:Label ID="Label20" style="font-weight:bold;" runat="server" Text="<%$Resources: Language,PAPERCHECK_CHECKS_TOTAL %>">
                </asp:Label>
            </td>
            <td align="center">
                <asp:Label ID="Label22" style="font-weight:bold;" runat="server" Text="<%$Resources: Language,PAPERCHECK_BOX_TRACKING %>">
                </asp:Label>
            </td>
            <td align="center">
                <asp:Label ID="Label23" style="font-weight:bold;" runat="server" Text="<%$Resources: Language,LABEL_PAPERCHECK_COMMENT %>">
                </asp:Label>
            </td>
        </tr>
        <%For Each item As ExpDictionary In checks.Values%>
        <tr>
            <%If IsActiveOrder Then %>
                <td align="left">
                    <a href="<% = deleteLink(CStrEx(item("CheckGuid")))%>">
                        <img class="CartLineField_DeleteImage" border="0" src="<% = VRoot & "/images/p.gif"%>"
                            alt="<% = Resources.Language.LABEL_DELETE_ITEM_FROM_ORDER_PAD%>" />
                    </a>
                </td>
            <%End IF %>
            <td align="center">
                <%If Not item("PaperCheckDate") Is DBNull.Value Then%>
                <%=Cdateex(item("PaperCheckDate")).ToString("MM/dd/yyyy")%>
                <%End If %>
            </td>
            <td align="right">
                <%=CStrEx(item("PaperCheckNumber"))%>
            </td>
            <td align="right">
                <%If Not item("PaperCheckTotals") Is DBNull.Value AndAlso CStrEx(item("PaperCheckTotals")) <> "" Then%>
                <%= formatAmount(Cdblex(item("PaperCheckTotals"))) %>
                <%End If %>
            </td>
            <td align="center">
                <%If Not item("PaperBoxTrackDate") Is DBNull.Value AndAlso CStrEx(item("PaperBoxTrack1")) <> "" AndAlso CStrEx(item("PaperBoxTrack2")) <> "" Then%>
                <%= "(" & CStrEx(item("PaperBoxTrack1")) & " \ " & CStrEx(item("PaperBoxTrack2")) & ") " & CDateEx(item("PaperBoxTrackDate")).ToString("MM/dd/yyyy")%>
                <%End If%>
            </td>
            <td align="center">
                <%=CStrEx(item("Comment"))%>
            </td>
        </tr>
        <%Next%>
    </table>
</asp:Panel>
<%--JA2010102801 - PAPERCHECK - End--%>
