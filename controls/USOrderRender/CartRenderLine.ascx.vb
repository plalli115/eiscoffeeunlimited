Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Web
Imports ExpandIT.GlobalsClass
Imports System.Collections.Generic

Partial Class CartRenderLine
    Inherits ExpandIT.UserControl

    Private m_OrderLine As ExpDictionary
    Private m_EditMode As Boolean
    Private m_IsEmail As Boolean
    'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
    Private m_IsBrowser As Boolean
    Private m_IsLedger As Boolean
    'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
    Private m_DefaultButton As Button
    Private m_IsActiveOrder As Boolean

    Protected product As ProductClass
    Protected UOM As String = ""
    Protected unitprice As Decimal = 0
    Protected discountamount As Decimal = 0
    Protected linetotal As Decimal = 0
    Protected deletelink As String = ""

    Protected colspan As Integer = 0

    'AM2010092201 - ENTERPRISE CSR - Start
    Private m_isReturn As Boolean = False
    Protected reasonDict As ExpDictionary
    'AM2010092201 - ENTERPRISE CSR - End

    'AM2010092201 - ENTERPRISE CSR - Start
    Public Property isReturn() As Boolean
        Get
            Return m_isReturn
        End Get
        Set(ByVal value As Boolean)
            m_isReturn = value
        End Set
    End Property
    'AM2010092201 - ENTERPRISE CSR - End

    Public Property OrderLine() As ExpDictionary
        Get
            Return m_OrderLine
        End Get
        Set(ByVal value As ExpDictionary)
            m_OrderLine = value
        End Set
    End Property

    Public Property IsActiveOrder() As Boolean
        Get
            Return m_IsActiveOrder
        End Get
        Set(ByVal value As Boolean)
            m_IsActiveOrder = value
        End Set
    End Property

    Public Property EditMode() As Boolean
        Get
            Return CBoolEx(m_EditMode)
        End Get
        Set(ByVal value As Boolean)
            m_EditMode = CBoolEx(value)
        End Set
    End Property

    Public Property IsEmail() As Boolean
        Get
            Return CBoolEx(m_IsEmail)
        End Get
        Set(ByVal value As Boolean)
            m_IsEmail = CBoolEx(value)
        End Set
    End Property

    'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
    Public Property IsLedger() As Boolean
        Get
            Return CBoolEx(m_IsLedger)
        End Get
        Set(ByVal value As Boolean)
            m_IsLedger = CBoolEx(value)
        End Set
    End Property

    Public Property IsBrowser() As Boolean
        Get
            Return CBoolEx(m_IsBrowser)
        End Get
        Set(ByVal value As Boolean)
            m_IsBrowser = CBoolEx(value)
        End Set
    End Property
    'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End

    Public Property DefaultButton() As Button
        Get
            Return m_DefaultButton
        End Get
        Set(ByVal value As Button)
            m_DefaultButton = value
        End Set
    End Property

    Protected Overloads Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender

        product = New ProductClass(OrderLine)
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
        If (AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_THUMBNAIL") = "1" And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_THUMBNAIL") = "1" And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOWCOLUMN_THUMBNAIL") = "1" And IsLedger) Then
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
            pnlThumbnail.Visible = True
            colspan = colspan + 1
        End If
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
        If AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_PRODUCTGUID") = "1" And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_PRODUCTGUID") = "1" And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOWCOLUMN_PRODUCTGUID") = "1" And IsLedger Then
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
            pnlProductGuidLine.Visible = True
            colspan = colspan + 1
        End If
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
        If AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_NAME") = "1" And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_NAME") = "1" And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOWCOLUMN_NAME") = "1" And IsLedger Then
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
            pnlNameLine.Visible = True
            colspan = colspan + 1
        End If
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
        If (AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_NAME") = "1" And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_NAME") = "1" And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOWCOLUMN_NAME") = "1" And IsLedger) And CStrEx(OrderLine("VariantCode")) <> "" Then
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
            pnlVariantNameLine.Visible = True
            colspan = colspan + 1
        End If
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
        If AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_NAMEANDVARIANT") = "1" And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_NAMEANDVARIANT") = "1" And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOWCOLUMN_NAMEANDVARIANT") = "1" And IsLedger Then
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
            pnlNameAndVariant.Visible = True
            colspan = colspan + 1
        End If
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
        If AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_QUANTITY") = "1" And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_QUANTITY") = "1" And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOWCOLUMN_QUANTITY") = "1" And IsLedger Then
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
            If EditMode Then
                pnlQuantityEdit.Visible = True
                colspan = colspan + 1
            Else
                pnlQuantityNoEdit.Visible = True
                colspan = colspan + 1
            End If
        End If
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
        If AppSettings("EXPANDIT_US_USE_UOM") = "1" And (AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_UOM") = "1" And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_UOM") = "1" And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOWCOLUMN_UOM") = "1" And IsLedger) Then
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
            UOM = CStrEx(getSingleValueDB("SELECT Description FROM UnitOfMeasure WHERE Code=" & SafeString(OrderLine("UOM"))))
            If UOM <> "" Then
                UOM &= "(s)"
            End If
            pnlUOMLine.Visible = True
            colspan = colspan + 1
        End If
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
        If AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_PRICE") = "1" And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_PRICE") = "1" And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOWCOLUMN_PRICE") = "1" And IsLedger Then
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
            pnlPriceLine.Visible = True
            colspan = colspan + 1
        End If
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
        If AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_DISCOUNTAMOUNT") = "1" And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_DISCOUNTAMOUNT") = "1" And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOWCOLUMN_DISCOUNTAMOUNT") = "1" And IsLedger Then
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
            pnlDiscountAmountLine.Visible = True
            colspan = colspan + 1
        End If
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
        If AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_DISCOUNTPCT") = "1" And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_DISCOUNTPCT") = "1" And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOWCOLUMN_DISCOUNTPCT") = "1" And IsLedger Then
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
            pnlDiscountPctLine.Visible = True
            colspan = colspan + 1
        End If
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
        If AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_TOTAL") = "1" And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_TOTAL") = "1" And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOWCOLUMN_TOTAL") = "1" And IsLedger Then
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
            pnlTotalLine.Visible = True
            colspan = colspan + 1
        End If
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
        If EditMode() And (AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_COMMENT") = "1" And IsBrowser) Then
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
            pnlCommentLineEdit.Visible = True
            colspan = colspan + 1
        End If
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
        If Not EditMode() And (AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_COMMENT") = "1" And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_COMMENT") = "1" And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOWCOLUMN_COMMENT") = "1" And IsLedger) Then
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
            pnlCommentLineNoEdit.Visible = True
            colspan = colspan + 1
        End If
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
        If EditMode() And (AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_DELETE") = "1" And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOWCOLUMN_DELETE") = "1" And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOWCOLUMN_DELETE") = "1" And IsLedger) Then
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
            pnlDeleteMark.Visible = True
            colspan = colspan + 1
        End If
        If AppSettings("ORDERRENDER_SHOW_INCLUDING_TAX") Then
            unitprice = RoundEx(CDblEx(OrderLine("ListPriceInclTax")), AppSettings("NUMDECIMALPLACES"))
        Else
            unitprice = RoundEx(CDblEx(OrderLine("ListPrice")), AppSettings("NUMDECIMALPLACES"))
        End If
        If AppSettings("ORDERRENDER_SHOW_INCLUDING_TAX") Then
            discountamount = CDblEx(OrderLine("LineDiscountAmountInclTax"))
        Else
            discountamount = CDblEx(OrderLine("LineDiscountAmount"))
        End If
        If AppSettings("ORDERRENDER_SHOW_INCLUDING_TAX") Then
            linetotal = CDblEx(OrderLine("TotalInclTax"))
        Else
            linetotal = CDblEx(OrderLine("LineTotal"))
        End If
        deletelink = "cart.aspx?cartaction=OutOfBasket&amp;LineGuid=" & HttpContext.Current.Server.UrlEncode(OrderLine("LineGuid").ToString)

        'AM2010061801 - KITTING - START
        If CStrEx(OrderLine("KitBOMNo")) <> "" And Not IsEmail Then
            Dim ProductKitting As ExpDictionary
            ProductKitting = eis.CatDefaultLoadProduct(CStrEx(OrderLine("ProductGuid")), True, True, True)
            If Not ProductKitting("KittingInfo") Is Nothing Then
                Me.KittingList1.KittingParts = ProductKitting("KittingInfo")
                Me.pnlKitting.Visible = True
            End If
        End If
        'AM2010061801 - KITTING - END


        'AM2010092201 - ENTERPRISE CSR - Start
        If isReturn() Then
            If IsBrowser And IsActiveOrder Then 'cart or payment
                Dim sql As String = ""
                sql = "SELECT * FROM ReturnReason"
                reasonDict = SQL2Dicts(sql, "Code")

                If Not reasonDict Is Nothing Then
                    If reasonDict.Count > 0 Then
                        Me.pnlReturnReason.Visible = True
                    End If
                End If
            ElseIf IsBrowser And IsActiveOrder = False Then 'History_Detal
                ShowReturnReasons.Visible = True
            End If
        End If
        'AM2010092201 - ENTERPRISE CSR - End


    End Sub

    'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
    Public Function formatAmount(ByVal amount As Double, ByVal currency As String)
        If CStrEx(currency) <> "" Then
            Return CurrencyFormatter.FormatCurrency(amount, currency)
        Else
            Return CurrencyFormatter.FormatCurrency(amount, CStrEx(AppSettings("MULTICURRENCY_SITE_CURRENCY")))
        End If
    End Function
    'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
End Class
