Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Web
Imports ExpandIT.GlobalsClass
Imports System.Collections.Generic

Partial Class CartRender
    Inherits ExpandIT.UserControl

    Private m_OrderDict As ExpDictionary
    Private m_EditMode As Boolean
    Private m_IsEmail As Boolean
    Private m_DefaultButton As Button
    Private m_ShowDate As Boolean
    Private m_ShowPaymentInfo As Boolean
    Private m_HeaderGuid As String
    Private m_IsActiveOrder As Boolean
    'AM2010061801 - KITTING - START
    Protected myscript As String = ""
    Protected kits As Boolean = False
    'AM2010061801 - KITTING - END
    'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
    Private m_IsLedger As Boolean
    Private m_IsBrowser As Boolean
    Private m_DrillDownType As Integer
    Private m_DrillDownGuid As String
    'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
    'AM2010092201 - ENTERPRISE CSR - Start
    Private m_isReturn As Boolean = False
    'AM2010092201 - ENTERPRISE CSR - End
    'JA2010092201 - RECURRING ORDERS - START
    Private m_isModifyRecurrency As Boolean = False
    'JA2010092201 - RECURRING ORDERS - END
    'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
    Protected csrObj As USCSR
    'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
    Protected sPath As String

    'AM2010092201 - ENTERPRISE CSR - Start
    Public Property isReturn() As Boolean
        Get
            Return m_isReturn
        End Get
        Set(ByVal value As Boolean)
            m_isReturn = value
        End Set
    End Property
    'AM2010092201 - ENTERPRISE CSR - End

    'JA2010092201 - RECURRING ORDERS - START
    Public Property isModifyRecurrency() As Boolean
        Get
            Return m_isModifyRecurrency
        End Get
        Set(ByVal value As Boolean)
            m_isModifyRecurrency = value
        End Set
    End Property
    'JA2010092201 - RECURRING ORDERS - END



    Public Property OrderDict() As ExpDictionary
        Get
            Return m_OrderDict
        End Get
        Set(ByVal value As ExpDictionary)
            m_OrderDict = value
        End Set
    End Property

    Public Property IsActiveOrder() As Boolean
        Get
            Return m_IsActiveOrder
        End Get
        Set(ByVal value As Boolean)
            m_IsActiveOrder = value
        End Set
    End Property

    'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
    Public Property IsLedger() As Boolean
        Get
            Return m_IsLedger
        End Get
        Set(ByVal value As Boolean)
            m_IsLedger = value
        End Set
    End Property

    Public Property IsBrowser() As Boolean
        Get
            Return m_IsBrowser
        End Get
        Set(ByVal value As Boolean)
            m_IsBrowser = value
        End Set
    End Property

    Public Property DrillDownGuid() As String
        Get
            Return m_DrillDownGuid
        End Get
        Set(ByVal value As String)
            m_DrillDownGuid = value
        End Set
    End Property

    Public Property DrillDownType() As Integer
        Get
            Return m_DrillDownType
        End Get
        Set(ByVal value As Integer)
            m_DrillDownType = value
        End Set
    End Property
    'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End

    Public Property HeaderGuid() As String
        Get
            Return m_HeaderGuid
        End Get
        Set(ByVal value As String)
            m_HeaderGuid = value
        End Set
    End Property

    Public Property EditMode() As Boolean
        Get
            Return CBoolEx(m_EditMode)
        End Get
        Set(ByVal value As Boolean)
            m_EditMode = CBoolEx(value)
        End Set
    End Property

    Public Property IsEmail() As Boolean
        Get
            Return CBoolEx(m_IsEmail)
        End Get
        Set(ByVal value As Boolean)
            m_IsEmail = CBoolEx(value)
        End Set
    End Property

    Public Property DefaultButton() As Button
        Get
            Return m_DefaultButton
        End Get
        Set(ByVal value As Button)
            m_DefaultButton = value
        End Set
    End Property

    Public Property ShowDate() As Boolean
        Get
            Return CBoolEx(m_ShowDate)
        End Get
        Set(ByVal value As Boolean)
            m_ShowDate = CBoolEx(value)
        End Set
    End Property

    Public Property ShowPaymentInfo() As Boolean
        Get
            Return CBoolEx(m_ShowPaymentInfo)
        End Get
        Set(ByVal value As Boolean)
            m_ShowPaymentInfo = CBoolEx(value)
        End Set
    End Property

    Public ReadOnly Property SelectedProvider() As String
        Get
            Return Me.ShippingProviders1.SelectedProviderValue()
        End Get
    End Property

    Public ReadOnly Property SelectedService() As String
        Get
            Return Me.ShippingProviders1.SelectedServiceValue
        End Get
    End Property

    Public ReadOnly Property ZipCode() As String
        Get
            Return Me.ShippingProviders1.zipcode
        End Get
    End Property

    Public ReadOnly Property Residential() As Boolean
        Get
            Return Me.ShippingProviders1.IsResidential
        End Get
    End Property

    Public ReadOnly Property SaturdayDelivery() As Boolean
        Get
            Return Me.ShippingProviders1.IsSaturdayDelivery
        End Get
    End Property

    Public ReadOnly Property Insurance() As Boolean
        Get
            Return Me.ShippingProviders1.IsInsurance
        End Get
    End Property

    Public ReadOnly Property InsuredValue() As String
        Get
            Return Me.ShippingProviders1.InsuredValue
        End Get
    End Property

    Public ReadOnly Property ShippingProvidersControl() As Object
        Get
            Return Me.ShippingProviders1
        End Get
    End Property

    Public ReadOnly Property PromotionCode() As String
        Get
            Return Me.PromoBoxEdit.PromotionCode
        End Get
    End Property


    Protected Overloads Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        Dim cartCommentOK As Boolean = False
        Dim poNumberOK As Boolean = False
        Dim orderNumberOK As Boolean = False
        Dim shippingAddressOK As Boolean = False
        Dim billingAddressOK As Boolean = False

		' ExpandIT.FileLogger.log("PreRendering Cart, RequireSSL = "  & Me.RequireSSL)
		
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
        If IsLedger Then
            OrderDict = eis.LoadOrder(DrillDownGuid, DrillDownType)
        ElseIf IsActiveOrder Then
            OrderDict = eis.LoadOrderDictionary(globals.User)
        Else
            OrderDict = eis.LoadOrder(HeaderGuid)
        End If

        cartCommentOK = AppSettings("ORDERRENDER_BROWSER_SHOW_CARTCOMMENT") And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOW_CARTCOMMENT") And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOW_CARTCOMMENT") And IsLedger
        poNumberOK = AppSettings("ORDERRENDER_BROWSER_SHOW_PONUMBER") And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOW_PONUMBER") And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOW_PONUMBER") And IsLedger
        orderNumberOK = AppSettings("ORDERRENDER_BROWSER_SHOW_ORDER_NUMBER") And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOW_ORDER_NUMBER") And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOW_ORDER_NUMBER") And IsLedger
        shippingAddressOK = Not EditMode And (AppSettings("ORDERRENDER_BROWSER_SHOW_SHIPPING_ADDRESS") And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOW_SHIPPING_ADDRESS") And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOW_SHIPPING_ADDRESS") And IsLedger)
        billingAddressOK = Not EditMode And (AppSettings("ORDERRENDER_BROWSER_SHOW_BILLING_ADDRESS") And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOW_BILLING_ADDRESS") And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOW_BILLING_ADDRESS") And IsLedger)
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End

        If shippingAddressOK Or billingAddressOK Then
            pnlAddresses.Visible = True
            If billingAddressOK Then
                pnlBillingAddress.Visible = True
                Me.CartRenderAddressesBillTo.OrderDict = OrderDict
                Me.CartRenderAddressesBillTo.Header = Resources.Language.LABEL_BILL_TO
                Me.CartRenderAddressesBillTo.Prefix = ""
                Me.CartRenderAddressesBillTo.IsEmail = IsEmail
                'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
                Me.CartRenderAddressesBillTo.IsBrowser = IsBrowser
                Me.CartRenderAddressesBillTo.IsLedger = IsLedger
                'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
            End If
            If shippingAddressOK Then
                pnlShippingAddress.Visible = True
                Me.CartRenderAddressesShipTo.OrderDict = OrderDict
                Me.CartRenderAddressesShipTo.Header = Resources.Language.LABEL_SHIP_TO
                Me.CartRenderAddressesShipTo.Prefix = "ShipTo"
                Me.CartRenderAddressesShipTo.IsEmail = IsEmail
                'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
                Me.CartRenderAddressesShipTo.IsBrowser = IsBrowser
                Me.CartRenderAddressesShipTo.IsLedger = IsLedger
                'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
            End If
        End If

        If Not OrderDict("Lines") Is Nothing Then
            ' Load information for products
            Dim myArray1() As String = {"ProductName"}
            Dim myArray2() As String = Nothing
            eis.CatDefaultLoadProducts(OrderDict("Lines"), False, False, False, myArray1, myArray2, Nothing)
            dlCartRender.DataSource = OrderDict("Lines")
            dlCartRender.DataBind()

            'AM2010061801 - KITTING - START
            myscript = "<script type=""text/javascript"">"
            For Each item As ExpDictionary In OrderDict("Lines").values
                If CStrEx(item("KitBOMNo")) <> "" Then
                    myscript = myscript & " animatedcollapse.addDiv('" & CStrEx(item("KitBOMNo")) & "', 'fade=1')" & vbCrLf
                    kits = True
                End If
            Next
            myscript = myscript & " animatedcollapse.ontoggle=function($, divobj, state){}" & vbCrLf
            myscript = myscript & " animatedcollapse.init()" & vbCrLf
            myscript = myscript & " </script>"
            'AM2010061801 - KITTING - END

        End If
        Me.CartRenderTotals1.OrderDict = OrderDict
        Me.CartRenderTotals1.IsEmail = IsEmail
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
        Me.CartRenderTotals1.IsBrowser = IsBrowser
        Me.CartRenderTotals1.IsLedger = IsLedger
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
        'AM2010092201 - ENTERPRISE CSR - Start
        Me.CartRenderTotals1.IsActiveOrder = IsActiveOrder
        'AM2010092201 - ENTERPRISE CSR - End
        If EditMode And (cartCommentOK Or poNumberOK) Then
            pnlAdditionalInfoEditMode.Visible = True
            CartRenderAdditionalInfoEdit.OrderDict = OrderDict
            CartRenderAdditionalInfoEdit.ShowDate = ShowDate
            CartRenderAdditionalInfoEdit.ShowPaymentInfo = ShowPaymentInfo
            CartRenderAdditionalInfoEdit.IsEmail = IsEmail
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
            Me.CartRenderAdditionalInfoEdit.IsBrowser = IsBrowser
            Me.CartRenderAdditionalInfoEdit.IsLedger = IsLedger
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
        End If
        If Not EditMode Then
            pnlAdditionalInfoNoEditMode.Visible = True
            CartRenderAdditionalInfoNoEdit.OrderDict = OrderDict
            CartRenderAdditionalInfoNoEdit.ShowDate = ShowDate
            CartRenderAdditionalInfoNoEdit.ShowPaymentInfo = ShowPaymentInfo
            CartRenderAdditionalInfoNoEdit.IsEmail = IsEmail
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
            Me.CartRenderAdditionalInfoNoEdit.IsBrowser = IsBrowser
            Me.CartRenderAdditionalInfoNoEdit.IsLedger = IsLedger
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
            'AM2010092201 - ENTERPRISE CSR - Start
        Else
            If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And CBoolEx(AppSettings("EXPANDIT_US_USE_ADJUSTMENT")) AndAlso csrObj.getSalesPersonGuid() <> "" AndAlso Not CBoolEx(isModifyRecurrency()) Then
                Me.pnlAdjustmentEdit.Visible = True
            End If
            If CStrEx(OrderDict("DocumentType")) = "Return Order" And CStrEx(OrderDict("OrderReference")) <> "" Then
                Me.pnlAdjustmentEdit.Visible = False
            End If
            'AM2010092201 - ENTERPRISE CSR - End
        End If
        '*****promotions*****-start
        If AppSettings("EXPANDIT_US_USE_PROMOTION_CODES") And CStrEx(OrderDict("PromotionCode")) <> "" Then
            pnlPromotionsNoEditMode.Visible = True
            PromoBoxNoEdit.OrderDict = OrderDict
        End If
        '*****promotions*****-end
        'AM0122201001 - UOM - Start
        'AM2010092201 - ENTERPRISE CSR - Start
        If EditMode And CBoolEx(AppSettings("CART_ENTRY")) And Not CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) Then
            If (CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And csrObj.getSalesPersonGuid() <> "") Or CBoolEx(globals.User("IsB2B")) Then
                'JA2010092201 - RECURRING ORDERS - START
                If (CBoolEx(AppSettings("RECURRING_ORDERS_SHOW")) And Not CBoolEx(isModifyRecurrency())) Or Not CBoolEx(AppSettings("RECURRING_ORDERS_SHOW")) Then
                    Me.pnlQuickCart.Visible = True
                End If
                'JA2010092201 - RECURRING ORDERS - END
            End If
        End If
        'AM2010092201 - ENTERPRISE CSR - End
        'AM0122201001 - UOM - End


    End Sub

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
        csrObj = New USCSR(globals)
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
        If IsLedger Then
            OrderDict = eis.LoadOrder(DrillDownGuid, DrillDownType)
        ElseIf IsActiveOrder Then
            OrderDict = eis.LoadOrderDictionary(globals.User)
        Else
            OrderDict = eis.LoadOrder(HeaderGuid)
        End If
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End

        '*****shipping*****-start
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
        If AppSettings("EXPANDIT_US_USE_SHIPPING") And (AppSettings("ORDERRENDER_BROWSER_SHOW_SHIPPING_CONTROL") And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOW_SHIPPING_CONTROL") And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOW_SHIPPING_CONTROL") And IsLedger) Then
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
            If (IsActiveOrder And Not globals.IsCartEmpty) Or Not IsActiveOrder Then
                pnlShippingProviderSelector.Visible = True
                ShippingProviders1.EditMode = EditMode
                If Not EditMode Then
                    shippingproviders1.OrderDict = OrderDict
                End If
            End If
        End If
        '*****shipping*****-end

        '*****promotions*****-start
        If AppSettings("EXPANDIT_US_USE_PROMOTION_CODES") Then
            pnlPromotionsEditMode.Visible = False
            If EditMode And Not globals.IsCartEmpty Then
                pnlPromotionsEditMode.Visible = True
                PromoBoxEdit.OrderDict = OrderDict
            End If
        End If
        '*****promotions*****-end


        'JA2010092201 - RECURRING ORDERS - START
        If CBoolEx(AppSettings("RECURRING_ORDERS_SHOW")) Then
            If EditMode Then  'cart
                Me.RecurringSchedule1.IsEditable = True
                Me.CbxRecurring.Visible = True
                Me.lblRecurring.Visible = True
            Else
                Me.RecurringSchedule1.IsEditable = False
                Me.CbxRecurring.Visible = False
                Me.lblRecurring.Visible = False
            End If

            Me.RecurringSchedule1.isModifyRecurrency = isModifyRecurrency
            Me.RecurringSchedule1.isActiveOrder = IsActiveOrder

            If IsBrowser And IsActiveOrder Then 'cart or payment
                If Not IsPostBack Then
                    If Not globals.OrderDict("RecurringOrder") Is DBNull.Value Then
                        If CBoolEx(OrderDict("RecurringOrder")) Then
                            Me.RecurringSchedule1.DictOrder = getOrderDictFromModification(globals.OrderDict, "cartPayment")
                            If Me.RecurringSchedule1.DictOrder Is Nothing Then
                                Me.lblRecurring.Visible = False
                                Me.CbxRecurring.Visible = False
                                Me.RecurringSchedule1.Visible = False
                            Else
                                Me.CbxRecurring.Checked = True
                                Me.RecurringSchedule1.Visible = True
                            End If
                        End If
                    End If
                ElseIf Me.CbxRecurring.Checked = True Then
                    Me.RecurringSchedule1.DictOrder = getOrderDictFromModification(globals.OrderDict, "cartPayment")
                    If Me.RecurringSchedule1.DictOrder Is Nothing Then
                        Me.lblRecurring.Visible = False
                        Me.CbxRecurring.Visible = False
                        Me.RecurringSchedule1.Visible = False
                    Else
                        Me.RecurringSchedule1.Visible = True
                    End If
                End If
            ElseIf (IsBrowser And Not IsActiveOrder) Or IsEmail Then 'History_Detail or email
                If Not OrderDict Is Nothing Then
                    If CBoolEx(OrderDict("RecurringOrder")) Then
                        Me.RecurringSchedule1.DictOrder = getOrderDictFromModification(OrderDict, "history")
                    Else
                        Me.lblRecurring.Visible = False
                        Me.CbxRecurring.Visible = False
                        Me.RecurringSchedule1.Visible = False
                    End If
                End If
            End If
            If CBoolEx(isModifyRecurrency()) Then
                Me.pnlCartRender.Visible = False
                Me.pnlOrderTotals.Visible = False
                Me.CbxRecurring.Visible = False
                Me.pnlAdditonalInformation.Visible = False
                Me.pnlQuickCart.Visible = False
                Me.lblRecurring.Text = Resources.Language.LABEL_MODIFYING_RECURRING_ORDER
            End If

            If CStrEx(OrderDict("DocumentType")) = "Return Order" Then
                Me.lblRecurring.Visible = False
                Me.CbxRecurring.Visible = False
                Me.RecurringSchedule1.Visible = False
            End If


        End If
        'JA2010092201 - RECURRING ORDERS - END



    End Sub

    'JA2010092201 - RECURRING ORDERS - START

    Protected Function getOrderDictFromModification(ByVal currentOrderDict As ExpDictionary, ByVal from As String)
        Dim newOrderDict As New ExpDictionary
        Dim sql As String = ""
        If from = "history" Then
            sql = "SELECT TOP 1 ModifiedRecurrency FROM CancelledOrders WHERE CustomerReferenceOriginal =" & SafeString(CStrEx(currentOrderDict("CustomerReference"))) & " ORDER BY CancelationDate DESC"
        ElseIf from = "cartPayment" Then
            sql = "SELECT TOP 1 ModifiedRecurrency FROM CancelledOrders WHERE CustomerReferenceOriginal =" & SafeString(CStrEx(currentOrderDict("OrderReference"))) & " ORDER BY CancelationDate DESC"
        End If

        Dim isMod As Boolean = CBoolEx(getSingleValueDB(sql))
        If isMod Then
            If from = "history" Then
                    sql = "SELECT * FROM ShopSalesLine WHERE HeaderGuid In (SELECT TOP 1 HeaderGuid FROM ShopSalesHeader WHERE ModifiedRecurrency =" & SafeString(CStrEx(currentOrderDict("CustomerReference"))) & " ORDER BY HeaderDate DESC)"
                    Dim newOrderLines As ExpDictionary = SQL2Dicts(sql)
                    If Not newOrderLines Is Nothing AndAlso Not newOrderLines Is DBNull.Value AndAlso newOrderLines.Count > 0 Then
                    newOrderDict = currentOrderDict
                    newOrderDict("Lines") = newOrderLines
                    Else
                        newOrderDict = currentOrderDict
                    End If
                    
            ElseIf from = "cartPayment" Then
                newOrderDict = currentOrderDict
            End If
            Return newOrderDict
        Else
            If from = "history" Then
                sql = "SELECT Top 1 CancelRecurrency FROM CancelledOrders WHERE CustomerReferenceOriginal =" & SafeString(CStrEx(currentOrderDict("CustomerReference"))) & " ORDER BY CancelationDate DESC"
            ElseIf from = "cartPayment" Then
                sql = "SELECT Top 1 CancelRecurrency FROM CancelledOrders WHERE CustomerReferenceOriginal =" & SafeString(CStrEx(currentOrderDict("OrderReference"))) & " ORDER BY CancelationDate DESC"
            End If
            Dim isCancel As Boolean = CBoolEx(getSingleValueDB(sql))
            If isCancel Then
                Return Nothing
            Else
                If CStrEx(Request("justReturn")) <> "" Then
                    Return Nothing
                Else
                    Return currentOrderDict
                End If
            End If
        End If

    End Function

    Protected Sub CbxRecurring_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CbxRecurring.CheckedChanged

        If globals.OrderDict Is Nothing Then
            'If CBoolEx(globals.User("Admin")) Then
            '    globals.OrderDict = eis.LoadOrderDictionaryAsAdmin(globals.User("SalesPersonCode"))
            'Else
            globals.OrderDict = eis.LoadOrderDictionary(globals.User)
            'End If
        End If
        If Me.CbxRecurring.Checked Then
            Me.RecurringSchedule1.Visible = True
            Me.RecurringSchedule1.DictOrder = globals.OrderDict
            globals.OrderDict("RecurringOrder") = True
        Else
            Me.RecurringSchedule1.Visible = False
            globals.OrderDict("RecurringOrder") = False
        End If

        eis.SaveOrderDictionary(globals.OrderDict)




        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script4", "closeLoading();", True)


    End Sub


    
    'JA2010092201 - RECURRING ORDERS - END





End Class
