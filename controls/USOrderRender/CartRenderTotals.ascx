<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CartRenderTotals.ascx.vb"
    Inherits="CartRenderTotals" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Import Namespace="System.Collections.Generic" %>
<table style="<%If eis.CheckPageAccess("Prices") Then Response.Write("margin-top: 10px; margin-bottom: 10px;") Else Response.Write("display:none;") %>">
    <tr class="CartTotals_SubTotal">
        <td class="CartTotals_SubTotalCaption">
            <% = Resources.Language.LABEL_SUB_TOTAL%>
        </td>
        <td class="CartTotals_SubTotalValue">
            <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start--%>
            <% = 
                formatAmount(subtotal, OrderDict("CurrencyGuid"))%>
            <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End--%>
        </td>
    </tr>
    <asp:Panel ID="pnlDiscountAmountTotals" runat="server" Visible="false">
        <tr class="CartTotals_InvoiceDiscount">
            <td class="CartTotals_InvoiceDiscountCaption">
                <% =Resources.Language.LABEL_INVOICE_DISCOUNT%>
            </td>
            <td class="CartTotals_InvoiceDiscountValue">
                <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start--%>
                <% =formatamount(invoicediscount, OrderDict("CurrencyGuid"))%>
                <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End--%>
            </td>
        </tr>
    </asp:Panel>
    <asp:Panel ID="pnlServiceChargeTotals" runat="server" Visible="false">
        <tr class="CartTotals_ServiceCharge" style="line-height: 12px;">
            <td class="CartTotals_ServiceChargeCaption">
                <% =Resources.Language.LABEL_SERVICE_CHARGE%>
            </td>
            <td class="CartTotals_ServiceChargeValue">
                <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start--%>
                <% = formatamount(servicecharge, OrderDict("CurrencyGuid"))%>
                <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End--%>
            </td>
        </tr>
    </asp:Panel>
    <asp:Panel ID="pnlShippingAmountTotals" runat="server" Visible="false">
        <tr class="CartTotals_Shipping" style="line-height: 12px;">
            <td class="CartTotals_ShippingCaption">
                <% = Resources.Language.LABEL_SHIPPING%>
            </td>
            <td class="CartTotals_ShippingValue">
                <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start--%>
                <% = formatamount(shippingamount, OrderDict("CurrencyGuid"))%>
                <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End--%>
            </td>
        </tr>
    </asp:Panel>
    <!-- AM2010042601 - ENTERPRISE SHIPPING - Start -->
    <asp:Panel ID="pnlFreeShippingAmountTotals" runat="server" Visible="false">
        <tr class="CartTotals_Shipping" style="line-height: 12px;">
            <td class="CartTotals_ShippingCaption">
                <% = Resources.Language.LABEL_SHIPPING%>
            </td>
            <td class="CartTotals_FreeShippingValue">
                <% =Resources.Language.LABEL_FREE_SHIPPING_HANDLING_VALUE%>
            </td>
        </tr>
    </asp:Panel>
    <!-- AM2010042601 - ENTERPRISE SHIPPING - End -->
    <asp:Panel ID="pnlHandlingAmountTotals" runat="server" Visible="false">
        <tr class="CartTotals_Handling" style="line-height: 12px;">
            <td class="CartTotals_HandlingCaption">
                <% =Resources.Language.LABEL_HANDLING %>
            </td>
            <td class="CartTotals_HandlingValue">
                <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start--%>
                <% = formatamount(handlingamount, OrderDict("CurrencyGuid"))%>
                <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End--%>
            </td>
        </tr>
    </asp:Panel>
    <!-- AM2010042601 - ENTERPRISE SHIPPING - Start -->
    <asp:Panel ID="pnlFreeHandlingAmountTotals" runat="server" Visible="false">
        <tr class="CartTotals_Handling" style="line-height: 12px;">
            <td class="CartTotals_HandlingCaption">
                <% =Resources.Language.LABEL_HANDLING %>
            </td>
            <td class="CartTotals_FreeHandlingValue">
                <% =Resources.Language.LABEL_FREE_SHIPPING_HANDLING_VALUE%>
            </td>
        </tr>
    </asp:Panel>
    <!-- AM2010042601 - ENTERPRISE SHIPPING - End -->
    <asp:Panel ID="pnlPaymentFeeAmountTotals" runat="server" Visible="false">
        <tr class="CartTotals_PaymentFee" style="line-height: 12px;">
            <td class="CartTotals_PaymentFeeCaption">
                <% =Resources.Language.LABEL_PAYMENT_FEE%>
            </td>
            <td class="CartTotals_PaymentFeeValue">
                <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start--%>
                <% =formatamount(CDblEx(OrderDict("PaymentFeeAmount")), OrderDict("CurrencyGuid"))%>
                <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End--%>
            </td>
        </tr>
    </asp:Panel>
    <%--ExpandIT US - MPF - US Sales Tax - Start
        TAX
        20080530 - ExpandIT US - MPF - US Sales Tax Modification--%>
    <asp:Panel ID="pnlTaxAmountUSTotals" runat="server" Visible="false">
        <tr class="CartTotals_Tax" style="line-height: 12px;">
            <td class="CartTotals_TaxCaption">
                <% =Resources.Language.LABEL_TAXAMOUNT%>
            </td>
            <td class="CartTotals_TaxValue">
                <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start--%>
                <% =formatamount(CDblEx(OrderDict("TaxAmount")), OrderDict("CurrencyGuid"))%>
                <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End--%>
            </td>
        </tr>
    </asp:Panel>
    <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start--%>
    <asp:Panel ID="pnlTaxAmountLedger" runat="server" Visible="false">
        <tr class="CartTotals_Tax" style="line-height: 12px;">
            <td class="CartTotals_TaxCaption">
                <% =Resources.Language.LABEL_TAXAMOUNT%>
            </td>
            <td class="CartTotals_TaxValue">
                <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start--%>
                <% =formatamount(CDblEx(OrderDict("TotalInclTax"))-subtotal, OrderDict("CurrencyGuid"))%>
                <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End--%>
            </td>
        </tr>
    </asp:Panel>
    <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End--%>
    <%--AM2010092201 - ENTERPRISE CSR - Start--%>
    <%If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And CBoolEx(AppSettings("EXPANDIT_US_USE_ADJUSTMENT")) AndAlso csrObj.getSalesPersonGuid() <> "" Then%>
    <asp:Panel ID="pnlAdjustment" runat="server" Visible="false">
        <tr class="CartTotals_Shipping" style="line-height: 12px;">
            <td class="CartTotals_ShippingCaption">
                <%=Resources.Language.LABEL_INPUT_ADJUSTMENT_AMOUNT %>
            </td>
            <td class="CartTotals_ShippingValue">
                <%=CurrencyFormatter.FormatCurrency(CDblEx(OrderDict("AdjustmentAmount")), OrderDict("CurrencyGuid"))%>
            </td> 
        </tr>
    </asp:Panel>
    <%End If%>
    <%--AM2010092201 - ENTERPRISE CSR - END--%>
    <asp:Panel ID="pnlTotalInclTax" runat="server" Visible="false">
        <tr class="CartTotals_Total" style="line-height: 12px;">
            <td class="CartTotals_TotalCaption">
                <% =Resources.Language.LABEL_TOTAL & " " & Resources.Language.LABEL_EXCLUSIVE_VAT%>
            </td>
            <td class="CartTotals_TotalValue">
                <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start--%>
                <% =formatamount(totalamount, OrderDict("CurrencyGuid"))%>
                <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End--%>
            </td>
        </tr>
    </asp:Panel>
    <%------------ TAX ------------%>
    <asp:Panel ID="pnlTaxAmountTotals" runat="server" Visible="false">
        <%For Each kvPair As KeyValuePair(Of Object, Object) In taxAmounts%>
        <%If Not CDblEx(kvPair.Key) = 0 And Not CDblEx(kvPair.Value) = 0 Then%>
        <tr class="CartTotals_Total" style="line-height: 12px;">
            <td class="CartTotals_TotalCaption">
                <% =Resources.Language.LABEL_TAXAMOUNT & " " & kvPair.Key & "%"%>
            </td>
            <td class="CartTotals_TotalValue">
                <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start--%>
                <% = formatamount(kvPair.Value, OrderDict("CurrencyGuid"))%>
                <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End--%>
            </td>
        </tr>
        <%End If%>
        <%Next%>
    </asp:Panel>
    <asp:Panel ID="pnlGrandTotalInclTax" runat="server" Visible="false">
        <tr class="CartTotals_Total" style="line-height: 12px;">
            <td class="CartTotals_TotalCaption">
                <% = Resources.Language.LABEL_TOTAL & " " & Resources.Language.LABEL_INCLUSIVE_VAT%>
            </td>
            <td class="CartTotals_TotalValue">
                <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start--%>
                <%--AM2010092201 - ENTERPRISE CSR - Start--%>
                <% =formatAmount(grandTotal, OrderDict("CurrencyGuid"))%>
                <%--AM2010092201 - ENTERPRISE CSR - End--%>
                <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End--%>
            </td>
        </tr>
    </asp:Panel>
    <asp:Panel ID="pnlGrandTotal" runat="server" Visible="false">
        <tr class="CartTotals_Total" style="line-height: 12px;">
            <td class="CartTotals_TotalCaption">
                <% =Resources.Language.LABEL_TOTAL%>
            </td>
            <td class="CartTotals_TotalValue">
                <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start--%>
                <%--AM2010092201 - ENTERPRISE CSR - Start--%>
                <% =formatAmount(grandTotal, OrderDict("CurrencyGuid"))%>
                <%--AM2010092201 - ENTERPRISE CSR - End--%>
                <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End--%>
            </td>
        </tr>
    </asp:Panel>
    <%------------ TAX END ------------%>
    <%--*****shipping*****-start--%>
    <asp:Panel ID="pnlShippingProviderTotals" runat="server" Visible="false">
        <tr class="Order_ShippingInfo" style="line-height: 12px;">
            <td class="Order_ShippingInfoCaption">
                <% =Resources.Language.LABEL_HANDLING_AND_SHIPPING %>
            </td>
            <td class="Order_ShippingInfoValue">
                <asp:Panel ID="pnlShippingProviderLink" runat="server" Visible="false">
                    <a href="<% = VRoot & "/shippingprovider.aspx?returl=" & HttpUtility.UrlEncode(HttpContext.Current.Request.Path)%>">
                        <% =ShippingProvidername %>
                    </a>
                </asp:Panel>
                <asp:Panel ID="pnlShippingProviderName" runat="server" Visible="false">
                    <% =ShippingProvidername %>
                </asp:Panel>
            </td>
        </tr>
    </asp:Panel>
    <%--*****shipping*****-end--%>
</table>
