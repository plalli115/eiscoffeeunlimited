Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Web
Imports ExpandIT.GlobalsClass
Imports System.Collections.Generic

Partial Class CartRenderTotals
    Inherits ExpandIT.UserControl

    Private m_OrderDict As ExpDictionary
    Private m_IsEmail As Boolean
    'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
    Private m_IsBrowser As Boolean
    Private m_IsLedger As Boolean
    'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
    'AM2010092201 - ENTERPRISE CSR - Start 
    Private m_IsActiveOrder As Boolean
    'AM2010092201 - ENTERPRISE CSR - End

    Protected subtotal As Decimal
    Protected invoicediscount As Decimal
    Protected servicecharge As Decimal
    Protected shippingamount As Decimal
    Protected handlingamount As Decimal
    Protected totalamount As Decimal
    Protected taxAmounts As ExpDictionary
    Protected shippingProviderName As String
    Protected shippingHandlingProviderGuid As String
    Protected shipToZipCode As String
    'AM2010092201 - ENTERPRISE CSR - Start
    Protected grandTotal As Decimal
    'AM2010092201 - ENTERPRISE CSR - End
    'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
    Protected csrObj As USCSR
    'AM2011031801 - ENTERPRISE CSR STAND ALONE - End

    Public Property OrderDict() As ExpDictionary
        Get
            Return m_OrderDict
        End Get
        Set(ByVal value As ExpDictionary)
            m_OrderDict = value
        End Set
    End Property

    Public Property IsEmail() As Boolean
        Get
            Return CBoolEx(m_IsEmail)
        End Get
        Set(ByVal value As Boolean)
            m_IsEmail = CBoolEx(value)
        End Set
    End Property

    'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
    Public Property IsLedger() As Boolean
        Get
            Return CBoolEx(m_IsLedger)
        End Get
        Set(ByVal value As Boolean)
            m_IsLedger = CBoolEx(value)
        End Set
    End Property

    Public Property IsBrowser() As Boolean
        Get
            Return CBoolEx(m_IsBrowser)
        End Get
        Set(ByVal value As Boolean)
            m_IsBrowser = CBoolEx(value)
        End Set
    End Property
    'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End

    'AM2010092201 - ENTERPRISE CSR - Start
    Public Property IsActiveOrder() As Boolean
        Get
            Return m_IsActiveOrder
        End Get
        Set(ByVal value As Boolean)
            m_IsActiveOrder = value
        End Set
    End Property
    'AM2010092201 - ENTERPRISE CSR - End

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
        csrObj = New USCSR(globals)
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
    End Sub

    Protected Overloads Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        shippingHandlingProviderGuid = CStrEx(OrderDict("ShippingHandlingProviderGuid"))
        shipToZipCode = CStrEx(OrderDict("ShipToZipCode"))
        If AppSettings("ORDERRENDER_SHOW_INCLUDING_TAX") Then
            subtotal = CDblEx(OrderDict("SubTotalInclTax"))
            invoicediscount = CDblEx(OrderDict("InvoiceDiscountInclTax"))
            servicecharge = CDblEx(OrderDict("ServiceChargeInclTax"))
            shippingamount = CDblEx(OrderDict("ShippingAmountInclTax"))
            handlingamount = CDblEx(OrderDict("HandlingAmountInclTax"))
        Else
            subtotal = CDblEx(OrderDict("SubTotal"))
            invoicediscount = CDblEx(OrderDict("InvoiceDiscount"))
            servicecharge = CDblEx(OrderDict("ServiceCharge"))
            shippingamount = CDblEx(OrderDict("ShippingAmount"))
            handlingamount = CDblEx(OrderDict("HandlingAmount"))
        End If
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
        If AppSettings("ORDERRENDER_BROWSER_SHOW_INVOICE_DISCOUNTS") And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOW_INVOICE_DISCOUNTS") And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOW_INVOICE_DISCOUNTS") And IsLedger Then
            pnlDiscountAmountTotals.Visible = True
        End If
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
        If AppSettings("ORDERRENDER_BROWSER_SHOW_SERVICECHARGE") And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOW_SERVICECHARGE") And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOW_SERVICECHARGE") And IsLedger Then
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
            pnlServiceChargeTotals.Visible = True
        End If
        If AppSettings("SHIPPING_HANDLING_ENABLED") Then
            '*****shipping*****-start
            Dim shippingObj As New liveShipping()
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
            If AppSettings("ORDERRENDER_BROWSER_SHOW_SHIPPINGAMOUNT") And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOW_SHIPPINGAMOUNT") And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOW_SHIPPINGAMOUNT") And IsLedger Then
                'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
                'AM2010042601 - ENTERPRISE SHIPPING - Start
                If shippingamount = 0 And shippingHandlingProviderGuid <> "" And shippingHandlingProviderGuid <> "NONE" And shipToZipCode <> "" And Not shippingObj.hasShippingError(globals) Then
                    pnlFreeShippingAmountTotals.Visible = True
                    pnlShippingAmountTotals.Visible = False
                Else
                    pnlShippingAmountTotals.Visible = True
                    pnlFreeShippingAmountTotals.Visible = False
                End If
                'AM2010042601 - ENTERPRISE SHIPPING - End
            End If
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
            If AppSettings("ORDERRENDER_BROWSER_SHOW_HANDLINGAMOUNT") And IsBrowser Or AppSettings("ORDERRENDER_BROWSER_SHOW_HANDLINGAMOUNT") And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOW_HANDLINGAMOUNT") And IsLedger Then
                'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
                'AM2010042601 - ENTERPRISE SHIPPING - Start
                If handlingamount = 0 And shippingHandlingProviderGuid <> "" And shippingHandlingProviderGuid <> "NONE" And shipToZipCode <> "" And Not shippingObj.hasShippingError(globals) Then
                    '*****shipping*****-end
                    pnlFreeHandlingAmountTotals.Visible = True
                    pnlHandlingAmountTotals.Visible = False
                Else
                    pnlHandlingAmountTotals.Visible = True
                    pnlFreeHandlingAmountTotals.Visible = False
                End If
                'AM2010042601 - ENTERPRISE SHIPPING - End
            End If
        End If
        If CDblEx(OrderDict("PaymentFeeAmount")) > 0 Then
            pnlPaymentFeeAmountTotals.Visible = True
        End If
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
        If IsLedger And CBoolEx(AppSettings("SHOW_TAX")) Then
            pnlTaxAmountLedger.Visible = True
        End If
        If Not IsLedger And AppSettings("SHOW_TAX") And AppSettings("EXPANDIT_US_USE_US_SALES_TAX") Then
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
            pnlTaxAmountUSTotals.Visible = True
        End If
        totalamount = CDblEx(OrderDict("Total"))
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
        If AppSettings("ORDERRENDER_BROWSER_SHOWTOTAL_ICLTAX") And IsBrowser Then
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
            pnlTotalInclTax.Visible = True
        End If

        '<!-- ' ExpandIT US - MPF - US Sales Tax - Start -->
        ' ---------- TAX ----------
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
        If Not IsLedger And CBoolEx(AppSettings("SHOW_TAX")) And Not CBoolEx(AppSettings("EXPANDIT_US_USE_US_SALES_TAX")) Then
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
            '<!-- ' ExpandIT US - MPF - US Sales Tax - End -->
            taxAmounts = New ExpDictionary()
            Dim invoiceDiscount2 As Decimal = CDblEx(OrderDict("InvoiceDiscountPct"))
            If Not String.IsNullOrEmpty(CType(DBNull2Nothing(OrderDict("VatTypes")), String)) Then
                Try
                    Dim x As ExpDictionary = UnmarshallDictionary(OrderDict("VatTypes"))
                    For Each vatType As Decimal In x.Keys
                        taxAmounts(vatType) = x(vatType)
                    Next
                Catch ex As Exception
                    FileLogger.log(ex.Message)
                End Try
            Else
                For Each orderline As ExpDictionary In OrderDict("Lines").values
                    Dim orderLineTaxPct As Decimal = CDblEx(orderline("TaxPct"))
                    If Not taxAmounts.Exists(orderLineTaxPct) Then
                        taxAmounts.Add(orderLineTaxPct, CDblEx(orderline("TaxAmount")) * (100 - invoiceDiscount2) / 100)
                    Else
                        Dim calcLine As Decimal = (CDblEx(orderline("TaxAmount")) * (100 - invoiceDiscount2) / 100)
                        taxAmounts(orderLineTaxPct) = CDblEx(taxAmounts(orderLineTaxPct)) + calcLine
                    End If
                Next
            End If
            ' Check also the Header for
            ' 1. Handling tax amount
            ' 2. Shipping tax amount
            ' 3. Service charge tax amount 

            Dim shippingTaxAmount As Decimal = CDblEx(OrderDict("ShippingAmountInclTax")) - CDblEx(OrderDict("ShippingAmount"))
            Dim handlingTaxAmount As Decimal = CDblEx(OrderDict("HandlingAmountInclTax")) - CDblEx(OrderDict("HandlingAmount"))
            Dim serviceChargeAmount As Decimal = CDblEx(OrderDict("ServiceChargeInclTax")) - CDblEx(OrderDict("ServiceCharge"))

            Dim shippingTaxPct As Decimal = CDblEx(AppSettings("SHIPPING_TAX_PCT"))
            Dim handlingTaxPct As Decimal = CDblEx(OrderDict("TaxPct"))
            Dim serviceChargeTacPct As Decimal = CDblEx(OrderDict("TaxPct")) ' This might be a different value - must be investigated

            ' Add Shipping Tax amounts
            If Not taxAmounts.Exists(shippingTaxPct) Then
                taxAmounts.Add(shippingTaxPct, shippingTaxAmount)
            Else
                taxAmounts(shippingTaxPct) = taxAmounts(shippingTaxPct) + shippingTaxAmount
            End If

            ' Add Handling Tax Amounts
            If Not taxAmounts.Exists(handlingTaxPct) Then
                taxAmounts.Add(handlingTaxPct, handlingTaxAmount)
            Else
                taxAmounts(handlingTaxPct) = taxAmounts(handlingTaxPct) + handlingTaxAmount
            End If

            ' Add ServiceCharge Tax Amounts
            If Not taxAmounts.Exists(serviceChargeTacPct) Then
                taxAmounts.Add(serviceChargeTacPct, serviceChargeAmount)
            Else
                taxAmounts(serviceChargeTacPct) = taxAmounts(serviceChargeTacPct) + serviceChargeAmount
            End If
            pnlTaxAmountTotals.Visible = True
        End If
        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
        If AppSettings("ORDERRENDER_BROWSER_SHOWTOTAL_ICLTAX") And IsBrowser Then
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
            pnlGrandTotalInclTax.Visible = True
        Else
            pnlGrandTotal.Visible = True
        End If
        'AM2010092201 - ENTERPRISE CSR - Start
        grandTotal = CDblEx(OrderDict("TotalInclTax"))
        If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And CBoolEx(AppSettings("EXPANDIT_US_USE_ADJUSTMENT")) AndAlso csrObj.getSalesPersonGuid() <> "" Then
            Me.pnlAdjustment.Visible = True
            'If IsActiveOrder And CStrEx(OrderDict("DocumentType")) <> "Return Order" Then
            '    grandTotal += CDblEx(OrderDict("AdjustmentAmount"))
            'ElseIf CStrEx(OrderDict("DocumentType")) = "Return Order" And CStrEx(OrderDict("OrderReference")) = "" Then
            '    grandTotal += CDblEx(OrderDict("AdjustmentAmount"))
            'ElseIf IsActiveOrder = False And CStrEx(OrderDict("DocumentType")) = "Return Order" And CStrEx(OrderDict("OrderReference")) <> "" Then
            '    grandTotal += CDblEx(OrderDict("AdjustmentAmount"))
            'ElseIf IsActiveOrder And CStrEx(OrderDict("DocumentType")) = "Return Order" And CBoolEx(OrderDict("NotRefundShipping")) Then
            '    grandTotal += CDblEx(OrderDict("AdjustmentAmount"))
            'End If
        End If
        'AM2010092201 - ENTERPRISE CSR - End
        '---------- TAX END ----------
        '*****shipping*****-start
        If Not CBoolEx(AppSettings("EXPANDIT_US_USE_SHIPPING")) Then
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
            If AppSettings("ORDERRENDER_BROWSER_SHOW_SHIPPING_INFO") And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOW_SHIPPING_INFO") And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOW_SHIPPING_INFO") And IsLedger Then
                'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
                Dim dictSHProviders As Object
                dictSHProviders = eis.GetShippingHandlingProviders(OrderDict("ShippingHandlingProviderGuid"), False)
                Dim dictSHProvider As ExpDictionary = GetFirst(dictSHProviders).Value
                Dim sql = "SELECT     COUNT(*) FROM ShippingHandlingProvider"
                Dim ret = getSingleValueDB(sql)
                If Not ret Is Nothing Then
                    If ret > 1 Then
                        pnlShippingProviderTotals.Visible = True
                        'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
                        If AppSettings("ORDERRENDER_BROWSER_SHOW_SHIPPING_AS_LINK") And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOW_SHIPPING_AS_LINK") And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOW_SHIPPING_AS_LINK") And IsLedger Then
                            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
                            pnlShippingProviderLink.Visible = True
                        Else
                            pnlShippingProviderName.Visible = False
                        End If
                        If dictSHProvider IsNot Nothing Then
                            shippingProviderName = IIf(CStrEx(dictSHProvider("ProviderName")) <> "", HTMLEncode(CStrEx(dictSHProvider("ProviderName"))), Resources.Language.LABEL_NOT_SELECTED)
                        Else
                            shippingProviderName = Resources.Language.LABEL_NOT_SELECTED
                        End If
                    End If
                End If
            End If
        End If
        '*****shipping*****-end

    End Sub

    'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
    Public Function formatAmount(ByVal amount As Double, ByVal currency As String)
        If CStrEx(currency) <> "" Then
            Return CurrencyFormatter.FormatCurrency(amount, currency)
        Else
            Return CurrencyFormatter.FormatCurrency(amount, CStrEx(AppSettings("MULTICURRENCY_SITE_CURRENCY")))
        End If
    End Function
    'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End

End Class
