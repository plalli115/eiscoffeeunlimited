Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Web
Imports ExpandIT.GlobalsClass
Imports System.Collections.Generic

Partial Class CartRenderAdditionalInfo
    Inherits ExpandIT.UserControl

    Private m_OrderDict As ExpDictionary
    Private m_EditMode As Boolean
    Private m_IsEmail As Boolean
    'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
    Private m_IsBrowser As Boolean
    Private m_IsLedger As Boolean
    'AM2010091001 - ONLINE PAYMENTS - Start
    Protected dictOnlinePayments As ExpDictionary
    Protected firstOnlinePayment As Boolean = True
    Protected onlinePaymentsTotal As Double
    'AM2010091001 - ONLINE PAYMENTS - End
    'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
    Private m_ShowDate As Boolean
    Private m_ShowPaymentInfo As Boolean

    Protected subtotal As Decimal
    Protected invoicediscount As Decimal
    Protected servicecharge As Decimal
    Protected shippingamount As Decimal
    Protected handlingamount As Decimal
    Protected totalamount As Decimal
    Protected taxAmounts As ExpDictionary
    Protected shippingProviderName As String
    'JA2010102801 - PAPERCHECK - Start
    Protected checks As ExpDictionary
    'JA2010102801 - PAPERCHECK - End
    'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
    Protected csrObj As USCSR
    'AM2011031801 - ENTERPRISE CSR STAND ALONE - End

    Public Property OrderDict() As ExpDictionary
        Get
            Return m_OrderDict
        End Get
        Set(ByVal value As ExpDictionary)
            m_OrderDict = value
        End Set
    End Property

    Public Property EditMode() As Boolean
        Get
            Return CBoolEx(m_EditMode)
        End Get
        Set(ByVal value As Boolean)
            m_EditMode = CBoolEx(value)
        End Set
    End Property

    Public Property IsEmail() As Boolean
        Get
            Return CBoolEx(m_IsEmail)
        End Get
        Set(ByVal value As Boolean)
            m_IsEmail = CBoolEx(value)
        End Set
    End Property

    'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
    Public Property IsLedger() As Boolean
        Get
            Return CBoolEx(m_IsLedger)
        End Get
        Set(ByVal value As Boolean)
            m_IsLedger = CBoolEx(value)
        End Set
    End Property

    Public Property IsBrowser() As Boolean
        Get
            Return CBoolEx(m_IsBrowser)
        End Get
        Set(ByVal value As Boolean)
            m_IsBrowser = CBoolEx(value)
        End Set
    End Property
    'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End

    Public Property ShowDate() As Boolean
        Get
            Return CBoolEx(m_ShowDate)
        End Get
        Set(ByVal value As Boolean)
            m_ShowDate = CBoolEx(value)
        End Set
    End Property

    Public Property ShowPaymentInfo() As Boolean
        Get
            Return CBoolEx(m_ShowPaymentInfo)
        End Get
        Set(ByVal value As Boolean)
            m_ShowPaymentInfo = CBoolEx(value)
        End Set
    End Property

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
        csrObj = New USCSR(globals)
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
    End Sub

    Protected Overloads Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        If EditMode Then
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
            If AppSettings("ORDERRENDER_BROWSER_SHOW_CARTCOMMENT") And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOW_CARTCOMMENT") And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOW_CARTCOMMENT") And IsLedger Then
                'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
                pnlAdditionalInfoEdit.Visible = True
                Me.pnlCommentEdit.Visible = True
            End If
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
            If AppSettings("ORDERRENDER_BROWSER_SHOW_PONUMBER") And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOW_PONUMBER") And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOW_PONUMBER") And IsLedger Then
                'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
                pnlAdditionalInfoEdit.Visible = True
                Me.pnlPONumberEdit.Visible = True
            End If

            'AM2010092201 - ENTERPRISE CSR - Start
            If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) AndAlso csrObj.getSalesPersonGuid() <> "" Then
                If CBoolEx(AppSettings("EXPANDIT_US_USE_HOLD_ORDER_DATE")) Then
                    Me.pnlHoldOrderDateEdit.Visible = True
                End If
            End If
            If CStrEx(OrderDict("DocumentType")) = "Return Order" Then
                If CBoolEx(AppSettings("EXPANDIT_US_USE_HOLD_ORDER_DATE")) Then
                    Me.pnlHoldOrderDateEdit.Visible = False
                End If
            End If
            'AM2010092201 - ENTERPRISE CSR - End
            'WLB 10/12/2012 - BEGIN - Add "Your E-mail" to Additional Information  
            '<%--WLB.REQUIRED --%>
            'inputYourEmail.Text = CStrEx(OrderDict("YourEmail"))
            'WLB 10/12/2012 - END - Add "Your E-mail" to Additional Information
        Else
            'AM2010091001 - ONLINE PAYMENTS - Start
            If IsLedger And CBoolEx(AppSettings("EXPANDIT_US_USE_EEPG_ONLINE_PAYMENTS")) Then
                Dim sql As String
                'JA2011030701 - PAYMENT TABLE - START
                'sql = "SELECT op.PaymentGuid, op.PaymentDate, op.PaymentTransactionAmount, pop.DocumentGuid, pop.ProcessedDate FROM EEPGOnlinePayments op LEFT OUTER JOIN EEPGProcessedOnlinePayments pop ON op.PaymentGuid=pop.PaymentGuid WHERE op.DocumentGuid=" & SafeString(OrderDict("HeaderGuid"))
                sql = "SELECT op.PaymentGuid, op.PaymentDate, op.PaymentTransactionAmount, pop.DocumentGuid, pop.ProcessedDate FROM PaymentTable op LEFT OUTER JOIN EEPGProcessedOnlinePayments pop ON op.PaymentGuid=pop.PaymentGuid WHERE op.LedgerPayment=1 AND op.DocumentGuid=" & SafeString(OrderDict("HeaderGuid"))
                'JA2011030701 - PAYMENT TABLE - END
                dictOnlinePayments = SQL2Dicts(sql, "PaymentGuid")
                If Not dictOnlinePayments Is Nothing Then
                    If dictOnlinePayments.Count > 0 Then
                        pnlOnlinePayments.Visible = True
                    End If
                End If
            End If
            'AM2010091001 - ONLINE PAYMENTS - End

            'AM2010092201 - ENTERPRISE CSR - Start
            If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) AndAlso csrObj.getSalesPersonGuid() <> "" Then
                If CBoolEx(AppSettings("EXPANDIT_US_USE_HOLD_ORDER_DATE")) Then
                    If Not OrderDict("HoldOrderDate") Is Nothing AndAlso Not OrderDict("HoldOrderDate") Is DBNull.Value Then
                        Me.pnlHoldOrderDateNoEdit.Visible = True
                        lblHoldOrderDate.Text = CDateEx(OrderDict("HoldOrderDate")).Date
                    End If
                End If
                If CBoolEx(AppSettings("EXPANDIT_US_USE_ADJUSTMENT")) And Not IsLedger And IsBrowser And CStrEx(OrderDict("AdjustmentNote")) <> "" Then
                    Me.pnlAdjustmentNoEdit.Visible = True
                    Me.lblAdjustmentNote.Text = CStrEx(OrderDict("AdjustmentNote"))
                End If
            End If
            'AM2010092201 - ENTERPRISE CSR - End


            pnlAdditionalInfoNoEdit.Visible = True
            If ShowDate Then
                pnlDate.Visible = True
                lblOrderDate.Text = CDateEx(OrderDict("HeaderDate")).Date
            End If

            If ShowPaymentInfo Then
                pnlPaymentInfo.Visible = True
                'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
                If Not IsLedger Then
                    pnlPaymentType.Visible = True
                    lblPaymentTypeValue.Text = HTMLEncode(GetPaymentTypeName(CStrEx(OrderDict("PaymentType"))))
                    'JA2010102801 - PAPERCHECK - Start
                    'AM2010092201 - ENTERPRISE CSR - Start
                    If CStrEx(OrderDict("PaymentType")) = "PAPERCHECK" AndAlso CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) AndAlso csrObj.getSalesPersonGuid() <> "" AndAlso Not IsEmail Then
                        Dim sql As String = "SELECT * FROM PaperChecks WHERE OrderNumber=" & SafeString(OrderDict("CustomerReference"))
                        checks = SQL2Dicts(sql)
                        If Not checks Is Nothing AndAlso Not checks Is DBNull.Value AndAlso checks.Count > 0 Then
                            Me.pnlPaperChecks.Visible = True
                            Me.PaperChecks1.OrderDict = checks
                        End If
                    End If
                    'AM2010092201 - ENTERPRISE CSR - End
                    'JA2010102801 - PAPERCHECK - End


                    'AM2010041301 - ENTERPRISE PAYMENT GATEWAY - Start
                    If CStrEx(HTMLEncode(GetPaymentTypeName(CStrEx(OrderDict("PaymentType"))))) = "Credit Card" Then
                        Me.pnlPaymentTypeCreditCard.Visible = True

                        'JA2011030701 - PAYMENT TABLE - START
                        'Me.lblLastDigits.Text = HTMLEncode(CStrEx(OrderDict("CCLastNumbers")))
                        Me.lblLastDigits.Text = HTMLEncode(CStrEx(eis.getPaymentTableColumn(OrderDict("HeaderGuid"), "PaymentLastNumbers")))
                        'Dim sql As String = "SELECT Description FROM EECCApprovedCreditCardType WHERE GatewayCardTypeCode =" & SafeString(HTMLEncode(CStrEx(OrderDict("CCCardType"))))
                        Dim sql As String = "SELECT Description FROM EECCApprovedCreditCardType WHERE GatewayCardTypeCode =" & SafeString(HTMLEncode(CStrEx(eis.getPaymentTableColumn(OrderDict("HeaderGuid"), "PaymentCardType"))))
                        'JA2011030701 - PAYMENT TABLE - END
                        Me.lblCardType.Text = CStrEx(getSingleValueDB(sql))
                    End If
                    'AM2010041301 - ENTERPRISE PAYMENT GATEWAY - End

                End If
                'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
            End If

            'JA2011030701 - PAYMENT TABLE - START
            'If CStrEx(OrderDict("PaymentTransactionID")) <> "" Then
            '    pnlPaymentTransactionID.Visible = True
            '    lblPaymentTransactionValue.Text = HTMLEncode(OrderDict("PaymentTransactionID"))
            'End If
            If CStrEx(eis.getPaymentTableColumn(OrderDict("HeaderGuid"), "PaymentTransactionID")) <> "" Then
                pnlPaymentTransactionID.Visible = True
                lblPaymentTransactionValue.Text = HTMLEncode(eis.getPaymentTableColumn(OrderDict("HeaderGuid"), "PaymentTransactionID"))
            End If
            'JA2011030701 - PAYMENT TABLE - END

            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
            If AppSettings("ORDERRENDER_BROWSER_SHOW_ORDER_NUMBER") And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOW_ORDER_NUMBER") And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOW_ORDER_NUMBER") And IsLedger Then
                'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
                pnlOrderNumber.Visible = True
                lblCustomerReference.Text = CStrEx(OrderDict("CustomerReference"))
            End If
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
            If AppSettings("ORDERRENDER_BROWSER_SHOW_CARTCOMMENT") And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOW_CARTCOMMENT") And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOW_CARTCOMMENT") And IsLedger Then
                'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
                pnlCommentPONumber.Visible = True
                Me.pnlComment.Visible = True
                lblComment.Text = CStrEx(OrderDict("HeaderComment"))
            End If
            'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
            If AppSettings("ORDERRENDER_BROWSER_SHOW_PONUMBER") And IsBrowser Or AppSettings("ORDERRENDER_EMAIL_SHOW_PONUMBER") And IsEmail Or AppSettings("ORDERRENDER_LEDGER_SHOW_PONUMBER") And IsLedger Then
                'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End
                pnlCommentPONumber.Visible = True
                'AM2010092201 - ENTERPRISE CSR - Start
                If Not CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Or (CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And csrObj.getSalesPersonGuid() <> "" And Not IsEmail) Or (CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And csrObj.getSalesPersonGuid() = "") Then
                    Me.pnlPONumber.Visible = True
                    lblPONumber.Text = CStrEx(OrderDict("CustomerPONumber"))
                End If
                'AM2010092201 - ENTERPRISE CSR - End
            End If

            pnlCommentPONumber.Visible = True
            If Not CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Or (CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And csrObj.getSalesPersonGuid() <> "" And Not IsEmail) Or (CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And csrObj.getSalesPersonGuid() = "") Then
                Me.pnlYourName.Visible = True
                lblYourName.Text = CStrEx(OrderDict("YourName"))
                'WLB 10/12/2012 - BEGIN - Add "Your E-mail" to Additional Information
                Me.pnlYourEmail.Visible = True
                lblYourEmail.Text = CStrEx(OrderDict("YourEmail"))
                'WLB 10/12/2012 - END - Add "Your E-mail" to Additional Information
            End If


        End If

    End Sub

    'AM2010091001 - ONLINE PAYMENTS - Start

    Public Function onlinePaymentLine(ByVal Line As ExpDictionary) As String
        Dim returnLine As String = ""

        onlinePaymentsTotal += Line("PaymentTransactionAmount")
        returnLine = "<span>" & formatAmount(Line("PaymentTransactionAmount")) & "  (" & CDateEx(Line("PaymentDate")).Date & ")"
        If CStrEx(Line("DocumentGuid")) <> "" Then
            returnLine &= "  (" & CDateEx(Line("ProcessedDate")).Date & " - " & CStrEx(Line("DocumentGuid")) & ")"
        End If
        returnLine &= "</span>"
        Return returnLine
    End Function

    Public Function formatAmount(ByVal amount As Double)
        If CStrEx(OrderDict("CurrencyGuid")) <> "" Then
            Return CurrencyFormatter.FormatCurrency(amount, OrderDict("CurrencyGuid"))
        Else
            Return CurrencyFormatter.FormatCurrency(amount, CStrEx(AppSettings("MULTICURRENCY_SITE_CURRENCY")))
        End If
    End Function

    'AM2010091001 - ONLINE PAYMENTS - End

End Class
