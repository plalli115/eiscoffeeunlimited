<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MenuItems.ascx.vb" Inherits="MenuItems" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%  If CStrEx(AppSettings("MasterPageFolder")).Contains("Layout") Then%>
<div class="MenuItemsLine">
    <div class="MenuItemSeparatorFirst">
        &nbsp;</div>
    <%
        Dim cnt As Integer = 1
        For Each item As ExpandIT.MenuItem In globals.Menu
    %>
    <div class="MenuItem">
        <div class="MenuItem_<% = htmlencode(item.id) %>">
            <a class="MenuItem" href="<% = item.URL %>" title="<% = htmlencode(item.Caption) %>">
                <% =HTMLEncode(item.Caption)%>
            </a>
        </div>
    </div>
    <%  If globals.Menu.Count > cnt Then%>
    <div class="MenuItemSeparator">
        <div class="MenuItemSeparator_<% = htmlencode(item.id) %>">
            &nbsp;
        </div>
    </div>
    <% 
    End If
    cnt = cnt + 1
Next
    %>
    <div class="MenuItemSeparatorLast">
        &nbsp;</div>
</div>
<%  Else%>
<table cellspacing="0" cellpadding="0" border="0" style="height: 30px; text-align: center;
    vertical-align: middle;">
    <tbody>
        <tr>
            <%Dim cnt As Integer = 1%>
            <%For Each item As ExpandIT.MenuItem In globals.Menu%>
            <td class="menuItems">
                <a href="<% = item.URL %>" title="<% = htmlencode(item.Caption) %>">
                    <% =HTMLEncode(item.Caption)%>
                </a>
            </td>
            <%  If globals.Menu.Count > cnt Then%>
            <td class="menu_separator">
                <img id="Img1" runat="server" src="~/images/p.gif" border="0" alt="" width="1" height="31" />
            </td>
            <%End If%>
            <%cnt = cnt + 1%>
            <%Next%>
        </tr>
    </tbody>
</table>
<%  End If%>
