<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UOM.ascx.vb" Inherits="UOM" %>
<%@ Register Namespace="ExpandIT.Buttons" TagPrefix="exbtn" %>
<%@ Register Src="~/controls/AddToFavoritesButton.ascx" TagName="AddToFavoritesButton"
    TagPrefix="uc1" %>
<%@ Register Src="~/controls/AddToCartButton.ascx" TagName="AddToCartButton" TagPrefix="uc1" %>
<%--AM2011060201 - PRICES BREAKDOWN - Start--%>
<%@ Register Src="~/controls/PricesBreakDown.ascx" TagName="PricesBreakDown" TagPrefix="uc1" %>
<%--AM2011060201 - PRICES BREAKDOWN - End--%>
<!--AM2010021901 - VARIANTS - Start-->
<%@ Register Src="~/controls/USVariants/Variants.ascx" TagName="Variants" TagPrefix="uc1" %>
<%--<%  If CBoolEx(AppSettings("EXPANDIT_US_USE_VARIANTS")) Then%>
<uc1:Variants ID="Variants1" runat="server" />
<%  End If%>--%>
<!--AM2010021901 - VARIANTS - End-->
<%  If Not dictUOM Is Nothing Then%>
<%--AM2011060201 - PRICES BREAKDOWN - Start--%>
<table cellpadding="0" cellspacing="0" style="width: 265px; margin-top: 5px;">
    <tr>
        <td>
            <uc1:PricesBreakDown ID="PricesBreakDown1" runat="server" />
        </td>
    </tr> 
</table> 
<%--AM2011060201 - PRICES BREAKDOWN - End--%>
<%  If Me.DisplayMode = DisplayModes.DropDown Then%>
<table cellpadding="0" cellspacing="0" style="width: 280px; margin-top: 5px;">
    <tr style="width: 100%;">
        <td style="padding: 1px 0 0 0; vertical-align: middle; width:30px;">
            <%--<input id="getQtyDropDown<%=ProductGuid() %>" name="getQtyDropDown<%=ProductGuid() %>" type="hidden" />
            <asp:Button ID="SetQtyButtonDropDown" runat="server" Style="display: none;" Text="Button" />--%>
            <input type="hidden" id="SKU" name="SKU" value="<% Response.Write(HTMLEncode(ProductGuid())) %>" />
            <input type="text" class="dpdBorderUOM" id="Quantity_<%=ProductGuid() %>" name="Quantity"
                style="width: 20px;" tabindex="1" onchange="showPriceLineUOM('<%= ProductGuid() %>');"
                 value='<% = DefaultQty()%>' />
                 <%--onkeyup="showPriceLineUOM('<%= ProductGuid() %>');this.blur();this.focus();"--%>
        </td>
        <td style="padding: 1px 0 0 0; vertical-align: middle;">
            <% If Not dictUOM Is Nothing And dictUOM.Count > 0 Then %>
            <select id="UOM_<%=ProductGuid() %>" name="UOM" class="dpdBorderUOM" onchange="showPriceLineUOM('<%= ProductGuid() %>');"
                style="height: 17px;">
                <%  
                    For Each UOM As ExpDictionary In dictUOM.Values%>
                <option value='<%=UOM("Code") %>'>
                    <%=UOM("Description")%>
                </option>
                <% Next%>
            </select>
            <% Else %>
            <input type="hidden" name="UOM" id="UOM_<%=ProductGuid() %>" value="EACH" />
            <% End If %>
        </td>
        <td style="padding: 1px 0 0 0; vertical-align: middle;">
            <%If Not dictPriceLines Is Nothing And dictPriceLines.Count > 0 Then%>
            <%--AM2010051801 - SPECIAL ITEMS - Start--%>            
            <%If eis.CheckPageAccess("Prices") Then%>
                <%
                    Dim PriceInfo() As String = firstPriceLine().split("*")%>
                <table cellpadding="0" cellspacing="0" style="width:100%;">
                    <tr>
                        <td>
                            <input readonly="readonly" class='<% = IIF(IsSpecial(),"SpecialPrice","RegularPrice") %>' type="text" 
                                id="ItemPriceLine_<%=ProductGuid() %>_QtyUom" style="border: none;padding-left: 2px; 
                                border-color: transparent; width: 35px; text-align:right;display: none;" value='<%= PriceInfo(0) %>' /> 
                        </td>
                        <td>
                            <input readonly="readonly" class='<% = IIF(IsSpecial(),"SpecialPrice","RegularPrice") %>' type="text" 
                                id="ItemPriceLine_<%=ProductGuid() %>" style="border: none;padding-left: 2px;
                                     border-color: transparent; width: 70px; text-align:left;display: none;" value='<%= PriceInfo(1) %>' />            
                        </td>
                    </tr>
                </table>            
            <%End If %>            
            <%--AM2010051801 - SPECIAL ITEMS - End--%>
            <%End If%>
        </td>
        <td style="padding: 1px 0 0 0; vertical-align: middle;">
            <input type="hidden" id="txtPrice_<%=ProductGuid() %>" name="txtPrice" value='<%= firstPrice() %>' />
            <%If eis.CheckPageAccess("Prices") Then%>
                <table cellpadding="0" cellspacing="0" style="width:100%;">
                    <tr>
                        <td style="vertical-align:middle;">
                                <input readonly="readonly" type="text" id="TxtPrefix_<%=ProductGuid() %>" style="border: none; padding-left: 2px;
                                border-color: transparent; width:15px;" value='=<%=GetCurrencySymbol("prefix")%>&nbsp;' />
                        </td>
                        <td>
                             <%Dim showPriceDropDown As String = CurrencyFormatter.FormatCurrency(CDblEx(firstPrice()), Session("UserCurrencyGuid").ToString)%>
                             <%showPriceDropDown = showPriceDropDown.Trim(CurrencyFormatter.GetCurrentSymbol(Session("UserCurrencyGuid").ToString))%>        
                            <input type="text" class="dpdBorderUOM" id="txtCalculatedPrice_<%=ProductGuid() %>"
                                readonly="readonly" name="txtCalculatedPrice" style="width: 45px; vertical-align: middle;
                                text-align: right;display:none;" onfocus="this.form.elements[0].focus()" value='<% = IIF (DefaultQty=1,showPriceDropDown,"") %>' />
                        </td>
                        <td style="vertical-align:middle;">
                            <input readonly="readonly" type="text" id="TxtSuffix_<%=ProductGuid() %>" style="border: none; padding-left: 2px;
                                border-color: transparent; width:15px;" value='<%=GetCurrencySymbol("suffix")%>' />
                        </td>
                    </tr>
                </table>
            <%End If %>
            <%
                    If Not dictPriceLines Is Nothing And dictPriceLines.Count > 0 Then%>
            <select id="PriceLines_<%=ProductGuid() %>" name="PriceLines" style="display: none;" >
                <%  
                    For Each key As String In dictPriceLines.Keys%>
                <option value='<%=key %>'>
                    <%=dictPriceLines(key)%>
                </option>
                <% Next%>
            </select>
            <%End If%>
            <%If Not dictPrices Is Nothing Then%>
            <select id="Prices_<%=ProductGuid() %>" name="Prices" style="display: none;">
                <%  
                    For Each key As String In dictPrices.Keys%>
                <option value='<%=key %>'>
                    <%=dictPrices(key)%>
                </option>
                <% Next%>
            </select>
            <%End If%>
        </td>
    </tr>
</table>
<%  Else%><%--List--%>
<%Dim allUoms As String = ""%>
<%  For Each UOM As ExpDictionary In dictUOM.Values%>
<table cellpadding="0" cellspacing="0" style="width: 280px; margin-top: 5px;">
    <tr>
        <td style="padding: 0px; vertical-align: middle; width:30px;">
            <%allUoms = allUoms & UOM("Code") & "_"%>            
            <%--<input id="getQty<%=ProductGuid() %>" name="getQty<%=ProductGuid() %>" type="hidden" />
            <asp:Button ID="SetQtyButton" runat="server" Style="display: none;" Text="Button" />--%>
            <input type="hidden" id="Hidden1" name="SKU" value="<% Response.Write(HTMLEncode(ProductGuid())) %>" />
            <input type="text" class="dpdBorderUOM" id="Quantity_<%=ProductGuid() & "_" & UOM("Code") %>"
                name="Quantity" style="width: 20px;" tabindex="1" onchange="calculateListMode('<%=UOM("Code") %>' ,'<%= ProductGuid() %>');"
                value='<% = IIF(UOM("Code")=ProductDict("BaseUOM"),DefaultQty(),"") %>' />
                <%--onkeyup="calculateListMode('<%= UOM("Code") %>','<%= ProductGuid() %>');this.blur();this.focus();"--%>
        </td>
        <td class="UOMDescription" style="vertical-align: middle;">
            <%--AM2010051801 - SPECIAL ITEMS - Start--%>
            <table cellpadding="0" cellspacing="0" style="width:100%;">
            <tr>
                <td style="text-align:left;">
                    <label id="UOM_<%=UOM("Code") %>_Description_<%= ProductGuid() %>"  class='<% = IIF(IsSpecial(),"SpecialPrice","RegularPrice") %>'>
                        <%--<%=UOM("Description") & "(s)"  %>--%>
                        <%=UOM("Description")%>
                    </label>               
                </td>
            <%If eis.CheckPageAccess("Prices") Then%>
                <%Dim str() As String = priceLine(UOM("Code")).Split("*")%>
                
                 <td>
                    <label id="UOM_<%=UOM("Code") %>_QtyUom_<%=ProductGuid() %>" class='<% = IIF(IsSpecial(),"SpecialPrice","RegularPrice") %>'>
                        <%If CStrEx(str(0)) <> " " Then%> 
                            <%=str(0)%>
                        <%Else %> 
                            &nbsp;&nbsp;
                        <%End If %> 
                    </label>
                </td>
                
                <td >
                    <label id="UOM_<%=UOM("Code") %>_Price_<%=ProductGuid() %>" class='<% = IIF(IsSpecial(),"SpecialPrice","RegularPrice") %>'>
                        <%=str(1)%>
                    </label>
                </td> 
            <%End If %>
            </tr> 
            </table>         
            <%--AM2010051801 - SPECIAL ITEMS - End--%>
            <input type="hidden" id="UOM_<%=ProductGuid() %>" name="UOM" value="<% Response.Write(HTMLEncode(UOM("Code"))) %>" />
        </td>
        <td style="padding: 0px; vertical-align: middle;">
            <table cellpadding="0" cellspacing="0" style="width:100%;">
                <tr>
                    <td style="vertical-align: middle;">
                        <input type="hidden" id="txtPrice_<%=ProductGuid() & "_" & UOM("Code") %>" name="txtPrice"
                            value="<% Response.Write(HTMLEncode(getPriceUOM(UOM("Code")))) %>" />
                        <%If eis.CheckPageAccess("Prices") Then%>
                            <input type="text" id="TxtPrefixList_<%=ProductGuid()%>_<%=HTMLEncode(UOM("Code")) %>" style="border: none; padding-left: 2px;
                                border-color: transparent; width:15px;" value='=<%=GetCurrencySymbol("prefix")%>&nbsp;' />
                            <%--<asp:Label ID="Label1" runat="server" Text="Label">=<%=GetCurrencySymbol("prefix")%>&nbsp;</asp:Label>--%>                            
                        <%End If %>
                    </td>
                    <td>
                        <%If eis.CheckPageAccess("Prices") Then%>
                            <%Dim showPriceList As String= CurrencyFormatter.FormatCurrency(CDblEx( getPriceUOM(UOM("Code"))), Session("UserCurrencyGuid").ToString) %>
                            <%showPriceList = showPriceList.Trim(CurrencyFormatter.GetCurrentSymbol(Session("UserCurrencyGuid").ToString))%>
                            <input type="text" class="dpdBorderUOM" id="txtCalculatedPrice_<%=ProductGuid() & "_" & UOM("Code") %>"
                                readonly="readonly" name="txtCalculatedPrice" style="width: 45px; vertical-align: middle;
                                text-align: right;display:none;" onfocus="this.form.elements[0].focus()" value='<% = IIF(UOM("Code")=ProductDict("BaseUOM") And DefaultQty()=1,showPriceList,"") %>' />
                         <%End If%>
                    </td>
                    <td style="vertical-align:middle;">       
                        <%If eis.CheckPageAccess("Prices") Then%>
                            <input type="text" id="TxtSuffixList_<%=ProductGuid()%>_<%=HTMLEncode(UOM("Code")) %>" style="border: none; padding-left: 2px;
                                    border-color: transparent; width:15px;" value='<%=GetCurrencySymbol("suffix")%>' />
                                    <%--<asp:Label ID="Label3" runat="server" Text="Label"><%=GetCurrencySymbol("suffix")%></asp:Label>--%>
                        <%End If %>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<%  Next%>
<%  allUoms = allUoms.TrimEnd("_")%>
<input type="hidden" id="UOMS_<%=ProductGuid()%>" name="UOMS_<%=ProductGuid()%>" value="<%= allUoms %>" />
<%--<%  If dictUOM.Count < 3 Then%>
<%  Dim moreTables As Integer = CIntEx(3) - CIntEx(dictUOM.Count)%>
<%  For i As Integer = 1 To moreTables%>
<table cellpadding="0" cellspacing="0" style="width: 300px; height: 17px; margin-top: 5px;">
    <tr>
        <td style="padding: 0px; vertical-align: middle;" colspan="3">
            &nbsp;
        </td> 
    </tr>
</table>
<%  Next%>
<%  End If%>--%>
<%  End If%>
<table cellpadding="0" cellspacing="0" style="width: 280px; margin-top: 5px; margin-bottom: 10px;">
    <tr>
        <input type="hidden" id="AddtoCartLabel" name="AddtoCartLabel" value="<%=Resources.Language.ACTION_ADDED_TO_CART %>" />
        <%If Me.ShowFavoritesBtn And eis.CheckPageAccess("Favorites") Then%>
        <td style="padding: 0px; vertical-align: middle;">
            <%If (CBoolEx(globals.User("B2B")) Or CBoolEx(globals.User("B2C"))) Then%>
            <div style="position: static;">
                <uc1:AddToFavoritesButton ID="AddToFavoritesButton1" DisplayMode="Button" runat="server" />
            </div>
            <%Else%>
            <div class="ProductTemplate_AddToFavoritesButton" style="position: static; background-color: Transparent;">
            </div>
            <%End If%>
        </td>
        <%End If%>
        <%If Me.ShowCartBtn Then%>
        <td style="width: 20px; padding: 0px; vertical-align: middle;">
        </td>
        <td align="right" style="padding: 0px; vertical-align: middle;">
            <div style="position: static; text-decoration: none;">
                <input type="hidden" id="GroupGuidTemp" name="GroupGuidTemp" value="<%= Request("GroupGuid") %>" />
                <input id="DisplayModeUOM_<%=ProductGuid()%>" type="hidden" value="<%=Me.DisplayMode %>" />  
                <input id="DefaultPrice_<%=ProductGuid()%>" type="hidden" value="<%=product.Price %> " /> 

                <%If eis.CheckPageAccess("Cart") Then%>
                    <%If Me.DisplayMode = DisplayModes.DropDown Then%>
                        <input id="btn<%=ProductGuid() %>" type="button" class="AddButton" value="<%= Resources.Language.ACTION_ADD_TO_ORDER_PAD %>" 
                           onmouseover ="calculateDropDown('<%=ProductGuid()%>');" />
                    <%ElseIf Me.DisplayMode = DisplayModes.List Then%>
                        <input id="btn<%=ProductGuid() %>" type="button" class="AddButton" value="<%= Resources.Language.ACTION_ADD_TO_ORDER_PAD %>" 
                           onmouseover="addToCartList('<%=ProductGuid()%>');" />
                    <%End If %>                    
                    <a id="lightboxIdAddToCart" style="display:none;"   rel="lightbox" class="" href="<%=Vroot %>/App_Themes/EnterpriseBlue/p.gif">
                        <img alt="" src="<%=Vroot %>/App_Themes/EnterpriseBlue/p.gif" />
                    </a>
                <%End If %>
                <%--<asp:Button ID="lbAddToCart" CssClass="AddButton" runat="server" Text="<%$Resources: Language, ACTION_ADD_TO_ORDER_PAD %>" />--%>
                <%--<uc1:AddToCartButton ID="AddButtonUOM" runat="server" />--%>
                <%--<exbtn:ExpLinkButton ID="lbAddToCart"  runat="server" LabelText="ACTION_ADD_TO_ORDER_PAD" />--%>
            </div>
        </td>
        <%End If%>
    </tr>
</table>
<%  End If%>

<script language="javascript" type="text/javascript">

//Dropdown Mode
function calculateDropDown(guid) {
<%If eis.CheckPageAccess("Prices") And eis.CheckPageAccess("Cart") Then%>
    var qty = document.getElementById('Quantity_' + guid).value;
    var eventClick=document.getElementById('btn' + guid);
    var uom=document.getElementById('UOM_' + guid);
    var group=document.getElementById('GroupGuidTemp');
    var variant;
    var valueVariant;
    if (document.getElementById('VariantCode_' + guid) != null){
        variant = document.getElementById('VariantCode_' + guid).value;     
        if(variant != ''){
            valueVariant= variant;
        }else{
            valueVariant= '';
        }   
    }else{
        valueVariant='';
    }    
    eventClick.onclick =  function() {     
        setAddToCartDropDown(eventClick,guid,qty,uom,group,valueVariant);  
    }; 
<%End If %>
}
function showPriceLineUOM(guid){
<%If eis.CheckPageAccess("Prices") Then%>
    var qty = document.getElementById('Quantity_' + guid).value;
    var eventClick=document.getElementById('btn' + guid);
    var uom=document.getElementById('UOM_' + guid);
    var group=document.getElementById('GroupGuidTemp');
    var variant;
    var valueVariant;
    if (document.getElementById('VariantCode_' + guid) != null){
        variant = document.getElementById('VariantCode_' + guid).value;     
        if(variant != ''){
            valueVariant= variant;
        }else{
            valueVariant= '';
        }   
    }else{
        valueVariant='';
    }   
    <%If eis.CheckPageAccess("Cart") Then%> 
    eventClick.onclick =  function() {     
        setAddToCartDropDown(eventClick,guid,qty,uom,group,valueVariant);  
    }; 
    <%End If %>
    
    
    var selected = document.getElementById('UOM_' + guid).selectedIndex;
    var priceline = document.getElementById('PriceLines_' + guid).options[selected].text.split('*');
    
    document.getElementById('ItemPriceLine_' + guid).value=priceline[1];
    document.getElementById('ItemPriceLine_' + guid + '_QtyUom').value=priceline[0];
    
    
    var qtyUom;
     if (document.getElementById('ItemPriceLine_' + guid + '_QtyUom') != null){
        if(document.getElementById('ItemPriceLine_' + guid + '_QtyUom').value != ''){
            qtyUom = document.getElementById('ItemPriceLine_' + guid + '_QtyUom').value.split(' ').join('');
        }
    }    
    //Ajax
    var defaultPrice=document.getElementById('DefaultPrice_' + guid).value;
    var url= '<%=Vroot %>' + "/getPricingAjax.aspx?Price=" + defaultPrice + "&Guid=" + guid + "&minQty=" + qty + '&Currency=' + '<%=Session("UserCurrencyGuid").ToString %>' + '&Variant=' + variant + '&Uom=' + uom.value + '&qtyUom=' + qtyUom;
    var xhReq = new XMLHttpRequest();  
    xhReq.open("GET", url += (url.match(/\?/) == null ? "?" : "&") + (new Date()).getTime(), false);
    xhReq.send(null);
    var html=xhReq.responseText;
    var start=html.indexOf('<body>') + 6;
    var end=html.indexOf('</body>');
    html=html.substring(start,end);
    var array=html.split('|');
    var resultUom=array[0];
    document.getElementById('ItemPriceLine_' + guid).value= '@ ' + array[1];
    document.getElementById('ItemPriceLine_' + guid).style.display= 'block';
    document.getElementById('txtCalculatedPrice_' + guid).value= array[2];  
    document.getElementById('txtCalculatedPrice_' + guid).style.display= 'block';
    document.getElementById('ItemPriceLine_' + guid + '_QtyUom').style.display= 'block';
    document.getElementById('TxtPrefix_' + guid).style.display= 'block';
    document.getElementById('TxtSuffix_' + guid).style.display= 'block'; 
    
    if(array[3] == 'special'){
        document.getElementById('ItemPriceLine_' + guid).style.color= 'red';
        document.getElementById('txtCalculatedPrice_' + guid).style.color= 'red';
        document.getElementById('ItemPriceLine_' + guid + '_QtyUom').style.color= 'red';
        document.getElementById('TxtPrefix_' + guid).style.color= 'red';
        document.getElementById('TxtSuffix_' + guid).style.color= 'red';
    }else{
        document.getElementById('ItemPriceLine_' + guid).style.color= 'gray';
        document.getElementById('txtCalculatedPrice_' + guid).style.color= 'gray';
        document.getElementById('ItemPriceLine_' + guid + '_QtyUom').style.color= 'gray';
        document.getElementById('TxtPrefix_' + guid).style.color= 'gray';
        document.getElementById('TxtSuffix_' + guid).style.color= 'gray';
    }
    
<%End If %>
    
}

function setAddToCartDropDown(myeventClick,myguid,myqty,myuom,mygroup,myvalueVariant){
    var label=document.getElementById('AddtoCartLabel').value;
    var e;
    e=window.event;
    AddToCart('~/cart.aspx?cartaction=add&SKU=' + myguid + '&Quantity=' + myqty + '&UOM=' + myuom.value + '&VariantCode=' + myvalueVariant + '&GroupGuid=' + mygroup.value);
    new Tooltip().schedule(myeventClick,e,label);
    
    //JA0531201001 - STAY IN CATALOG - START
    <%If CIntEx(AppSettings("STAY_IN_CATALOG")) = 0 Then %>
        setTimeout('goToCart()',500);
    <%ElseIf CIntEx(AppSettings("STAY_IN_CATALOG")) = 2 Then %>
        setTimeout('showLigthBox(' + myguid + ')',300); 
        document.getElementById('bottomNav').style.display='none';
    <%End If %>
    //JA0531201001 - STAY IN CATALOG - END
    
    
    return false;
}


//List Mode *******************************************************************************************
function addToCartList(guid) {
<%If eis.CheckPageAccess("Prices") And eis.CheckPageAccess("Cart") Then%>
    var qtyUom;
    var qty;
    var uom;
    var str='';
    var variant;
    var uoms= document.getElementById('UOMS_' + guid).value;
    var uomsArr=uoms.split('_');
    var i;
    for(i=0;i<uomsArr.length;i++){
        uom=uomsArr[i];
        qty = document.getElementById('Quantity_' + guid + '_' + uom).value;        
        if(qty != ''){
            str= str + '&Quantity=' + qty + '&UOM=' + uom ;
            if (document.getElementById('VariantCode_' + guid) != null){
                variant = document.getElementById('VariantCode_' + guid).value;     
                if(variant != ''){
                    str= str + '&VariantCode=' + variant;
                }else{
                    str= str + '&VariantCode=';
                }   
            }else{
                str= str + '&VariantCode=';
            }    
        }
//        if (document.getElementById('UOM_' + uom + '_QtyUom_' + guid) != null){
//            if(document.getElementById('UOM_' + uom + '_QtyUom_' + guid).innerHTML != ''){
//                qtyUom = document.getElementById('UOM_' + uom + '_QtyUom_' + guid).innerHTML.split(' ').join('');
//                qtyUom=qtyUom.split('\n').join('');
//            }
//        } 
//        
//        
//        //Ajax
//        var defaultPrice=document.getElementById('DefaultPrice_' + guid).value;
//        var url= '<%=Vroot %>' + "/getPricingAjax.aspx?Price=" + defaultPrice + "&Guid=" + guid + "&minQty=" + qty + '&Currency=' + '<%=Session("UserCurrencyGuid").ToString %>' + '&Variant=' + variant + '&Uom=' + uom + '&qtyUom=' + qtyUom;
//        var xhReq = new XMLHttpRequest();  
//        xhReq.open("GET", url += (url.match(/\?/) == null ? "?" : "&") + (new Date()).getTime(), false);
//        xhReq.send(null);
//        var html=xhReq.responseText;
//        var start=html.indexOf('<body>') + 6;
//        var end=html.indexOf('</body>');
//        html=html.substring(start,end);
//        var array=html.split('|');
//        var resultUom=array[0];
//        document.getElementById('UOM_' + resultUom + '_Price_' + guid).innerHTML= '@ ' + array[1];
//        document.getElementById('txtCalculatedPrice_' + guid + '_' + uom).value= array[2];
//           
    }
    var eventClick=document.getElementById('btn' + guid);
    var group=document.getElementById('GroupGuidTemp');
    eventClick.onclick =  function() {     
        setAddToCartList(eventClick,guid,str,group);  
    };  
<%End If %> 
}




function calculateListMode(uom,guid) {
<%If eis.CheckPageAccess("Prices") Then%>
    var getUoms;
    if(uom == 'getUoms'){
        getUoms=document.getElementById('UOMS_' + guid).value;
    }else{
        getUoms=uom;
    }

    var getUomsArray=getUoms.split('_'); 
    var i=0;
    for(i=0;i<getUomsArray.length;i++){

        var qty = document.getElementById('Quantity_' + guid + '_' + getUomsArray[i]).value;
        var variant;
        if (document.getElementById('VariantCode_' + guid) != null){
            variant = document.getElementById('VariantCode_' + guid).value;
        }    
        var qtyUom;
        if (document.getElementById('UOM_' + getUomsArray[i] + '_QtyUom_' + guid) != null){
            if(document.getElementById('UOM_' + getUomsArray[i] + '_QtyUom_' + guid).innerHTML != ''){
                qtyUom = document.getElementById('UOM_' + getUomsArray[i] + '_QtyUom_' + guid).innerHTML.split(' ').join('');
                qtyUom=qtyUom.split('\n').join('');
            }
        }    
        
        //Ajax  
        var defaultPrice=document.getElementById('DefaultPrice_' + guid).value;  
        var url= '<%=Vroot %>' + "/getPricingAjax.aspx?Price=" + defaultPrice + "&Guid=" + guid + "&minQty=" + qty + '&Currency=' + '<%=Session("UserCurrencyGuid").ToString %>' + '&Variant=' + variant + '&Uom=' + getUomsArray[i] + '&qtyUom=' + qtyUom;
        var xhReq = new XMLHttpRequest();  
        xhReq.open("GET", url += (url.match(/\?/) == null ? "?" : "&") + (new Date()).getTime(), false);
        xhReq.send(null);
        var html=xhReq.responseText;
        var start=html.indexOf('<body>') + 6;
        var end=html.indexOf('</body>');
        html=html.substring(start,end);
        var array=html.split('|');
        var resultUom=array[0];
        document.getElementById('UOM_' + resultUom + '_Price_' + guid).innerHTML= '@ ' + array[1];
        document.getElementById('UOM_' + resultUom + '_Price_' + guid).style.display= 'block';
                
        document.getElementById('UOM_' + resultUom + '_QtyUom_' + guid).style.display= 'block';
        
        document.getElementById('txtCalculatedPrice_' + guid + '_' + getUomsArray[i]).value= array[2];
        document.getElementById('txtCalculatedPrice_' + guid + '_' + getUomsArray[i]).style.display= 'block';
                
        document.getElementById('TxtPrefixList_' + guid + '_' + getUomsArray[i]).style.display= 'block';
        document.getElementById('TxtSuffixList_' + guid + '_' + getUomsArray[i]).style.display= 'block';
        
        
        if(array[3] == 'special'){
            document.getElementById('UOM_' + resultUom + '_Price_' + guid).style.color= 'red';
            document.getElementById('UOM_' + resultUom + '_QtyUom_' + guid).style.color= 'red';
            document.getElementById('txtCalculatedPrice_' + guid + '_' + getUomsArray[i]).style.color= 'red';
            
            document.getElementById('TxtPrefixList_' + guid + '_' + getUomsArray[i]).style.color= 'red';
            document.getElementById('TxtSuffixList_' + guid + '_' + getUomsArray[i]).style.color= 'red';
            
            document.getElementById('UOM' + '_' + getUomsArray[i] + '_Description_' + guid).style.color= 'red';
        }else{
            document.getElementById('UOM_' + resultUom + '_Price_' + guid).style.color= 'gray';
            document.getElementById('UOM_' + resultUom + '_QtyUom_' + guid).style.color= 'gray';
            document.getElementById('txtCalculatedPrice_' + guid + '_' + getUomsArray[i]).style.color= 'gray';
            
            document.getElementById('TxtPrefixList_' + guid + '_' + getUomsArray[i]).style.color= 'gray';
            document.getElementById('TxtSuffixList_' + guid + '_' + getUomsArray[i]).style.color= 'gray';
            
            document.getElementById('UOM' + '_' + getUomsArray[i] + '_Description_' + guid).style.color= 'gray';
        }
        
       
    }
<%End If %>
    
}


function setAddToCartList(myeventClick,myguid,mystr,mygroup){
    var label=document.getElementById('AddtoCartLabel').value;
    var e;
    e=window.event;
    AddToCart('~/cart.aspx?cartaction=add&SKU=' + myguid + mystr + '&GroupGuid=' + mygroup.value);
    new Tooltip().schedule(myeventClick,e,label);
    
    //JA0531201001 - STAY IN CATALOG - START
    <%If CIntEx(AppSettings("STAY_IN_CATALOG")) = 0 Then %>
        setTimeout('goToCart()',500);
    <%ElseIf CIntEx(AppSettings("STAY_IN_CATALOG")) = 2 Then %>
        setTimeout('showLigthBox(' + myguid + ')',300); 
        document.getElementById('bottomNav').style.display='none';
    <%End If %>
    //JA0531201001 - STAY IN CATALOG - END
    
    
    return false;
}






</script>

