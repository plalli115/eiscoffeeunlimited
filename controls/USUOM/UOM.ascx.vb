Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Web
Imports System.Collections
Imports System.Collections.Generic


Partial Class UOM
    Inherits ExpandIT.UserControl


    Protected dictUOM As ExpDictionary
    Protected dictPriceLines As New ExpDictionary
    Protected dictPrices As New ExpDictionary
    Protected ProductDict As ExpDictionary
    Protected product As ProductClass

    Private m_productguid As String
    Private m_displaymode As DisplayModes = DisplayModes.DropDown
    Private m_showFavoritesBtn As Boolean = True
    Private m_showCartBtn As Boolean = True
    Private m_defaultQty As String = 0
    'AM2010051801 - SPECIAL ITEMS - Start
    Private m_isSpecial As Boolean = True
    'AM2010051801 - SPECIAL ITEMS - End

    Enum DisplayModes
        DropDown = 0
        List = 1
    End Enum

    'JA2010030901 - PERFORMANCE - Start
    Public Property ProductDictProperty() As ExpDictionary
        Get
            Return ProductDict
        End Get
        Set(ByVal value As ExpDictionary)
            ProductDict = value
        End Set
    End Property
    'JA2010030901 - PERFORMANCE - End

    Public Property DisplayMode() As DisplayModes
        Get
            Return m_displaymode
        End Get
        Set(ByVal value As DisplayModes)
            m_displaymode = value
        End Set
    End Property

    Public Property DefaultQty() As Integer
        Get
            Return m_defaultQty
        End Get
        Set(ByVal value As Integer)
            m_defaultQty = value
        End Set
    End Property

    Public Property ShowFavoritesBtn() As Boolean
        Get
            Return m_showFavoritesBtn
        End Get
        Set(ByVal value As Boolean)
            m_showFavoritesBtn = value
        End Set
    End Property

    Public Property ShowCartBtn() As Boolean
        Get
            Return m_showCartBtn
        End Get
        Set(ByVal value As Boolean)
            m_showCartBtn = value
        End Set
    End Property

    Public Property ProductGuid() As String
        Get
            Return CStrEx(m_productguid)
        End Get
        Set(ByVal value As String)
            m_productguid = CStrEx(value)
        End Set
    End Property

    'AM2010051801 - SPECIAL ITEMS - Start 
    Public Property IsSpecial() As Boolean
        Get
            Return CBoolEx(m_isSpecial)
        End Get
        Set(ByVal value As Boolean)
            m_isSpecial = CBoolEx(value)
        End Set
    End Property
    'AM2010051801 - SPECIAL ITEMS - End

    Public WriteOnly Property PostBackURL() As String
        Set(ByVal value As String)
            If CBoolEx(Me.ShowCartBtn) Then
                'Me.lbAddToCart.PostBackUrl = value
            End If
        End Set
    End Property

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If CStrEx(ProductGuid) <> "" Then
            'JA2010030901 - PERFORMANCE - Start
            'ProductDict = eis.CatDefaultLoadProduct(ProductGuid, True, True, True)
            'JA2010030901 - PERFORMANCE - End

            'Dim ScriptManager1 As ScriptManager = ScriptManager.GetCurrent(Me.Page)
            'ScriptManager1.RegisterAsyncPostBackControl(Me.SetQtyButton)
            'ScriptManager1.RegisterAsyncPostBackControl(Me.SetQtyButtonDropDown)



            If CStrEx(ProductGuid) <> "" Then
                ProductDict = eis.CatDefaultLoadProduct(ProductGuid, True, True, True)
                product = New ProductClass(ProductDict)
                dictUOM = populateUOM(ProductGuid)
                If CBoolEx(Me.ShowFavoritesBtn) Then
                    Me.AddToFavoritesButton1.ProductGuid = ProductGuid
                End If
                If Me.DisplayMode = DisplayModes.DropDown Then
                    Me.priceLine()
                End If
                'AM2010021901 - VARIANTS - Start
                'If CBoolEx(AppSettings("EXPANDIT_US_USE_VARIANTS")) Then
                '    Me.Variants1.ProductGuid = ProductGuid()
                '    Me.Variants1.UOMDisplayMode = DisplayMode()
                '    Me.Variants1.UOMCount = Me.dictUOM.Count
                'End If
                'AM2010021901 - VARIANTS - End
            End If

            'AM2011060201 - PRICES BREAKDOWN - Start
            Me.PricesBreakDown1.ProductGuid = ProductGuid()
            Me.PricesBreakDown1.ProductDictVariant = ProductDict("Variants")
            'AM2011060201 - PRICES BREAKDOWN - End

        End If
    End Sub



    Public Function populateUOM(ByVal productGuid As String)

        Dim SQL As String = ""
        Dim UOMDict As ExpDictionary
        Dim baseUOM As String = ProductDict("BaseUOM")
        Dim baseUOMValue As New ExpDictionary
        Dim organizedUOMDict As New ExpDictionary
        Dim smallest As Integer = -1
        Dim smallestKey As String = ""
        Dim bLoadTranslation As Boolean = False

        'DJW 8/10/2012 Eliminated Description = 'each' from item query results.
        bLoadTranslation = CBoolEx(AppSettings("USE_UOMTRANSLATION"))
        If bLoadTranslation Then
            Dim extlangguid As String = ""
            extlangguid = CStrEx(GetExternalFromIsoLanguageGuid(HttpContext.Current.Session("LanguageGuid").ToString))
            'SQL = "SELECT DISTINCT ItemUnitOfMeasure.Code,UnitOfMeasure.Description, UOMT.Translation, " & _
            '    "QtyPerUnitOfMeasure FROM ItemUnitOfMeasure INNER JOIN UnitOfMeasure ON " & _
            '    "ItemUnitOfMeasure.Code=UnitOfMeasure.Code LEFT JOIN (SELECT * FROM UnitOfMeasureTranslation" & _
            '    " WHERE LanguageGuid=" & SafeString(extlangguid) & ") AS UOMT ON UnitOfMeasure.Code=UOMT.UOM " & _
            '    "WHERE ItemNo=" & SafeString(productGuid) & _
            '    " AND Description != 'EACH' "
            '11/19/2015 - Use Item Unit of Measure Description rather than Unit Of Measure Description.
            SQL = "SELECT DISTINCT ItemUnitOfMeasure.Code,ItemUnitOfMeasure.Description, UOMT.Translation, " & _
                "QtyPerUnitOfMeasure FROM ItemUnitOfMeasure INNER JOIN UnitOfMeasure ON " & _
                "ItemUnitOfMeasure.Code=UnitOfMeasure.Code LEFT JOIN (SELECT * FROM UnitOfMeasureTranslation" & _
                " WHERE LanguageGuid=" & SafeString(extlangguid) & ") AS UOMT ON UnitOfMeasure.Code=UOMT.UOM " & _
                "WHERE ItemUnitOfMeasure.ItemNo=" & SafeString(productGuid) & _
                " AND UnitOfMeasure.Description != 'EACH' "
        Else
            SQL = "SELECT DISTINCT ItemUnitOfMeasure.Code,UnitOfMeasure.Description, UnitOfMeasure.Description" & _
                " AS Translation, QtyPerUnitOfMeasure FROM ItemUnitOfMeasure INNER JOIN UnitOfMeasure ON " & _
                "ItemUnitOfMeasure.Code=UnitOfMeasure.Code WHERE ItemNo=" & SafeString(productGuid) & _
                " AND Description != 'EACH' "
        End If

        UOMDict = SQL2Dicts(SQL, "Code")

        ' Set the UOM translation to description where no translation was found
        If bLoadTranslation Then
            For Each UOMkey As String In UOMDict.Keys
                If CStrEx(UOMDict(UOMkey)("Translation")) <> "" Then
                    UOMDict(UOMkey)("Description") = UOMDict(UOMkey)("Translation")
                End If
                UOMDict(UOMkey).Remove("Translation")
            Next
        End If

        If UOMDict.ContainsKey(baseUOM) Then
            baseUOMValue = UOMDict(baseUOM)
            UOMDict.Remove(baseUOM)
            organizedUOMDict.Add(baseUOM, baseUOMValue)
        End If

        While UOMDict.Count > 0
            For Each key As String In UOMDict.Keys
                If smallest >= 0 Then
                    If smallest > CIntEx(UOMDict(key)("QtyPerUnitOfMeasure")) Then
                        smallest = UOMDict(key)("QtyPerUnitOfMeasure")
                        smallestKey = key
                    End If
                Else
                    smallest = UOMDict(key)("QtyPerUnitOfMeasure")
                    smallestKey = key
                End If
            Next
            If smallest >= 0 Then
                organizedUOMDict.Add(smallestKey, UOMDict(smallestKey))
                UOMDict.Remove(smallestKey)
                smallestKey = ""
                smallest = -1
            End If
        End While

        Return organizedUOMDict

    End Function

    Public Sub priceLine()

        Dim line As String = ""
        Dim UOMQty As Integer = 0
        Dim sql As String = ""
        Dim price As Double = 0
        Dim UOM As String = ""

        If globals.OrderDict Is Nothing Then
            globals.OrderDict = eis.LoadOrderDictionary(globals.User)
        End If

        For Each UOM In dictUOM.Keys
            line = priceLine(UOM)
        Next

    End Sub

    Public Function priceLine(ByVal UOM As String)
        Dim line As String = ""
        Dim UOMQty As Integer = 0
        Dim sql As String = ""
        Dim price As Double = 0


        If globals.OrderDict Is Nothing Then
            globals.OrderDict = eis.LoadOrderDictionary(globals.User)
        End If

        UOMQty = CIntEx(getSingleValueDB("SELECT QtyPerUnitOfMeasure FROM ItemUnitOfMeasure WHERE ItemNo=" & SafeString(ProductGuid) & " AND Code=" & SafeString(UOM)))
        If UOMQty > 1 Then
            line = " of " & UOMQty
        End If
        line &= " *@ "

        price = getPriceUOM(UOM, UOMQty)
        If Me.DisplayMode = DisplayModes.DropDown Then
            dictPrices.Add(UOM, price)
        End If
        If UOMQty > 1 Then
            price = RoundEx(price, CLngEx(AppSettings("NUMDECIMALPLACES")))
        End If
        line &= CurrencyFormatter.FormatCurrency(CDblEx(price), Session("UserCurrencyGuid").ToString)
        If Me.DisplayMode = DisplayModes.DropDown Then
            dictPriceLines.Add(UOM, line)
        End If

        Return line
    End Function

    Public Function getPriceUOM(ByVal UOM As String, Optional ByVal UOMQty As Integer = -1)

        Dim sql As String = ""
        Dim price As Double = 0

        If UOMQty = -1 Then
            UOMQty = CIntEx(getSingleValueDB("SELECT QtyPerUnitOfMeasure FROM ItemUnitOfMeasure WHERE ItemNo=" & SafeString(ProductGuid) & " AND Code=" & SafeString(UOM)))
        End If
        If UOMQty > 0 Then
            'AM2010051801 - SPECIAL ITEMS - Start
            If IsSpecial Then
                Dim campaign As String = ""

                'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - Start
                'campaign = CStrEx(getSingleValueDB("SELECT SpecialsCampaign FROM EESetup"))
                Try
                    campaign = CStrEx(eis.getEnterpriseConfigurationValue("SpecialsCampaign"))
                Catch ex As Exception
                End Try
                'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - End


                sql = "SELECT MIN(UnitPrice) FROM Attain_ProductPrice " & _
                    "WHERE ProductGuid =" & SafeString(ProductGuid) & " AND UOMGuid=" & SafeString(UOM) & " AND " & _
                    "(CustomerRelType = 3 AND CustomerRelGuid = " & SafeString(campaign) & ") AND " & _
                    "(StartingDate <= " & SafeDatetime(DateValue(Now)) & " OR StartingDate IS NULL) AND " & _
                    "(EndingDate >= " & SafeDatetime(DateValue(Now)) & " OR EndingDate IS NULL) AND "
            Else
                sql = "SELECT MIN(UnitPrice) FROM Attain_ProductPrice " & _
                    "WHERE ProductGuid =" & SafeString(ProductGuid) & " AND UOMGuid=" & SafeString(UOM) & " AND " & _
                    "((CustomerRelType = 0 AND CustomerRelGuid = " & SafeString(globals.Context("CustomerGuid")) & ") OR " & _
                    "(CustomerRelType = 1 AND CustomerRelGuid = " & SafeString(globals.OrderDict("PriceGroupCode")) & ") OR " & _
                    "(CustomerRelType = 2)) AND " & _
                    "(StartingDate <= " & SafeDatetime(DateValue(Now)) & " OR StartingDate IS NULL) AND " & _
                    "(EndingDate >= " & SafeDatetime(DateValue(Now)) & " OR EndingDate IS NULL) AND "

            End If
            'AM2010051801 - SPECIAL ITEMS - End
            sql = sql & "(CurrencyGuid = " & SafeString(globals.OrderDict("CurrencyGuid")) & " OR CurrencyGuid IS NULL OR CurrencyGuid = '' OR CurrencyGuid = " & SafeString(globals.Context("DefaultCurrency")) & ")"

            price = CDblEx(getSingleValueDB(sql))
            If price <= 0 Then
                price = product.Price * UOMQty
            End If
        Else
            price = product.Price
        End If

        Return price

    End Function

    Public Function firstPriceLine()

        Dim line As String = ""

        For Each key As String In dictPriceLines.Keys
            line = dictPriceLines(key)
            Exit For
        Next

        Return line

    End Function

    Public Function firstPrice()

        Dim line As String = ""

        For Each key As String In dictPrices.Keys
            line = dictPrices(key)
            Exit For
        Next

        Return line

    End Function


    Protected Function GetCurrencySymbol(ByVal position As String)
        Dim symbol As String = CurrencyFormatter.GetCurrentSymbol(Session("UserCurrencyGuid").ToString)
        Dim price As String = CurrencyFormatter.FormatCurrency(CDblEx(1), Session("UserCurrencyGuid").ToString)
        Dim index As Integer = CStrEx(price).IndexOf(symbol)
        If position = "prefix" Then
            If index = 0 Then
                Return symbol
            Else
                Return ""
            End If
        Else
            If index = 5 Then
                Return symbol
            Else
                Return ""
            End If
        End If

    End Function



    'Protected Sub SetQtyButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    '    'Me.AddButtonUOM.GetUOM = Request("UOM")
    '    'If CStrEx(Request("getQty")) <> "" Then
    '    '    Me.AddButtonUOM.SetQuantity = "1"
    '    'Else
    '    '    Me.AddButtonUOM.SetQuantity = CStrEx(Request("getQty"))
    '    'End If

    '    Me.UpdatePanelUOM.Update()


    'End Sub

    'Protected Sub SetQtyButtonDropDown_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Me.UpdatePanelUOM.Update()
    'End Sub





End Class
