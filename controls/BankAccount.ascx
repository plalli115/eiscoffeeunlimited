﻿<%--JA2010022301 - New Payment Methods - Start--%>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BankAccount.ascx.vb" Inherits="controls_BankAccount" 
    EnableTheming="true" %>
<%@ Register Src="~/controls/Message.ascx" TagName="Message" TagPrefix="uc1" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>

<asp:Panel ID="BankAccountPage" runat="server" > 
  <uc1:PageHeader ID="PageHeader1" runat="server" Text="<%$ Resources: Language, LABEL_GIROINFO %>"
        EnableTheming="true" />
  <p></p>      
  <uc1:Message ID="Message1" runat="server" />      
  <table border="0" cellspacing="0" width="100%"> 
    <tr>
      <td>
        <div class="userNewText">
          <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="LocalValidators" CssClass="PanelErrors" ForeColor="Black" />
          <div style="color:#FF0000; margin:20px; font-size: 12px;">
          <h3><asp:Label ID="accInfoLabel" runat="server" Text="<%$Resources: Language,LABEL_BANK_INFORMATION %>"></asp:Label></h3>
          </div>
          <table border="0" cellspacing="0" width="100%"  >
            <tr class="TR1">
              <td>&nbsp;</td>
              <td  style="width:70%"></td>
            </tr>     
            <tr class="TR1">
              <td class="TDR">
                <asp:Label ID="accNrLabel" runat="server" Text="<%$Resources: Language,LABEL_ACCOUNT_NR %>"></asp:Label>
              </td>
              <td class="TDL" >
                <asp:TextBox ID="accountNrTextBox" runat="server" CssClass="borderTextBox" Width="100%" 
                  MaxLength="34"></asp:TextBox>
              </td>
              <td>          
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                  ControlToValidate="accountNrTextBox" Display="Dynamic" 
                  ErrorMessage="<%$Resources: Language,LABEL_STARMARKED_INPUT_IS_REQUIRED %>" 
                  ValidationGroup="LocalValidators">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                  ControlToValidate="accountNrTextBox" Display="Dynamic" 
                  ErrorMessage="<%$Resources: Language,MESSAGE_ACCOUNT_NUM_NOT_VALID %>" 
                  ValidationExpression="^(\d\s*)+$|^[A-Z]{2}\d{2}(\s*\w\s*)+$" 
                  ValidationGroup="LocalValidators">*</asp:RegularExpressionValidator>
              </td>
            </tr>      
            <tr class="TR1">
              <td class="TDR">
                <asp:Label ID="bankCodeLabel" runat="server" Text="<%$Resources: Language,LABEL_BANK_CODE %>"></asp:Label>
              </td>
              <td class="TDL">
                <asp:TextBox ID="bankCodeTextBox" runat="server" CssClass="borderTextBox" 
                  MaxLength="11"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                  ControlToValidate="bankCodeTextBox" Display="Dynamic" 
                  ErrorMessage="<%$Resources: Language,LABEL_STARMARKED_INPUT_IS_REQUIRED %>" 
                  ValidationGroup="LocalValidators">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                  ControlToValidate="bankCodeTextBox" Display="Dynamic" 
                  ErrorMessage="<%$Resources: Language,MESSAGE_BANKCODE_NOT_VALID  %>" 
                  ValidationExpression="^(\d\s*){5,10}$|^[A-Z]{6}\w{2,5}$" 
                  ValidationGroup="LocalValidators">*</asp:RegularExpressionValidator>
              </td>
              <td>
                &nbsp;</td>
            </tr>
            <tr class="TR1">
              <td class="TDR">
                <asp:Label ID="accOwnerLabel" runat="server" Text="<%$Resources: Language,LABEL_ACCOUNT_OWNER %>"></asp:Label>
              </td>
              <td class="TDL">
                <asp:TextBox ID="accountOwnerTextBox" runat="server" CssClass="borderTextBox" Width="100%" 
                  MaxLength="60"></asp:TextBox>
              </td>
              <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                  ControlToValidate="accountOwnerTextBox" Display="Dynamic" 
                  ErrorMessage="<%$Resources: Language,LABEL_STARMARKED_INPUT_IS_REQUIRED %>" 
                  ValidationGroup="LocalValidators">*</asp:RequiredFieldValidator>
              </td>
            </tr>
            <tr class="TR1">
              <td>&nbsp;
              </td>
            </tr>            
          </table> 
        </div>  
      </td>
    </tr>
    <tr>
      <td>
        <div style="text-align:right">
         <asp:Button ID="submitBtn" runat="server" CssClass="BtnC"  Text="Next" 
            ValidationGroup="LocalValidators" />
        </div>
      </td>      
    </tr> 
  </table>
  
  
    
    
  
</asp:Panel>
<%--JA2010022301 - New Payment Methods - End--%>