<%--AM2010051801 - SPECIAL ITEMS - Start--%>
<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Specials.ascx.vb" Inherits="Specials" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<asp:DataList ID="dlProducts" HorizontalAlign="Center"  CssClass="SpecialsBorder" RepeatDirection="Horizontal"
    runat="server">
    <HeaderTemplate>
        <table cellspacing="0" cellpadding="0">
            <tr>
                <td class="SpecialsHeader" style="padding-left:5px;">
                    <%=Resources.Language.LABEL_SPECIALS%>
                </td>
            </tr>
        </table>
    </HeaderTemplate>
    <ItemTemplate>
        <table style="width: 250px; margin-bottom: 20px; text-align: center;" cellpadding="0"
            cellspacing="0">
            <tr>
                <td class="SpecialsItemStyle">
                    <span><a href="<%# getLink(Eval("value")("ProductGuid")) %>">
                        <%#Eval("value")("ProductName")%>
                    </a></span>
                </td>
            </tr>
            <tr align="Center">
                <td>
                    <a style="text-decoration: none;" href="<%# getLink(Eval("value")("ProductGuid"))%>">
                        <img alt="" style="<%# getStyle(Eval("value")("PICTURE2")) %>" src=" <%# getSrc(Eval("value")("PICTURE2"))  %>" />
                    </a>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 100%;">
                        <tr>
                            <td class="SpecialsBoxPrice">
                                <del id="SpecialsPriceDEL" class="RegularPrice">
                                    <asp:Label runat="server" CssClass="SpecialsBoxPrice" Visible='<%# eis.CheckPageAccess("Prices") %>' ID="lblRegularPrice" Text='<%# Eval("value")("originalListPrice")%>'></asp:Label>
                                </del>
                            </td>
                            <td class="SpecialsBoxPrice" style="text-align: right;">
                                <asp:Label runat="server" CssClass="SpecialPrice" Visible='<%# eis.CheckPageAccess("Prices") %>' ID="lblSpecialPrice" Text='<%# Eval("value")("ListPrice")%>'></asp:Label>
                            </td> 
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </ItemTemplate>
    <SeparatorTemplate>
        <img id="Img1" runat="server" src="~/App_Themes/EnterpriseBlue/deviderSpecials.PNG"
            alt="" />
    </SeparatorTemplate>
    <ItemStyle HorizontalAlign="Center" />
    <SeparatorStyle HorizontalAlign="Center" VerticalAlign="Middle" />
</asp:DataList>
<%--AM2010051801 - SPECIAL ITEMS - End--%>
