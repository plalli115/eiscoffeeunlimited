<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LanguageDropDown.ascx.vb"
    Inherits="LanguageDropDown" %>
<div class="LanguageDropDown">
    <div class="dpdBorder">
        <asp:DropDownList ID="dp1" runat="server" CssClass="borderDropDownList" DataTextField="LanguageName"
            DataValueField="LanguageGuid" AutoPostBack="true" />
    </div>
</div>
