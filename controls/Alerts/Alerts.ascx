﻿<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Alerts.ascx.vb" Inherits="Alerts" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Register TagPrefix="uc1" TagName="Box" Src="~/controls/Box.ascx" %>
<!--JA2010060101 - ALERTS - Start-->
<asp:Panel ID="Panel1" runat="server">
    <%  If Not sPath.ToUpper.Contains("ALERTSPAGE") Then%>
    <%  If Not dictAlerts Is Nothing AndAlso (dictAlerts.Count > 0 And m_show) Then%>
    
    <%
        If dictAlerts("ALTEST") Is Nothing Then%>
    
    <uc1:Box ID="BoxAlerts" runat="server" IdPrefix="AlertsBox">
        <headertemplate>        
        <asp:HyperLink ID="HyperLink1" runat="server"><%=Resources.Language.LABEL_ALERTS%></asp:HyperLink>
        <%--(<%=dictAlerts.Count  %>)--%>
    </headertemplate>
    </uc1:Box>
    <%  End If%>
    <%  End If%>
    <%  End If%>
</asp:Panel>
<asp:Panel ID="AlertsPanel" runat="server" Style="padding: 10px 25px 5px;">
    <%If Not dictAlerts Is Nothing And Not dictAlerts Is DBNull.Value Then%>
    <%If dictAlerts.Count > 0 Then%>
    <link rel="stylesheet" href="<%=Vroot %>/App_Themes/EnterpriseBlue/lightbox.css"
        type="text/css" media="screen" />
    <input id="myVroot" value="<%= Vroot %>" type="hidden" />
    <%= getAlerts() %>
   

    <script type="text/javascript" src="<%=Vroot %>/script/Alerts/prototype.js"></script>

    <script type="text/javascript" src="<%=Vroot %>/script/Alerts/scriptaculous.js?load=effects,builder"></script>

    <script type="text/javascript" src="<%=Vroot %>/script/Alerts/lightbox.js"></script>

    <script type="text/javascript" src="<%=Vroot %>/script/Alerts/builder.js"></script>

    <script type="text/javascript">
                function loadScript(){
                    setTimeout('myLightbox.start(document.getElementById(\'lightboxId\'))', 1000);
                }
    </script>

    <%End If%>
    <%End If%>
    <!--JA2010060101 - ALERTS - End-->
</asp:Panel>
