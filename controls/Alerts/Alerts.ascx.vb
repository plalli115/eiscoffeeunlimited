'<!--JA2010060101 - ALERTS - Start-->
Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

Partial Class Alerts
    Inherits ExpandIT.UserControl

    Protected m_show As Boolean
    Protected dictAlerts As ExpDictionary
    Protected sPath As String = ""

    Public Property ShowShop() As Boolean
        Get
            Return m_show
        End Get
        Set(ByVal value As Boolean)
            m_show = value
        End Set
    End Property

    Public Property AlertsDict() As ExpDictionary
        Get
            Return dictAlerts
        End Get
        Set(ByVal value As ExpDictionary)
            dictAlerts = value
        End Set
    End Property


    Protected Overloads Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        sPath = Me.Page.ToString()
        If m_show Then
            Me.AlertsPanel.Style.Add("display", "Block")
        Else
            Me.AlertsPanel.Style.Add("display", "none")
        End If
    End Sub


    Protected Function getAlerts()
        Dim sqlPropGrp As String = ""
        Dim groupsProperties As New ExpDictionary
        Dim groupsPrdFinal As New ExpDictionary
        Dim sortedproducts As New System.Collections.Generic.SortedDictionary(Of String, Object)

        If Not dictAlerts Is Nothing And Not dictAlerts Is DBNull.Value Then
            If dictAlerts.Count > 0 Then
                'Load the information about the groups
                For Each item As ExpDictionary In dictAlerts.Values
                    Dim groupsPrd As New ExpDictionary
                    sqlPropGrp = "SELECT PropOwnerRefGuid, PropGuid, PropLangGuid, PropTransText FROM PropVal INNER JOIN PropTrans" & _
                                           " ON PropVal.PropValGuid=PropTrans.PropValGuid" & _
                                           " WHERE PropOwnerTypeGuid='ALR'" & _
                                           " AND (PropLangGuid IS NULL OR PropLangGuid =" & SafeString(HttpContext.Current.Session("LanguageGuid").ToString) & ")" & _
                                           " AND (PropGuid='ALERT_IMAGE' OR PropGuid='ALERT_DESCRIPTION' OR PropGuid='ALERT_HTML')" & _
                                           " AND PropOwnerRefGuid=" & SafeString(item("AlertNo"))
                    groupsProperties = SQL2Dicts(sqlPropGrp)
                    If Not groupsProperties Is Nothing Then
                        For Each item2 As ExpDictionary In groupsProperties.Values
                            groupsPrd.Add(item2("PropGuid"), item2("PropTransText"))
                        Next
                        groupsPrd.Add("AlertNo", item("AlertNo"))
                        groupsPrdFinal.Add(item("AlertNo"), groupsPrd)
                    End If
                Next
                ' Sort the list of favorites
                'If Not groupsPrdFinal Is Nothing And Not groupsPrdFinal Is DBNull.Value Then
                '    For Each groups2 As ExpDictionary In groupsPrdFinal.Values
                '        sortedproducts.Add(groups2("AlertNo"), groups2)
                '    Next
                'End If
            End If
        End If


        Dim str As String = ""
        Dim item3
        Dim link As String = ""
        Dim cont As Integer = 0

        If Not groupsPrdFinal Is Nothing Then
            If groupsPrdFinal.Count = 1 Then
                link = "rel=""lightbox"" "
            Else
                link = "rel=""lightboxId[alerts]"" "
            End If
            For Each item3 In groupsPrdFinal.Values
                '<!--JM2010061001 - LINK TO - Start-->
                Dim newHtml As String = ""
                newHtml = eis.findLinkToHtml(HTMLEncode(item3("ALERT_HTML")))
                '<!--JM2010061001 - LINK TO - End-->

                If CStrEx(item3("AlertNo")) <> "ALTEST" Then
                cont = cont + 1
                str = str & "<a style=""text-decoration:none; cursor:pointer; """ & link & IIf(cont = 1, "id=""lightboxId""", "") & "  class=""" & newHtml & """ href=""" & VRoot & "/" & item3("ALERT_IMAGE") & """ >"
                str = str & "<table style=""margin-bottom: 10px;"" cellpadding=""0"" cellspacing=""0"" width=""100%"">"
                str = str & "<tr><td align=""center"" style=""vertical-align:middle; width:20%; "" > "
                str = str & "<img style=""border:0px;"" alt="""" src=""" & VRoot & "/" & item3("ALERT_IMAGE") & """ />"
                str = str & "</td><td align=""left"" style=""vertical-align:middle;padding-left: 5px; color:gray;"">"
                str = str & item3("ALERT_DESCRIPTION")
                str = str & "</td></tr></table></a>"
                End If
            Next
        End If

        Return str

    End Function

End Class
'<!--JA2010060101 - ALERTS - End-->