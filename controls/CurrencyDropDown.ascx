<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CurrencyDropDown.ascx.vb"
    Inherits="CurrencyDropDown" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<div class="CurrencyDropDown">
   
        <asp:DropDownList ID="ddlCurrency" runat="server"  AutoPostBack="True"
            DataSourceID="ObjectDataSource1" DataValueField="CurrencyCode" DataTextField="CurrencyName" OnDataBound="OnBound">
        </asp:DropDownList>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"
            SelectMethod="CurrencyTable" TypeName="ExpandIT.CurrencyControlLogic" OnObjectCreating="OnCreate"></asp:ObjectDataSource>

</div>
