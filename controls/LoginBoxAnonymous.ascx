<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LoginBoxAnonymous.ascx.vb"
    Inherits="LoginBoxAnonymous" %>
<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Register Namespace="ExpandIT.Buttons" TagPrefix="exbtn" %>
<asp:Panel ID="panel1" runat="server" DefaultButton="lbLogIn">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="2">
                <%= userIdText() %>
            </td>
        </tr>
        <tr>
            <td style="height: 7px;">
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <%= PasswordText() %>
            </td>
        </tr>
        <tr>
            <td style="height: 3px;">
            </td>
        </tr>
        <tr>
            <td <% If not eis.CheckPageAccess("OpenSite") Then Response.write("Style=""text-align:left;padding-left:10px;""") %> >
                <exbtn:ExpLinkButton ID="lbLogIn" Style="text-decoration: none;" runat="server" LabelText="LABEL_MENU_LOGIN" />
            </td>
            <td>
                <% If eis.CheckPageAccess("OpenSite") Then%>
                <exbtn:ExpLinkButton ID="lbNewAccount" Style="text-decoration: none;" runat="server"
                    LabelText="MENU_SIGNUP" PostBackUrl="~/user_new.aspx" />
                <%End If %>
            </td>
        </tr>
        <tr>
            <td style="height: 7px;">
            </td>
        </tr>
        <tr>
            <td colspan="2" align="left" style="padding-left: 10px;">
                <exbtn:ExpLinkButton ID="ExpLinkButton1" Style="text-decoration: none;" runat="server"
                    Text="Forgot my Password" PostBackUrl="~/user_typelost_password.aspx" />
            </td>
        </tr>
    </table>
    <%-- <%=Resources.Language.LABEL_USERDATA_LOGIN%>
    <br />
    <asp:TextBox ID="login" CssClass="borderTextBox" runat="server"></asp:TextBox><br />
    <%=Resources.Language.LABEL_USERDATA_PASSWORD%>
    <br />
    <asp:TextBox TextMode="Password" CssClass="borderTextBox" ID="password" runat="server"></asp:TextBox><br />--%>
</asp:Panel>
