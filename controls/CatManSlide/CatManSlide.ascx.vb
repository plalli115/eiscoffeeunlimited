'<!--JA2010031101 - CATMAN SLIDE - Start-->
Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

Partial Class CatManSlide
    Inherits ExpandIT.UserControl

    Private m_show As Boolean
    Protected infohtml As String
    Protected str As String = ""
    Protected newHtml As String = ""

    Public Property ShowSlides() As Boolean
        Get
            Return m_show
        End Get
        Set(ByVal value As Boolean)
            m_show = value
        End Set
    End Property

    Public Property SlideImages() As String
        Get
            Return str
        End Get
        Set(ByVal value As String)
            str = value
        End Set
    End Property

    Protected Overloads Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender

        If str = "" Then
            Me.Slides.Visible = False
            Me.NoSlides.Visible = True
            setDefaultHtml()
        Else
            If m_show Then
                Me.Slides.Visible = True
            Else
                Me.Slides.Visible = False
                Me.NoSlides.Visible = True
                setDefaultHtml()
            End If
        End If

    End Sub

    Public Sub setDefaultHtml()
        Dim InfoLanguage As String
        InfoLanguage = UCase(CStrEx(globals.User("LanguageGuid")))

        Select Case InfoLanguage
            Case "DA", "EN", "ES", "DE", "PT", "SV", "FR"
                infohtml = ReadTextFile(ApplicationRoot() & "\controls\CatManSlide\CatManSlide_" & LCase(InfoLanguage) & ".htm")
            Case Else
                infohtml = ReadTextFile(ApplicationRoot() & "\controls\CatManSlide\CatManSlide_en.htm")
        End Select

        '<!--JM2010061001 - LINK TO - Start-->
        newHtml = eis.findLinkToHtml(infohtml)
        '<!--JM2010061001 - LINK TO - End-->
    End Sub

    
End Class
'<!--JA2010031101 - CATMAN SLIDE - End-->