<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CatManSlide.ascx.vb"
    Inherits="CatManSlide" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<asp:Panel ID="Slides" runat="server" style="padding-bottom:20px;" Visible="false" >
    <table cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            <td align="center">
                <div id="bannerContainer">
                    <%=str%>
                    <div style="bottom: -20px; z-index: 2500;" id="thumbnailContainer">
                        <div id="thumbnailSubContainer">
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</asp:Panel>

<asp:Panel ID="NoSlides" runat="server" style="padding-bottom:20px;" Visible="false" >
    <%=newHtml %>
</asp:Panel>