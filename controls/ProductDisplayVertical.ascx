<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ProductDisplayVertical.ascx.vb"
    Inherits="ProductDisplayVertical" %>
    <%@ Import Namespace="ExpandIT" %>
    <%@ Import Namespace="ExpandIT.EISClass" %>
<asp:Panel ID="Panel1" runat="server" SkinID="ProductDisplayVertical" CssClass="ProductDisplayVerticalPanel">
    <asp:FormView ID="fvProduct" runat="server" SkinID="ProductDisplayVertical" Width="100%"
        CssClass="ProductDisplayVerticalFormView" CellPadding="0">
        <ItemTemplate> 
            <div class="ProductDisplayVertical" onclick="document.location='<% = m_productclass.ProductLinkURL %>';">
                <div class="ProductDisplayVerticalImage">
                    <asp:HyperLink ID="HyperLink1" runat="server" CssClass="ProductDisplayVerticalImage"
                        NavigateUrl='<%# Eval("ProductLinkURL") %>'>
                        <asp:Image ID="Image1" runat="server" CssClass="ProductDisplayVerticalImage" AlternateText='<%# Eval("Name") %>'
                            ImageUrl='<%# Eval("ThumbnailURL") %>' /> 
                    </asp:HyperLink>
                </div>
                <div class="ProductDisplayVerticalProductName">
                    <asp:HyperLink ID="HyperLink2" runat="server" CssClass="ProductDisplayVerticalProductName"
                        NavigateUrl='<%# Eval("ProductLinkURL") %>'><%# Eval("Name") %></asp:HyperLink>
                </div>
                <div class="ProductDisplayVerticalPrices">
                    <div class="ProductDisplayVerticalProductPrice">
                        <asp:HyperLink ID="HyperLink3" runat="server" CssClass="ProductDisplayVerticalProductPrice"
                            NavigateUrl='<%# Eval("ProductLinkURL") %>'><%#CurrencyFormatter.FormatCurrency(Eval("Price"), Session("UserCurrencyGuid").ToString)%></asp:HyperLink>
                    </div>
                </div>
            </div>
        </ItemTemplate>
    </asp:FormView>
</asp:Panel>


