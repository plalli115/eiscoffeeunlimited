Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

Partial Class ProductListVertical
    Inherits ExpandIT.UserControl

    Private m_products As ExpDictionary = Nothing
    Private m_maxitems As Integer = 0
    Private m_filtermethod As ExpDictionary.FilterMethods = ExpDictionary.FilterMethods.Top
    Private m_heading As String = ""
    Private m_sql As String = Nothing
    Private m_cachekey As String = ""
    Private m_selected As ExpDictionary

    Public Property SelectStatement() As String
        Get
            Return m_sql
        End Get
        Set(ByVal value As String)
            m_sql = value
        End Set
    End Property

    Public Property CacheKey() As String
        Get
            Return m_cachekey
        End Get
        Set(ByVal value As String)
            m_cachekey = value
        End Set
    End Property

    <Themeable(True)> _
    Public Property MaxItems() As Integer
        Get
            Return m_maxitems
        End Get
        Set(ByVal value As Integer)
            m_maxitems = value
        End Set
    End Property

    <Themeable(True)> _
    Public Property FilterMethod() As ExpDictionary.FilterMethods
        Get
            Return m_filtermethod
        End Get
        Set(ByVal value As ExpDictionary.FilterMethods)
            m_filtermethod = value
        End Set
    End Property

    <Themeable(True)> _
    Public Property Heading() As String
        Get
            Return m_heading
        End Get
        Set(ByVal value As String)
            m_heading = value
        End Set
    End Property

    Public Property Products() As ExpDictionary
        Get
            Return m_products
        End Get
        Set(ByVal value As ExpDictionary)
            m_products = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If m_sql <> "" Then
            If m_cachekey <> "" Then m_products = CacheGet(m_cachekey & m_sql)
            If m_products Is Nothing Then
                m_products = Sql2Dictionaries(m_sql, New String() {"ProductGuid"})
                eis.CatDefaultLoadProducts(m_products, False, False, True, New String() {"ProductName"}, New String() {"PICTURE2"}, Nothing)
                If m_cachekey <> "" Then CacheSetAggregated(m_cachekey & m_sql, m_products, Split(AppSettings("GroupProductRelatedTablesForCaching"), "|"))
            End If
        End If

        m_selected = ExpDictionary.FilterDict(m_products, m_maxitems, m_filtermethod)

        Dim dl As DataList
        dl = FindChildControl(Me, "dlProductsListVertical1")
        dl.DataSource = m_selected
        dl.DataBind()
    End Sub

    Protected Sub Page_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DataBinding
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim lbl As Label = FindChildControl(Me, "lblHeading")
        lbl.Text = m_heading
    End Sub

End Class
