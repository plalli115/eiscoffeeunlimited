'AM2010051902 - PRODUCT AVAILABILITY - Start
Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Web
Imports System.Collections
Imports System.Collections.Generic

Partial Class ProductAvailability
    Inherits ExpandIT.UserControl

    Private m_QtyOnHand As Double
    Private m_displaymode As DisplayModes = DisplayModes.QtyOnHand

    Enum DisplayModes
        QtyOnHand = 0
        Stock = 1
    End Enum

    Public Property DisplayMode() As DisplayModes
        Get
            Return m_displaymode
        End Get
        Set(ByVal value As DisplayModes)
            m_displaymode = value
        End Set
    End Property

    Public Property QtyOnHand() As Double
        Get
            Return m_QtyOnHand
        End Get
        Set(ByVal value As Double)
            m_QtyOnHand = value
        End Set
    End Property

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If DisplayMode = DisplayModes.QtyOnHand Then
            Me.pnlStock.Visible = False
            Me.pnlQtyOnHand.Visible = True
            Me.lblQtyOnHand.Text = QtyOnHand
        Else
            Me.pnlQtyOnHand.Visible = False
            Me.pnlStock.Visible = True
            If QtyOnHand > 0 Then
                Me.lblStock.Text = Resources.Language.LABEL_IN_STOCK
                Me.lblStock.CssClass = "InStock"
            Else
                Me.lblStock.Text = Resources.Language.LABEL_OUT_OF_STOCK
                Me.lblStock.ForeColor = Drawing.Color.Red
            End If
        End If
    End Sub

End Class
'AM2010051902 - PRODUCT AVAILABILITY - End
