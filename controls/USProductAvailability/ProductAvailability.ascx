<%--AM2010051902 - PRODUCT AVAILABILITY - Start--%>
<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ProductAvailability.ascx.vb"
    Inherits="ProductAvailability" %>
<asp:Panel runat="server" Visible="false" ID="pnlQtyOnHand">
    <table cellpadding="0" cellspacing="0" style="margin-bottom:5px;">
        <tr>
            <td style="vertical-align: middle;">
                <asp:Label ID="Label22" Style="font-weight: bold;" runat="server" Text="<%$Resources: Language,LABEL_QUANTITY_ON_HAND %>"></asp:Label>:
            </td>
            <td style="text-align: center; padding-left:5px;">
                <asp:Label ID="lblQtyOnHand" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel runat="server" Visible="false" ID="pnlStock">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="2">
                <asp:Label ID="lblStock" Style="font-weight: bold;" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Panel>
<%--AM2010051902 - PRODUCT AVAILABILITY - End--%>
