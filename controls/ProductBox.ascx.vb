Imports ExpandIT
Imports ExpandIT.EISClass

Partial Class ProductBox
    Inherits ExpandIT.UserControl

    Private m_product As ExpDictionary

    Public Property Product() As ExpDictionary
        Get
            Return m_product
        End Get
        Set(ByVal value As ExpDictionary)
            m_product = value
        End Set
    End Property

    Public ReadOnly Property ThumbnailURL() As String
        Get
            Return IIf(m_product("PICTURE2") <> "", PictureLink(m_product("PICTURE2")), PictureLink("images/nopix75.jpg"))
        End Get
    End Property

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        btnAdd.PostBackUrl = "~/cart.aspx?cartaction=add&SKU=" & m_product("ProductGuid") & "&Quantity=1"
    End Sub
End Class
