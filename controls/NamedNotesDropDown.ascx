﻿<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="NamedNotesDropDown.ascx.vb" Inherits="NamedNotesDropDown" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<div class="NamedNotesDropDown">
    <div class="dpdBorder">
        <asp:DropDownList ID="ddlNamedNotes" runat="server" CssClass="borderDropDownList" AutoPostBack="true">
        </asp:DropDownList>
    </div>
</div>