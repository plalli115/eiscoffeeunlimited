<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="GroupHeader.ascx.vb"
    Inherits="GroupHeader" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Register Src="~/controls/GroupHeaderSubGroups.ascx" TagName="GroupHeaderSubGroups" TagPrefix="uc1" %>
<div class="GroupHeader0" style="margin-bottom: 10px;">
    <table style="width: 100%;">
        <tr>
            <td style="text-align: center;">
                <% If Not Group("PICTURE1") = "" Then%>
                <asp:Image ID="Picture1" runat="server" />
                <% End If%>
            </td>
        </tr>
    </table>
    <div class="GroupHeaderHTMLDescription">
        <asp:Literal ID="litHTMLDescription" runat="server" Text="Label"></asp:Literal>
    </div>
    <br />
    <br />
    <%If Not Group("Subgroups") Is Nothing Then%>
        <%If CIntEx(Group("HIDESUBGROUPS")) = 0 Then%>
            <asp:DataList ID="dlProducts" SkinID="GroupTemplate" runat="server"  ItemStyle-Width="300px"
                RepeatColumns="2" RepeatDirection="Horizontal" CssClass="GroupProductList" ItemStyle-CssClass="GroupProductListItem">
                <ItemTemplate>
                    <uc1:GroupHeaderSubGroups ID="GroupHeaderSubGroups1" SubGroup='<%# Eval("Value") %>' runat="server"/>
                </ItemTemplate>
            </asp:DataList>
        <%End If %>
    <%End If %>
</div>
