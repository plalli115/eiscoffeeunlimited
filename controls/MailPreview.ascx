﻿<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MailPreview.ascx.vb" Inherits="MailPreview" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>

<div class="MailPreview">
  <div style="margin-left: 15px; margin-right: 15px; margin-bottom: 15px;">
  <asp:Label ID="Label1" runat="server" Text="<%$Resources: Language,LABEL_NOTE_EMAIL_PREVIEW %>"></asp:Label>
  </div>
  <asp:Panel ID="PanelPreview" runat="server">
    <div align="center">
    
        <table class="tables">
        <tr>
        <td width="80" align="right">
        <b>
        <asp:Label ID="toLabel" runat="server" Text="<%$Resources: Language,LABEL_MAIL_ADDRESSEE %>"></asp:Label>
        <br />&nbsp;
        </b>
        </td>
        <td align="left">
        <asp:TextBox ID="toTextBox" runat="server" Width="350"></asp:TextBox>           
        <asp:RequiredFieldValidator ID="toRequiredValidator" runat="server" Text="*"
        ErrorMessage="<%$Resources: Language,MESSAGE_REQUIRED_FIELD %>" ControlToValidate="toTextBox" 
        SetFocusOnError="True" Display="Dynamic" ValidationGroup="AllValidators" />
        <asp:RegularExpressionValidator ID="toRegularExpressionValidator" runat="server" 
        ErrorMessage="<%$Resources: Language,MESSAGE_EMAIL_ADDRESS_NOT_VALID %>" 
        ControlToValidate="toTextBox" Text="*" Display="Dynamic" 
        ValidationGroup="AllValidators" 
        ValidationExpression="^((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*))$|^((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)[,;]\s+)((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)[,;]\s+)*((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*))$"/>
        <br />
        <% =Resources.Language.LABEL_VARIOUS_RECEIVERS%>
        </td>
        </tr>
        </table>
        
        <table class="tables">
        <tr>
        <td width="80" align="right">
        <b>
        <asp:Label ID="subjLabel" runat="server" Text="<%$Resources: Language,LABEL_MAIL_SUBJ %>"></asp:Label>
        </b>
        </td>
        <td align="left">
        <asp:TextBox ID="subjTextBox" runat="server" Width="350"></asp:TextBox>
        </td>
        </tr>
        </table>
          
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="AllValidators" />            
    
      <iframe class="contentFrame" src="_NoteMailPreview.aspx" style="height: 315px; width: 541px; margin-top: 10px;"></iframe> 
      
        <table class="tables">
        <tr>
        <td align="left">
        <img id="Img1" src="~/App_Themes/Theme3/mailpfeil.gif" runat="server" height="40" width="38" align="top" /> 
        </td>
        <td align="right">
        <asp:Button ID="sendnote" runat="server"
        Text="<%$Resources: Language,ACTION_SEND %>" 
        ValidationGroup="AllValidators" />
        </td>
        </tr>
        </table>
    </div>
  </asp:Panel>
</div>
