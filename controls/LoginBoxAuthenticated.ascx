<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LoginBoxAuthenticated.ascx.vb"
    Inherits="LoginBoxAuthenticated" %>
<%@ Register Namespace="ExpandIT.Buttons" TagPrefix="exbtn" %>
<asp:LinkButton ID="lbLoginStatus" runat="server">You are logged in as John Doe</asp:LinkButton>
<p />
<exbtn:ExpLinkButton ID="lbLogOut" runat="server" LabelText="LABEL_MENU_LOGOUT" PostBackUrl="~/_user_logout.aspx" />