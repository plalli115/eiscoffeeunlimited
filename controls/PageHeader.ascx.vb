﻿Imports ExpandIT
Imports ExpandIT.ExpandITLib
Imports ExpandIT.EISClass

Partial Class controls_PageHeader
    Inherits ExpandIT.UserControl

    Private m_text As String = ""

    Public Property Text() As String
        Get
            Return m_text
        End Get
        Set(ByVal value As String)
            m_text = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        LabelHeader.Text = m_text

        ' Add text to page title
        If Not IsPostBack Then
            If LabelHeader.Text <> Page.Title Then
                Page.Title = LabelHeader.Text & " - " & Page.Title
            End If
        End If

    End Sub
End Class
