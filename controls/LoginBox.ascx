<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LoginBox.ascx.vb" Inherits="LoginBox" %>
<%@ Register Src="Box.ascx" TagName="Box" TagPrefix="uc1" %>
<%@ Register Src="LoginBoxAnonymous.ascx" TagName="LoginBoxAnonymous" TagPrefix="uc1" %>
<%@ Register Src="LoginBoxAuthenticated.ascx" TagName="LoginBoxAuthenticated" TagPrefix="uc1" %>
<%@ Register Namespace="ExpandIT.SimpleUIControls" TagPrefix="expui" %>
<%  If AppSettings("ADD_LOGIN_BOX") Then%>
<uc1:Box ID="Box1" runat="server" IdPrefix="LoginBox">
    <headertemplate>    
        <expui:ExpLabel ID="lblHeader" runat="server" LabelText="LABLE_USER"></expui:ExpLabel>    
    </headertemplate>
    <itemtemplate>
        <div class="LoginBoxContent">
            <uc1:LoginBoxAnonymous ID="LoginBoxAnonymous1" runat="server" />
            <uc1:LoginBoxAuthenticated ID="LoginBoxAuthenticated1" runat="server" />
        </div>
    </itemtemplate>
</uc1:Box>
<% Else%>
<div class="LoginBoxContent">
    <uc1:LoginBoxAnonymous ID="LoginBoxAnonymous2" runat="server" />
    <uc1:LoginBoxAuthenticated ID="LoginBoxAuthenticated2" runat="server" />
</div>
<% End If%>
