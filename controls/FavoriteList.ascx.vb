Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.ExpandITLib
Imports ExpandIT.EISClass

Partial Class FavoriteList
    Inherits ExpandIT.UserControl

    Protected test As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim products As ExpDictionary
        Dim sql As String
        Dim product As ExpDictionary

        ' Get products in favorite list.
        sql = "SELECT Favorites.ProductGuid FROM Favorites INNER JOIN ProductTable ON Favorites.ProductGuid=ProductTable.ProductGuid WHERE Favorites.UserGuid = " & SafeString(globals.User("UserGuid"))
        products = SQL2Dicts(sql, "ProductGuid")

        If products.Count > 0 Then
            PanelEmptyList.Visible = False

            ' Load the information about the products
            If CBoolEx(AppSettings("EXPANDIT_US_USE_VARIANTS")) Then
                eis.CatDefaultLoadProducts(products, True, False, True, New String() {"ProductName"}, New String() {"PICTURE2"}, "")
            Else
                eis.CatDefaultLoadProducts(products, False, False, True, New String() {"ProductName"}, New String() {"PICTURE2"}, "")
            End If


            ' Sort the list of favorites
            Dim sortedproducts As New System.Collections.Generic.SortedDictionary(Of String, Object)

            test = ""
            For Each product In products.Values
                sortedproducts.Add(product("ProductName") & vbTab & product("ProductGuid"), product)
                test = test & product("ProductGuid") & "_"
            Next
            test = test.TrimEnd("_")

            ' Databinding
            dlProducts.DataSource = sortedproducts
            dlProducts.DataBind()
        Else
            PanelList.Visible = False
        End If

    End Sub

End Class
