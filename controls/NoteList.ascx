﻿<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="NoteList.ascx.vb" Inherits="NoteList" %>

<%@ Register Src="~/controls/NoteLine.ascx" TagName="NoteLine" TagPrefix="uc1" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Register Src="AddToCartButton.ascx" TagName="AddToCartButton" TagPrefix="uc2" %>
<%@ Register Src="NamedNotesDropDown.ascx" TagName="NamedNotesDropDown" TagPrefix="uc3" %>
<%@ Register Src="MailTemplateDropDown.ascx" TagName="MailTemplateDropDown" TagPrefix="uc4" %>


<div class="NoteList">

    <asp:Panel ID="PanelEmptyList" runat="server">
        <asp:Label ID="Label2" runat="server" Text="<%$Resources: Language,LABEL_NOTES_IS_EMPTY %>">.</asp:Label>
        <p></p>
        <b><asp:Label ID="Label3" runat="server" Text="<%$Resources: Language,LABEL_NOTES_CHOOSE_ITEM %>"></asp:Label></b>
    </asp:Panel>
    <asp:Panel ID="PanelList" runat="server">
        <asp:Panel ID="notespanel" runat="server" DefaultButton="addtobasket">
            <asp:Label ID="Label4" runat="server" Text="<%$Resources: Language,LABEL_NOTES_ITEMS_ON_LIST %>"></asp:Label>
            <p></p>
            <div style="border: 1px solid #5b8dff; width: 539px; margin-bottom: 1px;">
            <asp:DataList ID="dlProducts" SkinID="NoteList" runat="server" RepeatColumns="1"
                RepeatDirection="Horizontal" CssClass="NoteList" ItemStyle-CssClass="NoteListItem">
                <AlternatingItemStyle CssClass="List_AlternatingItemStyle" />
                <ItemStyle CssClass="List_ItemStyle" />
                <HeaderTemplate>
                  <div style="border-bottom: 1px solid #f3f3f3; width: 537px">
                    <div class="NoteHeader_Delete">
                        <div class="List_Header">&nbsp;</div>
                    </div>
                    <asp:Panel ID="Panel1" runat="server" Visible='<%# AppSettings("HIDE_PRODUCTGUID") <> "1" %>' >
                        <div class="NoteHeader_ProductGuid">
                            <div class="List_Header">
                                <asp:Literal ID="Literal1" runat="server" Text="<%$Resources: Language, LABEL_PRODUCT %>"></asp:Literal>
                            </div>
                        </div>
                    </asp:Panel>
                    <div class="NoteHeader_ProductName">
                        <div class="List_Header">
                            <asp:Literal ID="Literal2" runat="server" Text="<%$Resources: Language, LABEL_NAME %>"></asp:Literal>
                        </div>
                    </div>
                    
                    <div class="NoteHeader_Price">
                        <div class="List_Header">
                            <asp:Literal ID="Literal4" runat="server" Text="<%$Resources: Language, LABEL_PRICE %>"></asp:Literal>
                        </div>
                    </div>
                    
                    <div class="NoteHeader_Quantity">
                        <div class="List_Header">
                            <asp:Literal ID="Literal3" runat="server" Text="<%$Resources: Language, LABEL_QUANTITY %>"></asp:Literal>
                        </div>
                    </div>
                  </div>
                </HeaderTemplate> 
                <ItemTemplate>
                    <uc1:NoteLine ID="NoteLine1" runat="server" Product='<%# Eval("Value") %>' />
                </ItemTemplate> 
            </asp:DataList> 
            </div> 
            <table cellspacing="0" cellpadding="0" width="541" style="padding-bottom: 10px;">
              <tr>
                <td valign="bottom">
                <% =eis.GetPriceInformation()%>
                </td>
                <td align="right">                  
                  <asp:Button ID="addtobasket" runat="server" CssClass="BtnC" Text="<%$Resources: Language,ACTION_ADD_TO_ORDER_PAD %>" PostBackUrl="~/cart.aspx?cartaction=add&update=update" />         
                </td>
              </tr>
            </table>
            
            <table cellspacing="0" cellpadding="0" class="tables">
              <tr>
                <td width="40">
                <img id="Img1" src="~/App_Themes/Theme3/disk.gif" runat="server" height="40" width="38" align="top" />  
                </td>
                <td>
                <b><asp:Label ID="Label5" runat="server" Text="<%$Resources: Language,LABEL_NOTES_SAVE %>"></asp:Label></b><br />
                <asp:Label ID="Label6" runat="server" Text="<%$Resources: Language,LABEL_NOTES_LISTNAME %>"></asp:Label>
                <asp:TextBox ID="ListNameTextBox" MaxLength="23" runat="server"></asp:TextBox>
                </td>
                <td align="right" valign="bottom">
                <asp:Button ID="savenote" runat="server" CssClass="BtnC" Text="<%$Resources: Language,ACTION_SAVE_NOTE %>" Width="170" />
                </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td colspan="2">
                <asp:Label ID="Label7" runat="server" Text="<%$Resources: Language,LABEL_NOTES_ADD_ITEMS %>"></asp:Label>
                </td>
              </tr>
            </table>
            
            <table cellspacing="0" cellpadding="0" class="tables">
              <tr>               
                <td width="40">
                  <img src="~/App_Themes/Theme3/mailpfeil.gif" runat="server" height="40" width="38" align="top" />               
                </td>
                <td>
                <b><asp:Label ID="Label9" runat="server" Text="<%$Resources: Language,LABEL_NOTES_SEND %>"></asp:Label></b>
                </td>
                <td align="right">
                  <!--<uc4:MailTemplateDropDown ID="sel_mailtempl" runat="server" />  -->                
                  <asp:Button ID="sendnote" runat="server" Text="<%$Resources: Language,LABEL_SEND_EMAIL %>" Width="170" />
                </td>                           
              </tr>
            </table>
            
            <table cellspacing="0" cellpadding="0" class="tables">
              <tr>
                <td width="40">
                <img id="Img2" src="~/App_Themes/Theme3/delete_list.gif" runat="server" height="40" width="38" align="top" />  
                </td>
                <td>
                <b><asp:Label ID="Label10" runat="server" Text="<%$Resources: Language,LABEL_NOTES_DELETE %>"></asp:Label></b>  
                </td>
                <td align="right">
                <asp:Button ID="clearnote" runat="server" CssClass="BtnC" Text="<%$Resources: Language,ACTION_CLEAR_NOTE %>" Width="170" /> 
                </td>
              </tr>
            </table>                  
        </asp:Panel>
    </asp:Panel>
    <br />
    <div class="NoteName">
        <table cellpadding="0" cellspacing="0" class="tables">
            <tr>
                <td>
                <b><asp:Label ID="Label11" runat="server" Text="<%$Resources: Language,LABEL_NOTES_CHOOSE %>"></asp:Label></b>  
                </td>
                <td width="171" align="right">                  
                   <uc3:NamedNotesDropDown ID="selnote" runat="server" />                    
                </td>        
            </tr>
        </table>
    </div>
</div>