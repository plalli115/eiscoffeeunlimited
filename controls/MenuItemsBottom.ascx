<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MenuItemsBottom.ascx.vb"
    Inherits="MenuItemsBottom" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Import Namespace="System.Collections.Generic" %>
<table cellspacing="0" cellpadding="0" border="0" style="height: 30px; text-align: center;
    vertical-align: middle;">
    <tbody>
        <tr>
            <%Dim cnt As Integer = 1%>
            <%  For Each item As ExpandIT.MenuItemBottom In globals.MenuBottom%>
                    <td class="menuItemsBottom" style="padding-left:5px; padding-right:5px;">
                        <a href="<% = item.URL %>" title="<% = htmlencode(item.Caption) %>">
                            <% =HTMLEncode(item.Caption)%>
                        </a>
                    </td>
                    <%If globals.MenuBottom.Count > cnt Then%>
                        <td class="menu_separator" style="vertical-align: middle;">
                            |
                        </td>
                    <%End If%>
            <%cnt = cnt + 1%>
            <%Next%>
        </tr>
    </tbody>
</table>
