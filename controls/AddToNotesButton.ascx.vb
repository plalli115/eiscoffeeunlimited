﻿Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

<Themeable(True)> _
Partial Class AddToNotesButton
    Inherits ExpandIT.UserControl
    
    Private m_productguid As String
    Private m_displaymode As DisplayModes = DisplayModes.Button

    Enum DisplayModes
        Button = 0
        Link = 1
        Image = 2
    End Enum
    
    <Themeable(False)> _
    Public Property ProductGuid() As String
        Get
            Return m_productguid
        End Get
        Set(ByVal value As String)
            m_productguid = value
        End Set
    End Property
    
    Public Property ImageURL() As String
        Get
            Return imgbtnNotes.ImageUrl
        End Get
        Set(ByVal value As String)
            imgbtnNotes.ImageUrl = ThemeURL(value)
        End Set
    End Property
    
    Public Property DisplayMode() As DisplayModes
        Get
            Return m_displaymode
        End Get
        Set(ByVal value As DisplayModes)
            m_displaymode = value
        End Set
    End Property
    
    
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        
        btnNotes.Visible = m_displaymode = DisplayModes.Button
        imgbtnNotes.Visible = m_displaymode = DisplayModes.Image
        linkbtnNotes.Visible = m_displaymode = DisplayModes.Link
        Dim postbackurl As String = "~/_notes.aspx?action=add&SKU=" + HttpUtility.UrlEncode(ProductGuid)       
        Dim buttonText As String = Resources.Language.LABEL_ADD_TO_NOTES
        
        Select Case m_displaymode
          Case DisplayModes.Button
            With btnNotes
              .PostBackUrl = postbackurl 
              .Text = buttonText 
            End With
          Case DisplayModes.Image
            With imgbtnNotes
              .PostBackUrl = postbackurl 
              .AlternateText = buttonText 
            End With
          Case DisplayModes.Link
            With linkbtnNotes
              .PostBackUrl = postbackurl 
              .Text = buttonText 
            End With
        End Select
    End Sub


End Class
