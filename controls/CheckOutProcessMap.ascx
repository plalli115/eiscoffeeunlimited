<%--AM2010080301 - CHECK OUT PROCESS MAP - Start--%>
<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CheckOutProcessMap.ascx.vb"
    Inherits="CheckOutProcessMap" %>
<table cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <%  
        For Each path As String In CheckOutProcessDict%>
        <td class="CheckOutMap" >
            <img src="App_Themes/EnterpriseBlue/checkOutMapArrow.png" />
        </td>
        <td class="CheckOutMap">
            <%RadioButton1.Checked = path = currentLocation()%>
            <asp:RadioButton ID="RadioButton1" CssClass="CheckOutMapRadio" Enabled="false" runat="server" />
        </td>
        <% Next%>
    </tr>
    <tr>
        <%  
            For Each path As String In CheckOutProcessDict%>
        <td>
        </td>
        <%
        If path = currentLocation() Then%>
        <td align="center" class="Checkoutprocess" >
        <%Else%>
        <td align="center">
            <%End If%>
            <%
            lblPath.Text = getPathTitle(path)%>
            <asp:Label ID="lblPath" runat="server"></asp:Label>
        </td>
        <% Next%>
    </tr>
</table>
<%--AM2010080301 - CHECK OUT PROCESS MAP - End--%>
