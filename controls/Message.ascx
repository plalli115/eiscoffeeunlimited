﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Message.ascx.vb" Inherits="controls_Message" %>
<asp:Panel ID="PanelOuter" runat="server" CssClass="PanelOuterMessages" Visible="false">
    <asp:Panel ID="PanelActions" runat="server" CssClass="PanelActions" Visible="false">
        <asp:DataList ID="dlActions" runat="server">
            <HeaderTemplate>
                <div class="MessageBlock">
            </HeaderTemplate>
            <ItemTemplate>
                <div class="Message_Item">
                    <asp:Label ID="Label1" runat="server" Text='<%# System.Web.HttpUtility.HtmlEncode(Container.DataItem) %>'></asp:Label>
                </div>            
            </ItemTemplate>
            <FooterTemplate>
                </div>
            </FooterTemplate>
        </asp:DataList>
    </asp:Panel>
    <asp:Panel ID="PanelErrors" runat="server" CssClass="PanelErrors" Visible="false">
        <asp:DataList ID="dlErrors" runat="server">
            <HeaderTemplate>
                <div class="MessageBlock">
            </HeaderTemplate>
            <ItemTemplate>
                <div class="Message_Item">
                    <asp:Label ID="Label1" runat="server" Text='<%# System.Web.HttpUtility.HtmlEncode(Container.DataItem) %>'></asp:Label>
                </div>            
            </ItemTemplate>
            <FooterTemplate>
                </div>
            </FooterTemplate>
        </asp:DataList>
    </asp:Panel>
    <asp:Panel ID="PanelWarnings" runat="server" CssClass="PanelWarnings" Visible="false">
        <asp:DataList ID="dlWarnings" runat="server">
            <HeaderTemplate>
                <div class="MessageBlock">
            </HeaderTemplate>
            <ItemTemplate>
                <div class="Message_Item">
                    
                    <asp:Label ID="Label1" runat="server" Text='<%# System.Web.HttpUtility.HtmlEncode(Container.DataItem) %>'></asp:Label>
                </div>            
            </ItemTemplate>
            <FooterTemplate>
                </div>
            </FooterTemplate>
        </asp:DataList>
    </asp:Panel>
    <asp:Panel ID="PanelMessages" runat="server" CssClass="PanelMessages" Visible="false">
        <asp:DataList ID="dlMessages" runat="server">
            <HeaderTemplate>
                <div class="MessageBlock">
            </HeaderTemplate>
            <ItemTemplate>
                <div class="Message_Item">
                    <asp:Label ID="Label1" runat="server" Text='<%# System.Web.HttpUtility.HtmlEncode(Container.DataItem) %>'></asp:Label>
                </div>            
            </ItemTemplate>
            <FooterTemplate>
                </div>
            </FooterTemplate>
        </asp:DataList>
    </asp:Panel>
</asp:Panel>
