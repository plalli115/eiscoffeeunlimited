<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="QuickSearch.ascx.vb"
    Inherits="QuickSearch" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<div class="QuickSearch">
    <asp:Panel ID="quicksearchpanel" runat="server" DefaultButton="ImageButton1">
    <!--[if gte IE 8]>
    <div class="ie8Search">
    <![endif]-->
    <!--[if lt IE 8]>
    <div class="ie7Search">
    <![endif]-->
        <table cellpadding="0" cellspacing="0" class="QuickSearch">
            <tr>
                <td class="QuickSearch_Caption">
                    <asp:Label ID="lblSearch" runat="server" Text="<%$ Resources: Language, LABEL_SEARCH_OUR_SHOP %>"></asp:Label>
                </td>
                <td class="QuickSearch_Input">
                     <%= searchText() %>
                </td>
                <td class="QuickSearch_Button">
                    <asp:ImageButton SkinID="QuickSearch" ID="ImageButton1" runat="server" PostBackUrl="~/search.aspx"
                        ImageUrl="~/images/search.png" /> 
                </td>
            </tr>
        </table>
        <!--[if lt IE 8]>
    </span>
    <![endif]-->
        <!--[if gte IE 8]>
    </span>
    <![endif]-->
    </asp:Panel>
</div>
 