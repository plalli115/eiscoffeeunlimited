Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

Partial Class LanguageFlags
    Inherits ExpandIT.UserControl

    Protected m_languages As ExpDictionary

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not New LanguageBoxLogic().isVisible(globals.User) Then
            Me.Visible = False
        Else
            ' Load languages from database
            m_languages = eis.LoadLanguages()
            If m_languages.Count > 1 Then
                dlLanguages.DataSource = m_languages
                dlLanguages.DataBind()
            End If
        End If
    End Sub

End Class
