<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="FavoriteList.ascx.vb"
    Inherits="FavoriteList" %>
<%@ Register Src="~/controls/FavoriteLine.ascx" TagName="FavoriteLine" TagPrefix="uc1" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Register Src="AddToCartButton.ascx" TagName="AddToCartButton" TagPrefix="uc2" %>
<div class="FavoriteList">
    <asp:Label ID="Label1" runat="server" Text="<%$Resources: Language,LABEL_THIS_LIST_IS_THE_FAVORITE_LIST %>">.</asp:Label>
    <p />
    <asp:Panel ID="PanelEmptyList" runat="server">
        <asp:Label ID="Label2" runat="server" Text="<%$Resources: Language,LABEL_FAVORITES_IS_EMPTY %>">.</asp:Label>
    </asp:Panel>
    <asp:Panel ID="PanelList" runat="server">
        <asp:Panel ID="favpanel" runat="server">
            <asp:DataList ID="dlProducts" SkinID="FavoriteList" runat="server" RepeatColumns="1"
                RepeatDirection="Horizontal" CssClass="FavoriteList" Width="100%" ItemStyle-CssClass="FavoriteListItem">
                <AlternatingItemStyle CssClass="List_AlternatingItemStyle" />
                <ItemStyle CssClass="List_ItemStyle" />
                <HeaderTemplate>
                    <div class="FavoriteHeader_Delete">
                        <div class="List_Header">
                            &nbsp;</div>
                    </div>
                    <%--WLB Add Pictures to Favorites 11/17/15 - BEGIN--%>
                    <div class="FavoriteHeader_ProductName">
                        <div class="List_Header">
                            &nbsp;</div>
                    </div>
                    <%--WLB Add Pictures to Favorites 11/17/15 - END--%>
                    <asp:Panel ID="Panel1" runat="server" Visible='<%# AppSettings("HIDE_PRODUCTGUID") <> "1" %>'>
                        <div class="FavoriteHeader_ProductGuid">
                            <div class="List_Header">
                                <asp:Literal ID="Literal1" runat="server" Text="<%$Resources: Language, LABEL_PRODUCT %>"></asp:Literal>
                            </div>
                        </div>
                    </asp:Panel>
                    <div class="FavoriteHeader_ProductName">
                        <div class="List_Header">
                            <asp:Literal ID="Literal2" runat="server" Text="<%$Resources: Language, LABEL_NAME %>"></asp:Literal>
                        </div>
                    </div>
                    <%--AM0122201001 - UOM - Start--%>
                    <%If Not CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) Then%>
                    <!--AM2010021901 - VARIANTS - Start-->
                    <%If CBoolEx(AppSettings("EXPANDIT_US_USE_VARIANTS")) Then%>
                    <div class="FavoriteHeader_Variants">
                        <div class="List_Header">
                            <asp:Literal ID="Literal4" runat="server" Text="<%$Resources:Language, LABEL_VARIANTS %>"></asp:Literal>
                        </div>
                    </div>
                    <%End If%>
                    <!--AM2010021901 - VARIANTS - End-->
                    <div class="FavoriteHeader_Quantity">
                        <div class="List_Header">
                            <asp:Literal ID="Literal3" runat="server" Text="<%$Resources: Language, LABEL_QUANTITY %>"></asp:Literal>
                        </div>
                    </div>
                    <%End If%>
                    <%--AM0122201001 - UOM - End--%>
                </HeaderTemplate>
                <ItemTemplate>
                    <uc1:FavoriteLine ID="FavoriteLine1" runat="server" Product='<%# Eval("Value") %>' />
                </ItemTemplate>
            </asp:DataList>
            <p>
            </p>
            <%--AM0122201001 - UOM - Start--%>
            <%If Not CBoolEx(AppSettings("EXPANDIT_US_USE_UOM")) Or CBoolEx(AppSettings("FAVORITES_UOM_SHOW_MAIN_ADD_TO_CART")) Then%>
            <%--<asp:Button ID="addtobasket" runat="server" CssClass="AddButton" Text="<%$Resources: Language,ACTION_ADD_TO_ORDER_PAD %>"
                PostBackUrl="~/cart.aspx?cartaction=add&update=update" />--%>
            
            <%If eis.CheckPageAccess("Cart") Then%>    
            <input id="lbAddToCart" type="button" class="AddButton" value="<%= Resources.Language.ACTION_ADD_TO_ORDER_PAD %>" />
            <input id="ProductsGuids" type="hidden" value="<%= test %>" /> 
            <input type="hidden" id="GroupGuidTemp" name="GroupGuidTemp" value="<%= Request("GroupGuid") %>" />
            <input type="hidden" id="AddtoCartLabel" name="AddtoCartLabel" value="<%=Resources.Language.ACTION_ADDED_TO_CART %>" />
            <%End If %>
            
                
            <%End If%>
            <%--AM0122201001 - UOM - End--%>
            <p>
            </p>
            
            
            
            <script type="text/javascript" >
                function setAddToCart(notuse){
                    var str='';
                    var guid;
                    var value;
                    var variant;
                    var guids=document.getElementById('ProductsGuids').value;
                    var guidsArr=guids.split('_');
                    for(i=0;i<guidsArr.length;i++){
                        guid=guidsArr[i];
                        if (document.getElementById('Quantity_' + guid) != null){
                            value = document.getElementById('Quantity_' + guid).value;        
                            if(value != ''){
                                str= str + '&SKU=' + guid + '&Quantity=' + value;
                                if (document.getElementById('VariantCode_' + guid) != null){
                                    variant = document.getElementById('VariantCode_' + guid).value;     
                                    if(variant != ''){
                                        str= str + '&VariantCode=' + variant;
                                    }else{
                                        str= str + '&VariantCode=';
                                    }   
                                }else{
                                    str= str + '&VariantCode=';
                                }    
                            }                     
                        }
                    }
                    var group=document.getElementById('GroupGuidTemp');
                    var eventClick=document.getElementById('lbAddToCart');
                    eventClick.onclick =  function() {     
                        setEvent(eventClick,guid,str,group);  
                    };
                }   
                function setEvent(myeventClick,myguid,mystr,mygroup){
                    var label=document.getElementById('AddtoCartLabel').value;
                    var e;
                    e=window.event;
                    AddToCart('~/cart.aspx?cartaction=add' + mystr + '&GroupGuid=' + mygroup.value );
                    new Tooltip().schedule(myeventClick,e,label);
                    return false;
                }
            </script>
            
            
        </asp:Panel>
    </asp:Panel>
</div>
