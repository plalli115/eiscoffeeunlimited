<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ProductBox.ascx.vb" Inherits="ProductBox" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<div class="ProductBox0" style="position: relative; width: 300px; height: 120px;
    margin-right: 10px">
    <div class="ProductBoxHeading" style="height: 20px; width: 300px; position: absolute;
        margin-left: 5px;">
        <a href="<%= ProductLink(Product("ProductGuid"), 0) %>&GroupGuid=<% = globals.GroupGuid %>">
            <% =Product("ProductName")%>
        </a>
    </div> 
        <div class="ProductBox1" style="border: solid 1px #999999; position: absolute;
            top: 20px; left: 0px; height: 100px; width: 300px;">&nbsp;
            <div class="ProductBox1-0" style="width: 100px; height: 100px; background: #ffffff; background-image: url('<% = ThumbnailURL %>'); background-position: center center; background-repeat: no-repeat;
                text-align: center; line-height: 100px; position: absolute; top: 0px; left: 0px">
                <a href="<%= ProductLink(Product("ProductGuid"), 0) %>&GroupGuid=<% = globals.GroupGuid %>">
                    <img src="../images/p.gif" style="height: 100px; width: 100px; border: 0px" alt="" />
                </a>
            </div>
            <div class="ProductBox1-1" style="border-left: solid 1px #999999; width: 200px; height: 100px;
                background: #ccffcc; position: absolute; top: 0px; left: 100px;">
                <div class="ProductBox1-1-0" style="font-size: 10px; height: 100px; position: absolute;
                    top: 0px; left: 0px; padding: 3px">
                    <% =HTMLEncode(SizeText(Product("DESCRIPTION"), 30))%>
                </div>
                <div class="ProductBox1-1-1" style="background: #ff00ff; position: absolute; width: 200px;
                    text-align: right; position: absolute; bottom: 0px; left: 0px;">
                    <div style="position: absolute; bottom: 3px; right: 70px">
                        <% =CurrencyFormatter.FormatAmount(Product("ListPrice_" & Session("UserCurrencyGuid").ToString), Session("UserCurrencyGuid").ToString)%>
                    </div>
                    <div style="position: absolute; bottom: 3px; right: 10px">
                        <asp:Button ID="btnAdd" CssClass="AddButton" runat="server" BackColor="#ffff00" BorderColor="#ff0000"
                            ForeColor="#ff0000" BorderWidth="2" BorderStyle="Solid" Width="50px" Text="Buy"
                             />
                    </div>
                </div>
            </div>
        </div>
</div>
<div style="height: 10px">
    &nbsp;
</div>
