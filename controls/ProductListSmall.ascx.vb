Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

Partial Class ProductListSmall
    Inherits ExpandIT.UserControl

    'JA2010061701 - SLIDE SHOW - Start
    Protected m_products As ExpDictionary
    Protected slideId As String = ""
    Protected myscript As String = ""

    Public Property SlideShowId() As String
        Get
            Return slideId
        End Get
        Set(ByVal value As String)
            slideId = value
            slideId = CLngEx(Mid(slideId, 3, Len(slideId) - 3))
        End Set
    End Property
    'JA2010061701 - SLIDE SHOW - End

    Public Property Products() As ExpDictionary
        Get
            Return m_products
        End Get
        Set(ByVal value As ExpDictionary)
            m_products = value
            dlProducts.DataSource = m_products
            dlProducts.DataBind()
        End Set
    End Property

    'JA2010061701 - SLIDE SHOW - Start
    Protected Overloads Sub Page_Prerender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        Me.GenericSlideShow2.ImageDict = Products()
        Me.GenericSlideShow2.SlideShowId = SlideShowId()
        myscript = "<script language=""javascript"" type=""text/javascript"">"
        myscript = myscript & "addLoadEvent(function(){slideShow.init('thumbs" & SlideShowId() & "'); slideShow.lim('thumbs" & SlideShowId() & "');});</script>"

    End Sub
    'JA2010061701 - SLIDE SHOW - End
End Class
