Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

Partial Class BestSellersByCategory
    Inherits ExpandIT.UserControl
    Protected MostPopularItems As ExpDictionary
    Protected groupGuid As String = ""

    Public Property getGroupGuid() As String
        Get
            Return groupGuid
        End Get
        Set(ByVal value As String)
            groupGuid = value
        End Set
    End Property


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        MostPopularItems = eis.GetMostPopularItemsByCategory(groupGuid)
    End Sub

End Class
