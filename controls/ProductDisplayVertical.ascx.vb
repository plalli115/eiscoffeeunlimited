Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass

<Themeable(True)> _
Partial Class ProductDisplayVertical
    Inherits ExpandIT.UserControl

    Private m_product As ExpDictionary
    Protected m_productclass As ProductClass

    <Themeable(False)> _
    Public Property Product() As ExpDictionary
        Get
            Return m_product
        End Get
        Set(ByVal value As ExpDictionary)
            m_product = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        m_productclass = New ProductClass(m_product)
        m_productclass.GroupGuid = globals.GroupGuid
        fvProduct.DataSource = New ProductClass() {m_productclass}
        fvProduct.DataBind()

        Dim hlOldPrice As HyperLink = FindChildControl(Me, "hlOldPrice")
    End Sub
End Class
