<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ProductListVertical.ascx.vb"
    Inherits="ProductListVertical" %> 
<%@ Register Src="~/controls/ProductDisplayVertical.ascx" TagName="ProductDisplayVertical"
    TagPrefix="uc1" %>
<%@ Register Src="~/controls/Box.ascx" TagName="Box" TagPrefix="ucbox" %>
<ucbox:box id="ProductListVertical" runat="server">
    <headertemplate>
        <asp:Label ID="lblHeading" runat="server" Text="Heading"></asp:Label>
    </headertemplate>
    <itemtemplate>
        <asp:DataList ID="dlProductsListVertical1" SkinID="ProductListVertical" runat="server" RepeatColumns="1" RepeatDirection="Horizontal"
            ItemStyle-VerticalAlign="top" Width="100%" ItemStyle-CssClass="ProductListVerticalDataList">
            <ItemTemplate>
                <div class="ProductDisplayVerticalItem" style="width: 100%">
                    <uc1:ProductDisplayVertical ID="ProductDisplayVertical1" runat="server" Product='<%# Eval("Value") %>' />
                </div>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <div class="ProductDisplayVerticalAlternate">
                    <uc1:ProductDisplayVertical ID="ProductDisplayVertical1" runat="server" Product='<%# Eval("Value") %>' />
                </div>
            </AlternatingItemTemplate>
        </asp:DataList>
    </itemtemplate>
</ucbox:box>
