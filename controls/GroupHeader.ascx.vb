Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.ExpandITLib
Imports ExpandIT.EISClass

Partial Class GroupHeader
    Inherits System.Web.UI.UserControl

    Protected m_group As ExpDictionary

    Public Property Group() As ExpDictionary
        Get
            Return m_group
        End Get
        Set(ByVal value As ExpDictionary)
            m_group = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        litHTMLDescription.Text = IIf(Group("HTMLDESC") <> "", Group("HTMLDESC"), "")
        Me.Picture1.ImageUrl = VRoot & "/" & Group("PICTURE1")
        dlProducts.DataSource = Group("Subgroups")
        dlProducts.DataBind()
    End Sub
End Class
