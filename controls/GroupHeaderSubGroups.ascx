<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="GroupHeaderSubGroups.ascx.vb"
    Inherits="GroupHeaderSubGroups" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>

<%  If CStrEx(SubGroup("PICTURE2")) <> "" And CStrEx(SubGroup("NAME")) <> "" Then%>
<div class="GroupHeader0" style="margin-bottom: 10px;">
    <table style="width: 100%;">
        <tr>
            <td align="center" >
                <%If Not SubGroup("PICTURE2") = "" Then%>
                <a href="<%= VRoot %>/link.aspx?GroupGuid=<%= SubGroup("GroupGuid") %>">
                    <img alt="" src="<%=VRoot & "/" & SubGroup("PICTURE2") %>" style="<%=getStyle() %>" />
                </a>
                <% End If%>
            </td>
        </tr>
        <tr>
            <td align="center" style="padding-top:5px; padding-bottom:5px;" >
                <a style="color:#6C6C6C; text-decoration:none; font-weight:bold; font-size:14px; " href="<%= VRoot %>/link.aspx?GroupGuid=<%= SubGroup("GroupGuid") %>">
                    <asp:Literal ID="litName" runat="server" Text="Label"></asp:Literal>
                </a> 
            </td> 
        </tr>
        <tr>
            <td style="padding-left: 25px; padding-right: 25px;">
                <a style="color:#6C6C6C; text-decoration:none; " href="<%= VRoot %>/link.aspx?GroupGuid=<%= SubGroup("GroupGuid") %>">
                    <asp:Literal ID="litHTMLDescription" runat="server" Text="Label"></asp:Literal>
                </a> 
            </td>
        </tr>
    </table>
</div>
<% End If%>