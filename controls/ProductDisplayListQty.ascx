<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ProductDisplayListQty.ascx.vb"
    Inherits="ProductDisplayListQty" %>
<%@ Register Src="AddToCartButton.ascx" TagName="AddToCartButton" TagPrefix="uc1" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Register Namespace="ExpandIT.Buttons" TagPrefix="exbtn" %>
<%--AM2010051902 - PRODUCT AVAILABILITY - Start--%>
<%@ Register Src="~/controls/USProductAvailability/ProductAvailability.ascx" TagName="ProductAvailability"
    TagPrefix="uc1" %>
<%--AM2010051902 - PRODUCT AVAILABILITY - End--%>
<%--AM2010021901 - VARIANTS - Start--%>
<%@ Register Src="~/controls/USVariants/Variants.ascx" TagName="Variants" TagPrefix="uc1" %>
<%--AM2010021901 - VARIANTS - End--%>

<%@ Register Src="~/controls/PriceNoUom.ascx" TagName="PriceNoUom"
    TagPrefix="uc1" %>

<%--AM0122201001 - UOM - Start--%>
<%@ Register Src="~/controls/USUOM/UOM.ascx" TagName="UOM" TagPrefix="uc1" %>
<%  If AppSettings("EXPANDIT_US_USE_UOM") Then%>
<div class="ProductDisplayList3">
    <%  Else%>
    <div class="ProductDisplayList">
        <%  End If%>
        <%--AM0122201001 - UOM - End--%>
        <asp:FormView ID="fvProduct" Style="height: 100%;" runat="server" RowStyle-CssClass="ProductDisplayListRow"
            CssClass="ProductDisplayListForm">
            <ItemTemplate>
                <table cellpadding="0" cellspacing="0" style="width: 100%; height: 100%;">
                    <tr>
                        <td colspan="2" class="ProductDisplayList_Name" style="padding-bottom: 10px; text-align: left;">
                            <%  If CBoolEx(AppSettings("SHOW_PRODUCT_GUID_TEMPLATES")) Then%>
                            <asp:HyperLink ID="HyperLink1" CssClass="ProductDisplayList_Name" runat="server"
                                SkinID="hlProductDisplayList_Name" Text='<%# Eval("Guid") %>' NavigateUrl='<%# ProductLink(Eval("Guid"), globals.groupguid) %>'></asp:HyperLink>
                            <asp:Label ID="Label4" runat="server" Text="/"></asp:Label>
                            <%End If%>
                            <asp:HyperLink ID="hlProductDisplayList_Name" CssClass="ProductDisplayList_Name"
                                runat="server" SkinID="hlProductDisplayList_Name" Text='<%# Eval("Name") %>'
                                NavigateUrl='<%# ProductLink(Eval("Guid"), globals.groupguid) %>'></asp:HyperLink>
                            <%--AM2010051801 - SPECIAL ITEMS - Start--%>
                            <%--AM2010051802 - WHAT'S NEW - Start--%>
                            <asp:HyperLink ID="HyperLink2" runat="server" CssClass="SpecialPrice" Style="text-decoration: none;"
                                Text='<%# "&nbsp;" & Eval("LastName") %>' NavigateUrl='<%# ProductLink(Eval("Guid"), globals.groupguid) %>'></asp:HyperLink>
                            <%--AM2010051802 - WHAT'S NEW - End--%>
                            <%--AM2010051801 - SPECIAL ITEMS - End--%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="ProductDisplayList_Thumbnail">
                            <asp:HyperLink ID="HyperLink6" runat="server" SkinID="ProductDisplayList_Thumbnail"
                                NavigateUrl='<%# ProductLink(Eval("Guid"), globals.groupguid) %>'>
                                <asp:Image ID="Image1" Style='<%# getStyle() %>' ImageUrl='<%# Eval("ThumbnailURL") %>'
                                    runat="server" />
                                <%--JA0531201001 - STAY IN CATALOG - START--%>
                                <input id='img_<%# Eval("Guid")%>' type="hidden" value='<%# Eval("ThumbnailURL") %>' />
                                <%--JA0531201001 - STAY IN CATALOG - END--%>
                            </asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="ProductDisplayList_Description" style="text-align: left;">
                            <!-- Changed here to replace <br/> with "" and to only use Description -->
                            <asp:HyperLink ID="hlDescription" Visible='<%# isNotEmptyString(Eval("Description")) %>'
                                runat="server" SkinID="ProductDisplayList_Description" Text='<%# Replace(findDescription(Eval("Description")),"<br/>"," ") %>'
                                NavigateUrl='<%# ProductLink(Eval("Guid"), globals.groupguid) %>'><%# Eval("Name") %></asp:HyperLink>
                        </td>
                    </tr>
                    
                    <%--AM2010021901 - VARIANTS - Start--%>
                    <%  If CBoolEx(AppSettings("EXPANDIT_US_USE_VARIANTS")) Then%>
                    <tr>
                        <td colspan="2" style="width: 100%; padding-bottom: 10px; text-align: left;">
                            <%--JA2010030901 - PERFORMANCE - Start--%>                         
                            <%--<uc1:Variants ID="Variants2" runat="server" ProductGuid='<%# Eval("Guid") %>' />--%>
                            <asp:Label ID="lblGuidVariants" style="display:none;" runat="server" Text='<%# Eval("Guid") %>'></asp:Label>
                            <asp:PlaceHolder ID="phVariants" runat="server" />                            
                            <%--JA2010030901 - PERFORMANCE - End--%> 
                        </td>
                    </tr>
                    <%End If%>
                    <%--AM2010021901 - VARIANTS - End--%>
                    
                    
                    <%--'AM0122201001 - UOM - Start--%>
                    <% 
                        If AppSettings("EXPANDIT_US_USE_UOM") Then%>
                    <tr>
                        <td colspan="2" align="left">
                            <%--AM2010051902 - PRODUCT AVAILABILITY - Start--%>
                            <uc1:ProductAvailability ID="ProductAvailability1" runat="server" QtyOnHand='<%# Eval("QtyOnHand") %>'
                                DisplayMode="QtyOnHand" />
                            <%--AM2010051902 - PRODUCT AVAILABILITY - End--%>
                        </td>
                    </tr>
                        <tr>
                            <td style="vertical-align: bottom; text-align: left;">
                            <%--JA2010030901 - PERFORMANCE - Start--%> 
                            
                                <%--AM2010051801 - SPECIAL ITEMS - Start--%>
                            <%--<uc1:UOM ID="UOM2" ProductGuid='<%# Eval("Guid") %>' runat="server" PostBackURL='<%# addtocartlink(Eval("Guid"))%>'
                                    DisplayMode="List" ShowCartBtn="true" ShowFavoritesBtn="true" DefaultQty="1"
                                IsSpecial='<%# CBoolEx(Eval("IsSpecial")) %>' />--%>
                            
                            <asp:Label ID="lblGuid" style="display:none;" runat="server" Text='<%# Eval("Guid") %>'></asp:Label>                            
                            <asp:Label ID="lblSpecial" style="display:none;"  runat="server" Text='<%# CBoolEx(Eval("IsSpecial")) %>'></asp:Label> 
                            <asp:PlaceHolder ID="phUOM" runat="server" />
                                <%--AM2010051801 - SPECIAL ITEMS - End--%>
                            
                            <%--JA2010030901 - PERFORMANCE - End--%>
                            
                            
                            </td>
                        </tr>
                    <%Else%>
                    <tr>
                        <%--AM2010051801 - SPECIAL ITEMS - Start--%>
                        <%If eis.CheckPageAccess("Prices") Then%>
                        <asp:Panel runat="server" ID="pnlSpecialPricing" Visible='<%# CBoolEx(Eval("IsSpecial")) %>'>
                            <td class="ProductDisplayList_Price">
                                <del class="RegularPrice">
                                    
                                    <uc1:PriceNoUom ID="PriceNoUom1" runat="server" Guid='<%# Eval("Guid") %>' CssClassText="RegularPrice" Price='<%# CDblEx(Eval("OriginalPrice")) %>' />
                                    <%--<asp:HyperLink ID="HyperLink4" CssClass="RegularPrice" runat="server" SkinID="ProductDisplayList_Price"
                                        Text='<%# CurrencyFormatter.FormatCurrency(CDblEx(Eval("OriginalPrice")), Session("UserCurrencyGuid").ToString) %>'
                                        NavigateUrl='<%# ProductLink(Eval("Guid"), globals.groupguid) %>'></asp:HyperLink>--%>
                                </del>
                            </td>
                            <td class="ProductDisplayList_Price" style="text-align: left;">
                                <uc1:PriceNoUom ID="PriceNoUom2" runat="server" Guid='<%# Eval("Guid") %>' CssClassText="SpecialPrice" Price='<%# CDblEx(Eval("Price")) %>' />
                                <%--<asp:HyperLink ID="HyperLink5" CssClass="SpecialPrice" runat="server" SkinID="ProductDisplayList_SpecialPrice"
                                    Text='<%# CurrencyFormatter.FormatCurrency(CDblEx(Eval("Price")), Session("UserCurrencyGuid").ToString) %>'
                                    NavigateUrl='<%# ProductLink(Eval("Guid"), globals.groupguid) %>'></asp:HyperLink>--%>
                            </td>
                        </asp:Panel>                        
                        <asp:Panel ID="pnlRegularPricing" runat="server" Visible='<%# Not CBoolEx(Eval("IsSpecial")) %>'>
                            <td colspan="2" class="ProductDisplayList_Price" style="text-align: left;">
                                <uc1:PriceNoUom ID="PriceNoUom3" runat="server" Guid='<%# Eval("Guid") %>' CssClassText="RegularPrice" Price='<%# CDblEx(Eval("Price")) %>' />
                                <%--<asp:HyperLink ID="HyperLink3" CssClass="RegularPrice" runat="server" SkinID="ProductDisplayList_Price"
                                    Text='<%# CurrencyFormatter.FormatCurrency(CDblEx(Eval("Price")), Session("UserCurrencyGuid").ToString) %>'
                                    NavigateUrl='<%# ProductLink(Eval("Guid"), globals.groupguid) %>'></asp:HyperLink>--%>
                            </td>
                        </asp:Panel>
                        <%End If %>
                        <%--AM2010051801 - SPECIAL ITEMS - End--%>
                    </tr>
                    <tr>
                        <td colspan="2" align="left">
                            <%--AM2010051902 - PRODUCT AVAILABILITY - Start--%>
                            <uc1:ProductAvailability ID="ProductAvailability2" runat="server" QtyOnHand='<%# Eval("QtyOnHand") %>'
                                DisplayMode="QtyOnHand" />
                            <%--AM2010051902 - PRODUCT AVAILABILITY - End--%>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle; padding-bottom: 10px; text-align: left;display:none;">
                            <asp:Label ID="Label9" Style="font-weight: bold;" runat="server" Text="<%$Resources: Language,LABEL_QUANTITY %>"></asp:Label>:
                        </td>
                        <td style="text-align: center; padding-bottom: 10px;display:none;">
                            <%--<input type="text" name="Quantity" style="height: 15px; width: 20px; text-align: center;" value="1" />--%>
                            <input type="text" id="Quantity_<%# Eval("Guid") %>" onblur="setAddToCart('<%# Eval("Guid") %>');getPricesNoUom('<%# Eval("Guid")%>');" name="Quantity_<%# Eval("Guid") %>" size="2" tabindex="1" value="1" style="text-align:center;" />
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">
                            <div class="ProductDisplayList_DetailsLink">
                                <asp:Button ID="hlDetailsLink" CssClass="AddToFavoritesButton" runat="server" PostBackUrl='<%# ProductLink(Eval("Guid"), globals.groupguid) %>'
                                    Text="<%$Resources: Language, LABEL_DETAILS %>" />
                                <%--<asp:HyperLink ID="hlDetailsLink" SkinID="ProductDisplayList_DetailsLink" Style="text-decoration: none;"
                                        runat="server" NavigateUrl='<%# ProductLink(Eval("Guid"), globals.groupguid) %>'
                                        Text="<%$Resources: Language, LABEL_DETAILS %>"></asp:HyperLink>--%>
                            </div>
                        </td>
                        <td style="text-align: left;">
                            <%If eis.CheckPageAccess("Cart") Then%>
                            <div>
                                <%--<asp:Button ID="lbAddToCart" CssClass="AddButton" runat="server" PostBackUrl='<%# addtocartlink(Eval("Guid"))%>'
                                    TabIndex="2" Text="<%$Resources: Language, ACTION_ADD_TO_ORDER_PAD %>" />
                                <input type="hidden" id="SKU" name="SKU" value='<%# Eval("Guid") %>' />--%>
                                
                                <input id="lbAddToCart_<%# Eval("Guid") %>" type="button" class="AddButton" value="<%= Resources.Language.ACTION_ADD_TO_ORDER_PAD %>" onmouseover="setAddToCart('<%# HTMLEncode(Eval("Guid")) %>');" />
                                
                                <%--JA0531201001 - STAY IN CATALOG - START--%>
                                <a id="lightboxIdAddToCart" style="display:none;"  rel="lightbox" class=""
                                    href="<%=Vroot %>/App_Themes/EnterpriseBlue/p.gif">
                                    <img alt="" src="<%=Vroot %>/App_Themes/EnterpriseBlue/p.gif" />
                                </a>
                                <%--JA0531201001 - STAY IN CATALOG - END--%> 
                                
                                
                                <input type="hidden" id="GroupGuidTemp" name="GroupGuidTemp" value="<%= Request("GroupGuid") %>" />
                                <input type="hidden" id="AddtoCartLabel" name="AddtoCartLabel" value="<%=Resources.Language.ACTION_ADDED_TO_CART %>" />
                            </div>
                            <%End If %>
                        </td>
                    </tr>
                    <%End If%>
                </table>
                <%--'AM0122201001 - UOM - End--%>
                
                <script type="text/javascript" >
                    function setAddToCart(guid){
                        var str='';
                        var value;
                        var variant;
                        str= str + '&SKU=' + guid + '&Quantity=1';  
                        if (document.getElementById('VariantCode_' + guid) != null){
                            variant = document.getElementById('VariantCode_' + guid).value;     
                            if(variant != ''){
                                str= str + '&VariantCode=' + variant;
                            }else{
                                str= str + '&VariantCode=';
                            }   
                        }else{
                            str= str + '&VariantCode=';
                        }    
                        var group=document.getElementById('GroupGuidTemp');
                        var eventClick=document.getElementById('lbAddToCart_' + guid);
                        eventClick.onclick =  function() {     
                            setEvent(eventClick,guid,str,group);  
                        };
                    }   
                    function setEvent(myeventClick,myguid,mystr,mygroup){
                        var label=document.getElementById('AddtoCartLabel').value;
                        var e;
                        e=window.event;
                        AddToCart('~/cart.aspx?cartaction=add' + mystr + '&GroupGuid=' + mygroup.value);
                        new Tooltip().schedule(myeventClick,e,label);
                        
                        //JA0531201001 - STAY IN CATALOG - START
                        <%If CIntEx(AppSettings("STAY_IN_CATALOG")) = 0 Then %>
                            setTimeout('goToCart()',500);
                        <%ElseIf CIntEx(AppSettings("STAY_IN_CATALOG")) = 2 Then %>
                            setTimeout('showLigthBox(' + myguid + ')',300); 
                            document.getElementById('bottomNav').style.display='none';
                        <%End If %>
                        //JA0531201001 - STAY IN CATALOG - END
                        
                        return false;
                    }
                    
                     //JA0531201001 - STAY IN CATALOG - START        
                    function showLigthBox(myguid){
                        
                        var variant='';
                        if (document.getElementById('VariantCode_' + myguid) != null){
                            variant = document.getElementById('VariantCode_' + myguid).value;
                            var index=document.getElementById('VariantCode_' + myguid).selectedIndex;
                            var varianName=document.getElementById('VariantCode_' + myguid).options[index].text;
                        }    
                        var image=document.getElementById('img_' + myguid).value;
                        var url= '<%=Vroot %>' + "/AddToCartAjax.aspx?Guid=" + myguid + "&Image=" + image + "&VariantCode=" + variant + '&VariantName=' + varianName;
                        var xhReq = new XMLHttpRequest();  
                        xhReq.open("GET", url += (url.match(/\?/) == null ? "?" : "&") + (new Date()).getTime(), false);
                        xhReq.send(null);
                        var html=xhReq.responseText;
                        var start=html.indexOf('<body>') + 6;
                        var end=html.indexOf('</body>');
                        html=html.substring(start,end);
                        
                        document.getElementById('lightboxIdAddToCart').className=html;
                        
                        actuateLink(document.getElementById('lightboxIdAddToCart'));            
                              
                    }        
                    function stayInPage(){
                        var str =window.location.href;
                        str=str.replace('#','');
                        window.location=str;        
                    }
                    function goToCart(){
                        window.location='<%=Vroot%>/cart.aspx';        
                    } 
                    
                    function actuateLink(link){
                       var allowDefaultAction = true;
                          
                       if (link.click){
                          link.click();
                          return;
                       }else if (document.createEvent){
                          var e = document.createEvent('MouseEvents');
                          e.initEvent(
                             'click'     // event type
                             ,true      // can bubble?
                             ,true      // cancelable?
                          );
                          allowDefaultAction = link.dispatchEvent(e);           
                       }
                             
                       if (allowDefaultAction){
                          var f = document.createElement('form');
                          f.action = link.href;
                          document.body.appendChild(f);
                          f.submit();
                       }
                    }
                    //JA0531201001 - STAY IN CATALOG - END
                    
                </script>
                
                
            </ItemTemplate>
        </asp:FormView>
    </div>
