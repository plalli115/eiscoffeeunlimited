﻿'JA2010022301 - New Payment Methods - Start
Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.ExpandITLib
Imports ExpandIT.EISClass
Imports System.Data 

Partial Class controls_BankAccount
    Inherits ExpandIT.UserControl
    
  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    
      globals.OrderDict = CType(Session("OrderDict"), ExpDictionary)    
      submitBtn.Text = HttpUtility.HtmlDecode(Resources.Language.ACTION_NEXT_GREATER_THAN)
    
  End Sub

  Protected Sub submitBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles submitBtn.Click
        Dim buff As String

        'JA2011030701 - PAYMENT TABLE - START
        Dim paymentTableDict As New ExpDictionary
        paymentTableDict.Add("CustomerReference", globals.OrderDict("CustomerReference"))
        paymentTableDict.Add("PaymentGuid", GenGuid())
        paymentTableDict.Add("PaymentReference", eis.GenCustRef())
        paymentTableDict.Add("PaymentDate", DateTime.Now())
        paymentTableDict.Add("UserGuid", globals.User("UserGuid"))
        paymentTableDict.Add("LedgerPayment", False)

        paymentTableDict.Add("DocumentGuid", globals.OrderDict("HeaderGuid"))
        paymentTableDict.Add("PaymentType", globals.OrderDict("PaymentType"))
        buff = accountNrTextBox.Text.Replace(" ", "")
        paymentTableDict.Add("BankAccountNum", buff)
        buff = bankCodeTextBox.Text.Replace(" ", "")
        paymentTableDict.Add("BankRegNum", buff)
        paymentTableDict.Add("BankAccountOwner", accountOwnerTextBox.Text)

        Dict2Table(paymentTableDict, "PaymentTable", "")

        'Dim buff As String
        'buff = accountNrTextBox.Text.Replace(" ", "")
        'If globals.OrderDict.Exists("BankAccountNum") Then
        '    globals.OrderDict("BankAccountNum") = buff
        'Else
        '    globals.OrderDict.Add("BankAccountNum", buff)
        'End If
        'buff = bankCodeTextBox.Text.Replace(" ", "")
        'If globals.OrderDict.Exists("BankRegNum") Then
        '    globals.OrderDict("BankRegNum") = buff
        'Else
        '    globals.OrderDict.Add("BankRegNum", buff)
        'End If
        'If globals.OrderDict.Exists("BankAccountOwner") Then
        '    globals.OrderDict("BankAccountOwner") = accountOwnerTextBox.Text
        'Else
        '    globals.OrderDict.Add("BankAccountOwner", accountOwnerTextBox.Text)
        'End If
        ' Save the changed order dictionary
        'eis.SaveOrderDictionary(globals.OrderDict)

        'JA2011030701 - PAYMENT TABLE - END


    Response.Redirect("~/_payment.aspx", true)
  End Sub
  
  '"^\d{5,10}$|^[\p{Lu}]{6}\w{2,5}$" 
  '"^\d+$|^[\p{Lu}]{2}\d{2}\d+$"
  
End Class
'JA2010022301 - New Payment Methods - End
