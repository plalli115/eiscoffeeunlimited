Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.ExpandITLib
Imports System.Collections.Generic

Partial Class controls_CreditCards
    Inherits ExpandIT.UserControl

    Protected Overloads Sub Page_load(ByVal o As Object, ByVal e As EventArgs) Handles Me.Load

        Dim folderpath As String = ApplicationRoot()
        Dim relativepath As String = "/images/cards"
        Dim dict As New ExpDictionary()
        'JA2010030901 - PERFORMANCE -Start
        Dim cachekey As String

        cachekey = "SupportedCreditCards"
        dict = EISClass.CacheGet(cachekey)
        If dict Is Nothing Then
            dict = New ExpDictionary()
            Dim supportedCC As ArrayList = loadSupportedCC()

            For Each file As String In System.IO.Directory.GetFiles(folderpath & relativepath)
                Dim tmpfile As String = file.Substring(folderpath.Length)
                tmpfile = Replace(tmpfile, "\", "/")
                Dim testStr As String = tmpfile.Substring(relativepath.Length + 1)
                If supportedCC.Contains(testStr.Substring(0, InStr(testStr, ".") - 1).ToUpper) Then
                    tmpfile = String.Concat(VRoot, tmpfile)
                    dict.Add(file, tmpfile)
                End If
            Next
            ' Set cache
            EISClass.CacheSet(cachekey, dict, "EECCApprovedCreditCardType")
        End If
        'JA2010030901 - PERFORMANCE -End
        cards.RepeatColumns = CIntEx(AppSettings("CREDIT_CARDS_PER__LINE"))

        cards.DataSource = dict
        cards.DataBind()

    End Sub

    'Private Function loadSupportedCC() As ArrayList

    '    Dim retArray As New ArrayList()

    '    For Each str As String In AppSettings
    '        If str.Contains("SUPPORTED_CC") Then
    '            If CBoolEx(AppSettings(str)) Then
    '                retArray.Add(str.Substring("SUPPORTED_CC_".Length))
    '            End If
    '        End If
    '    Next
    '    Return retArray

    'End Function

    Private Function loadSupportedCC() As ArrayList

        Dim retArray As New ArrayList()

        Dim SQL As String = "SELECT GatewayCardTypeCode FROM EECCApprovedCreditCardType WHERE (ApprovedForUse = 1)"
        Dim retv As ExpDictionary = SQL2Dicts(SQL)


        For Each str As ExpDictionary In retv.Values
            retArray.Add(CStrEx(str("GatewayCardTypeCode")))
        Next
        Return retArray

    End Function

    Protected Function getAlt(ByVal obj As String) As String
        Dim str As String = obj.Substring(0, obj.Length - 4)
        str = str.Split("/")(str.Split("/").Length - 1)
        Return str

    End Function

End Class
