'AM2011060201 - PRICES BREAKDOWN - Start
Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

Partial Class PricesBreakDown
    Inherits ExpandIT.UserControl

    Private m_productGuid As String
    Protected productDict As ExpDictionary
    Protected prices As ExpDictionary
    Protected lineDiscounts As ExpDictionary
    Protected ShowInfo As Boolean = False
    Protected finalDictPrice As New ExpDictionary
    Protected finalDictDiscounts As New ExpDictionary
    Protected ShowVariants As Boolean = False
    Protected colSpanTitle As Integer = 2

    Public Property ProductGuid() As String
        Get
            Return m_productGuid
        End Get
        Set(ByVal value As String)
            m_productGuid = value
        End Set
    End Property

    Public Property ProductDictVariant() As ExpDictionary
        Get
            Return productDict
        End Get
        Set(ByVal value As ExpDictionary)
            productDict = value
        End Set
    End Property


    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If eis.CheckPageAccess("Cart") Then
            If globals.Context Is Nothing Then
                globals.Context = eis.LoadContext(globals.User)
            End If
            getPrices()
            getLineDiscounts()
        End If
    End Sub

    Public Sub getLineDiscounts()

        Dim campaign As String = ""
        Dim LinkedItems As Boolean
        'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - Start
        Try
            LinkedItems = CBoolEx(globals.eis.getEnterpriseConfigurationValue("LinkedItemsEnabled"))
        Catch ex As Exception
        End Try
        'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - End

        Dim sql As String
        Dim customerDiscountGroup As String = CStrEx(getSingleValueDB("SELECT CustomerDiscountGroup FROM CustomerTable WHERE CustomerGuid=" & SafeString(globals.Context("CustomerGuid"))))
        Dim itemCustomerDiscountGroup As String
        Dim prevDiscount As Double = 0
        Dim prevVariant As String = ""
        Dim prevUom As String = ""
        Dim prevMinQty As Integer = 0
        Dim baseUOM As String = ""

        If LinkedItems AndAlso (Not productDict Is Nothing AndAlso Not productDict Is DBNull.Value AndAlso productDict.Count > 0) Then
            Dim cont As Integer = 0

            For Each itemVariant As ExpDictionary In productDict.Values
                itemCustomerDiscountGroup = CStrEx(getSingleValueDB("SELECT ItemCustomerDiscountGroup FROM ProductTable WHERE ProductGuid=" & SafeString(itemVariant("VariantCode"))))
                baseUOM = CStrEx(getSingleValueDB("SELECT BaseUOM FROM ProductTable WHERE ProductGuid=" & SafeString(itemVariant("VariantCode"))))
                prevDiscount = 0
                prevVariant = ""
                prevUom = ""
                prevMinQty = 0

                sql = "SELECT ISNULL(MinimumQuantity,1) AS MinQty, LineDiscountPct," & _
                    "VariantCode,UnitOfMeasureCode AS UOMGuid " & _
                    "FROM Attain_SalesLineDiscount WHERE ((Type=0 AND Code=" & SafeString(itemVariant("VariantCode")) & ") OR "
                If itemCustomerDiscountGroup <> "" Then sql &= "(Type=1 AND Code=" & SafeString(itemCustomerDiscountGroup) & ") OR "
                sql &= "Type=2) AND ((SalesType=0 AND SalesCode=" & SafeString(globals.Context("CustomerGuid")) & ") OR "
                If customerDiscountGroup <> "" Then sql &= "(SalesType=1 AND SalesCode=" & SafeString(customerDiscountGroup) & ") OR "
                sql &= "SalesType=2) AND " & _
                    "(StartingDate <= " & SafeDatetime(DateValue(Now)) & " OR StartingDate IS NULL) AND " & _
                    "(EndingDate >= " & SafeDatetime(DateValue(Now)) & " OR EndingDate IS NULL) AND " & _
                    "(CurrencyCode = " & SafeString(globals.OrderDict("CurrencyGuid")) & " OR CurrencyCode IS NULL OR " & _
                    "CurrencyCode = '' OR CurrencyCode = " & SafeString(globals.Context("DefaultCurrency")) & ") ORDER BY UnitOfMeasureCode,VariantCode, MinimumQuantity"
                lineDiscounts = SQL2Dicts(sql)

                For Each discountline As ExpDictionary In lineDiscounts.Values
                    If discountline("UOMGuid") = "" Then
                        discountline("UOMGuid") = baseUOM
                    End If
                    If (prevDiscount = discountline("LineDiscountPct") And prevUom = discountline("UOMGuid")) Then 'OrElse (CStrEx(priceline("UOMGuid")) = "" And prevMinQty < CIntEx(priceline("MinQty")))
                    Else
                        discountline("VariantCode") = itemVariant("VariantCode")
                        discountline("VariantName") = CStrEx(itemVariant("VariantName"))
                        finalDictDiscounts.Add(cont, discountline)
                        cont = cont + 1
                    End If
                    prevMinQty = CIntEx(discountline("MinQty"))
                    prevDiscount = discountline("LineDiscountPct")
                    prevUom = discountline("UOMGuid")
                Next
            Next
        Else
            itemCustomerDiscountGroup = CStrEx(getSingleValueDB("SELECT ItemCustomerDiscountGroup FROM ProductTable WHERE ProductGuid=" & SafeString(ProductGuid)))
            baseUOM = CStrEx(getSingleValueDB("SELECT BaseUOM FROM ProductTable WHERE ProductGuid=" & SafeString(ProductGuid)))
            prevDiscount = 0
            prevVariant = ""
            prevUom = ""
            prevMinQty = 0

            sql = "SELECT ISNULL(MinimumQuantity,1) AS MinQty, LineDiscountPct," & _
                "VariantCode,UnitOfMeasureCode AS UOMGuid " & _
                "FROM Attain_SalesLineDiscount WHERE ((Type=0 AND Code=" & SafeString(ProductGuid) & ") OR "
            If itemCustomerDiscountGroup <> "" Then sql &= "(Type=1 AND Code=" & SafeString(itemCustomerDiscountGroup) & ") OR "
            sql &= "Type=2) AND ((SalesType=0 AND SalesCode=" & SafeString(globals.Context("CustomerGuid")) & ") OR "
            If customerDiscountGroup <> "" Then sql &= "(SalesType=1 AND SalesCode=" & SafeString(customerDiscountGroup) & ") OR "
            sql &= "SalesType=2) AND " & _
                "(StartingDate <= " & SafeDatetime(DateValue(Now)) & " OR StartingDate IS NULL) AND " & _
                "(EndingDate >= " & SafeDatetime(DateValue(Now)) & " OR EndingDate IS NULL) AND " & _
                "(CurrencyCode = " & SafeString(globals.OrderDict("CurrencyGuid")) & " OR CurrencyCode IS NULL OR " & _
                "CurrencyCode = '' OR CurrencyCode = " & SafeString(globals.Context("DefaultCurrency")) & ") ORDER BY UnitOfMeasureCode,VariantCode, MinimumQuantity"
            lineDiscounts = SQL2Dicts(sql)

            'Clean the list of prices, delete duplicate prices
            Dim cont As Integer = 0

            For Each discountline As ExpDictionary In lineDiscounts.Values
                If discountline("UOMGuid") = "" Then
                    discountline("UOMGuid") = baseUOM
                End If
                If (prevDiscount = discountline("LineDiscountPct") And prevVariant = discountline("VariantCode") And prevUom = discountline("UOMGuid")) Then 'OrElse (CStrEx(priceline("VariantCode")) = "" And CStrEx(priceline("UOMGuid")) = "" And prevMinQty < CIntEx(priceline("MinQty")))
                Else
                    discountline("VariantName") = getVariantName(discountline("VariantCode"))
                    finalDictDiscounts.Add(cont, discountline)
                    cont = cont + 1
                End If
                prevMinQty = CIntEx(discountline("MinQty"))
                prevDiscount = discountline("LineDiscountPct")
                prevVariant = discountline("VariantCode")
                prevUom = discountline("UOMGuid")
            Next
        End If

    End Sub

    Public Sub getPrices()


        Dim campaign As String = ""
        Dim LinkedItems As Boolean
        'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - Start
        Try
            campaign = CStrEx(globals.eis.getEnterpriseConfigurationValue("SpecialsCampaign"))
            LinkedItems = CBoolEx(globals.eis.getEnterpriseConfigurationValue("LinkedItemsEnabled"))
        Catch ex As Exception
        End Try
        'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - End

        Dim sql As String
        Dim priceGroupCode As String = CStrEx(getSingleValueDB("SELECT PriceGroupCode FROM CustomerTable WHERE CustomerGuid=" & SafeString(globals.Context("CustomerGuid"))))
        Dim customerDiscountGroup As String = CStrEx(getSingleValueDB("SELECT CustomerDiscountGroup FROM CustomerTable WHERE CustomerGuid=" & SafeString(globals.Context("CustomerGuid"))))
        Dim itemCustomerDiscountGroup As String
        Dim listPrice As Double
        Dim prevPrice As Double = 0
        Dim prevVariant As String = ""
        Dim prevUom As String = ""
        Dim prevMinQty As Integer = 0
        Dim baseUOM As String = ""

        If LinkedItems AndAlso (Not productDict Is Nothing AndAlso Not productDict Is DBNull.Value AndAlso productDict.Count > 0) Then
            Dim cont As Integer = 0

            For Each itemVariant As ExpDictionary In productDict.Values
                itemCustomerDiscountGroup = CStrEx(getSingleValueDB("SELECT ItemCustomerDiscountGroup FROM ProductTable WHERE ProductGuid=" & SafeString(itemVariant("VariantCode"))))
                listPrice = CDblEx(getSingleValueDB("SELECT ListPrice FROM ProductTable WHERE ProductGuid=" & SafeString(itemVariant("VariantCode"))))
                baseUOM = CStrEx(getSingleValueDB("SELECT BaseUOM FROM ProductTable WHERE ProductGuid=" & SafeString(itemVariant("VariantCode"))))
                prevPrice = 0
                prevVariant = ""
                prevUom = ""
                prevMinQty = 0

                sql = "SELECT ISNULL(MinimumQuantity,1) AS MinQty, UnitPrice AS Price," & _
                    "VariantCode,UOMGuid,CustomerRelGuid " & _
                    "FROM Attain_ProductPrice WHERE (ProductGuid=" & SafeString(itemVariant("VariantCode"))
                If CStrEx(itemVariant("ProductGroupCode")) <> "" Then sql &= " OR ProductGroup=" & SafeString(itemVariant("ProductGroupCode"))
                sql &= ") AND ((CustomerRelType = 0 AND CustomerRelGuid = " & SafeString(globals.Context("CustomerGuid")) & ") OR "
                If priceGroupCode <> "" Then sql &= "(CustomerRelType = 1 AND CustomerRelGuid = " & SafeString(priceGroupCode) & ") OR "
                sql &= "(CustomerRelType = 2) OR (CustomerRelType= 3 AND CustomerRelGuid='" & campaign & "' ) ) AND " & _
                    "(StartingDate <= " & SafeDatetime(DateValue(Now)) & " OR StartingDate IS NULL) AND " & _
                    "(EndingDate >= " & SafeDatetime(DateValue(Now)) & " OR EndingDate IS NULL) AND " & _
                    "(CurrencyGuid = " & SafeString(globals.OrderDict("CurrencyGuid")) & " OR CurrencyGuid IS NULL OR " & _
                    "CurrencyGuid = '' OR CurrencyGuid = " & SafeString(globals.Context("DefaultCurrency")) & ") ORDER BY UOMGuid,VariantCode, MinimumQuantity"
                prices = SQL2Dicts(sql)

                For Each priceline As ExpDictionary In prices.Values
                    If priceline("UOMGuid") = "" Then
                        priceline("UOMGuid") = baseUOM
                    End If
                    If (prevPrice = priceline("Price") And prevUom = priceline("UOMGuid")) Then 'OrElse (CStrEx(priceline("UOMGuid")) = "" And prevMinQty < CIntEx(priceline("MinQty")))
                    Else
                        priceline("VariantCode") = itemVariant("VariantCode")
                        priceline("VariantName") = CStrEx(itemVariant("VariantName"))
                        If priceline("CustomerRelGuid") = campaign Then
                            priceline("Special") = True
                        End If
                        finalDictPrice.Add(cont, priceline)
                        cont = cont + 1
                    End If
                    prevMinQty = CIntEx(priceline("MinQty"))
                    prevPrice = priceline("Price")
                    prevUom = priceline("UOMGuid")
                Next
            Next
        Else
            itemCustomerDiscountGroup = CStrEx(getSingleValueDB("SELECT ItemCustomerDiscountGroup FROM ProductTable WHERE ProductGuid=" & SafeString(ProductGuid)))
            listPrice = CDblEx(getSingleValueDB("SELECT ListPrice FROM ProductTable WHERE ProductGuid=" & SafeString(ProductGuid)))
            baseUOM = CStrEx(getSingleValueDB("SELECT BaseUOM FROM ProductTable WHERE ProductGuid=" & SafeString(ProductGuid)))
            Dim productGroupCode As String = CStrEx(getSingleValueDB("SELECT ProductGroupCode FROM ProductTable WHERE ProductGuid=" & SafeString(ProductGuid)))
            prevPrice = 0
            prevVariant = ""
            prevUom = ""
            prevMinQty = 0

            sql = "SELECT ISNULL(MinimumQuantity,1) AS MinQty, UnitPrice AS Price," & _
                "VariantCode,UOMGuid " & _
                "FROM Attain_ProductPrice WHERE (ProductGuid=" & SafeString(ProductGuid)
            If CStrEx(productGroupCode) <> "" Then sql &= " OR ProductGroup=" & SafeString(productGroupCode)
            sql &= ") AND ((CustomerRelType = 0 AND CustomerRelGuid = " & SafeString(globals.Context("CustomerGuid")) & ") OR "
            If priceGroupCode <> "" Then sql &= "(CustomerRelType = 1 AND CustomerRelGuid = " & SafeString(priceGroupCode) & ") OR "
            sql &= "(CustomerRelType = 2) OR (CustomerRelType= 3 AND CustomerRelGuid='" & campaign & "' ) ) AND " & _
                "(StartingDate <= " & SafeDatetime(DateValue(Now)) & " OR StartingDate IS NULL) AND " & _
                "(EndingDate >= " & SafeDatetime(DateValue(Now)) & " OR EndingDate IS NULL) AND " & _
                "(CurrencyGuid = " & SafeString(globals.OrderDict("CurrencyGuid")) & " OR CurrencyGuid IS NULL OR " & _
                "CurrencyGuid = '' OR CurrencyGuid = " & SafeString(globals.Context("DefaultCurrency")) & ") " & _
                "ORDER BY UOMGuid,VariantCode"
            prices = SQL2Dicts(sql)

            'Clean the list of prices, delete duplicate prices
            Dim cont As Integer = 0

            For Each priceline As ExpDictionary In prices.Values
                If priceline("UOMGuid") = "" Then
                    priceline("UOMGuid") = baseUOM
                End If
                If (prevPrice = priceline("Price") And prevVariant = priceline("VariantCode") And prevUom = priceline("UOMGuid")) Then 'OrElse (CStrEx(priceline("VariantCode")) = "" And CStrEx(priceline("UOMGuid")) = "" And prevMinQty < CIntEx(priceline("MinQty")))
                Else
                    priceline("VariantName") = getVariantName(priceline("VariantCode"))
                    finalDictPrice.Add(cont, priceline)
                    cont = cont + 1
                End If
                prevMinQty = CIntEx(priceline("MinQty"))
                prevPrice = priceline("Price")
                prevVariant = priceline("VariantCode")
                prevUom = priceline("UOMGuid")
            Next
        End If
    End Sub

    Public Function getVariantName(ByVal VariantCode As String) As String
        Dim VariantName As String = ""
        If Not productDict Is Nothing Then
            For Each line As ExpDictionary In productDict.Values
                If line("VariantCode") = VariantCode Then
                    Return line("VariantName")
                End If
            Next
        End If
        Return VariantName
    End Function



End Class
'AM2011060201 - PRICES BREAKDOWN - End