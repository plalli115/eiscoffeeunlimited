Imports System.Configuration.ConfigurationManager

<Themeable(True)> _
Partial Class Box
    Inherits ExpandIT.UserControl

    Private m_headertemplate As ITemplate = Nothing
    Private m_itemtemplate As ITemplate = Nothing
    Private m_idprefix As String = ""

    Public Property IdPrefix() As String
        Get
            Return m_idprefix
        End Get
        Set(ByVal value As String)
            m_idprefix = value
        End Set
    End Property

    <TemplateContainer(GetType(ItemContainer))> _
    Public Property HeaderTemplate() As ITemplate
        Get
            Return m_headertemplate
        End Get
        Set(ByVal value As ITemplate)
            m_headertemplate = value
        End Set
    End Property

    <TemplateContainer(GetType(ItemContainer))> _
    Public Property ItemTemplate() As ITemplate
        Get
            Return m_itemtemplate
        End Get
        Set(ByVal value As ITemplate)
            m_itemtemplate = value
        End Set
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not (m_headertemplate Is Nothing) Then
            Dim container As New ItemContainer
            m_headertemplate.InstantiateIn(container)
            phHeader.Controls.Add(container)
        End If
        If Not (m_itemtemplate Is Nothing) Then
            Dim container As New ItemContainer
            m_itemtemplate.InstantiateIn(container)
            phContent.Controls.Add(container)
        End If
    End Sub

    Public Class HeaderContainer
        Inherits Control
        Implements INamingContainer

    End Class

    Public Class ItemContainer
        Inherits Control
        Implements INamingContainer

        Public ReadOnly Property Test() As Integer
            Get
                Return 7
            End Get
        End Property
    End Class

End Class
