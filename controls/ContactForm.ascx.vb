﻿Imports System.Configuration.ConfigurationManager
Imports System.Collections.Generic 
Imports System.IO
Imports ExpandIT
Imports ExpandIT.ExpandITLib
Imports System.Net.Mail
Imports ExpandIT31.ExpanditFramework.Util


Partial Class ContactFormCtrl
    Inherits ExpandIT.UserControl
    
    Private Const FILE_NAME_TEMPL AS String = "ContactTempl_{0}.html"
    private languageISO As String 
    private m_subj As string = ""
    Private m_SendToSelection As SendToAddrSelections = SendToAddrSelections.Ordering 
    
    Public Enum SendToAddrSelections As Byte 
      Ordering = 0 
      Support  = 1    	
      Complaints = 2
      Webmaster = 3
    End Enum
    
    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
      setCountries()
      setAddressList()      
      If Not CBoolEx(globals.User("Anonymous"))
        If Not IsPostBack Then    
          ResetForm()                  
        End If
        languageISO = CStrEx(globals.User("LanguageGuid")).ToLower()
      else 
        languageISO = CStrEx(HttpContext.Current.Session("LanguageGuid")).ToLower()      
      End If 
      If String.IsNullOrEmpty(m_subj)
        Subj  = Resources.Language.LABEL_CONTACT_MAIL_SUBJ   
      End If      
    End Sub
    
#Region "Properties"

    Public Property Subj As String
      Get
        Return m_subj
      End Get
      Set(ByVal value As String)
        m_subj = value
        subjTextBox.Text = m_subj
      End Set
    End Property
    
    Public Property SendToSelection As SendToAddrSelections
      Get
        Return m_SendToSelection
      End Get
      Set(ByVal value As SendToAddrSelections)
        m_SendToSelection = value
        SendTo.SelectedIndex = CByte(m_SendToSelection)
      End Set
    End Property

#End Region
    
    Private Sub ResetForm()
      CompanyName.Text = CStrEx(globals.User("CompanyName"))
      ContactName.Text = CStrEx(globals.User("ContactName"))
      Address1.Text = CStrEx(globals.User("Address1"))
      Zip_CityName.Text = CStrEx(globals.User("ZipCode")) & " " & CStrEx(globals.User("CityName"))
      CountryGuid.SelectedIndex  = CountryGuid.Items.IndexOf(CountryGuid.Items.FindByValue(CStrEx(globals.User("CountryGuid"))))
      PhoneNo.Text = CStrEx(globals.User("PhoneNo"))
      EmailAddress.Text = CStrEx(globals.User("EmailAddress"))       
      EmailContent.Text = ""
    End Sub
    
    Protected Sub setCountries()
      If Not IsPostBack Then
        Dim m_countries As ExpDictionary = eis.LoadCountries()

        For Each itm As ExpDictionary In m_countries.Values
            If Not itm("CountryGuid").Equals(DBNull.Value) Then
                    CountryGuid.Items.Add(New ListItem(itm("CountryName").ToString(), itm("CountryGuid").ToString()))
            End If
        Next

        If Not String.IsNullOrEmpty(globals.User("CountryGuid")) Then
            CountryGuid.SelectedValue = globals.User("CountryGuid")
        End If
      End if
    End Sub
    
    Protected Sub setAddressList() 
      If Not IsPostBack Then
            SendTo.Items.Add(New ListItem(Resources.Language.LABEL_CONTACT_RESELLER, AppSettings("MAIL_EMAIL_RESELLER"))) 'Should use Labels
            SendTo.Items.Add(New ListItem(Resources.Language.LABEL_CONTACT_SUPPORT, AppSettings("MAIL_EMAIL_SUPPORT")))
            SendTo.Items.Add(New ListItem(Resources.Language.LABEL_CONTACT_RECLAMATION, AppSettings("MAIL_EMAIL_RECLAMATION")))
            SendTo.Items.Add(New ListItem(Resources.Language.LABEL_CONTACT_WEBMASTER, AppSettings("MAIL_EMAIL_WEB_MASTER")))
        SendTo.SelectedIndex = CByte(m_SendToSelection)
      End If      
    End Sub

  Protected Sub BtnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSend.Click
    Dim recipient As New ExpDictionary()
    recipient.Add(SendTo.SelectedItem.Value, SendTo.SelectedItem.Text)     
    Dim fromAddress As String = CStrEx(AppSettings("MAIL_EMAIL_SENDAS"))    
    Dim mail As MailMessage = Mailer.CreateMailMessage(fromAddress, recipient, m_subj, true, Encoding.UTF8)
    Dim message As String = GenerateHtmlContent()
    If String.IsNullOrEmpty(message) OrElse message.StartsWith("ERROR")
      globals.messages.Errors.Add(Resources.Language.MESSAGE_MAIL_SEND_FAIL)
    Else
      Dim templDir As String = string.Format("~/{0}/", AppSettings("CONTACT_MAIL_FOLDER"))
      MailUtils.EmbedImages(message, templDir, mail) 
      MailUtils.ConfigMailer()
      if Mailer.SendEmail(AppSettings("MAIL_REMOTE_SERVER"), AppSettings("MAIL_REMOTE_SERVER_PORT"), mail, globals.messages)
        globals.messages.Messages.Add(Resources.Language.MESSAGE_MAIL_SEND_OK)
        ResetForm()
      Else
        globals.messages.Errors.Add(Resources.Language.MESSAGE_MAIL_SEND_FAIL)  
      End if
    End If    
  End Sub
  
  
  
  Private Function  CreateReplacementsList() As Dictionary(Of String, String)
    Dim replacements As New Dictionary(Of String, String)
    Dim builder As New StringBuilder(HttpContext.Current.Request.Url.Scheme)
    builder.Append("://")
    builder.Append(HttpContext.Current.Request.Url.Host)
    builder.Append(HttpContext.Current.Request.ApplicationPath)  
    replacements.Add("%ROOT%", builder.ToString())
    replacements.Add("%COMPANY%", CompanyName.Text) 
    replacements.Add("%CONTACT_NAME%", ContactName.Text) 
    replacements.Add("%ADDRESS1%", Address1.Text) 
    replacements.Add("%ZIPCODE_CITYNAME%", Zip_CityName.Text) 
    replacements.Add("%COUNTRY%", CountryGuid.SelectedItem.Text) 
    replacements.Add("%PHONENO%", PhoneNo.Text) 
    replacements.Add("%EMAIL%", EmailAddress.Text) 
    replacements.Add("%CONTENT%", EmailContent.Text) 
    Return replacements 
  End Function
  
  Protected Function GenerateContent() As String
    Dim result As String = ""
    Dim templDir As String = AppSettings("CONTACT_MAIL_FOLDER") 
    Dim shortTemplFileName As String = String.Format("ContactTempl_{0}.txt", languageISO)
    Dim phPath As String = HttpContext.Current.Server.MapPath(String.Format("~/{0}", templDir ))
    Dim fullTemplFileName As String = Path.Combine(phPath, shortTemplFileName)
    If (Not File.Exists(fullTemplFileName)) then 
      globals.messages.Errors.Add(String.Format("The mail template for the language {0} could not be found", languageISO))
    Else
      Dim contentTempl As string = ""
      Dim reader As StreamReader = Nothing
      Dim replacements As Dictionary(Of String, String)
      replacements = CreateReplacementsList()
      Try 
        reader = New StreamReader(fullTemplFileName)
        contentTempl = reader.ReadToEnd()
        reader.Close()        
        result = MailUtils.MakeReplacements(contentTempl, replacements) 
      Catch ex As Exception
        globals.messages.Errors.Add("The mail template could not be read.")
      Finally
        If Not reader Is Nothing
          reader.Dispose()
          reader = Nothing
        End If
      End Try
    End If    
    Return result
  End Function
  
  
  
  Protected Function GenerateHtmlContent() As String
    Dim result As String = ""
    Dim standardLanguageISO As String = "en"
    Dim templDir As String = AppSettings("CONTACT_MAIL_FOLDER")
    Dim templFileName As String = String.Format(FILE_NAME_TEMPL, languageISO)
    Dim stdTemplFileName As String =  String.Format(FILE_NAME_TEMPL, standardLanguageISO)
    Dim templPath As String = String.Format("~/{0}/{1}", templDir, templFileName)
    Dim stdTemplPath As String = String.Format("~/{0}/{1}", templDir, stdTemplFileName)
    Dim templFilePath As String = Nothing
    if ExpandIT31.ExpanditFramework.Util.FileReader.fileHasContent(templPath)
      templFilePath = templPath
    Else If ExpandIT31.ExpanditFramework.Util.FileReader.fileHasContent(stdTemplPath)
      templFilePath = stdTemplPath
    End If
    If  templFilePath IsNot Nothing
      result = MailUtils.EmbedCss(templFilePath, Nothing)
      Dim replacements As Dictionary(Of String, String)
      replacements = CreateReplacementsList()
      result = MailUtils.MakeReplacements(result, replacements)
    End If
    Return result
  End Function
  
  
  
End Class
