﻿<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="NoteLine.ascx.vb" Inherits="NoteLine" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<asp:FormView ID="fvProduct" runat="server" SkinID="NoteLine" Width="100%" CssClass="NoteLineFormView" CellPadding="0">
    <ItemTemplate>
        <div class ="NoteLine">
            <div class="NoteLineDelete">
                <div class="List_Item">
                    <asp:ImageButton ID="DeleteBtn" runat="server" ImageUrl="~/images/p.gif" CssClass="NoteLineDelete" />
                </div>
            </div>
            <asp:Panel ID="panelProductGuid" runat="server" Visible='<%# AppSettings("HIDE_PRODUCTGUID") <> "1"  %>'>
                <div class="NoteLineProductGuid">
                    <div class="List_Item">
                        <asp:HyperLink ID="HyperLink3" runat="server" CssClass="NoteLineProductName"
                            NavigateUrl='<%# Eval("ProductLinkURL") %>'><%#Eval("Guid")%></asp:HyperLink>
                    </div>
                </div>
            </asp:Panel>
            <div class="NoteLineProductName">
                <div class="List_Item">
                    <asp:HyperLink ID="HyperLink1" runat="server" CssClass="NoteLineProductName"
                        NavigateUrl='<%# Eval("ProductLinkURL") %>'><%# Eval("Name") %></asp:HyperLink>
                </div>
            </div>
            <div class="NoteLinePrice">
                <asp:HyperLink ID="hlPrice" runat="server" 
                    Text='<%# CurrencyFormatter.FormatCurrency(CDblEx(Eval("Price")), Session("UserCurrencyGuid").ToString) %>' NavigateUrl='<%# ProductLink(Eval("Guid"), globals.groupguid) %>'></asp:HyperLink>
            </div>
            <%-- 
            <asp:Panel ID="panelSecondaryPrice" runat="server" Visible='<%# Eval("ShowSecondaryPrice") %>'>
                <div class="NoteLinePrice">
                    <asp:HyperLink ID="hlSecondaryPrice" runat="server"
                        Text='<%# CurrencyFormatter.FormatCurrency(CDblEx(Eval("SecondaryPrice")), Session("UserCurrencyGuid").ToString) %>' NavigateUrl='<%# ProductLink(Eval("Guid"), globals.groupguid) %>'></asp:HyperLink>
                </div>
            </asp:Panel>
            --%>
            <div class="NoteLineQuantity">
                <div class="List_Item">
                    <input type="hidden" name="SKU" value="<%#Eval("Guid")%>" />
                    <input class="NoteLineQuantity noteQuantity" type="text" name="Quantity" size="4" value="" />
                </div>
            </div>
        </div>
    </ItemTemplate> 
</asp:FormView>
    