﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SearchResultList.ascx.vb"
    Inherits="controls_SearchResultList" %>
<%@ Register Namespace="ExpandIT.SimpleUIControls" TagPrefix="expui" %>
<%@ Register Src="AddToCartButton.ascx" TagName="AddToCartButton" TagPrefix="uc1" %>
<asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False"
    DataSourceID="dsSearchResults" AllowSorting="True" PageSize="20" GridLines="None"
    CssClass="SearchResultGrid" ShowFooter="True" SkinID="SearchResultGrid" Width="100%">
    <Columns>
        <asp:HyperLinkField DataNavigateUrlFields="ProductLinkURL" DataTextField="ThumbnailURL"
            SortExpression="Name" DataTextFormatString='{0}'>
            <HeaderStyle CssClass="SearchResultThumbnail"></HeaderStyle>
            <ItemStyle CssClass="SearchResultThumbnail"></ItemStyle>
        </asp:HyperLinkField>
        <asp:HyperLinkField DataNavigateUrlFields="ProductLinkURL" DataTextField="Guid" HeaderText="<%$ Resources:Language, LABEL_ITEM %>"
            SortExpression="Guid">
            <HeaderStyle CssClass="SearchResultProductGuid"></HeaderStyle>
            <ItemStyle CssClass="SearchResultProductGuid"></ItemStyle>
        </asp:HyperLinkField>
        <asp:HyperLinkField DataNavigateUrlFields="ProductLinkURL" DataTextField="Name" HeaderText="<%$ Resources:Language, LABEL_NAME %>"
            SortExpression="Name">
            <ItemStyle CssClass="SearchResultProductName"></ItemStyle>
        </asp:HyperLinkField>
        <asp:HyperLinkField DataNavigateUrlFields="ProductLinkURL" DataTextField="Description"
            HeaderText="<%$ Resources:Language, LABEL_DESCRIPTION %>" SortExpression="Description">
            <ItemStyle CssClass="SearchResultDescription"></ItemStyle>
        </asp:HyperLinkField>
        <%-- <asp:BoundField DataField="Description" HtmlEncode="False" HeaderText="<%$ Resources:Language, LABEL_DESCRIPTION %>"
            SortExpression="Description" ReadOnly="True">
            <ItemStyle CssClass="SearchResultDescription"></ItemStyle>
        </asp:BoundField>--%>
        <asp:HyperLinkField DataNavigateUrlFields="ProductLinkURL" DataTextField="PriceFormatted" 
            HeaderText="<%$ Resources:Language, LABEL_PRICE %>" SortExpression="Price">
            <ItemStyle CssClass="SearchResultPrice"></ItemStyle>
        </asp:HyperLinkField>
        <%--AM2010051801 - SPECIAL ITEMS - Start--%>
        <%--AM2010051802 - WHAT'S NEW - Start--%>
        <asp:BoundField DataField="LastName" HeaderText=" " ReadOnly="True" SortExpression="LastName">
            <ItemStyle CssClass="SpecialPrice"></ItemStyle>
        </asp:BoundField>
        <%--AM2010051802 - WHAT'S NEW - End--%>
        <%--AM2010051801 - SPECIAL ITEMS - End--%>
    </Columns>
    <EmptyDataTemplate>
        <asp:Label ID="lblEmptyResult" runat="server" Text='<%$Resources: Language, LABEL_NO_PRODUCTS_MATCHED_THE_SEARCH %>'></asp:Label>
    </EmptyDataTemplate>
</asp:GridView>
<asp:ObjectDataSource ID="dsSearchResults" runat="server" SelectMethod="GetResults"
    TypeName="ExpandIT.USSearch">
    <SelectParameters>
        <asp:QueryStringParameter Name="searchstring1" DefaultValue="" QueryStringField="searchstring"
            Type="String" />
        <asp:FormParameter Name="searchstring2" DefaultValue="" FormField="searchstring"
            Type="String" />
        <asp:FormParameter Name="searchstring3" DefaultValue="" FormField="mainsearchstring"
            Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>