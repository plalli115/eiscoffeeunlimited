Imports System.Configuration.ConfigurationManager
Imports System.Data
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

Partial Class LanguageDropDown
    Inherits ExpandIT.UserControl

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not New LanguageBoxLogic().isVisible(globals.User) Then
            Me.Visible = False
            Exit Sub
        End If
        If Not Me.Page.IsPostBack Then
            Dim dt As DataTable = eis.LoadLanguages(True)
            Me.dp1.DataSource = dt
            dp1.DataTextField = "LanguageName"
            dp1.DataValueField = "LanguageGuid"
            Try
                dp1.DataBind()
            Catch ex As Exception

            End Try
        End If
    End Sub

    Protected Overloads Sub Page_Prerender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        If Not New LanguageBoxLogic().isVisible(globals.User) Then
            Me.Visible = False
            Exit Sub
        End If

        If Me.Page.IsPostBack Then
            If Request.Form("__EVENTTARGET") = Me.dp1.UniqueID OrElse Request.Form("__EVENTTARGET") = Me.dp1.ClientID OrElse Request.Form("__EVENTTARGET") = Me.dp1.ID Then
                HttpContext.Current.Session("LanguageGuid") = dp1.SelectedValue
                eis.SetUserLanguage(HttpContext.Current.Session("LanguageGuid").ToString, True)
                Response.Redirect(Request.RawUrl, True)
            End If
        End If

    End Sub

    Protected Sub dp1_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles dp1.DataBound
        Try
            If HttpContext.Current.Session("LanguageGuid") IsNot Nothing Then
                Me.dp1.SelectedValue = LCase(HttpContext.Current.Session("LanguageGuid").ToString)
            ElseIf DBNull2Nothing(globals.User("LanguageGuid")) IsNot Nothing Then
                dp1.SelectedValue = LCase(CStrEx(globals.User("LanguageGuid")))
            ElseIf HttpContext.Current.Request.Cookies("user")("LanguageGuid") IsNot Nothing Then
                Me.dp1.SelectedValue = LCase(HttpContext.Current.Request.Cookies("user")("LanguageGuid"))
            Else
                Me.dp1.SelectedValue = LCase(AppSettings("LANGUAGE_DEFAULT"))
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
            Me.dp1.SelectedValue = LCase(AppSettings("LANGUAGE_DEFAULT"))
        End Try
    End Sub
End Class
