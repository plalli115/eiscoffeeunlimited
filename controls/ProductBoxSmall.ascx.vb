Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass

Partial Class ProductBoxSmall
    Inherits ExpandIT.UserControl

    Private m_product As ExpDictionary
    Protected m_productclass As ProductClass

    <Themeable(False)> _
    Public Property Product() As ExpDictionary
        Get
            Return m_product
        End Get
        Set(ByVal value As ExpDictionary)
            m_product = value
        End Set
    End Property

    Public ReadOnly Property ThumbnailURL() As String
        Get
            Return IIf(m_product("PICTURE2") <> "", PictureLink(m_product("PICTURE2")), PictureLink("images/nopix75.jpg"))
        End Get
    End Property

    Protected Overrides Sub OnPreRender(ByVal e As System.EventArgs)
        MyBase.OnPreRender(e)

        m_productclass = New ProductClass(m_product)
        m_productclass.GroupGuid = globals.GroupGuid
        fvProduct.DataSource = New ProductClass() {m_productclass}
        fvProduct.DataBind()


    End Sub

    Protected Function getStyle()
        Dim style As String = m_productclass.ThumbnailURLStyle(100, 100)
        Return style
    End Function

End Class
