<%@ Import Namespace="System.Configuration.ConfigurationManager" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="shippinghandling.aspx.vb"
    Inherits="shippinghandling" CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master"
    RuntimeMasterPageFile="ThreeColumn.master" CrumbName="<%$ Resources: Language, LABEL_SHIPPING_AND_HANDLING %>" %>

<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <div class="ShippingHandlingPage">
        <uc1:PageHeader ID="PageHeader1" runat="server" Text="<%$ Resources: Language, LABEL_SHIPPING_AND_HANDLING %>"
            EnableTheming="true" />
        <% =Resources.Language.MESSAGE_SHIPPING_SELECTED_ON_PURCHASE%>
        <br />
        <br />
        <%
            If dictSH.Count = 0 Then%>
        <% =Resources.Language.LABEL_NO_SHIPPING_AND_HANDLING_INFO_AVAILABLE%>
        <%
        Else%>
        <table cellspacing="0" cellpadding="2">
            <%
                For Each aKey In dictSH%>
            <tr>
                <th class="THL">
                    <% =Resources.Language.LABEL_INFORMATION%>
                </th>
                <th class="THL">
                    <% =Resources.Language.LABEL_PRICELIST%>
                </th>
            </tr>
            <tr>
                <td valign="top">
                    <br />
                    <b>
                        <% =eis.GetLabel(dictSH(aKey.Key)("ProviderName"))%>
                    </b>
                    <br />
                    <br />
                    <% =HTMLFormatSHDescription(eis.GetLabel(dictSH(aKey.Key)("ProviderDescription")))%>
                </td>
                <td valign="top" align="right">
                    <table cellspacing="0" cellpadding="2">
                        <tr>
                            <th colspan="3">
                                <% =Resources.Language.LABEL_INTERVAL%>
                            </th>
                            <th>
                                <% =Resources.Language.LABEL_SHIPPING%>
                            </th>
                            <th>
                                <% =Resources.Language.LABEL_HANDLING%>
                            </th>
                        </tr>
                        <% 
                            prevVal = -1 * nCurrDiff
                            ' Prepare format of rows.
                            eis.InitNextRowAB()
                            For Each aKey2 In dictSH(aKey.Key)("Pricelist")%>
                        <tr class="TR<% = globals.stylesheetrowcounter %>">
                            <td class="tdr" width="80">
                                &nbsp;<% =CurrencyFormatter.FormatAmount(currency.ConvertCurrency((prevVal + nCurrDiff), globals.Context("DefaultCurrency"), globals.Context("CurrencyGuid")), Session("UserCurrencyGuid").ToString)%>
                            </td>
                            <td class="tdr">
                                &nbsp;-&nbsp;
                            </td>
                            <td class="tdr" width="80">
                                &nbsp;<% 
                                          If Not IsNull(dictSH(aKey.Key)("Pricelist")(aKey2.Key)("CalculatedValue")) Then%>
                                <% =CurrencyFormatter.FormatAmount(currency.ConvertCurrency(dictSH(aKey.Key)("Pricelist")(aKey2.Key)("CalculatedValue"), globals.Context("DefaultCurrency"), globals.Context("CurrencyGuid")), Session("UserCurrencyGuid").ToString)%>
                                <%
                                End If%>
                            </td>
                            <td class="tdr" width="80">
                                &nbsp;<% =CurrencyFormatter.FormatAmount(currency.ConvertCurrency(dictSH(aKey.Key)("Pricelist")(aKey2.Key)("ShippingAmount"), globals.Context("DefaultCurrency"), globals.Context("CurrencyGuid")), Session("UserCurrencyGuid").ToString)%>
                            </td>
                            <td class="tdr" width="80">
                                &nbsp;<% =CurrencyFormatter.FormatAmount(currency.ConvertCurrency(dictSH(aKey.Key)("Pricelist")(aKey2.Key)("HandlingAmount"), globals.Context("DefaultCurrency"), globals.Context("CurrencyGuid")), Session("UserCurrencyGuid").ToString)%>
                            </td>
                        </tr>
                        <%
                            prevVal = dictSH(aKey.Key)("Pricelist")(aKey2.Key)("CalculatedValue")
                            eis.NextRowAB()
                        Next%>
                    </table>
                </td>
            </tr>
            <% 
            Next%>
        </table>
        <%
        End If%>
    </div>
</asp:Content>
