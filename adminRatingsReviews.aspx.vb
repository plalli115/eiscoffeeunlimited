Imports System.Configuration.ConfigurationManager
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT

Partial Class adminRatingsReviews
    Inherits ExpandIT.Page

    Protected showDict As ExpDictionary


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim ScriptManager1 As ScriptManager = ScriptManager.GetCurrent(Me.Page)

        ScriptManager1.RegisterAsyncPostBackControl(Me.ShowAll)

        ScriptManager1.RegisterAsyncPostBackControl(Me.ShowByUserId)

        ScriptManager1.RegisterAsyncPostBackControl(Me.ShowByGuid)

        ScriptManager1.RegisterAsyncPostBackControl(Me.ShowByDate)


        If Not Page.IsPostBack Then
            Dim ratingGuid As String = ""
            Dim user As String = ""
            Dim sql As String = ""
            If CStrEx(Request("delete")) <> "" Then
                ratingGuid = CStrEx(Request("RatingGuid"))
                sql = "SELECT Rating FROM ProductRatings WHERE RatingGuid=" & SafeString(ratingGuid)
                Dim rate As String = CStrEx(getSingleValueDB(sql))
                If rate <> "" Then
                    sql = "DELETE FROM ProductReviews WHERE RatingGuid=" & SafeString(ratingGuid)
                    excecuteNonQueryDb(sql)
                    sql = "DELETE FROM ProductRatings WHERE RatingGuid=" & SafeString(ratingGuid)
                    excecuteNonQueryDb(sql)
                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script1", "clickShowAll();", True)

                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script2", "showDeleteMsg();", True)
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script3", "hideDeleteMsg();", True)
                End If
            ElseIf CStrEx(Request("find")) <> "" Then
                Dim newUser As String = ""
                sql = "SELECT UserLogin FROM UserTable WHERE UserGuid =" & SafeString(CStrEx(Request("UserGuid")))
                newUser = getSingleValueDB(sql)
                Me.UserId.Text = CStrEx(newUser)
                If Not Page.IsPostBack Then
                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script1", "clickShowByUser();", True)
                End If
                Me.UpdatePanel1.Update()
            ElseIf CStrEx(Request("ShowByGuid")) <> "" Then
                Me.TbxShowByGuid.Text = CStrEx(Request("ProductGuid"))
                If Not Page.IsPostBack Then
                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script1", "clickShowByProductGuid();", True)
                End If
            ElseIf CStrEx(Request("ShowByDate")) <> "" Then
                Dim scriptTxt As String = "clickShowByDate('" & CStrEx(Request("Start")) & "','" & CStrEx(Request("End")) & "');"
                If Not Page.IsPostBack Then
                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script1", scriptTxt, True)
                End If
            ElseIf CStrEx(Request("ShowByUser")) <> "" Then
                Me.UserId.Text = CStrEx(Request("UserLogin"))
                If Not Page.IsPostBack Then
                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script1", "clickShowByUser();", True)
                End If
            ElseIf CStrEx(Request("ShowAll")) <> "" Then
                If Not Page.IsPostBack Then
                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script1", "clickShowAll();", True)
                End If
            End If
        End If

    End Sub

    Protected Sub ShowAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ShowAll.Click

        Dim query As String = "SELECT REV.ReviewGuid, REV.ProductGuid, REV.UserGuid, REV.RatingGuid, REV.ReviewTitle, REV.Review, REV.ShowName, REV.CreatedOne, RAT.Rating" & _
                                      " FROM ProductReviews AS REV INNER JOIN  ProductRatings AS RAT ON REV.RatingGuid = RAT.RatingGuid" & _
                                      " ORDER BY REV.ProductGuid, RAT.Rating DESC"
        showDict = SQL2Dicts(query)

        Me.InvalidGuid.Visible = False

        Me.invalidLoginUser.Visible = False

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script1", "hideNoRecordsFound();", True)

        Me.UpdatePanel1.Update()

    End Sub

    Protected Sub ShowByGuid_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ShowByGuid.Click
        Dim guid As String = Me.TbxShowByGuid.Text
        Dim query As String = ""
        If CStrEx(guid) <> "" Then
            query = "SELECT REV.ReviewGuid, REV.ProductGuid, REV.UserGuid, REV.RatingGuid, REV.ReviewTitle, REV.Review, REV.ShowName, REV.CreatedOne, RAT.Rating" & _
                              " FROM ProductReviews AS REV INNER JOIN  ProductRatings AS RAT ON REV.RatingGuid = RAT.RatingGuid" & _
                              " WHERE REV.ProductGuid=" & SafeString(guid) & _
                              " ORDER BY REV.ProductGuid, RAT.Rating DESC"
        Else
            query = "SELECT REV.ReviewGuid, REV.ProductGuid, REV.UserGuid, REV.RatingGuid, REV.ReviewTitle, REV.Review, REV.ShowName, REV.CreatedOne, RAT.Rating" & _
                              " FROM ProductReviews AS REV INNER JOIN  ProductRatings AS RAT ON REV.RatingGuid = RAT.RatingGuid" & _
                              " ORDER BY REV.ProductGuid, RAT.Rating DESC"
        End If

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script1", "hideNoRecordsFound();", True)

        showDict = SQL2Dicts(query)

        If showDict.Count > 0 Then
            Me.InvalidGuid.Visible = False
        Else
            Me.InvalidGuid.Visible = True
        End If

        Me.invalidLoginUser.Visible = False

        Me.UpdatePanel1.Update()


    End Sub

    Protected Sub ShowByDate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ShowByDate.Click

        Dim startDate As String = Request("StartDate")
        Dim endDate As String = Request("EndDate")
        Dim query As String = ""

        If CStrEx(startDate) <> "" And CStrEx(endDate) <> "" Then
            query = "SELECT REV.ReviewGuid, REV.ProductGuid, REV.UserGuid, REV.RatingGuid, REV.ReviewTitle, REV.Review, REV.ShowName, REV.CreatedOne, RAT.Rating" & _
                              " FROM ProductReviews AS REV INNER JOIN  ProductRatings AS RAT ON REV.RatingGuid = RAT.RatingGuid" & _
                              " WHERE REV.CreatedOne >=" & SafeString(CDate(startDate)) & " AND REV.CreatedOne <=" & SafeString(CDate(endDate)) & _
                              " ORDER BY REV.ProductGuid, RAT.Rating DESC"
        ElseIf CStrEx(startDate) <> "" Then
            query = "SELECT REV.ReviewGuid, REV.ProductGuid, REV.UserGuid, REV.RatingGuid, REV.ReviewTitle, REV.Review, REV.ShowName, REV.CreatedOne, RAT.Rating" & _
                              " FROM ProductReviews AS REV INNER JOIN  ProductRatings AS RAT ON REV.RatingGuid = RAT.RatingGuid" & _
                              " WHERE REV.CreatedOne >=" & SafeString(startDate) & _
                              " ORDER BY REV.ProductGuid, RAT.Rating DESC"
        End If

        showDict = SQL2Dicts(query)

        If showDict.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script1", "hideNoRecordsDate();", True)
        Else
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script2", "showNoRecordsDate();", True)
        End If

        Me.invalidLoginUser.Visible = False


        Me.UpdatePanel1.Update()


    End Sub

    Protected Sub ShowByUserId_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ShowByUserId.Click
        Dim user As String = Me.UserId.Text
        Dim query As String = ""
        Dim newUser As String = ""

        query = "SELECT UserGuid FROM UserTable WHERE UserLogin =" & SafeString(CStrEx(user))
        newUser = getSingleValueDB(query)
        If CStrEx(newUser) <> "" Then
            query = "SELECT REV.ReviewGuid, REV.ProductGuid, REV.UserGuid, REV.RatingGuid, REV.ReviewTitle, REV.Review, REV.ShowName, REV.CreatedOne, RAT.Rating" & _
                              " FROM ProductReviews AS REV INNER JOIN  ProductRatings AS RAT ON REV.RatingGuid = RAT.RatingGuid" & _
                              " WHERE REV.UserGuid=" & SafeString(newUser) & _
                              " ORDER BY REV.ProductGuid, RAT.Rating DESC"
        Else
            Me.invalidLoginUser.Visible = True
        End If

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script1", "hideNoRecordsFound();", True)

        showDict = SQL2Dicts(query)

        If showDict.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script2", "hideInvalidUser();", True)
        Else
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script3", "showInvalidUser();", True)
        End If


        Me.UpdatePanel1.Update()


    End Sub

    Protected Function getMailto(ByVal userGuid As String)
        Dim email As String = CStrEx(getSingleValueDB("SELECT EmailAddress FROM UserTable WHERE UserGuid=" & SafeString(CStrEx(userGuid))))
        Dim link As String = "mailto:" & email
        Return link
    End Function

End Class
