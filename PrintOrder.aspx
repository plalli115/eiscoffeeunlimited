<%--AM2010080201 - ORDER PRINT OUT - Start--%>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PrintOrder.aspx.vb" Inherits="PrintOrder" %>

<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%--AM2010031501 - ORDER RENDER - Start--%>
<%@ Register Src="~/controls/USOrderRender/CartRender.ascx" TagName="CartRender"
    TagPrefix="uc1" %>
<%--AM2010031501 - ORDER RENDER - End--%>
<html>
<head id="Head1" runat="server">

    <script language="Javascript1.2">
        function Print() {
            window.print();  
        }
    </script>

</head>
<body onload="Print();" style="background-color: #FFFFFF; background-image: none;">
    <%--<img src="App_Themes/EnterpriseBlue/logo.gif" />--%>
    <div class="PaymentPage" style="padding-left: 5px;">
        <table style="width: 500px; height: 132px;">
            <tr>
                <td id="LogoIndex">
                    <img alt="" src="images/p.gif" />
                </td>
            </tr>
        </table>
        <br />
        <br />
        <br />
        <!-- AM2010031501 - ORDER RENDER - Start -->
        <table style="width: 600px;">
            <tr>
                <td>
                    <uc1:CartRender ID="CartRender1" runat="server" IsActiveOrder="false" EditMode="false"
                        IsEmail="false" ShowDate="true" ShowPaymentInfo="true" />
                </td>
            </tr>
        </table>
        <!-- AM2010031501 - ORDER RENDER - End -->
        <br />
    </div>
</body>
</html>
<%--AM2010080201 - ORDER PRINT OUT - End--%>
