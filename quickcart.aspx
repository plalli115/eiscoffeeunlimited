<%--
Template information used by the Catalog Manager
#NAME=Price List
#DESCRIPTION=Displays a list of products with price information.
#GROUPTEMPLATE=TRUE
#PRODUCTTEMPLATE=FALSE
--%>
<%@ Import Namespace="System.Configuration.ConfigurationManager" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="quickcart.aspx.vb" Inherits="quickcart"
    CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master"
    RuntimeMasterPageFile="ThreeColumn.master" CrumbName="<%$ Resources: Language, LABEL_QUICK_CART %>" %>

<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.GlobalsClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Register Src="~/controls/AddToCartButton.ascx" TagName="AddToCartButton" TagPrefix="uc1" %>
<%@ Register Namespace="ExpandIT.SimpleUIControls" TagPrefix="expui" %>
<%@ Register Namespace="ExpandIT.Buttons" TagPrefix="exbtn" %>
<%@ Register Src="~/controls/Message.ascx" TagName="Message" TagPrefix="uc1" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <br />
    <%  Me.Label1.Text = Resources.Language.LABEL_QUICK_CART%>
    <asp:Label ID="Label1" Style="padding-left: 50px; color: Black; font-weight: bold;"
        runat="server"></asp:Label>
    <br />
    <asp:Panel DefaultButton="Update" ID="Panel1" runat="server">
        <table style="margin-left: 40px; margin-top: 30px; height: 260px;">
            <tr>
                <td>
                    <table border="0" cellspacing="0">
                        <tr>
                            <th class="THC">
                                <a style="color: Black;">#</a>
                            </th>
                            <th class="THC">
                                <a style="color: Black;">Stock#</a>
                            </th>
                            <th class="THC">
                                <a style="color: Black;">Qty.</a>
                            </th>
                            <th class="TDSP">
                                &nbsp;&nbsp;</th>
                            <th class="THC">
                                <a style="color: Black;">#</a>
                            </th>
                            <th class="THC">
                                <a style="color: Black;">Stock#</a>
                            </th>
                            <th class="THC">
                                <a style="color: Black;">Qty.</a>
                            </th>
                        </tr>
                        <% 
                            Dim intSide As Integer = 1
                            Dim iCount As Integer = 1
                        %>
                        <%
                            For Each CartLine In globals.OrderDict("Lines").Values%>
                        <% If (globals.OrderDict("Lines").count >= iCount) And (intSide = 1) Then%>
                        <tr>
                            <td class="TDR">
                                &nbsp;<%=iCount%></td>
                            <td class="TDL" style="padding-right: 3px;">
                                <input class="InpL" type="text" name="sku<%=iCount%>" size="10" value="<% = CartLine("ProductGuid") %>" /></td>
                            <td class="TDR">
                                <input class="InpR" type="text" name="Quantity<%=iCount%>" size="4" value="<% = CartLine("Quantity") %>" /></td>
                            <%intSide = 2%>
                            <% ElseIf (intSide = 2) Then%>
                            <td class="TDSP">
                                &nbsp;</td>
                            <td class="TDR">
                                &nbsp;<%=iCount%></td>
                            <td class="TDL" style="padding-right: 3px;">
                                <input class="InpL" type="text" name="sku<%=iCount%>" size="10" value="<% = CartLine("ProductGuid") %>" /></td>
                            <td class="TDR">
                                <input class="InpR" type="text" name="Quantity<%=iCount%>" size="4" value="<% = CartLine("Quantity") %>" /></td>
                            <%intSide = 1%>
                        </tr>
                        <%End If%>
                        <%                                         
                            Response.Write("<input type='hidden' name='LineGuid" & CStr(iCount) & "' VALUE=" & CartLine("LineGuid") & " />")
                            Response.Write("<input type='hidden' name='VersionGuid" & CStr(iCount) & "' VALUE=" & CartLine("VersionGuid") & " />")
                            Response.Write("<input type='hidden' name='VariantCode" & CStr(iCount) & "' VALUE=" & CartLine("VariantCode") & " />")
                            Response.Write("<input type='hidden' name='Quantity_prev" & CStr(iCount) & "' VALUE=" & CartLine("Quantity") & " />")
                                                                                                        
                            iCount = iCount + 1%>
                        <%Next%>
                        <% 
                            If Not ((globals.OrderDict("Lines").Count <= iCount) And (intSide <> 2)) Then%>
                        <%Response.Write("<td class=""TDSP"">")%>
                        <%Response.Write("&nbsp;</td>")%>
                        <%Response.Write("<td class=""TDR"">")%>
                        <%Response.Write("&nbsp;" & iCount & "</td>")%>
                        <%Response.Write("<td class=""TDL""  style=""padding-right: 3px;"">")%>
                        <%Response.Write("<input class=""InpL"" type=""text"" name=""sku" & iCount & """ size=""10"" value="""" /></td>")%>
                        <%Response.Write("<td class=""TDR"">")%>
                        <%Response.Write("<input class=""InpR"" type=""text"" name=""Quantity" & iCount & """ size=""4"" value="""" /></td>")%>
                        <%Response.Write("</tr>")%>
                        <%intSide = 1%>
                        <% iCount = iCount + 1%>
                        <%End If%>
                        <% 
                            While iCount <= CIntEx(AppSettings("NUMBER_OF_ITEMS_QUICK_CART"))%>
                        <%If (intSide = 1) Then%>
                        <tr class="TR1">
                            <td class="TDR">
                                &nbsp;<%=iCount%></td>
                            <td class="TDL" style="padding-right: 3px;">
                                <input class="InpL" type="text" name="sku<%=iCount%>" size="10" value="" /></td>
                            <td class="TDR">
                                <input class="InpR" type="text" name="Quantity<%=iCount%>" size="4" value="" /></td>
                            <% intSide = 2%>
                            <%ElseIf (intSide = 2) Then%>
                            <td class="TDSP">
                                &nbsp;</td>
                            <td class="TDR">
                                &nbsp;<%=iCount%></td>
                            <td class="TDL" style="padding-right: 3px;">
                                <input class="InpL" type="text" name="sku<%=iCount%>" size="10" value="" /></td>
                            <td class="TDR">
                                <input class="InpR" type="text" name="Quantity<%=iCount%>" size="4" value="" /></td>
                            <%                      intSide = 1%>
                            <%End If%>
                            <%iCount = iCount + 1%>
                            <%End While%>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <!-- Here begins HTML code for displaying actions buttons -->
        <table border="0" cellspacing="3" style="margin-left: 40px;">
            <tr>
                <td>
                    <input class="AddButton" type="reset" value="Undo Changes" name="reset1" /></td>
                <td>
                    <asp:Button runat="server" CssClass="AddButton" ID="Update" Text="<%$Resources: Language,ACTION_UPDATE%>"
                        PostBackUrl="~/quickcart.aspx?update=update" />
                    <%--<input class="BtnC" type="submit" value="Update" name="update" id="Submit1" />--%>
                </td>
                <td>
                    <asp:Button runat="server" CssClass="AddButton" ID="Purchase" Text="<%$Resources: Language,LABEL_MINI_CART_HEAD%>"
                        PostBackUrl="~/quickcart.aspx?orderpad=orderpad" />
                    <%--<input class="BtnC" type="submit" value="Shopping Cart" name="orderpad" />--%>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
