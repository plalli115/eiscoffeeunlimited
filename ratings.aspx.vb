﻿'JA2010071601 - RATINGS AND REVIEWS - START
Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Net.Mail
Imports ExpandIT31.ExpanditFramework.Util

Partial Class ratings
    Inherits ExpandIT.Page

    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	
	Protected Overloads Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        Dim sql As String = ""
        Dim ProductGuid As String = ""
        Dim RatingGuid As String = ""
        Dim myRate As String = ""
        If CStrEx(Request("SetStars")) = "1" Then
            ProductGuid = Request("ProductGuid")
            myRate = Request("ratingValue")
            sql = "SELECT * FROM ProductRatings WHERE UserGuid=" & SafeString(globals.User("UserGuid")) & " AND ProductGuid=" & SafeString(Request("ProductGuid"))
            Dim dict As ExpDictionary = SQL2Dicts(sql)
            If Not dict Is Nothing And Not dict Is DBNull.Value And dict.Count > 0 Then
                sql = "UPDATE ProductRatings SET Rating=" & SafeString(myRate) & " WHERE UserGuid=" & SafeString(globals.User("UserGuid")) & " AND ProductGuid=" & SafeString(Request("ProductGuid"))
                excecuteNonQueryDb(sql)
            Else
                RatingGuid = GenGuid()
                sql = "INSERT INTO ProductRatings (RatingGuid, ProductGuid, UserGuid, Rating, CreatedOn)" & _
                " VALUES (" & SafeString(RatingGuid) & "," & SafeString(ProductGuid) & "," & SafeString(globals.User("UserGuid")) & "," & SafeString(myRate) & "," & SafeString(DateTime.Now) & " )"
                excecuteNonQueryDb(sql)
            End If
        ElseIf CStrEx(Request("SetForm")) = "1" Then
            Dim title As String = CStrEx(Request("Title"))
            Dim reviewText As String = CStrEx(Request("Comment"))
            Dim ShowMyName As Integer = Request("CheckBox1")

            SendEmail(title, reviewText, Request("ProductGuid"))


            sql = "SELECT * FROM ProductReviews WHERE UserGuid=" & SafeString(globals.User("UserGuid")) & " AND ProductGuid=" & SafeString(Request("ProductGuid"))
            Dim dict2 As ExpDictionary = SQL2Dicts(sql)
            If Not dict2 Is Nothing And Not dict2 Is DBNull.Value And dict2.Count > 0 Then
                sql = "UPDATE ProductReviews SET ReviewTitle=" & SafeString(title) & ", Review=" & SafeString(reviewText) & ", ShowName=" & ShowMyName & ", CreatedOne=" & SafeString(DateTime.Now) & " WHERE UserGuid=" & SafeString(globals.User("UserGuid")) & " AND ProductGuid=" & SafeString(Request("ProductGuid"))
                excecuteNonQueryDb(sql)
            Else
                sql = "SELECT * FROM ProductRatings WHERE UserGuid=" & SafeString(globals.User("UserGuid")) & " AND ProductGuid=" & SafeString(Request("ProductGuid"))
                Dim dict As ExpDictionary = SQL2Dicts(sql)
                If Not dict Is Nothing And Not dict Is DBNull.Value And dict.Count > 0 Then
                    sql = "INSERT INTO ProductReviews (ReviewGuid, ProductGuid, UserGuid, RatingGuid, ReviewTitle, Review, ShowName, CreatedOne)" & _
                    " VALUES (" & SafeString(GenGuid()) & "," & SafeString(Request("ProductGuid")) & "," & SafeString(globals.User("UserGuid")) & "," & SafeString(CStrEx(dict.Item("1")("RatingGuid"))) & _
                    "," & SafeString(title) & "," & SafeString(reviewText) & "," & ShowMyName & "," & SafeString(DateTime.Now) & " )"
                    excecuteNonQueryDb(sql)
                End If
            End If
        End If
    End Sub

    Sub SendEmail(ByVal title As String, ByVal reviewText As String, ByVal guid As String)
        Dim emailFrom As String = CStrEx(AppSettings("RATINGS_AND_REVIEWS_EMAIL"))
        Dim emailTo As String = CStrEx(AppSettings("RATINGS_AND_REVIEWS_EMAIL"))
        Dim userLogin As String = CStrEx(globals.User("UserLogin"))
        Dim subject As String = Resources.Language.LABEL_ADMIN_REVIEWS_EMAIL_SUBJECT & "-" & guid
        Dim builder As New StringBuilder()

        Dim rating As String = getSingleValueDB("SELECT Rating FROM ProductRatings WHERE UserGuid=" & SafeString(CStrEx(globals.User("UserGuid"))) & " AND ProductGuid=" & SafeString(guid))

        If reviewText.Contains("*") Then
            builder.AppendLine("!!!!!" & Resources.Language.LABEL_ADMIN_REVIEWS_EMAIL_BAD_WORDS & "!!!!!")
        End If

        builder.AppendLine(" " & Resources.Language.LABEL_ADMIN_REVIEWS_EMAIL_USER_NAME & " " & CStrEx(globals.User("ContactName")))

        builder.AppendLine(" " & Resources.Language.LABEL_ADMIN_REVIEWS_EMAIL_USER_LOGIN & " " & CStrEx(userLogin))

        builder.AppendLine(" " & Resources.Language.LABEL_ADMIN_REVIEWS_EMAIL & " " & CStrEx(globals.User("EmailAddress")))

        builder.AppendLine(" " & Resources.Language.LABEL_ADMIN_REVIEWS_EMAIL_PRODUCT_GUID & " " & CStrEx(guid))

        builder.AppendLine(" " & Resources.Language.LABEL_ADMIN_REVIEWS_EMAIL_RATING & " " & CStrEx(rating))

        builder.AppendLine(" " & Resources.Language.LABEL_ADMIN_REVIEWS_EMAIL_TITLE & " " & CStrEx(title))

        builder.AppendLine(" " & Resources.Language.LABEL_ADMIN_REVIEWS_EMAIL_COMMENT & " " & CStrEx(reviewText))

        Try
            'System.Web.Mail.SmtpMail.Send(emailFrom, emailTo, subject, body)
            Dim recipient As New ExpDictionary()
            recipient.Add(emailTo, emailTo)
            Mailer.SendEmail(AppSettings("MAIL_REMOTE_SERVER"), AppSettings("MAIL_REMOTE_SERVER_PORT"), emailFrom, recipient, _
                    subject, builder.ToString(), False, Encoding.UTF8, globals.messages, Nothing)
        Catch ex As Exception
            MsgBox("Delivery Failure: " & ex.Source & ex.Message)
        End Try



    End Sub
End Class
'JA2010071601 - RATINGS AND REVIEWS - END
