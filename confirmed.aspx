<%@ Page Language="VB" AutoEventWireup="false" CodeFile="confirmed.aspx.vb" Inherits="Confirmed"
    CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master"
    RuntimeMasterPageFile="ThreeColumn.master" CrumbName="<%$ Resources: Language, LABEL_CONFIRMATION %>" %>

<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Import Namespace="ExpandIT.GlobalsClass" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <% If boolWaitMode Then%>
    <div class="ConfirmedWaitPage">
        <uc1:PageHeader ID="PageHeader1" runat="server" Text="<%$ Resources: Language, LABEL_CONFIRMATION %>"
            EnableTheming="true" />
        <p>
            <% =Resources.Language.LABEL_ORDER_BEING_PROCESSED%>
        </p>
        <% 
            txtActionString = GetQueryString()
					
            If txtActionString = "" Then
                txtActionString = Request.ServerVariables("SCRIPT_NAME") & "?RetryCounter=" & intRetryCounter
            Else
                txtActionString = Request.ServerVariables("SCRIPT_NAME") & "?" & txtActionString & "&RetryCounter=" & intRetryCounter
            End If
        %>
        <% PrintPostElements()%>
        <%=Resources.Language.LABEL_RELOAD_MANUALLY%>
        <br />
        <br />
        <input type="submit" class="AddButton" value="<%= Resources.Language.ACTION_RELOAD_PAGE %>"
            name="Reload page" />

        <script type="text/javascript">
						<!--

						// Timeout of 5 seconds before refresh script is activated.
						setTimeout( "submitpage()", 5*1000 );

						function submitpage()
						{
							// This code will submit the form "formClosed".							
							document.forms[0].action = '<% =txtActionString %>';
							document.forms[0].submit();
						}
						//-->
        </script>

    </div>
    <% ElseIf OrderFound Then%>
    <div class="ConfirmedFoundPage">
        <!-- Here begins the HTML code for the page -->
        <uc1:PageHeader ID="PageHeader2" runat="server" Text="<%$ Resources: Language, LABEL_CONFIRMATION %>"
            EnableTheming="true" />
        <%--AM2011031801 - ENTERPRISE CSR STAND ALONE - Start--%>
        <% If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then%>
        <asp:PlaceHolder runat="server" ID="phCSRConfirmedOrderCompleted"></asp:PlaceHolder>
        <%Else%>
        <p>
            <% Response.Write(Resources.Language.MESSAGE_YOUR_ORDER_IS_FINISHED)%>
            &nbsp;<%=csrObj.getLinkToHistoryDetail(Order)%>
            <br />
            <br />
            <%--AM2010080201 - ORDER PRINT OUT - Start--%>
            <% Response.Write(Resources.Language.CLICK_TO_PRINT_ORDER)%>
            &nbsp;<a href="PrintOrder.aspx?HeaderGuid=<% Response.Write(Order("HeaderGuid")) %>"
                target="_blank">here</a>.
            <%--AM2010080201 - ORDER PRINT OUT - End--%>
        </p>
        <br />
        <p>
            <%
                If bMailSend Then
                    Response.Write(Resources.Language.MESSAGE_MAIL_SEND_OK)
                Else
                    Response.Write(Resources.Language.MESSAGE_MAIL_SEND_FAIL)
                End If
            %>
            <%End If%>
            <%--AM2011031801 - ENTERPRISE CSR STAND ALONE - End--%>
        </p>
        <!-- start DIBS required information for credit card payments on the internet -->
        <br />
        <% Response.Write(strAdditionalInformation)%>
        <br />
        <!-- end DIBS required information for credit card payments on the internet -->
        <!-- Here ends the HTML code for the page -->
    </div>
    <% Else%>
    <div class="ConfirmedNotFoundPage">
        <uc1:PageHeader ID="PageHeader3" runat="server" Text="<%$ Resources: Language, LABEL_CONFIRMATION %>"
            EnableTheming="true" />
        <p>
            <% Response.Write(Server.HtmlEncode(Replace(Resources.Language.LABEL_UNABLE_TO_FIND_ORDER, "%1", HeaderGuid & CustomerReference)))%>
        </p>
    </div>
    <% End If%>
</asp:Content>
