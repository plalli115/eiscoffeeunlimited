﻿<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="NoteMailPreview.aspx.vb" Inherits="NoteMailPreview" 
    CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master" 
    RuntimeMasterPageFile="ThreeColumn.master" CrumbName="<%$ Resources: Language, LABEL_NOTE_MAIL_PREVIEW %>"%>
    
  <%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
  <%@ Register src="~/controls/Message.ascx" tagname="Message" tagprefix="uc1" %>
  <%@ Register Src="~/controls/MailPreview.ascx" TagName="MailPreview" TagPrefix="uc1" %>


<asp:content id="Content1" contentplaceholderid="cphSubMaster" runat="Server">
  <div class="NoteMailPreviewPage">
    <uc1:Message ID="Message1" runat="server" />
    <uc1:PageHeader ID="PageHeader1" runat="server" text="<%$ Resources: Language, LABEL_NOTE_MAIL_PREVIEW %>" EnableTheming="true" />
    <uc1:MailPreview ID="MailPreview1" runat="server" />
  </div>
  <p><a href="javascript:history.back()"><% =Resources.Language.LABEL_BACK%></a></p>
</asp:content>