<%@ Page Language="VB" AutoEventWireup="false" CodeFile="cart.aspx.vb" Inherits="cart"
    ValidateRequest="false" CodeFileBaseClass="ExpandIT.ShippingPayment.ShippingPaymentPage"
    MasterPageFile="~/masters/default/main.master" RuntimeMasterPageFile="ThreeColumn.master"
    CrumbName="<%$ Resources: Language, LABEL_MENU_ORDER_PAD %>" %>

<%--CodeFileBaseClass="ExpandIT.ShippingPayment.ShippingMethodPage"--%>
<%--JA2010061701 - SLIDE SHOW - Start--%>
<%@ MasterType VirtualPath="~/masters/EnterpriseBlue/ThreeColumn.master" %>
<%--JA2010061701 - SLIDE SHOW - End--%>
<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%--*****promotions*****-start--%>
<%@ Register Src="~/controls/USPromoCodes/PromoBox.ascx" TagName="PromoBox" TagPrefix="uc1" %>
<%--*****promotions*****-end--%>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<%@ Register Src="~/controls/Message.ascx" TagName="Message" TagPrefix="uc1" %>
<%--*****shipping*****-start --%>
<%@ Register Src="~/controls/shipping/parts/USShipping/ShippingProviders.ascx" TagName="ShippingProviders"
    TagPrefix="uc1" %>
<%--*****shipping*****-end --%>
<%--AM2010031501 - ORDER RENDER - Start--%>
<%@ Register Src="~/controls/USOrderRender/CartRender.ascx" TagName="CartRender"
    TagPrefix="uc1" %>
<%--AM2010031501 - ORDER RENDER - End--%>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <%-- WGS - 10-14-2013 - Javascript used to validate Your Name and Your Email - BEGIN  --%>
             <script type="text/javascript" >
                function customValidate(source, args) {
                   var bYourName = JQParsley("[name='YourName']").parsley('validate');
                   var bYourEmail = JQParsley("[name='YourEmail']").parsley('validate');
                   args.IsValid = (bYourName && bYourEmail);
                }
            </script>
     <%-- WGS - 10-14-2013 - Javascript used to validate Your Name and Your Email - END  --%>

    <asp:Panel ID="cartPanel" runat="server" DefaultButton="Purchase">
        <%--WLB.10.12.2012 - Require YourEmail - Start--%>
        <%--WLB.REQUIRED --%>
        <asp:ValidationSummary runat="server" ID="validationSummary1" DisplayMode="List"
            ShowMessageBox="false" HeaderText="<%$Resources: Language, LABEL_ERROR %>" Visible="true"
            ValidationGroup="additionalinfogroup" ShowSummary="false" EnableClientScript="false" />
        <%--WLB.10.12.2012 - Require YourEmail - End--%>
        <div class="CartPage">
            <uc1:Message ID="Message1" runat="server" />
            <uc1:PageHeader ID="PageHeader1" runat="server" Text="<%$ Resources: Language, LABEL_MENU_ORDER_PAD %>"
                EnableTheming="true" />
            <input type="hidden" name="cartaction" value="Update" />
            <input type="hidden" name="pageID" value="cart" />
            <input type="hidden" name="RetURL" value="cart.aspx?GroupGuid=<% = globals.GroupGuid %>" />
            <input type="hidden" name="GroupGuid" value="<% = globals.GroupGuid %>" />
            <!-- Here begins HTML code for displaying the cart -->
            <asp:Panel ID="panelCartEmpty" runat="server">
                <!-- Display "empty cart" message -->
                <br />
                <div class="CartEmptyMessage">
                    <% =Resources.Language.LABEL_ORDER_PAD_IS_EMPTY & "."%>
                </div>
            </asp:Panel>
            <!-- Here ends HTML code for displaying a commet and PO number field on the order -->
            <!-- Here begins HTML code for displaying a product entry box -->
            <asp:Panel ID="panelCartNotEmpty" CssClass="panelCartNotEmpty" runat="server">
                <%--<asp:UpdatePanel ID="UpdatePanelCartRender" UpdateMode="Conditional" runat="Server">
                    <contenttemplate>--%>
                <table style="width: 100%;" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <!-- AM2010031501 - ORDER RENDER - Start -->
                            <uc1:CartRender ID="CartRender1" runat="server" IsActiveOrder="true" EditMode="true"
                                ShowDate="false" ShowPaymentInfo="false" IsBrowser="true" IsLedger="false" IsEmail="false" />
                            <!-- AM2010031501 - ORDER RENDER - End -->
                        </td>
                    </tr>
                </table>
                <%--</contenttemplate>
                </asp:UpdatePanel>--%>
            </asp:Panel>
            <%--AM2011031801 - ENTERPRISE CSR STAND ALONE - Start--%>
            <%--<%If CStrEx(globals.OrderDict("DocumentType")) = "Return Order" And Not globals.IsCartEmpty Then%>
            <br />
            <table>
                <tr>
                    <td>
                        <asp:CheckBox ID="RefundShipping" CssClass="refundShipping" AutoPostBack="true" runat="server"
                            Text="<%$Resources: Language,LABEL_REFOUND_SHIPPING %>" />
                    </td>
                </tr>
            </table>
            <br />
            <%End If%>--%>
            <%If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then%>
            <asp:PlaceHolder runat="server" ID="phCSRCartRefundShipping"></asp:PlaceHolder>
            <%End If%>
            <!--AM2011031801 - ENTERPRISE CSR STAND ALONE - End-->
            <!-- Here ends HTML code for displaying a product entry box -->
            <!-- Here begins HTML code for displaying actions buttons -->

            <table border="0" cellspacing="3">
                <tr>
                    <%--AM2011031801 - ENTERPRISE CSR STAND ALONE - Start--%>
                    <%--<%If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And csrObj.getSalesPersonGuid() <> "" Then%>
                    <%If Not globals.IsCartEmpty Then%>
                    <td>
                        <asp:Button runat="server" CssClass="AddButton" ID="parkBtn" Text="<%$Resources: Language,ACTION_PARK%>"
                            PostBackUrl="~/cart.aspx?save=save" />
                    </td>
                    <%End If%>
                    <%End If%>--%>
                    <%If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then%>
                    <asp:PlaceHolder runat="server" ID="phCSRCartPark"></asp:PlaceHolder>
                    <%End If%>
                    <!--AM2011031801 - ENTERPRISE CSR STAND ALONE - End-->
                    <td>
                        <input runat="server" class="AddButton" id="btn1" type="reset" name="Reset1" value="<%$Resources: Language,ACTION_UNDO_CHANGES %>" />
                    </td>
                    <td>
                        <%-- WGS - 10-14-2013 - Your Name/Email; Added CausesValidation="false"  --%>
                        <asp:Button runat="server" CssClass="AddButton" ID="Update" Text="<%$Resources: Language,ACTION_UPDATE%>" 
                            PostBackUrl="~/cart.aspx?update=update" CausesValidation="false"/>
                    </td>
                    <td>
                        <%-- WGS - 10-14-2013 - Your Name/Email; Added CausesValidation="false"  --%>
                        <asp:Button runat="server" CssClass="AddButton" ID="Clear" Text="<%$Resources: Language,ACTION_CLEAR%>"
                            PostBackUrl="~/cart.aspx?clear=clear" CausesValidation="false"/>
                    </td>
                    <%If Not globals.IsCartEmpty Then%>
                    <td>
                        <%-- WGS - 10-14-2013 - Your Name/Email; Added CausesValidation="true"  --%>
                        <asp:Button runat="server" CssClass="AddButton" ID="Purchase" Text="<%$Resources: Language,LABEL_MENU_PURCHASE%>"
                            PostBackUrl="~/cart.aspx?purchase=purchase" CausesValidation="true" />
                    </td>
                    <%End If%>
                    <%--AM2011031801 - ENTERPRISE CSR STAND ALONE - Start--%>
                    <%--<%If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And csrobj.getSalesPersonGuid() <> "" And Not globals.IsCartEmpty Then%>
                    <td>
                        <asp:Button runat="server" Visible="false" CssClass="AddButton" ID="Returned" Text="<%$Resources: Language,LABEL_RETURN_LINK%>" />
                    </td>
                    <%End If%>--%>
                    <%If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then%>
                    <asp:PlaceHolder runat="server" ID="phCSRCartReturn"></asp:PlaceHolder>
                    <%End If%>
                    <!--AM2011031801 - ENTERPRISE CSR STAND ALONE - End-->
            </table>
        </div>
    </asp:Panel>
    <%--AM2011031801 - ENTERPRISE CSR STAND ALONE - Start--%>
    <%--<asp:Label ID="MessageSaved" runat="server" Style="color: #208040;" Visible="false"
        Text="<%$Resources: Language,LABEL_PARK_SAVED_MESSAGE%>"></asp:Label>--%>
    <%If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then%>
    <asp:PlaceHolder runat="server" ID="phCSRCartParkSaved"></asp:PlaceHolder>
    <%End If%>
    <!--AM2011031801 - ENTERPRISE CSR STAND ALONE - End-->
    <!-- Start javascript code for setting fokus to a field. -->
    <%
        ' Insert script that sets focus on the entry box
        If CBool(AppSettings("CART_ENTRY")) Then
            SetFocusOnElement_OnLoad("InpSku")
        End If
    %>
    <!-- End javascript code for setting fokus to a field. -->
</asp:Content>
