Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT.GlobalsClass
Imports ExpandIT.CartClass
Imports System.Web
Imports System.Collections.Generic

Partial Class cart
    Inherits ExpandIT.ShippingPayment._CartPage

    Protected pStr As Object
    Protected CartLine As ExpDictionary
    Protected bUseSecondaryCurrency As Boolean
    Protected key As String
    Protected UsingVariants As Boolean
    Protected UseLineComment As Boolean
    Protected i As Integer
    Protected ColSpanNumber As Integer
    Protected strID As String
    Protected dictSHProviders As ExpDictionary
    Protected dictSHProvider As ExpDictionary
    Protected dictNotUpdatedLine As Object
    Private Action As String = String.Empty
    Private UpdateMethod As String = String.Empty
    Private InfoString As String = String.Empty
    '*****shipping*****-start
    Public shippingObj As New liveShipping()
    '*****shipping*****-end
    'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
    Public csrObj As USCSR
    'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
    'AM2011032201 - RECURRING ORDERS STAND ALONE - START
    Public recurringObj As USRecurringOrders
    'AM2011032201 - RECURRING ORDERS STAND ALONE - END


    Protected Function FindUpdateMethod() As String
        If CStrEx(Request("Clear")) <> "" Then
            UpdateMethod = "clear"
        ElseIf CStrEx(Request("Update")) <> "" Then
            UpdateMethod = "update"
        ElseIf CStrEx(Request("Purchase")) <> "" Then
            UpdateMethod = "purchase"
            'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
        ElseIf CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
            UpdateMethod = csrObj.findCartUpdateMethod()
            'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
        End If
        Return UpdateMethod
    End Function

    Protected Function FindAction() As String
        Action = UCase(Request("cartaction"))
        Return Action
    End Function

    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	Protected Sub Page_Load_PostBack(ByVal sender As Object, ByVal e As System.EventArgs)
        ' Check the page access.
        eis.PageAccessRedirect("Cart")

        ' Read parameters
        globals.GroupGuid = CLngEx(Request("GroupGuid"))

        ' Find action
        FindAction()

        ' Find update method
        FindUpdateMethod()

        ' Do action
        Select Case Action.ToUpper()
            Case "ADD"
                'AM0122201001 - UOM - Start
                If CStrEx(Request("OnlyProduct")) <> "" Then
                    Add(False, CStrEx(Request("OnlyProduct")))
                Else
                    Add()
                End If
                'AM0122201001 - UOM - End
                'JA2010092201 - RECURRING ORDERS - Start
                If CBoolEx(globals.OrderDict("RecurringOrder")) Then
                    SetCurrentRecurrency()
                End If
                'JA2010092201 - RECURRING ORDERS - End
            Case "UPDATE"
                UpdateOrder()
            Case "OUTOFBASKET"
                OutOfBasket()
            Case ""
                UpdateOrder()
            Case Else
                globals.messages.Errors.Add(Resources.Language.MESSAGE_UNKNOWN_CART_ACTION & " '" & Action & "'")
        End Select

        ' Update the cartcounters.
        If OrderObject Is Nothing Then
            eis.UpdateMiniCartInfo(Nothing)
        Else
            globals.OrderDict = OrderObject
            'eis.CalculateOrder()
            'eis.UpdateMiniCartInfo(globals.OrderDict)

            'JA2010092201 - RECURRING ORDERS - START
            If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
            If Not IsPostBack Then
                If CStrEx(globals.OrderDict("ModifiedRecurrency")) = "" Then
                    recurringObj.applyRecurring(globals.OrderDict)
                End If
            ElseIf CStrEx(Request("Purchase")) <> "" Or CStrEx(Request("update")) <> "" Then
                recurringObj.applyRecurring(globals.OrderDict)
            End If
            End If
            'JA2010092201 - RECURRING ORDERS - END
        End If

        ' Find the url to redirect to.
        Dim myUrl As String

        '*****shipping*****-start
        If AppSettings("EXPANDIT_US_USE_SHIPPING") Then
            If IsPostBack() Then
                Me.getShippingValuesFromCart()
            End If
        End If
        '*****shipping*****-end

        eis.CalculateOrder()
        eis.UpdateMiniCartInfo(globals.OrderDict)
        eis.SaveOrderDictionary(globals.OrderDict)
        If UpdateMethod <> "" Then
            If Request("RetURL") Is Nothing Then
                If bLinesHaveBeenRemoved > 0 Then
                    myUrl = "cart.aspx?GroupGuid=" & globals.GroupGuid & "&LinesHaveBeenRemoved=" & bLinesHaveBeenRemoved
                Else
                    myUrl = "cart.aspx?GroupGuid=" & globals.GroupGuid
                End If
                Response.Redirect(MakeParam(myUrl, globals.messages.QueryString, InfoString))
            Else
                If UpdateMethod = "purchase" Then
                    'AM2010032601 - CREDIT LIMIT WARNING - START
                    If AppSettings("EXPANDIT_US_USE_CREDIT_LIMIT_WARNING") Then
                        If globals.User("IsB2B") Then
                            Dim creditLimitObj As New USCreditLimit(globals)
                            creditLimitObj.overCreditLimit(globals.User)
                        End If
                    End If
                    'AM2010032601 - CREDIT LIMIT WARNING - END
                    'AM2010032602 - PAST DUE WARNING - START
                    If AppSettings("EXPANDIT_US_USE_PAST_DUE_WARNING") Then
                        If globals.User("IsB2B") Then
                            Dim pastDueObj As New USPastDueWarning(globals)
                            pastDueObj.IsPastDue(globals.User("CustomerGuid"))
                        End If
                    End If
                    'AM2010032602 - PAST DUE WARNING - END
                    'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
                    If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
                        csrObj.checkCSRAllowed2Order("order")
                    End If
                    'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
                    ' find out if there is a shipping provider selected
                    '*****shipping*****-start
                    If AppSettings("EXPANDIT_US_USE_SHIPPING") Then
                        ''AM2010031501 - ORDER RENDER - Start
                        'shippingObj.ApplyShipping(globals, Me.CartRender1.SelectedProvider, Me.CartRender1.SelectedService, Me.CartRender1.ZipCode, eis, CustomerShippingProvider)
                        ''AM2010031501 - ORDER RENDER - End
                    Else
                        Dim custProvider As Object = CustomerShippingProvider
                        ' if not - add a possible shipping provider to cartheader
                        If custProvider.Equals(DBNull.Value) OrElse custProvider Is Nothing Then
                            CustomerShippingProvider = FirstPossibleShippingProvider()
                        End If
                    End If
                    '*****shipping*****-end
                    ' calculate url by calling NextUrl
                    If bLinesHaveBeenRemoved > 0 Then
                        myUrl = Me.NextUrl & "?LinesHaveBeenRemoved=" & bLinesHaveBeenRemoved
                    Else
                        myUrl = Me.NextUrl
                    End If

                    globals.OrderDict("HeaderComment") = Request("HeaderComment")
                    globals.OrderDict("CustomerPONumber") = Request("CustomerPONumber")
                    globals.OrderDict("YourName") = Request("YourName")
                    'WLB 10/10/2012 - BEGIN - Add "Your E-mail" to Additional Information
                    globals.OrderDict("YourEmail") = Request("YourEmail")
                    'WLB 10/10/2012 - END - Add "Your E-mail" to Additional Information

                    'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
                    If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
                        csrObj.requestAdjustmentInfo(True, globals.OrderDict)
                        csrObj.requestHoldOrderDate(globals.OrderDict)
                    End If
                    'AM2011031801 - ENTERPRISE CSR STAND ALONE - End

                    eis.SaveOrderDictionary(globals.OrderDict)
                    Me.CartPaymentAddress = globals.User
                    Me.CartShippingAddress = globals.User
                    'AM2010032601 - CREDIT LIMIT WARNING - START
                    'AM2010032602 - PAST DUE WARNING - START
                    If globals.messages.Errors.Count = 0 Then
                        Response.Redirect(MakeParam(myUrl, globals.messages.QueryString, InfoString))
                    Else
                        Response.Redirect(MakeParam("cart.aspx", globals.messages.QueryString, InfoString))
                    End If
                    'AM2010032602 - PAST DUE WARNING - END
                    'AM2010032601 - CREDIT LIMIT WARNING - END
                    'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
                ElseIf CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
                    'If 
                    csrObj.saveOrder(myUrl, UpdateMethod, bLinesHaveBeenRemoved, Me, cartPanel, InfoString, phCSRCartParkSaved)
                    'Then
                    'Me.MessageSaved.Visible = True
                    'Me.cartPanel.Visible = False
                    'Else
                    '    Response.Redirect(MakeParam(myUrl, globals.messages.QueryString, InfoString))
                    'End If
                    'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
                Else
                    If bLinesHaveBeenRemoved > 0 Then
                        myUrl = Request("RetURL") & "?LinesHaveBeenRemoved=" & bLinesHaveBeenRemoved
                    Else
                        myUrl = Request("RetURL")
                    End If
                    Response.Redirect(MakeParam(myUrl, globals.messages.QueryString, InfoString))
                End If
            End If
        End If
    End Sub

    ' Helper function: Adds the error string to the url.
    Private Function MakeParam(ByVal url As String, ByVal errorString As String, ByVal infoString As String) As String
        Dim retv As String

        retv = url
        If errorString <> "" Then
            retv = retv & IIf(InStr(1, retv, "?") = 0, "?", "&") & errorString
        End If
        If infoString <> "" Then
            retv = retv & IIf(InStr(1, retv, "?") = 0, "?", "&") & infoString
        End If

        Return retv
    End Function

    ' Helper function: Update the item(s) to the order. (Delete content and add)
    Sub UpdateOrder()
        Dim HeaderGuid, sql As String

        HeaderGuid = eis.GetHeaderGuidEx(Session("UserGuid"), False)
        Select Case UpdateMethod
            Case "clear"
                ' Clear the order and update header.
                sql = "DELETE FROM CartLine WHERE HeaderGuid=" & SafeString(HeaderGuid)
                excecuteNonQueryDb(sql)
                ' Update header information to tell that calculations has to be done again.
                sql = "UPDATE CartHeader SET IsCalculated = 0, VersionGuid = " & SafeString(GenGuid()) & " WHERE HeaderGuid = " & SafeString(HeaderGuid)
                excecuteNonQueryDb(sql)
                '*****promotions*****-start
                If AppSettings("EXPANDIT_US_USE_PROMOTION_CODES") Then
                    'AM2010031501 - ORDER RENDER - Start
                    If globals.OrderDict Is Nothing Then globals.OrderDict = eis.LoadOrderDictionary(globals.User)
                    'JA2010102801 - PAPERCHECK - Start
                    If AppSettings("PAYMENT_PAPERCHECK_NET") AndAlso CStrEx(globals.OrderDict("CustomerReference")) <> "" Then
                        sql = "DELETE FROM PaperChecks WHERE OrderNumber=" & SafeString(CStrEx(globals.OrderDict("CustomerReference")))
                        excecuteNonQueryDb(sql)
                    End If
                    'JA2010102801 - PAPERCHECK - End
                    'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
                    If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
                        csrObj.clearAdjustmentInfo(globals.OrderDict)
                        csrObj.clearHoldOrderDate(globals.OrderDict)
                    End If
                    'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
                    'JA2010092201 - RECURRING ORDERS - START
                    globals.OrderDict("RecurringOrder") = DBNull.Value
                    globals.OrderDict("EncryptCcNumber") = DBNull.Value
                    globals.OrderDict("EncryptExpDate") = DBNull.Value
                    globals.OrderDict("EncryptCcvNumber") = DBNull.Value
                    'JA2010092201 - RECURRING ORDERS - END

                    'AM2010031501 - ORDER RENDER - Start
                    '*****promotions*****-start
                    If AppSettings("EXPANDIT_US_USE_PROMOTION_CODES") Then
                        globals.OrderDict("PromotionCode") = ""
                        globals.OrderDict("PromotionMet") = False
                        Session("PromotionCode") = ""
                        globals.OrderDict("PromotionName") = ""
                        globals.OrderDict("PromotionDescription") = ""
                        globals.OrderDict("InvoiceDiscount") = 0
                        globals.OrderDict("InvoiceDiscountInclTax") = 0
                        globals.OrderDict("ShippingAmount") = 0
                        globals.OrderDict("ShipToZipCode") = ""
                    End If
                    '*****promotions*****-end
                    globals.OrderDict("ResidentialShipping") = DBNull.Value
                    'JA2011030701 - PAYMENT TABLE - START
                    'globals.OrderDict("PaymentTransactionID") = DBNull.Value
                    'globals.OrderDict("PaymentTransactionAmount") = DBNull.Value
                    'globals.OrderDict("PaymentTransactionSignature") = DBNull.Value
                    'JA2011030701 - PAYMENT TABLE - END
                    globals.OrderDict("UserGuid") = globals.User("UserGuid")
                    globals.OrderDict("DocumentType") = DBNull.Value
                    globals.OrderDict("SubTotal") = DBNull.Value
                    globals.OrderDict("TaxAmount") = DBNull.Value
                    globals.OrderDict("TotalInclTax") = DBNull.Value
                    globals.OrderDict("Total") = DBNull.Value
                    globals.OrderDict("CustomerReference") = DBNull.Value
                    globals.OrderDict("SubTotalInclTax") = DBNull.Value
                    globals.OrderDict("HeaderComment") = DBNull.Value
                    globals.OrderDict("CustomerPONumber") = DBNull.Value
                    globals.OrderDict("YourName") = DBNull.Value
                    'WLB 10/10/2012 - BEGIN - Add "Your E-mail" to Additional Information
                    globals.OrderDict("YourEmail") = globals.User("EmailAddress")
                    'globals.OrderDict("YourEmail") = DBNull.Value
                    'WLB 10/10/2012 - BEGIN - Add "Your E-mail" to Additional Information

                    eis.SaveOrderDictionary(globals.OrderDict)
                    'AM2010031501 - ORDER RENDER - End
                End If
                '*****promotions*****-end
                '*****shipping*****-start
                If AppSettings("EXPANDIT_US_USE_SHIPPING") Then
                    'AM2010031501 - ORDER RENDER - Start
                    shippingObj.clearShippingData(globals, eis)
                    'AM2010031501 - ORDER RENDER - End
                End If
                '*****shipping*****-end
            Case "purchase"
                ' Update the order content and mark edited lines and order.
                CompareAndUpdateOrder(HeaderGuid)
                '    'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
                'Case "save"

                '    'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
            Case "update", ""
                'Update the order content and mark edited lines and order.
                CompareAndUpdateOrder(HeaderGuid)

                'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
                If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
                    csrObj.requestAdjustmentInfo(False, OrderObject)
                    csrObj.requestHoldOrderDate(OrderObject)
                End If
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - End

                '*****shipping*****-start
                If AppSettings("EXPANDIT_US_USE_SHIPPING") Then
                    OrderObject = eis.LoadOrderDictionary(globals.User)
                    OrderObject("ShippingAmount") = 0
                    OrderObject("ShippingAmountInclTax") = 0
                End If

                eis.SaveOrderDictionary(OrderObject)
                '*****shipping*****-end
        End Select
    End Sub

    '  Notes:
    ' 	Error messages is send back to the return page
    ' 	FROM clause must contain both version and guid: in order not to overwrite data.
    ' 	Record comparison after writing data.
    ' 	Updates: Use delete and write method instead of the update method
    ' 	versioning of a line is done using guids instead of the not so safe int version (mulit-user issues)
    ' 	Error messages should be translated
    ' 
    '  Compare and update structure:
    ' 	1. Load order from database
    ' 	2. Compare user cart for any updates (changed Quantity, remarks or other fields)
    ' 	3. Write updates based on guid and version
    ' 	4. Check which updates that were successful
    ' 
    Sub CompareAndUpdateOrder(ByVal HeaderGuid As String)
        Dim bLineUpdated As Boolean = False
        Dim bUpdateHeader As Boolean = False
        Dim bDoUpdateLine As Boolean
        Dim sqlVariant As String
        Dim dictVariants As ExpDictionary

        ' Initialazation
        OrderObject = eis.LoadOrderDictionary(globals.User)

        ' Loop the form elements / line updates
        Dim dictForm As ExpDictionary
        Dim dictFormLine As ExpDictionary
        Dim i, j As Integer
        Dim dictOrderLine As ExpDictionary
        Dim LineGuid As String = ""
        Dim VersionGuid, strUpdatedLineGuids As String
        Dim dictOrderLinesToUpdate As ExpDictionary
        Dim dictOrderLinesDeleted, dictOrderlinesNotUpdated, dictFieldsNotFound As ExpDictionary
        Dim bDBValueChanged, bUserValueChanged As Boolean
        Dim arrFieldMatch, arrTmp As Object()
        Dim strFieldName, strFieldType As String
        Dim dictTestUpdates As ExpDictionary
        Dim aKey As String
        Dim bNewNonExistingLine As Boolean
        Dim strUpdatedProductGuidsMissingVariant As String
        Dim bReCalculate As Boolean
        Dim sku As String
        Dim qty As Decimal
        Dim var As String
        Dim sql As String
        Dim item As String
        Dim dict As ExpDictionary

        Dim req As Dictionary(Of String, ArrayList) = QueryString2Dict()
        If Not req.ContainsKey("SKU") Then req.Add("SKU", New ArrayList())
        If Not req.ContainsKey("LineGuid") Then req.Add("LineGuid", New ArrayList())
        If Not req.ContainsKey("VersionGuid") Then req.Add("VersionGuid", New ArrayList())
        If Not req.ContainsKey("HeaderComment") Then req.Add("HeaderComment", New ArrayList())
        If Not req.ContainsKey("CustomerPONumber") Then req.Add("CustomerPONumber", New ArrayList())
        If Not req.ContainsKey("YourName") Then req.Add("YourName", New ArrayList())
        'WLB 10/10/2012 - BEGIN - Add "Your E-mail" to Additional Information
        If Not req.ContainsKey("YourEmail") Then req.Add("YourEmail", New ArrayList())
        'WLB 10/10/2012 - END - Add "Your E-mail" to Additional Information

        'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
        If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
            csrObj.add2ReqDictAdjustment(req)
        End If
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - End

        ' The array containing information on which elements to compare/update....
        If AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_COMMENT") Then
            arrFieldMatch = New Object() {New String() {"Quantity", "NUMBER"}, New String() {"LineComment", "STRING"}}
        Else
            arrFieldMatch = New Object() {New String() {"Quantity", "NUMBER"}}
        End If
        strUpdatedLineGuids = ""
        strUpdatedProductGuidsMissingVariant = ""
        dictForm = New ExpDictionary
        dictOrderLinesToUpdate = New ExpDictionary
        dictOrderLinesDeleted = New ExpDictionary
        dictOrderlinesNotUpdated = New ExpDictionary
        dictFieldsNotFound = New ExpDictionary
        bReCalculate = False
        ' loop the sku's and look for updates
        For i = 0 To req("SKU").Count - 1
            sku = Trim(req("SKU")(i))
            If sku <> "" Then
                dictFormLine = New ExpDictionary
                bNewNonExistingLine = False
                bDoUpdateLine = True
                If req("LineGuid").Count >= i + 1 Then
                    LineGuid = req("LineGuid")(i)
                Else
                    ' The user has selected to add another product via the cart.
                    ' The last sku comes from CBoolEx(AppSettings("CART_ENTRY")) being true
                    If i = req("SKU").Count - 1 Then
                        If strUpdatedProductGuidsMissingVariant <> "" Then strUpdatedProductGuidsMissingVariant = strUpdatedProductGuidsMissingVariant & ","
                        strUpdatedProductGuidsMissingVariant = strUpdatedProductGuidsMissingVariant & SafeString(sku)
                        bNewNonExistingLine = True
                        Exit For
                    End If
                End If
                VersionGuid = CStrEx(req("VersionGuid")(i))

                ' Does the line exist in the order?
                If Not IsDict(OrderObject("Lines")(LineGuid)) Then
                    ' Since the line doesn't exist please ignore the line. 
                    ' Tell the user that a line has been deleted by another user.
                    bLinesHaveBeenRemoved = 1
                    dictOrderLinesDeleted(LineGuid) = LineGuid
                Else
                    dictOrderLine = OrderObject("Lines")(LineGuid)
                    If VersionGuid <> CStrEx(dictOrderLine("VersionGuid")) Then
                        dictOrderlinesNotUpdated(LineGuid) = dictOrderLine
                    Else
                        dictFormLine("LineGuid") = LineGuid
                        bDBValueChanged = False
                        bUserValueChanged = False

                        ' Do matching of field(s)
                        ' Loop fields, and convert by type.
                        For j = 0 To UBound(arrFieldMatch)
                            arrTmp = arrFieldMatch(j)
                            strFieldName = arrTmp(0)
                            strFieldType = arrTmp(1)

                            ' Is field present on the order line? If not then exit the for loop
                            ' Ignore. Could be due to wrong field information, add debugging information
                            If dictOrderLine(strFieldName) Is Nothing Then
                                dictFieldsNotFound(strFieldName) = strFieldName & ";" & strFieldType
                                Exit For
                            End If
                            dictFormLine(strFieldName & "_prev") = req(strFieldName & "_prev")(i)
                            dictFormLine(strFieldName) = req(strFieldName)(i)

                            ' Convert to numbers if necessary
                            If strFieldType = "NUMBER" Then
                                dictOrderLine(strFieldName) = CDblEx(dictOrderLine(strFieldName))
                                dictFormLine(strFieldName & "_prev") = CDblEx(dictFormLine(strFieldName & "_prev"))
                                If Not IsNumeric(dictFormLine(strFieldName)) Then
                                    dictFormLine(strFieldName) = CDblEx(dictFormLine(strFieldName & "_prev"))
                                Else
                                    dictFormLine(strFieldName) = CDblEx(dictFormLine(strFieldName))
                                End If
                                If strFieldName = "Quantity" Then
                                    If CBool(AppSettings("CART_ALLOW_DECIMAL")) Then
                                        dictFormLine(strFieldName) = CDblEx(SafeUFloat(CDblEx(dictFormLine(strFieldName))))
                                    Else
                                        Dim q As Integer = CInt(CDblEx(dictFormLine(strFieldName)))
                                        dictFormLine(strFieldName) = IIf(q < 1, 1, q)
                                    End If
                                    If dictFormLine(strFieldName) <= 0 Then
                                        ' Delete the line and skip the rest of the update.
                                        sql = "DELETE FROM CartLine WHERE LineGuid = " & SafeString(LineGuid) & " AND (VersionGuid = " & SafeString(VersionGuid) & " OR VersionGuid IS NULL)"
                                        excecuteNonQueryDb(sql)

                                        If OrderObject("Lines").Exists(LineGuid) Then
                                            OrderObject("Lines").Remove(LineGuid)
                                        End If

                                        bDoUpdateLine = False
                                        bReCalculate = True
                                    End If
                                End If
                            End If

                            If strFieldType = "BOOLEAN" Then
                                dictOrderLine(strFieldName) = CBoolEx(dictOrderLine(strFieldName))
                                dictFormLine(strFieldName) = CBoolEx(dictFormLine(strFieldName))
                                dictFormLine(strFieldName & "_prev") = CBoolEx(dictFormLine(strFieldName & "_prev"))
                            End If

                            If bDoUpdateLine Then
                                ' Compare value to database? 
                                ' If changed inform the user that this line was not edited since another user changed the value
                                If CStrEx(dictOrderLine(strFieldName)) <> dictFormLine(strFieldName & "_prev") Then
                                    ' Inform the user of the updated record. 
                                    ' E.g. mark line with "red" or a star. Placing a legend saying that some information was not updated.
                                    dictOrderLine("DBFieldValueChange") = strFieldName
                                    dictOrderlinesNotUpdated(LineGuid) = dictOrderLine
                                    bDBValueChanged = True
                                End If

                                ' Compare value to updated value?
                                If Not bDBValueChanged Then
                                    If dictFormLine(strFieldName) <> dictFormLine(strFieldName & "_prev") Then
                                        bUserValueChanged = True
                                        ' Create the WHERE clause for getting information on deleted/not updated records after the update.
                                        If strUpdatedLineGuids <> "" Then strUpdatedLineGuids = strUpdatedLineGuids & ","
                                        strUpdatedLineGuids = strUpdatedLineGuids & SafeString(LineGuid)
                                        ' Is variant guid missing?
                                        If CStrEx(dictOrderLine("VariantCode")) = "" Then
                                            If strUpdatedProductGuidsMissingVariant <> "" Then strUpdatedProductGuidsMissingVariant = strUpdatedProductGuidsMissingVariant & ","
                                            strUpdatedProductGuidsMissingVariant = strUpdatedProductGuidsMissingVariant & SafeString(dictOrderLine("ProductGuid"))
                                        End If
                                        ' Set the value and mark the line for re-calculation and set new version.
                                        dictOrderLine(strFieldName) = dictFormLine(strFieldName)
                                        dictOrderLine("IsCalculated") = False
                                        dictOrderLine("VersionGuid") = GenGuid()
                                        dictOrderLinesToUpdate(LineGuid) = dictOrderLine
                                    End If
                                End If
                            End If ' // bDoUpdateLine (No else)
                        Next
                        dictForm.Add(dictFormLine("LineGuid"), dictFormLine)
                    End If ' // VersionGuid <> dictOrderLine("VersionGuid")
                End If ' // Not IsDict(OrderObject("Lines")(LineGuid))
            End If ' // If sku<>"" Then
        Next ' // Looping sku's from posting page (cart.aspx)

        dictVariants = New ExpDictionary()

        ' Get variants for the lines that are missing the variant field.
        If strUpdatedProductGuidsMissingVariant <> "" Then
            'AM2010021901 - VARIANTS - Start
            If CBoolEx(AppSettings("EXPANDIT_US_USE_VARIANTS")) Then
                'AM2010021901 - VARIANTS - End
                If Len(strUpdatedProductGuidsMissingVariant) > 0 Then
                    sqlVariant = "SELECT * FROM ProductVariant WHERE ProductGuid IN (" & strUpdatedProductGuidsMissingVariant & ") ORDER BY VariantName DESC"
                    dictVariants = SQL2Dicts(sqlVariant, "ProductGuid")
                End If
            End If
        End If

        ' Run update statement if any lines to update.
        ' Might need an extra check on the line guid	
        If strUpdatedLineGuids <> "" Then
            ' Make sure that a variant guid is assigned to a line if a VariantCode is not present on the line 

            ' Get possible variants for the ProductGuid's collected in myGuidStr
            If dictVariants.Count > 0 Then
                For Each item In dictOrderLinesToUpdate.Keys
                    If IsDict(dictVariants(dictOrderLinesToUpdate(item)("ProductGuid"))) Then
                        If CStrEx(dictOrderLinesToUpdate(item)("VariantCode")) = "" Then
                            dictOrderLinesToUpdate(item)("VariantCode") = dictVariants(dictOrderLinesToUpdate(item)("ProductGuid"))("VariantCode")
                        End If
                    End If
                Next
            End If
            ' Delete/Save the order lines.
            ' Due to performance issues it has been chosen to delete all elements and then create them again.
            ' To write a more multi-user safe structure: run through each line and delete lines based on
            ' both VersionGuid and LineGuid. Create the line if a line was deleted. (A "true" update).
            Dicts2Table(dictOrderLinesToUpdate, "CartLine", "LineGuid IN (" & strUpdatedLineGuids & ")")

            ' Run through the updated records and check if the records actually updated.
            sql = "SELECT LineGuid, VersionGuid FROM CartLine WHERE LineGuid IN (" & strUpdatedLineGuids & ")"
            dictTestUpdates = SQL2Dicts(sql, "LineGuid")
            For Each aKey In dictOrderLinesToUpdate.ClonedKeyArray
                dict = dictOrderLinesToUpdate(aKey)
                ' Is the line present in the update?
                If IsDict(dictTestUpdates(aKey)) Then
                    ' Compare versions.
                    If dict("VersionGuid") <> dictTestUpdates(aKey)("VersionGuid") Then
                        dictOrderlinesNotUpdated(aKey) = dictOrderLinesToUpdate(aKey)
                        dictOrderLinesToUpdate.Remove(aKey)
                    End If
                Else
                    bLinesHaveBeenRemoved = 1
                    dictOrderLinesDeleted(aKey) = aKey
                    dictOrderLinesToUpdate.Remove(aKey)
                End If
            Next
        End If

        ' Compare and update header information changes!
        ' comment and purchase order number
        If req("HeaderComment").Count = 1 Then
            ' Compare old value by db value, these should be the same otherwise the header has changed.
            If CStrEx(OrderObject("HeaderComment")) = CStrEx(Request("HeaderComment_prev")) Then
                ' Has the value been updated?
                If CStrEx(Request("HeaderComment_prev")) <> CStrEx(Request("HeaderComment")) Then
                    OrderObject("HeaderComment") = CStrEx(Request("HeaderComment"))
                    bUpdateHeader = True
                End If
            End If
        End If
        If req("CustomerPONumber").Count = 1 Then
            ' Compare old value by db value, these should be the same otherwise the header has changed.
            If CStrEx(OrderObject("CustomerPONumber")) = CStrEx(Request("CustomerPONumber_prev")) Then
                ' Has the value been updated?
                If CStrEx(Request("CustomerPONumber_prev")) <> CStrEx(Request("CustomerPONumber")) Then
                    OrderObject("CustomerPONumber") = CStrEx(Request("CustomerPONumber"))
                    bUpdateHeader = True
                End If
            End If
        End If
        If req("YourName").Count = 1 Then
            ' Compare old value by db value, these should be the same otherwise the header has changed.
            If CStrEx(OrderObject("YourName")) = CStrEx(Request("YourName_prev")) Then
                ' Has the value been updated?
                If CStrEx(Request("YourName_prev")) <> CStrEx(Request("YourName")) Then
                    OrderObject("YourName") = CStrEx(Request("YourName"))
                    bUpdateHeader = True
                End If
            End If
        End If
        'WLB 10/10/2012 - BEGIN - Add "Your E-mail" to Additional Information
        If req("YourEmail").Count = 1 Then
            ' Compare old value by db value, these should be the same otherwise the header has changed.
            If CStrEx(OrderObject("YourEmail")) = CStrEx(Request("YourEmail_prev")) Then
                ' Has the value been updated?
                If CStrEx(Request("YourEmail_prev")) <> CStrEx(Request("YourEmail")) Then
                    OrderObject("YourEmail") = CStrEx(Request("YourEmail"))
                    bUpdateHeader = True
                End If
            End If
        End If
        'WLB 10/10/2012 - END - Add "Your E-mail" to Additional Information

        'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
        If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
            csrObj.compareAndUpdateAdjustmentInfo(OrderObject, bUpdateHeader, req)
        End If
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - End

        ' Update Header information if either lines or Header has changed
        ' IsCalculated should not be updated if only the header comments has been updated.
        If bUpdateHeader Or dictOrderLinesToUpdate.Count > 0 Or bReCalculate Then
            OrderObject("VersionGuid") = GenGuid()
            ' Update IsCalculated if some lines has been updated
            If dictOrderLinesToUpdate.Count > 0 Or bReCalculate Then
                OrderObject("IsCalculated") = False
            End If
            Dict2Table(OrderObject, "CartHeader", "HeaderGuid = " & SafeString(HeaderGuid))
        End If

        ' A special case is the scenario where the user add's an item using the box on the cart.
        ' Too time consuming to build another version of this element.
        If CBool(AppSettings("CART_ENTRY")) And bNewNonExistingLine Then
            ' Load the order
            OrderObject = eis.LoadOrderDictionary(globals.User)

            i = req("SKU").Count - 1
            sku = CStrEx(Trim(req("SKU")(i)))
            qty = CLngEx(Trim(req("Quantity")(i)))
            var = Nothing
            If IsDict(dictVariants(sku)) Then
                var = dictVariants(sku)("VariantCode")
            End If
            eis.AddItemToOrder2(OrderObject, sku, qty, "", var)
            ' For performance reasons.
            OrderObject("IsCalculated") = False
            OrderObject("VersionGuid") = GenGuid()
            ' Save the order		
            eis.SaveOrderDictionary(OrderObject)
        End If

        ' Process the error message handling for the retval post.
        ' Collect information
        If InfoString <> "" Then InfoString = InfoString & "&"
        InfoString = InfoString & "DeletedLinesCount=" & CLngEx(dictOrderLinesDeleted.Count)
        InfoString = InfoString & "&NotUpdatedLinesCount=" & CLngEx(dictOrderlinesNotUpdated.Count)
        InfoString = InfoString & "&UpdatedLinesCount=" & CLngEx(dictOrderLinesToUpdate.Count)
        InfoString = InfoString & "&UpdatedHeaderInformation=" & CBoolEx(bUpdateHeader Or (dictOrderLinesToUpdate.Count > 0))
        InfoString = InfoString & "&AddedElements=" & (CBoolEx(("CART_ENTRY")) And CBoolEx(bNewNonExistingLine))
        For Each aKey In dictOrderlinesNotUpdated.Keys
            InfoString = InfoString & "&NotUpdatedLine=" & Server.UrlEncode(CStrEx(dictOrderlinesNotUpdated(aKey)("LineGuid")))
        Next
        For Each aKey In dictFieldsNotFound.Keys
            InfoString = InfoString & "&FieldNotFound=" & Server.UrlEncode(CStrEx(dictFieldsNotFound(aKey)))
        Next
    End Sub

    ' Helper function:
    ' "OutOfBasket" will allow a single line to be removed from the Order Pad, by clicking the red cross
    ' (delete) on the cart.aspx page.
    Sub OutOfBasket()
        Dim myLineGuid As String

        OrderObject = eis.LoadOrderDictionary(globals.User)

        ' Remove the line if present.
        If OrderObject("Lines").Count > 0 Then
            myLineGuid = CStrEx(Request("LineGuid"))
            If OrderObject("Lines").Exists(myLineGuid) Then
                If OrderObject("Lines").Count = 1 Then
                    OrderObject("PromotionCode") = ""
                    OrderObject("PromotionMet") = False
                    Session("PromotionCode") = ""
                    OrderObject("PromotionName") = ""
                    OrderObject("PromotionDescription") = ""
                    OrderObject("InvoiceDiscount") = 0
                    OrderObject("InvoiceDiscountInclTax") = 0
                    'JA2010102801 - PAPERCHECK - Start
                    If AppSettings("PAYMENT_PAPERCHECK_NET") AndAlso CStrEx(OrderObject("CustomerReference")) <> "" Then
                        Dim sql As String
                        sql = "DELETE FROM PaperChecks WHERE OrderNumber=" & SafeString(CStrEx(OrderObject("CustomerReference")))
                        excecuteNonQueryDb(sql)
                    End If
                    'JA2010102801 - PAPERCHECK - End
                    'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
                    If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
                        csrObj.clearAdjustmentInfo(OrderObject)
                        csrObj.clearHoldOrderDate(OrderObject)
                    End If
                    'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
                    'JA2010092201 - RECURRING ORDERS - START
                    OrderObject("RecurringOrder") = DBNull.Value
                    OrderObject("EncryptCcNumber") = DBNull.Value
                    OrderObject("EncryptExpDate") = DBNull.Value
                    OrderObject("EncryptCcvNumber") = DBNull.Value
                    'JA2010092201 - RECURRING ORDERS - END
                    'JA2011030701 - PAYMENT TABLE - START
                    'OrderObject("PaymentTransactionID") = DBNull.Value
                    'OrderObject("PaymentTransactionAmount") = DBNull.Value
                    'OrderObject("PaymentTransactionSignature") = DBNull.Value
                    'JA2011030701 - PAYMENT TABLE - END
                    OrderObject("UserGuid") = globals.User("UserGuid")
                    OrderObject("DocumentType") = DBNull.Value
                    OrderObject("SubTotal") = DBNull.Value
                    OrderObject("TaxAmount") = DBNull.Value
                    OrderObject("TotalInclTax") = DBNull.Value
                    OrderObject("Total") = DBNull.Value
                    OrderObject("CustomerReference") = DBNull.Value
                    OrderObject("SubTotalInclTax") = DBNull.Value
                    OrderObject("HeaderComment") = DBNull.Value
                    OrderObject("CustomerPONumber") = DBNull.Value
                    OrderObject("YourName") = DBNull.Value
                    'WLB 10/10/2012 - BEGIN - Add "Your E-mail" to Additional Information
                    OrderObject("YourEmail") = globals.User("EmailAddress")
                    'OrderObject("YourEmail") = DBNull.Value
                    'WLB 10/10/2012 - END - Add "Your E-mail" to Additional Information

                    '*****shipping*****-start
                    If AppSettings("EXPANDIT_US_USE_SHIPPING") Then
                        'AM2010031501 - ORDER RENDER - Start
                        globals.OrderDict = OrderObject
                        shippingObj.clearShippingData(globals, eis)
                        OrderObject = globals.OrderDict
                        'AM2010031501 - ORDER RENDER - End
                    End If
                    '*****shipping*****-end
                End If

                OrderObject("Lines").Remove(myLineGuid)
                OrderObject("IsCalculated") = False
                OrderObject("VersionGuid") = GenGuid()
                '*****promotions*****-start
                If AppSettings("EXPANDIT_US_USE_PROMOTION_CODES") Then
                    OrderObject("PromotionMet") = False
                End If
                '*****promotions*****-end
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
                If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
                    csrObj.outOfBasketSetShippingValue(OrderObject)
                Else
                    '*****shipping*****-start
                    OrderObject("ShippingAmount") = 0
                    '*****shipping*****-end
                End If
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
                eis.SaveOrderDictionary(OrderObject)
            End If
        End If
    End Sub

    Protected Sub CheckShippingProvider()
        Dim custProvider As Object = CustomerShippingProvider
        '*****shipping*****-start
        If AppSettings("EXPANDIT_US_USE_SHIPPING") Then
            shippingObj.checkShippingProvider(globals, custProvider, eis, CustomerShippingProvider)
        Else
            ' If customer has a cart
            If custProvider IsNot Nothing Then
                ' find out if there is a shipping provider selected
                If custProvider.Equals(DBNull.Value) Then
                    ' No provider is selected, add one here
                    CustomerShippingProvider = FirstPossibleShippingProvider()
                Else
                    ' If a shipping provider exists then
                    ' find out if the selected shipping provider is valid.
                    Dim providers As ArrayList = AllPossibleShippingProviders()
                    For i As Integer = 0 To providers.Count - 1
                        If providers(i).Equals(custProvider) Then
                            ' provider exists and is valid
                            Exit Sub
                        End If
                    Next
                    ' If we got here then either no provider is selected
                    ' or the provider selected is invalid.
                    ' Here we add a valid provider
                    CustomerShippingProvider = FirstPossibleShippingProvider()
                End If
            End If
        End If
        '*****shipping*****-end
    End Sub

    'JA2010061701 - SLIDE SHOW - Start
    Protected Sub SetSlideToMaster(ByVal products As ExpDictionary)
        Dim SlideDict As ExpDictionary = Nothing

        If CStrEx(AppSettings("SHOW_SLIDE_CART")) = "0" Then
            SlideDict = Nothing
        Else
            If Not products Is Nothing Then
                If products.Count > 0 Then
                    SlideDict = eis.CreateRelatedItemsDictForSlide(products, CIntEx(AppSettings("SHOW_SLIDE_ON_CART_CODE")))
                End If
            End If
        End If


        Me.Master.SlideToShow.ImageDict = SlideDict
    End Sub
    'JA2010061701 - SLIDE SHOW - End


    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        ' If this page is a redirect from _login.aspx then
        ' redirect to the next page in the checkout process
        If Not String.IsNullOrEmpty(Request("bouncecheckout")) Then
            Me.CartPaymentAddress = globals.User
            Me.CartShippingAddress = globals.User
            Response.Redirect(MakeParam(NextUrl, globals.messages.QueryString, InfoString), True)
        End If
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
        If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
            csrObj = New USCSR(globals)
            csrObj.cartLoadPlaceHolders(Me, phCSRCartrefundshipping, phcsrcartpark, phcsrcartparksaved, phcsrcartreturn, CartPaymentAddress, CartShippingAddress)
        End If
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
        'AM2011032201 - RECURRING ORDERS STAND ALONE - START
        recurringObj = New USRecurringOrders(globals)
        'AM2011032201 - RECURRING ORDERS STAND ALONE - END
        ' If we come from another redirect
        If Not IsPostBack AndAlso String.IsNullOrEmpty(Request("pageID")) Then
            CheckShippingProvider()
            If Request("cartaction") = "add" Or Request("cartaction") = "OutOfBasket" Then
                If Request("cartaction") = "add" Then
                    UpdateMethod = "Update"
                    'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
                    If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
                        csrObj.requestHoldOrderDate(globals.OrderDict)
                    End If
                    'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
                End If
                Page_Load_PostBack(sender, e)
            End If
            'ElseIf (IsPostBack AndAlso Not String.IsNullOrEmpty(FindUpdateMethod())) Or (IsPostBack AndAlso FindAction() = "OutOfBasket") Then
        ElseIf (IsPostBack) Then
            ' Only do this in response to update, delete and purchase buttons
            Page_Load_PostBack(sender, e)
        End If
        ''JA2010061701 - SLIDE SHOW - Start
        'SetSlideToMaster()
        ''JA2010061701 - SLIDE SHOW - End


        'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
        If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
            'Me.parkBtn.Visible = csrObj.parkAllowed()
            csrObj.parkAllowed(phCSRCartPark)
        End If
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
        If globals.OrderDict Is Nothing Then
            globals.OrderDict = eis.LoadOrderDictionary(globals.User)
        End If
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
        'If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
        'If CStrEx(Request("HeaderGuidHold")) <> "" Then
        '    globals.OrderDict = eis.LoadHoldOrder(Request("HeaderGuidHold"))
        '    globals.OrderDict("MultiCartStatus") = "ACTIVE"
        '    globals.OrderDict("IsCalculated") = True
        '    Me.CartRender1.IsActiveOrder = True
        '    eis.SaveOrderDictionary(globals.OrderDict)
        'End If


        'If CStrEx(globals.OrderDict("DocumentType")) = "Return Order" Then
        '    Me.CartRender1.isReturn = True
        '    Me.Purchase.Visible = False
        '    Me.Returned.Visible = True
        '    Me.parkBtn.Visible = False
        'End If

        'End If
        If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
            csrObj.cartLoadHoldOrder(Me.CartRender1)
            csrObj.cartLoadReturnOrder(Me.CartRender1, Me.Purchase, phCSRCartPark, phCSRCartReturn)
        End If
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
        'JA2010092201 - RECURRING ORDERS - START
        'hide everything but recurrency control during recurrency modification
        If CStrEx(globals.OrderDict("ModifiedRecurrency")) <> "" Then
            Me.CartRender1.isModifyRecurrency = True
            Me.btn1.Visible = False
            Me.Update.Visible = False
            Me.Clear.Visible = False
            'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
            If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
                csrObj.controlVisible(phCSRCartPark, "parkBtn", False)
                csrObj.controlVisible(phCSRCartReturn, "Returned", False)
            End If
            'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
            Me.Purchase.Text = Resources.Language.LABEL_INPUT_ACEPT
        End If
        'JA2010092201 - RECURRING ORDERS - END
       
    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        ' Make sure this page expires.
        pStr = "private, no-cache, must-revalidate"
        Response.ExpiresAbsolute = New Date(1990, 1, 1) '#1990-01-01# 
        Response.AddHeader("pragma", "no-cache")
        Response.AddHeader("cache-control", pStr)

        ' Check the page access.
        eis.PageAccessRedirect("Cart")
        UseLineComment = AppSettings("ORDERRENDER_BROWSER_SHOWCOLUMN_COMMENT")

        If globals.OrderDict Is Nothing Then globals.OrderDict = eis.LoadOrderDictionary(globals.User)

        'JA2010061701 - SLIDE SHOW - Start
        If AppSettings("SHOW_SLIDE_CART") Then
            SetSlideToMaster(globals.OrderDict("Lines"))
        End If
        'JA2010061701 - SLIDE SHOW - End

        If CustomerShippingProvider IsNot Nothing Then
            If Not CStrEx(globals.OrderDict("ShippingHandlingProviderGuid")).Equals(CustomerShippingProvider) Then
                globals.OrderDict("ShippingHandlingProviderGuid") = IIf(Me.CustomerShippingProvider IsNot Nothing, CustomerShippingProvider, DBNull.Value)
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
                If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
                    csrObj.noReturnRecalculate(globals.OrderDict)
                End If
                'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
            End If
        End If
        ''*****shipping*****-start
        'If AppSettings("EXPANDIT_US_USE_SHIPPING") Then
        '    shippingObj.cartLoad(globals, CustomerShippingProvider)
        'End If
        ''*****shipping*****-end
        eis.CalculateOrder()
        eis.SaveOrderDictionary(globals.OrderDict)
        eis.UpdateMiniCartInfo(globals.OrderDict)

        bUseSecondaryCurrency = eis.UseSecondaryCurrency(globals.User)
        ' Get information from the order and build two arrays containing product guids and variant guids

        ' Load information for products
        Dim myArray1() As String = {"ProductName"}
        Dim myArray2() As String = Nothing
        eis.CatDefaultLoadProducts(globals.OrderDict("Lines"), False, False, False, myArray1, myArray2, Nothing)

        ' Shipping and Handling information
        '*****shipping*****-start
        If Not CBoolEx(AppSettings("EXPANDIT_US_USE_SHIPPING")) Then
            dictSHProviders = eis.GetShippingHandlingProviders(CStrEx(globals.OrderDict("ShippingHandlingProviderGuid")), False)
            dictSHProvider = New ExpDictionary
            If dictSHProviders.Count > 0 Then
                dictSHProvider = GetFirst(dictSHProviders).Value
            End If
        End If
        '*****shipping*****-end

        ' Get the NotUpdatedLine LineGuids
        dictNotUpdatedLine = New ExpDictionary()

        Dim req As Dictionary(Of String, ArrayList) = QueryString2Dict()
        If req.ContainsKey("NotUpdatedLine") Then
            For i = 0 To req("NotUpdatedLine").Count - 1
                dictNotUpdatedLine(req("NotUpdatedLine")(i)) = req("NotUpdatedLine")(i)
            Next
        End If

        If dictNotUpdatedLine.Count > 0 Then
            globals.messages.Warnings.Add(Resources.Language.MESSAGE_LINES_UPDATED_BY_ANOTHER_USER)
        End If
        'AM2010021901 - VARIANTS - Start
        UsingVariants = CBoolEx(AppSettings("EXPANDIT_US_USE_VARIANTS"))
        'AM2010021901 - VARIANTS - End
        If globals.IsCartEmpty Then
            Clear.Visible = False
            Purchase.Visible = False
        End If

        cartPanel.DefaultButton = Update.ID

        Try
            '<!-- ' ExpandIT US - MPF - US Sales Tax - Start -->
            'AM2010031501 - ORDER RENDER - Start
            'If AppSettings("EXPANDIT_US_USE_US_SALES_TAX") Then
            'Me.CartRender1.IsActiveOrder = True
            'Me.CartRender1.EditMode = True
            'Me.CartRender1.IsEmail = False
            Me.CartRender1.DefaultButton = Update
            'Me.CartRender1.ShowDate = False
            'Me.CartRender1.ShowPaymentInfo = False
            'litOrderRender.Text = OrderRender.Render(globals.OrderDict, eis, AppSettings("SHOW_TAX_TYPE") = "INCL", False, False, True, True, False, False, False, False, False, True, False, CustomerShippingProviderName, True, Update True)
            'Else
            'litOrderRender.Text = OrderRender.Render(globals.OrderDict, eis, AppSettings("SHOW_TAX_TYPE") = "INCL", False, False, True, True, False, False, False, False, False, True, False, CustomerShippingProviderName, True, Update)
            'End If
            'AM2010031501 - ORDER RENDER - End
            '<!-- ' ExpandIT US - MPF - US Sales Tax - Finish -->
        Catch ex As Exception

        End Try
        panelCartEmpty.Visible = globals.IsCartEmpty
        If CBoolEx(globals.IsCartEmpty) Then
            Me.btn1.Visible = False
            Me.Update.Visible = False
        End If
        panelCartNotEmpty.Visible = Not globals.IsCartEmpty
        'AM2010031501 - ORDER RENDER - Start
        'ShippingAndPromo.Visible = Not globals.IsCartEmpty
        'panelCartEntry.Visible = globals.IsCartEmpty
        'panelCommentAndPONumber.Visible = AppSettings("CART_COMMENT") Or AppSettings("CART_PONUMBER")
        'panelComment.Visible = AppSettings("CART_COMMENT")
        'panelPONumber.Visible = AppSettings("CART_PONUMBER")
        'panelCartEntry.Visible = CBoolEx(AppSettings("CART_ENTRY"))
        ''AM0122201001 - UOM - Start
        'panelCartEntry.Visible = Not CBoolEx(AppSettings("EXPANDIT_US_USE_UOM"))
        ''AM0122201001 - UOM - End

        '*****promotions*****-start
        'If Not globals.OrderDict("PromotionCode") Is DBNull.Value Then
        '    promoCode.Visible = AppSettings("SHOW_PROMOTION_CODE") And (globals.OrderDict("PromotionCode") <> "")
        '    promoTitle.Visible = AppSettings("SHOW_PROMOTION_CODE") And (globals.OrderDict("PromotionCode") <> "")
        'End If
        'If Not globals.OrderDict("PromotionName") Is DBNull.Value Then
        '    promoName.Visible = AppSettings("SHOW_PROMOTION_NAME") And (globals.OrderDict("PromotionName") <> "")
        '    If Not promoTitle.Visible Then
        '        promoTitle.Visible = AppSettings("SHOW_PROMOTION_NAME") And (globals.OrderDict("PromotionName") <> "")
        '    End If
        'End If
        'If Not globals.OrderDict("PromotionShortDescription") Is DBNull.Value Then
        '    promoShortDescription.Visible = AppSettings("SHOW_PROMOTION_SHORT_DESCRIPTION") And (globals.OrderDict("PromotionShortDescription") <> "")
        '    If Not promoTitle.Visible Then
        '        promoTitle.Visible = AppSettings("SHOW_PROMOTION_SHORT_DESCRIPTION") And (globals.OrderDict("PromotionShortDescription") <> "")
        '    End If
        'End If

        'If Not globals.OrderDict("PromotionDescription") Is DBNull.Value Then
        '    promoLongDescription.Visible = AppSettings("SHOW_PROMOTION_LONG_DESCRIPTION") And (globals.OrderDict("PromotionDescription") <> "")
        '    If Not promoTitle.Visible Then
        '        promoTitle.Visible = AppSettings("SHOW_PROMOTION_LONG_DESCRIPTION") And (globals.OrderDict("PromotionDescription") <> "")
        '    End If
        'End If
        '*****promotions*****-end
        'AM2010031501 - ORDER RENDER - End

        'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
        If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
            csrObj.notRefundShipping(phCSRCartRefundShipping)
        End If
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - End

        ' ---------------------------------------------------------------
        ' DEBUG INFORMATION COLLECTION
        ' ---------------------------------------------------------------
        If globals.DebugLogic Then
            DebugValue(globals.User, "User")
            DebugValue(globals.OrderDict, "OrderDict")
            DebugValue(globals.Context, "Context")
            DebugValue(dictSHProvider, "SH Provider information")
        End If
    End Sub

    Function ShowLineUpdateTag(ByVal strAttributes As Object) As Object
        ShowLineUpdateTag = "<span class=""Error""" & strAttributes & "><b>!</b></span>"
    End Function

    Protected Sub purchase_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Purchase.Click
        Server.Transfer("cart.aspx")
    End Sub
    '*****shipping*****-start
    Protected Sub getShippingValuesFromCart()
        If Not globals.OrderDict Is Nothing Then
            If Not globals.OrderDict("Lines") Is Nothing And globals.OrderDict("Lines").Count > 0 Then
                'AM2010031501 - ORDER RENDER - Start
                globals.OrderDict("ShippingHandlingProviderGuid") = Me.CartRender1.SelectedProvider
                globals.OrderDict("ServiceCode") = Me.CartRender1.SelectedService
                globals.OrderDict("ResidentialShipping") = Me.CartRender1.Residential()
                globals.OrderDict("ShipToZipCode") = Me.CartRender1.ZipCode()
                globals.OrderDict("SaturdayDelivery") = Me.CartRender1.SaturdayDelivery()
                globals.OrderDict("Insurance") = Me.CartRender1.Insurance()
                globals.OrderDict("InsuredValue") = CDblEx(Me.CartRender1.InsuredValue())
            Else
                globals.OrderDict("ShippingHandlingProviderGuid") = "NONE"
                globals.OrderDict("ServiceCode") = "NONE"
                globals.OrderDict("ResidentialShipping") = False
                globals.OrderDict("ShipToZipCode") = ""
                globals.OrderDict("SaturdayDelivery") = False
                globals.OrderDict("Insurance") = False
                globals.OrderDict("InsuredValue") = 0
                'AM2010031501 - ORDER RENDER - End
            End If
        End If
    End Sub
    '*****shipping*****-end

    'JA2010092201 - RECURRING ORDERS - Start
    Protected Sub SetCurrentRecurrency()
        Dim sql As String = "SELECT TOP 1 HeaderGuid FROM ShopSalesHeader WHERE ModifiedRecurrency =" & SafeString(CStrEx(globals.OrderDict("ModifiedRecurrency"))) & " ORDER BY HeaderDate DESC"
        Dim oldHeaderGuid As String = CStrEx(getSingleValueDB(sql))
        Dim currentRecurrency As New ExpDictionary

        If oldHeaderGuid <> "" Then
            currentRecurrency = eis.LoadOrder(oldHeaderGuid)
            If Not currentRecurrency Is Nothing Then
                currentRecurrency = currentRecurrency("Lines")
                If Not currentRecurrency Is Nothing Then
                    If currentRecurrency.Count > 0 Then
                        For Each item As ExpDictionary In currentRecurrency.Values
                            item("HeaderGuid") = globals.OrderDict("HeaderGuid")
                            item("LineGuid") = GenGuid()
                        Next
                        globals.OrderDict("Lines") = currentRecurrency
                        eis.SaveOrderDictionary(globals.OrderDict)
                    End If
                End If
            End If
        End If
    End Sub
    'JA2010092201 - RECURRING ORDERS - End

End Class