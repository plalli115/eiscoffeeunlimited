    Imports System.Configuration.ConfigurationManager
Imports ExpandIT.GlobalsClass
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

Partial Class _language
    Inherits ExpandIT.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim returl As String

        ' Check the page access.
        Session("LanguageGuid") = CStrEx(Request("LanguageGuid"))

        returl = CStrEx(Request("language_returl"))
        If returl = "" Then returl = "shop.aspx"

        ' If no language guid is set then set it.
        If Session("LanguageGuid").ToString <> "" Then
            eis.SetUserLanguage(Session("LanguageGuid").ToString, True)
        End If

        ' Redirect and pass along the url parameters send to this page if parameters are available.
        If Request.QueryString("ExpQueryString") <> "" Then
            SafeRedirect(returl & "?" & Request.QueryString("ExpQueryString"))
        Else
            SafeRedirect(returl)
        End If
    End Sub
End Class
