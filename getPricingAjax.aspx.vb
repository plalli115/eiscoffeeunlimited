Imports System.Configuration.ConfigurationManager
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT
Imports ExpandIT.ExpandITLib
Imports System.Web
Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.Data

Partial Class getPricingAjax
    Inherits ExpandIT.ShippingPayment._CartPage

    Protected html As String = ""
    Protected price As Double
    Protected currencyCode As String = ""
    Protected variantCode As String = ""
    Protected uomCode As String = ""
    Protected guidCode As String = ""
    Protected currentQty As Integer = 0
    Protected quantityUom As String = ""

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        price = CDblEx(Request("Price"))
        currencyCode = CStrEx(Request("Currency"))
        variantCode = CStrEx(Request("Variant"))
        uomCode = CStrEx(Request("Uom"))
        guidCode = CStrEx(Request("Guid"))
        currentQty = CIntEx(Request("minQty"))
        quantityUom = CStrEx(Request("qtyUom"))

        If currentQty = 0 Then
            currentQty = 1
        End If

        If uomCode = "undefined" Then
            uomCode = ""
        End If

        If variantCode = "undefined" Then
            variantCode = ""
        End If



        Dim cacheString As String = ""
        Dim cachekey As String = "GetPricingAjax-" & CStrEx(globals.User("CustomerGuid")) & "-" & currencyCode & "-" & variantCode & "-" & uomCode & "-" & guidCode & "-" & CStrEx(Request("minQty")) & "-" & quantityUom & "-" & CStrEx(Request("NoUom"))
        If cachekey <> "" Then cacheString = EISClass.CacheGet(cachekey) Else cacheString = Nothing
        If CStrEx(cacheString) <> "" Then
            html = cacheString
        Else
            Dim finalPriceCheck As Double
            'getSpecialPrice
            Dim specialPrice As Double = GetPrices("SpecialPrice")

            If specialPrice <> 0 Then
                finalPriceCheck = specialPrice
            Else
                'getListPrice
                finalPriceCheck = GetPrices("ListPrice")
            End If



            finalPriceCheck = NFItemPriceFixCurrencyAjax(currencyCode, finalPriceCheck)




            'Build Response Parameters ***** Start
            Dim pricePerUnit As Double = CDblEx(finalPriceCheck) '* GetProductUOMQuantity(guidCode, uomCode)
            Dim pricePerUnitCurrency As String = CurrencyFormatter.FormatCurrency(CDblEx(pricePerUnit), currencyCode)
            Dim priceNoCurrency As String = pricePerUnitCurrency.Trim(CurrencyFormatter.GetCurrentSymbol(currencyCode))

            If CStrEx(Request("NoUom")) <> "" And CStrEx(Request("NoUom")) <> "undefined" Then
                html = CurrencyFormatter.FormatCurrency(CDblEx(pricePerUnit), currencyCode)
            Else
                Dim priceQty As String
                If currencyCode <> "USD" Then
                    priceQty = CurrencyFormatter.FormatCurrency(CDblEx(pricePerUnit) * currentQty, currencyCode)
                Else
                    priceQty = CurrencyFormatter.FormatCurrency(CDblEx(priceNoCurrency) * currentQty, currencyCode)
                End If

                Dim priceQtyNoCurrency As String = priceQty.Trim(CurrencyFormatter.GetCurrentSymbol(currencyCode))

                If CStrEx(Request("minQty")) = "" Then
                    html = uomCode & "|" & pricePerUnitCurrency & "|" & "0"
                Else
                    html = uomCode & "|" & pricePerUnitCurrency & "|" & priceQtyNoCurrency
                End If

                If specialPrice <> 0 Then
                    html = html & "|special"
                Else
                    html = html & "|regular"
                End If

            End If
            'Build Response Parameters ***** End



            ' Set cache
            If cachekey <> "" Then
                Dim tableNames As String() = {"Attain_ProductPrice", "ProductTable", "ItemUnitOfMeasure"}
                cacheString = html
                EISClass.CacheSetAggregated(cachekey, cacheString, tableNames)
            End If
        End If

    End Sub

    Protected Function NFItemPriceFixCurrencyAjax(ByVal currencyGuidCheck As String, ByVal finalPrice As Double)

        'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
        'If Not CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
        If "USD" <> currencyGuidCheck Then
            finalPrice = globals.currency.ConvertCurrency(finalPrice, "USD", currencyGuidCheck)
            'orderline("CurrencyGuid") = OrderObject("CurrencyGuid")
        End If
        'End If
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - End

        Return finalPrice
    End Function

    Public Function GetProductUOMQuantity(ByVal prdGuid As String, ByVal UOM As String) As Double

        Dim sql As String

        sql = "SELECT QtyPerUnitOfMeasure FROM ProductTable INNER JOIN ItemUnitOfMeasure ON (ProductTable.ProductGuid = ItemUnitOfMeasure.ItemNo)  WHERE ProductTable.ProductGuid=" & SafeString(prdGuid) & " AND ItemUnitOfMeasure.Code=" & SafeString(UOM)

        If CDblEx(getSingleValueDB(sql)) <= 0 Then
            GetProductUOMQuantity = 1
        Else
            GetProductUOMQuantity = CDblEx(getSingleValueDB(sql))
        End If

    End Function


    Public Function GetPrices(ByVal typePrice As String)

        Dim sql As String
        Dim lowPrice As Decimal = 0
        Dim UOMPrice As Decimal = 0

        Dim currentGuid As String = guidCode
        Dim currentVariantCode As String = variantCode
        Dim currentUom As String = uomCode

        'AM2011061701 - ITEM LINKING - START
        If CBoolEx(eis.getEnterpriseConfigurationValue("LinkedItemsEnabled")) And currentVariantCode <> "" Then
            currentGuid = currentVariantCode
            currentVariantCode = ""
        End If
        'AM2011061701 - ITEM LINKING - END

        Dim filterdict1 As New ExpDictionary
        Dim filterdict2 As New ExpDictionary
        Dim filterdict3 As New ExpDictionary
        Dim filterdict4 As New ExpDictionary

        Dim minqty2 As Integer = 0
        Dim UomQty As Integer = 0
        Dim priceGroupCode As String = CStrEx(getSingleValueDB("SELECT PriceGroupCode FROM CustomerTable WHERE CustomerGuid=" & SafeString(globals.User("CustomerGuid"))))

        Dim campaign As String = ""
        If typePrice = "SpecialPrice" Then
            'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - Start
            'campaign = CStrEx(getSingleValueDB("SELECT SpecialsCampaign FROM EESetup"))

            Try
                Dim pageobj As ExpandIT.Page = CType(System.Web.HttpContext.Current.Handler, ExpandIT.Page)
                campaign = CStrEx(globals.eis.getEnterpriseConfigurationValue("SpecialsCampaign"))
            Catch ex As Exception
            End Try
            'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - End
        End If



        'Checking Currency
        sql = "SELECT ProductGuid, AllowInvoiceDiscount, Attain_VATBusPostingGrPrice, CustomerRelType, " & _
        "MinimumQuantity, EndingDate, CustomerRelGuid, CurrencyGuid, StartingDate, UnitPrice, " & _
        "UOMGuid, VariantCode, PriceInclTax, AllowLineDiscount FROM Attain_ProductPrice " & _
        "WHERE ProductGuid IN ('" & currentGuid & "') AND "

        If typePrice = "SpecialPrice" Then
            sql = sql & "(CustomerRelType = 3 AND CustomerRelGuid = " & SafeString(campaign) & ") AND "
        Else
            sql = sql & "((CustomerRelType = 0 AND CustomerRelGuid = " & SafeString(globals.User("CustomerGuid")) & ") OR " & _
                        "(CustomerRelType = 1 AND CustomerRelGuid = " & SafeString(priceGroupCode) & ") OR " & _
                        "(CustomerRelType = 2)) AND "
        End If

        sql = sql & "(StartingDate <= " & SafeDatetime(DateValue(Now)) & " OR StartingDate IS NULL) AND " & _
        "(EndingDate >= " & SafeDatetime(DateValue(Now)) & " OR EndingDate IS NULL) "
        sql = sql & "AND (CurrencyGuid = " & SafeString(CStrEx(currencyCode)) & ") "
        sql = sql & "AND (VariantCode = " & SafeString(currentVariantCode) & " OR VariantCode='') "
        sql = sql & "AND (UOMGuid = " & SafeString(currentUom) & " OR UOMGuid='') "
        sql = sql & "ORDER BY ProductGuid,VariantCode"

        Dim currencyDict As ExpDictionary = SQL2Dicts(sql)

        If Not currencyDict Is Nothing AndAlso Not currencyDict Is DBNull.Value AndAlso currencyDict.Count > 0 Then
            For Each itemCurrency As ExpDictionary In currencyDict.Values
                Dim currentQty2 As Integer = currentQty
                UOMPrice = CDblEx(itemCurrency("UnitPrice"))
                If CStrEx(itemCurrency("UOMGuid")) <> currentUom Then
                    UomQty = GetProductUOMQuantity(currentGuid, currentUom)
                    currentQty2 = currentQty2 * UomQty
                    UOMPrice = UOMPrice * UomQty
                End If
                If currentQty2 >= CDblEx(itemCurrency("MinimumQuantity")) Then
                    If lowPrice = 0 Or UOMPrice < lowPrice Then
                        lowPrice = UOMPrice
                    End If
                End If
            Next
        Else

            'Checking Not Currency

            'First Filter - Start
            If currentVariantCode <> "" Then
                sql = "SELECT ProductGuid, AllowInvoiceDiscount, Attain_VATBusPostingGrPrice, CustomerRelType, " & _
                "MinimumQuantity, EndingDate, CustomerRelGuid, CurrencyGuid, StartingDate, UnitPrice, " & _
                "UOMGuid, VariantCode, PriceInclTax, AllowLineDiscount FROM Attain_ProductPrice " & _
                "WHERE ProductGuid IN ('" & currentGuid & "') AND "

                If typePrice = "SpecialPrice" Then
                    sql = sql & "(CustomerRelType = 3 AND CustomerRelGuid = " & SafeString(campaign) & ") AND "
                Else
                    sql = sql & "((CustomerRelType = 0 AND CustomerRelGuid = " & SafeString(globals.User("CustomerGuid")) & ") OR " & _
                                "(CustomerRelType = 1 AND CustomerRelGuid = " & SafeString(priceGroupCode) & ") OR " & _
                                "(CustomerRelType = 2)) AND "
                End If

                sql = sql & "(StartingDate <= " & SafeDatetime(DateValue(Now)) & " OR StartingDate IS NULL) AND " & _
                "(EndingDate >= " & SafeDatetime(DateValue(Now)) & " OR EndingDate IS NULL) "
                sql = sql & "AND (CurrencyGuid = '') "
                sql = sql & "AND (VariantCode = " & SafeString(currentVariantCode) & ") "
                sql = sql & "AND (UOMGuid = " & SafeString(currentUom) & " OR UOMGuid = '') "
                sql = sql & " ORDER BY ProductGuid,VariantCode"

                filterdict1 = SQL2Dicts(sql)
                If Not filterdict1 Is Nothing AndAlso Not filterdict1 Is DBNull.Value AndAlso filterdict1.Count > 0 Then

                    For Each itemFilterdict1 As ExpDictionary In filterdict1.Values
                        UOMPrice = CDblEx(itemFilterdict1("UnitPrice"))
                        If itemFilterdict1("UOMGuid") <> currentUom Then
                            UomQty = GetProductUOMQuantity(currentGuid, currentUom)
                            minqty2 = UomQty * currentQty
                            UOMPrice = UOMPrice * UomQty
                        Else
                            minqty2 = currentQty
                        End If
                        If itemFilterdict1("MinimumQuantity") <= minqty2 Then
                            If lowPrice = 0 Or UOMPrice < lowPrice Then
                                lowPrice = UOMPrice
                            End If
                        End If
                    Next
                End If
            End If
            'First Filter - End


            'Second Filter - Start
            If lowPrice = 0 AndAlso currentUom <> "" Then
                sql = "SELECT ProductGuid, AllowInvoiceDiscount, Attain_VATBusPostingGrPrice, CustomerRelType, " & _
                "MinimumQuantity, EndingDate, CustomerRelGuid, CurrencyGuid, StartingDate, UnitPrice, " & _
                "UOMGuid, VariantCode, PriceInclTax, AllowLineDiscount FROM Attain_ProductPrice " & _
                "WHERE ProductGuid IN ('" & currentGuid & "') AND "

                If typePrice = "SpecialPrice" Then
                    sql = sql & "(CustomerRelType = 3 AND CustomerRelGuid = " & SafeString(campaign) & ") AND "
                Else
                    sql = sql & "((CustomerRelType = 0 AND CustomerRelGuid = " & SafeString(globals.User("CustomerGuid")) & ") OR " & _
                                "(CustomerRelType = 1 AND CustomerRelGuid = " & SafeString(priceGroupCode) & ") OR " & _
                                "(CustomerRelType = 2)) AND "
                End If

                sql = sql & "(StartingDate <= " & SafeDatetime(DateValue(Now)) & " OR StartingDate IS NULL) AND " & _
                "(EndingDate >= " & SafeDatetime(DateValue(Now)) & " OR EndingDate IS NULL) "
                sql = sql & "AND (CurrencyGuid = '') "
                sql = sql & "AND (VariantCode = '') "
                sql = sql & "AND (UOMGuid = " & SafeString(currentUom) & ") "
                sql = sql & " ORDER BY ProductGuid,VariantCode"

                filterdict3 = SQL2Dicts(sql)

                If Not filterdict3 Is Nothing AndAlso Not filterdict3 Is DBNull.Value AndAlso filterdict3.Count > 0 Then
                    For Each itemFilterdict3 As ExpDictionary In filterdict3.Values
                        UOMPrice = CDblEx(itemFilterdict3("UnitPrice"))
                        If itemFilterdict3("UOMGuid") <> currentUom Then
                            UomQty = GetProductUOMQuantity(currentGuid, currentUom)
                            minqty2 = UomQty * currentQty
                            UOMPrice = UOMPrice * UomQty
                        Else
                            minqty2 = currentQty
                        End If
                        If itemFilterdict3("MinimumQuantity") <= minqty2 Then
                            If lowPrice = 0 Or UOMPrice < lowPrice Then
                                lowPrice = UOMPrice
                            End If
                        End If
                    Next
                End If
            End If
            'Second Filter - End

            'Third Filter - Start
            If lowPrice = 0 Then
                sql = "SELECT ProductGuid, AllowInvoiceDiscount, Attain_VATBusPostingGrPrice, CustomerRelType, " & _
                "MinimumQuantity, EndingDate, CustomerRelGuid, CurrencyGuid, StartingDate, UnitPrice, " & _
                "UOMGuid, VariantCode, PriceInclTax, AllowLineDiscount FROM Attain_ProductPrice " & _
                "WHERE ProductGuid IN ('" & currentGuid & "') AND "

                If typePrice = "SpecialPrice" Then
                    sql = sql & "(CustomerRelType = 3 AND CustomerRelGuid = " & SafeString(campaign) & ") AND "
                Else
                    sql = sql & "((CustomerRelType = 0 AND CustomerRelGuid = " & SafeString(globals.User("CustomerGuid")) & ") OR " & _
                                "(CustomerRelType = 1 AND CustomerRelGuid = " & SafeString(priceGroupCode) & ") OR " & _
                                "(CustomerRelType = 2)) AND "
                End If

                sql = sql & "(StartingDate <= " & SafeDatetime(DateValue(Now)) & " OR StartingDate IS NULL) AND " & _
                "(EndingDate >= " & SafeDatetime(DateValue(Now)) & " OR EndingDate IS NULL) "
                sql = sql & "AND (CurrencyGuid = '') "
                sql = sql & "AND (VariantCode = '') "
                sql = sql & "AND (UOMGuid = '') "
                sql = sql & " ORDER BY ProductGuid,VariantCode"

                filterdict4 = SQL2Dicts(sql)

                If Not filterdict4 Is Nothing AndAlso Not filterdict4 Is DBNull.Value AndAlso filterdict4.Count > 0 Then
                    For Each itemFilterdict4 As ExpDictionary In filterdict4.Values
                        UOMPrice = CDblEx(itemFilterdict4("UnitPrice"))
                        If itemFilterdict4("UOMGuid") <> currentUom Then
                            UomQty = GetProductUOMQuantity(currentGuid, currentUom)
                            minqty2 = UomQty * currentQty
                            UOMPrice = UOMPrice * UomQty
                        Else
                            minqty2 = currentQty
                        End If
                        If itemFilterdict4("MinimumQuantity") <= minqty2 Then
                            If lowPrice = 0 Or UOMPrice < lowPrice Then
                                lowPrice = UOMPrice
                            End If
                        End If
                    Next
                End If
            End If
            'Third Filter - End

        End If

        If lowPrice = 0 And typePrice <> "SpecialPrice" Then
            'AM2011061701 - ITEM LINKING - START
            If CBoolEx(eis.getEnterpriseConfigurationValue("LinkedItemsEnabled")) And currentGuid <> "" Then
                price = CDblEx(getSingleValueDB("SELECT ListPrice FROM ProductTable WHERE ProductGuid=" & SafeString(currentGuid)))
            End If
            'AM2011061701 - ITEM LINKING - END
            lowPrice = price * GetProductUOMQuantity(currentGuid, currentUom)
        End If

        Return lowPrice

        'Dim pricePerUnit As Double = CDblEx(lowPrice) * GetProductUOMQuantity(currentGuid, currentUom)
        'Dim pricePerUnitCurrency As String = CurrencyFormatter.FormatCurrency(CDblEx(pricePerUnit), currencyCode)
        'Dim priceNoCurrency As String = pricePerUnitCurrency.Trim(CurrencyFormatter.GetCurrentSymbol(currencyCode))

        'If CStrEx(Request("NoUom")) <> "" And CStrEx(Request("NoUom")) <> "undefined" Then
        '    html = CurrencyFormatter.FormatCurrency(CDblEx(pricePerUnit), currencyCode)
        'Else
        '    Dim priceQty As String
        '    If currencyCode <> "USD" Then
        '        priceQty = CurrencyFormatter.FormatCurrency(CDblEx(pricePerUnit) * currentQty, currencyCode)
        '    Else
        '        priceQty = CurrencyFormatter.FormatCurrency(CDblEx(priceNoCurrency) * currentQty, currencyCode)
        '    End If

        '    Dim priceQtyNoCurrency As String = priceQty.Trim(CurrencyFormatter.GetCurrentSymbol(currencyCode))

        '    If CStrEx(Request("minQty")) = "" Then
        '        html = uomCode & "|" & pricePerUnitCurrency & "|" & "0"
        '    Else
        '        html = uomCode & "|" & pricePerUnitCurrency & "|" & priceQtyNoCurrency
        '    End If

        'End If

    End Function

    Protected Function onlinePriceAvailable(ByVal guid As String)
        Dim sql As String = "SELECT OnlinePrice FROM ProductTable WHERE ProductGuid=" & SafeString(guid)
        Return CDblEx(getSingleValueDB(sql))
    End Function

End Class
