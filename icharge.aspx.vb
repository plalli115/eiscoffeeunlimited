Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports nsoftware.IBizPay


Namespace vb


Partial Class icharge
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        Me.icharge1 = New nsoftware.IBizPay.Icharge()
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not (Page.IsPostBack) Then
            Dim Years As New ArrayList()
            Dim i As Integer
            For i = 1 To 20
                Years.Add(DateTime.Now.AddYears(i - 1).Date.ToString("yy"))
            Next i
            ddlCardExpYear.DataSource = Years
                ddlCardExpYear.DataBind()
                ddlCardExpYear.SelectedIndex = 3

            ddlCardExpMonth.SelectedIndex = (DateTime.Now.Month - 1)

            ddlGateway.DataSource = System.Enum.GetValues(GetType(nsoftware.IBizPay.IchargeGateways))
            ddlGateway.DataBind()
            ddlGateway.SelectedIndex = 1 'set Authorze.Net as the default gateway
        End If

    End Sub

    Private Sub bCharge_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bCharge.Click

        If (Me.cardIsValid()) Then 'If PreAuthorize passes, continue authorization:
            icharge1.Gateway = System.Enum.Parse(GetType(nsoftware.IBizPay.IchargeGateways), ddlGateway.SelectedItem.Value)

            If icharge1.Gateway = IchargeGateways.gwAuthorizeNet Then
                icharge1.GatewayURL = "https://test.authorize.net/gateway/transact.dll"
                txtGatewayLogin.Text = "cnpdev1047"
                icharge1.AddSpecialField("x_test_request", "true")
            End If


            icharge1.AddSpecialField("x_test_request", "true")

            icharge1.CardExpMonth = ddlCardExpMonth.SelectedItem.Value
            icharge1.CardExpYear = ddlCardExpYear.SelectedItem.Value
            icharge1.CardNumber = txtCardNumber.Text
            icharge1.CustomerAddress = txtCustomerAddress.Text
            icharge1.CustomerCity = txtCustomerCity.Text
            icharge1.CustomerCountry = "US"
            icharge1.CustomerEmail = txtCustomerEmail.Text
            icharge1.CustomerFirstName = txtCustomerFirstName.Text
            icharge1.CustomerLastName = txtCustomerLastName.Text
            icharge1.CustomerPhone = txtCustomerPhone.Text
            icharge1.CustomerState = txtCustomerState.Text
            icharge1.CustomerZip = txtCustomerZip.Text
            icharge1.InvoiceNumber = txtTransactionInvoice.Text
            icharge1.MerchantLogin = txtGatewayLogin.Text
            icharge1.MerchantPassword = txtGatewayPassword.Text
            icharge1.TransactionAmount = txtTransactionAmount.Text
            icharge1.TransactionDesc = txtTransactionDescription.Text
            Try
                'Add the x_test_request for test transactions with Authorize.Net
                icharge1.AddSpecialField("x_test_request", "true")

                icharge1.Authorize() ' DO AUTHORIZATION
                If (icharge1.TransactionApproved) Then
                    litOutput.Text += "Transaction Approved." ' Evaluate Response Code
                Else
                    litOutput.Text += "<font color='red'>Transaction Declined:</font> " + icharge1.ResponseText
                End If
            Catch Ex As nsoftware.IBizPay.IBizPayException
                litOutput.Text += "Transaction Error: " + Ex.Message + "<br>"
            End Try

            pnlAuthResults.Visible = True
            Me.Page.DataBind()
            End If
    End Sub

    Private Function cardIsValid() As Boolean
        'Lets preauthorize first:
        icharge1.CardNumber = txtCardNumber.Text
        icharge1.CardExpMonth = ddlCardExpMonth.SelectedItem.Value
        icharge1.CardExpYear = ddlCardExpYear.SelectedItem.Value
        Try
            litOutput.Text += "Pre-Authorization Status: <br><br>"
            icharge1.PreAuthorize()
            litOutput.Text += "<font color='blue'><i>Pre-Authorization checks passed. </i></font><br><br>"
            Return True
        Catch ex As nsoftware.IBizPay.IBizPayException
            litOutput.Text += "<font color='red'>Pre-Authorization checks failed: </font>" + ex.Message
        End Try
        Return False
    End Function

    Private Sub ddlGateway_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlGateway.SelectedIndexChanged
   icharge1.ResetData()

   Select Case (ddlGateway.SelectedIndex)
        Case 1
            lblWarning.Visible = False
            icharge1.Gateway = IchargeGateways.gwAuthorizeNet
            icharge1.GatewayURL = "https://test.authorize.net/gateway/transact.dll"
            icharge1.AddSpecialField("x_test_request", "true")
            txtGatewayLogin.Text = "cnpdev1047"
            txtCardNumber.Text = "4444333322221111"
        Case Else
            lblWarning.Visible = True
            txtGatewayLogin.Text = ""
            txtGatewayPassword.Text = ""
            txtCardNumber.Text = ""
    End Select
    End Sub

End Class

End Namespace
