Imports System.Configuration.ConfigurationManager
Imports ExpandIT.Debug
Imports ExpandIT.GlobalsClass
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT

Partial Class _mailtip
    Inherits ExpandIT.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim bSendSuccess As Boolean = False
        Dim initialized As String
        Dim returl As String

        initialized = Request("initialized")

        If initialized = "true" Then

            Dim mail As New Net.Mail.MailMessage(Request("senderMail"), Request("recipientMail"))
            mail.IsBodyHtml = CStrEx(AppSettings("MAIL_FORMAT")) = "0" '0: HTML
            mail.Subject = Request("productName")
            mail.Body = HtmlString()

            '  Send the mail.
            Dim smtp As New Net.Mail.SmtpClient()
            smtp.Host = AppSettings("MAIL_REMOTE_SERVER")
            smtp.Port = AppSettings("MAIL_REMOTE_SERVER_PORT")
            Try
                smtp.Credentials = Net.CredentialCache.DefaultNetworkCredentials
                smtp.Send(mail)
                bSendSuccess = True
            Catch ex As Exception
            End Try

        End If

        returl = VRoot & "/mailtip_iframe.asp?mylink=" & Request("mylink") & "&productName=" & Request("productName") & "&bSendSuccess=" & bSendSuccess & "&initialized=" & Request("initialized")

        Response.Redirect(returl)

    End Sub

    Function HtmlString() As String

        Dim topString, bottomString As String

        topString = buildHtmlTop()
        bottomString = buildHtmlBottom()

        Return topString & "<p>" & Request("comments") & "</p><p><i>" & Resources.Language.LABEL_PRODUCT_LINK & ":</i><a href=" & Request("mylink") & ">" & Request("productName") & "</a></p>" & bottomString
    End Function

    Function buildHtmlTop() As String
        Return "<html><body><table><tr><td><p><i>" & Resources.Language.MAIL_TOP_MESSAGE & "</i></p>"
    End Function

    Function buildHtmlBottom() As String
        Return "<p><i>" & Resources.Language.MAIL_BOTTOM_MESSAGE & "</i></p></td></tr></table></body></html>"
    End Function

End Class
