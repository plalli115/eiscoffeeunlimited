﻿Imports System.Configuration.ConfigurationManager
Imports System.Xml
Imports System.Web
Imports System.IO 

Partial Class _NoteMailPreview
    Inherits System.Web.UI.Page
  
  Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Response.Write(AdjustImgUrls(Session("NoteMailHtml"))) 
  End Sub
  
  private Function AdjustImgUrls(inHtml As String) As String    
    Dim outBuilder as StringBuilder = Nothing
    Dim errorMsg As String = ""
    Dim templDirVirtualPath As String = String.Format("~/{0}/",AppSettings("NOTE_MAIL_FOLDER"))
    Try
      Dim contentDOM As New XmlDocument()
      contentDOM.LoadXml(inHtml)
      Dim imgList As XmlNodeList = contentDOM.SelectNodes("descendant::img") 
      For Each node As XmlNode In imgList
        Dim attrib As XmlNode = node.Attributes.GetNamedItem("src")
        If Not (attrib is Nothing) then
          Dim oldPath As String = attrib.Value 
          Dim newPath As String = VirtualPathUtility.ToAbsolute(VirtualPathUtility.Combine(templDirVirtualPath, oldPath)) 
          attrib.Value = newPath   
        End If
      Next
      outBuilder = new StringBuilder()
      Dim writer as TextWriter  = new StringWriter(outBuilder)
      contentDOM.Save(writer) 
    Catch ex As Exception
      errorMsg = ex.Message
    End Try
    If outBuilder is Nothing then
      Return errorMsg
    Else
      Return outBuilder.ToString()
    End If
  End Function
  
End Class
