<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PayPalPostForm.aspx.vb" Inherits="PayPalPostForm" Theme="" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EisClass" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Check out with PayPal</title>
</head>
<%="" %>
<body>
    
    <% =writeFormCart()%>
    
    <script type="text/javascript">
        document.forms["payForm"].submit ();
    </script>
</body>
</html>
