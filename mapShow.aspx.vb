'JA2011021701 - STORE LOCATOR - Start
Imports System.Configuration.ConfigurationManager
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT
Imports ExpandIT.ExpandITLib
Imports System.Web

Partial Class mapShow
    Inherits ExpandIT.ShippingPayment._CartPage

    Protected PointsDict As ExpDictionary
    
    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If CBoolEx(AppSettings("SHOW_STORE_LOCATOR")) Then
            Dim sql As String = "SELECT * FROM FranchiseLocation"
            PointsDict = SQL2Dicts(sql)
        Else
            Response.Redirect(VRoot & "/shop.aspx")
        End If
       

    End Sub

    'JA2011021701 - STORE LOCATOR - End

End Class
