Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT.GlobalsClass
Imports ExpandIT.SectionSettingsManager
REM << Added Import Statement >>
Imports ExpandIT31.ExpanditFramework.Util

Partial Class UserPassword
    Inherits ExpandIT.Page

    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	
	Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If CBoolEx(AppSettings("USE_NTLM")) Then Exit Sub
        ' Check the page access.
        eis.PageAccessRedirect("OpenSite")

        If Page.IsPostBack Then
            Dim errorString As String = String.Empty
            Dim pwd As String = validateNewPassword(errorString)

            If Not String.IsNullOrEmpty(pwd) Then
                If updatePassword(pwd) Then
                    successinfo.Text = Resources.Language.MESSAGE_USER_INFORMATION_UPDATED
                    ' Send mail
                    sendCustomerConfirmationEmail(Resources.Language.LABEL_MAIL_SUBJECT_PASSWORD_CHANGED)
                End If
            Else
                If Not String.IsNullOrEmpty(errorString) Then
                    Dim s() As String = Split(errorString, Environment.NewLine)
                    errorBulletList.Items.Clear()
                    For i As Integer = 0 To s.Length - 1
                        If Not String.IsNullOrEmpty(s(i)) Then
                            errorBulletList.Items.Add(New ListItem(s(i)))
                        End If
                    Next
                End If
            End If
        Else

        End If

    End Sub

    Private Function updatePassword(ByVal newPassword As String) As Boolean
        Try
            ExpandIT.ExpandITLib.excecuteNonQueryDb("UPDATE UserTable SET UserPassword = " & SafeString(newPassword) & " WHERE UserGuid = " & SafeString(Session("UserGuid")))
            Return True
        Catch ex As Exception

        End Try
    End Function

    Private Function validateNewPassword(ByRef errorString As String) As String
        Dim FormerPassword, NewPassword1, NewPassword2 As String
        Dim sb As New StringBuilder()

        FormerPassword = Trim(CStrEx(Request.Form(OldPassword.UniqueID)))
        NewPassword1 = Trim(CStrEx(Request.Form(Password1.UniqueID)))
        NewPassword2 = Trim(CStrEx(Request.Form(Password2.UniqueID)))

        ' If user inputs the correct (old) password
        If eis.Encrypt(FormerPassword).Equals(globals.User("UserPassword")) Then
            ' And the new password fields have equal input
            If NewPassword1 = NewPassword2 Then
                ' If the input isn't too long or too short
                If NewPassword1.Length <= Password1.MaxLength AndAlso NewPassword1.Length >= CLngEx(AppSettings("PASSWORD_LEN")) Then
                    ' return the new password
                    Return eis.Encrypt(NewPassword1)
                Else
                    sb.AppendLine(Resources.Language.MESSAGE_THE_VALUE_IN_FIELD_IS_NOT_CORRECT_1 & " " & _
                    Resources.Language.LABEL_USERDATA_NEW_PASSWORD & " " & _
                    Resources.Language.MESSAGE_THE_VALUE_IN_FIELD_IS_NOT_CORRECT_2 & " " & _
                    CLngEx(AppSettings("PASSWORD_LEN")) & " " & _
                    Resources.Language.MESSAGE_THE_VALUE_IN_FIELD_IS_NOT_CORRECT_3 & " " & _
                    Password1.MaxLength & " " & _
                    Resources.Language.MESSAGE_THE_VALUE_IN_FIELD_IS_NOT_CORRECT_4 & ".")
                End If
            Else
                sb.AppendLine(Resources.Language.MESSAGE_NEW_PASSWORDS_ARE_INCORRECT)
            End If
        Else
            sb.AppendLine(Resources.Language.MESSAGE_PASSWORD_INCORRECT)
        End If
        If sb.Length > 0 Then
            errorString = sb.ToString()
        End If
        Return Nothing
    End Function

    Public Function sendCustomerConfirmationEmail(ByVal subject As String) As Boolean
        Dim message As String = Nothing
        message = prepareHtmlMail(Encoding.UTF8)
        If message IsNot Nothing Then
            Mailer.SendEmail(AppSettings("MAIL_REMOTE_SERVER"), AppSettings("MAIL_REMOTE_SERVER_PORT"), _
                CStrEx(AppSettings("ORDER_EMAIL_FROM")), _
                Trim(CStrEx(globals.User("EmailAddress"))), _
                Trim(CStrEx(globals.User("ContactName"))), _
                subject, message, True, Encoding.UTF8, globals.messages)
        End If
    End Function

    Protected Function prepareHtmlMail(ByVal reqEncoding As Encoding) As String
        Dim retStr As String = Nothing
        Dim standardLanguageISO As String = "en"
        Dim folder As String = AppSettings("USER_UPDATE_MAIL_FOLDER")
        Dim cssFileFolder As String = AppSettings("CSS_MAIL_FILE_FOLDER")
        Dim cssfile As String = AppSettings("CSS_MAIL_FILE")
        Dim languageISO As String = CStrEx(HttpContext.Current.Session("LanguageGuid")).ToLower()

        Dim filename As String
        Dim standardFilename As String
        Dim mailPath As String
        Dim standardMailPath As String
        Dim cssPath As String

        filename = AppSettings("USER_UPDATE_MAIL_NAME_MAIN_PART") & languageISO & "." & AppSettings("USER_UPDATE_MAIL_SUFFIX")
        standardFilename = AppSettings("USER_UPDATE_MAIL_NAME_MAIN_PART") & standardLanguageISO & "." & AppSettings("USER_UPDATE_MAIL_SUFFIX")

        mailPath = "~/" & folder & "/" & filename
        standardMailPath = "~/" & folder & "/" & standardFilename
        cssPath = "~/" & cssFileFolder & "/" & cssfile

        Dim str As String = ExpandIT.FileReader.readFile(mailPath)

        If str Is Nothing Then
            str = ExpandIT.FileReader.readFile(standardMailPath)
        End If

        If str IsNot Nothing Then
            Dim dictUser As ExpDictionary = globals.User
            Dim replaceDict As ExpDictionary = eis.replaceParametersWithUserDictValues("MAIL_USER_REPLACE_PARAMS", globals.User)
            retStr = MailUtils.getInlineStyleSheetFromFile(cssPath, reqEncoding) & eis.replaceHtmlFileContent(str, replaceDict)
        End If

        Return retStr

    End Function

End Class