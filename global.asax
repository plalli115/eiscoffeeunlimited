﻿<%@ Application Language="VB" %>

<script runat="server">  
    

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)        
        
        
        'Code that runs on application startup
        'set up the cache dependencies
        Application.Lock()
        
        'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - Start
        Dim sql As String = "SELECT * FROM EnterpriseCatalogSetup"
        Dim catalogFieldsDict As ExpandIT.ExpDictionary = ExpandIT.ExpandITLib.SQL2Dicts(sql)
        Dim instanceFieldsDict As New ExpandIT.ExpDictionary
        Dim setupFieldsDict As New ExpandIT.ExpDictionary
        
        Application("Config") = New ExpandIT.ExpDictionary
        
        If Not catalogFieldsDict Is Nothing AndAlso Not catalogFieldsDict Is DBNull.Value AndAlso catalogFieldsDict.Count > 0 Then
            For Each item As ExpandIT.ExpDictionary In catalogFieldsDict.Values

                
                Application("Config")(item("Catalog")) = New ExpandIT.ExpDictionary
                
                For Each itemKeys As String In item.Keys
                    Application("Config")(item("Catalog"))(itemKeys) = item(itemKeys)
                Next
             
                
                sql = "SELECT * FROM EnterpriseInstanceSetup"
                instanceFieldsDict = ExpandIT.ExpandITLib.Sql2Dictionary(sql)
                If Not instanceFieldsDict Is Nothing AndAlso Not instanceFieldsDict Is DBNull.Value AndAlso instanceFieldsDict.Count > 0 Then
                    For Each item2 As String In instanceFieldsDict.Keys
                        If Application("Config")(item("Catalog"))(item2) Is Nothing Then
                            Application("Config")(item("Catalog"))(item2) = instanceFieldsDict(item2)
                        End If
                    Next
                End If
                
                
                sql = "SELECT * FROM EnterpriseSetup"
                setupFieldsDict = ExpandIT.ExpandITLib.Sql2Dictionary(sql)
                If Not setupFieldsDict Is Nothing AndAlso Not setupFieldsDict Is DBNull.Value AndAlso setupFieldsDict.Count > 0 Then
                    For Each item3 As String In setupFieldsDict.Keys
                        If Application("Config")(item("Catalog"))(item3) Is Nothing Then
                            Application("Config")(item("Catalog"))(item3) = setupFieldsDict(item3)
                        End If
                    Next
                End If
                
                
            Next
        End If
        
        
        'Application("Config")("Catalog")("key")
        'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - End
        
        
        
        'ExpandIT31.ExpanditFramework.Infrastructure.CacheManager.initializeCachedTables()
        Application("Closed") = False
        Application.UnLock()
    End Sub
    
    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application shutdown
    End Sub
        
    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when an unhandled error occurs
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a new session is started        
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a session ends. 
        ' Note: The Session_End event is raised only when the sessionstate mode
        ' is set to InProc in the Web.config file. If session mode is set to StateServer 
        ' or SQLServer, the event is not raised.
    End Sub   
    
</script>