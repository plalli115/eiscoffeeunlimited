Imports System.Configuration.ConfigurationManager
Imports ExpandIT.Debug
Imports ExpandIT.EISClass

Partial Class user_logout
    Inherits ExpandIT.Page

    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	
	Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '<!--JA2010060101 - ALERTS - Start-->
        Session("LoadFirstTime") = "True"
        '<!--JA2010060101 - ALERTS - End-->
    End Sub

End Class
