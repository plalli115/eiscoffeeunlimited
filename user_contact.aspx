﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="user_contact.aspx.vb" Inherits="user_contact"
    CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master"
    RuntimeMasterPageFile="ThreeColumn.master" CrumbName="<%$ Resources: Language, LABEL_REGISTRATION_CONFIRM %>" %>

<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<%@ Register Src="~/controls/ContactForm.ascx" TagName="ContactForm" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <uc1:pageheader id="PageHeader1" runat="server" text="<%$ Resources: Language, LABEL_REGISTRATION_CONFIRM %>"
        enabletheming="true" />
        
    <div class="RegistrationConfirfPage">
        <asp:Label ID=Label1 Text="<%$ Resources: Language, LABEL_REGISTRATION_CONFIRM_INFO %>"
            runat="server"></asp:Label>
            <uc2:ContactForm id="ContactForm1" runat="server" />
    </div>
</asp:Content>
