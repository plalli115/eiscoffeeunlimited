'AM2010091001 - ONLINE PAYMENTS - Start
Imports System.Configuration.ConfigurationManager
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT.GlobalsClass
Imports ExpandIT
Imports System.Web
Imports System.Collections.Generic

Partial Class onlinePaymentConfirmed
    Inherits ExpandIT.Page

    Protected PaymentReference As String
    Protected DrillDownType As String
    Protected DrillDownGuid As String

    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	
	Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        PaymentReference = CStrEx(Request("PaymentReference"))
        DrillDownType = CStrEx(Request("DrillDownType"))
        DrillDownGuid = CStrEx(Request("DrillDownGuid"))
    End Sub

    Public Function paymentMessage()
        Dim message As String = ""

        message = Resources.Language.MESSAGE_ONLINE_PAYMENT_IS_FINISHED
        message = Replace(message, "[%PaymentReference%]", "<b>" & PaymentReference & "</b>")
        message = Replace(message, "[%DocumentType%]", CStrEx(b2b.GetDocumentTypeDescription(DrillDownType)))
        message = Replace(message, "[%DocumentGuid%]", "<b>" & DrillDownGuid & "</b>")

        Return message
    End Function

    Public Function backToDocumentMessage()
        Dim message As String

        message = Resources.Language.MESSAGE_CLICK_BACK_TO_DOCUMENT
        message = Replace(message, "[%DocumentType%]", CStrEx(b2b.GetDocumentTypeDescription(DrillDownType)))
        message = Replace(message, "[%Here%]", "<a href=""" & drillDownLink() & """>here</a>")

        Return message
    End Function

    Public Function backToLedgerEntries()
        Dim message As String

        message = Resources.Language.MESSAGE_CLICK_BACK_TO_LEDGER_ENTRIES
        message = Replace(message, "[%Here%]", "<a href=""history_ledgerentry.aspx"">here</a>")

        Return message
    End Function

    Protected Function drillDownLink() As String
        Dim link As String = ""
        Dim amount As Double

        'JA2011030701 - PAYMENT TABLE - START
        'Dim sql As String = "SELECT SUM(PaymentTransactionAmount) AS PaidAmount FROM EEPGOnlinePayments WHERE DocumentGuid = " & SafeString(DrillDownGuid)
        Dim sql As String = "SELECT SUM(PaymentTransactionAmount) AS PaidAmount FROM PaymentTable WHERE DocumentGuid = " & SafeString(DrillDownGuid)
        'JA2011030701 - PAYMENT TABLE - END

        amount = CDblEx(getSingleValueDB("SELECT Amount FROM CustLedgerEntryView WHERE DocumentType=" & DrillDownType & " AND DocumentGuid=" & SafeString(DrillDownGuid)))

        If CBoolEx(AppSettings("EXPANDIT_US_USE_EEPG_ONLINE_PAYMENTS")) And CDblEx(getSingleValueDB(sql)) < amount And DrillDownType = 2 Then
            link = "history_detail.aspx?OnlinePayments=1&DrillDownType=" & CIntEx(DrillDownType) & "&DrillDownGuid=" & CStrEx(DrillDownGuid)
        Else
            link = "history_detail.aspx?DrillDownType=" & CIntEx(DrillDownType) & "&DrillDownGuid=" & CStrEx(DrillDownGuid)
        End If

        Return link
    End Function

End Class
'AM2010091001 - ONLINE PAYMENTS - End
