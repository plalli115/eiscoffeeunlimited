<%@ Page Language="VB" AutoEventWireup="false" CodeFile="shipping.aspx.vb" Inherits="shipping"
    ValidateRequest="false" EnableEventValidation="false" Debug="true" CodeFileBaseClass="ExpandIT.ShippingPayment.ShippingPaymentPage"
    MasterPageFile="~/masters/default/main.master" RuntimeMasterPageFile="ThreeColumn.master"
    CrumbName="<%$ Resources: Language, LABEL_SHIPPING_ADDRESS %>" %>
    
<%--AM2010080301 - CHECK OUT PROCESS MAP - Start--%>
<%@ MasterType VirtualPath="~/masters/EnterpriseBlue/ThreeColumn.master" %>
<%--AM2010080301 - CHECK OUT PROCESS MAP - End--%>

<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.GlobalsClass" %>
<%@ Reference Control="~/controls/shipping/designmodels/DesignModel_2.ascx" %>
<%@ Reference Control="~/controls/shipping/designmodels/designModel_1.ascx" %>
<%@ Reference Control="~/controls/shipping/designmodels/DesignModelB2B1.ascx" %>
<%@ Reference Control="~/controls/shipping/designmodels/DesignModelB2B3.ascx" %>
<%@ Reference Control="~/controls/shipping/designmodels/ShippingAnonymous.ascx" %>
<%@ Reference Control="~/controls/shipping/designmodels/DesignModel_3_List.ascx" %>
<%@ Reference Control="~/controls/shipping/designmodels/DesignModelB2B1List.ascx" %>
<%@ Reference Control="~/controls/shipping/designmodels/DesignModelB2B3List.ascx" %>
<%@ Reference Control="~/controls/shipping/designmodels/B2C1OnlyOne.ascx" %>
<%@ Register TagPrefix="exp" TagName="shippingprovider" Src="~/controls/shipping/parts/ShippingProviderSelection.ascx" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<%@ Register Src="~/controls/Message.ascx" TagPrefix="uc1" TagName="PageMessage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <script type="text/javascript">
        function ssiCopyCustomFields(source, destination) {
            document.getElementById(destination).checked = document.getElementById(source).checked;
        }
    </script>

    <script type="text/javascript" src="script/genericScripts.js"></script>

    <asp:Panel runat="server" ID="ShippingPanel" DefaultButton="Purchase">
        <uc1:PageMessage runat="server" ID="pageMessage1" />
        <div id="errorMessageDiv" style="display: none;">
            <asp:Label ID="errorHeaderText" runat="server" ForeColor="Red" Text="<%$Resources: Language, MESSAGE_REQUIRED_FIELD %>" />
        </div>
        <asp:ValidationSummary runat="server" ID="validationSummary1" DisplayMode="List"
            ShowMessageBox="false" HeaderText="<%$Resources: Language, LABEL_ERROR %>" Visible="true"
            ValidationGroup="purchasegroup" ShowSummary="false" EnableClientScript="false" />
        <div class="ShippingAddress">
            <uc1:PageHeader ID="PageHeader1" runat="server" Text="<%$ Resources: Language, LABEL_SHIPPING_ADDRESS %>"
                EnableTheming="true" />
            <asp:PlaceHolder ID="placeholder1" runat="server" />
            <div class="ShippingPurchaseButtonDiv" id="purchasebuttondiv">
                <asp:Button ID="Purchase" runat="server" CssClass="AddButton" ValidationGroup="purchasegroup"
                    OnClick="Purchase_Click" />
            </div>
        </div>
    </asp:Panel>
</asp:Content>
