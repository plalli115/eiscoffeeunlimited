Imports System.Configuration.ConfigurationManager
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT

Partial Class Link
    Inherits ExpandIT.Page

    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ProductGuid As String = ""
        Dim info As ExpDictionary
        Dim Product As ExpDictionary
        Dim template As String
        Dim sql As String
        Dim groupproduct As ExpDictionary
        Dim group As ExpDictionary
        Dim linkurl As String
        Dim GroupGuid As Integer
        Dim cachekey As String
        Dim tableNames As String() = Split(AppSettings("GroupProductRelatedTablesForCaching"), "|")



        '<!--JM2010061001 - LINK TO - Start-->
        If CStrEx(Request("LinkTo")) = "" Then

            ProductGuid = Request("ProductGuid")
            GroupGuid = Integer.Parse(CLngEx(Request("GroupGuid")))
        Else
            Dim linktoDict As ExpDictionary
            linktoDict = eis.getLinkTo(CStrEx(Request("LinkTo")))
            If Not linktoDict Is Nothing Then
                For Each item As ExpDictionary In linktoDict.Values
                    If item("PropOwnerTypeGuid") = "PRD" Then
                        ProductGuid = item("PropOwnerRefGuid")
                    Else
                        GroupGuid = item("PropOwnerRefGuid")
                    End If
                Next
            End If

        End If
        '<!--JM2010061001 - LINK TO - End-->
        'AM2010071901 - ENTERPRISE CSC - Start
        cachekey = "Link-" & HttpContext.Current.Request.QueryString.ToString & "-" & globals.User("CatalogNo")
        'AM2010071901 - ENTERPRISE CSC - End
        '*****CUSTOMER SPECIFIC CATALOGS***** -START
        'ExpandIT US - USE CSC - Start
        If CBoolEx(AppSettings("EXPANDIT_US_USE_CSC")) Then
            cachekey &= "-" & CStrEx(globals.User("CustomerGuid"))
        End If
        'cachekey = "Link-" & HttpContext.Current.Request.QueryString.ToString
        'ExpandIT US - USE CSC - End
        '*****CUSTOMER SPECIFIC CATALOGS***** -END

        If ProductGuid <> "" Then
            linkurl = CacheGet(cachekey)
            If linkurl Is Nothing Then

                ' Display Product
                info = eis.CatProducts2Info(New String() {ProductGuid})
                info("ProductInfo")("ProductFieldArray") = New String() {"ProductName"}
                info("ProductInfo")("PropertyArray") = New String() {"TEMPLATE"}
                eis.CatLoadProducts(info)
                Product = info("ProductInfo")("Products")(ProductGuid)

                ' Determine the template
                If Product("TEMPLATE") Is Nothing Then
                    template = AppSettings("DEFAULT_PRODUCT_TEMPLATE")
                Else
                    template = Product("TEMPLATE")
                End If

                ' Get the groupguid if none was set
                If Request("GetGroup") <> "" And GroupGuid = 0 Then
                    'AM2010071901 - ENTERPRISE CSC - Start
                    sql = "SELECT TOP 1 GroupTable.GroupGuid FROM GroupTable INNER JOIN GroupProduct ON GroupTable.GroupGuid=GroupProduct.GroupGuid WHERE GroupProduct.ProductGuid=" & SafeString(ProductGuid) & " AND GroupTable.CatalogNo=" & SafeString(globals.User("CatalogNo")) & " ORDER BY GroupTable.SortIndex"
                    'sql = "SELECT TOP 1 GroupTable.GroupGuid FROM GroupTable INNER JOIN GroupProduct ON GroupTable.GroupGuid=GroupProduct.GroupGuid WHERE GroupProduct.ProductGuid=" & SafeString(ProductGuid) & " ORDER BY GroupTable.SortIndex"
                    'AM2010071901 - ENTERPRISE CSC - End
                    groupproduct = Sql2Dictionary(sql)
                    If groupproduct IsNot Nothing Then
                        GroupGuid = CIntEx(groupproduct("GroupGuid"))
                    Else
                        globals.messages.Errors.Add(Replace(eis.GetLabel(Resources.Language.MESSAGE_PRODUCT_DOES_NOT_EXISTS), "%1", ProductGuid))
                        Response.Redirect("~/shop.aspx?" & globals.messages.QueryString(), True)
                    End If
                End If

                linkurl = Server.UrlPathEncode(VRoot) & "/templates/" & Server.UrlPathEncode(template) & "?ProductGuid=" & Server.UrlEncode(ProductGuid)
                linkurl = linkurl & IIf(GroupGuid <> 0, "&GroupGuid=" & Server.UrlEncode(GroupGuid.ToString), "")
                linkurl = linkurl & isLanguageGuid(Session("LanguageGuid").ToString)
                CacheSetAggregated(cachekey, linkurl, tableNames)
            End If
            Response.Redirect(linkurl)
        ElseIf GroupGuid <> 0 Then
            cachekey = "Link-" & HttpContext.Current.Request.QueryString.ToString & "-" & globals.User("CatalogNo")
            linkurl = CacheGet(cachekey)
            If linkurl Is Nothing Then
                info = eis.CatGroups2Info(New String() {GroupGuid})
                info("GroupInfo")("PropertyArray") = New String() {"TEMPLATE"}
                eis.CatLoadGroups(info)
                group = info("GroupInfo")("Groups")(GroupGuid.ToString)
                If group("TEMPLATE") Is Nothing Then
                    template = AppSettings("DEFAULT_GROUP_TEMPLATE")
                Else
                    template = group("TEMPLATE")
                End If
                linkurl = VRoot & "/templates/" & template & "?GroupGuid=" & GroupGuid & isLanguageGuid(Session("LanguageGuid").ToString)
                CacheSetAggregated(cachekey, linkurl, tableNames)
            End If
            Response.Redirect(linkurl)
        Else
            Response.Write("Product or Group not specified.")
        End If
    End Sub

    Function isLanguageGuid(ByVal LangGuid As String) As String
        Dim retv As String
        If Request("LanguageGuid") <> "" Then
            retv = "&LanguageGuid=" & Request("LanguageGuid")
            Response.Cookies("user")("LanguageGuid") = Request("LanguageGuid")
            Response.Cookies("user").Path = HttpContext.Current.Server.UrlPathEncode(VRoot) & "/"
        Else
            retv = ""
        End If
        Return retv
    End Function

End Class
