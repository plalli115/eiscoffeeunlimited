<%@ WebHandler Language="VB" Class="pmt_dibs_popup" %>

Imports System
Imports System.Web
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

Public Class pmt_dibs_popup : Implements IHttpHandler
    
    Private AcceptURL, CancelURL As String
    Private ParentWindow As String
    
    Private posList As String() = _
    { _
    "popaction", _
    "shopwindow", _
    "color", _
    "merchant", _
    "orderid", _
    "lang", _
    "amount", _
    "currency", _
    "accepturl", _
    "callbackurl", _
    "cancelurl", _
    "orgaccepturl", _
    "orgcancelurl", _
    "HTTP_COOKIE", _
    "calcfee", _
    "delivery1.", _
    "delivery5.", _
    "ordline", _
    "priceinfo", _
    "test" _
    }
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/html"
        AcceptURL = context.Request.Form("orgaccepturl")  ' Your original accepturl without parameters
        CancelURL = context.Request.Form("orgcancelurl") ' Your original cancelurl without parameters
        ParentWindow = context.Request.Form("shopwindow") ' The name of the window that opened the popup
        
        If context.Request("popaction") = "PopUp" Then
            Call PopUp(context)
        Else 'On the way back from DIBS payment window
            If context.Request("popresult") = "accept" Then
                Call PopDown(context, AcceptURL, ParentWindow)
            End If
            If context.Request("popresult") = "cancel" Then
                Call PopDown(context, CancelURL, ParentWindow)
            End If
        End If
    End Sub
 
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property
        
    ''' <summary>
    ''' A function that acts as an intermediate step in opening the DIBS payment window.
    ''' </summary>
    ''' <param name="context"></param>
    ''' <remarks></remarks>
    Sub PopUp(ByVal context As HttpContext)

        Dim inputField As String
        Dim inputValue As String
        
        context.Response.Write("<html>")
        context.Response.Write("<body onload=""document.DIBSForm.submit();"">")
        'context.Response.Write("<body>")
        context.Response.Write("<form method=""post"" name=""DIBSForm"" action=""https://payment.architrade.com/payment/start.pml"" />")
        For Each inputField In context.Request.Form
            inputValue = context.Request.Form(inputField)
            If filter(inputField) Then
                context.Response.Write("<input type=""hidden"" name=""" & inputField & """ value=""" & HTMLEncode(inputValue) & """ />")
            End If
        Next
        context.Response.Write("</form>")
        context.Response.Write("</body></html>")
    End Sub
   
    ''' <summary>
    ''' A function that acts as an intermediate step in comming back from the DIBS payment window.
    ''' </summary>
    ''' <param name="context"></param>
    ''' <param name="URL"></param>
    ''' <param name="Window"></param>
    ''' <remarks></remarks>
    Sub PopDown(ByVal context As HttpContext, ByVal URL As String, ByVal Window As String)
        context.Response.Write("<html>")
        context.Response.Write("<head>")
        context.Response.Write("<script type=""text/javascript"">")
        context.Response.Write("function closePopup() {")
        context.Response.Write("  document.write('This window should close automatically. If it does not, please close it manually.');")
        context.Response.Write("  window.close();")
        context.Response.Write("}")
        context.Response.Write("</script>")
        context.Response.Write("</head>")
        context.Response.Write("<body onload=""document.ReturnFromDIBSForm.submit(); setTimeout('closePopup();',500)"" >")
        context.Response.Write("<form method=""post"" name=""ReturnFromDIBSForm"" action=""" & HTMLEncode(URL) & """ target=""" & HTMLEncode(Window) & """ >")
        Dim inputField, inputValue As String
        For Each inputField In context.Request.Form
            For Each inputValue In context.Request.Form(inputField)
                context.Response.Write("<input type=""hidden"" name=""" & HTMLEncode(inputField) & """ value=""" & HTMLEncode(inputValue) & """ />")
            Next
        Next
        context.Response.Write("</form>")
        context.Response.Write("</body>")
        context.Response.Write("</html>")
    End Sub
    
    ''' <summary>
    ''' Checks if string content is valid 
    ''' </summary>
    ''' <param name="mStr"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function filter(ByVal mStr As String) As Boolean
        For Each x As String In posList
            If mStr.ToUpper.Contains(x.ToUpper) Then
                Return True
            End If
        Next
    End Function

End Class