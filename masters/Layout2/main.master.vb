Imports System.Configuration.ConfigurationManager
Imports ExpandIT

Partial Class masters_Layout2_Main
    Inherits ExpandIT.MasterPage

    Protected Overloads Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        Dim uc As Control
        If AppSettings("LanguageDisplay").Equals("Flags") Then
            uc = Page.LoadControl("~/controls/LanguageFlags.ascx")
            Me.DivLanguagePlaceHolder.CssClass = "LanguagePlaceHolderFlags"
        Else
            uc = Page.LoadControl("~/controls/LanguageDropDown.ascx")
            Me.DivLanguagePlaceHolder.CssClass = "LanguagePlaceHolderDropDown"
        End If
        Me.languagePlaceHolder.Controls.Add(uc)
    End Sub
End Class

