Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

Partial Class masters_TwoColumn
    Inherits ExpandIT.MasterPage
    Protected onloadstr As String = ""
    Protected retv As ExpDictionary
    'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
    Protected csrObj As USCSR
    'AM2011031801 - ENTERPRISE CSR STAND ALONE - End

    Protected sPath As String = ""


    'AM2010080301 - CHECK OUT PROCESS MAP - Start
    Public ReadOnly Property CheckOutProcess()
        Get
            Return CheckOutProcessMap1
        End Get
    End Property

    'AM2010080301 - CHECK OUT PROCESS MAP - End

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '<!--JA2010060101 - ALERTS - Start-->
        sPath = Me.Page.ToString()
        If AppSettings("SHOW_ALERT_POP_UP") Then

            Dim SQL As String = ""

            If CBoolEx(globals.User("Anonymous")) Then
                SQL = "SELECT * FROM EEMCAlerts WHERE Disabled = 0 AND AlertType='SITEMESSAGE' AND (CustomerType = 0) AND (StartingDate <= getDate()) AND (EndingDate >= getDate() OR EndingDate IS NULL) ORDER BY CustomerType DESC"
            ElseIf CBoolEx(globals.User("B2B")) Then
                SQL = "SELECT * FROM EEMCAlerts WHERE Disabled = 0 AND AlertType='SITEMESSAGE' AND (CustomerType = 0 Or (CustomerType = 1 AND CustomerNo=" & SafeString(globals.User("CustomerGuid")) & ")) AND (StartingDate <= getDate()) AND (EndingDate >= getDate() OR EndingDate IS NULL) ORDER BY CustomerType DESC"
            ElseIf CBoolEx(globals.User("B2C")) Then
                SQL = "SELECT * FROM EEMCAlerts WHERE Disabled = 0 AND AlertType='SITEMESSAGE' AND (CustomerType = 0 Or CustomerType = 2) AND (StartingDate <= getDate()) AND (EndingDate >= getDate() OR EndingDate IS NULL) ORDER BY CustomerType DESC"
            End If

            retv = SQL2Dicts(SQL, "AlertNo")

            If retv.Count = 0 Then
                Dim valuesDict As New ExpDictionary
                valuesDict.Add("AlertNo", "ALTEST")
                valuesDict.Add("CustomerType", "1")
                valuesDict.Add("CustomerNo", "00000")
                valuesDict.Add("CustomerName", "Customer Name")
                valuesDict.Add("User", "")
                valuesDict.Add("UserName", "")
                valuesDict.Add("Message", "")
                valuesDict.Add("Disabled", "0")
                valuesDict.Add("NoSeries", "EEMC-ALERT")
                valuesDict.Add("StartingDate", Date.Now)
                valuesDict.Add("EndingDate", DBNull.Value)
                valuesDict.Add("AlertType", "")
                retv.Add("ALTEST", valuesDict)
            End If

            Dim showAlert As Boolean = False
            If Not retv Is Nothing AndAlso Not retv Is DBNull.Value AndAlso retv.Count > 0 Then
                Me.Alerts2.AlertsDict = retv
                If CBoolEx(globals.User("B2B")) Or CBoolEx(globals.User("B2C")) Then
                    For Each item As ExpDictionary In retv.Values
                        If item("CustomerType") = "1" Or item("CustomerType") = "2" Then
                            showAlert = True
                        End If
                    Next
                End If
                If sPath.ToUpper.Contains("SHOP") Then
                ElseIf sPath.ToUpper.Contains("ALERTSPAGE") Then
                    Me.Alerts2.ShowShop = True
                    Me.Alerts2.Visible = True
                End If
            End If
        End If

        If Not retv("ALTEST") Is Nothing AndAlso retv("ALTEST")("AlertNo") = "ALTEST" Then
            Me.Alerts2.ShowShop = False
            Session("LoadFirstTime") = "True"
        End If

        '<!--JA2010060101 - ALERTS - End-->

        'AM2010092201 - ENTERPRISE CSR - Start
        If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
            csrObj = New USCSR(globals)
            If csrObj.getSalesPersonGuid() <> "" Then
                pnlCSRCurrentUser.Visible = True
            End If
        End If
        'AM2010092201 - ENTERPRISE CSR - End

        'JA2010030901 - PERFORMANCE -Start
        If Not globals.User("Anonymous") And CBoolEx(AppSettings("SHOW_BEST_SELLERS")) Then
            Dim myControl As Control = Me.LoadControl("~/controls/boxes/BestSellersBox.ascx")
            myControl.ID = "BestSellersBox1"
            eis.SetControlProperties(myControl, "SetMethod", "GetMyMostPopularItems")
            Me.phMyMostPopularItems.Controls.Clear()
            Me.phMyMostPopularItems.Controls.Add(myControl)
        End If
        'JA2010030901 - PERFORMANCE -End
    End Sub


End Class



