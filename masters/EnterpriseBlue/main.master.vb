Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

Partial Class masters_layout1_main
    Inherits ExpandIT.MasterPage


    '<!--JA2010060101 - ALERTS - Start-->
    Protected numberAlerts As Integer = 0
    Protected retv As ExpDictionary
    '<!--JA2010060101 - ALERTS - End-->

    Protected Overloads Sub Page_Load(ByVal o As Object, ByVal e As EventArgs) Handles Me.Load
        '<!--JA2010060101 - ALERTS - Start-->
        retv = eis.getAlerts()
        If Not retv Is Nothing AndAlso Not retv Is DBNull.Value AndAlso retv.Count > 0 Then
            numberAlerts = CIntEx(retv.Count)
        End If
        '<!--JA2010060101 - ALERTS - End-->
        
    End Sub

End Class

