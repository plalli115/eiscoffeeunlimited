Imports System.Configuration.ConfigurationManager
Imports ExpandIT.ExpandITLib

Partial Class include_cookietest
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim CheckCookie As String = ""
        Dim CheckCookiePer As String = ""
        Dim RetUrl As String
        Dim bCheckCookie, bCheckCookiePer As Boolean
        Dim ExpQueryString As String = ""
        Dim bExpQueryString As Boolean

        ' Check if a RetUrl is send along with the cookie check.
        ' If not RetUrl is present the the website redirects to the main root.
        If Request.QueryString("returl") <> "" Then
            RetUrl = Request.QueryString("returl")
        Else
            RetUrl = VRoot & "/"
        End If

        bExpQueryString = False
        If Request.QueryString("ExpQueryString") <> "" Then
            ExpQueryString = Request.QueryString("ExpQueryString")
            bExpQueryString = True
        End If

        ' Try to read cookie
        If Not Request.Cookies("ExpTest") Is Nothing Then CheckCookie = Request.Cookies("ExpTest")("ExpTestCookie")
        If Not Request.Cookies("ExpTestPer") Is Nothing Then CheckCookiePer = Request.Cookies("ExpTestPer")("ExpTestCookiePer")

        ' Checking session cookie.
        bCheckCookie = False
        If CheckCookie = "ThisCookieIsForTesting" Then
            bCheckCookie = True
        Else
            bCheckCookie = False
        End If

        ' Checking Persistent cookie.
        bCheckCookiePer = False
        If CheckCookiePer = "ThisCookieIsForTestingPersistent" Then
            bCheckCookiePer = True
        Else
            bCheckCookiePer = False
        End If

        If bCheckCookie Then
            ' The session cookie is ok, so we can proceed with the shop.
            If Not bCheckCookiePer Then
                ' The persistent cookie is not ok, we wite a parameter, which will prevent Expires from being written.
                Response.Cookies("store")("OnlyPersistentCookies") = "NotAllowed"
            End If

            If bExpQueryString Then
                Response.Redirect(RetUrl & "?" & ExpQueryString)
            Else
                Response.Redirect(RetUrl)
            End If
            Response.End()

        Else
            ' The session cookie is not ok.
            Response.Redirect(VRoot & "/include/cookietest_error.aspx?RetUrl=" & RetUrl)
            Response.End()
        End If
    End Sub
End Class
