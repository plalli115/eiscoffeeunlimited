<%@ Page Language="VB" AutoEventWireup="false" CodeFile="adminRatingsReviews.aspx.vb"
    Inherits="adminRatingsReviews" CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master"
    RuntimeMasterPageFile="ThreeColumn.master" %>

<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">

    <script src="<%=Vroot %>/script/calendar/calendar_us.js" language="JavaScript"></script>

    <link href="<%=Vroot %>/script/calendar/calendar.css" rel="stylesheet">
    <div class="AccountPage">
        <uc1:PageHeader ID="PageHeader1" runat="server" Text="SQL QUERIES" EnableTheming="true" />
        <div>
            <table cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td colspan="4" align="center" style="padding-bottom: 20px;">
                        <asp:Label ID="Label3" Style="font-size: 14px; font-weight: bold;" runat="server"
                            Text="<%$ Resources: Language, LABEL_ADMIN_REVIEWS_FILTER %>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" style="width: 100%;">
                            <tr>
                                <td align="center" colspan="2" style="padding-bottom: 10px;">
                                    <asp:Label ID="Label12" runat="server" Style="font-size: 14px; font-weight: bold;"
                                        Text="<%$ Resources: Language, LABEL_ADMIN_REVIEWS_GUID %>" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center" style="padding-bottom: 10px;">
                                    <asp:Button CssClass="AddButton" runat="server" ID="ShowByGuid" Text="<%$ Resources: Language, LABEL_ADMIN_REVIEWS_SHOW_BY_GUID %>" />
                                </td>
                            </tr>                            
                            <tr>
                                <td align="center" style="vertical-align: middle; padding-bottom: 30px;">
                                    <asp:TextBox ID="TbxShowByGuid" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table cellpadding="0" cellspacing="0" style="width: 100%;">
                            <tr>
                                <td colspan="2" align="center" style="padding-bottom: 10px;">
                                    <asp:Label ID="Label5" runat="server" Style="font-size: 14px; font-weight: bold;"
                                        Text="<%$ Resources: Language, LABEL_ADMIN_REVIEWS_DATE %>" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center" style="vertical-align: middle; padding-bottom:10px;">
                                    <asp:Button runat="server" ID="ShowByDate" Style="display: none;" OnClick="ShowByDate_Click" />
                                    <input id="ShowByDate2" class="AddButton" type="button" onclick="javascript:checkStartDate();"
                                        value="<%= Resources.Language.LABEL_ADMIN_REVIEWS_SHOW_DATE %>" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="padding-bottom: 30px;">
                                    <table cellpadding="0" cellspacing="0" style="width: 100%;">
                                        <tr>
                                            <td>
                                                <input id="vroot" type="hidden" value="<%=Vroot %>" />
                                                <table cellpadding="0" cellspacing="0" style="width: 100%;">
                                                    <tr>
                                                        <td align="center" style="padding-bottom: 10px;">
                                                            <asp:Label ID="Label6" runat="server" Text="<%$ Resources: Language, LABEL_ADMIN_REVIEWS_START_DATE %>" />
                                                            <input id="StartDate" style="width: 70px;" name="StartDate" value="<%= Request("StartDate") %>"
                                                                type="text" />
                                                            <img alt="Open Calendar" class="tcalIcon" onclick="A_TCALS['0'].f_toggle()" id="tcalico_0"
                                                                src="<%=Vroot %>/script/calendar/img/cal.gif" title="Open Calendar" />

                                                            <script type="text/javascript" language="javascript">
                                                                        // whole calendar template can be redefined per individual calendar
                                                                        
                                                                        var path=document.getElementById('vroot').value
                                                                         var A_CALTPL = {
                                                                         'months' : ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                                                                         'weekdays' : ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                                                                         'yearscroll': true,
                                                                         'weekstart': 0,
                                                                         'centyear' : 70,
                                                                         'imgpath' : path + '/script/calendar/img/'
                                                                         }
                                                                                                                       
                                                                        new tcal ({
                                                                         // if referenced by ID then form name is not required
                                                                         'controlname': 'StartDate'
                                                                         }, A_CALTPL);  
                                                            </script>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <asp:Label ID="Label10" runat="server" Style="padding-right: 7px;" Text="<%$ Resources: Language, LABEL_ADMIN_REVIEWS_END_DATE %>" />
                                                            <input id="EndDate" style="width: 70px;" name="EndDate" value="<%= Request("EndDate") %>"
                                                                type="text" />
                                                            <img alt="Open Calendar" class="tcalIcon" onclick="A_TCALS['1'].f_toggle()" id="tcalico_1"
                                                                src="<%=Vroot %>/script/calendar/img/cal.gif">

                                                            <script language="javascript">
                                                                    // whole calendar template can be redefined per individual calendar
                                                                     var path=document.getElementById('vroot').value
                                                                     var A_CALTPL = {
                                                                     'months' : ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                                                                     'weekdays' : ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                                                                     'yearscroll': true,
                                                                     'weekstart': 0,
                                                                     'centyear' : 70,
                                                                     'imgpath' : path + '/script/calendar/img/'
                                                                     }
                                                                    
                                                                     new tcal ({
                                                                     // if referenced by ID then form name is not required
                                                                     'controlname': 'EndDate'
                                                                     }, A_CALTPL); 
                                                            </script>

                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table cellpadding="0" cellspacing="0" style="width: 100%;">
                            <tr>
                                <td align="center" colspan="2" style="padding-bottom: 10px;">
                                    <asp:Label ID="Label11" runat="server" Style="font-size: 14px; font-weight: bold;"
                                        Text="<%$ Resources: Language, LABEL_ADMIN_REVIEWS_USER %>" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center" style="padding-bottom: 10px;">
                                    <asp:Button CssClass="AddButton" runat="server" ID="ShowByUserId" Text="<%$ Resources: Language, LABEL_ADMIN_REVIEWS_SHOW_USER %>" />
                                </td>
                            </tr>                            
                            <tr>
                                <td align="center" style="vertical-align: middle; padding-bottom: 30px;">
                                    <asp:TextBox ID="UserId" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table cellpadding="0" cellspacing="0" style="width: 100%;">
                            <tr>
                                <td colspan="2" align="center" style="padding-bottom: 10px;">
                                    <asp:Label ID="Label4" runat="server" Style="font-size: 14px; font-weight: bold;"
                                        Text="<%$ Resources: Language, LABEL_ADMIN_REVIEWS_SHOW_ALL %>" />:
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button CssClass="AddButton" runat="server" ID="ShowAll" Text="<%$ Resources: Language, LABEL_ADMIN_REVIEWS_SHOW_ALL %>" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:Label ID="enterStartDate" runat="server" Style="color: Red; font-weight: bold;
                            display: none;" Text="<%$ Resources: Language, LABEL_ADMIN_REVIEWS_NO_DATE %>" />
                        <asp:Label ID="noRecords" runat="server" Style="color: Red; font-weight: bold; display: none;"
                            Text="<%$ Resources: Language, LABEL_ADMIN_REVIEWS_NO_RECORDS_DATE %>" />
                        <asp:Label ID="InvalidGuid" Visible="false" runat="server" Style="color: Red; font-weight: bold;"
                            Text="<%$ Resources: Language, LABEL_ADMIN_REVIEWS_NO_GUID %>" />
                        <asp:Label ID="deleteMsg" runat="server" Style="color: Green; font-weight: bold;
                            display: none;" Text="<%$ Resources: Language, LABEL_ADMIN_REVIEWS_DELETE %>" />
                        <asp:Label ID="invalidLoginUser" runat="server" Style="color: Red; display:none;
                            font-weight: bold;" Text="<%$ Resources: Language, LABEL_ADMIN_REVIEWS_INVALID_USER %>" />
                    </td>
                </tr>
            </table>
            <br />
            <br />
            <br />
            <%--<asp:ScriptManager ID="ScriptManager1" runat="server" />--%>
            <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="Server">
                <ContentTemplate>
                    <div style="width: 100%; height: 400px; overflow: auto;">
                        <table cellpadding="0" cellspacing="0" style="width: 97%; border-bottom: 2px solid gray;
                            border-left: 2px solid gray; border-right: 2px solid gray;">
                            <%Dim lastGuid As String = ""%>
                            <%
                                If Not showDict Is Nothing And Not showDict Is DBNull.Value Then%>
                            <%If showDict.Count > 0 Then%>
                            <%
                                For Each item As ExpDictionary In showDict.Values%>
                            <tr>
                                <%If lastGuid = CStrEx(item("ProductGuid")) Then%>
                                <td style="padding: 5px;">
                                    <%Else%>
                                    <td style="padding: 5px; border-top: 2px solid gray;">
                                        <%End If%>
                                        <table cellpadding="0" cellspacing="0" style="width: 100%;">
                                            <%If lastGuid <> CStrEx(item("ProductGuid")) Then%>
                                            <tr>
                                                <td style="padding-bottom: 5px;">
                                                    <asp:Label ID="Label7" runat="server" Style="font-weight: bold;" Text="Product Guid"></asp:Label>:
                                                </td>
                                                <td style="padding-bottom: 5px;">
                                                    <%=item("ProductGuid")%>
                                                </td>
                                            </tr>
                                            <%End If%>
                                            <tr>
                                                <td style="padding-bottom: 5px; padding-left: 20px; vertical-align: middle;">
                                                    <asp:Label ID="Label9" runat="server" Style="font-weight: bold;" Text="Rating"></asp:Label>:
                                                </td>
                                                <td style="padding-bottom: 5px; padding-left: 20px;">
                                                    <%If CStrEx(item("Rating")) = "1" Then%>
                                                    <img alt="" src="<%= Vroot %>/script/stars/1.PNG" />
                                                    <%ElseIf CStrEx(item("Rating")) = "2" Then%>
                                                    <img alt="" src="<%= Vroot %>/script/stars/2.PNG" />
                                                    <%ElseIf CStrEx(item("Rating")) = "3" Then%>
                                                    <img alt="" src="<%= Vroot %>/script/stars/3.PNG" />
                                                    <%ElseIf CStrEx(item("Rating")) = "4" Then%>
                                                    <img alt="" src="<%= Vroot %>/script/stars/4.PNG" />
                                                    <%ElseIf CStrEx(item("Rating")) = "5" Then%>
                                                    <img alt="" src="<%= Vroot %>/script/stars/5.PNG" />
                                                    <%End If%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-bottom: 5px; padding-left: 20px;">
                                                    <asp:Label ID="Label8" runat="server" Style="font-weight: bold;" Text="Date"></asp:Label>:
                                                </td>
                                                <td style="padding-bottom: 5px; padding-left: 20px;">
                                                    <%=item("CreatedOne")%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-bottom: 5px; padding-left: 20px;">
                                                    <asp:Label ID="Label1" runat="server" Style="font-weight: bold;" Text="Review Title"></asp:Label>:
                                                </td>
                                                <td style="padding-bottom: 5px; padding-left: 20px;">
                                                    <%=item("ReviewTitle")%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-bottom: 5px; padding-left: 20px; width: 112px;">
                                                    <asp:Label ID="Label2" runat="server" Style="font-weight: bold;" Text="Review Comment"></asp:Label>:
                                                </td>
                                                <td style="width: 460px; padding-bottom: 5px; padding-left: 20px;">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%;">
                                                        <tr>
                                                            <td style="padding: 5px; border: 1px solid gray;">
                                                                <%=item("Review")%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 5px;">
                                                                <table cellpadding="0" cellspacing="0" style="width: 100%;">
                                                                    <tr>
                                                                        <td align="center" style="width: 50%;">
                                                                            <a href="adminRatingsReviews.aspx?delete=1&amp;RatingGuid=<%= item("RatingGuid") %>">
                                                                                <img style="border: 0px;" alt="" src="<%= Vroot %>/images/p.gif" class="CartLineField_DeleteImage" />
                                                                            </a>
                                                                        </td>
                                                                        <td>
                                                                            <a href="adminRatingsReviews.aspx?find=1&amp;UserGuid=<%= item("UserGuid") %>">
                                                                                <img style="border: 0px;" alt="" src="<%= Vroot %>/images/search.png" />
                                                                            </a>
                                                                        </td>
                                                                        <td align="center" style="width: 50%;">
                                                                            <a href="<%= getMailto(item("UserGuid")) %>">Send Email </a>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <%lastGuid = CStrEx(item("ProductGuid"))%>
                                    </td>
                            </tr>
                            <%Next%>
                            <%End If%>
                            <%End If%>
                        </table>
                    </div>
                    <triggers>
        	            <asp:AsyncPostBackTrigger ControlID="ShowAll" />
        	            <asp:AsyncPostBackTrigger ControlID="ShowByUserId" />
        	            <asp:AsyncPostBackTrigger ControlID="ShowByGuid" />
        	            <asp:AsyncPostBackTrigger ControlID="ShowByDate" />
    	            </triggers>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <script type="text/javascript">
        function showDeleteMsg(){
            //document.getElementById('ctl00_ctl00_cphRoot_cphSubMaster_ShowAll').click();
            document.getElementById('ctl00_ctl00_cphRoot_cphSubMaster_deleteMsg').style.display='Block';
        }
        
        function hideDeleteMsg(){
            document.getElementById('ctl00_ctl00_cphRoot_cphSubMaster_deleteMsg').style.display='none';
        }
        function hideNoRecordsFound(){
            document.getElementById('ctl00_ctl00_cphRoot_cphSubMaster_noRecords').style.display='none';
        }
        
        function checkStartDate(){
            var start=document.getElementById('StartDate').value;
            if(start == ''){
                document.getElementById('ctl00_ctl00_cphRoot_cphSubMaster_enterStartDate').style.display='Block';
            }else{
                document.getElementById('ctl00_ctl00_cphRoot_cphSubMaster_enterStartDate').style.display='none';
                //alert('zzzzzz');
                document.getElementById('ctl00_ctl00_cphRoot_cphSubMaster_ShowByDate').click();
            }    
        }
        function clickShowByUser(){
            document.getElementById('ctl00_ctl00_cphRoot_cphSubMaster_ShowByUserId').click();
        }
        function clickShowByDate(start,end){
            document.getElementById('StartDate').value=start;
            document.getElementById('EndDate').value=end;
            document.getElementById('ctl00_ctl00_cphRoot_cphSubMaster_ShowByDate').click();
        }
        function clickShowByProductGuid(){
            document.getElementById('ctl00_ctl00_cphRoot_cphSubMaster_ShowByGuid').click();
        }
        function clickShowAll(){
            document.getElementById('ctl00_ctl00_cphRoot_cphSubMaster_ShowAll').click();
        }

        function hideNoRecordsDate(){
            document.getElementById('ctl00_ctl00_cphRoot_cphSubMaster_noRecords').style.display='none';
        }
        function showNoRecordsDate(){
            document.getElementById('ctl00_ctl00_cphRoot_cphSubMaster_noRecords').style.display='block';
        }
        
        function hideInvalidUser(){
            document.getElementById('ctl00_ctl00_cphRoot_cphSubMaster_invalidLoginUser').style.display='none';
        }
        function showInvalidUser(){
            document.getElementById('ctl00_ctl00_cphRoot_cphSubMaster_invalidLoginUser').style.display='block';
        }
    
    </script>

</asp:Content>
