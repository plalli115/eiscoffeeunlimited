<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShippingProvider.aspx.vb"
    Inherits="ShippingProvider" CodeFileBaseClass="ExpandIT.ShippingPayment.ShippingPaymentPage"
    MasterPageFile="~/masters/default/main.master" RuntimeMasterPageFile="ThreeColumn.master"
    CrumbName="<%$ Resources: Language, LABEL_SHIPPING_PROVIDER_SELECTION %>" %>
    
<%--AM2010080301 - CHECK OUT PROCESS MAP - Start--%>
<%@ MasterType VirtualPath="~/masters/EnterpriseBlue/ThreeColumn.master" %>
<%--AM2010080301 - CHECK OUT PROCESS MAP - End--%>

<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Register TagPrefix="exp" TagName="shippingprovider" Src="~/controls/shipping/parts/ShippingProviderSelection.ascx" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <div class="ShippingProviderPage">
        <uc1:PageHeader ID="PageHeader1" runat="server" Text="<%$ Resources: Language, LABEL_SHIPPING_PROVIDER_SELECTION %>"
            EnableTheming="true" />
        <asp:Panel runat="server" DefaultButton="Purchase" ID="purchasepanel">
            <exp:shippingprovider ID="shipprovider" runat="server" />
            <div class="ShippingProviderButton">
                <asp:Button ID="Purchase" runat="server" CssClass="AddButton" ValidationGroup="purchasegroup"
                    OnClick="Purchase_Click" />
            </div>
        </asp:Panel>
    </div>
</asp:Content>
