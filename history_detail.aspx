<%@ Import Namespace="System.Configuration.ConfigurationManager" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="history_detail.aspx.vb"
    Inherits="history_detail" CodeFileBaseClass="ExpandIT.ShippingPayment.ShippingPaymentPage"
    MasterPageFile="~/masters/default/main.master" RuntimeMasterPageFile="ThreeColumn.master"
    CrumbName="<%$ Resources: Language, LABEL_HISTORY_INFORMATION %>" %>
<%--CodeFileBaseClass="ExpandIT.ShippingPayment.ShippingMethodPage"--%>

<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Import Namespace="ExpandIT.GlobalsClass" %>
<%--AM2010031501 - ORDER RENDER - Start--%>
<%@ Register Src="~/controls/USOrderRender/CartRender.ascx" TagName="CartRender"
    TagPrefix="uc1" %>
<%--AM2010031501 - ORDER RENDER - End--%>
<%--AM2010091001 - ONLINE PAYMENTS - Start--%>
<%@ Register Namespace="ExpandIT.SimpleUIControls" TagPrefix="expui" %>
<%--AM2010091001 - ONLINE PAYMENTS - End--%>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<%--JA2010092201 - RECURRING ORDERS - START--%>
<%@ Register Src="~/controls/USRecurringOrders/RecurringSchedule.ascx" TagName="RecurringSchedule"
    TagPrefix="uc1" %>
<%--JA2010092201 - RECURRING ORDERS - END--%>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <div class="HistoryDetailPage">
        <uc1:PageHeader ID="PageHeader1" runat="server" Text="<%$ Resources: Language, LABEL_HISTORY_INFORMATION %>"
            EnableTheming="true" />
        <!-- Here begins the HTML code for the page -->
        <asp:Panel ID="panelOrderNotAvailable" runat="server">
            <p>
                <% =Resources.Language.LABEL_NO_HISTORY_AVAILABLE & "."%>
            </p>
        </asp:Panel>
        <asp:Label ID="Message" runat="server" Style="color: #208040;" Visible="false" Text=""></asp:Label>
        <asp:Label ID="lblError" runat="server" Style="color: Red;" Visible="false" Text=""></asp:Label>
        <br />
        <%If CStrEx(Request("CancelationTrack")) <> "" Then%>
        <%Dim line%>
        <table>
            <tr>
                <td style="font-weight: bold; border-bottom: 1px dotted #999999; width: 442px;">
                    <% =Resources.Language.CANCELATION_INFORMATION%>
                </td>
            </tr>
            <tr>
                <td style="height: 3px;">
                </td>
            </tr>
        </table>
        <%Dim retval As ExpDictionary = SQL2Dicts("SELECT * FROM CancelledOrders WHERE CancelRecurrency=0 AND HeaderGuidOriginal=" & SafeString(Request("HeaderGuid")))%>
        <%If Not retval Is Nothing Then%>
        <table>
            <%For Each line In retval.Values%>
            <tr>
                <td style="font-weight: bold;">
                    <% =Resources.Language.LABEL_CANCELLATION_REFERENCE & ": "%>
                </td>
                <td>
                    <%=line("CustomerReference")%>
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold;">
                    <% =Resources.Language.CANCELATION_DATE & ": "%>
                </td>
                <td>
                    <%= formatdate(line("CancelationDate")) %>
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold;">
                    <% =Resources.Language.CANCELATION_REASON & ": "%>
                </td>
                <td>
                    <%= line("CancelationReason") %>
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold;">
                    <% =Resources.Language.CANCELATION_SALES_PERSON & ": "%>
                </td>
                <td>
                    <%=line("SalesPersonGuid")%>
                </td>
            </tr>
            <%Next%>
        </table>
        <br />
        <%End If%>
        <%End If%>
        <asp:Panel ID="panelOrderAvailable" runat="server">
            <%--AM2010092201 - ENTERPRISE CSR - Start--%>
            <%If CStrEx(Request("justReturn")) <> "" And CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then%>
            <h2>
                <a href="javascript: history.go(-1)"><%= Resources.Language.LABEL_CSR_BACK_ORIGINAL_ORDER %></a>
            </h2>
            <%End If%>
            
            <h2>
                <% =Resources.Language.LABEL_ORDER_STATUS%>
                <% =
                    csrObj.getOrderStatusFromHeader(OrderDict("HeaderGuid"))%>
            </h2>
            
            <%
                If CStrEx(csrObj.getOrderStatusFromHeader(OrderDict("HeaderGuid"))) = Resources.Language.LABEL_CSR_STATUS_SHIPPED Then%>
                <%Dim dictTracking As ExpDictionary = csrObj.getTrackingNo(OrderDict("HeaderGuid"))%>
                <%Dim headerDate As String = ""%>
                <%If dictTracking.Count > 0 Then%>
                    <h2>
                        <%=Resources.Language.LABEL_ORDER_TRACKING_NO%><br />
                    </h2>
                    <table>
                    <%For Each item As ExpDictionary In dictTracking.Values%>
                        <tr>
                        <%If CStrEx(item("HeaderDate")) <> headerDate Then%>                            
                            <td>
                            <%=CDateEx(item("HeaderDate")).ToString("MM/dd/yyyy")%>
                            <%headerDate = CStrEx(item("HeaderDate"))%>
                            </td> 
                            <td style="width:20px;"></td> 
                            <td>
                            <%=getTrakingLink(item("PackageTrackingNo"))%>
                            </td>                               
                        <%Else %>
                            <td>
                            </td>
                            <td style="width:20px;">
                            </td>
                            <td>
                            <%=getTrakingLink(item("PackageTrackingNo"))%>
                            </td>
                        <%End If %>
                        </tr> 
                    <%Next%>                     
                    </table>                  
                <%End If %>
            <%End If %>
            
            <%If CStrEx(csrObj.getOrderStatusFromHeader(OrderDict("HeaderGuid"))) = CStrEx(Resources.Language.LABEL_CSR_STATUS_RETURNED) And CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then%>
            <h2>
                <%=CStrEx(getReturnedOrder())%>
            </h2>
            <%End If%>
            <%--AM2010092201 - ENTERPRISE CSR - End--%>
            <h2>
                <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start--%>
                <% = getDocumentTypeTittle()%>
                <% = getDocumentReference()%>
                <%--AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End--%>
            </h2>
            <%--JA2010092201 - RECURRING ORDERS - START--%>
            <%If CStrEx(IsRecurrencyCancelled(OrderDict("HeaderGuid"))) <> "" And CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then%>
            <h2>
                <% =CStrEx(IsRecurrencyCancelled(OrderDict("HeaderGuid")))%>
            </h2>
            <%End If%>
            <%--JA2010092201 - RECURRING ORDERS - END--%>
            <!-- AM2010031501 - ORDER RENDER - Start -->
            <uc1:CartRender ID="CartRender1" runat="server" IsActiveOrder="false" EditMode="false"
                IsEmail="false" ShowDate="true" ShowPaymentInfo="true" />
            <%--<% =OrderRender.Render(OrderDict, eis, AppSettings("SHOW_TAX_TYPE") = "INCL", True, True, False, True, True, IsCommentShown, IsPoNumberShown, True, True, CBoolEx(AppSettings("SHIPPING_HANDLING_ENABLED")), False, CustomerShippingProviderName, False)%>--%>
            <!-- AM2010031501 - ORDER RENDER - End -->
            <input type="hidden" name="HeaderComment" value="<% = HTMLEncode(OrderDict("HeaderComment")) %>" />
            <input type="hidden" name="CustomerPONumber" value="<% = HTMLEncode(OrderDict("CustomerPONumber")) %>" />
            <%													    
                Dim OrderLine As Object
                For Each OrderLine In OrderDict("Lines").Values
            %>
            <input type="hidden" name="SKU" value="<% Response.Write(HTMLEncode(OrderLine("ProductGuid"))) %>" />
            <input type="hidden" name="VariantCode" value="<% Response.Write(HTMLEncode(OrderLine("VariantCode"))) %>" />
            <input type="hidden" name="Quantity" value="<% Response.Write(OrderLine("Quantity")) %>" />
            <input type="hidden" name="LineComment" value="<% Response.Write(HTMLEncode(OrderLine("LineComment"))) %>" />
            <%--'AM0122201001 - UOM - Start--%>
            <% If AppSettings("EXPANDIT_US_USE_UOM") Then%>
            <input type="hidden" name="UOM" value="<% Response.Write(HTMLEncode(OrderLine("UOM"))) %>" />
            <% End If%>
            <%--'AM0122201001 - UOM - End--%>
            <%--JA2010092201 - RECURRING ORDERS - START--%>
            <%
                If CBoolEx(globals.OrderDict("RecurringOrder")) = True Then%>
            <input type="hidden" name="RNumber" value="<% Response.Write(HTMLEncode(OrderLine("RNumber"))) %>" />
            <input type="hidden" name="RPeriod" value="<% Response.Write(HTMLEncode(OrderLine("RPeriod"))) %>" />
            <input type="hidden" name="RQuantity" value="<% Response.Write(HTMLEncode(OrderLine("RQuantity"))) %>" />
            <input type="hidden" name="RFinalDate" value="<% Response.Write(HTMLEncode(OrderLine("RFinalDate"))) %>" />
            <%End If%>
            <%--JA2010092201 - RECURRING ORDERS - END--%>
            <%
            Next
            %>
            <%--JA2010092201 - RECURRING ORDERS - START--%>
            <%--<uc1:RecurringSchedule ID="RecurringSchedule1" runat="server" IsEditable="false" />--%>
            <input type="hidden" name="HeaderGuid" value="<% Response.Write(HTMLEncode(OrderDict("HeaderGuid"))) %>" />
            <%--JA2010092201 - RECURRING ORDERS - END--%>
            <%If CStrEx(Request("CancelationTrack")) = "" Then%>
            <%--JA2010092201 - RECURRING ORDERS - START--%>
            <%If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) And csrObj.getSalesPersonGuid() <> "" Then%>
            <p>
                <asp:Button ID="btnCancelRecurrency" CssClass="AddButton" runat="server" Text="<%$ Resources: Language, ACTION_CANCEL_RECURRENCY %>">
                </asp:Button>
                <asp:Button ID="btnModifyRecurrency" CssClass="AddButton" runat="server" Text="<%$ Resources: Language, ACTION_MODIFY_RECURRENCY %>">
                </asp:Button>
            </p>
            <%End If %>
            <%--JA2010092201 - RECURRING ORDERS - START--%>
            <p>
                <asp:Button ID="btnReorder" CssClass="AddButton" runat="server" Text="<%$ Resources: Language, ACTION_REORDER %>"
                    PostBackUrl="cart.aspx?cartaction=add"></asp:Button>
                <%--AM2010080201 - ORDER PRINT OUT - Start--%>
                <asp:Button ID="btnPrint" CssClass="AddButton" runat="server" Text="<%$ Resources: Language, PRINT_ORDER %>">
                </asp:Button>
                <%--AM2010080201 - ORDER PRINT OUT - End--%>
                <%--AM2010091001 - ONLINE PAYMENTS - Start--%>
                <asp:Button ID="btnOnlinePayment" CssClass="AddButton" Visible="false" runat="server"
                    Text="<%$ Resources: Language, ONLINE_PAYMENT_BUTTON %>"></asp:Button>
                <%--AM2010091001 - ONLINE PAYMENTS - End--%>
                <asp:Button ID="btnCancel" runat="server" CssClass="AddButton" Text="<%$ Resources: Language, ACTION_CANCEL_ORDER %>" />
                <asp:Button ID="btnReturn" CssClass="AddButton" runat="server" Text="<%$ Resources: Language, LABEL_RETURN_LINK %>">
                </asp:Button>
                <!--JA2010102801 - PAPERCHECK - Start-->
                <asp:Button ID="btnAddPayperCheck" style="margin-top:10px;" runat="server" Visible="false" CssClass="AddButton" Text="<%$ Resources: Language, LABEL_PAPERCHECK_ADD %>" />
                <!--JA2010102801 - PAPERCHECK - End-->
            </p>
            <%End If%>
            <br />
            <table>
                <tr>
                    <td style="vertical-align:middle;">
                        <asp:Label ID="lblCancelReason" runat="server" Visible="false" Text="<%$ Resources: Language, LABEL_CANCELATION_REASON %>"></asp:Label>
                        <asp:Label ID="lblModifyCancelReason" runat="server" Visible="false" Text="<%$ Resources: Language, LABEL_MODIFICATION_REASON %>"></asp:Label>
                    </td>
                    <td style="vertical-align:middle;">
                        <asp:TextBox Width="200px" ID="txbCancelReason" Visible="false" runat="server"></asp:TextBox>
                    </td>
                    <td style="vertical-align:middle;">
                        <asp:Button ID="SubmitCancelation" CssClass="AddButton" Visible="false" runat="server"
                            Text="<%$ Resources: Language, ACTION_SUBMIT %>" />
                        <asp:Button ID="SubmitCancelationRecurrency" CssClass="AddButton" Visible="false"
                            runat="server" Text="<%$ Resources: Language, ACTION_SUBMIT %>" />
                        <asp:Button ID="SubmitCancelationForModification" CssClass="AddButton" Visible="false"
                            runat="server" Text="<%$ Resources: Language, ACTION_SUBMIT %>" />
                    </td>
                </tr>
            </table>
            <%--JA2010092201 - RECURRING ORDERS - END--%>
            <%--AM2010091001 - ONLINE PAYMENTS - Start--%>
            <div>
                <!-- AM2010041301 - ENTERPRISE PAYMENT GATEWAY - Start -->
                <% If AppSettings("EXPANDIT_US_USE_EEPG_ONLINE_PAYMENTS") Then%>
                <asp:UpdateProgress ID="udPanelProgress" DynamicLayout="true" runat="server" AssociatedUpdatePanelID="pnlOnlinePayments">
                    <ProgressTemplate>
                        <div class="ctrl_OnlinePayments_udPanel_pnlProgress">
                            <div class="ctrl_OnlinePayments_udPanel_divProgress">
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/progress.gif" /> Submitting Payment
                            </div>
                        </div>
                    </ProgressTemplate> 
                </asp:UpdateProgress>
                <asp:UpdatePanel ID="pnlOnlinePayments" UpdateMode="Conditional" runat="Server" Visible="false">
                    <ContentTemplate>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:Label ID="lblOnlinePaymentMessage" Visible="false" runat="server">
                                    </asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <expui:EnterprisePaymentGatewayAIMControl ID="eepgform" runat="server" TableWidth="600"
                                        ButtonText="ACTION_ACCEPT_2" HeaderText="LABEL_PAYMENT_INFORMATION" LabelText="ONLINE_PAYMENT_MESSAGE_CLICK_TO_SUBMIT_EEPG"
                                        TextPart2="ACTION_ACCEPT_2" isOnlinePayment="true" />
                                </td>
                            </tr>
                        </table>
                <% Dim htmlEEPG As String = objEEPG.getCreditCardList()%>
                <%If CStrEx(htmlEEPG) <> "" Then%>
                    <table border="0" cellpadding="0" cellspacing="0" style="width:100%;"> 
                        <tr>
                            <td>
                                <a onclick="javascript:animatedcollapse.toggle('ExistingCreditCards')" style="cursor: pointer;">
                                    <asp:Label ID="WriteReview" Text="<%$ Resources: Language, LABEL_EEPG_EXISTING_CC %>"
                                        runat="server" />
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a onclick="javascript:clearFrom();" style="cursor: pointer;">
                                    <asp:Label ID="lblClearForm" style="display:none;" Text="<%$ Resources: Language, LABEL_EEPG_CLEAR_FROM %>"
                                        runat="server" />
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div id="ExistingCreditCards" style="display: none; width: 100%; height: 300px; top: -325px;
                                    border: 1px solid gray; background-color: white; position: relative; padding-top: 5px;
                                    padding-bottom: 5px; overflow: auto;">
                                    <%= CStrEx(htmlEEPG)  %>
                                </div>
                            </td>
                        </tr>                      
                    </table>
                <% End If %>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <!-- JA2011042101 - EEPG USE EXISTING CREDIT CARDS - End -->


                <% End If%>
                <!-- AM2010041301 - ENTERPRISE PAYMENT GATEWAY - End -->
            </div>
            <%--AM2010091001 - ONLINE PAYMENTS - End--%>
        </asp:Panel>
    </div>
    <%--AM2010080201 - ORDER PRINT OUT - Start--%>
<p><a href="javascript:history.back()"><% =Resources.Language.LABEL_BACK%></a></p>

    <script type="text/javascript">
    function printOrder(link){
        window.open(link,'_blank');
    }
    </script>

    <%--AM2010080201 - ORDER PRINT OUT - End--%>


    <script type="text/javascript"> 
        animatedcollapse.addDiv('ExistingCreditCards', 'fade=1')
        animatedcollapse.ontoggle=function($, divobj, state){}
        animatedcollapse.init()
        
        function clearFrom(){
            document.getElementById('cphRoot_cphSubMaster_lblClearForm').style.display='none';
            document.getElementById('cphRoot_cphSubMaster_eepgform_FName').value='';
            document.getElementById('cphRoot_cphSubMaster_eepgform_FName').disabled=false;
            document.getElementById('cphRoot_cphSubMaster_eepgform_FNameValidator').style.visibility='hidden';
            
            document.getElementById('cphRoot_cphSubMaster_eepgform_LName').value='';
            document.getElementById('cphRoot_cphSubMaster_eepgform_LName').disabled=false;
            document.getElementById('cphRoot_cphSubMaster_eepgform_LNameValidator').style.visibility='hidden';
            
            document.getElementById('cphRoot_cphSubMaster_eepgform_CCN').value='';
            document.getElementById('cphRoot_cphSubMaster_eepgform_CCN').disabled=false;
            document.getElementById('cphRoot_cphSubMaster_eepgform_CCNValidator').style.visibility='hidden';
            
            document.getElementById('cphRoot_cphSubMaster_eepgform_ExpDate').value='';
            document.getElementById('cphRoot_cphSubMaster_eepgform_ExpDate').disabled=false;
            document.getElementById('cphRoot_cphSubMaster_eepgform_ExpDateValidator').style.visibility='hidden';
            
            document.getElementById('cphRoot_cphSubMaster_eepgform_CCV').value='';
            <%If CBoolEx(AppSettings("PAYMENT_EEPG_CVV_MANDATORY")) Then %>
                document.getElementById('cphRoot_cphSubMaster_eepgform_CCVDateValidator').style.visibility='hidden';
            <%End If %>
            document.getElementById('cphRoot_cphSubMaster_eepgform_decryptData').value='';
            
            //Address
            <%If CBoolEx(AppSettings("PAYMENT_EEPG_REQUIRE_ADDRESS_INFO")) Then %>
                document.getElementById('cphRoot_cphSubMaster_eepgform_cbxAddress').style.visibility='visible';
                document.getElementById('cphRoot_cphSubMaster_eepgform_cbxAddress').checked=false;
                document.getElementById('cphRoot_cphSubMaster_eepgform_lblCbxAddress').style.display='block';
                  
                document.getElementById('cphRoot_cphSubMaster_eepgform_Address1').value='';
                document.getElementById('cphRoot_cphSubMaster_eepgform_Address1Validator').style.visibility='hidden';          
                document.getElementById('cphRoot_cphSubMaster_eepgform_Address2').value='';
                document.getElementById('cphRoot_cphSubMaster_eepgform_City').value='';
                document.getElementById('cphRoot_cphSubMaster_eepgform_CityValidator').style.visibility='hidden';
                document.getElementById('cphRoot_cphSubMaster_eepgform_State').value='';
                document.getElementById('cphRoot_cphSubMaster_eepgform_StateValidator').style.visibility='hidden';
                document.getElementById('cphRoot_cphSubMaster_eepgform_Phone').value='';
                document.getElementById('cphRoot_cphSubMaster_eepgform_Email').value='';
                document.getElementById('cphRoot_cphSubMaster_eepgform_EmailValidator').style.visibility='hidden';
            <%End If %>
            
            <%If CBoolEx(AppSettings("PAYMENT_EEPG_REQUIRE_ZIP_CODE")) Then %>    
                document.getElementById('cphRoot_cphSubMaster_eepgform_ZipCode').value='';
                document.getElementById('cphRoot_cphSubMaster_eepgform_ZipCodeValidator').style.visibility='hidden';
            <%End If %>
            
            <%If CBoolEx(AppSettings("PAYMENT_EEPG_REQUIRE_ZIP_CODE")) AND NOT CBoolEx(AppSettings("PAYMENT_EEPG_REQUIRE_ADDRESS_INFO")) Then %>
                document.getElementById('cphRoot_cphSubMaster_eepgform_cbxZipCode').style.display='block';
                document.getElementById('cphRoot_cphSubMaster_eepgform_lblCbxZipCode').style.display='block';
            <%End If %>
            
        }

        function preventDoubleClick(btn) {
            btn.disabled = true;
            btn.className = "AddButton Processing";
        }
        
        function SetParametersEEPG(theArray){
            var splitResult=theArray.split('|');
            
            document.getElementById('cphRoot_cphSubMaster_eepgform_FName').value=splitResult[0];
            document.getElementById('cphRoot_cphSubMaster_eepgform_FName').disabled=true;
            document.getElementById('cphRoot_cphSubMaster_eepgform_FNameValidator').style.visibility='hidden';
    
            document.getElementById('cphRoot_cphSubMaster_eepgform_LName').value=splitResult[1];
            document.getElementById('cphRoot_cphSubMaster_eepgform_LName').disabled=true;
            document.getElementById('cphRoot_cphSubMaster_eepgform_LNameValidator').style.visibility='hidden';
            
            document.getElementById('cphRoot_cphSubMaster_eepgform_CCN').value=splitResult[2];
            document.getElementById('cphRoot_cphSubMaster_eepgform_CCN').disabled=true;
            document.getElementById('cphRoot_cphSubMaster_eepgform_CCNValidator').style.visibility='hidden';
            
            document.getElementById('cphRoot_cphSubMaster_eepgform_ExpDate').value='XXXX';
            document.getElementById('cphRoot_cphSubMaster_eepgform_ExpDate').disabled=true;
            document.getElementById('cphRoot_cphSubMaster_eepgform_ExpDateValidator').style.visibility='hidden';
            //document.getElementById('cphRoot_cphSubMaster_eepgform_CCV')
            <%If CBoolEx(AppSettings("PAYMENT_EEPG_CVV_MANDATORY")) Then %>
                document.getElementById('cphRoot_cphSubMaster_eepgform_CCVDateValidator').style.visibility='hidden';
            <%End If %>
            document.getElementById('cphRoot_cphSubMaster_eepgform_decryptData').value=splitResult[3];
            
            //Address
            <%If CBoolEx(AppSettings("PAYMENT_EEPG_REQUIRE_ADDRESS_INFO")) Then %>
                document.getElementById('cphRoot_cphSubMaster_eepgform_cbxAddress').style.visibility='hidden';
                document.getElementById('cphRoot_cphSubMaster_eepgform_lblCbxAddress').style.display='none';
                
                document.getElementById('cphRoot_cphSubMaster_eepgform_Address1').value=splitResult[4];
                document.getElementById('cphRoot_cphSubMaster_eepgform_Address1Validator').style.visibility='hidden';
                document.getElementById('cphRoot_cphSubMaster_eepgform_Address2').value=splitResult[5];
                document.getElementById('cphRoot_cphSubMaster_eepgform_City').value=splitResult[6];
                document.getElementById('cphRoot_cphSubMaster_eepgform_CityValidator').style.visibility='hidden';
                document.getElementById('cphRoot_cphSubMaster_eepgform_State').value=splitResult[7];
                document.getElementById('cphRoot_cphSubMaster_eepgform_StateValidator').style.visibility='hidden';
                document.getElementById('cphRoot_cphSubMaster_eepgform_Phone').value=splitResult[8];
                document.getElementById('cphRoot_cphSubMaster_eepgform_Email').value=splitResult[9];
                document.getElementById('cphRoot_cphSubMaster_eepgform_EmailValidator').style.visibility='hidden';
            <%End If %>
            
            <%If CBoolEx(AppSettings("PAYMENT_EEPG_REQUIRE_ZIP_CODE")) Then %>
                document.getElementById('cphRoot_cphSubMaster_eepgform_ZipCode').value=splitResult[10];
                document.getElementById('cphRoot_cphSubMaster_eepgform_ZipCodeValidator').style.visibility='hidden';
            <%End If %>
            
            <%If CBoolEx(AppSettings("PAYMENT_EEPG_REQUIRE_ZIP_CODE")) AND NOT CBoolEx(AppSettings("PAYMENT_EEPG_REQUIRE_ADDRESS_INFO")) Then %>
                document.getElementById('cphRoot_cphSubMaster_eepgform_cbxZipCode').style.display='none';
                document.getElementById('cphRoot_cphSubMaster_eepgform_lblCbxZipCode').style.display='none';
            <%End If %>
            
            
            document.getElementById('cphRoot_cphSubMaster_lblClearForm').style.display='block';
        }
        
        
    </script>


</asp:Content>
