Imports System.Configuration.ConfigurationManager
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT.GlobalsClass
Imports ExpandIT
Imports ExpandIT.B2BBaseClass
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports ExpandIT31.ExpanditFramework.Util
Imports ExpandIT31.ExpanditFramework.BLLBase.UserBase


Partial Class _user_login
    Inherits ExpandIT.Page

    Protected UserB2BExists As Boolean
    Protected UserExists, dictUser, FormLogin, FormPassword As Object
    Protected rs, sql As Object
    Private useConfirmedRegistration As Boolean = False

    Private Const SQL_STANDARD_STR As String = "SELECT TOP 1 * FROM UserTable WHERE LOWER(UserLogin)={0}"

    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	
	Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim dtUser As DataTable
        Dim dt As DataTable
        Dim dtB2B As DataTable
        Dim dictUserB2B As ExpDictionary = Nothing

        FormLogin = Trim(Context.Items("login"))
        FormPassword = Trim(Context.Items("password"))
        useConfirmedRegistration = Utilities.CBoolEx(AppSettings("USE_MAIL_CONFIRMED_REGISTRATION"))

        ' Make sure that password data and login data are correct
        If CStrEx(FormLogin) = "" Or (CStrEx(FormPassword) = "" And (AppSettings("PASSWORD_LEN") > 0)) Then DenyLogin()
        ' Load the user        
        Dim sqlTempl As String = SQL_STANDARD_STR
        If useConfirmedRegistration Then
          sqlTempl = sqlTempl & String.Format(" And Status IN ({0}, {1})", UserStates.Confirmed.ToString("d"), UserStates.Updated.ToString("d"))
        End If
        sql = String.Format(sqlTempl, SafeString(FormLogin))
        dt = SQL2DataTable(sql)
        If dt.Rows.Count > 0 Then
            dictUser = SetDictionary(dt.Rows(0))
            UserExists = True
        Else
            dictUser = Nothing
            UserExists = False
        End If

        ' Investigate whether B2BUser information is available.
        If B2BEnabled Then
            sql = "SELECT TOP 1 * FROM UserTableB2B WHERE EnableLogin<>0 AND ltrim(rtrim(LOWER(UserLogin)))=" & SafeString(FormLogin)
            dtB2B = SQL2DataTable(sql)
            If dtB2B.Rows.Count > 0 Then
                dictUserB2B = SetDictionary(dtB2B.Rows(0))
                UserB2BExists = True
            Else
                UserB2BExists = False
            End If

            If UserB2BExists Then
                ' Check that the corresponding customer exists in the CusotmerTable
                sql = "SELECT CustomerGuid FROM CustomerTable WHERE EnableLogin<>0 AND ltrim(rtrim(CustomerGuid))=" & SafeString(Trim(dictUserB2B("CustomerGuid")))
                If Not HasRecords(sql) Then
                    DenyLogin()
                End If
            End If

            ' Check if another user had the same login
            If UserB2BExists Then
                If UserExists Then
                    If dictUser("UserGuid") <> dictUserB2B("UserGuid") Then

                        sql = "SELECT UserGuid FROM UserTable WHERE LOWER(UserLogin)=" & SafeString(FormLogin)
                        dtUser = SQL2DataTable(sql)
                        If dtUser.Rows.Count > 0 Then
                            sql = "DELETE FROM UserTable WHERE UserGuid=" & SafeString(dtUser.Rows(0)("UserGuid"))
                            excecuteNonQueryDb(sql)
                        End If
                    End If
                End If
            End If
        Else
            UserB2BExists = False
        End If

        If UserB2BExists Then
            If UserExists Then
                If SafeString(dictUser("PasswordVersion")) <> SafeString(dictUserB2B("PasswordVersion")) Then
                    If Trim(dictUserB2B("UserPassword")) <> Trim(eis.Encrypt(FormPassword)) Then
                        DenyLogin()
                    Else
                        AllowLogin(dictUserB2B("UserGuid"))
                    End If
                Else
                    If Trim(dictUser("UserPassword")) <> Trim(eis.Encrypt(FormPassword)) Then
                        DenyLogin()
                    Else
                        AllowLogin(dictUser("UserGuid"))
                    End If
                End If
            Else
                If Trim(dictUserB2B("UserPassword")) <> Trim(eis.Encrypt(FormPassword)) Then
                    DenyLogin()
                Else
                    AllowLogin(dictUserB2B("UserGuid"))
                End If
            End If
        Else
            If Not UserExists Then
                DenyLogin()
            Else
                If Trim(dictUser("UserPassword")) <> Trim(eis.Encrypt(FormPassword)) Or CStrEx(dictUser("IsB2B")) = "1" Then
                    DenyLogin()
                Else
                    AllowLogin(dictUser("UserGuid"))
                End If
            End If
        End If

    End Sub


    ' If login is permittet then set the cookie and redirect to the shop            
    Sub AllowLogin(ByVal dictUserGuid As Object)
        cart.MergeCarts(Session("UserGuid"), dictUserGuid)
        'JA2010071601 - RATINGS AND REVIEWS - START
        Dim sql As String = "SELECT * FROM ProductRatings WHERE UserGuid =" & SafeString(Session("UserGuid"))
        Dim ProductGuidDict As ExpDictionary = SQL2Dicts(sql)
        Dim oldRatingGuid As String = ""
        If Not ProductGuidDict Is Nothing And Not ProductGuidDict Is DBNull.Value Then
            If ProductGuidDict.Count > 0 Then
                For Each item As ExpDictionary In ProductGuidDict.Values
                    If CStrEx(item("ProductGuid")) <> "" Then
                        sql = "SELECT RatingGuid FROM ProductRatings WHERE UserGuid =" & SafeString(dictUserGuid) & " AND ProductGuid=" & SafeString(item("ProductGuid"))
                        oldRatingGuid = getSingleValueDB(sql)
                        If CStrEx(oldRatingGuid) <> "" Then
                            sql = "DELETE FROM ProductRatings WHERE UserGuid =" & SafeString(dictUserGuid) & " AND RatingGuid=" & SafeString(oldRatingGuid) & " AND ProductGuid=" & SafeString(item("ProductGuid"))
                            excecuteNonQueryDb(sql)
                            sql = "UPDATE ProductRatings SET UserGuid=" & SafeString(dictUserGuid) & ", RatingGuid=" & SafeString(oldRatingGuid) & " WHERE UserGuid=" & SafeString(Session("UserGuid")) & " AND ProductGuid=" & SafeString(item("ProductGuid"))
                            excecuteNonQueryDb(sql)
                        Else
                            sql = "UPDATE ProductRatings SET UserGuid=" & SafeString(dictUserGuid) & " WHERE UserGuid=" & SafeString(Session("UserGuid")) & " AND ProductGuid=" & SafeString(item("ProductGuid"))
                            excecuteNonQueryDb(sql)
                        End If
                    End If
                Next
            End If
        End If
        'JA2010071601 - RATINGS AND REVIEWS - END
        Session("UserGuid") = dictUserGuid
        Response.Cookies("store")("UserGuid") = Session("UserGuid")

        'AM2011031801 - ENTERPRISE CSR STAND ALONE - End - Start
        Dim csrObj As New USCSR(globals)
        If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
            csrObj.setCSROnLogin()
        End If
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - End - End
        eis.SetCookieExpireDate()

        eis.SetUserLanguage(globals.User("LanguageGuid"), False)
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
        If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
            csrObj.deleteCSRActiveOrders()
        End If
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - End

        ' Update minicart info
        globals.OrderDict = eis.LoadOrderDictionary(globals.User)
        eis.CalculateOrder()
        eis.UpdateMiniCartInfo(globals.OrderDict)


        If Not String.IsNullOrEmpty(Request("bouncecheckout")) Then
            Response.Redirect("cart.aspx?bouncecheckout=""true""", True)
        End If
        If Not String.IsNullOrEmpty(Request("returl")) AndAlso Not Request("returl").Contains("user_logout.aspx") AndAlso Not Request("returl").Contains("user_no_login.aspx") Then
            Response.Redirect(Request("returl"))
        Else
            Response.Redirect("~/default.aspx", True)
        End If
    End Sub

    ' The login was not permittet then redirect to a not logged in page.
    Sub DenyLogin()
        Response.Redirect(VRoot & "/user_no_login.aspx")
    End Sub

End Class
