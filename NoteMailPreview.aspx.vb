﻿Imports ExpandIT
Imports ExpandIT.ExpandITLib

Partial Class NoteMailPreview
    Inherits ExpandIT.Page
    
  Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
  
  Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    ' Check the page access.
    eis.PageAccessRedirect("NoteMailPreview")
    Dim xmlContent As String = Session("XmlNoteContent") 
    Dim languageISO As String = CStrEx(HttpContext.Current.Session("LanguageGuid")).ToLower()
    Dim userName As String = CStrEx(globals.User("ContactName")) 
    Dim templName As String = Request("templ_name")      
    If templName <> "" then
      Session("NoteMailHtml") = NoteMailContentBuilder.Process(userName, templName, xmlContent, languageISO, Encoding.UTF8)  
    Else
    End If
  End Sub

End Class
