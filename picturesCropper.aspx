<!-- AM2010050401 - PICTURES CROPPER - Start -->

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="picturesCropper.aspx.vb"
    Inherits="picturesCropper" CodeFileBaseClass="ExpandIT.Page" %>

<%@ Register Assembly="Radactive.WebControls.ILoad" Namespace="Radactive.WebControls.ILoad"
    TagPrefix="RAWCIL" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <table style="padding-left: 20px; padding-top: 20px;">
            <tr>
                <td style="padding-bottom: 10px;" colspan="2">
                    <b>Please select an option below:</b>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:DropDownList ID="dplPicturesTypes" runat="server" DataSourceID="SQLdsPicturesTypes"
                        DataTextField="PictureTypeDescription" DataValueField="PictureTypeGuid" AutoPostBack="true">
                    </asp:DropDownList><asp:SqlDataSource ID="SQLdsPicturesTypes" runat="server" ConnectionString="<%$ ConnectionStrings:ExpandITConnectionString %>"
                        SelectCommand="SELECT PictureTypeGuid, PictureTypeDescription FROM ImageProcessor WHERE Crop=1">
                    </asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding-top:10px;padding-bottom:10px;">
                    <RAWCIL:ILoad ID="ILoad1" runat='server'></RAWCIL:ILoad></td>
            </tr>
            <asp:Panel ID="pnlNewName" runat="server" Visible="false">
                <tr>
                    <td colspan="2">
                        New picture name:
                        <asp:TextBox ID="tbxPictureName" runat="server"></asp:TextBox></td>
                </tr>
            </asp:Panel>
            <tr>
                <td align="center">
                    <asp:Button ID="btnAccept" runat="server" CssClass="AddButton" Text="Save Changes" /></td>
                <td align="center">
                    <asp:Button ID="btnCancel" runat="server" CssClass="AddButton" Text="Cancel Changes" /></td>
            </tr>
            <tr>
                <td colspan="2" >
                    <asp:Label ID="lblError" runat="server" Visible="false" ForeColor="red"></asp:Label></td>
            </tr>
        </table>
    </form>
</body>
</html>
<!-- AM2010050401 - PICTURES CROPPER - End -->
