Imports ExpandIT
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT.ShippingPayment
Imports System.Collections.Generic
Imports System.Configuration.ConfigurationManager

Partial Class paymentMethod
    Inherits ShippingPaymentPage
    'Inherits PaymentMethodPage

    Protected dictLine As ExpDictionary
    Protected OrderDictionary, Shipping, Payment As ExpDictionary
    Protected bUseSecondaryCurrency As Boolean
    Protected dictSHProviders, dictSHProvider As ExpDictionary
    Protected strAdditionalInformation As String
    Protected txtPaymentProcessError As String = ""

    ' Used on payment.asp
    Protected CartLine As ExpDictionary
    Protected ColSpanNumber As Integer
    Protected StrId As String
    Protected sqlString As String
    Protected ret, sequence As Object

    'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
    Protected csrObj As USCSR
    'AM2011031801 - ENTERPRISE CSR STAND ALONE - End

    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	
	Protected Overloads Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender

        ' Make sure this page expires.
        Dim pStr As String

        pStr = "private, no-cache, must-revalidate"
        Response.ExpiresAbsolute = New Date(1990 - 1 - 1)
        Response.AddHeader("pragma", "no-cache")
        Response.AddHeader("cache-control", pStr)

        ' Check the page access.
        eis.PageAccessRedirect("Order")
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
        csrObj = New USCSR(globals)
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - End

        If globals.User.Count < 1 Then
            If Request("returl") = "" Then
                Response.Redirect("user_login.aspx")
            Else
                Response.Redirect("user_login.aspx?returl=" & Server.UrlEncode(ToString(Request("returl"))))
            End If
        Else
            ' Get dictionary with ShippingAddress info
            bUseSecondaryCurrency = eis.UseSecondaryCurrency(globals.User)

            globals.OrderDict = eis.LoadOrderDictionary(globals.User)
            ' Always calculate the order on Payment page. This is the final step, and the order should be the final order here.
            globals.OrderDict("IsCalculated") = False
            globals.OrderDict("VersionGuid") = GenGuid()
            eis.CalculateOrder()

            ' Load information for products
            eis.CatDefaultLoadProducts(globals.OrderDict("Lines"), False, False, False, New String() {"ProductName"}, New String() {}, Nothing)

            globals.OrderDict("CustomerReference") = eis.GenCustRef()

            ' Read PaymentProcessStatus and clear the field.
            txtPaymentProcessError = CStrEx(globals.OrderDict("PaymentStatus"))
            globals.OrderDict("PaymentStatus") = ""

            ' Save the order dictionary
            eis.SaveOrderDictionary(globals.OrderDict)
            Dim csp As Object = CustomerShippingProvider

            If (CBoolEx(AppSettings("USE_SHIPPING_ADDRESS")) AndAlso IsDBNullOrNothing(csp) OrElse String.IsNullOrEmpty(csp)) OrElse (csp <> "NONE") Then
                Try
                    Shipping = CartShippingAddress()
                    Payment = CartPaymentAddress()
                    If Payment Is Nothing Then
                        ' Shipping and Payment Addresses are the same - make Payment point to Shipping
                        Payment = Shipping
                    End If
                    If Shipping Is Nothing Then Throw New Exception("No active order exists.")
                Catch ex As Exception
                    If ex.Message.Equals("No active order exists.") Then
                        Response.Redirect("cart.aspx")
                    End If
                End Try
            Else
                Shipping = globals.User
                Payment = globals.User
            End If
        End If


        ' read paymentfailed
        If CStrEx(Request("paymentfailed")) <> "" Then
            If txtPaymentProcessError = "" Then
                txtPaymentProcessError = Server.HtmlEncode(CStrEx(Request("paymentfailed")))
            Else
                txtPaymentProcessError = Server.HtmlEncode(CStrEx(Request("paymentfailed"))) & "</LI><LI>" & Server.HtmlEncode(txtPaymentProcessError)
            End If
        End If
        '*****shipping*****-start
        If Not CBoolEx(AppSettings("EXPANDIT_US_USE_SHIPPING")) Then
            ' Shipping and Handling information
        dictSHProviders = eis.GetShippingHandlingProviders(globals.OrderDict("ShippingHandlingProviderGuid"), False)
            If dictSHProviders.Count > 0 Then
                dictSHProvider = GetFirst(dictSHProviders).Value
            Else
                dictSHProvider = New ExpDictionary()
            End If
        End If
        '*****shipping*****-end

        ' Additional information required by danish laws
        strAdditionalInformation = Resources.Language.LABEL_PAYMENT_MORE_INFO
        strAdditionalInformation = Replace(strAdditionalInformation, "%LINK_START%", "<a href=""" & VRoot & "/info/info.aspx"">")
        strAdditionalInformation = Replace(strAdditionalInformation, "%LINK_END%", "</a>")

        ' Initialize the shifting system between TR0 and TR1 (the color shifting in the tables).
        eis.InitNextRowAB()

        ' ---------------------------------------------------------------
        ' DEBUG
        ' ---------------------------------------------------------------
        If Globals.DebugLogic Then
            DebugValue(Globals.User, "User")
            DebugValue(Shipping, "Shipping")
            DebugValue(Payment, "Payment")
            DebugValue(Globals.OrderDict, "OrderDict")
        End If

    End Sub

    Private Sub findCtrl(ByVal parent As Control, ByVal child As Control, ByVal ctrlName As Type, ByVal cDict As Dictionary(Of String, Control))
        For Each child In parent.Controls
            If child.GetType().IsSubclassOf(ctrlName) Then
                cDict.Add(child.UniqueID, child)
            End If
            findCtrl(child, child, ctrlName, cDict)
        Next
    End Sub

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        ' Check if cart is empty. if so redirect to cart.aspx
        If globals.IsCartEmpty Then
            Response.Redirect("cart.aspx", True)
        End If

        Dim str As Object = CustomerShippingProvider
        Dim dt As Data.DataTable = ExpandITLib.SQL2DataTable(PossiblePaymentTypesSQLString()) '("SELECT PaymentType FROM PaymentType_ShippingHandlingProvider WHERE ShippingProviderGuid = '" & str & "'")
        Dim cDict As New Dictionary(Of String, Control)

        findCtrl(Me, New Control(), GetType(ExpandIT.SimpleUIControls.CoreCustomControl), cDict)
        Dim checked As Boolean
        Dim lastId As ExpandIT.SimpleUIControls.GenericPaymentControl = Nothing
        For Each row As Data.DataRow In dt.Rows
            For Each epuc As Control In cDict.Values
                If epuc.ID = String.Concat(CType(row(0), String), "_form").ToLower() Then
                    epuc.Visible = True
                    If CBoolEx(globals.User("B2B")) AndAlso AppSettings("B2B_DEFAULT_PAYMENT_PROVIDER").Equals(CType(row(0), String).ToUpper) _
                        OrElse CBoolEx(globals.User("B2C")) AndAlso AppSettings("B2C_DEFAULT_PAYMENT_PROVIDER").Equals(CType(row(0), String).ToUpper) Then
                        CType(epuc, ExpandIT.SimpleUIControls.GenericPaymentControl).RadioButtonChecked = True
                        checked = True
                    End If
                    lastId = epuc
                    CType(epuc, ExpandIT.SimpleUIControls.GenericPaymentControl).clickEvent = New EventHandler(AddressOf BtnAccept_Click)
                End If
            Next
        Next
        If Not checked AndAlso lastId IsNot Nothing Then CType(lastId, ExpandIT.SimpleUIControls.GenericPaymentControl).RadioButtonChecked = True
        nextbutton.Text = Server.HtmlDecode(Resources.Language.ACTION_NEXT_GREATER_THAN)

        'AM2010080301 - CHECK OUT PROCESS MAP - Start
        If CBoolEx(AppSettings("EXPANDIT_US_USE_CHECK_OUT_PROCESS_MAP")) And Not CheckOutPaths() Is Nothing Then
            If CheckOutPaths.Length > 0 Then
                Master.CheckOutProcess.CheckOutProcessDict = CheckOutPaths()
                Master.CheckOutProcess.currentLocation = getCurrentLocation()
            End If
        End If
        'AM2010080301 - CHECK OUT PROCESS MAP - End

    End Sub

    ''' <summary>
    ''' Handles Click Event on BtnAccept          
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub BtnAccept_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim ea As ExpandIT.ExpEventArgs = CType(e, ExpandIT.ExpEventArgs)
        'WLB.10042012        'If ea.Text.Equals(cod_form.ID) Then
        'WLB.10042012        '    Me.CustomerPaymentMethod = "COD"
        'WLB.10042012        'End If
        'WLB.10042012        'If ea.Text.Equals(cop_form.ID) Then
        'WLB.10042012        '    Me.CustomerPaymentMethod = "COP"
        'WLB.10042012        'End If
        If ea.Text.Equals(_form.ID) Then
            Me.CustomerPaymentMethod = ""
        End If
        'WLB.10042012        'If ea.Text.Equals(vk_form.ID) Then
        'WLB.10042012        '    Me.CustomerPaymentMethod = "VK"
        'WLB.10042012        'End If
        'WLB.10042012        'If ea.Text.Equals(ls_form.ID) Then
        'WLB.10042012        '    Me.CustomerPaymentMethod = "LS"
        'WLB.10042012        'End If
        If ea.Text.Equals(paypal_form.ID) Then
            Me.CustomerPaymentMethod = "PAYPAL"
        End If
        'WLB.10042012        'If ea.Text.Equals(authnet_form.ID) Then
        'WLB.10042012        '    Me.CustomerPaymentMethod = "AUTHNET"
        'WLB.10042012        'End If
        ''<!--JA2010022601 - ECHECK - Start-->
        'WLB.10042012        'If ea.Text.Equals(echeck_form.ID) Then
        'WLB.10042012        '    Me.CustomerPaymentMethod = "ECHECK"
        'WLB.10042012        'End If
        '<!--JA2010022601 - ECHECK - End-->
        'AM2010041301 - ENTERPRISE PAYMENT GATEWAY - Start
        'Added the validation for the selected payment method.
        If ea.Text.Equals(eepg_form.ID) Then
            Me.CustomerPaymentMethod = "EEPG"
        End If
        'JA2010102801 - PAPERCHECK - Start
        If ea.Text.Equals(papercheck_form.ID) Then
            Me.CustomerPaymentMethod = "PAPERCHECK"
        End If
        'JA2010102801 - PAPERCHECK - End
        'AM2010041301 - ENTERPRISE PAYMENT GATEWAY - End
        If ea.Text.Equals(dibs_form.ID) Then
            Me.CustomerPaymentMethod = "DIBS"
        End If

        Response.Redirect("payment.aspx")

    End Sub

End Class
