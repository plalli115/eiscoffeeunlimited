Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT.GlobalsClass
Imports System.Web

Partial Class catalog
    Inherits ExpandIT.Page

    Protected rsCatalog, sql, Catalogs, CatalogKeys, keys As Object
    Protected CatalogAvailable As Boolean = False
    Protected Catalog As ExpDictionary

    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        eis.PageAccessRedirect("Catalog")

        CatalogAvailable = False
        Catalogs = eis.CatLoadCatalogs()
        If Catalogs.Count > 0 Then CatalogAvailable = True
        If Catalogs.Count = 1 Then
            keys = Catalogs.keys
            Response.Redirect(GroupLink(GetFirst(Catalogs).Value("GroupGuid")))
        End If

        panelAvailable.Visible = CatalogAvailable
        panelNotAvailable.Visible = Not CatalogAvailable
    End Sub

End Class
