﻿<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="contactform.aspx.vb" Inherits="contactform"
    CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master" 
    RuntimeMasterPageFile="ThreeColumn.master" CrumbName="<%$Resources: Language,LABEL_CONTACT_FORM %>" %>
    
<%@ Register TagPrefix="uc1" TagName="contactform" Src="~/controls/ContactForm.ascx" %>  


<asp:content id="Content1" contentplaceholderid="cphSubMaster" runat="Server">
  <div class="ContactFormPage">
    <uc1:contactform runat="server" ID="contact" />   
  </div>
  <p><a href="javascript:history.back()"><% =Resources.Language.LABEL_BACK%></a></p>
</asp:content>
