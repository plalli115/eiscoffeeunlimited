<%@ Import Namespace="System.Configuration.ConfigurationManager" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="history_ledgerentryBkp.aspx.vb"
    Inherits="history_ledgerentry" CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master"
    RuntimeMasterPageFile="ThreeColumn.master" CrumbName="<%$ Resources: Language, LABEL_ORDER_LEDGER %>" %>

<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">

    <script type="text/javascript">
        function PrepareEmail(strBody) {
            // SET MESSAGE VALUES
            var to = '<% =AppSettings("CUSTOMER_LEDGER_REQUEST_EMAIL_ADDRESS") %>';
            var strSubject = '<% =Resources.Language.MESSAGE_CUSTOMER_LEDGER_EMAIL_REQUEST_SUBJECT %>';

            // BUILD MAIL MESSAGE COMPONENTS
            var doc = "mailto:" + to +
			"?subject=" + strSubject +
			"&amp;body=" + strBody +
			"&amp;enctype=text/plain";

            // POP UP EMAIL MESSAGE WINDOW		
            window.location = doc;
        }
    </script>

    <div class="HistoryLedgerEntryPage">
        <uc1:PageHeader ID="PageHeader1" runat="server" Text="<%$ Resources: Language, LABEL_ORDER_LEDGER %>"
            EnableTheming="true" />
        <%
					
            If dictCuLeEn("Lines").Count = 0 Then%>
        <% =Resources.Language.MESSAGE_LEDGER_ENTRIES_NOT_AVAILABLE%>
        <%
        Else%>
        <% =Resources.Language.LABEL_CUSTOMER_LEDGER_EMAIL_INFORMATION%>
        <br />
        <br />
        <table cellspacing="0" cellpadding="1">
            <tr class="TR1">
                <td colspan="7">
                    <table>
                        <tr>
                            <td class="tdb">
                                &nbsp;<% =Resources.Language.LABEL_LEDGER_ENTRY_BALANCE%>:
                            </td>
                            <td class="tdr" style="text-align:right;">
                                <% =CurrencyFormatter.FormatAmount(dictCuLeEn("BalanceAmountLCY"), Session("UserCurrencyGuid").ToString)%>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdb">
                                &nbsp;<% =Resources.Language.LABEL_LEDGER_ENTRY_TOTAL%>:
                            </td>
                            <td class="tdr" style="text-align:right;">
                                <% =CurrencyFormatter.FormatAmount(dictCuLeEn("BalanceAmountLCY") + dictCuLeEn("OutstandingOrdersLCY") + dictCuLeEn("ShippedNotInvoicedLCY"), Session("UserCurrencyGuid").ToString)%>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdb">
                                &nbsp;<% =Resources.Language.LABEL_LEDGER_ENTRY_CREDIT_LIMIT%>:
                            </td>
                            <td class="tdr" style="text-align:right;">
                                <% =CurrencyFormatter.FormatAmount(dictCuLeEn("CreditLimitLCY"), Session("UserCurrencyGuid").ToString)%>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdb">
                                &nbsp;<% =Resources.Language.LABEL_LEDGER_ENTRY_OVERDUE_AMOUNTS%>:<br />
                                (<% =FormatDate(Now)%>)
                            </td>
                            <td class="tdr" valign="top" style="text-align:right;">
                                <% =CurrencyFormatter.FormatAmount(dictCuLeEn("BalanceDueLCY"), Session("UserCurrencyGuid").ToString)%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <th class="THR">
                    &nbsp;<% =Resources.Language.LABEL_FIELD_LEDGER_POSTDATE%>&nbsp;
                </th>
                <th class="THR">
                    &nbsp;<% =Resources.Language.LABEL_FIELD_LEDGER_DOCUMENT_NO%>&nbsp;
                </th>
                <th class="THL">
                    &nbsp;<% =Resources.Language.LABEL_FIELD_LEDGER_DOCUMENT_TYPE%>&nbsp;
                </th>
                <th class="THL">
                    &nbsp;<% =Resources.Language.LABEL_FIELD_LEDGER_DOCUMENT_DESCRIPTION%>&nbsp;
                </th>
                <th class="THR">
                    &nbsp;<% =Resources.Language.LABEL_FIELD_LEDGER_AMOUNT%>&nbsp;
                </th>
                <th class="THC">
                    &nbsp;<% =Resources.Language.LABEL_FIELD_LEDGER_DOCUMENT_OPEN%>&nbsp;
                </th>
                <th class="THC">
                    &nbsp;&nbsp;
                </th>
            </tr>
            <%
                For Each dictItem In dictCuLeEn("Lines").Values%>
            <tr class="TR<% = globals.stylesheetrowcounter %>">
                <td class="tdr">
                    &nbsp;<% = FormatDate(dictItem("PostingDate")) %>
                </td>
                <td class="tdr">
                    &nbsp;<% = HTMLEncode(dictItem("DocumentGuid")) %>
                </td>
                <td class="tdl">
                    &nbsp;<% =HTMLEncode(b2b.GetDocumentTypeDescription(CLngEx(dictItem("DocumentType"))))%>
                </td>
                <td class="tdl">
                    &nbsp;<% = HTMLEncode(dictItem("Description")) %>
                </td>
                <td class="tdr" style="text-align:right;">
                    &nbsp;<% =CurrencyFormatter.FormatAmount(dictItem("Amount"), dictItem("CurrencyGuid"))%>
                </td>
                <td class="tdc">
                    &nbsp;<input type="checkbox" <%If CBoolEx(dictItem("IsOpen")) Then Response.Write("checked=""checked""")%> disabled="disabled" />
                </td>
                <% 
                    If CStrEx(dictItem("Description")) = "" Then dictItem("Description") = "?"
                    If CStrEx(dictItem("DocumentGuid")) = "" Then dictItem("DocumentGuid") = "?"
                    strBody = Resources.Language.MESSAGE_CUSTOMER_LEDGER_EMAIL_REQUEST_BODY
                    strBody = Replace(strBody, "%LEDGER_ENTRY_NO%", CStrEx(dictItem("DocumentGuid")))
                    strBody = Replace(strBody, "%LEDGER_ENTRY_TYPE%", b2b.GetDocumentTypeDescription(CLngEx(dictItem("DocumentType"))))
                    strBody = Replace(strBody, "%LEDGER_ENTRY_DESCRIPTION%", CStrEx(dictItem("Description")))
                    strBody = Replace(strBody, "%USER_INFO%", globals.User("ContactName") & _
                    "%NEWLINE%" & globals.User("CompanyName") & "%NEWLINE%" & globals.User("Address1") & _
                    "%NEWLINE%" & globals.User("ZipCode") & " " & globals.User("CityName") & _
                    "%NEWLINE%" & Resources.Language.LABEL_CUSTOMER_NO & ":" & globals.User("CustomerGuid"))
                    strBody = Replace(strBody, "%NEWLINE%", AppSettings("CUSTOMER_LEDGER_NEWLINE_CHARS"))
                %>
                <td>                    
                    <img style="cursor: hand" onclick="PrepareEmail('<% = HTMLEncode(strBody) %>');"
                        src="<% = VRoot %>/images/email.gif" border="0" alt="<% = Resources.Language.LABEL_SEND_EMAIL_REQUEST %>" />
                </td>
            </tr>
            <%
            Next%>
        </table>        
        <% End If%>
        <br />
        <!-- Here ends the HTML code for the page -->
    </div>
</asp:Content>
