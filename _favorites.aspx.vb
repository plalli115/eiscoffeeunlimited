Imports System.Configuration.ConfigurationManager
Imports ExpandIT.GlobalsClass
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT
Imports System.Data.SqlClient

Partial Class _favorites
    Inherits ExpandIT.Page

    Protected ProductGuid As String

    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Action As String

        ' Check the page access.
        eis.PageAccessRedirect("Favorites")

        ' Get Action.
        Action = UCase(Request("action"))
        globals.GroupGuid = Request("GroupGuid")
        ' Get ProductGuid.
        If Request("SKU") <> "" Then
            ProductGuid = Request("SKU")
        Else
            ProductGuid = "NO_PRODUCT_GUID"
        End If

        ' Do Action
        Select Case Action
            Case "ADD"
                Add()
            Case "OUTOFFAVORITE"
                OutOfFavList()
            Case ""
                Response.Redirect(VRoot & "/favorites.aspx")
            Case Else
                globals.messages.Errors.Add(Resources.Language.MESSAGE_UNKNOWN_ACTION & " '" & Action & "'")
        End Select

        ' Find URL to redirect to.
        If Request("returl") Is Nothing Then
            Response.Redirect(MakeParam(VRoot & "/favorites.aspx?GroupGuid=" & globals.GroupGuid, globals.messages.QueryString))
        Else
            Response.Redirect(MakeParam(VRoot & "/" & Request("returl"), globals.messages.QueryString))
        End If

    End Sub

    ' Helper function: Add the item(s) to favorite.
    Sub Add()
        Dim SQLtest As String
        Dim SQLinsert As String

        ' Test if product is already in Favorites.	
        SQLtest = "SELECT * FROM Favorites WHERE UserGuid = " & SafeString(globals.User("UserGuid")) & " AND ProductGuid = " & SafeString(ProductGuid)

        ' If not then add it. If it is then remind the user that the products is already added.
        If Not HasRecords(SQLtest) Then
            SQLinsert = "INSERT INTO Favorites (ProductGuid, UserGuid) VALUES (" & SafeString(ProductGuid) & "," & SafeString(globals.User("UserGuid")) & ")"
            excecuteNonQueryDb(SQLinsert)
        Else
            globals.messages.Warnings.Add(Resources.Language.LABEL_THE_PRODUCT_IS_ALREADY_IN_THE_LIST)
        End If
    End Sub

    ' Helper function: 
    ' "OutOfFavList" will allow a product to be removed from the favorite list, by clicking the red cross
    ' (delete) on the favorites.asp page.
    Sub OutOfFavList()
        ProductGuid = GetRequest("ProductGuid")
        If ProductGuid <> "" Then
            excecuteNonQueryDb("DELETE FROM Favorites WHERE UserGuid=" & SafeString(globals.User("UserGuid")) & " AND ProductGuid = " & SafeString(ProductGuid))
        End If
    End Sub

    ' Helper function: Adds the error string to the URL
    Function MakeParam(ByVal url As Object, ByVal errorString As Object) As String
        Dim retv As String

        retv = url
        If errorString <> "" Then
            retv = retv & IIf(InStr(1, url, "?") = 0, "?", "&") & errorString
        End If

        Return retv
    End Function
End Class
