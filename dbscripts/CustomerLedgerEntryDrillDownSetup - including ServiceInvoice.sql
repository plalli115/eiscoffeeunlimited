USE [EIS_CoffeeUnlimited_20230725]
GO
/****** Object:  Table [dbo].[CustomerLedgerDrillDownSetup]    Script Date: 7/25/2023 2:38:50 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CustomerLedgerDrillDownSetup]') AND type in (N'U'))
DROP TABLE [dbo].[CustomerLedgerDrillDownSetup]
GO
/****** Object:  Table [dbo].[CustomerLedgerDrillDownSetup]    Script Date: 7/25/2023 2:38:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerLedgerDrillDownSetup](
	[DocumentType] [int] NOT NULL,
	[HeaderTable] [nvarchar](50) NOT NULL,
	[LineTable] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
INSERT [dbo].[CustomerLedgerDrillDownSetup] ([DocumentType], [HeaderTable], [LineTable]) VALUES (2, N'SalesInvoiceHeader', N'SalesInvoiceLine')
INSERT [dbo].[CustomerLedgerDrillDownSetup] ([DocumentType], [HeaderTable], [LineTable]) VALUES (3, N'SalesMemoHeader', N'SalesMemoLine')
INSERT [dbo].[CustomerLedgerDrillDownSetup] ([DocumentType], [HeaderTable], [LineTable]) VALUES (7, N'ServiceInvoiceHeader', N'ServiceInvoiceLine')
GO
