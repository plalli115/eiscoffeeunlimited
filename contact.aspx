﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="contact.aspx.vb" Inherits="contact"
    CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master"
    RuntimeMasterPageFile="ThreeColumn.master" CrumbName="<%$ Resources: Language, LABEL_CONTACT_FORM %>" %>

<%@ Register Src="~/controls/ContactForm.ascx" TagPrefix="uc1" TagName="contactForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <div class="AccountPage">
        <uc1:contactForm ID="contactForm1" runat="server" />
    </div> 
</asp:Content>
