﻿Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT.GlobalsClass


Partial Class contactform
    Inherits ExpandIT.Page
    
    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
      Dim subject As String = Request.Params("subject")      
      If Not String.IsNullOrEmpty(subject)
        Dim sendToSel As ContactFormCtrl.SendToAddrSelections = [Enum].Parse(GetType(ContactFormCtrl.SendToAddrSelections), Request.Params("SendTo"))
        contact.Subj = subject
        contact.SendToSelection = sendToSel
      End If
    End Sub

End Class
