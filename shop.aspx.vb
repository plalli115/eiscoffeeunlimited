Imports System.Configuration.ConfigurationManager
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT
Imports ExpandIT.ExpandITLib
Imports System.Web

Partial Class shop
    Inherits ExpandIT.ShippingPayment._CartPage
    '<!--JA2010031101 - CATMAN SLIDE - Start-->
    Protected SlideShowEnabled As Boolean
    Protected str As String
    '<!--JA2010031101 - CATMAN SLIDE - End-->

    '<!--JA2010061701 - SLIDE SHOW - Start-->
    Protected ItemsSlideShow As Boolean
    Protected SlideDict As ExpDictionary
    '<!--JA2010061701 - SLIDE SHOW - End-->
	Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'AM2010051801 - SPECIAL ITEMS - Start
        Dim showSpecials As Boolean = False
        'AM2010051801 - SPECIAL ITEMS - End
        ' Check the page access.
        eis.PageAccessRedirect("HomePage")


        ' Add meta tags
        Dim meta As HtmlMeta
        meta = New HtmlMeta
        meta.Name = "description"
        meta.Content = Resources.Language.MESSAGE_WELCOME_TO_SHOP
        Header.Controls.Add(meta)

        'AM2010040601 - GEOIP - Start
        If Not IsLocalMode() And AppSettings("EXPANDIT_US_USE_GEOIP_REDIRECTION") Then
            Dim objGEOIP As New GEOIP()
            objGEOIP.GEOIPRedirection()
        End If
        'AM2010040601 - GEOIP - End


        'AM2010051801 - SPECIAL ITEMS - Start

        'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - Start
        'showSpecials = CBoolEx(getSingleValueDB("SELECT SpecialsEnabled FROM EESetup"))
        Try
            showSpecials = CBoolEx(eis.getEnterpriseConfigurationValue("SpecialsEnabled"))
        Catch ex As Exception
        End Try
        'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - End
        If Not showSpecials Then
            Me.Specials1.Visible = False
        End If
        'AM2010051801 - SPECIAL ITEMS - End

        'JA2010030901 - PERFORMANCE -Start
        'If Not CBoolEx(AppSettings("SHOW_BEST_SELLERS")) Then
        '    Me.BestSellersBox1.Visible = False
        'End If

        If CBoolEx(AppSettings("SHOW_BEST_SELLERS")) Then
            Dim myControl As Control = Me.LoadControl("~/controls/boxes/BestSellersBox.ascx")
            myControl.ID = "BestSellersBox1"
            eis.SetControlProperties(myControl, "SetMethod", "GetMostPopularItems")
            Me.phGetMostPopularItems.Controls.Clear()
            Me.phGetMostPopularItems.Controls.Add(myControl)
        End If
        'JA2010030901 - PERFORMANCE -End

    End Sub



    Protected Overloads Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        '<!--JA2010061701 - SLIDE SHOW - Start-->
        Dim sql As String = ""

        If CBoolEx(AppSettings("REQUIRE_CATALOG_ENTRY")) Then
            'ExpandIT US - USE CSC - Start
            If CBoolEx(AppSettings("EXPANDIT_US_USE_CSC")) Then
                Dim cscObj As New csc(globals)
                sql = cscObj.featuredItemsForSlideSQL()
            Else
                'AM2010071901 - ENTERPRISE CSC - Start
                sql = "SELECT P.ProductGuid FROM ProductTable P INNER JOIN (SELECT DISTINCT " & _
                    "GroupProduct.ProductGuid FROM GroupProduct WHERE CatalogNo=" & SafeString(globals.User("CatalogNo")) & ") AS GP ON P.ProductGuid=GP.ProductGuid "
                'AM2010071901 - ENTERPRISE CSC - End
            End If
            'ExpandIT US - USE CSC - End
        Else
            sql = "SELECT ProductGuid FROM ProductTable "
        End If

        sql &= "WHERE FeaturedItem='True'"
        SlideDict = Sql2Dictionaries(sql, "ProductGuid")

        If SlideDict.Count = 0 Then
            ItemsSlideShow = False
        Else
            ItemsSlideShow = True
        End If
        '<!--JA2010061701 - SLIDE SHOW - End-->


        '<!--JA2010031101 - CATMAN SLIDE - Start-->
        'Ckeck if the Slide Show is Enabled
        If CBoolEx(AppSettings("SHOW_CATMAN_SLIDE")) Then

            If eis.CheckPageAccess("Catalog") Then
                'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - Start
                'sql = "SELECT SlideShowEnabled FROM EESetup"
                'SlideShowEnabled = getSingleValueDB(sql)
                Try
                    SlideShowEnabled = CBoolEx(eis.getEnterpriseConfigurationValue("SlideShowEnabled"))
                Catch ex As Exception
                End Try
                'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - End
            Else
                SlideShowEnabled = Nothing
            End If
            Me.CatManSlide1.ShowSlides = CBoolEx(SlideShowEnabled)
            str = CStrEx(eis.getSlideImages())
            Me.CatManSlide1.SlideImages = str
        End If
        '<!--JA2010031101 - CATMAN SLIDE - End-->



    End Sub
	
	


    '<!--JA2010060101 - ALERTS - Start-->
    Function getAlerts()
        Dim SQL As String = ""
        If CBoolEx(globals.User("Anonymous")) Then
            SQL = "SELECT * FROM EEMCAlerts WHERE Disabled = 0 AND AlertType='SITEMESSAGE' AND (CustomerType = 0) AND (StartingDate <= getDate()) AND (EndingDate >= getDate() OR EndingDate IS NULL) ORDER BY CustomerType DESC"
        ElseIf CBoolEx(globals.User("B2B")) Then
            SQL = "SELECT * FROM EEMCAlerts WHERE Disabled = 0 AND AlertType='SITEMESSAGE' AND (CustomerType = 0 Or (CustomerType = 1 AND CustomerNo=" & SafeString(globals.User("CustomerGuid")) & ")) AND (StartingDate <= getDate()) AND (EndingDate >= getDate() OR EndingDate IS NULL) ORDER BY CustomerType DESC"
        ElseIf CBoolEx(globals.User("B2C")) Then
            SQL = "SELECT * FROM EEMCAlerts WHERE Disabled = 0 AND AlertType='SITEMESSAGE' AND (CustomerType = 0 Or CustomerType = 2) AND (StartingDate <= getDate()) AND (EndingDate >= getDate() OR EndingDate IS NULL) ORDER BY CustomerType DESC"
        End If

        Dim retv As ExpDictionary = SQL2Dicts(SQL, "AlertNo")

        Dim load As Boolean = False
        Dim showAlert As Boolean = False

        If retv.Count = 0 Then
            Dim valuesDict As New ExpDictionary
            valuesDict.Add("AlertNo", "ALTEST")
            valuesDict.Add("CustomerType", "1")
            valuesDict.Add("CustomerNo", "00000")
            valuesDict.Add("CustomerName", "Customer Name")
            valuesDict.Add("User", "")
            valuesDict.Add("UserName", "")
            valuesDict.Add("Message", "")
            valuesDict.Add("Disabled", "0")
            valuesDict.Add("NoSeries", "EEMC-ALERT")
            valuesDict.Add("StartingDate", Date.Now)
            valuesDict.Add("EndingDate", DBNull.Value)
            valuesDict.Add("AlertType", "")
            retv.Add("ALTEST", valuesDict)
        End If

        If Not retv Is Nothing And Not retv Is DBNull.Value Then
            If retv.Count > 0 Then
                If CBoolEx(globals.User("B2B")) Or CBoolEx(globals.User("B2C")) Then
                    For Each item As ExpDictionary In retv.Values
                        If item("CustomerType") = "1" Or item("CustomerType") = "2" Then
                            showAlert = True
                        End If
                    Next
                    If showAlert Then
                        load = True
                    Else
                        load = False
                    End If
                Else
                    load = True
                End If
            End If
        End If

        If Not retv("ALTEST") Is Nothing AndAlso retv("ALTEST")("AlertNo") = "ALTEST" Then
            load = False
            Session("LoadFirstTime") = "True"
        End If

        Return load
    End Function
    '<!--JA2010060101 - ALERTS - End-->
End Class
