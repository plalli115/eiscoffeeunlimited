﻿<%@ Import Namespace="System.Configuration.ConfigurationManager" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ConfirmReg.aspx.vb" Inherits="confirmReg"
  CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master"  
  RuntimeMasterPageFile="ThreeColumn.master" CrumbName="<%$ Resources: Language, LABEL_REGISTRATION_CONFIRM %>"  %>

<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<%@ Register Src="~/controls/Message.ascx" TagName="Message" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">  
  <uc1:PageHeader ID="PageHeader1" runat="server" text="<%$ Resources: Language, LABEL_REGISTRATION_CONFIRM %>" EnableTheming="true" />
  <div class="UserLoginPage">
    <uc1:Message ID="Message1" runat="server" />
    <p></p>
    <div style="text-align:right">
      <asp:Button ID="HomeButton" runat="server" CssClass="AddButton" Text="<%$ Resources: Language, LABEL_MENU_HOME %>" />
    </div>
  </div>  
</asp:Content>