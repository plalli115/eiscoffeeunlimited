<%@ Import Namespace="System.Configuration.ConfigurationManager" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="UserPassword.aspx.vb" Inherits="UserPassword"
    CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master"
    RuntimeMasterPageFile="ThreeColumn.master" CrumbName="<%$Resources: Language, LABEL_USERDATA_UPDATE_USER_PASSWORD %>" %>

<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <asp:Panel ID="passwordPanel" runat="server" DefaultButton="BtnSubmit">
        <div class="UserPassword">
            <!-- Here begins the HTML code for the page -->
            <h1 class="PageHeader">
                <asp:Label runat="server" ID="headerText" />
            </h1>
            <asp:BulletedList runat="server" ID="errorBulletList" BulletStyle="Disc" EnableViewState="false" />
            <asp:Label ID="successinfo" runat="server" EnableViewState="false" />
            <p>
                <asp:Label runat="server" ID="userRegisterInformation" Text='<%$Resources: Language, MESSAGE_USE_FIELDS_BELOW_TO_CHANGE_PASSWORD %>' />
            </p>
            <table border="0" cellspacing="0">
                <tr class="TR1">
                    <td class="TDR">
                        <asp:Label ID="Label14" runat="server" Text='<%$Resources: Language,LABEL_USERDATA_PASSWORD %>' />:
                    </td>
                    <td class="TDL">
                        <asp:TextBox ID="OldPassword" runat="server" CssClass="borderTextBox" TextMode="Password"
                            MaxLength="40" /><asp:Label runat="server" ID="lblUserLogin" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr class="TR1">
                    <td class="TDR">
                        <asp:Label ID="Label15" runat="server" Text='<%$Resources: Language, LABEL_USERDATA_NEW_PASSWORD %>' />:
                    </td>
                    <td class="TDL">
                        <asp:TextBox ID="Password1" runat="server" CssClass="borderTextBox" TextMode="Password"
                            MaxLength="40" /><asp:Label runat="server" ID="lblPassword1" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr class="TR1">
                    <td class="TDR">
                        <asp:Label ID="Label16" runat="server" Text='<%$Resources: Language, LABEL_USERDATA_NEW_PASSWORD_CONFIRM %>' />:
                    </td>
                    <td class="TDL">
                        <asp:TextBox ID="Password2" runat="server" CssClass="borderTextBox" TextMode="Password"
                            MaxLength="40" /><asp:Label runat="server" ID="lblPassword2" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td align="right" colspan="3">
                        <input class="AddButton" id="BtnReset" type="reset" value="<%=Resources.Language.ACTION_RESET %>" />
                        <asp:Button runat="server" CssClass="AddButton" ID="BtnSubmit" Text='<%$Resources: Language,ACTION_UPDATE %>' />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
</asp:Content>
