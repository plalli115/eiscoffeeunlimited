﻿'AM2010042101 - PICTURES RESIZER - Start
Imports System.Data
Imports System.Data.OleDb
Imports Radactive.WebControls.ILoad
Imports Radactive.WebControls.ILoad.Configuration
Imports System.IO
Imports System.Drawing
Imports System.Configuration.ConfigurationManager
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT.GlobalsClass
Imports ExpandIT
Imports ExpandIT.ExpandITLib


Partial Public Class picturesResizer
    Inherits ExpandIT.Page

    Dim picturesTypesDict As New ExpDictionary()
    Dim picturesProcessed As Integer = 0


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If lblError.Text = "" Then
            lblError.Visible = False
        End If
        If Not Page.IsPostBack Then
            fillPicturesTypesDict()

            Me.RptPictureTypes.DataSource = picturesTypesDict
            Me.RptPictureTypes.DataBind()
        End If

    End Sub

    Public Sub fillPicturesTypesDict()
        Dim sql As String

        sql = "SELECT * FROM ImageProcessor WHERE Resize=1"
        picturesTypesDict = SQL2Dicts(sql, "PictureTypeGuid")
    End Sub


    Protected Sub processPictures(ByVal selectedTypes As ExpDictionary)

        Dim organizedTypesDict As New ExpDictionary
        Dim previousSource As String = ""
        Dim cont As Integer = 0

        For Each type As ExpDictionary In selectedTypes.Values
            If previousSource <> type("SourcePath") Then
                cont += 1
                previousSource = type("SourcePath")
                organizedTypesDict(cont) = New ExpDictionary
                organizedTypesDict(cont)(type("PictureTypeGuid")) = type
            Else
                organizedTypesDict(cont)(type("PictureTypeGuid")) = type
            End If
        Next
        For Each groupOfTypes As ExpDictionary In organizedTypesDict.Values
            processPicturesBySourcePath(groupOfTypes)
        Next
        Me.ClientScript.RegisterStartupScript(Me.GetType(), "allDone", "alert('Successfully processed " + Me.picturesProcessed.ToString() + " image files.');", True)

    End Sub

    Protected Sub processPicturesBySourcePath(ByVal PathDict As ExpDictionary)

        Dim config As ILoadConfiguration
        Dim sourceFolderPath As String = ""
        Dim sourceFileType As String = ""
        Dim producedFolderPath As String = ""
        Dim destinationFolderPath As String = ""
        Dim processedFolderPath As String = ""
        Dim prevProcessedFolderPath As String = ""
        Dim pictureWidth As Double = 0
        Dim pictureHeight As Double = 0
        Dim Fname As String
        Dim size As String = ""
        Dim OriginalFilename As String = ""
        Dim definition1 As WebImageDefinition

        For Each type As ExpDictionary In PathDict.Values
            config = New ILoadConfiguration()
            sourceFolderPath = CStrEx(type("SourcePath"))
            producedFolderPath = CStrEx(type("ProcessingPath"))
            destinationFolderPath = CStrEx(type("DestinationPath"))
            processedFolderPath = CStrEx(type("ProcessedPath"))
            pictureWidth = CDblEx(type("PictureWidth"))
            pictureHeight = CDblEx(type("PictureHeight"))
            sourceFileType = CStrEx(type("SourceFileType"))

            Try
                definition1 = New WebImageDefinition("Default", "Default")
                config.WebImageDefinitions.Add(definition1)

                definition1.SizeConstraint.Mode = WebImageSizeConstraintMode.FreeSize
                definition1.SizeConstraint.FreeSizeData.LimitsW.LowLimit.Value = 50
                definition1.SizeConstraint.FreeSizeData.LimitsW.LowLimit.Enabled = True
                definition1.SizeConstraint.FreeSizeData.LimitsH.LowLimit.Value = 50
                definition1.SizeConstraint.FreeSizeData.LimitsH.LowLimit.Enabled = True
                definition1.SizeConstraint.FreeSizeData.LimitsH.HiLimit.Value = 10000
                definition1.SizeConstraint.FreeSizeData.LimitsH.HiLimit.Enabled = True
                definition1.SizeConstraint.FreeSizeData.LimitsW.HiLimit.Value = 10000
                definition1.SizeConstraint.FreeSizeData.LimitsW.HiLimit.Enabled = True

                ' Automatic thumbnail #1

                Dim resize1 As WebImageResizeDefinition = New WebImageResizeDefinition("Thumbnail 1", "Thumb1")
                definition1.ResizeDefinitions.Add(resize1)
                resize1.Mode = Radactive.WebControls.ILoad.Core.Drawing.ImageResizeMode.Fixed
                resize1.Fit = False
                Dim backColor As New System.Drawing.Color()
                Try
                    backColor = System.Drawing.ColorTranslator.FromHtml(CStrEx(type("HexColorCode")))
                Catch ex As Exception
                    backColor = Color.White
                End Try
                resize1.BackgroundColor = backColor
                resize1.FixedSize = New System.Drawing.Size(pictureWidth, pictureHeight)

                Dim filePaths() As String = System.IO.Directory.GetFiles(sourceFolderPath, "*." & sourceFileType)
                Dim picturesToProcessList As String = ""
                Dim picturesToProcess As String()
                picturesToProcessList = CStrEx(type("PicturesToProcess")).Trim()
                If picturesToProcessList <> "" Then
                    picturesToProcess = picturesToProcessList.Split(",")
                End If

                Dim filePath As String
                For Each filePath In filePaths
                    ' Use the file name as id for the generated I-Load WebImage
                    Dim imageId As String = System.IO.Path.GetFileNameWithoutExtension(filePath)
                    Dim process As Boolean = False
                    Dim nameArray As String()

                    If picturesToProcessList = "" Then
                        process = True
                    Else
                        nameArray = imageId.Split("_")
                        For Each index As String In picturesToProcess
                            If index = nameArray(1) Then
                                process = True
                            End If
                        Next
                    End If
                    If process Then
                        ' Process the image 
                        WebImage.CreateIntoFileSystem(filePath, producedFolderPath, imageId, config)
                        picturesProcessed += 1
                    End If
                Next

                ' All images have been processed

                Dim dir3 As DirectoryInfo = New DirectoryInfo(producedFolderPath)
                Dim folderfiles1 As FileInfo() = dir3.GetFiles()
                Dim ThumbPosi As Integer
                Dim sql As String = ""
                If folderfiles1.Length > 0 Then
                    For Each afile1 As FileInfo In folderfiles1
                        If afile1.Name.Contains("_Thumb1") Then
                            ThumbPosi = afile1.Name.LastIndexOf("_Thumb1")
                            Fname = afile1.Name.Substring(0, ThumbPosi)
                            'use delete logic if file aready exists
                            If File.Exists(destinationFolderPath + "\\" + Fname + ".jpg") Then
                                File.Delete(destinationFolderPath + "\\" + Fname + ".jpg")
                            End If
                            afile1.MoveTo(destinationFolderPath + "\\" + Fname + ".jpg")
                            size = pictureWidth & "x" & pictureHeight
                            OriginalFilename = afile1.Name.ToString()
                            
                            If File.Exists(destinationFolderPath + "\" + Fname + ".jpg") Then
                                sql = "INSERT INTO ImageProcessorLog VALUES (" & SafeString(size) & "," & SafeString(destinationFolderPath) & ",'" & Fname & "." & sourceFileType & "','" & Fname & ".jpg','Resized',getdate())"
                                excecuteNonQueryDb(Sql)
                            End If

                        End If
                    Next
                    Kill(producedFolderPath + "\\" + "*.*")
                End If

                If processedFolderPath <> prevProcessedFolderPath Then
                    Dim dir1 As DirectoryInfo = New DirectoryInfo(sourceFolderPath)
                    Dim dir2 As DirectoryInfo = New DirectoryInfo(processedFolderPath)
                    Dim folderfiles As FileInfo() = dir1.GetFiles()
                    'Copy the pictures to the backup folder
                    If folderfiles.Length > 0 Then
                        For Each afile As FileInfo In folderfiles
                            afile.CopyTo(processedFolderPath + "\\" + afile.Name, True)
                        Next
                    End If
                End If

                prevProcessedFolderPath = processedFolderPath
            Catch ex As Exception
                lblError.Text = ex.Message.ToString()
                lblError.Visible = True
            End Try
        Next
        'Delete all pictures from the source folder
        Try
            Kill(sourceFolderPath + "\\" + "*.*")
        Catch ex As Exception
        End Try

    End Sub

    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        Dim selectedTypes As New ExpDictionary()

        If Me.picturesTypesDict Is Nothing Or Me.picturesTypesDict.Count = 0 Then
            fillPicturesTypesDict()
        End If
        If Me.picturesTypesDict.Count > 0 Then
            For Each typeDict As ExpDictionary In Me.picturesTypesDict.Values
                If CStrEx(Request("cbx_ALL")) <> "" Or CStrEx(Request("cbx_" & typeDict("PictureTypeGuid"))) <> "" Then
                    selectedTypes(typeDict("PictureTypeGuid")) = typeDict
                End If
            Next
            If selectedTypes IsNot Nothing And selectedTypes.Count > 0 Then
                processPictures(selectedTypes)
            End If
        End If
    End Sub
	
	Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub

End Class
'AM2010042101 - PICTURES RESIZER - End