Imports System.Configuration.ConfigurationManager
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT.GlobalsClass
'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
Imports ExpandIT
'AM2011031801 - ENTERPRISE CSR STAND ALONE - End

Partial Class history_lookup
    Inherits ExpandIT.Page

    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        eis.PageAccessRedirect("Order")
    End Sub

    Protected Sub submit1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles submit1.Click
        Context.Items.Add("CustomerReference", Me.CustomerReference.Text)
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
        If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
            Dim csrObj As New USCSR(globals)
            csrObj.historyLookupTransfer()
        Else
            Server.Transfer("~/_history_lookup.aspx", False)
        End If
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
    End Sub
End Class
