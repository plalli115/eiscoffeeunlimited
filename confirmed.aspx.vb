Imports System.Configuration.ConfigurationManager
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT.GlobalsClass
Imports ExpandIT
Imports System.Web
Imports System.Collections.Generic

Partial Class Confirmed
    Inherits ExpandIT.Page

    Protected Order, OrderHeader As ExpDictionary
    Protected HeaderGuid, CustomerReference, sql, OrderFound As Object
    Protected intRetryCounter, boolWaitMode, dictCart As Object
    Protected txtActionString As Object
    Protected bMailSend As Boolean
    Protected strAdditionalInformation As String
    'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
    Protected csrObj As USCSR
    'AM2011031801 - ENTERPRISE CSR STAND ALONE - End

    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            ' Check the page access.
            eis.PageAccessRedirect("Order")

            

            intRetryCounter = CLngEx(Request("RetryCounter"))

            If intRetryCounter > 5 Then
                ' Do some error handling
                paymentFailed()
            End If

            intRetryCounter = intRetryCounter + 1

            HeaderGuid = CStrEx(Request("HeaderGuid"))

            If HeaderGuid <> "" Then
                Order = eis.LoadOrder(HeaderGuid)
                OrderFound = isOrderFound(Order)
            End If

            If Not OrderFound Then
                ' When using DIBS the Customer Reference is sent as parameter, not the HeaderGuid
                CustomerReference = CStrEx(Request("CustomerReference"))
                If String.IsNullOrEmpty(CustomerReference) Then
                    CustomerReference = CStrEx(Request("invoice"))
                End If
                If CustomerReference <> "" Then
                    sql = "SELECT HeaderGuid FROM ShopSalesHeader WHERE CustomerReference = " & SafeString(CustomerReference)
                    OrderHeader = Sql2Dictionary(sql)
                    If OrderHeader IsNot Nothing Then
                        HeaderGuid = OrderHeader("HeaderGuid")
                        Order = eis.LoadOrder(OrderHeader("HeaderGuid"))
                    End If
                End If
                If Order IsNot Nothing Then
                    OrderFound = isOrderFound(Order)
                End If
            End If

            boolWaitMode = False

            If Not OrderFound Then
                ' Look for cart with error information.
                dictCart = eis.LoadOrderDictionary(globals.User)
                If CStrEx(dictCart("PaymentStatus")) <> "" Then
                    ' Do some error handling.
                    paymentFailed()
                Else
                    boolWaitMode = True
                End If
            Else
                bMailSend = CBoolEx(ExpandITLib.getSingleValueDB("SELECT MailSent FROM ShopSalesHeader WHERE HeaderGuid = " & SafeString(HeaderGuid)))
                ' Update the cartcounters. Call with Empty aregument to clear all counters.
                ' When Confirmed.asp is called, the cart will always be empty. 
                eis.UpdateMiniCartInfo(Nothing)
            End If

            ' Additional information required by danish laws
            strAdditionalInformation = Resources.Language.LABEL_PAYMENT_MORE_INFO
            strAdditionalInformation = Replace(strAdditionalInformation, "%LINK_START%", "<a href=""" & VRoot & "/info/info.aspx"">")
            strAdditionalInformation = Replace(strAdditionalInformation, "%LINK_END%", "</a>")

            'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
            csrObj = New USCSR(globals)
            csrObj.confirmedLoadPlaceHolder(Me, phCSRConfirmedOrderCompleted, Order, bMailSend)
            'AM2011031801 - ENTERPRISE CSR STAND ALONE - End

            ' DEBUG INFORMATION COLLECTION
            If globals.DebugLogic Then
                DebugValue(User, "User")
                DebugValue(Order, "Order")
            End If
        Catch ex As Exception

        End Try
    End Sub

    ' HELPER FUNCTIONS
    Sub paymentFailed()
        ' Redirect to payment.asp with information about the payment error.
        Response.Redirect(VRoot & "/payment.aspx?paymentfailed=" & Server.UrlEncode("Unable to locate order on confirm page. Reference: [" & CStrEx(Request("HeaderGuid")) & " " & CStrEx(Request("invoice")) & " " & CStrEx(Request("CustomerReference"))) & "]", False)
    End Sub

    Function isOrderFound(ByVal OrderDict As ExpDictionary) As Boolean

        Try
            If OrderDict.Exists("Lines") Then
                If OrderDict("Lines").Count > 0 Then
                    Return True
                End If
            End If
        Catch ex As Exception

        End Try

    End Function

    ' Here all the GET elements are picked up and put in a string value.
    Function GetQueryString() As String
        Dim Item As String
        Dim i As Integer
        Dim txtQueryString As String
        Dim counter As Integer
        Dim qs As Dictionary(Of String, ArrayList) = QueryString2Dict(Request.QueryString.ToString)

        txtQueryString = ""
        counter = 0
        If qs.Count > 0 Then
            For Each Item In qs.Keys
                For i = 0 To qs(Item).Count - 1
                    If Item <> "RetryCounter" Then
                        counter = counter + 1
                        txtQueryString = txtQueryString & HTMLEncode(Item) & "=" & Server.UrlEncode(qs(Item)(i)) & "&"
                    End If
                Next
            Next
            If counter > 0 Then
                txtQueryString = Left(txtQueryString, Len(txtQueryString) - 1)
            End If
        End If

        GetQueryString = Replace(txtQueryString, "'", "\")
    End Function

    ' Here all the POST elements are picked up written as hidden fields.
    Sub PrintPostElements()
        Dim qs As Dictionary(Of String, ArrayList) = QueryString2Dict(Request.Form.ToString)

        If qs.Count > 0 Then
            For Each Item As KeyValuePair(Of String, ArrayList) In qs
                For i As Integer = 0 To qs(Item.Key).Count - 1
                    Response.Write("<input type=""hidden"" name=""" & HTMLEncode(Item) & """ value=""" & HTMLEncode(qs(Item.Key)(i)) & """>")
                Next
            Next
        End If
    End Sub

End Class
