Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT.GlobalsClass

Partial Class user_login
    Inherits ExpandIT.Page

    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        ' Redirect to front page if user already logged in.
        If Not globals.User("Anonymous") Then
            Response.Redirect(VRoot & "/")
        Else
            Page.SetFocus(login)
        End If

    End Sub

    Protected Sub SubmitLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles SubmitLogin.Click
        Context.Items.Add("password", Me.password.Text)
        Context.Items.Add("login", Me.login.Text)
        Server.Transfer("~/_user_login.aspx")
    End Sub

    Protected Sub NewAccount_Click(ByVal sender As Object, ByVal e As EventArgs) Handles NewAccount.Click
        Response.Redirect("~/user_new.aspx" & Request.Url.Query)
    End Sub

End Class
