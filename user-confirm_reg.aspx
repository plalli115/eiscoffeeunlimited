﻿<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="user-confirm_reg.aspx.vb" Inherits="user_confirm_reg" 
  CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master" 
  RuntimeMasterPageFile="ThreeColumn.master" CrumbName="<%$ Resources: Language, LABEL_REGISTRATION_CONFIRM %>"%>

<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">  
  <uc1:PageHeader ID="PageHeader1" runat="server" text="<%$ Resources: Language, LABEL_REGISTRATION_CONFIRM %>" EnableTheming="true" />
  <div class="RegistrationConfirfPage"> 
    <asp:Label Text="<%$ Resources: Language, LABEL_REGISTRATION_CONFIRM_INFO %>" runat="server"></asp:Label>
  </div>
</asp:Content>
