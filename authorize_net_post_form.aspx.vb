Imports System.Collections.Generic
Imports System.Configuration.ConfigurationManager
Imports System.IO
Imports ExpandIT
Imports ExpandIT.AuthorizeNet.Authorizenet_Simlib
Imports ExpandIT.Crypto
Imports ExpandIT.ExpandITLib

Partial Class authorize_net_post_form
    Inherits ExpandIT.Page

    Private AuthorizeNet_Login As String
    Private AuthorizeNet_TxnKey As String
    Private AuthorizeNet_ADC_URL As String
    Private OrderDict As ExpDictionary
    Private Shipping As ExpDictionary
    Private payment As ExpDictionary
    Private gUserGuid As String
    Private CollectCardInformation As Boolean

    ' Enable the real URL before going live
    ' This have the same effect as swithing the TestMode constant
    ' Test URL
    Const AuthorizeNet_URL As String = "https://test.authorize.net/gateway/transact.dll"
    ' Live URL
    ' Const AuthorizeNet_URL As String = "https://secure.authorize.net/gateway/transact.dll"

    Const TestMode As Boolean = True

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' AuthorizeNet 
        AuthorizeNet_Login = AppSettings("AUTHORIZENET_LOGIN")
        AuthorizeNet_TxnKey = AppSettings("AUTHORIZENET_TRANSACTION_KEY")
        AuthorizeNet_ADC_URL = EISClass.ShopFullPath & "/_payment.aspx"
        OrderDict = CType(Context.Items("OrderDict"), ExpDictionary)
        Shipping = CType(Context.Items("Shipping"), ExpDictionary)
        payment = CType(Context.Items("Payment"), ExpDictionary)
        gUserGuid = CType(Context.Items("gUserGuid"), String)
    End Sub

    Protected Function HiddenInputField(ByVal aName As Object, ByVal aValue As Object) As String
        Return "<input type=""hidden"" name=""" & aName & """ value=""" & aValue & """>"
    End Function

    Function AuthorizeNet_FormatAmount(ByVal aValue As Decimal) As String
        Dim retval As String

        retval = Replace(FormatNumber(aValue, -1, -1, 0, 0), ",", ".")
        Return retval
    End Function

    Protected Function writeFormCart() As String
        Dim sb As StringBuilder
        sb = New StringBuilder()

        sb.AppendLine("<form id=""payForm"" action=""" & AuthorizeNet_URL & """ method=""post"">")
        sb.AppendLine("<!-- Authorize.Net specific fields -->")
        sb.AppendLine(HiddenInputField("x_ADC_Delim_Data", "FALSE"))
        sb.AppendLine(HiddenInputField("X_Relay_Response", "TRUE"))
        sb.AppendLine(HiddenInputField("x_Relay_URL", AuthorizeNet_ADC_URL))
        sb.AppendLine(HiddenInputField("x_Amount", AuthorizeNet_FormatAmount(OrderDict("TotalInclTax"))))

        sb.AppendLine(HiddenInputField("x_Currency_Code", OrderDict("CurrencyGuid")))
        sb.AppendLine(HiddenInputField("x_Login", AuthorizeNet_Login))
        sb.AppendLine(HiddenInputField("x_Version", "3.0"))

        sb.AppendLine("<!-- Authorize.Net SIM specific fields -->")
        sb.AppendLine(InsertFP(AuthorizeNet_Login, AuthorizeNet_TxnKey, AuthorizeNet_FormatAmount(OrderDict("TotalInclTax")), OrderDict("CustomerReference"), OrderDict("CurrencyGuid")))

        sb.AppendLine("<!-- payment Information -->")
        sb.AppendLine(HiddenInputField("x_Address", HTMLEncode(payment("Address1"))))
        sb.AppendLine(HiddenInputField("x_City", HTMLEncode(payment("CityName"))))
        sb.AppendLine(HiddenInputField("x_State", HTMLEncode(payment("StateName"))))

        If payment("ContactName") <> "" Then
            sb.AppendLine(HiddenInputField("x_Company", HTMLEncode(payment("CompanyName"))))
            Dim strs As String() = split(CStr(payment("ContactName")))
            If strs IsNot Nothing Then
                Dim fName As String = String.Empty
                Dim lName As String = String.Empty
                splitNames(fName, lName, strs)
                sb.AppendLine(HiddenInputField("x_First_Name", HTMLEncode(fName)))
                sb.AppendLine(HiddenInputField("x_Last_Name", HTMLEncode(lName)))
            End If
        Else
            sb.AppendLine(HiddenInputField("x_Last_Name", HTMLEncode(payment("CompanyName"))))
        End If

        If Not IsNull(payment("CountryGuid")) Then
            sb.AppendLine(HiddenInputField("x_Country", payment("CountryGuid")))
        End If

        sb.AppendLine(HiddenInputField("x_Cust_ID", gUserGuid))
        sb.AppendLine(HiddenInputField("x_Phone", payment("PhoneNo")))
        sb.AppendLine(HiddenInputField("x_Zip", payment("ZipCode")))
        sb.AppendLine(HiddenInputField("x_Email", payment("EmailAddress")))

        sb.AppendLine("<!-- Shipping Information -->")
        sb.AppendLine(HiddenInputField("x_Ship_To_Address", HTMLEncode(Shipping("ShipToAddress1"))))
        sb.AppendLine(HiddenInputField("x_Ship_To_City", HTMLEncode(Shipping("ShipToCityName"))))
        sb.AppendLine(HiddenInputField("x_Ship_To_State", HTMLEncode(Shipping("ShipToStateName"))))

        If CStrEx(Shipping("ShipToContactName")) <> "" Then
            sb.AppendLine(HiddenInputField("x_Ship_To_Company", HTMLEncode(Shipping("ShipToCompanyName"))))
            Dim strs As String() = split(CStr(Shipping("ShipToContactName")))
            If strs IsNot Nothing Then
                Dim a As String = String.Empty
                Dim b As String = String.Empty
                splitNames(a, b, strs)
                sb.AppendLine(HiddenInputField("x_Ship_To_First_Name", HTMLEncode(a)))
                sb.AppendLine(HiddenInputField("x_Ship_To_Last_Name", HTMLEncode(b)))
            End If
        Else
            sb.AppendLine(HiddenInputField("x_Ship_To_Last_Name", HTMLEncode(Shipping("ShipToCompanyName"))))
        End If

        If Not IsNull(Shipping("CountryGuid")) Then
            sb.AppendLine(HiddenInputField("x_Ship_To_Country", (Shipping("ShipToCountryGuid"))))
        End If

        sb.AppendLine(HiddenInputField("x_Ship_To_Zip", HTMLEncode(Shipping("ShipToZipCode"))))

        sb.AppendLine("<!-- Order Information -->")
        sb.AppendLine(HiddenInputField("x_Invoice_Num", OrderDict("CustomerReference")))
        sb.AppendLine(HiddenInputField("x_Method", "CC"))
        sb.AppendLine(HiddenInputField("x_Type", "AUTH_ONLY"))

        sb.AppendLine("<!-- ExpandIT Specific -->")
        sb.AppendLine(HiddenInputField("globals.UserGuid", gUserGuid))
        sb.AppendLine(HiddenInputField("PaymentType", "AUTHNET"))

        If TestMode Then
            sb.AppendLine(HiddenInputField("x_Test_Request", "TRUE"))
        End If

        sb.AppendLine("<TABLE border=""0"" width=""400"" cellspacing=""0"">")
        sb.AppendLine("<tr><td colspan=""3"" class=""TDDark"" style=""height: 1px""></td></tr>")
        sb.AppendLine("<tr>")
        sb.AppendLine("<TH class=""THL"" colspan=""3"">" & Resources.Language.LABEL_AUTHORIZE_NET & "</TH>")
        sb.AppendLine("</TR>")
        sb.AppendLine("<tr><td colspan=""3"" class=""TDDark"" STYLE=""HEIGHT: 1px""></td></tr>")
        sb.AppendLine(HiddenInputField("x_show_form", "PAYMENT_FORM"))
        sb.AppendLine("<tr><td colspan=""3"" STYLE=""HEIGHT: 2px""></td></tr>")
        sb.AppendLine("<tr><td colspan=""3"" class=""TDDark"" STYLE=""HEIGHT: 1px""></td></tr>")
        sb.AppendLine("<tr><th colSpan=""3"" STYLE=""HEIGHT: 3px""></th></tr>")
        sb.AppendLine("<tr><td colspan=""3"" class=""TDDark"" STYLE=""HEIGHT: 1px""></td></tr>")
        sb.AppendLine("</TABLE>")
        sb.AppendLine("</FORM>")

        Return sb.ToString()
    End Function

    Private Sub splitNames(ByRef fName As String, ByRef lName As String, ByVal strs As String())

        If strs IsNot Nothing Then
            Dim sbFirstName As New StringBuilder()
            Dim strLastName As String = String.Empty
            Dim strlen As Integer = strs.Length
            If strlen > 1 Then
                For i As Integer = 0 To strlen - 2
                    sbFirstName.Append(strs(i))
                    If i < strlen - 2 Then
                        sbFirstName.Append(" ")
                    End If
                Next
                strLastName = strs(strs.Length - 1)
            ElseIf strs.Length = 1 Then
                sbFirstName.Append(strs(0))
                strLastName = strs(0)
            End If
            fName = sbFirstName.ToString
            lName = strLastName
        End If

    End Sub

    Private Function split(ByVal str As String) As String()
        Try
            Return str.Split(New Char() {CChar(" ")})
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

End Class