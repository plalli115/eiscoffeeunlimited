Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT.ShippingPayment
Imports System.Collections.Generic

Partial Class ShippingProvider
    Inherits ExpandIT.ShippingPayment.ShippingPaymentPage
    'Inherits ShippingMethodPage

    Protected shippingAmounts As ExpDictionary
    Protected returnUrl As String
    Delegate Function callbackDelegate(ByVal arg As String) As String

    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        ' Check the page access.
        eis.PageAccessRedirect("Order")

        ' added this to use web.config file settings "IGNORE_LOCALMODE"

        ' Check for local mode
        If IsLocalMode And Not CBoolEx(AppSettings("IGNORE_LOCALMODE")) Then
            Response.Redirect("localmode.aspx", True)
        End If

        ' Define a delegate that handles the callback call
        Dim delShippingProvider As New callbackDelegate(AddressOf processActionOnCallback)

        ' Set shipprovider delegate to execute processActionOnCallback
        shipprovider.ExecFunction = delShippingProvider

        ' This is the control that should receive and render callback result
        shipprovider.ControlToUpdateOnCallback = shipprovider.FindControl("shipcostlbl")

        ' Set shipproviders radiobuttonlists sqlstatement
        shipprovider.SqlStatement = Me.PossibleShippingProvidersSQLString(False)

        If Not IsPostBack Then

            ' Add the users current shipping provider to ViewState
            ' This value is used to check if another provider where selected
            ' between postback.
            ViewState("oldShippingProvider") = CustomerShippingProvider

            ' If this page was called with an appended querystring, add return url to ViewState
            If Request.QueryString("returl") IsNot Nothing Then
                If Request.UrlReferrer IsNot Nothing Then
                    returnUrl = Request.UrlReferrer.ToString()
                    ViewState("returnUrl") = returnUrl
                End If
            End If

        Else

            ' Find and insert the selected shipping provider into cartheader
            CustomerShippingProvider = shipprovider.SelectedValue

            ' If this page was called with an appended querystring, return url is in ViewState
            ' Else the return url will be calculated by NextUrl
            If ViewState("returnUrl") IsNot Nothing Then
                returnUrl = ViewState("returnUrl")
            Else
                returnUrl = NextUrl
            End If

        End If

        'AM2010080301 - CHECK OUT PROCESS MAP - Start
        If CBoolEx(AppSettings("EXPANDIT_US_USE_CHECK_OUT_PROCESS_MAP")) And Not CheckOutPaths() Is Nothing Then
            If CheckOutPaths.Length > 0 Then
                Master.CheckOutProcess.CheckOutProcessDict = CheckOutPaths()
                Master.CheckOutProcess.currentLocation = getCurrentLocation()
            End If
        End If
        'AM2010080301 - CHECK OUT PROCESS MAP - End

    End Sub

    Protected Overloads Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender

        eis.CalculateOrder()
        '*****shipping*****-start
        If Not CBoolEx(AppSettings("EXPANDIT_US_USE_SHIPPING")) Then
            Me.calculateShippingCosts()
        End If
        '*****shipping*****-end
        Me.Purchase.Text = Server.HtmlDecode(Resources.Language.ACTION_NEXT_GREATER_THAN)

    End Sub

    Protected Sub Purchase_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Purchase.Click
        UpdateOrderDict()
        Response.Redirect(returnUrl)
    End Sub

    ''' <summary>
    ''' Updates Shipping and Handling information on the Cart
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub UpdateOrderDict()

        Dim dictRequest As Object = RequestGetDict()
        Dim shpg As String = Me.shipprovider.RadioButtonListName

        If ViewState("oldShippingProvider") IsNot Nothing Then
            Try
                ' If another shippingProvider was selected
                ' the cart needs to be recalculated
                If CStrEx(dictRequest(shpg)) <> CStrEx(ViewState("oldShippingProvider")) Then
                    If globals.OrderDict Is Nothing Then
                        globals.OrderDict = eis.LoadOrderDictionary(globals.User)
                    End If
                    globals.OrderDict("ShippingHandlingProviderGuid") = CStrEx(dictRequest(shpg))
                    globals.OrderDict("IsCalculated") = 0
                    globals.OrderDict("ReCalculated") = 1
                    eis.CalculateOrder()
                    eis.SaveOrderDictionary(globals.OrderDict)
                End If
            Catch ex As Exception

            End Try

        End If

    End Sub

    ''' <summary>
    ''' Calculate shipping and handlig costs for all availible carriers
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub calculateShippingCosts()
        shippingAmounts = New ExpDictionary()
        Dim dt As System.Data.DataTable = ExpandITLib.SQL2DataTable(Me.PossibleShippingProvidersSQLString(False))
        For i As Integer = 0 To dt.Rows.Count - 1
            shippingAmounts.Add(dt.Rows(i)(1), eis.CalcShippingHandlingOptions(globals.OrderDict, globals.Context, dt.Rows(i)(0)))
        Next
    End Sub

    ''' <summary>
    ''' Processes the callback reqest and returns a HTML-formatted string
    ''' </summary>
    ''' <param name="arg"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function processActionOnCallback(ByVal arg As String) As String
        eis.CalculateOrder()
        '*****shipping*****-start
        If Not CBoolEx(AppSettings("EXPANDIT_US_USE_SHIPPING")) Then
            calculateShippingCosts()
        End If
        '*****shipping*****-end
        Dim index As Integer = 0
        Dim retval As String = String.Empty

        Try
            index = Integer.Parse(arg)
        Catch ex As Exception

        End Try

        Try
            Dim items() As Object = shippingAmounts.DictKeys()
            Dim items2 As Object = shippingAmounts(items(index)).dictKeys()
            Dim sb As New StringBuilder()
            Dim temp As New StringBuilder()
            Dim table As String = "<table>{0}</table>"
            Dim t As String = "<tr><td>{0}</td><td>{1}</td></tr>"
            Dim i As Integer = 0
            Dim currency As String = globals.OrderDict("CurrencyGuid")

            sb.AppendLine("<tr><td><b>" & Resources.Language.LABEL_ESTIMATED_SHIPPING_COST & ":</b></td></tr>")
            'AM2010042601 - ENTERPRISE SHIPPING - Start
            If AppSettings("SHOW_TAX_TYPE") = "INCL" Then
                If CDblEx(shippingAmounts(items(index))("ShippingAmountInclTax")) = 0 Then
                    temp.AppendLine(String.Format(t, Resources.Language.LABEL_SHIPPING, Resources.Language.LABEL_FREE_SHIPPING_HANDLING_VALUE))
                Else
                    temp.AppendLine(String.Format(t, Resources.Language.LABEL_SHIPPING, CurrencyFormatter.FormatCurrency(shippingAmounts(items(index))("ShippingAmountInclTax"), currency)))
                End If
                If CDblEx(shippingAmounts(items(index))("HandlingAmountInclTax")) = 0 Then
                    temp.AppendLine(String.Format(t, Resources.Language.LABEL_HANDLING, Resources.Language.LABEL_FREE_SHIPPING_HANDLING_VALUE))
                Else
                    temp.AppendLine(String.Format(t, Resources.Language.LABEL_HANDLING, CurrencyFormatter.FormatCurrency(shippingAmounts(items(index))("HandlingAmountInclTax"), currency)))
                End If
                If CDblEx(shippingAmounts(items(index))("TotalInclTax")) = 0 Then
                    temp.AppendLine(String.Format(t, Resources.Language.LABEL_SHIPPING_HANDLING_TOTAL, Resources.Language.LABEL_FREE_SHIPPING_HANDLING_VALUE))
                Else
                    temp.AppendLine(String.Format(t, Resources.Language.LABEL_SHIPPING_HANDLING_TOTAL, CurrencyFormatter.FormatCurrency(shippingAmounts(items(index))("TotalInclTax"), currency)))
                End If
            Else
                If CDblEx(shippingAmounts(items(index))("ShippingAmount")) = 0 Then
                    temp.AppendLine(String.Format(t, Resources.Language.LABEL_SHIPPING, Resources.Language.LABEL_FREE_SHIPPING_HANDLING_VALUE))
                Else
                    temp.AppendLine(String.Format(t, Resources.Language.LABEL_SHIPPING, CurrencyFormatter.FormatCurrency(shippingAmounts(items(index))("ShippingAmount"), currency)))
                End If
                If CDblEx(shippingAmounts(items(index))("HandlingAmount")) = 0 Then
                    temp.AppendLine(String.Format(t, Resources.Language.LABEL_HANDLING, Resources.Language.LABEL_FREE_SHIPPING_HANDLING_VALUE))
                Else
                    temp.AppendLine(String.Format(t, Resources.Language.LABEL_HANDLING, CurrencyFormatter.FormatCurrency(shippingAmounts(items(index))("HandlingAmount"), currency)))
                End If
                If CDblEx(shippingAmounts(items(index))("Total")) = 0 Then
                    temp.AppendLine(String.Format(t, Resources.Language.LABEL_SHIPPING_HANDLING_TOTAL, Resources.Language.LABEL_FREE_SHIPPING_HANDLING_VALUE))
                Else
                    temp.AppendLine(String.Format(t, Resources.Language.LABEL_SHIPPING_HANDLING_TOTAL, CurrencyFormatter.FormatCurrency(shippingAmounts(items(index))("Total"), currency)))
                End If
            End If
            'AM2010042601 - ENTERPRISE SHIPPING - End
            sb.AppendLine(temp.ToString())
            retval = String.Format(table, sb.ToString())
        Catch ex As Exception
            retval = "NO VALID SHIPPINGPROVIDER FOUND"
        End Try

        Return retval

    End Function

End Class