<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="info_cc_logo.aspx.vb" Inherits="info_cc_logo" %>
<%
' This page must be available due to danish laws on payment as the information should be shown on
' a page visible on a display with only 640x480 pixels available.

	' Check the page access.
    eis.PageAccessRedirect("Homepage")
%>
<html>
<head>
	<title><% =Resources.Language.LABEL_PAYMENT_CC_HEADING%></title>
</head>
<body>
<h1><% =Resources.Language.LABEL_PAYMENT_CC_HEADING%></h1>
<p>
	<% =Resources.Language.LABEL_PAYMENT_CC_INFO%>
</p>
<p>
	<img src="images/dankort.gif" WIDTH="35" HEIGHT="22" alt="Dankort" />
	<img src="images/eurocard.gif" WIDTH="35" HEIGHT="22" alt="Eurocard" />
	<img src="images/visacard.gif" WIDTH="35" HEIGHT="22" alt="Visacard" />
</p>
</body>
</html>