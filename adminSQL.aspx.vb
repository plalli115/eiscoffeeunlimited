'AM2010050503 - ENTERPRISE ADMIN SECTION SQL QUERIES - Start
Imports System.Configuration.ConfigurationManager
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT.GlobalsClass
Imports ExpandIT
Imports ExpandIT.ExpandITLib

Partial Class adminSQL
    Inherits ExpandIT.Page

    Const AllowedLogins As String = "mfrabottaCSR"

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        eis.PageAccessRedirect("HomePage")
        If Not (CBoolEx(globals.User("B2B")) Or CBoolEx(globals.User("B2C"))) Then ShowLoginPage()
        AccessToAdminSection(globals.User("UserLogin"))
    End Sub

    Public Sub AccessToAdminSection(ByVal login As String)
        Dim redirectpage As String
        Dim thePath As String

        If Not isLoginAllowed(login) Then
            If HttpContext.Current.Request.ServerVariables("QUERY_STRING") <> "" Then
                thePath = HttpContext.Current.Request.ServerVariables("SCRIPT_NAME") & "?" & HttpContext.Current.Request.ServerVariables("QUERY_STRING")
            Else
                thePath = HttpContext.Current.Request.ServerVariables("SCRIPT_NAME")
            End If

            redirectpage = VRoot & "/" & AppSettings("URL_NOACCESS") & "?AccessClass=AdminSection&returl=" & HttpContext.Current.Server.UrlEncode(CStrEx(thePath))
            HttpContext.Current.Response.Redirect(redirectpage)
        End If
    End Sub

    Public Function isLoginAllowed(ByVal login As String)
        Dim validLogins As String()
        Dim cont As Integer

        validLogins = AllowedLogins.Split("|")

        If validLogins.Length > 0 Then
            For cont = 0 To validLogins.Length - 1 Step 1
                If login = validLogins(cont) Then
                    Return True
                End If
            Next
        End If
        Return False
    End Function

    Protected Sub btnAccept_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAccept.Click
        Dim query As String
        Dim message As String = ""

        Me.lblMessage.Text = ""
        query = CStrEx(Me.txtSQLQuery.Text)

        If query <> "" Then
            If Me.cbxGetResult.Checked Then
                Try
                    Me.SQLdsResult.SelectCommand = query
                    Me.SQLdsResult.DataBind()
                    grvResult.DataBind()
                    Me.lblMessage.Visible = False
                    Me.pnlResult.Visible = True
                Catch ex As Exception
                    Me.lblMessage.Text = ex.ToString()
                    Me.lblMessage.Visible = True
                    Me.pnlResult.Visible = False
                End Try
            Else
                message = CStrEx(excecuteNonQueryDb(query))
                If message <> "" Then
                    Me.lblMessage.Text = message
                    Me.lblMessage.Visible = True
                    Me.pnlResult.Visible = False
                End If
            End If
        End If
    End Sub
End Class
'AM2010050503 - ENTERPRISE ADMIN SECTION SQL QUERIES - End
