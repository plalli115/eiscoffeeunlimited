﻿<%--JA2010022301 - New Payment Methods - Start--%>
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="DebitNote.aspx.vb" Inherits="DebitNote" 
    CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master" 
    RuntimeMasterPageFile="ThreeColumn.master" CrumbName="<%$ Resources: Language, LABEL_GIROINFO %>" %>
    
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Import Namespace="System.Configuration.ConfigurationManager" %>

<%--<%@ Register Src="~/controls/StepIndicator.ascx" TagName="StepIndicator" TagPrefix="uc2"  %>--%>
<%@ Register Src="~/controls/BankAccount.ascx" TagName="BankAccountControl" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <%--<uc2:StepIndicator ID="StepIndicator1" runat="server" NumOfSteps="6" CurrStep="5" />--%>
    <div class="UserNewPage">
      <uc2:BankAccountControl ID="BankAccount" runat="server" />
    </div>
</asp:Content>
<%--JA2010022301 - New Payment Methods - End--%>
