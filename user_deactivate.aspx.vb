﻿Imports ExpandIT.ExpandITLib
Imports System.Configuration.ConfigurationManager
Imports ExpandIT31.ExpanditFramework.Util
Imports ExpandIT31.ExpanditFramework.BLLBase
Imports ExpandIT31.ExpanditFramework.Infrastructure
'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
Imports ExpandIT
'AM2011031801 - ENTERPRISE CSR STAND ALONE - End

Partial Class user_deactivate
    Inherits ExpandIT.Page

    Private Const UPDATE_SQL_TEMPL As String = "UPDATE UserTable SET Status = {0:d} WHERE UserGuid={1}"



    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	
	Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If globals.User("Anonymous") Then
            UserDeactivatingPage.Style.Add("display", "none")
            Dim status As String = Request.Params("Status")
            If Not String.IsNullOrEmpty(status) AndAlso status = "OK" Then
                Me.PageHeader1.Text = Resources.Language.LABEL_USER_ACCOUNT_DEACTIVATED
                Me.CrumbName = Resources.Language.LABEL_USER_ACCOUNT_DEACTIVATED
                globals.messages.Messages.Add(Resources.Language.LABEL_USER_ACCOUNT_DEACTIVATED_INFO)
            End If
        Else
            globals.messages.Warnings.Add(Resources.Language.LABEL_USER_ACCOUNT_DEACTIVATING_INFO)
        End If
    End Sub



    Protected Sub continueButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles continueButton.Click
        If confirmCheckBox.Checked AndAlso DeactivateCurrentAccount() Then
            Response.Redirect(Request.CurrentExecutionFilePath & "?Status=OK")
        End If

    End Sub

    Private Function DeactivateCurrentAccount() As Boolean
        Dim result As Boolean = False
        If Not globals.User("Anonymous") Then
            Dim userGuid As String = globals.User("UserGuid")
            Dim sql As String = String.Format(UPDATE_SQL_TEMPL, UserBase.UserStates.Deactivated, Utilities.SafeString(userGuid))
            If String.IsNullOrEmpty(DataAccess.excecuteNonQueryDb(sql)) Then
                makeLogout()
                result = True
            End If
        End If
        Return result
    End Function

    Private Sub makeLogout()
        ' Delete users cookie
        Response.Cookies("store")("UserGuid") = ""
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
        If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
            Dim csrObj As New USCSR(globals)
            csrObj.setCSR2Cookie()
        End If
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
        Response.Cookies("store").Path = HttpContext.Current.Server.UrlPathEncode(VRoot) & "/"
        eis.UpdateMiniCartInfo(Nothing)
    End Sub

    Public Function findNamedControl(ByVal Parent As Control, ByVal name As String, ByRef c As Control) As Control
        Dim child As Control = Parent
        For Each child In Parent.Controls
            If child.ID = name Then
                c = child
            Else
                findNamedControl(child, name, c)
            End If
        Next
        Return c
    End Function
End Class
