<%@ Import Namespace="System.Configuration.ConfigurationManager" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="user_typelost_password.aspx.vb"
    Inherits="user_typelost_password" CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master"
    RuntimeMasterPageFile="ThreeColumn.master" CrumbName="<%$ Resources: Language, LABEL_I_FORGOT_MY_PASSWORD %>" %>

<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <asp:Panel runat="server" ID="lostPasswordPanel" DefaultButton="cmdSubmitPwd">
        <div class="UserTypeLostPassword">
            <uc1:pageheader id="PageHeader1" runat="server" text="<%$ Resources: Language, LABEL_I_FORGOT_MY_PASSWORD %>"
                enabletheming="true" />
            <p>
                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources: Language, MESSAGE_DID_YOU_FORGET_YOUR_EMAIL_THEN_RECIEVE_A_NEW %>"></asp:Literal>
            </p>
            <input type="hidden" name="ACTION" value="LOSTPWD" />
            <input type="hidden" name="returl" value="<% = Request("returl") %>" />
            <asp:Literal runat="server" Text='<%$ Resources: Language, LABEL_EMAIL_ADDRESS %>'></asp:Literal>
            <input type="text" class="InpL" name="LOSTPWDEMAIL" id="LOSTPWDEMAIL" size="30" /><br />
            &nbsp;<br />
            <asp:Button runat="server" PostBackUrl="user_lost_password.aspx" CssClass="AddButton"
                ID="cmdSubmitPwd" Text='<%$Resources: Language, ACTION_SUBMIT%>' />
        </div>
    </asp:Panel>
    <!-- Start of script block code for setting cursor to the User ID field -->
    <%  SetFocusOnElement_OnLoad("LOSTPWDEMAIL")%>
    <!-- End of script block code for setting cursor to the User ID field -->
</asp:Content>
