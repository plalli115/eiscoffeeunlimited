Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT.GlobalsClass
Imports System.Web
Imports System.Globalization

Partial Class history_ledgerentry
    Inherits ExpandIT.Page

    Protected AgingTitles(3), AgingAmounts(3)
    Protected strBody, dictItem, CustomerGuid As Object
    Protected dictCuLeEn As ExpDictionary
    Protected Group As ExpDictionary
    Protected objPage As New PagedDataSource()

    Protected page1 As String = "0"
    Protected page2 As String = "0"
    Protected page3 As String = "0"

    Public Property CurrentPage() As Integer
        Get
            If (Me.ViewState("CurrentPage") Is Nothing) Then
                Return 0
            Else
                Return Integer.Parse(Me.ViewState("CurrentPage").ToString())
            End If
        End Get

        Set(ByVal value As Integer)
            Me.ViewState("CurrentPage") = value
        End Set
    End Property

    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	
	Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            Dim cultureName As String = Threading.Thread.CurrentThread.CurrentUICulture.Name
            Dim LCID As Integer = CultureInfo.GetCultureInfo(cultureName).TextInfo.ANSICodePage
            Session.CodePage = LCID
        Catch ex As Exception
            Session.CodePage = 1252
        End Try

        ' Check the page access.
        eis.PageAccessRedirect("Order")
        If Not (globals.User("B2B") AndAlso CIntEx(globals.User("UserType")) = 1) Then ShowPage("account.aspx")

        ' DEBUG INFORMATION COLLECTION
        If globals.DebugLogic Then
            DebugValue(globals.User, "User")
            DebugValue(dictCuLeEn, "CustomerLedgerEntries: " & CustomerGuid)
        End If


        If CStrEx(page1) = "0" Then
            page1 = CStrEx(AppSettings("Page_1"))
        End If
        If CStrEx(page2) = "0" Then
            page2 = CStrEx(AppSettings("Page_2"))
        End If
        If CStrEx(page3) = "0" Then
            page3 = CStrEx(AppSettings("Page_3"))
        End If


    End Sub

    Protected Overloads Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender

        If Not Page.IsPostBack Then
            Dim list1 As New ListItem(page1, page1, True)
            Me.ddlPageSize.Items.Add(list1)
            Dim list2 As New ListItem(page2, page2)
            Me.ddlPageSize.Items.Add(list2)
            Dim list3 As New ListItem(page3, page3)
            Me.ddlPageSize.Items.Add(list3)
            Dim all As New ListItem("All", "All")
            Me.ddlPageSize.Items.Add(all)


            Me.ddlPageSize2.Items.Add(list1)
            Me.ddlPageSize2.Items.Add(list2)
            Me.ddlPageSize2.Items.Add(list3)
            Me.ddlPageSize2.Items.Add(all)
        End If

        BindData()

    End Sub

    Private Sub BindData()

        ' Load Ledger entries
        dictCuLeEn = b2b.GetCustomerLedgerEntries(globals.User)

        ' Prepare stylesheet counter
        eis.InitNextRowAB()


        Group = dictCuLeEn("Lines")

        objPage.DataSource = Group

        If Not Group Is Nothing Then
            objPage.AllowPaging = True
            If CStrEx(ddlPageSize.SelectedValue) = "All" Then
                objPage.PageSize = Group.Count
                ddlPageSize2.SelectedValue = "All"
            Else
                objPage.PageSize = Integer.Parse(ddlPageSize.SelectedValue)
                ddlPageSize2.SelectedValue = Integer.Parse(ddlPageSize.SelectedValue)
            End If
            If CStrEx(ddlPageSize2.SelectedValue) = "All" Then
                objPage.PageSize = Group.Count
                ddlPageSize.SelectedValue = "All"
            Else
                objPage.PageSize = Integer.Parse(ddlPageSize2.SelectedValue)
                ddlPageSize.SelectedValue = Integer.Parse(ddlPageSize2.SelectedValue)
            End If
            If CurrentPage < 0 Or CurrentPage >= objPage.PageCount Then
                CurrentPage = 0
            End If
            objPage.CurrentPageIndex = CurrentPage
            lnkbtnNext.Visible = Not objPage.IsLastPage
            lnkbtnPrevious.Visible = Not objPage.IsFirstPage
            lnkbtnNext2.Visible = Not objPage.IsLastPage
            lnkbtnPrevious2.Visible = Not objPage.IsFirstPage
            pricelist.DataSource = objPage
            pricelist.DataBind()
            Me.txtPage.Text = CurrentPage + 1
            Me.txtPage2.Text = CurrentPage + 1
            Me.lblNumPages.Text = Resources.Language.LABEL_PAGINATION_OF & " " & objPage.PageCount
            Me.lblNumPages2.Text = Resources.Language.LABEL_PAGINATION_OF & " " & objPage.PageCount
        End If


        'OverDUE Dates
        Dim PeriodStart As Date
        Dim PeriodEnd As Date
        Dim PeriodIndex, SQL
        AgingTitles(0) = Resources.Language.LABEL_LEDGER_ENTRY_OVERDUE_AMOUNTS_NOT_YET_DUE
        AgingTitles(1) = Resources.Language.LABEL_LEDGER_ENTRY_OVERDUE_AMOUNTS_1_30DAYS
        AgingTitles(2) = Resources.Language.LABEL_LEDGER_ENTRY_OVERDUE_AMOUNTS_31_60DAYS
        AgingTitles(3) = Resources.Language.LABEL_LEDGER_ENTRY_OVERDUE_AMOUNTS_OVER_60DAYS

        PeriodStart = DateTime.Now.Date

        For PeriodIndex = 0 To 3
            Select Case PeriodIndex

                Case 0
                    SQL = "SELECT Sum(RemainingAmountLCY) AS TotalDue FROM CustomerLedgerEntry" & _
                            " WHERE CustomerGuid = " & SafeString(dictCuLeEn("CustomerGuid")) & _
                            " AND DueDate >= " & SafeString(PeriodStart)
                Case 3
                    PeriodEnd = PeriodStart.AddDays(-1)

                    SQL = "SELECT Sum(RemainingAmountLCY) AS TotalDue FROM CustomerLedgerEntry" & _
                          " WHERE CustomerGuid = " & SafeString(dictCuLeEn("CustomerGuid")) & _
                          " AND DueDate <= " & SafeString(PeriodEnd)
                Case Else

                    PeriodEnd = PeriodStart.AddDays(-1)
                    PeriodStart = PeriodStart.AddDays(-30)
                    SQL = "SELECT Sum(RemainingAmountLCY) AS TotalDue FROM CustomerLedgerEntry" & _
                          " WHERE CustomerGuid = " & SafeString(dictCuLeEn("CustomerGuid")) & _
                          " AND DueDate >= " & SafeString(PeriodStart) & _
                          " AND DueDate <= " & SafeString(PeriodEnd)
            End Select

            AgingAmounts(PeriodIndex) = CDblEx(getSingleValueDB(SQL))
        Next

    End Sub

    Sub dlPaging_ItemCommand(ByVal sender As Object, ByVal e As DataListCommandEventArgs)

        If (e.CommandName.Equals("lnkbtnPaging")) Then
            CurrentPage = Integer.Parse(e.CommandArgument.ToString())
            BindData()
        End If

    End Sub

    Sub dlPaging_ItemDataBound(ByVal sender As Object, ByVal e As DataListItemEventArgs)

        Dim lnkbtnPage = CType(e.Item.FindControl("lnkbtnPaging"), LinkButton)
        If (lnkbtnPage.CommandArgument.ToString() = CurrentPage.ToString()) Then
            lnkbtnPage.Enabled = False
            lnkbtnPage.Font.Bold = True
        End If
    End Sub
    Sub lnkbtnPrevious_Click(ByVal sender As Object, ByVal e As EventArgs)

        CurrentPage -= 1
        BindData()

    End Sub
    Sub lnkbtnPrevious2_Click(ByVal sender As Object, ByVal e As EventArgs)

        CurrentPage -= 1
        BindData()

    End Sub

    Sub txtPage_TextChanged(ByVal sender As Object, ByVal e As EventArgs)

        CurrentPage = txtPage.Text - 1
        BindData()

    End Sub
    Sub txtPage2_TextChanged(ByVal sender As Object, ByVal e As EventArgs)

        CurrentPage = txtPage.Text - 1
        BindData()

    End Sub

    Sub lnkbtnNext_Click(ByVal sender As Object, ByVal e As EventArgs)

        CurrentPage += 1
        BindData()

    End Sub

    Sub lnkbtnNext2_Click(ByVal sender As Object, ByVal e As EventArgs)

        CurrentPage += 1
        BindData()

    End Sub

    Protected Sub ddlPageSize_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
        CurrentPage = 0
        BindData()
    End Sub

    Protected Sub ddlPageSize2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPageSize2.SelectedIndexChanged
        CurrentPage = 0
        BindData()
    End Sub

    Protected Function getCheckbox(ByVal check As Boolean)
        If check Then
            Return "checked=""checked"""
        Else
            Return ""
        End If
    End Function

    Protected Function getImage(ByVal Description As String, ByVal DocumentGuid As String, ByVal DocumentType As String)
        strBody = "PrepareEmail('"
        If CStrEx(Description) = "" Then Description = "?"
        If CStrEx(DocumentGuid) = "" Then DocumentGuid = "?"
        strBody = strBody & Resources.Language.MESSAGE_CUSTOMER_LEDGER_EMAIL_REQUEST_BODY
        strBody = Replace(strBody, "%LEDGER_ENTRY_NO%", CStrEx(DocumentGuid))
        strBody = Replace(strBody, "%LEDGER_ENTRY_TYPE%", b2b.GetDocumentTypeDescription(CLngEx(DocumentType)))
        strBody = Replace(strBody, "%LEDGER_ENTRY_DESCRIPTION%", CStrEx(Description))
        strBody = Replace(strBody, "%USER_INFO%", globals.User("ContactName") & _
        "%NEWLINE%" & globals.User("CompanyName") & "%NEWLINE%" & globals.User("Address1") & _
        "%NEWLINE%" & globals.User("ZipCode") & " " & globals.User("CityName") & _
        "%NEWLINE%" & Resources.Language.LABEL_CUSTOMER_NO & ":" & globals.User("CustomerGuid"))
        strBody = Replace(strBody, "%NEWLINE%", AppSettings("CUSTOMER_LEDGER_NEWLINE_CHARS"))
        strBody = strBody & "')"
        Return strBody

    End Function

    'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - Start
    Protected Function isDrillDown(ByVal DocumentType As Integer) As Boolean
        Return getSingleValueDB("SELECT COUNT(*) FROM [CustomerLedgerDrillDownSetup] WHERE [DocumentType]=" & CIntEx(DocumentType)) > 0
    End Function

    'AM2010091001 - ONLINE PAYMENTS - Start
    Protected Function drillDownLink(ByVal DocumentType As Integer, ByVal DocumentGuid As String, ByVal IsOpen As Boolean, ByVal Amount As Double) As String
        Dim link As String = ""

        'JA2011030701 - PAYMENT TABLE - START
        'Dim sql As String = "SELECT SUM(PaymentTransactionAmount) AS PaidAmount FROM EEPGOnlinePayments WHERE DocumentGuid = " & SafeString(DocumentGuid)
        Dim sql As String = "SELECT SUM(PaymentTransactionAmount) AS PaidAmount FROM PaymentTable WHERE DocumentGuid = " & SafeString(DocumentGuid)
        'JA2011030701 - PAYMENT TABLE - END

        If CBoolEx(AppSettings("EXPANDIT_US_USE_EEPG_ONLINE_PAYMENTS")) And IsOpen And CDblEx(getSingleValueDB(sql)) < Amount And (DocumentType = 2 Or DocumentType = 7) Then
            link = "history_detail.aspx?OnlinePayments=1&DrillDownType=" & CIntEx(DocumentType) & "&DrillDownGuid=" & CStrEx(DocumentGuid)
        Else
            link = "history_detail.aspx?DrillDownType=" & CIntEx(DocumentType) & "&DrillDownGuid=" & CStrEx(DocumentGuid)
        End If
        'AM2010091001 - ONLINE PAYMENTS - End

        Return link
    End Function
    'AM2010080501 - DRILL DOWN ON CUSTOMER LEDGER ENTRIES - End

End Class
