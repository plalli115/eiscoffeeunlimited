Imports System.Configuration.ConfigurationManager
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT
Imports ExpandIT.ExpandITLib
Imports System.Web

Partial Class PromoCodes
    Inherits ExpandIT.ShippingPayment._CartPage

    Protected PromoDict As ExpDictionary

    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	
	Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sql As String = "SELECT * FROM Promotion WHERE (GETDATE() BETWEEN PromotionStartDate AND PromotionEndDate)"
        PromoDict = SQL2Dicts(sql)
        If Not PromoDict Is Nothing And Not PromoDict Is DBNull.Value Then
            If PromoDict.Count > 0 Then
                Me.ShowPromoCodes1.PromoDict = PromoDict
                Me.litContent.Visible = False
            Else
                Me.litContent.Visible = True
            End If
        Else
            Me.litContent.Visible = True
        End If
    End Sub
End Class
