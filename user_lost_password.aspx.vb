Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT.GlobalsClass

Partial Class user_lost_password
    Inherits ExpandIT.Page

    Protected bSuccessFullySendEmail As Boolean = False
    Protected strSuccessMessage As String
    Protected UserError As ExpDictionary

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim dictRequest As ExpDictionary

        UserError = New ExpDictionary

        ' Collecting information
        dictRequest = RequestGetDict()
        If dictRequest("ACTION") IsNot Nothing Then
            If dictRequest("ACTION") = "LOSTPWD" Then
                ' Require a new email adress.
                bSuccessFullySendEmail = eis.ForgottenPassword(dictRequest("LOSTPWDEMAIL"))
            End If
        End If

        ' Message on successfull mail send
        If bSuccessFullySendEmail Then
            globals.messages.Actions.Add(Replace(Resources.Language.MESSAGE_LOST_PASSWORD_SUCCCESSFULLY_SEND, "%EMAIL_ADDRESS%", dictRequest("LOSTPWDEMAIL")))
            PanelBack.Visible = False
        End If

        ' Showing debug information
        If globals.DebugLogic Then
            Response.Write("Retval from sending email: " & bSuccessFullySendEmail & "<br />")
            DebugValue(UserError, "UserError, Additional error messages.")
            DebugValue(dictRequest, "Request object")
        End If
    End Sub
End Class
