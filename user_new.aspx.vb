Imports System.Configuration.ConfigurationManager
Imports ExpandIT.EISClass
Imports ExpandIT.GlobalsClass
Imports ExpandIT
Imports ExpandIT.Debug
Imports ExpandIT.ExpandITLib
Imports System.Web

Partial Class user_new
    Inherits ExpandIT.Page

    ' added this to use web.config file settings "IGNORE_LOCALMODE"

    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	
	Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)
        ' Check for local mode
        If IsLocalMode And Not CBoolEx(AppSettings("IGNORE_LOCALMODE")) Then
            Response.Redirect("localmode.aspx", True)
        End If
        ' Check the page access.
        eis.PageAccessRedirect("OpenSite")
    End Sub

    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)
        Dim myControl As Control = Me.LoadControl("~/controls/user/Controls_ExpandITUser.ascx")
        myControl.ID = "myControl"
        Me.PlaceHolder1.Controls.Clear()
        Me.PlaceHolder1.Controls.Add(myControl)
    End Sub

End Class
