'AM2010050502 - ENTERPRISE ADMIN SECTION - Start
Imports System.Configuration.ConfigurationManager
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT.GlobalsClass
Imports ExpandIT
Imports ExpandIT.ExpandITLib

Partial Class adminSection
    Inherits ExpandIT.Page

    Const AllowedLogins As String = "mfrabottaCSR"

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        eis.PageAccessRedirect("HomePage")
        If Not (CBoolEx(globals.User("B2B")) Or CBoolEx(globals.User("B2C"))) Then ShowLoginPage()
        AccessToAdminSection(globals.User("UserLogin"))
    End Sub

    Public Sub AccessToAdminSection(ByVal login As String)
        Dim redirectpage As String
        Dim thePath As String

        If Not isLoginAllowed(login) Then
            If HttpContext.Current.Request.ServerVariables("QUERY_STRING") <> "" Then
                thePath = HttpContext.Current.Request.ServerVariables("SCRIPT_NAME") & "?" & HttpContext.Current.Request.ServerVariables("QUERY_STRING")
            Else
                thePath = HttpContext.Current.Request.ServerVariables("SCRIPT_NAME")
            End If

            redirectpage = VRoot & "/" & AppSettings("URL_NOACCESS") & "?AccessClass=AdminSection&returl=" & HttpContext.Current.Server.UrlEncode(CStrEx(thePath))
            HttpContext.Current.Response.Redirect(redirectpage)
        End If
    End Sub

    Public Function isLoginAllowed(ByVal login As String)
        Dim validLogins As String()
        Dim cont As Integer

        validLogins = AllowedLogins.Split("|")

        If validLogins.Length > 0 Then
            For cont = 0 To validLogins.Length - 1 Step 1
                If login = validLogins(cont) Then
                    Return True
                End If
            Next
        End If
        Return False
    End Function

End Class
'AM2010050502 - ENTERPRISE ADMIN SECTION - End
