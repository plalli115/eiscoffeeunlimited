<%--AM2011031801 - ENTERPRISE CSR STAND ALONE - Start--%>
<%@ Import Namespace="System.Configuration.ConfigurationManager" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CSRNotes.aspx.vb" Inherits="CSRNotes"
    CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master"
    RuntimeMasterPageFile="ThreeColumn.master" CrumbName="<%$ Resources: Language, LABEL_CSR_NOTES %>" %>

<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <div class="HistoryPage">
        <uc1:PageHeader ID="PageHeader1" runat="server" Text="<%$ Resources: Language, LABEL_CSR_NOTES %>"
            EnableTheming="true" />
        <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="Server">
            <ContentTemplate>
                <asp:Panel ID="pnlCSRNotes" runat="server" DefaultButton="btnSaveNote">
                    <table>
                        <tr>
                            <td style="width: 52px; vertical-align: middle;">
                                <%=Resources.Language.LABEL_CSR_NOTES_NEW%>
                            </td>
                            <td style="width: 448px;">
                                <asp:TextBox ID="txbNewNote" runat="server" Width="100%"></asp:TextBox>
                            </td>
                            <td style="width: 45px; text-align: right;">
                                <asp:Button ID="btnSaveNote" runat="server" Text="<%$ Resources: Language, LABEL_CSR_NOTES_ADD_NOTE %>" />
                            </td>
                        </tr>
                    </table>
                    <br />
                    <asp:DataList runat="server" ID="dlNotes">
                        <HeaderTemplate>
                            <table cellpadding="0" cellspacing="0" border="1">
                        <tr>
                                    <td style="width:25%;" align="center"><%=Resources.Language.LABEL_CSR_NOTES_DATE %></td>
                                    <td style="width:25%;" align="center"><%=Resources.Language.LABEL_CSR_NOTES_CSR_NAME %></td>
                                    <td style="width:50%;" align="center"><%=Resources.Language.LABEL_CSR_NOTES_MESSAGE %></td>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td align="center"><%# Eval("Value")("CreationDate")%></td>
                                <td align="center"><%#Eval("Value")("ContactName")%></td>
                                <td align="left"><%#Eval("Value")("Note")%></td>
                        </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table></FooterTemplate>
                    </asp:DataList>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
<%--AM2011031801 - ENTERPRISE CSR STAND ALONE - End--%>