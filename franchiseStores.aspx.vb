'JA2011021701 - STORE LOCATOR - Start
Imports System.Configuration.ConfigurationManager
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT
Imports ExpandIT.ExpandITLib
Imports System.Web

Partial Class franchiseStores
    Inherits ExpandIT.ShippingPayment._CartPage

    Protected PointsDict As ExpDictionary
    Protected franchiseInfo As ExpDictionary
    Protected info As String = ""
    Protected theLat As Double
    Protected theLon As Double
    Protected address As String = ""

    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	
	Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If CBoolEx(AppSettings("SHOW_STORE_LOCATOR")) Then
            Dim sql As String = "SELECT * FROM FranchiseLocation"
            PointsDict = SQL2Dicts(sql)

            Dim franchiseGuid As String = CStrEx(Request("guid"))
            sql = "SELECT * FROM FranchiseLocation WHERE FranchiseGuid=" & SafeString(franchiseGuid)
            franchiseInfo = SQL2Dicts(sql)
            For Each item As ExpDictionary In franchiseInfo.Values
                theLat = CDblEx(item("Latitude"))
                theLon = CDblEx(item("Longitude"))
                address = CStrEx(item("Address1"))
                Exit For
            Next

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "script1", "SetLocation('" & address & "');", True)
        Else
            Response.Redirect(VRoot & "/shop.aspx")
        End If
    End Sub



End Class
'JA2011021701 - STORE LOCATOR - End
