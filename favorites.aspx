<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="favorites.aspx.vb" Inherits="favorites"
    CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master" 
    RuntimeMasterPageFile="ThreeColumn.master" CrumbName="<%$ Resources: Language, LABEL_FAVORITES %>" %>

<%@ Register Src="~/controls/FavoriteList.ascx" TagName="FavoriteList" TagPrefix="uc1" %>
<%@ Register Src="~/controls/GroupHeader.ascx" TagName="GroupHeader" TagPrefix="uc1" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<%@ Register src="~/controls/Message.ascx" tagname="Message" tagprefix="uc1" %>
<asp:content id="Content1" contentplaceholderid="cphSubMaster" runat="Server">
    <div class="FavoritesPage" >
        
        <script src="<%= Vroot %>/script/Geometry.js" type="text/javascript"></script>

        <script src="<%= Vroot %>/script/tooltip.js" type="text/javascript"></script>

        <script src="<%= Vroot %>/script/addToCart.js" type="text/javascript"></script>
    
        <uc1:Message ID="Message1" runat="server" />
        <uc1:PageHeader ID="PageHeader1" runat="server" text="<%$ Resources: Language, LABEL_FAVORITES %>" EnableTheming="true" />
        <uc1:FavoriteList ID="FavoriteList1" runat="server" EnableTheming="true" />
    </div>
</asp:content>
