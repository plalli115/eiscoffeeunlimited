<%@ Page Language="VB" AutoEventWireup="true" CodeFile="Search.aspx.vb" Inherits="Search"
    ValidateRequest="false" CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master"
    EnableEventValidation="false" RuntimeMasterPageFile="ThreeColumn.master" CrumbName="<%$ Resources: Language, LABEL_MENU_SEARCH %>" %>

<%@ Import Namespace="ExpandIT" %>
<%@ Register Src="controls/AddToCartButton.ascx" TagName="AddToCartButton" TagPrefix="uc1" %>
<%@ Register Src="~/controls/search/CategorySearchControl.ascx" TagName="CategorySearchResult"
    TagPrefix="uc1" %>
<%@ Register Src="~/controls/SearchResultList.ascx" TagName="SearchResultList" TagPrefix="uc1" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <div class="SearchPage">
        <uc1:PageHeader ID="PageHeader1" runat="server" Text="<%$ Resources: Language, LABEL_MENU_SEARCH %>"
            EnableTheming="true" />
        <uc1:CategorySearchResult ID="CategorySearchResult" runat="server" />
        <asp:Panel ID="searchpanel" runat="server" DefaultButton="btnSearch">            
             <%= searchText() %>
            &nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="btnSearch" runat="server" CssClass="AddButton" Text="<%$ Resources: Language, ACTION_SEARCH %>" PostBackUrl="~/search.aspx" />
        </asp:Panel>
        <br class="BoxSeperator" />
        <uc1:SearchResultList ID="SearchResultList" runat="server" />
    </div>
</asp:Content>
