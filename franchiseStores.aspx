<%--JA2011021701 - STORE LOCATOR - Start--%>
<%@ Import Namespace="System.Configuration.ConfigurationManager" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="franchiseStores.aspx.vb"
    Inherits="franchiseStores" CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master"
    RuntimeMasterPageFile="TwoColumn.master" %>

<%@ Register Src="controls/Box.ascx" TagName="Box" TagPrefix="uc2" %>
<%@ Register Src="controls/LoginStatus.ascx" TagName="LoginStatus" TagPrefix="uc3" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Register Src="~/controls/MostPopular.ascx" TagName="MostPopular" TagPrefix="uc1" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<%@ Register Src="~/controls/Message.ascx" TagName="Message" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <asp:Panel ID="Panel1" runat="server">

        <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=true&amp;key=<%= AppSettings("STORE_LOCATOR_GOOGLE_MAPS_KEY") %>"
            type="text/javascript"></script>

        <table style="width: 100%;">
            <tr>
                <td colspan="2" style="padding-bottom: 20px; padding-top: 20px;">
                    <asp:Label ID="Label1" Style="font-size: 14px; font-weight: bold; color: Black;"
                        runat="server" Text="<%$ Resources: Language, LABEL_MAPS_LOCATION  %>"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <div id="regularMap" style="position: relative; top: 0px; left: 0px; background-color: White;">
                        <%If Not franchiseInfo Is Nothing Then%>
                        <table>
                            <%For Each item As ExpDictionary In franchiseInfo.Values%>
                            <tr>
                                <td colspan="3" align="center" style="padding-bottom: 15px; padding-top: 15px;">
                                    <img alt="" src="<%=Vroot %>/catalog/images/Franchises/<%= Cstrex(item("Picture1")) %>" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="center" style="padding-bottom: 15px; padding-top: 15px;">
                                    <img alt="" src="<%=Vroot %>/catalog/images/Franchises/<%= Cstrex(item("Picture2")) %>" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="padding-bottom: 15px; padding-top: 15px;">
                                    <asp:Label ID="Label2" runat="server" Text="<%$ Resources: Language, LABEL_FRANCHISE_TEXT  %>"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="padding-bottom: 15px; padding-top: 15px;">
                                    <%Dim arrHours As Object() = CStrEx(item("Hours")).Split("|")%>
                                    <%For cont As Integer = 0 To arrHours.Length - 1 Step 1%>
                                    <%=Cstrex(arrHours(cont))%>
                                    <br />
                                    <%Next%>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" style="padding-bottom: 15px; padding-top: 15px;">
                                    <a href="<%=item("EmailClubLink") %>">
                                        <%=Resources.Language.LABEL_FRANCHISE_EMAIL_CLUB_LINK%>
                                    </a>
                                </td>
                                <td style="width: 200px;">
                                </td>
                                <td align="center" style="padding-bottom: 15px; padding-top: 15px;">
                                    <a href="<%=item("ApplyWorkLink") %>">
                                        <%=Resources.Language.LABEL_FRANCHISE_APPLY_WORK_LINK%>
                                    </a>
                                </td>
                            </tr>
                            
                            <%info = "<b>" & item("FranchiseName") & "</b><br />" & item("Address1") & "<br />" & item("City") & "<br />" & item("StateName") & "<br />" & item("ZipCode") & "<br />" & item("Phone") & "<br />"%>
                            <%Exit For%>
                            <%Next%>
                        </table>
                        <%End If%>
                    </div>
                </td>
                <td>
                    <table>
                        <tr>
                            <td>
                                <input id="geocodeInput" value="<%=address %>" onkeypress="stopRKey(event);" style="height: 15px; width: 145px;display:none;"
                                    name="geocodeInput" type="text" />
                                <%--<asp:Button ID="btnSearch" runat="server" Text="Search" /> --%>
                                <input id="Button1" name="Button1" runat="server" type="button" style="display:none;" value="Send" onclick="setMyMap();" />
                                <a style="font-size:14px;" href="<%=Vroot %>/mapShow.aspx">Find another location</a>
                                
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span id="Radius" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top: 50px;">
                                <span id="ShowInfo" style="display: none;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div id="international" style="position: relative; top: 10px; left: 3px;">
                                    <table>
                                        <tr>
                                            <td style="padding-bottom: 20px;">
                                                <%=info %>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div id="map" style="width: 250px; height: 200px;">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <input id="vroot" value="<%=Vroot %>" type="hidden" />

        <script type="text/javascript">

        var vroot=document.getElementById('vroot').value;

        var map = new GMap2(document.getElementById("map"));
               
        map.setMapType(google.maps.NORMAL_MAP);
        map.addControl(new GSmallMapControl());
        map.addControl(new GMapTypeControl());
        
        //map.setCenter(new GLatLng(<%=theLat %>, <%=theLon %>), 12);
        //map.checkResize();


        // Create a base icon for all of our markers that specifies the
        // shadow, icon dimensions, etc.
        var baseIcon = new GIcon(G_DEFAULT_ICON);
        baseIcon.shadow = vroot + '/images/map/icons/shadow50.png';
        baseIcon.iconSize = new GSize(32, 68);
        baseIcon.shadowSize = new GSize(37, 34);
        baseIcon.iconAnchor = new GPoint(9, 34);
        baseIcon.infoWindowAnchor = new GPoint(5, 2);

        // Creates a marker whose info window displays the letter corresponding
        // to the given index.
        function createMarker(point, index, guid) {
          var vroot=document.getElementById('vroot').value;
          // Create a lettered icon for this point using our icon class
          //var letter = String.fromCharCode("A".charCodeAt(0) + index);
          var letteredIcon = new GIcon(baseIcon);
          letteredIcon.image = vroot + '/images/map/icons/user_red.png';

          // Set up our GMarkerOptions object
          markerOptions = { icon:letteredIcon };
          var marker = new GMarker(point, markerOptions);

          return marker;
        }

        //Get All the points

        <%For Each item As ExpDictionary In PointsDict.Values %>
            var latTemp='<%=item("Latitude") %>';
            //alert(latTemp);
            var lonTemp='<%=item("Longitude") %>';
            //alert(lonTemp);
            var html='<b><%=item("FranchiseName") & "</b><br />" & item("Address1") & "<br />" & item("City") & "<br />" & item("StateName") & "<br />" & item("ZipCode") & "<br />" & item("Phone") & "<br />" %>';
            //alert(html);
            var point = new GLatLng(latTemp,lonTemp);
            map.addOverlay(createMarker(point, html, '<%=item("FranchiseGuid") %>'));
        <% Next %>

        //var point = new GLatLng(28.489995,-81.412222)
        //map.addOverlay(createMarker(point,1))
        //var point = new GLatLng(28.655952,-81.346288)
        //map.addOverlay(createMarker(point,2))

        // Set up Geocoder
        function setMyMap(){
            window.geocoder = new google.maps.ClientGeocoder();
            
            // If query string was provided, geocode it
//            var bits = window.location.href.split('?');
//            if (bits[1]) {
//                var location = decodeURI(bits[1]);
//                document.getElementById('geocodeInput').value = location;
//                geocode(location);
//            }
            
            // Set up the form
                //alert(document.getElementById('geocodeInput').value);
                geocode(document.getElementById('geocodeInput').value);
                
        }

        var accuracyToZoomLevel = [
            1,  // 0 - Unknown location
            5,  // 1 - Country
            6,  // 2 - Region (state, province, prefecture, etc.)
            8,  // 3 - Sub-region (county, municipality, etc.)
            11, // 4 - Town (city, village)
            13, // 5 - Post code (zip code)
            15, // 6 - Street
            16, // 7 - Intersection
            17, // 8 - Address
            17  // 9 - Premise
        ];

        function geocodeComplete(result) {
            if (result.Status.code != 200) {
                //alert('Could not geocode "' + result.name + '"');
                //alert('Please fill out the text box.');
                return;
            }
            var placemark = result.Placemark[0]; // Only use first result
            var accuracy = placemark.AddressDetails.Accuracy;
            var zoomLevel = accuracyToZoomLevel[accuracy] || 1;
            var lon = placemark.Point.coordinates[0];
            var lat = placemark.Point.coordinates[1];
//            var currentLocation= document.getElementById('geocodeInput').value;
//            if(currentLocation.toLowerCase() == 'europe'){
//                zoomLevel=3;
//            }
            map.setCenter(new google.maps.LatLng(lat, lon), zoomLevel - 4);//-1 closer. 
            
            map.checkResize(); 
            
            setMyMap();
             
        }

        function geocode(location) {
            geocoder.getLocations(location, geocodeComplete);
        }
        function SetLocation(location2) {
            document.getElementById('geocodeInput').value=location2;
            setMyMap();
        }
  
        
 
        
        
        </script>

    </asp:Panel>
</asp:Content>
<asp:Content ID="ContentBestSellers" ContentPlaceHolderID="ContentPlaceHolderBestSellers"
    runat="server">
</asp:Content>
<asp:Content ID="contentSpecials" ContentPlaceHolderID="SpecialsHolder" runat="server">
</asp:Content>
<%--JA2011021701 - STORE LOCATOR - End--%>