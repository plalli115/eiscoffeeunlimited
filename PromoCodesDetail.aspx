<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PromoCodesDetail.aspx.vb"
    Inherits="PromoCodesDetail" CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master"
    RuntimeMasterPageFile="ThreeColumn.master" CrumbName="<%$ Resources: Language, LABEL_PROMO_TITLE_DETAILS %>" %>

<%@ Register Src="~/controls/ShowPromoCodes/ShowPromoCodes.ascx" TagName="ShowPromoCodes"
    TagPrefix="uc1" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <div class="InfoPage">
        <uc1:PageHeader ID="PageHeader1" runat="server" Text="<%$ Resources: Language, LABEL_PROMO_TITLE_DETAILS %>"
            EnableTheming="true" />
        <%If Not groupsPrdFinal Is Nothing And Not groupsPrdFinal Is DBNull.Value Then%>
        <%  If groupsPrdFinal.Count > 0 Then%>
        <table cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom: 20px; margin-top: 20px;">
            <% 
                For Each promo As ExpDictionary In groupsPrdFinal.Values%>
            <tr>
                <td align="center">
                    <img src="<%=promo("PROMO_SMALL_IMAGE") %>" />
                </td>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="Label1" runat="server" Style="font-weight: bold;" Text="<%$ Resources: Language, LABEL_PROMOTION_CODE %>"></asp:Label>:
                                <%=promo("PromotionCode") %>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label2" runat="server" Style="font-weight: bold;" Text="<%$ Resources: Language, LABEL_PROMOTION_NAME %>"></asp:Label>:
                                <%=promo("PROMO_NAME") %>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%=promo("PROMO_HTML_MAIN")%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <% Next%>
        </table>
        <%  Else%>
        <asp:Literal ID="litContent" runat="server" Text="<%$ Resources: Language, LABEL_NO_PROMO_CODES %>"></asp:Literal>
        <%  End If%>
        <%  End If%>
        <asp:Button ID="Button1" CssClass="AddButton" runat="server" PostBackUrl="~/PromoCodes.aspx"
            Text="<%$ Resources: Language, LABEL_BACK %>" />
    </div>
</asp:Content>
