'JA0531201001 - STAY IN CATALOG - START
Imports System.Configuration.ConfigurationManager
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT
Imports ExpandIT.ExpandITLib
Imports System.Web

Partial Class AddToCartAjax
    Inherits ExpandIT.ShippingPayment._CartPage

    Protected html As String = ""

    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        html = eis.getHtmlAddToCartBox(CStrEx(Request("Image")), CStrEx(Request("Guid")), CStrEx(Request("VariantName")), CStrEx(Request("VariantCode")))
    End Sub

End Class
'JA0531201001 - STAY IN CATALOG - END
