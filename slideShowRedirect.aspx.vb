Imports System.Configuration.ConfigurationManager
Imports ExpandIT.ExpandITLib
Imports ExpandIT.GlobalsClass
Imports ExpandIT
Imports System.Web
Imports System.Collections.Generic
Imports ExpandIT.EISClass
Imports System.IO

Imports ExpandIT.Debug


Partial Class slideShowRedirect
    Inherits ExpandIT.Page

    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	
	Protected Overloads Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender


        Dim testStr, testStr2 As String
        Dim folderpath As String = ApplicationRoot()
        Dim relativePath As String = "/controls/slideshow/url"
        Dim link1 As String = Request("id")
        Dim link As String = "URL" & link1.ToString()
        Dim flaglink = False
        Dim text As String = ""
        Dim cont As Integer = 1
        
        For Each fileName As String In System.IO.Directory.GetFiles(folderpath & relativePath)
            testStr = fileName.Substring(folderpath.Length + relativePath.Length + 1)
            If testStr.Substring(0, testStr.IndexOf(".")).ToUpper().EndsWith(link) Then
                ViewState("fileName") = VRoot & relativePath & "\" & testStr
                text = File.ReadAllText(Server.MapPath("/" & VRoot & relativePath & "\" & testStr))
                flaglink = True
            End If
            If flaglink Then
                Exit For
            End If
        Next


        If flaglink Then
            Response.Redirect(text.ToString())
            flaglink = False
        Else
            Response.Redirect("shop.aspx")
        End If



    End Sub



End Class
