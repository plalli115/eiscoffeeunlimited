<%@ Page Language="VB" AutoEventWireup="false" CodeFile="user_page.aspx.vb" Inherits="user_page"
    CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master"
    RuntimeMasterPageFile="ThreeColumn.master" CrumbName="<%$ Resources: Language, LABEL_MENU_USER_PAGE %>" %>

<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
        <uc1:PageHeader ID="PageHeader1" runat="server" Text="<%$ Resources: Language, LABEL_MENU_USER_PAGE %>"
            EnableTheming="true" />
        
        <table width="100%" align="center" cellspacing="15">
            <tr>
                <td align="center">
                <a href="account.aspx">
                <%= Resources.Language.PIC_ACCOUNT_MY %></a>
                </td>
                <td align="center">
                <a href="notes.aspx">
                <%= Resources.Language.PIC_ACCOUNT_NOTES %></a>
                </td>
            </tr>
            <tr>
                <td align="center">
                <a href="history.aspx">
                <%= Resources.Language.PIC_ACCOUNT_HISTORY %></a>
                </td>
                <td align="center">
                <a href="_user_logout.aspx">
                <%= Resources.Language.PIC_ACCOUNT_LOGOUT %></a>
                </td>
            </tr>
        </table>
        
</asp:Content>
