//<!--JA2010031101 - CATMAN SLIDE - Start-->
var currentCount=0;
var oldAd=0;
var newAd=0;
var masterOpacity=1;
var defaultTimer=7000;
var crossFadeInterval;
var startFadeTimeout;
var bannerArray;


function startFade(){

	currentCount++;

	if(currentCount>bannerArray.length-1){
	currentCount=0
	}

	oldAd=newAd;
	newAd=currentCount;
	ad1=document.getElementById(bannerArray[oldAd].id);
	///**/ad11=document.getElementById(bannerArray[oldAd].id2);
	ad2=document.getElementById(bannerArray[newAd].id);
	///**/ad22=document.getElementById(bannerArray[newAd].id2);
	ad1.style.zIndex=999;
	///**/ad11.style.zIndex=999;
	setOpacity(ad2,0.01);
	///**/setOpacity(ad22,0.01);
	hilite_tn(document.getElementById(ad1.id+"_thumbnail").firstChild,"deselect");
	hilite_tn(document.getElementById(ad2.id+"_thumbnail").firstChild,"select");
	ad2.style.zIndex=1000;
    ///**/ad22.style.zIndex=1000;

	if(startFadeTimeout){
	window.clearTimeout(startFadeTimeout)
	}

	crossFadeInterval=setInterval("crossFade()",30)
}

function setOpacity(B,A){

	if(document.all){
	B.style.filter="alpha(opacity="+Math.ceil(A*100)+")"
	}

	B.style.opacity=(A>=1)?0.99:((A<=0)?0.01:A);
	B.style.mozOpacity=(A>=1)?0.99:((A<=0)?0.01:A)
}

function crossFade(){

	ad1=document.getElementById(bannerArray[oldAd].id);
	///**/ad11=document.getElementById(bannerArray[oldAd].id2);
	ad2=document.getElementById(bannerArray[newAd].id);
	///**/ad22=document.getElementById(bannerArray[newAd].id2);
	masterOpacity=masterOpacity-0.02;
	setOpacity(ad2,(1-masterOpacity));
	///**/setOpacity(ad22,(1-masterOpacity));

	if(masterOpacity<=0){
		clearInterval(crossFadeInterval);
		ad1.style.zIndex=1;setOpacity(ad2,1);
		///**/ad11.style.zIndex=1;setOpacity(ad22,1);
		masterOpacity=1;hilite_tn(document.getElementById(ad1.id+"_thumbnail").firstChild,"deselect");
		hilite_tn(document.getElementById(ad2.id+"_thumbnail").firstChild,"select");

		if(startFadeTimeout){
		window.clearTimeout(startFadeTimeout)
		}

		startFadeTimeout=(bannerArray[newAd].timer)?setTimeout("startFade()",bannerArray[newAd].timer):setTimeout("startFade()",defaultTimer)
	}
}

function crossFadePause(A){

	if(A){
		if(crossFadeInterval){
		clearInterval(crossFadeInterval)
		}

		if(startFadeTimeout){
		window.clearTimeout(startFadeTimeout)
		}

		ad1=document.getElementById(bannerArray[oldAd].id);
		///**/ad11=document.getElementById(bannerArray[oldAd].id2);
		ad2=document.getElementById(bannerArray[newAd].id);
		///**/ad22=document.getElementById(bannerArray[newAd].id2);
		
		if(masterOpacity!=1){
			
			if(masterOpacity>0){
				ad1.style.zIndex=1;setOpacity(ad2,1);
				///**/ad11.style.zIndex=1;setOpacity(ad22,1);
				hilite_tn(document.getElementById(ad1.id+"_thumbnail").firstChild,"deselect");
				hilite_tn(document.getElementById(ad2.id+"_thumbnail").firstChild,"select")
			}else{
				setOpacity(ad1,1);ad1.style.zIndex=1000;ad2.style.zIndex=1
				///**/setOpacity(ad11,1);ad11.style.zIndex=1000;ad22.style.zIndex=1
			}
			masterOpacity=1
		}

	}else{
		masterOpacity=1;
		startFadeTimeout=(bannerArray[newAd].timer)?setTimeout("startFade()",bannerArray[newAd].timer):setTimeout("startFade()",defaultTimer)
	}
}

function crossFadeInterrupt(A){

	if(crossFadeInterval){
	clearInterval(crossFadeInterval)
	}

	if(startFadeTimeout){
	window.clearTimeout(startFadeTimeout)
	}

	ad1=document.getElementById(bannerArray[oldAd].id);
	///**/ad11=document.getElementById(bannerArray[oldAd].id2);
	ad2=document.getElementById(bannerArray[newAd].id);
	///**/ad22=document.getElementById(bannerArray[newAd].id2);
	ad3=document.getElementById(bannerArray[A].id);
	///**/ad33=document.getElementById(bannerArray[A].id2);
	setOpacity(ad1,0);setOpacity(ad2,0);setOpacity(ad3,1);
	///**/setOpacity(ad11,0);setOpacity(ad22,0);setOpacity(ad33,1);
	hilite_tn(document.getElementById(ad1.id+"_thumbnail").firstChild,"deselect");
	hilite_tn(document.getElementById(ad2.id+"_thumbnail").firstChild,"deselect");
	hilite_tn(document.getElementById(ad3.id+"_thumbnail").firstChild,"select");
	ad1.style.zIndex=1;
	///**/ad11.style.zIndex=1;
	ad2.style.zIndex=1;
	///**/ad22.style.zIndex=1;
	ad3.style.zIndex=1000;
	///**/ad33.style.zIndex=1;
	currentCount=A;
	oldAd=A;
	newAd=A;
}

function goToAd(){
	if(bannerArray[newAd].landingPageUrl){
	var A=bannerArray[newAd].landingPageUrl;

		if(A.match(/\?/)){
			A+="&"
		}else{
			A+="?"
		}
		A+="order="+(newAd+1)+","+((bannerArray[newAd].timer)?bannerArray[newAd].timer:defaultTimer);

		if(!A.match("idg=")){
		A+="&offerName="+bannerArray[newAd].imageName
		}

		if(bannerArray[newAd].popup){
		    try{openHelpWnd(A)}

		        catch(B){
	            }

        }else{
            document.location.href=A
        }
    }
}


function hilite_tn(B,A){

    if(A=="on"){
        B.src=B.selectImg
    }else{
        if(A=="select"){
            B.src=B.selectImg;
            B.className="select"
         }else{
            if(A=="off"){
                if(!B.className.match("select")){
                    B.src=B.defaultImg
                }
             }else{
                B.className="";
                B.src=B.defaultImg
             }
         }
     }
}

function buildBannerSet(B){
    try{
        if(!B){
            throw"noParameters"
        }
        bannerArray=B.banners;
        if(!bannerArray||bannerArray.length==0){
            throw"noBannerSpecified"
        }
        for(i=0;i<bannerArray.length;i++){
            if(!bannerArray[i]){
                throw"invalidBanner"
            }
            if(!bannerArray[i].id||!bannerArray[i].id.match("banner")){
                throw"invalidBannerId"
            }
        }
        if(B.defaultTimer){
           defaultTimer=parseInt(B.defaultTimer)
        }
        var A=document.getElementById("bannerContainer");
        var H=document.getElementById("thumbnailSubContainer");
        if(bannerArray.length>1){
            A.onmouseover=function(){
                             crossFadePause(true)
                          };
            A.onmouseout=function(){
                             crossFadePause(false)
                          }
         }
         for(i=0;i<bannerArray.length;i++){
                var D=document.createElement("DIV");
                var C=document.createElement("DIV");
                var F=document.createElement("IMG");
                A.appendChild(D);
                if(bannerArray[i].landingPageUrl){
                    D.onclick=goToAd
                }else{
                    D.style.cursor="default"
                }
                D.id=bannerArray[i].id;
                D.style.zIndex=(i>0)?"1":"1002";
                D.style.width="550px";
                D.style.height="260px";
                D.style.textAlign="center";
                D.className="bannerImage";
                //D.style.backgroundImage="URL("+ bannerArray[i].imageSrc + ")";
		D.style.backgroundImage="URL('"+ bannerArray[i].imageSrc + "')";
                D.style.backgroundRepeat="no-repeat";
                D.style.position="absolute";
                D.title=bannerArray[i].imageName;
                //Html
                D.innerHTML=bannerArray[i].html;                
                
                if(bannerArray.length>1){
                    H.appendChild(C);
                    C.id=bannerArray[i].id+"_thumbnail";
                    C.className="thumbnailImageContainer";
                    C.index=i;
                    C.onclick=function(){
                                crossFadeInterrupt(this.index)
                              };
                    C.appendChild(F);
                    F.src=bannerArray[i].thumbnailImageSrc;
                    F.defaultImg=bannerArray[i].thumbnailImageSrc;
                    F.selectImg=bannerArray[i].thumbnailSelectedImageSrc;
                    if(i==0){
                        F.src=bannerArray[i].thumbnailSelectedImageSrc
                    }
                    F.onmouseover=function(){
                                    hilite_tn(this,"on")
                                  };
                    F.onmouseout=function(){
                                    hilite_tn(this,"off")
                                 };
                    F.className=(i>0)?"":"select"
                 }
            }
            document.getElementById("thumbnailContainer").style.zIndex="2500";
            if(bannerArray.length>1){
                startFadeTimeout=(bannerArray[0].timer)?setTimeout("startFade()",bannerArray[0].timer):setTimeout("startFade()",defaultTimer)
            }
        }
        catch(E){
        }
};
//<!--JA2010031101 - CATMAN SLIDE - End-->