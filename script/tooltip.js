﻿// JScript File

function Tooltip(){
	// create outer div
	this.tooltip = document.createElement("div");
	this.tooltip.style.position = "absolute";	
	this.tooltip.style.visibility = "hidden";
	this.tooltip.className = "toolTipShadow";
	// create inner div
	this.content = document.createElement("div");
	this.content.style.position = "relative";
	this.content.className = "toolTipContent";	
	// add content to tooltip
	this.tooltip.appendChild(this.content);	
}

Tooltip.prototype.show = function(text, x, y){
	this.content.innerHTML = text;	
	this.tooltip.style.left = x + "px";
	this.tooltip.style.top = y + "px";	
	this.tooltip.style.visibility = "hidden";
	if(this.tooltip.parentNode != document.body){
		document.body.appendChild(this.tooltip);
	}
	// check if tooltip will fall over the edge of window
	// if so relocate tooltip position
	this.locate(x,y);
	// show tooltip
	this.tooltip.style.visibility = "visible";		
};

Tooltip.prototype.hide = function(){
	// hide tooltip
	this.tooltip.style.visibility = "hidden";
};

Tooltip.prototype.locate = function(posx, posy){

	// get tooltip widht and height
	var tipWidth = this.tooltip.offsetWidth;
	var tipHeight = this.tooltip.offsetHeight;
	
	// get width and height of current window
	var winWidth = Geometry.getViewportWidth();
	var winHeight = Geometry.getViewportHeight();	

	// get tooltip margin values and parse to int
	var leftMargin = getElementStyle(this.tooltip, "marginLeft", "margin-left");
	var topMargin = getElementStyle(this.tooltip, "marginTop", "margin-top");
	leftMargin.replace(/px/, "");
	topMargin.replace(/px/, "");
	var lMargin = parseInt(leftMargin);
	var tMargin = parseInt(topMargin);	
	/* see if the tooltip will fall over the edge of the current window
		if this is the case - calculate new posistions for x and y 
	*/
	// if target is to close to the windows right edge calculate new value for x
	if((posx + lMargin + tipWidth) > (winWidth + Geometry.getHorizontalScroll())){
		x = Geometry.getHorizontalScroll() + ((winWidth - (tipWidth + lMargin))); 
		this.tooltip.style.left = x + "px";
		this.tooltip.style.width = tipWidth;
	}
	// if target is to close to the windows bottom edge calculate new value for y
	if((posy + tMargin) > (winHeight - tipHeight + Geometry.getVerticalScroll())){
		y = Geometry.getVerticalScroll() + (winHeight - tipHeight);	
		this.tooltip.style.top = y + "px";	
	}		
};

Tooltip.prototype.schedule = function(target, e, text){
	// set values for x and y - same as target position
	var x = getElementPosition(target, 'x');
	var y = getElementPosition(target, 'y');
	var self = this;
	// assign timers to show and hide the tooltip. pass parameters x and y to set tooltip position in function show()
	var timer = window.setTimeout(function(){self.show(text,x,y);},400);
	var timer2 = window.setTimeout(function(){self.hide();},3000);
}

function getElementStyle(elem, IEStyleProp, CSSStyleProp) {    
	if (elem.currentStyle) {
		return elem.currentStyle[IEStyleProp];
    } 
	else if (window.getComputedStyle) {
		var compStyle = window.getComputedStyle(elem, "");
        return compStyle.getPropertyValue(CSSStyleProp);
    }
    return "";
}