//AM2010062201 - VERTICAL SLIDE - START
jQuery.fn.vertSlider = function(options) {

    jQuery('#main-container-background a[name]').removeAttr("name");
	jQuery('#main-container').css('overflow', 'hidden');
	jQuery('#side-nav').css('visibility', 'visible');
    return this.each(function() {
        jQuery(this).click(function() {
            var index = jQuery('#side-nav a').index(this);
            jQuery(this).parent().removeClass('remove-right-border').css("border-right-color", "#3F9AC9");
	        jQuery(this).parent().siblings().addClass('remove-right-border');
		    jQuery("#main-container-background").animate({ top: index * -300 }, 'slow');
        });
    });
};	
//AM2010062201 - VERTICAL SLIDE - END
	
