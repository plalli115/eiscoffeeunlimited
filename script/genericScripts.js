﻿/* genericScripts.js */

var errorMessageControl = '';      

function hideErrorMessage(){
    try{
        document.getElementById(errorMessageControl).style.display = 'none';  
    }catch(Error){
    
    }               
}
  
function unhideErrorMessage(controlId){
    errorMessageControl = controlId;
    var isBlockedByError = IsValidatorInvalid();
    if(isBlockedByError){
        document.getElementById(controlId).style.display = 'block';                
    }        
}

function IsValidatorInvalid(){
    Page_ClientValidate();
    var i;
    for (i = 0; i < Page_Validators.length; i++) {
        if (!Page_Validators[i].isvalid) {                   
            return true;                  
        }
    }
    return false;                
}

function toggleValidation(id) {
    id == 2 ? setValidation(true) : setValidation(false);
}

function setValidation(state) {
    for (var i = 0; i < Page_Validators.length; i++) {
        ValidatorEnable(Page_Validators[i], state);
    }
}