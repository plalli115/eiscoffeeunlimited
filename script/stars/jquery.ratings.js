jQuery.fn.ratings = function(stars, initialRating) {
  
  //Save  the jQuery object for later use.
  var elements = this;
  
  //Go through each object in the selector and create a ratings control.
  return this.each(function() {
  
    //Make sure intialRating is set.
    if(!initialRating)
      initialRating = 0;
      
    //Save the current element for later use.
    var containerElement = this;
    
    //grab the jQuery object for the current container div
    var container = jQuery(this);
    
    //Create an array of stars so they can be referenced again.
    var starsCollection = Array();
    
    //Save the initial rating.
    //***********Ratings New***************START
    var initialRate=document.getElementById('startRate').value;  
    initialRating = parseInt(initialRate);
    //***********Ratings New***************END
    containerElement.rating = initialRating;
    
    //Set the container div's overflow to auto.  This ensure it will grow to
    //hold all of its children.
    //container.css('overflow', 'auto');
    
    //create each star
    for(var starIdx = 0; starIdx < stars; starIdx++) {
      
      //Create a div to hold the star.
      var starElement = document.createElement('div');
      
      //Get a jQuery object for this star.
      var star = jQuery(starElement);
      
      //Store the rating that represents this star.
      starElement.rating = starIdx + 1;
      
      //Add the style.
      star.addClass('jquery-ratings-star');
      
      //Add the full css class if the star is beneath the initial rating.
      if(starIdx < initialRating) {
        star.addClass('jquery-ratings-full');
      }
      
      //add the star to the container
      container.append(star);
      starsCollection.push(star);
      
      //hook up the click event
      star.click(function() {
        //When clicked, fire the 'ratingchanged' event handler.  Pass the rating through as the data argument.
        elements.triggerHandler("ratingchanged", {rating: this.rating});
        containerElement.rating = this.rating;
        //***********Ratings New***************START
        animatedcollapse.hide('ReadReviews');
        animatedcollapse.hide('RateItem');
        var newRate= document.getElementById('newRate');
        newRate.value=this.rating;
        var ProductGuid= document.getElementById('ProductGuid').value;
        var vroot= document.getElementById('vroot').value;
        var url= vroot + "/ratings.aspx?SetStars=1&ratingValue=" + this.rating + "&ProductGuid=" + ProductGuid;
         //Loading wait
        var divLoading=document.getElementById('Loading');
        var loadingImg=document.getElementById('LoadingImg');
        loadingImg.style.paddingTop='0px';
        divLoading.style.top='60px';
        divLoading.style.left='230px';
        divLoading.style.width='188px';
        divLoading.style.height='22px';
        divLoading.style.display='block';
         //Loading wait
        var xhReq = new XMLHttpRequest();  
        //xhReq.open("GET", url , false);
        xhReq.open("GET", url += (url.match(/\?/) == null ? "?" : "&") + (new Date()).getTime(), false);  
        xhReq.send(null);
        //reload 
        //setTimeout('document.getElementById(\'ctl00_ctl00_cphRoot_cphSubMaster_fwProductForm_VerticalSlide1_ctl04_btnHiddenForUpdate\').click()', 1000);
        
        var modeDisplay=document.getElementById('verticalSlideMode').value;
        if(modeDisplay == 1){
            document.getElementById('ctl00_ctl00_cphRoot_cphSubMaster_fwProductForm_VerticalSlide1_ctl04_btnHiddenForUpdate').click();
        }else{
            if(modeDisplay == 2){
                document.getElementById('ctl00_ctl00_cphRoot_cphSubMaster_fwProductForm_VerticalSlide1_ctl09_btnHiddenForUpdate').click();
            }
        }
                    
        
        //var serverResponse = xhReq.responseText;
        //alert(serverResponse); 
        //***********Ratings New***************END
        
      });
      
      star.mouseenter(function() {
        //Highlight selected stars.
        for(var index = 0; index < this.rating; index++) {
          starsCollection[index].addClass('jquery-ratings-full');
        }
        //Unhighlight unselected stars.
        for(var index = this.rating; index < stars; index++) {
          starsCollection[index].removeClass('jquery-ratings-full');
        }
      });
      
      container.mouseleave(function() {
        //Highlight selected stars.
        for(var index = 0; index < containerElement.rating; index++) {
          starsCollection[index].addClass('jquery-ratings-full');
        }
        //Unhighlight unselected stars.
        for(var index = containerElement.rating; index < stars ; index++) {
          starsCollection[index].removeClass('jquery-ratings-full');
        }
      });
    }
  });
};