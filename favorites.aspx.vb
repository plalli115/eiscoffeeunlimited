Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports ExpandIT.Debug
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT.GlobalsClass

Partial Class favorites
    Inherits ExpandIT.ShippingPayment._CartPage

	Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        Me.RequireSSL = True
    End Sub
	
	Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        ' Check the page access.
        eis.PageAccessRedirect("Favorites")

        ' DEBUG INFORMATION COLLECTION
        If globals.DebugLogic Then
            DebugValue(User, "User")
        End If
    End Sub
	
	

End Class
