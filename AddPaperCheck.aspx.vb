﻿'<!--JA2010102801 - PAPERCHECK - Start-->
Imports System.Configuration.ConfigurationManager
Imports ExpandIT.EISClass
Imports ExpandIT.GlobalsClass
Imports ExpandIT
Imports ExpandIT.Debug
Imports ExpandIT.ShippingPayment
Imports ExpandIT.ExpandITLib
Imports System.Collections.Generic
Imports System.Data
Imports System.Data.SqlClient
Imports ExpandIT.AuthorizeNet.Authorizenet_Simlib

Partial Class AddPaperCheck
    Inherits ShippingPaymentPage
    'Inherits PaymentMethodPage

    Protected dictLine As ExpDictionary
    Protected OrderDictionary, Shipping, Payment As ExpDictionary
    Protected bUseSecondaryCurrency As Boolean
    Protected dictSHProviders, dictSHProvider As ExpDictionary
    Protected strAdditionalInformation As String
    Protected txtPaymentProcessError As String = ""

    ' Used on payment.asp
    Protected CartLine As ExpDictionary
    Protected ColSpanNumber As Integer
    Protected StrId As String
    Protected sqlString As String
    Protected ret, sequence As Object

    Protected isPaymentShippingAddressLoaded As Boolean = False
    'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
    Protected csrObj As USCSR
    'AM2011031801 - ENTERPRISE CSR STAND ALONE - End


    Protected Overloads Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender

        '' Make sure this page expires.
        'Dim pStr As String

        'pStr = "private, no-cache, must-revalidate"
        'Response.ExpiresAbsolute = New Date(1990 - 1 - 1)
        'Response.AddHeader("pragma", "no-cache")
        'Response.AddHeader("cache-control", pStr)

        ' Check the page access.
        eis.PageAccessRedirect("Order")

        If Globals.User.Count < 1 Then
            If Request("returl") = "" Then
                Response.Redirect("user_login.aspx")
            Else
                Response.Redirect("user_login.aspx?returl=" & Server.UrlEncode(ToString(Request("returl"))))
            End If
            'Else
            '    ' Get dictionary with ShippingAddress info
            '    bUseSecondaryCurrency = eis.UseSecondaryCurrency(globals.User)

            '    globals.OrderDict = eis.LoadOrderDictionary(globals.User)
            '    ' Always calculate the order on Payment page. This is the final step, and the order should be the final order here.
            '    globals.OrderDict("IsCalculated") = False
            '    globals.OrderDict("VersionGuid") = GenGuid()
            '    eis.CalculateOrder()
            '    ' Load information for products
            '    eis.CatDefaultLoadProducts(globals.OrderDict("Lines"), False, False, False, New String() {"ProductName"}, New String() {"PICTURE2"}, Nothing)

            '    If String.IsNullOrEmpty(DBNull2Nothing(globals.OrderDict("CustomerReference"))) Then
            '        globals.OrderDict("CustomerReference") = eis.GenCustRef()
            '    End If

            '    ' Read PaymentProcessStatus and clear the field.
            '    txtPaymentProcessError = CStrEx(globals.OrderDict("PaymentStatus"))
            '    globals.OrderDict("PaymentStatus") = ""

            '    ' Save the order dictionary
            '    eis.SaveOrderDictionary(globals.OrderDict)
            '    loadPaymentAndShippingData()
        End If



    End Sub



    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        'globals.OrderDict = eis.LoadOrderDictionary(globals.User)

        'Dim custPaymentMethod As Object = CustomerPaymentMethod
        'If custPaymentMethod IsNot Nothing Then
        Dim eh As EventHandler = New EventHandler(AddressOf BtnAccept_Click)
        Me.papercheckform.Visible = True
        papercheckform.clickEvent = eh
        'End If
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
        csrObj = New USCSR(globals)
        'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
    End Sub


    'Protected Sub loadPaymentAndShippingData()

    '    If CBoolEx(AppSettings("USE_SHIPPING_ADDRESS")) Then
    '        Try
    '            Shipping = CartShippingAddress
    '            Payment = CartPaymentAddress
    '        Catch ex As Exception
    '            globals.messages.Errors.Add(ex.Message)
    '            'Response.Redirect("cart.aspx", True)
    '        End Try
    '    Else
    '        CartShippingAddress = globals.User
    '        Shipping = CartShippingAddress
    '        Payment = globals.User
    '    End If

    '    ' Set Flag to loaded to avoid reload
    '    isPaymentShippingAddressLoaded = True

    'End Sub

    ''' <summary>
    ''' Handles Click Event on BtnAccept          
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub BtnAccept_Click(ByVal sender As Object, ByVal e As EventArgs)

        Dim ea As ExpandIT.ExpEventArgs = CType(e, ExpandIT.ExpEventArgs)

        Dim paperCheckNumber As String
        Dim paperCheckDate As String
        Dim paperCheckTotals As Double
        Dim paperCheckBoxTrack1 As Integer
        Dim paperCheckBoxTrack2 As Integer
        Dim paperCheckTrackDate As Date
        Dim paperCheckComment As String

        'prepareContextItems()
        paperCheckNumber = CStrEx(papercheckform.CheckNumber)
        paperCheckDate = CStrEx(papercheckform.CheckDate)
        paperCheckTotals = papercheckform.CheckTotals
        paperCheckBoxTrack1 = papercheckform.BoxTrack1
        paperCheckBoxTrack2 = papercheckform.BoxTrack2
        paperCheckTrackDate = papercheckform.BoxTrackDate
        paperCheckComment = papercheckform.Comment

        If paperCheckNumber = "" Or paperCheckDate = "" Then
            Response.Redirect("AddPaperCheck.aspx?PaperCheck_ErrorString=" & Resources.Language.PAPERCHECK_ERROR & "&HeaderGuid=" & Request("HeaderGuid") & "&OrderNumber=" & Request("OrderNumber")) 'Show error message
        Else
            If Not validPaperCheckDate(paperCheckDate) Then
                Response.Redirect("AddPaperCheck.aspx?PaperCheck_ErrorString=" & Resources.Language.PAPERCHECK_DATE_ERROR & "&HeaderGuid=" & Request("HeaderGuid") & "&OrderNumber=" & Request("OrderNumber")) 'Show error message
            End If
        End If

        Dim sql As String = ""
        Dim salesPersonGuid As String = csrobj.getSalesPersonGuid()
        If CStrEx(Request("ActiveOrder")) = "1" Then
            sql = "INSERT INTO PaperChecks VALUES (" & SafeString(CStrEx(Request("OrderNumber"))) & ",'PAPERCHECK'," & SafeString(paperCheckNumber) & "," & SafeString(paperCheckDate) & "," & SafeString(paperCheckTotals) & "," & SafeString(paperCheckBoxTrack1) & "," & SafeString(paperCheckBoxTrack2) & "," & SafeString(paperCheckTrackDate) & "," & SafeString(CStrEx(paperCheckComment)) & "," & CIntEx(Request("ActiveOrder")) & "," & SafeString(salesPersonGuid) & "," & SafeString(GenGuid()) & ",'0'" & ")"
            excecuteNonQueryDb(sql)
            Response.Redirect("payment.aspx")
        Else
            sql = "INSERT INTO PaperChecks VALUES (" & SafeString(CStrEx(Request("OrderNumber"))) & ",'PAPERCHECK'," & SafeString(paperCheckNumber) & "," & SafeString(paperCheckDate) & "," & SafeString(paperCheckTotals) & "," & SafeString(paperCheckBoxTrack1) & "," & SafeString(paperCheckBoxTrack2) & "," & SafeString(paperCheckTrackDate) & "," & SafeString(CStrEx(paperCheckComment)) & "," & CIntEx(Request("ActiveOrder")) & "," & SafeString(salesPersonGuid) & "," & SafeString(GenGuid()) & ",'1'" & ")"
            excecuteNonQueryDb(sql)
            Response.Redirect("history_detail.aspx?HeaderGuid=" & CStrEx(Request("HeaderGuid")), True)
        End If


    End Sub

    'Private Sub prepareContextItems()
    '    If Not isPaymentShippingAddressLoaded Then
    '        loadPaymentAndShippingData()
    '    End If
    '    Context.Items("OrderDict") = IIf(globals.OrderDict Is Nothing, eis.LoadOrderDictionary(globals.User), globals.OrderDict)
    '    Context.Items("Shipping") = Shipping
    '    Context.Items("Payment") = Payment
    'End Sub

    Private Function validPaperCheckDate(ByVal checkDate As String) As Boolean
        Dim dateArray As String()

        checkDate = Trim(checkDate)
        If checkDate.Length = 10 Then
            dateArray = checkDate.Split(New Char() {"/"})
            If dateArray.Length = 3 Then
                If dateArray(0).Length = 2 Then
                    If dateArray(1).Length = 2 Then
                        If dateArray(2).Length = 4 Then
                            If IsDate(checkDate) Then
                                Return True
                            End If
                        End If
                    End If
                End If
            End If
        End If

        Return False
    End Function
    '<!--JA2010102801 - PAPERCHECK - End-->

End Class