Imports System.Configuration.ConfigurationManager
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports ExpandIT.GlobalsClass
Imports ExpandIT
Imports System.Text

Partial Class admin_password
    Inherits ExpandIT.Page

    Protected PasswordUser As ExpDictionary
    Protected bShowForm As Boolean
    Protected DisplayMessage As String

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim myUserGuid As String
        Dim pwd1, pwd2 As String
        Dim sql As String

        ' Setting and getting information.
        myUserGuid = Request("UserGuid")
        DisplayMessage = ""
        bShowForm = False

        If myUserGuid = "" Then
            DisplayMessage = DisplayMessage & Resources.Language.MESSAGE_USER_WAS_NOT_FOUND & "!"
        ElseIf Request.Form("newExppassword1") <> "" Or Request.Form("newExppassword2") <> "" Then
            PasswordUser = eis.LoadUser(myUserGuid)

            ' Check that the user was found
            If PasswordUser("Anonymous") Then
                DisplayMessage = DisplayMessage & Resources.Language.MESSAGE_USER_WAS_NOT_FOUND & "!"
            Else
                pwd1 = Request.Form("newExppassword1")
                pwd2 = Request.Form("newExppassword2")
                If pwd1 = pwd2 Then
                    If Len(pwd1) < CLngEx(AppSettings("PASSWORD_LEN")) Then
                        DisplayMessage = DisplayMessage & Resources.Language.MESSAGE_NEW_PASSWORD_MUST_BE_LONGER_THAN_THREE & "."
                    Else
                        sql = "UPDATE UserTable SET UserPassword=" & SafeString(eis.Encrypt(pwd1)) & " WHERE UserGuid=" & SafeString(Request("UserGuid"))
                        excecuteNonQueryDb(sql)
                        DisplayMessage = DisplayMessage & Resources.Language.LABEL_THE_PASSWORD_HAS_BEEN_CHANGED & "."
                    End If
                Else
                    DisplayMessage = DisplayMessage & Resources.Language.MESSAGE_NEW_PASSWORDS_ARE_INCORRECT & "."
                    bShowForm = True
                End If
            End If
        Else
            PasswordUser = eis.LoadUser(myUserGuid)

            ' Check that the user was found
            If PasswordUser("Anonymous") Then
                DisplayMessage = DisplayMessage & Resources.Language.MESSAGE_USER_WAS_NOT_FOUND & "!"
            Else
                DisplayMessage = DisplayMessage & Resources.Language.LABEL_YOU_CAN_NOW_CHANGE_PASSWORD & "!"
                bShowForm = True
            End If
        End If
    End Sub

End Class
