<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Page Language="VB" Theme="" StylesheetTheme="" %>

<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Import Namespace="ExpandIT.Debug" %>

<script runat="server" language="vbscript">
    Const adStateClosed As Integer = 0
    Const adStateOpen As Integer = 1

    ' CursorTypeEnum Values ----
    Const adOpenForwardOnly As Integer = 0
    Const adOpenKeyset As Integer = 1
    Const adOpenDynamic As Integer = 2
    Const adOpenStatic As Integer = 3

    ' LockTypeEnum Values ----
    Const adLockReadOnly As Integer = 1
    Const adLockPessimistic As Integer = 2
    Const adLockOptimistic As Integer = 3
    Const adLockBatchOptimistic As Integer = 4

    Const adPersistADTG As Integer = 0
    Const adPersistXML As Integer = 1

    ' FileSystemObject ----
    Const TristateUseDefault As Integer = -2
    Const TristateTrue As Integer = -1
    Const TristateFalse As Integer = 0

    Const ForReading As Integer = 1
    Const ForWriting As Integer = 2
    Const ForAppending As Integer = 3

    ' Variables
    Dim i As Integer
    Dim appValue As Object
    Dim strPos As Integer
    Dim strPos2 As Integer
    Dim name As String
    Dim tmp As String
</script>

<html>
<head>
    <title>Web Site Debug Information</title>
    <style>
<!--
	BODY 
	{
		background-color: ffffff;
		font-family: Verdana, geneva, Arial, Helvetica;
		font-size: 10px;
	}
	TD
	{
		font-size: 10px;
		background-color: #FFFFFF;
		vertical-align: top;
	}
	TH
	{
		font-size: 10px;
		background-color: #6699CC;
		vertical-align: top;
		text-align: left;
	}
	TABLE
	{
		background-color: #6699CC;
		xborder: 0;
		xhspace: 0;
	}
	
-->
</style>
</head>
<% =Request("t2")%>
<h1>
    Web Site Debug Information</h1>
<table cellspacing="1" cellpadding="2">
    <tr>
        <th colspan="2">
            Server Information</th>
    </tr>
    <tr>
        <td>
            Name</td>
        <td>
            <% = Request.ServerVariables("SERVER_NAME") %>
        </td>
    </tr>
    <tr>
        <td>
            IP Address</td>
        <td>
            <% =  Request.ServerVariables("LOCAL_ADDR") %>
        </td>
    </tr>
    <tr>
        <td>
            Physical Root</td>
        <td>
            <% = Request.ServerVariables("APPL_PHYSICAL_PATH") %>
        </td>
    </tr>
    <tr>
        <td>
            Software</td>
        <td>
            <% = Request.ServerVariables("SERVER_SOFTWARE") %>
        </td>
    </tr>
    <tr>
        <td>
            ADO Version</td>
        <td>
            <% = ADOVersion %>
        </td>
    </tr>
    <tr>
        <td>
            Script Engine</td>
        <td>
            <% = ScriptEngine & " v" & ScriptEngineMajorVersion & "." & ScriptEngineMinorVersion & " (Build " & ScriptEngineBuildVersion & ")" %>
        </td>
    </tr>
    <tr>
        <td>
            Script Timeout</td>
        <td>
            <% = Server.ScriptTimeout %>
            seconds</td>
    </tr>
    <tr>
        <td>
            Session Timeout</td>
        <td>
            <% = Session.Timeout%>
            minutes</td>
    </tr>
    <tr>
        <td>
            Protocol</td>
        <td>
            <% = Request.ServerVariables("SERVER_PROTOCOL") %>
        </td>
    </tr>
    <tr>
        <td>
            Gateway</td>
        <td>
            <% = Request.ServerVariables("GATEWAY_INTERFACE") %>
        </td>
    </tr>
</table>
<p>
    <table cellspacing="1" cellpadding="2">
        <tr>
            <th colspan="2">
                Application Information</th>
        </tr>
        <tr>
            <td>
                Physical Root</td>
            <td>
                <% = Request.ServerVariables("APPL_PHYSICAL_PATH") %>
            </td>
        </tr>
        <tr>
            <td>
                Write Access</td>
            <td>
                <% = WriteAccessApp() %>
            </td>
        </tr>
        <tr>
            <td>
                Database Path</td>
            <td>
                <% = Application("WEBDBFILEPATH") %>
            </td>
        </tr>
        <tr>
            <td>
                Write Access</td>
            <td>
                <% = WriteAccessDB() %>
            </td>
        </tr>
        <tr>
            <td>
                Cache Limit</td>
            <td>
                <% =HttpContext.Current.Cache.EffectivePrivateBytesLimit & " Bytes"%>
            </td>
        </tr>
    </table>
    <p>
        <table cellspacing="1" cellpadding="2">
            <tr>
                <th colspan="2">
                    Cache Object</th>
            </tr>
            <%
                Dim enmr As IDictionaryEnumerator = Cache.GetEnumerator()
                ' Hide login information to database.
                For i = 0 To Cache.Count - 1
                    enmr.MoveNext()
                    Response.Write("<tr><td>" & enmr.Key.ToString() & "</td><td>")
                    appValue = Cache.Item(enmr.Key.ToString()).GetType()
                    
                    Response.Write(appValue)
                    Response.Flush()
                    tmp = DebugType(Cache.Item(enmr.Key.ToString()), 0)
                    Response.Write("</td></tr>")
                Next
            %>
        </table>
        <p>
            <table cellspacing="1" cellpadding="2">
                <tr>
                    <th colspan="2">
                        Application Object</th>
                </tr>
                <%
                    ' Hide login information to database.
                    For i = 0 To Application.Contents.Count - 1
                        Response.Write("<tr><td>" & Application.Contents.GetKey(i) & "</td><td>")
                        Select Case Application.Contents.GetKey(i)
                            Case "WebConnectionString", "UpdateConnectionString"
                                appValue = Application.Contents.Item(i)
                
                                strPos = InStr(1, appValue, "uid=", 1)
                                If strPos > 0 Then
                                    strPos2 = InStr(strPos, appValue, ";")
                                    If strPos2 = 0 Then strPos2 = Len(appValue) + 1
                                    appValue = Left(appValue, strPos + 3) & "<i>Hidden</i>" & Mid(appValue, strPos2)
                                End If

                                strPos = InStr(1, appValue, "pwd=", 1)
                                If strPos > 0 Then
                                    strPos2 = InStr(strPos, appValue, ";")
                                    If strPos2 = 0 Then strPos2 = Len(appValue) + 1
                                    appValue = Left(appValue, strPos + 3) & "<i>Hidden</i>" & Mid(appValue, strPos2)
                                End If

                                strPos = InStr(1, appValue, "User Id=", 1)
                                If strPos > 0 Then
                                    strPos2 = InStr(strPos, appValue, ";")
                                    If strPos2 = 0 Then strPos2 = Len(appValue) + 1
                                    appValue = Left(appValue, strPos + 7) & "<i>Hidden</i>" & Mid(appValue, strPos2)
                                End If

                                strPos = InStr(1, appValue, "Password=", 1)
                                If strPos > 0 Then
                                    strPos2 = InStr(strPos, appValue, ";")
                                    If strPos2 = 0 Then strPos2 = Len(appValue) + 1
                                    appValue = Left(appValue, strPos + 8) & "<i>Hidden</i>" & Mid(appValue, strPos2)
                                End If

                                Response.Write(appValue)
                            Case Else
                                Response.Flush()
                                tmp = DebugType(Application.Contents.Item(i), 0)
                                Response.Write("<pre><font size=1 face=verdana>" & tmp & "</font></pre>")
                    
                        End Select
        
        
                        Response.Write("</td></tr>")
                    Next
                %>
            </table>
            <p>
                <table cellspacing="1" cellpadding="2">
                    <tr>
                        <th colspan="2">
                            Type Conversions</th>
                    </tr>
                    <tr>
                        <td>
                            Decimal Point</td>
                        <td>
                            <% = mid(1.1,2,1) %>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            CDate(now)</td>
                        <td>
                            <% = CDate(now) %>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            vbGeneralDate</td>
                        <td>
                            <% = FormatDateTime(Now(), vbGeneralDate) %>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            vbLongDate</td>
                        <td>
                            <% = FormatDateTime(Now(), vbLongDate) %>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            vbShortDate</td>
                        <td>
                            <% = FormatDateTime(Now(), vbShortDate) %>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            vbLongTime</td>
                        <td>
                            <% = FormatDateTime(Now(), vbLongTime) %>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            vbShortTime</td>
                        <td>
                            <% = FormatDateTime(Now(), vbShortTime) %>
                        </td>
                    </tr>
                </table>
                <p>
                    <table cellspacing="1" cellpadding="2">
                        <tr>
                            <th colspan="2">
                                Server Objects</th>
                        </tr>
                        <tr>
                            <td>
                                <b>Name</b></td>
                            <td>
                                <b>Prog Id</b></td>
                            <td>
                                <b>Type Name</b></td>
                        </tr>
                        <tr>
                            <td>
                                Ad Rotator</td>
                            <td>
                                MSWC.AdRotator</td>
                            <td>
                                <% = ServerCreateObject("MSWC.AdRotator") %>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Browser Capabilities</td>
                            <td>
                                MSWC.BrowserType</td>
                            <td>
                                <% = ServerCreateObject("MSWC.BrowserType") %>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Content Linking</td>
                            <td>
                                MSWC.NextLink</td>
                            <td>
                                <% = ServerCreateObject("MSWC.NextLink") %>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Content Rotator</td>
                            <td>
                                MSWC.ContentRotator</td>
                            <td>
                                <% = ServerCreateObject("MSWC.ContentRotator") %>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Counters</td>
                            <td>
                                MSWC.Counters</td>
                            <td>
                                <% = ServerCreateObject("MSWC.Counters") %>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Database Access</td>
                            <td>
                                ADODB.Connection</td>
                            <td>
                                <% = ServerCreateObject("ADODB.Connection") %>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                File Access Component</td>
                            <td>
                                Scripting.FileSystemObject</td>
                            <td>
                                <% = ServerCreateObject("Scripting.FileSystemObject") %>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Logging Utility</td>
                            <td>
                                MSWC.IISLog</td>
                            <td>
                                <% = ServerCreateObject("MSWC.IISLog") %>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                MyInfo</td>
                            <td>
                                MSWC.MyInfo</td>
                            <td>
                                <% = ServerCreateObject("MSWC.MyInfo") %>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Page Counter</td>
                            <td>
                                MSWC.PageCounter</td>
                            <td>
                                <% = ServerCreateObject("MSWC.PageCounter") %>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Permission Checker</td>
                            <td>
                                MSWC.PermissionChecker</td>
                            <td>
                                <% = ServerCreateObject("MSWC.PermissionChecker") %>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Status</td>
                            <td>
                                MSWC.Status</td>
                            <td>
                                <% = ServerCreateObject("MSWC.Status") %>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Tools</td>
                            <td>
                                MSWC.Tools</td>
                            <td>
                                <% = ServerCreateObject("MSWC.Tools") %>
                            </td>
                        </tr>
                    </table>
                    <p>
                        <%
                            ' **********************************************************************************
                            ' 
                            '  Information on available and supported email components.
                            ' 
                            ' **********************************************************************************
                            On Error Resume Next
                            Dim objAspMail, objJmail, objCDONTS, objCDOSYS As Object
                            Dim strJmail As String
                        
                            objAspMail = CreateObject("SMTPsvg.Mailer")
                            objJmail = CreateObject("Jmail.Message")
                            ' A license information string.
                            strJmail = Replace(Replace(Replace(Replace(Replace(objJmail.About, "<h2>", " "), "</h2>", ","), "<h4>", " "), "</h4>", ","), "<br />", ",")
                            objCDONTS = CreateObject("CDONTS.NewMail")
                            objCDOSYS = CreateObject("CDO.Message")
                        %>
                        <table cellspacing="1" cellpadding="2">
                            <tr>
                                <th colspan="2">
                                    Mail Objects</th>
                            </tr>
                            <tr>
                                <td>
                                    <b>Name</b></td>
                                <td>
                                    <b>Prog Id</b></td>
                                <td>
                                    <b>Type Name</b></td>
                                <td>
                                    <b>Version</b></td>
                                <td>
                                    <b>Additional information</b></td>
                            </tr>
                            <tr>
                                <td>
                                    CDONTS</td>
                                <td>
                                    CDONTS.NewMail</td>
                                <td>
                                    <% = ServerCreateObject("CDONTS.NewMail") %>
                                </td>
                                <td>
                                    <% = objCDONTS.Version %>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    CDOSYS</td>
                                <td>
                                    CDO.Message</td>
                                <td>
                                    <% = ServerCreateObject("CDO.Message") %>
                                </td>
                                <td>
                                    N/A</td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    JMail</td>
                                <td>
                                    Jmail.Message</td>
                                <td>
                                    <% = ServerCreateObject("Jmail.Message") %>
                                </td>
                                <td>
                                    <% = objJMail.Version %>
                                </td>
                                <td>
                                    <b>About:</b>
                                    <% = strJmail %>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    ASPMail</td>
                                <td>
                                    SMTPsvg.Mailer</td>
                                <td>
                                    <% = ServerCreateObject("SMTPsvg.Mailer") %>
                                </td>
                                <td>
                                    <% = objAspMail.Version %>
                                </td>
                                <td>
                                    <b>Expires:</b>
                                    <% = objAspMail.Expires %>
                                    , <b>Registered to:</b>
                                    <% = objAspMail.RegisteredTo %>
                                </td>
                            </tr>
                        </table>
                        <%

                            objJmail = Nothing
                            objAspMail = Nothing
                            objCDONTS = Nothing
                            On Error GoTo 0
                            ' ********************************************************************************** %>
                    </p>
<p>
    <% DriveInfo()%>
    <p>
        <table border="0" cellspacing="1" cellpadding="2">
            <tr>
                <th colspan="2">
                    Server Variables</th>
            </tr>
            <%
                For Each name In Request.ServerVariables
                    Response.Write("<tr><td>" & name & "</td><td>" & Request.ServerVariables(name) & "</td></tr>")
                Next
            %>
            </TR>
        </table>
</html>

<script runat="server" language="vbscript">
    Function ADOVersion() As String
        Dim conn As Object
        Dim retval As String
        On Error Resume Next
        conn = CreateObject("ADODB.Connection")
        If Err.Number <> 0 Then
            retval = "Can't create object."
        Else
            retval = conn.Version
        End If
        Return retval
    End Function

    Function ServerCreateObject(ByVal ProgID As String) As Object
        Dim obj As Object = Nothing
    
        On Error Resume Next
        obj = CreateObject(ProgID)
        If Err.Number = 0 Then
            ServerCreateObject = TypeName(obj)
        Else
            ServerCreateObject = "N/A"
        End If

        obj = Nothing
    End Function

    Public Sub DriveInfo()
        Dim objFS, objDrv As Object
        Dim s As String

        objFS = CreateObject("Scripting.FileSystemObject")

        s = _
            "<table cellpadding=2 cellspacing=1>" & _
            "<tr><th colspan=5>Drive Information</th></tr>" & _
            "<tr>" & _
            "	<td><b>Drive</b></td>" & _
            "	<td><b>Name</b></td>" & _
            "	<td><b>Type</b></td>" & _
            "	<td><b>File System</b></td>" & _
            "	<td><b>Size</b></td>" & _
            "	<td><b>Free</b></td>"
        Response.Write(s)
        
	
        For Each objDrv In objFS.Drives
            Response.Write("<tr><td>" & objDrv.DriveLetter & "</td><td>")

            If objDrv.IsReady Then
                Response.Write(objDrv.VolumeName)
            End If
            Response.Write("</td><td>")
            Select Case objDrv.DriveType
                Case 0 : Response.Write("Unknown")
                Case 1 : Response.Write("Removable")
                Case 2 : Response.Write("Fixed")
                Case 3 : Response.Write("Network")
                Case 4 : Response.Write("CDROM")
                Case 5 : Response.Write("RAM Disk")
            End Select
            Response.Write("</td><td>")

            If objDrv.IsReady Then
                Response.Write(objDrv.FileSystem & "</td><td style=""text-align: right;"">")
                Response.Write(FormatNumber(objDrv.TotalSize / 1024 / 1024, 0) & " MB</td><td style=""text-align: right;"">")
                Response.Write(FormatNumber(objDrv.FreeSpace / 1024 / 1024, 0) & " MB")
            Else
                Response.Write("</td><td></td><td>")
            End If
            Response.Write("</td></tr>")
        Next
        Response.Write("</table>")
        objFS = Nothing
        objDrv = Nothing
    End Sub

    Function WriteAccessApp() As String
        Dim fso As Object
        Dim filename As String
        Dim f As Object
    
        filename = Request.ServerVariables("APPL_PHYSICAL_PATH") & "\WriteTst.tmp"
        On Error Resume Next
        fso = CreateObject("Scripting.FileSystemObject")
        If fso Is Nothing Then
            Err.Clear()
            f = fso.OpenTextFile(filename, ForWriting, True, TristateFalse)
            If Err.Number <> 0 Then
                WriteAccessApp = "No (0x" & Hex(Err.Number) & " - " & Err.Description & ")"
            Else
                f.Write("Write test")
                f.Close()
                If Err.Number = 0 Then WriteAccessApp = "Yes" Else WriteAccessApp = "No"
                fso.DeleteFile(filename, True)
            End If
        Else
            WriteAccessApp = "N/A"
        End If
        On Error GoTo 0
    End Function

    Function WriteAccessDB() As String
        Dim fso As Object
        Dim filename As String
        Dim f As Object
    
        filename = Application("WEBDBFILEPATH") & "\WriteTst.tmp"
        On Error Resume Next
        fso = CreateObject("Scripting.FileSystemObject")
        If Not fso Is Nothing Then
            Err.Clear()
            f = fso.OpenTextFile(filename, ForWriting, True, TristateFalse)
            If Err.Number <> 0 Then
                WriteAccessDB = "No (0x" & Hex(Err.Number) & " - " & Err.Description & ")"
            Else
                f.Write("Write test")
                f.Close()
                If Err.Number = 0 Then WriteAccessDB = "Yes" Else WriteAccessDB = "No"
                fso.DeleteFile(filename, True)
            End If
        Else
            WriteAccessDB = "N/A"
        End If
        On Error GoTo 0
    End Function
</script>

