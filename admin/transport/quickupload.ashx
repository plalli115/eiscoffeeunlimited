<%@ WebHandler Language="VB" Class="quickupload" %>

Imports ExpandIT.ExpandITLib
Imports System
Imports System.Web
Imports System.Collections.Generic
Imports System.Data.SqlClient


Public Class quickupload : Implements IHttpHandler

    Dim ResponseDom As System.Xml.XmlDocument
    Dim DidRemove As Boolean
    Dim conn As Data.SqlClient.SqlConnection
    Dim SafeValDT As Data.DataTable

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim RequestDom As System.Xml.XmlDocument
        Dim RecordNode As System.Xml.XmlNode
        Dim Action As String
        Dim TableName As String
        Dim myCommand As Data.SqlClient.SqlCommand
        Dim SafeValReader As Data.SqlClient.SqlDataReader
        Dim sql As String
        Dim TempString As String
        Dim RemoteAddr As String
        Dim FoundIP As Boolean
        Dim IPFile As IO.StreamReader
        Dim IPLine As String
        
        context.Response.ContentType = "text/plain"

        REM -- Make sure we do not run out of time. Set timeout to 5 min
        context.Server.ScriptTimeout = 300

        RemoteAddr = context.Request.ServerVariables("REMOTE_ADDR")

        REM -- Initialize shop application
        conn = New System.Data.SqlClient.SqlConnection

        Try
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ExpandITConnectionString").ConnectionString
            conn.Open()
        Catch ex As SqlException
            ErrorState(context, "Error opening connection to database", ex.ErrorCode, ex.Message)
        End Try
        
        REM -- Make XML document
        ResponseDom = New System.Xml.XmlDocument
        
        RequestDom = New System.Xml.XmlDocument
        RequestDom.Load(context.Request.InputStream)

        Action = RequestDom.SelectSingleNode("/XML/Control/Action").InnerText

        Select Case UCase(Action)
            Case "IP"
                ResponseDom.LoadXml("<XML><IP>" & RemoteAddr & "</IP></XML>")
                context.Response.ContentType = "text/xml"
                ResponseDom.Save(context.Response.OutputStream)
                context.Response.End()
            Case "UPLOAD"
                ResponseDom.LoadXml("<XML><Code>0</Code><Message>Success</Message></XML>")
        End Select

        REM -- Validate IP address

        FoundIP = False
        Try
            IPFile = New IO.StreamReader(HttpContext.Current.Server.MapPath("~") & "\admin\transport\ipfilterstatic.txt")
            While Not IPFile.EndOfStream
                IPLine = IPFile.ReadLine
                If IPLine = RemoteAddr Then
                    FoundIP = True
                End If
            End While
            IPFile.Close()
            IPFile = Nothing
        Catch ex As Exception
            Try
                IPFile = New IO.StreamReader(HttpContext.Current.Server.MapPath("~") & "\admin\transport\ipfilter.txt")
                While Not IPFile.EndOfStream
                    IPLine = IPFile.ReadLine
                    If IPLine = RemoteAddr Then
                        FoundIP = True
                    End If
                End While
                IPFile.Close()
                IPFile = Nothing
            Catch ex1 As Exception
            End Try
        End Try
        If Not FoundIP Then
            ErrorState(context, "Access Denied", 1, _
            "Access blocked by IP filter. If you need to test the 'quickupload.ashx' page " & _
            "you should add your ip-address [" & RemoteAddr & "] to the 'ipfilterstatic.txt' file in the '/shoplocal/admin/transport' folder. " & _
            "After a normal upload you should have access to the page")
        End If

        ' Get Tablename
        TableName = CStrEx(RequestDom.SelectSingleNode("/XML/Table").Attributes.GetNamedItem("name").Value)

        sql = "SELECT * FROM " & TableName & " WHERE 1=2"
        Try
            myCommand = New SqlCommand(sql, conn)
            SafeValReader = myCommand.ExecuteReader
            myCommand = Nothing
            SafeValDT = New Data.DataTable
            SafeValDT.Load(SafeValReader)
            SafeValReader.Close()
            SafeValReader = Nothing
        Catch ex As SqlException
            ErrorState(context, "Error reading table schema", ex.ErrorCode, ex.Message)
        End Try
        
        DidRemove = False

        For Each RecordNode In RequestDom.SelectNodes("/XML/Table/Records/Record")
            TempString = ""
            If Not UpdateTable(context, TableName, RequestDom.SelectNodes("/XML/Table/Key/Fields/Field"), RecordNode, TempString) Then
                ErrorState(context, "Error updating table [" & TableName & "]", -1, TempString)
            End If
        Next

        context.Response.ContentType = "text/xml"
        ResponseDom.Save(context.Response.OutputStream)
        context.Response.End()

        ' Done
    End Sub

    Function UpdateTable(ByVal context As HttpContext, ByVal TableName As String, ByVal keyfields As Xml.XmlNodeList, ByVal rownode As Xml.XmlNode, ByRef Reason As String) As Boolean
        Dim sql As String
        Dim setlist As String = ""
        Dim KeyValue As Xml.XmlNode
        Dim FieldNode As Xml.XmlNode
        Dim fieldlist As String = ""
        Dim valuelist As String = ""
        Dim keylist As String = ""
        Dim FieldName As String
        Dim tmpRs As SqlDataReader, localRC As Integer
        Dim myCommand As Data.SqlClient.SqlCommand
        Dim RowAction As String = ""

        Try
            For Each KeyValue In keyfields
                If keylist <> "" Then
                    keylist = keylist & " AND "
                End If
                FieldNode = rownode.SelectSingleNode("Field[@name='" & KeyValue.InnerText & "']")
                If CStrEx(FieldNode.Attributes.GetNamedItem("isnull")) <> "" Then
                    keylist = keylist & "[" & KeyValue.InnerText & "] IS NULL"
                Else
                    keylist = keylist & "[" & KeyValue.InnerText & "]=" & SafeVal(KeyValue.InnerText, FieldNode.InnerText)
                End If
            Next
        Catch ex As Exception
            Reason = "Error building Key List. Err: " & ex.Message
            UpdateTable = False
            Exit Function
        End Try
    
        Try
            For Each FieldNode In rownode.SelectNodes("*")
                If setlist <> "" Then
                    setlist = setlist & ", "
                    fieldlist = fieldlist & ", "
                    valuelist = valuelist & ", "
                End If
                FieldName = FieldNode.Attributes.GetNamedItem("name").Value
                fieldlist = fieldlist & "[" & FieldName & "]"
                If CStrEx(FieldNode.Attributes.GetNamedItem("isnull")) <> "" Then
                    setlist = setlist & "[" & FieldName & "]=Null"
                    valuelist = valuelist & "Null"
                Else
                    setlist = setlist & "[" & FieldName & "]=" & SafeVal(FieldName, FieldNode.InnerText)
                    valuelist = valuelist & SafeVal(FieldName, FieldNode.InnerText)
                End If
            Next
        Catch ex As Exception
            Reason = "Error building Field Lists. Err: " & ex.Message
            UpdateTable = False
            Exit Function
        End Try
        
        If Not rownode.Attributes.GetNamedItem("action") Is Nothing Then
            If CStrEx(rownode.Attributes.GetNamedItem("action").Value) = "remove" Then
                RowAction = "remove"
            End If
        End If
        If RowAction = "remove" Then
            REM -- Try Delete
            sql = "DELETE FROM [" & TableName & "] WHERE " & keylist
            Try
                myCommand = New Data.SqlClient.SqlCommand(sql, conn)
                myCommand.ExecuteNonQuery()
                myCommand = Nothing
            Catch ex As SqlException
                Reason = "Error removing record in table '" & TableName & "'. Err: (" & ex.ErrorCode & ") " & ex.Message
                UpdateTable = False
                Exit Function
            End Try
            DidRemove = True
        Else
            Try
                myCommand = New Data.SqlClient.SqlCommand("SELECT Count(*) AS RecordCount FROM [" & TableName & "] WHERE " & keylist, conn)
                tmpRs = myCommand.ExecuteReader
                tmpRs.Read()
                localRC = tmpRs("RecordCount")
                tmpRs.Close()
                tmpRs = Nothing
                myCommand = Nothing
            Catch ex As SqlException
                Reason = "Error checking records in table '" & TableName & "'. Err: (" & ex.ErrorCode & ") " & ex.Message
                UpdateTable = False
                Exit Function
            End Try
            If localRC = 0 Then
                REM -- Try Insert
                sql = "INSERT INTO [" & TableName & "] (" & fieldlist & ") VALUES (" & valuelist & ")"
                Try
                    myCommand = New Data.SqlClient.SqlCommand(sql, conn)
                    myCommand.ExecuteNonQuery()
                    myCommand = Nothing
                Catch ex As SqlException
                    Reason = "Error inserting record in table '" & TableName & "'. Err: " & ex.Message & " (" & ex.Number & ")"
                    UpdateTable = False
                    Exit Function
                End Try
            ElseIf localRC = 1 Then
                REM -- Try update
                sql = "UPDATE [" & TableName & "] SET " & setlist & " WHERE " & keylist
                Try
                    myCommand = New Data.SqlClient.SqlCommand(sql, conn)
                    myCommand.ExecuteNonQuery()
                    myCommand = Nothing
                Catch ex As SqlException
                    Reason = "Error updating record in table '" & TableName & "'. Err: " & ex.Message & " (" & ex.Number & ")"
                    UpdateTable = False
                    Exit Function
                End Try
            Else ' More than on record with this key - Error
                Reason = "Duplicate keys in the table. Keylist: " & keylist
                UpdateTable = False
                Exit Function
            End If
        End If
    
        UpdateTable = True
    End Function

    Function SafeVal(ByVal FieldName As String, ByVal Value As Object) As String
        Dim retv As String
        Select Case SafeValDT.Columns(FieldName.ToString).DataType.Name
            Case "String"
                retv = SafeStringQU(Value)
            Case "Date", "DateTime", "Time"
                retv = SafeDatetime(Value)
            Case Else
                REM -- The rest is formated in XML Document
                retv = Value
        End Select
        SafeVal = retv
    End Function

    Sub ErrorState(ByVal context As HttpContext, ByVal msg As String, ByVal errno As Object, ByVal errdesc As String)
        ResponseDom.SelectSingleNode("/XML/Code").InnerText = errno.ToString
        ResponseDom.SelectSingleNode("/XML/Message").InnerText = msg & "; " & errdesc
        context.Response.ContentType = "text/xml"
        ResponseDom.Save(context.Response.OutputStream)
        context.Response.End()
    End Sub

    Function SafeStringQU(ByVal s As Object) As String
        If IsNull(s) Then
            SafeStringQU = "NULL"
        Else
            SafeStringQU = "'" & Replace(BackslashDecode(s), "'", "''") & "'"
        End If
    End Function

    Function BackslashDecode(ByVal InString As String) As String
        Dim Res As String
        Dim i As Integer
    
        Res = ""
        i = InStr(1, InString, "\")
        While i <> 0 And Len(InString) > 0
            If i = Len(InString) Then
                Res = Res & InString
                InString = ""
            Else
                Select Case Mid(InString, i + 1, 1)
                    Case "\"
                        Res = Res & Left(InString, i - 1) & "\"
                        InString = Mid(InString, i + 2)
                    Case "r"
                        Res = Res & Left(InString, i - 1) & vbCr
                        InString = Mid(InString, i + 2)
                    Case "n"
                        Res = Res & Left(InString, i - 1) & vbLf
                        InString = Mid(InString, i + 2)
                    Case "t"
                        Res = Res & Left(InString, i - 1) & vbTab
                        InString = Mid(InString, i + 2)
                    Case Else
                        Res = Res & Left(InString, i)
                        InString = Mid(InString, i + 1)
                End Select
            End If
            i = InStr(1, InString, "\")
        End While
        Res = Res & InString
    
        BackslashDecode = Res
    End Function
 
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class