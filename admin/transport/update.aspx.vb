
Partial Class Update
    Inherits System.Web.UI.Page

    Protected Sub PostUpload(ByVal TableArray As ArrayList) Handles Update1.Post_Upload
        Response.Write("Post_Upload Handler;")
        Update1.ResetIsCalculated()
        ExpandIT31.ExpanditFramework.Infrastructure.CacheManager.TablesRecreated(TableArray)
    End Sub

    Protected Sub PostDownload(ByVal TableArray As ArrayList) Handles Update1.Post_Download
        Response.Write("Post_Download Handler;")
        Update1.CleanUpCarts()

        Update1.QuickDownload_CleanUp()
    End Sub

End Class
