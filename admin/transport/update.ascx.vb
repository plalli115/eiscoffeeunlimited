Imports System.Text
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports ExpandIT.ExpandITLib

Partial Class UpdateControl
    Inherits System.Web.UI.UserControl

    Public Event Pre_Upload()
    Public Event Post_Upload(ByVal TableArray As ArrayList)
    Public Event Pre_Download()
    Public Event Post_Download(ByVal TableArray As ArrayList)
    Public Event Pre_Cleanup()
    Public Event Post_Cleanup()
    Public Event Pre_Update()
    Public Event Post_Update()

    Private Version as Integer = 0


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		Dim remoteaddr As String
		Dim FoundIP As Boolean
		Dim IPFile As IO.StreamReader
		Dim IPLine As String
		
        ' Make sure we do not run out of time. Set timeout to 5 min
        Server.ScriptTimeout = 300

		remoteaddr = Request.ServerVariables("REMOTE_ADDR")

        REM -- Used to find ip-address of Editors Workstation
        If GetRequest("ip") <> "" Then
            Response.Write(remoteaddr)
            Response.End()
		Else
			REM -- Validate IP address

			FoundIP = False
			Try
				IPFile = New IO.StreamReader(HttpContext.Current.Server.MapPath("~") & "\admin\transport\ipfilterstatic.txt")
				While Not IPFile.EndOfStream
					IPLine = IPFile.ReadLine
					If IPLine = remoteaddr Then
						FoundIP = True
					End If
				End While
				IPFile.Close()
				IPFile = Nothing
			Catch ex1 As Exception
				Try
					IPFile = New IO.StreamReader(HttpContext.Current.Server.MapPath("~") & "\admin\transport\ipfilter.txt")
					While Not IPFile.EndOfStream
						IPLine = IPFile.ReadLine
						If IPLine = remoteaddr Then
							FoundIP = True
						End If
					End While
					IPFile.Close()
					IPFile = Nothing
				Catch ex2 As Exception
				End Try
			End Try
			If Not FoundIP Then
				DebugText("ERRORSTRING=Access Denied. " & _
				"Access blocked by IP filter." & _
				"You can add your ip-address [" & remoteaddr & "] to the 'ipfilterstatic.txt' file in the '/shoplocal/admin/transport' folder. " & _
				"After a normal upload you should have access to the page")
				Response.End()
			End If
        End If
		
        ' Call trigger defined in update_triggers.asp
        RaiseEvent Pre_Update()
        ' Initialize shop application

        '
        ' Initialization Code
        '

        ' Output start message
        DebugText("Start")

        '
        ' Run the requested actions
        '

        If GetRequest("version") <> "" Then
            Try
                Version = Cint(GetRequest("version"))
            Catch ex as Exception
            End Try
        End If

        ' Update Web Site
        If GetRequest("upload") = "1" Then Upload()

        ' Update transport database
        If GetRequest("download") = "1" Then Download()

        ' Clean up
        If GetRequest("cleanup") = "1" Then Cleanup()

        ' Compact database
        If GetRequest("compact") = "1" Then Compact()

        '
        ' Finalize
        '

        DebugText("Calling Post_Update trigger")
        RaiseEvent Post_Update()
        DebugText("Returned from Post_Update trigger")

        ' This stream MUST end with Done as it's last four characters.
        ' The calling application checks for this string in the end
        ' of the HTTP response.
        Response.Write("Done")
        Response.End()
    End Sub

    ' ************************************************************************
    '
    '
    ' FUNCTIONS FOR MAIN ACTIONS
    '
    '
    ' ************************************************************************

    '
    ' Upload Request
    '

    Private Sub Upload()
        '        Dim connWeb As New System.Data.SqlClient.SqlConnection
        Dim QueryDict As Object = QueryString2Dict()
        Dim UploadList As ArrayList = QueryDict("uploadtables")
        If UploadList Is Nothing Then UploadList = New ArrayList()
        Dim Result As String
        Dim RenameStrings As String
        Dim RenameStringsCollection As String
        Dim RenameArray As String()
        Dim i As Integer
        Dim Conn As SqlConnection
        Dim Command As SqlCommand
        Dim Transact As SqlTransaction

        DebugText("Upload")

        ' Call trigger
        DebugText("Calling Pre_Upload trigger")
        RaiseEvent Pre_Upload()
        DebugText("Returned from Pre_Upload trigger")

        ' Copy tables from transport files
        RenameStringsCollection = ""
        For i = 0 To UploadList.Count - 1
            Result = ""
            RenameStrings = ""
            DebugText("Copying [" & UploadList(i) & "]")
            If Not ExpandIT.DataCopy.CopyFile2DB(MapPath("~") & "\App_Data\" & UploadList(i), ConfigurationManager.ConnectionStrings("ExpandITConnectionString").ConnectionString, Iif(Version >= 378, "$transport$", "$upload$") & UploadList(i), Result, RenameStrings) Then
                DebugText("ERRORSTRING=Error copying " & UploadList(i) & ". Err: " & Result)
            Else
                If RenameStrings <> "" Then
                    If RenameStringsCollection <> "" Then
                        RenameStringsCollection = RenameStringsCollection & vbCrLf
                    End If
                    RenameStringsCollection = RenameStringsCollection & RenameStrings
                End If
            End If
        Next

        Conn = New SqlConnection(ConfigurationManager.ConnectionStrings("ExpandITConnectionString").ConnectionString)
        Conn.Open()
        RenameArray = Split(RenameStringsCollection, vbCrLf)
        DebugText("Starting Rename Transaction - " & Now)
        Transact = Conn.BeginTransaction()
        Try
            For i = LBound(RenameArray) To UBound(RenameArray)
                Command = New SqlCommand(RenameArray(i), Conn, Transact)
                Command.ExecuteNonQuery()
                Command = Nothing
            Next
            Transact.Commit()
            DebugText("Rename Transaction succeeded - " & Now)
        Catch ex As Exception
            DebugText("ERRORSTRING=Error renaming tables (transaction) [" & RenameArray(i) & "]. Err: " & ex.Message)
            Transact.Rollback()
        End Try
        Conn.Close()
        Conn = Nothing

        ' Call trigger
        DebugText("Calling Post_Upload trigger")
        RaiseEvent Post_Upload(UploadList)
        DebugText("Returned from Post_Upload trigger")
    End Sub

    '
    ' Download Request
    '

    Sub Download()
        Dim QueryDict As Object = QueryString2Dict()
        Dim DownloadList As ArrayList = QueryDict("downloadtables")
        If DownloadList Is Nothing Then DownloadList = New ArrayList()
        Dim Result As String

        DebugText("Download")

        ' Call trigger
        DebugText("Calling Pre_Download trigger")
        RaiseEvent Pre_Download()
        DebugText("Returned from Pre_Download trigger")

        ' Create tables to be transported
        For i As Integer = 0 To DownloadList.Count - 1
            Result = ""
            If GetRequest("notemptables") = "1" Or Version < 378 Then
            If Not ExpandIT.DataCopy.CopyDB2File(ConfigurationManager.ConnectionStrings("ExpandITConnectionString").ConnectionString, DownloadList(i), MapPath("~") & "\App_Data\" & DownloadList(i), 10000, Result) Then
                DebugText("ERRORSTRING=Error copying " & DownloadList(i) & ". Err: " & Result)
                End If
            Else
                If Not ExpandIT.DataCopy.CopyDB2File(ConfigurationManager.ConnectionStrings("ExpandITConnectionString").ConnectionString, DownloadList(i), MapPath("~") & "\App_Data\" & DownloadList(i), 10000, Result,"", True) Then
                    DebugText("ERRORSTRING=Error copying " & DownloadList(i) & ". Err: " & Result)
                End If
            End If
        Next

        ' Call trigger
        DebugText("Calling Post_Download trigger")
        RaiseEvent Post_Download(DownloadList)
        DebugText("Returned from Post_Download trigger")
    End Sub

    '
    ' Cleanup Request
    '

    Sub Cleanup()
        Dim connWeb As New System.Data.SqlClient.SqlConnection

        Try
            DebugText("Calling Pre_Cleanup trigger")
            RaiseEvent Pre_Cleanup()
            DebugText("Returned from Pre_Cleanup trigger")

            Try
                connWeb.ConnectionString = ConfigurationManager.ConnectionStrings("ExpandITConnectionString").ConnectionString
                connWeb.Open()
            Catch ex As Exception
                Response.Write("ERRORSTRING=[Cleanup] Error connecting to database. Err: " & ex.Message & ";")
                Response.End()
                Exit Sub
            End Try

            ' Remove configuration table if available
            ' When doing a synchronize all, the local
            ' web.mdb is transferred to the web hotel
            ' containing the configuration table
            ' (including passwords etc.). This table
            ' should be removed for security reasons.
            Try
                DropTable(connWeb, "Configuration")
            Finally
            End Try

            CreateIndexes(connWeb)

            connWeb.Close()
            connWeb = Nothing

            DebugText("Calling Post_Cleanup trigger")
            RaiseEvent Post_Cleanup()
            DebugText("Returned from Post_Cleanup trigger")
        Catch ex As Exception
            Response.Write("ERRORSTRING=[Cleanup] Unhandled error. Err: " & ex.Message)
            Response.End()
        End Try
    End Sub

    '
    ' Compact Request
    '

    Sub Compact()
    End Sub

    ' ************************************************************************
    '
    '
    ' OTHER FUNCTIONS
    '
    '
    ' ************************************************************************

    '
    ' Drop table in database
    '

    Function DropTable(ByVal Conn As SqlConnection, ByVal sTablename As String) As Boolean
        Dim retry As Integer
        Dim ErrNum As Integer
        Dim myCommand As SqlCommand

        DebugText("Drop " & sTablename)
        retry = 0
        Do
            If retry > 0 Then System.Threading.Thread.Sleep(1000)
            Try
                retry = retry + 1
                myCommand = New SqlCommand("DROP TABLE [" & sTablename & "]", Conn)
                myCommand.ExecuteNonQuery()
                myCommand = Nothing
            Catch ex As SqlException
                DebugText("Drop returned: Error=" & ex.ErrorCode & "; Reason=" & ex.Message)
                ErrNum = ex.ErrorCode
            Catch ex As Exception
                DebugText("Drop returned: Error=N/A; Reason=" & ex.Message)
                ErrNum = -1
            End Try
        Loop Until (ErrNum = 0) Or (retry > 3) Or (ErrNum = 5) Or (ErrNum = -2147217900)
        If (ErrNum <> 5) And (ErrNum <> -2147217900) And (ErrNum <> 0) Then
            Return False
        Else
            Return True
        End If
    End Function

    '
    ' This function can determine if a table is in the set of uploaded tables.
    '

    Function IsTableUploaded(ByVal tablename As String) As Boolean
        Dim retval As Boolean
        Dim querydict As Object = QueryString2Dict()
        Dim uploadlist As ArrayList = querydict("uploadtables")
        If uploadlist Is Nothing Then uploadlist = New ArrayList()

        retval = False
        retval = uploadlist.Contains(tablename)

        IsTableUploaded = retval
    End Function

    '
    ' Execute a SQL statement. Don't mind if it fails. Just write it to the log.
    '

    Sub SafeExecute(ByVal conn As SqlConnection, ByVal SQLStatement As String)
        Dim myCommand As SqlCommand

        myCommand = conn.CreateCommand
        myCommand.CommandText = SQLStatement
        Try
            myCommand.ExecuteNonQuery()
        Catch ex As Exception
            DebugText("SQL Statement '" & SQLStatement & "' failed. Reason=" & ex.Message)
        End Try
        myCommand = Nothing
    End Sub

    '
    ' Run all statements specified in the IndexTable.
    '

    Sub CreateIndexes(ByVal Conn As SqlConnection)
        Dim localTableName As String
        Dim DA As SqlDataAdapter
        Dim DT As New Data.DataTable
        Dim DR As Data.DataRow

        ' Read the statements from the database table
        Try
            DA = New SqlDataAdapter("SELECT * FROM IndexTable ORDER BY RunSequence", Conn)
            DA.Fill(DT)
            DA = Nothing
        Catch ex As Exception
            DebugText("Error reading IndexTable. Err: " & ex.Message)
            Exit Sub
        End Try

        ' Run the statements
        For Each DR In DT.Rows
            localTableName = DR("TableName")
            If localTableName <> "" Then
                If IsTableUploaded(localTableName) Then
                    SafeExecute(Conn, DR("SQLStatement"))
                End If
            Else
                SafeExecute(Conn, DR("SQLStatement"))
            End If
        Next
        DR = Nothing
    End Sub


    ' This function resets IsCalculated on the Cart tables after upload (in case prices have changed)
    Public Sub ResetIsCalculated()
        Dim Conn As Data.SqlClient.SqlConnection
        Dim myCommand As Data.SqlClient.SqlCommand

        Try
            DebugText("Resetting IsCalculated")
            Try
                Conn = New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ExpandITConnectionString").ConnectionString)
                Conn.Open()
            Catch ex As Exception
                Response.Write("ERRORSTRING=[ResetIsCalculated] Error opening connection to database. Err: " & ex.Message & ";")
                Response.End()
                Exit Sub
            End Try

            ' Set IsCalculated = NULL in Cart tables
            Try
                myCommand = Conn.CreateCommand()
                myCommand.CommandText = "UPDATE CartHeader SET IsCalculated = NULL"
                myCommand.ExecuteNonQuery()
                myCommand = Nothing
            Catch ex As Exception
                Response.Write("ERRORSTRING=[ResetIsCalculated] Error resetting IsCalculated on CartHeader. Err: " & ex.Message & ";")
                Response.End()
                Exit Sub
            End Try

            Try
                myCommand = Conn.CreateCommand()
                myCommand.CommandText = "UPDATE CartLine SET IsCalculated = NULL"
                myCommand.ExecuteNonQuery()
                myCommand = Nothing
            Catch ex As Exception
                Response.Write("ERRORSTRING=[ResetIsCalculated] Error resetting IsCalculated on CartLine. Err: " & ex.Message & ";")
                Response.End()
                Exit Sub
            End Try

            Conn.Close()
        Catch ex As Exception
            Response.Write("ERRORSTRING=[ResetIsCalculated] Unhandled error. Err: " & ex.Message & ";")
            Response.End()
        Finally
            Conn = Nothing
        End Try
    End Sub



    ' This function deletes old carts.
    Public Sub CleanUpCarts()
        Dim Conn As SqlConnection
        Dim timeStamp As String, cart_lifetime As Integer
        Dim myCommand As SqlCommand
        Dim SQLStatement As String

        Try
            DebugText("Cleaning up old carts.")

            cart_lifetime = CIntEx(System.Configuration.ConfigurationManager.AppSettings("CART_LIFETIME"))

            If cart_lifetime > 0 Then
                Try
                    Conn = New SqlConnection
                    Conn.ConnectionString = ConfigurationManager.ConnectionStrings("ExpandITConnectionString").ConnectionString
                    Conn.Open()
                Catch ex As Exception
                    Response.Write("ERRORSTRING=[CleanUpCarts] Error connecting to database. Err: " & ex.Message & ";")
                    Response.End()
                    Exit Sub
                End Try

                timeStamp = SafeDatetime(Now.AddDays(-CInt(cart_lifetime)))

                Try
                    SQLStatement = "DELETE FROM CartLine WHERE LineGuid IN (SELECT LineGuid FROM CartHeader LEFT JOIN CartLine ON CartHeader.HeaderGuid = CartLine.HeaderGuid WHERE CartHeader.ModifiedDate < " & timeStamp & ")"
                    DebugText("Delete Cart lines: " & SQLStatement)
                    myCommand = Conn.CreateCommand
                    myCommand.CommandText = SQLStatement
                    myCommand.ExecuteNonQuery()
                    myCommand = Nothing
                Catch ex As Exception
                    Response.Write("ERRORSTRING=[CleanUpCarts] Error removing records from CartLine. Err: " & ex.Message & ";")
                    Response.End()
                    Exit Sub
                End Try

                Try
                    SQLStatement = "DELETE FROM CartHeader WHERE ModifiedDate <= " & timeStamp
                    DebugText("Delete Cart headers: " & SQLStatement)
                    myCommand = Conn.CreateCommand
                    myCommand.CommandText = SQLStatement
                    myCommand.ExecuteNonQuery()
                    myCommand = Nothing
                Catch ex As Exception
                    Response.Write("ERRORSTRING=[CleanUpCarts] Error removing records from CartHeader. Err: " & ex.Message & ";")
                    Response.End()
                    Exit Sub
                End Try

                DebugText("Done cleaning up old carts.")
                Conn.Close()
                Conn = Nothing
            Else
                DebugText("CART_LIFETIME = " & cart_lifetime & ". Nothing done.")
            End If
        Catch ex As Exception
            Response.Write("ERRORSTRING=[CleanUpCarts] Unhandled error. Err: " & ex.Message & ";")
            Response.End()
            Exit Sub
        Finally
            Conn = Nothing
        End Try

    End Sub

    '
    ' Output a debug message
    '

    Sub DebugText(ByVal s As String)
        Response.Write(s & "; ")
    End Sub


    Public Sub QuickDownload_CleanUp()
        Dim orders As Boolean, orderlines As Boolean, shoppers As Boolean, i As Integer
        Dim req As Dictionary(Of String, ArrayList) = QueryString2Dict()

        Try
            DebugText("Marking all Orders as QuickDownloaded")

            ' Check if orders, orderlines and shoppers was downloaded
            orders = False
            orderlines = False
            shoppers = False
            For i = 0 To req("downloadtables").Count - 1
                If LCase(req("downloadtables")(i)) = "shopsalesheader" Then orders = True
                If LCase(req("downloadtables")(i)) = "shopsalesline" Then orderlines = True
                If LCase(req("downloadtables")(i)) = "usertable" Then shoppers = True
            Next

            If orders And orderlines And shoppers Then
                QuickDownloadMarkAll()
            End If

        Catch ex As Exception
            Response.Write("ERRORSTRING=[QuickDownload_CleanUp] Unhandled error. Err: " & ex.Message & ";")
            Response.End()
        End Try
    End Sub

    Sub QuickDownloadMarkAll()
        Dim Conn As SqlConnection
        Dim myCommand As SqlCommand

        Try
            Try
                Conn = New SqlConnection
                Conn.ConnectionString = ConfigurationManager.ConnectionStrings("ExpandITConnectionString").ConnectionString
                Conn.Open()
            Catch ex As Exception
                Response.Write("ERRORSTRING=[QuickDownloadMarkAll] Error opening connection to database. Err. " & ex.Message & ";")
                Response.End()
                Exit Sub
            End Try

            ' Add column to ShopSalesHeader table
            Try
                myCommand = Conn.CreateCommand
                myCommand.CommandText = "ALTER TABLE [ShopSalesHeader] ADD QuickDownload DATETIME"
                myCommand.ExecuteNonQuery()
                myCommand = Nothing
            Catch ex As SqlException
                If ex.ErrorCode <> -2146232060 Then
                    Response.Write("ERRORSTRING=[QuickDownloadMarkAll] Error adding Quickdownload column to ShopSalesHeader Table. Err: " & ex.ErrorCode & " - " & ex.Message & ";")
                    Response.End()
                    Exit Sub
                Else
                    myCommand = Nothing
                End If
            Catch ex As Exception
                Response.Write("ERRORSTRING=[QuickDownloadMarkAll] Error adding Quickdownload column to ShopSalesHeader Table. Err: " & ex.Message & ";")
                Response.End()
                Exit Sub
            End Try

            ' Update all rows in ShopSalesHeader table
            Try
                myCommand = Conn.CreateCommand
                myCommand.CommandText = "UPDATE [ShopSalesHeader] SET QuickDownload=" & SafeDatetime(Now) & " WHERE QuickDownload IS NULL"
                myCommand.ExecuteNonQuery()
                myCommand = Nothing
            Catch ex As Exception
                Response.Write("ERRORSTRING=[QuickDownloadMarkAll] Error setting Quickdownload timestamps in table ShopSalesHeader. Err: " & ex.Message & ";")
                Response.End()
                Exit Sub
            End Try

            Conn.Close()
            Conn = Nothing
        Catch ex As Exception
            Response.Write("ERRORSTRING=[QuickDownloadMarkAll] Unhandled error. Err: " & ex.Message & ";")
            Response.End()
            Exit Sub
        Finally
            Conn = Nothing
        End Try

    End Sub

    Public Shared Function GetRequest(ByVal varname As String) As String
        Dim retv As String = ""

        Try
            If Not HttpContext.Current.Request(varname) Is Nothing Then retv = HttpContext.Current.Request(varname)
        Catch
            retv = ""
        End Try

        Return retv
    End Function

    Public Shared Function QueryString2Dict() As Dictionary(Of String, ArrayList)
        Dim q, f, s As String

        q = HttpContext.Current.Request.QueryString.ToString
        f = HttpContext.Current.Request.Form.ToString

        s = q
        If f <> "" Then
            If s <> "" Then s = s & "&"
            s = s & f
        End If

        Return QueryString2Dict(s)
    End Function

    Public Shared Function QueryString2Dict(ByVal s As String) As Dictionary(Of String, ArrayList)
        Dim retv As Dictionary(Of String, ArrayList) = New Dictionary(Of String, ArrayList)
        Dim a() As String
        Dim e As String
        Dim n, v As String
        Dim i As Integer
        Dim stringlist As ArrayList

        If s = "" Then Return retv

        a = s.Split("&")

        For Each e In a
            i = e.IndexOf("=")
            If i >= 0 Then
                n = e.Substring(0, i)
                v = e.Substring(i + 1)
            Else
                n = e
                v = ""
            End If
            n = HttpUtility.UrlDecode(n)
            v = HttpUtility.UrlDecode(v)

            If retv.ContainsKey(n) Then
                stringlist = retv(n)
            Else
                stringlist = New ArrayList()
                retv.Add(n, stringlist)
            End If
            stringlist.Add(v)
        Next

        Return retv
    End Function

    Public Shared Function SafeDatetime(ByVal d As Object) As String
        Dim retv As String, dateval As Object

        dateval = DBNull.Value
        Try
            dateval = CDate(d)
        Catch
        End Try

        If Not dateval Is DBNull.Value Then
            retv = "{ts '" & Year(dateval).ToString.PadLeft(4, "0") & "-" & Month(dateval).ToString.PadLeft(2, "0") & "-" & Day(dateval).ToString.PadLeft(2, "0") & " " & Hour(dateval).ToString.PadLeft(2, "0") & ":" & Minute(dateval).ToString.PadLeft(2, "0") & ":" & Second(dateval).ToString.PadLeft(2, "0") & "'}"
        Else
            retv = "NULL"
        End If
        SafeDatetime = retv
    End Function

End Class

