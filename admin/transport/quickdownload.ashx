<%@ WebHandler Language="VB" Class="quickdownload" %>

Imports System
Imports System.Web
Imports System.Collections.Generic
Imports System.Data.SqlClient

Public Class quickdownload : Implements IHttpHandler
    
    Dim Dom As System.Xml.XmlDocument

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim genericTableName As String, genericKey As String(), genericMaxRecords As Integer
        Dim ack As String, ip As String, qdprofile As String, maxorders As Integer, remoteaddr As String
        Dim rootnode As System.Xml.XmlNode
        Dim idliststr, sql As String, i As Integer
        Dim idlist As String()
        Dim TablesNode, HeaderNode, AttNode, KeyNode, FieldsNode, FieldNode, LineNode, RecordNode, CustomerNode As System.Xml.XmlNode
        Dim AttsNode As System.Xml.XmlAttributeCollection
        Dim LineRecords, CustomerRecords, HeaderRecords As System.Xml.XmlNode
        Dim rsOrder As SqlDataReader = Nothing
        Dim dtOrder As Data.DataTable = Nothing
        Dim rowOrder As Data.DataRow
        Dim rsShopper As SqlDataReader = Nothing
        Dim rsLine As SqlDataReader = Nothing
        Dim cnt As Integer
        Dim j, k As Integer, WhereStatement As String, idvalue As String
        Dim RecordsNode, TableNode As System.Xml.XmlNode
        Dim rsGeneric As SqlDataReader = Nothing
        Dim querydict As Dictionary(Of String, ArrayList) = QueryString2Dict()
        Dim guidlist As ArrayList
        Dim conn As System.Data.SqlClient.SqlConnection
        Dim myCommand As System.Data.SqlClient.SqlCommand
        Dim ID As String
        Dim FoundIP As Boolean
        Dim IPFile As IO.StreamReader
        Dim IPLine As String

        REM -- Make sure we do not run out of time. Set timeout to 5 min
        context.Server.ScriptTimeout = 300

        REM -- These three values defines the settings for QuickDownload of a generic table. The fields in the key should be of type TEXT
        genericTableName = "MyTable"
        genericKey = New String() {"MyField1", "MyField2"}
        genericMaxRecords = 1

        ack = GetRequest("ack")
        ip = GetRequest("ip")
        qdprofile = GetRequest("quickdownload")
        If qdprofile = "" Then
            qdprofile = "1"
        End If
        maxorders = 5

        remoteaddr = context.Request.ServerVariables("REMOTE_ADDR")

        REM -- Used to find ip-address of Editors Workstation
        If ip <> "" Then
            context.Response.Write(remoteaddr)
            context.Response.End()
        End If

        conn = New System.Data.SqlClient.SqlConnection

        Try
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ExpandITConnectionString").ConnectionString
            conn.Open()
        Catch ex As SqlException
            ErrorState(context, "Error opening connection to database", ex.ErrorCode, ex.Message)
        End Try
        
        REM -- Make XML document
        Dom = New System.Xml.XmlDocument
        Dom.LoadXml("<XML><Code>0</Code><Message>Success</Message></XML>")
        rootnode = Dom.DocumentElement

        REM -- Validate IP address

        FoundIP = False
        Try
            IPFile = New IO.StreamReader(HttpContext.Current.Server.MapPath("~") & "\admin\transport\ipfilterstatic.txt")
            While Not IPFile.EndOfStream
                IPLine = IPFile.ReadLine
                If IPLine = remoteaddr Then
                    FoundIP = True
                End If
            End While
            IPFile.Close()
            IPFile = Nothing
        Catch ex As Exception
            Try
                IPFile = New IO.StreamReader(HttpContext.Current.Server.MapPath("~") & "\admin\transport\ipfilter.txt")
                While Not IPFile.EndOfStream
                    IPLine = IPFile.ReadLine
                    If IPLine = remoteaddr Then
                        FoundIP = True
                    End If
                End While
                IPFile.Close()
                IPFile = Nothing
            Catch ex1 As Exception
            End Try
        End Try
        If Not FoundIP Then
            ErrorState(context, "Access Denied", 1, _
            "Access blocked by IP filter. If you need to test the 'quickdownload.ashx' page " & _
            "you should add your ip-address [" & remoteaddr & "] to the 'ipfilterstatic.txt' file in the '/shoplocal/admin/transport' folder. " & _
            "After a normal upload you should have access to the page")
        End If

        Select Case qdprofile
            Case "1"
                If ack <> "" Then
                    guidlist = querydict("ShopSalesHeader")
                    If guidlist Is Nothing Then guidlist = New ArrayList()
                    Dom.LoadXml("<XML><Code>0</Code><Message>Thanks</Message></XML>")
                    idliststr = ""
                    For i = 0 To guidlist.Count - 1
                        If idliststr <> "" Then idliststr = idliststr & ", "
                        idliststr = idliststr & "'" & DecodeID(guidlist(i)) & "'"
                    Next

                    sql = "UPDATE [ShopSalesHeader] SET QuickDownload=" & SafeDatetime(Now) & " WHERE [HeaderGuid] IN (" & idliststr & ")"
                    Try
                        myCommand = New SqlCommand(sql, conn)
                        myCommand.ExecuteNonQuery()
                        myCommand = Nothing
                    Catch ex As SqlException
                        Dom.SelectSingleNode("/XML/Code").Value = ex.ErrorCode
                        Dom.SelectSingleNode("/XML/Message").Value = ex.Message & "; SQL=" & sql

                    End Try

                    context.Response.ContentType = "text/xml"
                    Dom.Save(context.Response.OutputStream)

                Else
                    REM -- Make sure that house-keeping field is in place

                    sql = "SELECT QuickDownload FROM [ShopSalesHeader] WHERE 1=2"
                    Try
                        myCommand = New SqlCommand(sql, conn)
                        myCommand.ExecuteNonQuery()
                        myCommand = Nothing
                    Catch ex As SqlException
                        If ex.ErrorCode = &H80040E10 Then ' QuickDownload field not present
                            sql = "ALTER TABLE [ShopSalesHeader] ADD QuickDownload DATETIME"
                            context.Application.Lock()
                            Try
                                myCommand = New SqlCommand(sql, conn)
                                myCommand.ExecuteNonQuery()
                                myCommand = Nothing
                            Catch ex1 As SqlException
                                context.Application.UnLock()
                                If ex1.ErrorCode <> &H80040E14 And ex1.ErrorCode <> &H80040E21 And ex1.ErrorCode <> 0 Then
                                    ErrorState(context, "Error adding Quickdownload column to ShopSalesHeader Table ", ex1.ErrorCode, ex1.Message)
                                End If
                            End Try
                            ' REM ** eis.AppCacheSet("TableProperties_ShopSalesHeader", "")
                            context.Application.UnLock()
                        Else
                            ErrorState(context, "Error checking for QuickDownload column in ShopSalesHeader Table ", ex.ErrorCode, ex.Message)
                        End If
                    End Try

                    REM -- Prepare DOM
                    TablesNode = rootnode.AppendChild(Dom.CreateElement("Tables"))

                    REM -- ShopSalesHeader Table
                    HeaderNode = TablesNode.AppendChild(Dom.CreateElement("Table"))
                    AttsNode = HeaderNode.Attributes
                    AttNode = AttsNode.SetNamedItem(Dom.CreateAttribute("name"))
                    AttNode.Value = "ShopSalesHeader"
                    KeyNode = HeaderNode.AppendChild(Dom.CreateElement("Key"))
                    FieldsNode = KeyNode.AppendChild(Dom.CreateElement("Fields"))
                    FieldNode = FieldsNode.AppendChild(Dom.CreateElement("Field"))
                    FieldNode.InnerText = "HeaderGuid"
                    HeaderRecords = HeaderNode.AppendChild(Dom.CreateElement("Records"))

                    REM -- ShopSalesLine Table
                    LineNode = TablesNode.AppendChild(Dom.CreateElement("Table"))
                    AttsNode = LineNode.Attributes
                    AttNode = AttsNode.SetNamedItem(Dom.CreateAttribute("name"))
                    AttNode.Value = "ShopSalesLine"
                    KeyNode = LineNode.AppendChild(Dom.CreateElement("Key"))
                    FieldsNode = KeyNode.AppendChild(Dom.CreateElement("Fields"))
                    FieldNode = FieldsNode.AppendChild(Dom.CreateElement("Field"))
                    FieldNode.InnerText = "LineGuid"
                    LineRecords = LineNode.AppendChild(Dom.CreateElement("Records"))

                    REM -- UserTable Table
                    CustomerNode = TablesNode.AppendChild(Dom.CreateElement("Table"))
                    AttsNode = CustomerNode.Attributes
                    AttNode = AttsNode.SetNamedItem(Dom.CreateAttribute("name"))
                    AttNode.Value = "UserTable"
                    KeyNode = CustomerNode.AppendChild(Dom.CreateElement("Key"))
                    FieldsNode = KeyNode.AppendChild(Dom.CreateElement("Fields"))
                    FieldNode = FieldsNode.AppendChild(Dom.CreateElement("Field"))
                    FieldNode.InnerText = "UserGuid"
                    CustomerRecords = CustomerNode.AppendChild(Dom.CreateElement("Records"))

                    REM -- Select new orders
                    sql = "SELECT * FROM [ShopSalesHeader] WHERE QuickDownload IS NULL"
                    Try
                        myCommand = New SqlCommand(sql, conn)
                        rsOrder = myCommand.ExecuteReader
                        myCommand = Nothing
                        dtOrder = New Data.DataTable
                        dtOrder.Load(rsOrder)
                        rsOrder.Close()
                        rsOrder = Nothing
                    Catch ex As SqlException
                        ErrorState(context, "Error selecting new orders for Quickdownload", ex.ErrorCode, ex.Message)
                    End Try

                    REM -- Add orders to xml document
                    cnt = 0
                    For Each rowOrder In dtOrder.Rows
                        cnt = cnt + 1
                        If cnt > maxorders Then
                            Exit For
                        End If

                        RecordNode = HeaderRecords.AppendChild(Dom.CreateElement("Record"))
                        For i = 0 To dtOrder.Columns.Count - 1
                            If LCase(dtOrder.Columns(i).ColumnName) <> "quickdownload" Then
                                FieldNode = RecordNode.AppendChild(Dom.CreateElement("Field"))
                                AttsNode = FieldNode.Attributes
                                AttNode = AttsNode.SetNamedItem(Dom.CreateAttribute("name"))
                                AttNode.Value = dtOrder.Columns(i).ColumnName
                                If rowOrder.Item(i).Equals(DBNull.Value) Then
                                    AttNode = AttsNode.SetNamedItem(Dom.CreateAttribute("isnull"))
                                    AttNode.Value = "1"
                                Else
                                    Try
                                        FieldNode.InnerText = GetFieldText(rowOrder.Item(i))
                                    Catch ex As Exception
                                        ErrorState(context, "Error getting value for field [" & dtOrder.Columns(i).ColumnName & "] in table [ShopSalesHeader]", 1024, ex.Message)
                                    End Try
                                End If
                            End If
                        Next

                        REM -- Add Shopper
                        sql = "SELECT * FROM [UserTable] WHERE [UserGuid]=" & SafeString(rowOrder("UserGuid"))
                        Try
                            myCommand = New SqlCommand(sql, conn)
                            rsShopper = myCommand.ExecuteReader
                            myCommand = Nothing
                        Catch ex As SqlException
                            ErrorState(context, "Error selecting User for Quickdownload", ex.ErrorCode, ex.Message)
                        End Try

                        If rsShopper.HasRows Then
                            rsShopper.Read()
                            RecordNode = CustomerRecords.AppendChild(Dom.CreateElement("Record"))
                            For j = 0 To rsShopper.FieldCount - 1
                                If LCase(rsShopper.GetName(j)) <> "quickdownload" Then
                                    FieldNode = RecordNode.AppendChild(Dom.CreateElement("Field"))
                                    AttsNode = FieldNode.Attributes
                                    AttNode = AttsNode.SetNamedItem(Dom.CreateAttribute("name"))
                                    AttNode.Value = rsShopper.GetName(j)
                                    If rsShopper.Item(j).Equals(DBNull.Value) Then
                                        AttNode = AttsNode.SetNamedItem(Dom.CreateAttribute("isnull"))
                                        AttNode.Value = "1"
                                    Else
                                        Try
                                            FieldNode.InnerText = GetFieldText(rsShopper.Item(j))
                                        Catch ex As Exception
                                            ErrorState(context, "Error getting value for field [" & rsShopper.GetName(j) & "] in table [UserTable]", 1024, ex.Message)
                                        End Try
                                    End If
                                End If
                            Next
                        End If
                        rsShopper.Close()
                        rsShopper = Nothing

                        REM -- Add Lines
                        sql = "SELECT * FROM [ShopSalesLine] WHERE [HeaderGuid]=" & SafeString(rowOrder("HeaderGuid"))
                        Try
                            myCommand = New SqlCommand(sql, conn)
                            rsLine = myCommand.ExecuteReader
                            myCommand = Nothing
                        Catch ex As SqlException
                            ErrorState(context, "Error selecting order lines for Quickdownload", ex.ErrorCode, ex.Message)
                        End Try

                        While rsLine.Read
                            RecordNode = LineRecords.AppendChild(Dom.CreateElement("Record"))
                            For j = 0 To rsLine.FieldCount - 1
                                If LCase(rsLine.GetName(j)) <> "quickdownload" Then
                                    FieldNode = RecordNode.AppendChild(Dom.CreateElement("Field"))
                                    AttsNode = FieldNode.Attributes
                                    AttNode = AttsNode.SetNamedItem(Dom.CreateAttribute("name"))
                                    AttNode.Value = rsLine.GetName(j)
                                    If rsLine.Item(j).Equals(DBNull.Value) Then
                                        AttNode = AttsNode.SetNamedItem(Dom.CreateAttribute("isnull"))
                                        AttNode.Value = "1"
                                    Else
                                        Try
                                            FieldNode.InnerText = GetFieldText(rsLine.Item(j))
                                        Catch ex As Exception
                                            ErrorState(context, "Error getting value for field [" & rsLine.GetName(j) & "] in table [ShopSalesLine]", 1024, ex.Message)
                                        End Try
                                    End If
                                End If
                            Next
                        End While
                        rsLine.Close()
                        rsLine = Nothing
                    Next
                    context.Response.ContentType = "text/xml"
                    Dom.Save(context.Response.OutputStream)

                End If

            Case "generic"
                If ack <> "" Then
                    guidlist = querydict(genericTableName)
                    If guidlist Is Nothing Then guidlist = New ArrayList()

                    Dom.LoadXml("<XML><Code>0</Code><Message>Thanks</Message></XML>")
                    For i = 0 To guidlist.Count - 1

                        ID = guidlist(i)
                        idlist = Split(Replace(ID, "\;", "\ "), ";")
                        If UBound(idlist) <> UBound(genericKey) Then
                            ErrorState(context, "Received IDs does not match the defined key on the table", 1234, "")
                        End If
                        k = 1
                        WhereStatement = ""
                        For j = 0 To UBound(idlist)
                            idvalue = DecodeID(Mid(ID, k, Len(idlist(j))))
                            k = k + Len(idlist(j)) + 1
                            If j <> 0 Then
                                WhereStatement = WhereStatement & " AND "
                            End If
                            WhereStatement = WhereStatement & "[" & genericKey(j) & "]='" & idvalue & "'"
                        Next

                        sql = "UPDATE [" & genericTableName & "] SET QuickDownload=" & SafeDatetime(Now) & " WHERE " & WhereStatement
                        Try
                            myCommand = New SqlCommand(sql, conn)
                            myCommand.ExecuteNonQuery()
                            myCommand = Nothing
                        Catch ex As SqlException
                            ErrorState(context, "Error updating [" & genericTableName & "] - SQL=" & sql, ex.ErrorCode, ex.Message)
                        End Try
                    Next

                    context.Response.ContentType = "text/xml"
                    Dom.Save(context.Response.OutputStream)

                Else
                    REM -- Make sure that house-keeping field is in place

                    sql = "SELECT QuickDownload FROM [" & genericTableName & "] WHERE 1=2"
                    Try
                        myCommand = New SqlCommand(sql, conn)
                        myCommand.ExecuteNonQuery()
                        myCommand = Nothing
                    Catch ex As SqlException
                        If ex.ErrorCode = &H80040E10 Then ' QuickDownload field not present
                            sql = "ALTER TABLE [" & genericTableName & "] ADD QuickDownload DATETIME"
                            context.Application.Lock()
                            Try
                                myCommand = New SqlCommand(sql, conn)
                                myCommand.ExecuteNonQuery()
                                myCommand = Nothing
                            Catch ex1 As SqlException
                                context.Application.UnLock()
                                If ex1.ErrorCode <> &H80040E14 And ex1.ErrorCode <> &H80040E21 And ex1.ErrorCode <> 0 Then
                                    ErrorState(context, "Error adding Quickdownload column to " & genericTableName & " Table ", ex1.ErrorCode, ex1.Message)
                                End If
                            End Try
                            context.Application.UnLock()
                        Else
                            ErrorState(context, "Error checking for QuickDownload column in " & genericTableName & " Table ", ex.ErrorCode, ex.Message)
                        End If

                    End Try

                    REM -- Prepare DOM
                    TablesNode = rootnode.AppendChild(Dom.CreateElement("Tables"))

                    REM -- Prepare table
                    TableNode = TablesNode.AppendChild(Dom.CreateElement("Table"))
                    AttsNode = TableNode.Attributes
                    AttNode = AttsNode.SetNamedItem(Dom.CreateAttribute("name"))
                    AttNode.Value = genericTableName
                    KeyNode = TableNode.AppendChild(Dom.CreateElement("Key"))
                    FieldsNode = KeyNode.AppendChild(Dom.CreateElement("Fields"))
                    For i = 0 To UBound(genericKey)
                        FieldNode = FieldsNode.AppendChild(Dom.CreateElement("Field"))
                        FieldNode.InnerText = genericKey(i)
                    Next
                    RecordsNode = TableNode.AppendChild(Dom.CreateElement("Records"))

                    REM -- Select new records
                    sql = "SELECT * FROM [" & genericTableName & "] WHERE QuickDownload IS NULL"
                    Try
                        myCommand = New SqlCommand(sql, conn)
                        rsGeneric = myCommand.ExecuteReader
                        myCommand = Nothing
                    Catch ex As SqlException
                        ErrorState(context, "Error selecting new records for Quickdownload", ex.ErrorCode, ex.Message)
                    End Try

                    REM -- Add records to xml document
                    cnt = 0
                    While rsGeneric.Read And cnt < genericMaxRecords
                        cnt = cnt + 1

                        RecordNode = RecordsNode.AppendChild(Dom.CreateElement("Record"))
                        For i = 0 To rsGeneric.FieldCount - 1
                            If LCase(rsGeneric.GetName(i)) <> "quickdownload" Then
                                FieldNode = RecordNode.AppendChild(Dom.CreateElement("Field"))
                                AttsNode = FieldNode.Attributes
                                AttNode = AttsNode.SetNamedItem(Dom.CreateAttribute("name"))
                                AttNode.Value = rsGeneric.GetName(i)
                                If rsGeneric.Item(i).Equals(DBNull.Value) Then
                                    AttNode = AttsNode.SetNamedItem(Dom.CreateAttribute("isnull"))
                                    AttNode.Value = "1"
                                Else
                                    Try
                                        FieldNode.InnerText = GetFieldText(rsGeneric.Item(i))
                                    Catch ex As Exception
                                        ErrorState(context, "Error getting value for field [" & rsGeneric.GetName(i) & "] in table [" & genericTableName & "]", 1024, ex.Message)
                                    End Try
                                End If
                            End If
                        Next
                    End While
                    rsGeneric.Close()
                    rsGeneric = Nothing
                    context.Response.ContentType = "text/xml"
                    Dom.Save(context.Response.OutputStream)
                End If
        End Select
    End Sub

    Sub ErrorState(ByVal context As HttpContext, ByVal msg As String, ByVal errno As Object, ByVal errdesc As String)
        Dom.SelectSingleNode("/XML/Code").InnerText = errno.ToString
        Dom.SelectSingleNode("/XML/Message").InnerText = msg & "; " & errdesc
        context.Response.ContentType = "text/xml"
        Dom.Save(context.Response.OutputStream)
        context.Response.End()
    End Sub

    Function DecodeID(ByVal ID As String) As String
        DecodeID = Replace(Replace(ID, "\;", ";"), "\\", "\")
    End Function

    Function GetFieldText(ByVal field As Object) As String
        Dim retv As String

        Select Case TypeName(field)
            Case "String", "Integer", "Long", "Byte", "Short"
                retv = CStrEx(field)
            Case "Date"
                retv = XmlDateTime(field)
            Case "Double", "Single", "Decimal"
                retv = SafeFloat(field)
            Case "Boolean"
                If field Then
                    retv = "1"
                Else
                    retv = "0"
                End If
            Case Else
                Throw New Exception("QuickDownload does not support fields of type [" & TypeName(field) & "].")
        End Select

        GetFieldText = retv
    End Function

    Function XmlDateTime(ByVal d As Object) As String
        d = CDateEx(d)
        XmlDateTime = Right("0000" & Year(d), 4) & "-" & Right("00" & Month(d), 2) & "-" & Right("00" & Day(d), 2) & " " & Right("00" & Hour(d), 2) & ":" & Right("00" & Minute(d), 2) & ":" & Right("00" & Second(d), 2)
    End Function

    Function XmlDate(ByVal d As Object) As String
        d = CDateEx(d)
        XmlDate = Right("0000" & Year(d), 4) & "-" & Right("00" & Month(d), 2) & "-" & Right("00" & Day(d), 2)
    End Function

    Function XmlTime(ByVal d As Object) As String
        d = CDateEx(d)
        XmlTime = Right("00" & Hour(d), 2) & ":" & Right("00" & Minute(d), 2) & ":" & Right("00" & Second(d), 2)
    End Function

    Function InArray(ByVal haystack As Object, ByVal needle As Object) As Boolean
        Dim retv As Boolean, i As Integer

        retv = False
        For i = LBound(haystack) To UBound(haystack)
            If haystack(i) = needle Then
                retv = True
                Exit For
            End If
        Next

        InArray = retv
    End Function

    
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Shared Function GetRequest(ByVal varname As String) As String
        Dim retv As String = ""

        Try
            If Not HttpContext.Current.Request(varname) Is Nothing Then retv = HttpContext.Current.Request(varname)
        Catch
            retv = ""
        End Try

        Return retv
    End Function

    Public Shared Function QueryString2Dict() As Dictionary(Of String, ArrayList)
        Dim q, f, s As String

        q = HttpContext.Current.Request.QueryString.ToString
        f = HttpContext.Current.Request.Form.ToString

        s = q
        If f <> "" Then
            If s <> "" Then s = s & "&"
            s = s & f
        End If

        Return QueryString2Dict(s)
    End Function

    Public Shared Function QueryString2Dict(ByVal s As String) As Dictionary(Of String, ArrayList)
        Dim retv As Dictionary(Of String, ArrayList) = New Dictionary(Of String, ArrayList)
        Dim a() As String
        Dim e As String
        Dim n, v As String
        Dim i As Integer
        Dim stringlist As ArrayList

        If s = "" Then Return retv

        a = s.Split("&")

        For Each e In a
            i = e.IndexOf("=")
            If i >= 0 Then
                n = e.Substring(0, i)
                v = e.Substring(i + 1)
            Else
                n = e
                v = ""
            End If
            n = HttpUtility.UrlDecode(n)
            v = HttpUtility.UrlDecode(v)

            If retv.ContainsKey(n) Then
                stringlist = retv(n)
            Else
                stringlist = New ArrayList()
                retv.Add(n, stringlist)
            End If
            stringlist.Add(v)
        Next

        Return retv
    End Function

    Public Shared Function SafeDatetime(ByVal d As Object) As String
        Dim retv As String, dateval As Object

        dateval = DBNull.Value
        Try
            dateval = CDate(d)
        Catch
        End Try

        If Not dateval Is DBNull.Value Then
            retv = "{ts '" & Year(dateval).ToString.PadLeft(4, "0") & "-" & Month(dateval).ToString.PadLeft(2, "0") & "-" & Day(dateval).ToString.PadLeft(2, "0") & " " & Hour(dateval).ToString.PadLeft(2, "0") & ":" & Minute(dateval).ToString.PadLeft(2, "0") & ":" & Second(dateval).ToString.PadLeft(2, "0") & "'}"
        Else
            retv = "NULL"
        End If
        SafeDatetime = retv
    End Function
    
    Public Shared Function SafeString(ByVal s As Object) As String
        s = CStrEx(s)
        If s = "" Then
            Return "NULL"
        Else
            Return "'" & Replace(s, "'", "''") & "'"
        End If
    End Function

    Public Shared Function CStrEx(ByVal v As Object) As String
        Dim retv As String
        Try
            retv = v.ToString
        Catch
            retv = ""
        End Try
        Return retv
    End Function

    Public Shared Function SafeFloat(ByVal f As Object) As String
        Dim s As String

        If f Is DBNull.Value Then f = 0
        f = RoundEx(f, 10)
        s = CStr(f)
        REM -- Get the commaseparator and replace that by dot (.)
        REM -- This is done to make ADO understand the comma.
        Dim CommaSeparator As String
        CommaSeparator = Mid(CDblEx(11 / 10), 2, 1)
        Return Replace(s, CommaSeparator, ".")
    End Function

    Public Shared Function RoundEx(ByVal value As Decimal, ByVal decimals As Integer) As Decimal
        Dim Factor As Object

        value = CDblEx(value)
        decimals = Int(Math.Abs(decimals))
        Factor = 10 ^ decimals
        Return SymArith(value, Factor)
    End Function

    Public Shared Function SymArith(ByVal X As Object, ByVal Factor As Object) As Object
        Return Fix(X * Factor + 0.5 * Math.Sign(X)) / Factor
    End Function

    Public Shared Function CDblEx(ByVal v As Object) As Decimal
        CDblEx = 0
        Try
            CDblEx = CDec(v)
        Catch ex As Exception
        End Try
    End Function
    
    Public Shared Function CDateEx(ByVal v As Object) As Date
        CDateEx = Date.MinValue
        Try
            CDateEx = CDate(v)
        Catch
        End Try
    End Function

End Class