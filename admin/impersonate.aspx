<%@ Page Language="VB" AutoEventWireup="false" CodeFile="impersonate.aspx.vb" Inherits="admin_transport_impersonate"
    CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master" RuntimeMasterPageFile="ThreeColumn.master"
    CrumbName="<%$ Resources: Language, LABEL_IMPERSONATION %>" %>

<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.GlobalsClass" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <div class="ImpersonatePage">
        <uc1:PageHeader ID="PageHeader1" runat="server" Text="<%$ Resources: Language, LABEL_IMPERSONATION %>" />
        <h2><asp:Literal ID="Literal1" runat="server" Text="<%$ Resources: Language, LABEL_MESSAGE %>"></asp:Literal></h2>
        <p>
        <asp:Literal ID="litMessage" runat="server" Text="<%$ Resources: Language, LABEL_MESSAGE %>"></asp:Literal>
        </p>
    </div>
</asp:Content>
