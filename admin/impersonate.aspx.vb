Imports System.Configuration.ConfigurationManager
Imports ExpandIT.EISClass
Imports ExpandIT.GlobalsClass
Imports ExpandIT

Partial Class admin_transport_impersonate
    Inherits ExpandIT.Page

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim DisplayMessage As String
        Dim newuserguid As String
        Dim returl As String
        Dim IpFilter() As String = {}
        Dim AccessAllowed As Boolean

        Dim i As Integer
        
        ' Get request parameters
        newuserguid = Request.QueryString("UserGuid")
        DisplayMessage = ""

        If UBound(IpFilter) < 0 Then
            AccessAllowed = True
        Else
            AccessAllowed = False
            For i = 0 To UBound(IpFilter)
                If IpFilter(i) = Request.ServerVariables("REMOTE_ADDR") Then
                    AccessAllowed = True
                End If
            Next
        End If

        If AccessAllowed Then
            If newuserguid = "" Then
                DisplayMessage = DisplayMessage & Resources.Language.MESSAGE_THE_CALL_WAS_MISSING_PARAMETERS & "."
            Else
                ' Load the user based on the UserGuid.
                globals.User = eis.LoadUser(newuserguid)

                ' Check that the user was found.
                If globals.User("Anonymous") Then
                    DisplayMessage = DisplayMessage & Resources.Language.MESSAGE_USER_WAS_NOT_FOUND & "."
                Else
                    ' Set the new user guid in the cookie
                    Response.Cookies("store")("UserGuid") = newuserguid
                    eis.SetCookieExpireDate()

                    returl = Request("returl")
                    If returl <> "" Then
                        Response.Redirect(returl)
                    Else
                        DisplayMessage = DisplayMessage & Replace(Replace(Resources.Language.MESSAGE_YOU_HAVE_NOW_ENTERED_THE_SHOP_AS_THIS_USER, "%1", "<I>'" & globals.User("ContactName") & "'</I>"), "%2", "<I>'" & Request("UserGuid") & "'</I>") & "."
                    End If  'If returl <> "" Then
                End If  'If User("Anonymous") Then
            End If  'If newuserguid = "" Then
        Else
            DisplayMessage = Replace(Resources.Language.MESSAGE_IMPERSONATE_ACCESS_DENIED, "%1", Request.ServerVariables("REMOTE_ADDR"))
        End If ' If AccessAllowed
        litMessage.Text = DisplayMessage
    End Sub

End Class
