<%@ Import Namespace="System.Configuration.ConfigurationManager" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="password.aspx.vb" Inherits="admin_password"
    CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master"
    RuntimeMasterPageFile="ThreeColumn.master" %>

<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.GlobalsClass" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <h1>
        <% =Resources.Language.LABEL_USERDATA_NEW_PASSWORD%>
    </h1>
    <b>
        <% =Resources.Language.LABEL_MESSAGE%>
        :</b>
    <% = DisplayMessage %>
    <% If bShowForm Then%>
    <br />
    <br />
    (<% =Resources.Language.LABLE_USER%>: <i>
        <% = PasswordUser("ContactName") %>
        ,
        <% = PasswordUser("Address1") %>
        ,
        <% = PasswordUser("ZipCode") %>
        &nbsp;<% = PasswordUser("CityName") %></i>)
    
        <table border="0">
            <tr>
                <td class="TDR">
                    <% =Resources.Language.LABEL_USERDATA_NEW_PASSWORD%>
                    :</td>
                <td>
                    <input class="InpL" type="password" name="newExppassword1" size="20" maxlength="20"></td>
            </tr>
            <tr>
                <td class="TDR">
                    <% =Resources.Language.LABEL_USERDATA_NEW_PASSWORD_CONFIRM%>
                    :</td>
                <td>
                    <input class="InpL" type="password" name="newExppassword2" size="20" maxlength="20"></td>
            </tr>
            <tr>
                <td colspan="2">
                </td>
            </tr>
            <tr>
                <td colspan="2" class="TDR">
                    <input type="submit" value="<% = Resources.Language.ACTION_SUBMIT %>" name="submit1"
                        class="AddButton"></td>
            </tr>
        </table>
        <input type="HIDDEN" name="UserGuid" value="<% = Request("UserGuid") %>">
    
    <% End If%>
</asp:Content>
