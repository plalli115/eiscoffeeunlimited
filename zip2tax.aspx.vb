Imports System.Configuration.ConfigurationManager
Imports ExpandIT
Imports System.Web
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib

Partial Class zip2tax
    Inherits ExpandIT.Page

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)

        ' Check the page access.
        eis.PageAccessRedirect("Catalog")
        zipInfo()

    End Sub

    Public Sub zipInfo()

        Dim strServer As String = "db.Zip2Tax.com"
        Dim strUsername As String = "z2t_link"
        Dim strDBPassword As String = "H^2p6~r"
        Dim strDatabase As String = "zip2tax"
        Dim conn As Object
        Dim rs As Object

        'Open the connection

        conn = CreateObject("ADODB.Connection")
        conn.Open("driver=SQL Server;server=" & strServer & ";uid=" & strUsername & ";pwd=" & strDBPassword & ";database=" & strDatabase)

        'Assign values to the input variables
        Dim strZipCode As String = "33179" 'sample zip code must be between 90001 and 92999
        Dim strPassword As String = "we73es"

        'Open the recordset using the stored procedure
        rs = Server.CreateObject("ADODB.Recordset")
        rs.open("z2t_lookup_ExpandIT_ss('" & strZipCode & "', '" & strPassword & "')", conn, 3, 3, 4)

        'Read the results
        If Not rs.EOF Then
            Response.Write("Zip Code: " & cstrex(rs.fields("Zip_Code").value))
            Response.Write("Sales Tax Rate: " & cstrex(rs.fields("Sales_Tax_Rate").value))
            Response.Write("Post Office City: " & cstrex(rs.fields("Post_Office_City").value))
            Response.Write("County: " & cstrex(rs.fields("County").value))
            Response.Write("State: " & cstrex(rs.fields("State").value))
            Response.Write("Shipping Taxable: " & cstrex(rs.fields("Shipping_Taxable").value))
        End If

        'Close the Database
        rs.Close()
        conn.Close()

    End Sub

End Class