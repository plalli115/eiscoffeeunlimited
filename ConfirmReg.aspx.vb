﻿Imports System.Configuration.ConfigurationManager
Imports ExpandIT31.ExpanditFramework.Util
Imports ExpandIT31.ExpanditFramework.BLLBase 
Imports ExpandIT31.ExpanditFramework.Infrastructure 
Imports ExpandIT.ExpandITLib
Imports System.Data
'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
Imports ExpandIT
'AM2011031801 - ENTERPRISE CSR STAND ALONE - End

Partial Class ConfirmReg
    Inherits ExpandIT.Page
    
    Private Enum ConfirmStates
      OK  = 1
      Warn = 2
      Err = 3     
    End Enum
    
    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      If Not IsPostBack 
        Dim userGuid As String = Request.Params("UserRegKey")
        If Not String.IsNullOrEmpty(userGuid)
          Dim msg As String = ""
          Select processConfirmation(userGuid, msg)
            Case ConfirmStates.OK 
              globals.messages.Messages.Add(msg)
            Case ConfirmStates.Warn 
              globals.messages.Warnings.Add(msg) 
            Case ConfirmStates.Err 
              globals.messages.Errors.Add(msg)
          End Select          
        Else
          globals.messages.Errors.Add(Resources.Language.LABEL_REGISTRATION_CONFIRM_ERROR)
        End If 
      End If      
    End Sub
    
    Private Function processConfirmation(userGuid As String, Optional ByRef msg As String = Nothing) As ConfirmStates
      Dim result As ConfirmStates = ConfirmStates.Err       
      'get user data from db
      Dim sql As String = String.Format("SELECT * FROM UserTable WHERE UserGuid={0}", Utilities.SafeString(userGuid))
      Dim table as DataTable = DataAccess.SQL2DataTable(sql)      
      'check exists and status
      If table.Rows.Count > 0
        Dim Status As UserBase.UserStates = CType(table.Rows(0).Item("Status"), UserBase.UserStates)
        Select Status
          Case UserBase.UserStates.New
            If ActivateAccount(userGuid)
              msg = Resources.Language.LABEL_REGISTRATION_CONFIRM_OK
              result = ConfirmStates.OK 
            Else
              msg = Resources.Language.LABEL_REGISTRATION_CONFIRM_ERROR
              result = ConfirmStates.Err 
            End If 
          Case UserBase.UserStates.Deactivated
            msg = Resources.Language.LABEL_REGISTRATION_CONFIRM_DEACTIVATED_ERROR
            result = ConfirmStates.Err 
          Case Else
            msg = Resources.Language.LABEL_REGISTRATION_CONFIRM_ALREADY_ACTIVE 
            result = ConfirmStates.Warn  
        End Select        
      End If
      Return result
    End Function
    
    Private Function ActivateAccount(userGuid As string) As Boolean
      Dim result As Boolean = False
      Dim sql As String = String.Format("UPDATE UserTable SET Status = {0:d} WHERE UserGuid = {1}", UserBase.UserStates.Confirmed, Utilities.SafeString(userGuid))
      If String.IsNullOrEmpty (DataAccess.excecuteNonQueryDb(sql))
        'prepare autologin
        Session("UserGuid") = userGuid
        Response.Cookies("store")("UserGuid") = Session("UserGuid")
            'AM2011031801 - ENTERPRISE CSR STAND ALONE - Start
            If CBoolEx(AppSettings("EXPANDIT_US_USE_CSR")) Then
                Dim csrObj As New USCSR(globals)
                csrObj.setCSR2Cookie()
            End If
            'AM2011031801 - ENTERPRISE CSR STAND ALONE - End
        eis.SetCookieExpireDate()
        result = true 
      End If          
      Return result
    End Function
    

    Protected Sub HomeButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HomeButton.Click
      Response.Redirect("~/shop.aspx", True)      
    End Sub
End Class
