<%@ Import Namespace="System.Configuration.ConfigurationManager" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="catalog.aspx.vb" Inherits="catalog"
    CodeFileBaseClass="ExpandIT.Page" MasterPageFile="~/masters/default/main.master"
    RuntimeMasterPageFile="ThreeColumn.master" CrumbName="<%$ Resources: Language, LABEL_MENU_CATALOG %>" %>

<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Import Namespace="ExpandIT" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
    <div class="CatalogPage">
        <uc1:PageHeader ID="PageHeader1" runat="server" Text="<%$ Resources: Language, LABEL_MENU_CATALOG %>" />
        <asp:Panel ID="panelAvailable" runat="server" Visible="<%# CatalogAvailable %>">
            <table border="0" cellspacing="0" cellpadding="2" width="200px">
                <% For Each Catalog In Catalogs.Values%>
                <tr class="TR">
                    <td class="TDL">
                        <a href="<% = GroupLink(Catalog("GroupGuid")) %>">
                            <% = HTMLEncode(Catalog("NAME")) %></a>
                    </td>
                </tr>
                <% Next%>
            </table>
        </asp:Panel>
        <asp:Panel ID="panelNotAvailable" runat="server">
            <p>
                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources: Language, MESSAGE_NO_CATALOG_AVAILABLE %>"></asp:Literal>
            </p>
        </asp:Panel>
    </div>
</asp:Content>
