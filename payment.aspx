<%@ Page Language="VB" AutoEventWireup="false" CodeFile="payment.aspx.vb" Inherits="payment"
    ValidateRequest="false" EnableEventValidation="false" Debug="true" CodeFileBaseClass="ExpandIT.ShippingPayment.ShippingPaymentPage"
    MasterPageFile="~/masters/default/main.master" RuntimeMasterPageFile="ThreeColumn.master"
    CrumbName="<%$ Resources: Language, LABEL_PAYMENT_INFORMATION %>" %>

<%--AM2010080301 - CHECK OUT PROCESS MAP - Start--%>
<%@ MasterType VirtualPath="~/masters/EnterpriseBlue/ThreeColumn.master" %>
<%--AM2010080301 - CHECK OUT PROCESS MAP - End--%>
<%@ Import Namespace="ExpandIT" %>
<%@ Import Namespace="ExpandIT.EISClass" %>
<%@ Import Namespace="ExpandIT.ExpandITLib" %>
<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Register Namespace="ExpandIT.SimpleUIControls" TagPrefix="expui" %>
<%@ Register Src="~/controls/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<%--AM2010031501 - ORDER RENDER - Start--%>
<%@ Register Src="~/controls/USOrderRender/CartRender.ascx" TagName="CartRender"
    TagPrefix="uc1" %>
<%--AM2010031501 - ORDER RENDER - End--%>
<%--JA2010102801 - PAPERCHECK - Start--%>
<%@ Register Src="~/controls/USOrderRender/PaperChecks.ascx" TagName="PaperChecks"
    TagPrefix="uc1" %>
<%--JA2010102801 - PAPERCHECK - End--%>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSubMaster" runat="Server">
<%--AM2010041301 - ENTERPRISE PAYMENT GATEWAY - Start--%>
<%--<asp:ScriptManager ID="ScriptManager1" runat="server" />--%>
<%--AM2010041301 - ENTERPRISE PAYMENT GATEWAY - End--%>
    <%="" %>
    <!-- Here begins the HTML code for the page -->
    <div class="PaymentPage">
        <uc1:PageHeader ID="PageHeader1" runat="server" Text="<%$ Resources: Language, LABEL_PAYMENT_INFORMATION %>"
            EnableTheming="true" />
        <%
            If txtPaymentProcessError <> "" Then%>
        <div class="Error">
            <h3>
                <% =Resources.Language.LABEL_PAYMENT_ERROR_HEADING%>
            </h3>
            <% =Resources.Language.LABEL_PAYMENT_ERROR%>
            <ul>
                <li>
                    <% = txtPaymentProcessError %>
                </li>
            </ul>
        </div>
        <br />
        <br />
        <%
        End If
        %>
        <!-- AM2010031501 - ORDER RENDER - Start -->
        <%--<asp:Literal ID="litOrderRender" runat="server"></asp:Literal>--%>
        <asp:Panel ID="panelCartEmpty" runat="server" Visible="false">
            <!-- Display "empty cart" message -->
            <br />
            <div class="CartEmptyMessage">
                <% =Resources.Language.LABEL_ORDER_PAD_IS_EMPTY & "."%>
            </div>
        </asp:Panel>
        <asp:Panel ID="panelCartNotEmpty" runat="server" Visible="false">
            <uc1:CartRender ID="CartRender1" runat="server" IsActiveOrder="true" EditMode="false"
                IsEmail="false" ShowDate="false" ShowPaymentInfo="false" IsBrowser="true" IsLedger="false" />
        </asp:Panel>
        <!-- AM2010031501 - ORDER RENDER - End -->
        <!-- Here ends HTML code for displaying a commet and PO number fields -->
        <br />
        <!-- start DIBS required information for credit card payments on the internet -->
        *
        <% = strAdditionalInformation %>
        <br />
        <br />
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <expui:GenericPaymentControl ID="billform" runat="server" Visible="false" TableWidth="450"
                        ButtonText="ACTION_ACCEPT_2" HeaderText="LABEL_PAYMENT_INFORMATION" LabelText="LABEL_PAYMENT_INFORMATION_BILL" />
                </td>
            </tr>
        </table>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <expui:GenericPaymentControl ID="pickupform" runat="server" Visible="false" TableWidth="450"
                        ButtonText="ACTION_ACCEPT_2" HeaderText="LABEL_PAYMENT_INFORMATION" LabelText="LABEL_PAYMENT_INFORMATION_PICKUP" />
                </td>
            </tr>
        </table>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <expui:GenericPaymentControl ID="codform" runat="server" Visible="false" TableWidth="450"
                        ButtonText="ACTION_ACCEPT_2" HeaderText="LABEL_PAYMENT_INFORMATION" LabelText="LABEL_PAYMENT_INFORMATION_COD" />
                </td>
            </tr>
        </table>
        <%--JA2010022301 - New Payment Methods - Start--%>
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td align="left">
                    <expui:GenericPaymentControl ID="vkform" runat="server" Visible="false" TableWidth="100%"
                        ButtonText="ACTION_ACCEPT_2" HeaderText="LABEL_PAYMENT_INFORMATION" LabelText="LABEL_PAYMENT_INFORMATION_PREPAYMENT" />
                </td>
            </tr>
        </table>
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td align="left">
                    <expui:GenericPaymentControl ID="lsform" runat="server" Visible="false" TableWidth="100%"
                        ButtonText="ACTION_ACCEPT_2" HeaderText="LABEL_PAYMENT_INFORMATION" LabelText="LABEL_PAYMENT_INFORMATION_GIRO" />
                </td>
            </tr>
        </table>
        <%--JA2010022301 - New Payment Methods - End--%>
        <% If AppSettings("PAYMENT_PAYPAL") Then%>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <expui:PayPalPaymentControl ID="paypalform" runat="server" Visible="false" TableWidth="450"
                        ButtonText="ACTION_ACCEPT_2" HeaderText="LABEL_PAYMENT_INFORMATION" LabelText="LABEL_PAYMENT_INFORMATION_PAYPAL" />
                </td>
            </tr>
        </table>
        <% End If%>
        <!-- AM2010041301 - ENTERPRISE PAYMENT GATEWAY - Start -->
        <!-- Adding the control for the EEPG payment type. Modify or add more according to the generic needs.-->
        <% If AppSettings("PAYMENT_EEPG") Then%>
        <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="Server">
            <ContentTemplate>
                <%--<asp:Button ID="btnHiddenForUpdate" runat="server" Style="display: none;" OnClick="btnHiddenForUpdate_Click" />--%>
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <expui:EnterprisePaymentGatewayAIMControl ID="eepgform" runat="server" Visible="false"
                                TableWidth="600" ButtonText="ACTION_ACCEPT_2" HeaderText="LABEL_PAYMENT_INFORMATION"
                                LabelText="MESSAGE_CLICK_TO_SUBMIT_EEPG" TextPart2="ACTION_ACCEPT_2" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <%--<Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnHiddenForUpdate" />
            </Triggers>--%>
        </asp:UpdatePanel>
        
        
        <!-- JA2011042101 - EEPG USE EXISTING CREDIT CARDS - Start -->
        <% If CustomerPaymentMethod() = "EEPG" Then %>
        <% Dim htmlEEPG As String = objEEPG.getCreditCardList()%>
        <%If CStrEx(htmlEEPG) <> "" Then%>
            <table border="0" cellpadding="0" cellspacing="0" style="width:100%;"> 
                <tr>
                    <td>
                        <a onclick="javascript:animatedcollapse.toggle('ExistingCreditCards')" style="cursor: pointer;">
                            <asp:Label ID="WriteReview" Text="<%$ Resources: Language, LABEL_EEPG_EXISTING_CC %>"
                                runat="server" />
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a onclick="javascript:clearFrom();" style="cursor: pointer;">
                            <asp:Label ID="lblClearForm" style="display:none;" Text="<%$ Resources: Language, LABEL_EEPG_CLEAR_FROM %>"
                                runat="server" />
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="ExistingCreditCards" style="display: none; width: 100%; height: 300px; top: -325px;
                            border: 1px solid gray; background-color: white; position: relative; padding-top: 5px;
                            padding-bottom: 5px; overflow: auto;">
                            <%= CStrEx(htmlEEPG)  %>
                        </div>
                    </td>
                </tr>                      
            </table>
        <%End If%> 
        <% End If %>
        <!-- JA2011042101 - EEPG USE EXISTING CREDIT CARDS - End -->
        
        
        
        <% End If%>
        <!-- AM2010041301 - ENTERPRISE PAYMENT GATEWAY - End -->
        <% If AppSettings("PAYMENT_DIBS") Then%>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <expui:DibsPaymentControl ID="dibsform" runat="server" Visible="false" TableWidth="450"
                        ButtonText="ACTION_ACCEPT_2" HeaderText="LABEL_PAYMENT_INFORMATION" LabelText="LABEL_DIBS_3"
                        TextPart2="ACTION_ACCEPT_2" RenderScriptBlock="true" />
                </td>
            </tr>
        </table>
        <% End If%>
        <% If AppSettings("PAYMENT_AUTHORIZENET") Then%>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <!--JA2010022602 - AUTHORIZENET - Start-->
                    <expui:AuthNetAIMPaymentControl ID="authnetform" runat="server" Visible="false" TableWidth="450"
                        ButtonText="ACTION_ACCEPT_2" HeaderText="LABEL_PAYMENT_INFORMATION" LabelText="MESSAGE_CLICK_TO_SUBMIT_AUTHORIZE_DOT_NET"
                        TextPart2="ACTION_ACCEPT_2" />
                    <!--JA2010022602 - AUTHORIZENET - End-->
                </td>
            </tr>
        </table>
        <% End If%>
        <!--JA2010022601 - ECHECK - Start-->
        <% If AppSettings("PAYMENT_ECHECK_NET") Then%>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <expui:EcheckPaymentControl ID="echeckform" runat="server" Visible="false" TableWidth="465"
                        ButtonText="ACTION_ACCEPT_2" HeaderText="LABEL_PAYMENT_INFORMATION" LabelText="MESSAGE_CLICK_TO_GO_TO_AUTHORIZE_DOT_NET_ECHECK"
                        TextPart2="ACTION_ACCEPT_2" />
                </td>
            </tr>
        </table>
        <% End If%>
        <!--JA2010022601 - ECHECK - End-->
        <%--JA2010102801 - PAPERCHECK - Start--%>
        <% If AppSettings("PAYMENT_PAPERCHECK_NET") Then%>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <asp:Button ID="btnAddPaperCheck" runat="server" Visible="false" CssClass="AddButton" 
                    Text="<%$ Resources: Language, LABEL_PAPERCHECK_ADD %>" />
                    <br />
                    <br />
                    <uc1:PaperChecks runat="server" ID="PaperChecks1" Visible="false"/>
                    <br />
                    <asp:Button ID="btnAcceptPaperChecks" runat="server" Visible="false" CssClass="AddButton" 
                        Text="<%$ Resources: Language, ACTION_ACCEPT_2 %>" PostBackUrl="_payment.aspx" />
                    <%--<expui:PaperCheckPaymentControl ID="papercheckform" runat="server" Visible="false" HideComments="True" 
                        TableWidth="450"  ButtonText="ACTION_ACCEPT_2" HeaderText="LABEL_PAYMENT_INFORMATION"
                        LabelText="LABEL_PAYMENT_INFORMATION_PAPERCHECK" TextPart2="ACTION_ACCEPT_2" />--%>
                </td>
            </tr>
        </table>
        <%End If %>
        
        <%--JA2010102801 - PAPERCHECK - End--%>
        <!-- Here ends the HTML code for the page -->
    </div>

    <script type="text/javascript" language="javascript">
        function postAuthorizeNet(sNewFormAction)
        {
            if(document.layers) //The browser is Netscape 4
            {                
                document.layers['Content'].document.forms[0].__VIEWSTATE.name = 
                                                                   'NOVIEWSTATE';
                document.layers['Content'].document.forms[0].action = sNewFormAction;  
                document.layers['Content'].document.forms[0].submit();                    
            }
            else //It is some other browser that understands the DOM
            {                      
                document.forms[0].__VIEWSTATE.name = 'NOVIEWSTATE';     
                document.forms[0].action = sNewFormAction;
                document.forms[0].submit();          
            }
        }


        function preventDoubleClick(btn) {
            btn.disabled = true;
            btn.className = "AddButton Processing";
        }
        
    </script>
    
    <!-- JA2011042101 - EEPG USE EXISTING CREDIT CARDS - Start -->

    <script type="text/javascript"> 
        animatedcollapse.addDiv('ExistingCreditCards', 'fade=1')
        animatedcollapse.ontoggle=function($, divobj, state){}
        animatedcollapse.init()
        
        function clearFrom(){
            document.getElementById('cphRoot_cphSubMaster_lblClearForm').style.display='none';
            document.getElementById('cphRoot_cphSubMaster_eepgform_FName').value='';
            document.getElementById('cphRoot_cphSubMaster_eepgform_FName').disabled=false;
            document.getElementById('cphRoot_cphSubMaster_eepgform_FNameValidator').style.visibility='hidden';
            
            document.getElementById('cphRoot_cphSubMaster_eepgform_LName').value='';
            document.getElementById('cphRoot_cphSubMaster_eepgform_LName').disabled=false;
            document.getElementById('cphRoot_cphSubMaster_eepgform_LNameValidator').style.visibility='hidden';
            
            document.getElementById('cphRoot_cphSubMaster_eepgform_CCN').value='';
            document.getElementById('cphRoot_cphSubMaster_eepgform_CCN').disabled=false;
            document.getElementById('cphRoot_cphSubMaster_eepgform_CCNValidator').style.visibility='hidden';
            
            document.getElementById('cphRoot_cphSubMaster_eepgform_ExpDate').value='';
            document.getElementById('cphRoot_cphSubMaster_eepgform_ExpDate').disabled=false;
            document.getElementById('cphRoot_cphSubMaster_eepgform_ExpDateValidator').style.visibility='hidden';
            
            document.getElementById('cphRoot_cphSubMaster_eepgform_CCV').value='';
            <%If CBoolEx(AppSettings("PAYMENT_EEPG_CVV_MANDATORY")) Then %>
                document.getElementById('cphRoot_cphSubMaster_eepgform_CCVDateValidator').style.visibility='hidden';
            <%End If %>
            document.getElementById('cphRoot_cphSubMaster_eepgform_decryptData').value='';
            
            //Address
            <%If CBoolEx(AppSettings("PAYMENT_EEPG_REQUIRE_ADDRESS_INFO")) Then %>
                document.getElementById('cphRoot_cphSubMaster_eepgform_cbxAddress').style.visibility='visible';
                document.getElementById('cphRoot_cphSubMaster_eepgform_cbxAddress').checked=false;
                document.getElementById('cphRoot_cphSubMaster_eepgform_lblCbxAddress').style.display='block';
                  
                document.getElementById('cphRoot_cphSubMaster_eepgform_Address1').value='';
                document.getElementById('cphRoot_cphSubMaster_eepgform_Address1Validator').style.visibility='hidden';          
                document.getElementById('cphRoot_cphSubMaster_eepgform_Address2').value='';
                document.getElementById('cphRoot_cphSubMaster_eepgform_City').value='';
                document.getElementById('cphRoot_cphSubMaster_eepgform_CityValidator').style.visibility='hidden';
                document.getElementById('cphRoot_cphSubMaster_eepgform_State').value='';
                document.getElementById('cphRoot_cphSubMaster_eepgform_StateValidator').style.visibility='hidden';
                document.getElementById('cphRoot_cphSubMaster_eepgform_Phone').value='';
                document.getElementById('cphRoot_cphSubMaster_eepgform_Email').value='';
                document.getElementById('cphRoot_cphSubMaster_eepgform_EmailValidator').style.visibility='hidden';
            <%End If %>
            
            <%If CBoolEx(AppSettings("PAYMENT_EEPG_REQUIRE_ZIP_CODE")) Then %>    
                document.getElementById('cphRoot_cphSubMaster_eepgform_ZipCode').value='';
                document.getElementById('cphRoot_cphSubMaster_eepgform_ZipCodeValidator').style.visibility='hidden';
            <%End If %>
            
            <%If CBoolEx(AppSettings("PAYMENT_EEPG_REQUIRE_ZIP_CODE")) AND NOT CBoolEx(AppSettings("PAYMENT_EEPG_REQUIRE_ADDRESS_INFO")) Then %>
                document.getElementById('cphRoot_cphSubMaster_eepgform_cbxZipCode').style.display='block';
                document.getElementById('cphRoot_cphSubMaster_eepgform_lblCbxZipCode').style.display='block';
            <%End If %>
            
        }
        
        function SetParametersEEPG(theArray){
            var splitResult=theArray.split('|');
            
            document.getElementById('cphRoot_cphSubMaster_eepgform_FName').value=splitResult[0];
            document.getElementById('cphRoot_cphSubMaster_eepgform_FName').disabled=true;
            document.getElementById('cphRoot_cphSubMaster_eepgform_FNameValidator').style.visibility='hidden';
    
            document.getElementById('cphRoot_cphSubMaster_eepgform_LName').value=splitResult[1];
            document.getElementById('cphRoot_cphSubMaster_eepgform_LName').disabled=true;
            document.getElementById('cphRoot_cphSubMaster_eepgform_LNameValidator').style.visibility='hidden';
            
            document.getElementById('cphRoot_cphSubMaster_eepgform_CCN').value=splitResult[2];
            document.getElementById('cphRoot_cphSubMaster_eepgform_CCN').disabled=true;
            document.getElementById('cphRoot_cphSubMaster_eepgform_CCNValidator').style.visibility='hidden';
            
            document.getElementById('cphRoot_cphSubMaster_eepgform_ExpDate').value='XXXX';
            document.getElementById('cphRoot_cphSubMaster_eepgform_ExpDate').disabled=true;
            document.getElementById('cphRoot_cphSubMaster_eepgform_ExpDateValidator').style.visibility='hidden';
            //document.getElementById('cphRoot_cphSubMaster_eepgform_CCV')
            <%If CBoolEx(AppSettings("PAYMENT_EEPG_CVV_MANDATORY")) Then %>
                document.getElementById('cphRoot_cphSubMaster_eepgform_CCVDateValidator').style.visibility='hidden';
            <%End If %>
            document.getElementById('cphRoot_cphSubMaster_eepgform_decryptData').value=splitResult[3];
            
            //Address
            <%If CBoolEx(AppSettings("PAYMENT_EEPG_REQUIRE_ADDRESS_INFO")) Then %>
                document.getElementById('cphRoot_cphSubMaster_eepgform_cbxAddress').style.visibility='hidden';
                document.getElementById('cphRoot_cphSubMaster_eepgform_lblCbxAddress').style.display='none';
                
                document.getElementById('cphRoot_cphSubMaster_eepgform_Address1').value=splitResult[4];
                document.getElementById('cphRoot_cphSubMaster_eepgform_Address1Validator').style.visibility='hidden';
                document.getElementById('cphRoot_cphSubMaster_eepgform_Address2').value=splitResult[5];
                document.getElementById('cphRoot_cphSubMaster_eepgform_City').value=splitResult[6];
                document.getElementById('cphRoot_cphSubMaster_eepgform_CityValidator').style.visibility='hidden';
                document.getElementById('cphRoot_cphSubMaster_eepgform_State').value=splitResult[7];
                document.getElementById('cphRoot_cphSubMaster_eepgform_StateValidator').style.visibility='hidden';
                document.getElementById('cphRoot_cphSubMaster_eepgform_Phone').value=splitResult[8];
                document.getElementById('cphRoot_cphSubMaster_eepgform_Email').value=splitResult[9];
                document.getElementById('cphRoot_cphSubMaster_eepgform_EmailValidator').style.visibility='hidden';
            <%End If %>
            
            <%If CBoolEx(AppSettings("PAYMENT_EEPG_REQUIRE_ZIP_CODE")) Then %>
                document.getElementById('cphRoot_cphSubMaster_eepgform_ZipCode').value=splitResult[10];
                document.getElementById('cphRoot_cphSubMaster_eepgform_ZipCodeValidator').style.visibility='hidden';
            <%End If %>
            
            <%If CBoolEx(AppSettings("PAYMENT_EEPG_REQUIRE_ZIP_CODE")) AND NOT CBoolEx(AppSettings("PAYMENT_EEPG_REQUIRE_ADDRESS_INFO")) Then %>
                document.getElementById('cphRoot_cphSubMaster_eepgform_cbxZipCode').style.display='none';
                document.getElementById('cphRoot_cphSubMaster_eepgform_lblCbxZipCode').style.display='none';
            <%End If %>
            
            
            document.getElementById('cphRoot_cphSubMaster_lblClearForm').style.display='block';
        }
        
        
    </script>

    <!-- JA2011042101 - EEPG USE EXISTING CREDIT CARDS - End -->
    <%--<script type="text/javascript" language="javascript">
        function openLoading(){
            //Loading wait
                var divLoading=document.getElementById('ctl00_ctl00_cphRoot_cphSubMaster_eepgform_Loading');
                var loadingImg=document.getElementById('ctl00_ctl00_cphRoot_cphSubMaster_eepgform_LoadingImg');
                loadingImg.style.paddingTop='80px';
                divLoading.style.top='835px';
                divLoading.style.left='709px';
                divLoading.style.width='195px';
                divLoading.style.height='180px';
                divLoading.style.display='block';
            //Loading wait
        }
        function uploadPanel(){
            document.getElementById('ctl00_ctl00_cphRoot_cphSubMaster_eepgform_btnHiddenForUpdate').click();
        }
        function closeLoading(){ 
            var divLoading=document.getElementById('ctl00_ctl00_cphRoot_cphSubMaster_eepgform_Loading');
            divLoading.style.display='none';
        }
    
    </script>--%>
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
</asp:Content>